/***************************************************************************
* File        : osel_arch.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <osel_arch.h>

#if NO_SYS == 0

void osel_post(osel_signal_t sig, osel_param_t param, osel_eblock_prio_t event_prio)
{
    ;
}


#endif
