/***************************************************************************
* File        : wsnos_epool.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件是OS缓存消息缓存块的缓存池的实现
 *
 * file	: wsnos_epool.c
 *  
 * Date	: 2011--8--04
 *  
**/

#include <wsnos.h>

static list_head_t epool_list_head;				

#if OSEL_EPOOL_DBG_EN > 0
static osel_uint8_t g_osel_maxepool_cnt = 0;   		
osel_uint8_t g_osel_epool_cnt = 0;	
static uint32_t g_epool_free_cnt = 0;
#endif

osel_bool_t osel_epool_init(osel_uint32_t event_cnts)
{
	event_pool_t *epool = OSEL_NULL;
	osel_uint8_t i =0;

	list_init(&epool_list_head);
	
	epool = (event_pool_t *)osel_mem_alloc(sizeof(event_pool_t) * event_cnts);
	OSEL_ASSERT(epool != OSEL_NULL);

	if (epool == OSEL_NULL)
	{
		return OSEL_FALSE;
	}
	for (i=0; i<event_cnts; i++)
	{
		list_add_to_tail(&(epool + i)->list, &epool_list_head);
	}
	
    return OSEL_TRUE;
}

event_pool_t *osel_epool_malloc(_EPOOL_LINE1_)
{
	event_pool_t *epool = OSEL_NULL;
	epool = list_entry_decap(&epool_list_head, event_pool_t, list);

	if(epool != OSEL_NULL)
	{
		osel_memset((void *)epool, 0,sizeof(event_pool_t));
		list_init(&(epool->list));
        #if OSEL_EPOOL_DBG_EN > 0
		epool->alloc_line = line;

		if(g_osel_maxepool_cnt < ++g_osel_epool_cnt)
		{
			g_osel_maxepool_cnt = g_osel_epool_cnt;
		}
        #endif
	}

	return epool;
}

void osel_epool_free(event_pool_t **epool _EPOOL_LINE2_)
{
	OSEL_ASSERT(*epool != OSEL_NULL);
	if(*epool == OSEL_NULL)
	{
		return;
	}

    #if OSEL_EPOOL_DBG_EN > 0
	g_osel_epool_cnt--;
	#endif

	osel_memset((void *)(*epool), 0, sizeof(event_pool_t));

    #if OSEL_EPOOL_DBG_EN > 0
	(*epool)->free_line = line;
    g_epool_free_cnt++;
	#endif

	list_add_to_tail(&((*epool)->list), &epool_list_head);	

	*epool = OSEL_NULL;
}


/**@}**/

