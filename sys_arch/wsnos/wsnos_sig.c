#include <wsnos.h>
#include <debug.h>
#define OS_SIG_NUM	(20u)
typedef  void (*os_sig_cb)();
typedef struct
{
	uint16_t sig;
	os_sig_cb sig_cb;
}os_sig_t;

os_sig_t os_sig_group[OS_SIG_NUM];

bool_t os_sig_bind(uint16_t sig,void *cb)
{
	for(int i=0; i<OS_SIG_NUM; i++)
	{
		if(os_sig_group[i].sig == OSEL_INIT_SIG || os_sig_group[i].sig == sig)
		{
			os_sig_group[i].sig = sig;
			os_sig_group[i].sig_cb= (os_sig_cb)cb;
			return TRUE;
		}
	}
	return FALSE;
}

static void os_sig_task(void *e)
{
	DBG_ASSERT(e != NULL __DBG_LINE);
	event_block_t *pmsg = (event_block_t *)e;
	uint16_t sig = ((uint32_t)(pmsg->param) & 0xFFFF);
	for(int i=0; i<OS_SIG_NUM; i++)
	{
		if(os_sig_group[i].sig == sig)
		{
			os_sig_group[i].sig_cb();
		}
	}
}

void os_msg_init(void)
{
	osel_task_tcb *os_task_handle = osel_task_create(&os_sig_task, OS_TASK_PRIO);
	osel_subscribe(os_task_handle, WSNOS_EVENT);
	osel_memset(os_sig_group, OSEL_INIT_SIG, OS_SIG_NUM);
}