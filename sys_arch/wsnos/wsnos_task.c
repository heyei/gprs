/***************************************************************************
* File        : wsnos_task.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件是os的任务创建部分
 *
 * file	: wsnos_task.c
 *
 * Date	: 2011--8--04
 *
**/

#include <wsnos.h>

static list_head_t osel_event_list_head[OSEL_MAX_PRIO];
osel_task_tcb g_task_cb[OSEL_MAX_PRIO];

static osel_task_handler osel_idle_task_handler;

#if OSEL_DBG_EN > 0
static osel_uint8_t osel_register_grp = 0;
static osel_uint8_t osel_register_tbl[((OSEL_MAX_PRIO-1)/8) + 1];
#endif

void osel_init(void)
{
    osel_mem_init();
    osel_memset(&osel_event_list_head[0], 0x00, sizeof(osel_event_list_head));
    osel_memset(&g_task_cb[0], 0x00, sizeof(g_task_cb));
    osel_idle_task_handler = NULL;
#if OSEL_DBG_EN > 0
    osel_register_grp = 0;
    osel_memset(&osel_register_tbl[0], 0x00, sizeof(osel_register_tbl));
#endif
	osel_sub_init();
#if OSEL_USE_MULTIPLES
	osel_epool_init(OSEL_EPOOL_NUM);
#endif
	osel_eblock_init(OSEL_EBLOCK_NUM);
	osel_rdy_grp = 0x00;
	osel_memset(&osel_rdy_tbl[0], 0x00, OSEL_DIM(osel_rdy_tbl));
	
	os_msg_init();
}

osel_task_tcb *osel_task_create(osel_task_handler const handler, osel_uint8_t prio)
{

	OSEL_ASSERT(prio < OSEL_MAX_PRIO);

#if OSEL_DBG_EN > 0
    osel_uint8_t i = 0;
	if((osel_register_grp & osel_map_tbl[prio >> 3]) == osel_map_tbl[prio >> 3]
		&& ((osel_register_tbl[prio >> 3] & osel_map_tbl[prio & 0x07])
			== osel_map_tbl[prio & 0x07]))
	{
		OSEL_ASSERT(OSEL_FALSE);
	}
	osel_register_grp |= osel_map_tbl[prio >> 3];
    osel_register_tbl[prio >> 3] |= osel_map_tbl[prio & 0x07];

	for(i=0; i<OSEL_MAX_PRIO; i++)
	{
		if(g_task_cb[i].task.handler == handler)
		{
			OSEL_ASSERT(OSEL_FALSE);
		}
	}
#endif
    osel_task_tcb *tcb  = &g_task_cb[prio];
	tcb->task.handler = handler;
	tcb->task.prio = prio;
	tcb->queue = osel_event_list_head[prio];
	list_init(&tcb->queue);

	return tcb;
}

void osel_idle_hook(osel_task_handler idle)
{
	if(idle != OSEL_NULL)
	{
		osel_idle_task_handler = idle;
	}
}

static void osel_onidle(void)
{
    if(osel_idle_task_handler != OSEL_NULL)
    {
		osel_idle_task_handler(OSEL_NULL);
	}
}

void osel_run(void)
{
    osel_start();

    OSEL_INT_LOCK();
    osel_schedule();
    OSEL_INT_UNLOCK();

    while(1)
    {
        osel_onidle();
    }
}

/**@}**/

