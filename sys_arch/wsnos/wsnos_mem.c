/***************************************************************************
* File        : wsnos_mem.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *  这个文件实现了内存的静态申请管理
 *
 * file : wsnos_mem.c
 *
 * Date : 2011--8--04
 *
**/
#include <wsnos.h>

#if OSEL_STATIC_MEM_EN > 0

#if OSEL_MEM_ALIGNMENT == 4
#define HEAP_BYTE_ALIGNMENT_MASK    ((osel_uint32_t)0x0003)
#endif

#if OSEL_MEM_ALIGNMENT == 2
#define HEAP_BYTE_ALIGNMENT_MASK    ((osel_uint32_t)0x0001)
#endif

#if OSEL_MEM_ALIGNMENT == 1
#define HEAP_BYTE_ALIGNMENT_MASK    ((osel_uint32_t)0x0000)
#endif

#ifndef HEAP_BYTE_ALIGNMENT_MASK
#error "Invalid OSEL_MEM_ALIGNMENT definition"
#endif


static struct _heap
{
    osel_uint16_t padding;
    osel_uint8_t store[OSEL_HEAP_SIZE];
} heap;

static osel_uint32_t next_free_byte = (osel_uint32_t)0;

void osel_mem_init(void)
{
    osel_memset(&heap, 0, sizeof(heap));
    next_free_byte = 0;
}

void *osel_mem_alloc(osel_uint16_t size)
{
    void *mem = NULL;

#if OSEL_MEM_ALIGNMENT != 1
    if ( size & HEAP_BYTE_ALIGNMENT_MASK )
    {
        size += (OSEL_MEM_ALIGNMENT - (size & HEAP_BYTE_ALIGNMENT_MASK));
    }
#endif

    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);
    {
        if ((( next_free_byte + size) <= OSEL_HEAP_SIZE)
                && (( next_free_byte + size) > next_free_byte))
        {
            mem = &(heap.store[next_free_byte]);
            next_free_byte += size;
        }
    }
    OSEL_EXIT_CRITICAL(status);

    return mem;
}

void osel_mem_free(void *mem)
{
    (void)mem;
}

void osel_memset(void *const dst, osel_uint8_t val, osel_uint16_t len)
{
    OSEL_ASSERT(dst != OSEL_NULL);
    osel_uint8_t *p = (osel_uint8_t *)dst;

    while (len--)
    {
        *p++ = val;
    }
}

void osel_memcpy(void *dst, const void *const src, osel_uint16_t len)
{
    OSEL_ASSERT(dst != OSEL_NULL);
    OSEL_ASSERT(src != OSEL_NULL);
    osel_uint8_t *tmp = (osel_uint8_t *)dst;
    osel_uint8_t *s = (osel_uint8_t *)src;

    while (len--)
    {
        *tmp++ = *s++;
    }
}

bool_t osel_memcmp(void *const dst, const void *const src, osel_uint16_t len)
{
    OSEL_ASSERT(dst != OSEL_NULL);
    OSEL_ASSERT(src != OSEL_NULL);
    osel_uint8_t *tmp = (osel_uint8_t *)dst;
    osel_uint8_t *s = (osel_uint8_t *)src;
    while (len--)
    {
        if (*tmp++ != *s++)
        {
            return FALSE;
        }
    }
    return TRUE;
}

#endif

