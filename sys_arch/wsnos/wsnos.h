/***************************************************************************
* File        : wsnos.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件实现了OS给外部使用的接口申明
 *
 * file	: wsnos.h
 *
 * Date	: 2011--8--04
 *
**/
#ifndef __WSNOS_H
#define __WSNOS_H

#include <wsnos_internal.h>


/**
 * 系统初始化
 */
void osel_init(void);


/**
 * 创建任务
 *
 * @handler - 任务入口地址
 * @prio - 任务的优先级
 *
 * @return - 指向任务的任务控制块
 */
osel_task_tcb *osel_task_create(osel_task_handler handler,
                          osel_uint8_t prio);

/**
 * 系统开始运行
 */
void osel_run(void);


/**
 *	idle 回调函数
 *	@idle - 设置的空闲钩子函数
 */
void osel_idle_hook(osel_task_handler idle);

/**
 * 内存模块初始化
 */
void osel_mem_init(void);

/**
 * 内存申请
 *
 * @size - 内存大小
 *
 * @return - 指向申请内存的指针
 */
void *osel_mem_alloc(osel_uint16_t size);


/**
 * 内存拷贝
 *
 * @dst - 拷贝到的内存地址
 * @src - 需要拷贝的原内存地址
 * @len - 拷贝内存长度
 *
 */
void osel_memcpy(void *const dst, const void *const src, osel_uint16_t len);


/**
 * 内存设置
 *
 * @dst - 需要设置的内存地址
 * @val - 内存设置的值
 * @len - 需要设置内存长度
 *
 */
void osel_memset(void *const dst, osel_uint8_t val, osel_uint16_t len);

/**
 * 内存比较
 *
 * @dst - 需要设比较的内存地址
 * @val - 需要设比较的内存地址
 * @len - 需要比较的内存长度
 *
 */
bool_t osel_memcmp(void *const dst, const void *const src, osel_uint16_t len);

/**
 *  把消息与任务之间的关联建立
 *
 *  @tcb -指向关联的任务控制块
 *  @sig -消息类型
 */
void osel_subscribe(const osel_task_tcb *const tcb, osel_signal_t sig);


/**
 *  把消息与任务之间的关联取消掉
 *
 *  @tcb -指向关联的任务控制块
 *  @sig -消息类型
 */
void osel_unsubscribe(osel_task_tcb *const tcb, osel_signal_t sig);


/**
 *  发送一个指定参数和优先级的消息，os 自动发送消息到任务
 *
 *  @sig -消息类型
 *  @param - 消息携带参数
 *  @epool_prio - 消息优先级
 */
void osel_post(osel_signal_t sig, osel_param_t param, osel_eblock_prio_t event_prio);

#endif

