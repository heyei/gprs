/***************************************************************************
* File        : wsnos_internal.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件是内部需要的一些头文件申明
 *
 * file	: wsnos_internal.h
 *  
 * Date	: 2011--8--04
 *  
**/
#ifndef __WSNOS_INTERNAL_H
#define __WSNOS_INTERNAL_H

#include <wsnos_config.h>
#include <wsnos_port.h>
#include <list.h>
#include <wsnos_exa.h>
#include <wsnos_sig.h>

#define OSEL_NULL               	(void *)0
#define OSEL_DIM(array_) 			(sizeof(array_) / sizeof(array_[0]))

/**
 *  事件内存块宏配置
**/
#if OSEL_EBLOCK_DBG_EN > 0
#define _EBLOCK_LINE1_  uint16_t line
#define _EBLOCK_LINE2_  ,uint16_t line
#define __EBLOCK_LINE1  __LINE__
#define __EBLOCK_LINE2  ,__LINE__
#else
#define _EBLOCK_LINE1_  void
#define _EBLOCK_LINE2_
#define __EBLOCK_LINE1
#define __EBLOCK_LINE2
#endif

/**
 *  缓冲事件内存块宏配置
**/
#if OSEL_EPOOL_DBG_EN > 0
#define _EPOOL_LINE1_  uint16_t line
#define _EPOOL_LINE2_  ,uint16_t line
#define __EPOOL_LINE1  __LINE__
#define __EPOOL_LINE2  ,__LINE__
#else
#define _EPOOL_LINE1_  void
#define _EPOOL_LINE2_
#define __EPOOL_LINE1
#define __EPOOL_LINE2
#endif


/**
 *  事件内存块
**/
typedef osel_uint16_t osel_signal_t;
typedef void * osel_param_t;

typedef struct _event_block_tag_
{
    list_head_t list;

    osel_signal_t sig;
    osel_param_t param;
    osel_uint8_t event_prio;
    osel_uint8_t cnt;

#if OSEL_EBLOCK_DBG_EN > 0
    osel_uint16_t alloc_line;
    osel_uint16_t free_line;
#endif
} event_block_t;


/**
 *  缓冲事件内存
**/
typedef enum _osel_eblock_prio
{
    OSEL_EVENT_PRIO_LOW = 0,
    OSEL_EVENT_PRIO_HIG = 1
} osel_eblock_prio_t;

typedef struct _osel_pool_tag_
{
    list_head_t list;
    event_block_t *eblock;

#if OSEL_EPOOL_DBG_EN > 0
    osel_uint16_t alloc_line;
    osel_uint16_t free_line;
#endif
} event_pool_t,*p_event_pool_t;

typedef void (*osel_task_handler)(void *e);  // 调度器

typedef struct _osel_task_tag_
{
    osel_task_handler handler;
    osel_uint8_t prio;
} osel_task_t;

typedef struct task_cb_tag
{
    osel_task_t task;
    list_head_t queue;
} osel_task_tcb;

extern osel_uint8_t osel_rdy_grp;
extern osel_uint8_t osel_rdy_tbl[((OSEL_MAX_PRIO-1)/8) + 1];


/**
 *  注册/反注册
**/
#define OSEL_SUBSCRLIST_NUM             (OSEL_STOP_SIG + 1)

typedef struct _subscrlist_tag_
{
    osel_uint8_t bits[((OSEL_MAX_PRIO - 1) / 8) + 1];
} subscrlist;

extern subscrlist *osel_subscrlist;
extern osel_signal_t osel_maxsignal;

extern osel_uint8_t const CODE osel_map_tbl[64];


/**
 *  静态内存
**/
void osel_mem_free(void *mem);


/**
 *  消息 内存
**/
osel_bool_t osel_eblock_init(osel_uint32_t block_cnts);

event_block_t *osel_eblock_malloc(_EBLOCK_LINE1_);

void osel_eblock_free(event_block_t **eblock _EBLOCK_LINE2_);


/**
 *  缓冲消息内存
**/
osel_bool_t osel_epool_init(osel_uint32_t event_cnts);

event_pool_t *osel_epool_malloc(_EPOOL_LINE1_);

void osel_epool_free(event_pool_t **epool _EPOOL_LINE2_);


/**
 *  调度器
**/
osel_uint8_t osel_mutex_lock(osel_uint8_t prio_ceiling);

void osel_mutex_unlock(osel_uint8_t org_prio);

void osel_schedule(void);


/**
 *  注册/反注册
**/
void osel_sub_init(void);
#endif

