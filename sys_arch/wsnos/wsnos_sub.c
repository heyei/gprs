/***************************************************************************
* File        : wsnos_sub.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件是os的任务与消息关联部分
 *
 * file	: wsnos_sub.c
 *  
 * Date	: 2011--8--04
 *  
**/

#include <wsnos.h>

static subscrlist g_subscr_sto[OSEL_SUBSCRLIST_NUM];

subscrlist *osel_subscrlist;
osel_signal_t osel_maxsignal;


osel_uint8_t const CODE osel_map_tbl[64] =
{
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U,
    0x01U, 0x02U, 0x04U, 0x08U, 0x10U, 0x20U, 0x40U, 0x80U
};

osel_uint8_t const CODE osel_div8lkup[64]=
{
    0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U,
    1U, 1U, 1U, 1U, 1U, 1U, 1U, 1U,
    2U, 2U, 2U, 2U, 2U, 2U, 2U, 2U,
    3U, 3U, 3U, 3U, 3U, 3U, 3U, 3U,
    4U, 4U, 4U, 4U, 4U, 4U, 4U, 4U,
    5U, 5U, 5U, 5U, 5U, 5U, 5U, 5U,
    6U, 6U, 6U, 6U, 6U, 6U, 6U, 6U,
    7U, 7U, 7U, 7U, 7U, 7U, 7U, 7U

};


extern osel_task_tcb g_task_cb[OSEL_MAX_PRIO];

void osel_sub_init(void)
{
    osel_subscrlist = &g_subscr_sto[0];
    osel_maxsignal = OSEL_DIM(g_subscr_sto);
}

void osel_subscribe(osel_task_tcb const * const tcb, osel_signal_t sig)
{
    osel_uint8_t prio = tcb->task.prio;
    OSEL_ASSERT(prio < OSEL_MAX_PRIO);

    osel_uint8_t i = osel_div8lkup[prio];

    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);
    
    osel_subscrlist[sig].bits[i] |= osel_map_tbl[prio];
    OSEL_EXIT_CRITICAL(status);
}

#if OSEL_USE_MULTIPLES
void osel_unsubscribe(osel_task_tcb * const tcb, osel_signal_t sig)
{
    osel_uint8_t prio = tcb->task.prio;
    OSEL_ASSERT(prio < OSEL_MAX_PRIO);
    list_head_t *queue_list_head = NULL;
    event_pool_t *epool = OSEL_NULL;

    osel_uint8_t i = osel_div8lkup[prio];

    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);

    if ((osel_subscrlist[sig].bits[i] & osel_map_tbl[prio]) == osel_map_tbl[prio])
    {
        osel_subscrlist[sig].bits[i] &= ~osel_map_tbl[prio];
        queue_list_head = &(tcb->queue);
        while (queue_list_head->next != &(tcb->queue))
        {
            epool = list_entry_addr_find(queue_list_head->next, event_pool_t, list);
            if (epool->eblock->sig == sig)
            {
                list_del(&epool->list);

                if (--(epool->eblock->cnt) == 0)
                {
                    osel_eblock_free(&(epool->eblock) __EBLOCK_LINE2);
                }
                osel_epool_free(&epool __EPOOL_LINE2);
            }
            queue_list_head = queue_list_head->next;
        }
    }

    OSEL_EXIT_CRITICAL(status);
}
#else
void osel_unsubscribe(osel_task_tcb * const tcb, osel_signal_t sig)
{
    osel_uint8_t prio = tcb->task.prio;
    OSEL_ASSERT(prio < OSEL_MAX_PRIO);
    list_head_t *queue_list_head = NULL;
    event_block_t *eblock = OSEL_NULL;

    osel_uint8_t i = osel_div8lkup[prio];

    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);

    if ((osel_subscrlist[sig].bits[i] & osel_map_tbl[prio]) == osel_map_tbl[prio])
    {
        osel_subscrlist[sig].bits[i] &= ~osel_map_tbl[prio];
        queue_list_head = &(tcb->queue);
        while (queue_list_head->next != &(tcb->queue))
        {
            eblock = list_entry_addr_find(queue_list_head->next, event_block_t, list);
            if (eblock->sig == sig)
            {
                list_del(&(eblock->list));
                osel_eblock_free(&(eblock) __EBLOCK_LINE2);
            }
            queue_list_head = queue_list_head->next;
        }
    }

    OSEL_EXIT_CRITICAL(status);
}
#endif


/**@}**/
