/***************************************************************************
* File        : wsnos_sched.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件实现了消息发送以及任务调度
 *
 * file	: wsnos_sched.c
 *  
 * Date	: 2011--8--04
 *  
**/

#include <wsnos.h>

extern osel_task_tcb g_task_cb[OSEL_MAX_PRIO];

osel_uint8_t osel_rdy_grp;

osel_uint8_t osel_rdy_tbl[((OSEL_MAX_PRIO-1)/8) + 1];

static osel_int8_t osel_curr_prio = -1;

static osel_uint8_t const CODE osel_unmap_tbl[] =
{
    0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02,
    0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03, 0x03,
    0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
    0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
    0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07
};

//for debug
#if OSEL_EBLOCK_DBG_EN > 0
extern osel_uint8_t g_osel_eblock_cnt;   		
extern osel_uint8_t g_osel_epool_cnt;	
#include <debug.h>
#endif

#if OSEL_USE_MULTIPLES
static void osel_post_(event_pool_t *e, osel_uint8_t prio)
{
    osel_task_tcb *tcb = &g_task_cb[prio];
    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);

    osel_rdy_grp |= osel_map_tbl[prio >> 3];
    osel_rdy_tbl[prio >> 3] |= osel_map_tbl[prio & 0x07];

    if (e->eblock->event_prio == OSEL_EVENT_PRIO_HIG)
    {
        list_add_to_head(&(e->list), &tcb->queue);
        OSEL_EXIT_CRITICAL(status);
    }
    else
    {
        list_add_to_tail(&(e->list), &tcb->queue);
        OSEL_EXIT_CRITICAL(status);
    }

    OSEL_POST_EXIT();
}
#else
static void osel_post_(event_block_t *e, osel_uint8_t prio)
{
    osel_task_tcb *tcb = &g_task_cb[prio];
    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);

    osel_rdy_grp |= osel_map_tbl[prio >> 3];
    osel_rdy_tbl[prio >> 3] |= osel_map_tbl[prio & 0x07];
    
    if (e->event_prio == OSEL_EVENT_PRIO_HIG)
    {
        list_add_to_head(&(e->list), &tcb->queue);
        OSEL_EXIT_CRITICAL(status);
    }
    else
    {
        list_add_to_tail(&(e->list), &tcb->queue);
        OSEL_EXIT_CRITICAL(status);
    }

    OSEL_POST_EXIT();
}
#endif

static event_block_t *fill_eblock(osel_signal_t sig, osel_param_t param,
                                  osel_eblock_prio_t event_prio)
{
    event_block_t *eblock = OSEL_NULL;
    eblock = osel_eblock_malloc(__EBLOCK_LINE1);
    OSEL_ASSERT(eblock != OSEL_NULL);

    if (eblock != OSEL_NULL)
    {
        eblock->sig = sig;
        eblock->param = param;
        eblock->event_prio = event_prio;
        eblock->cnt = 0;
    }

    return eblock;
}



void osel_post(osel_signal_t sig, osel_param_t param, osel_eblock_prio_t event_prio)
{
#if OSEL_USE_MULTIPLES
    event_pool_t *epool = OSEL_NULL;
#endif
    event_block_t *eblock = OSEL_NULL;

    OSEL_ASSERT(sig <= OSEL_STOP_SIG);
    osel_int_status_t status = 0;   
    OSEL_ENTER_CRITICAL(status);
    
    osel_uint8_t first_malloc = OSEL_TRUE;
    osel_uint8_t i = OSEL_DIM(osel_subscrlist[sig].bits);
    OSEL_EXIT_CRITICAL(status);

    do
    {
        osel_int_status_t status = 0;
        OSEL_ENTER_CRITICAL(status);
        
        i--;
        osel_uint8_t temp = osel_subscrlist[sig].bits[i];
        while (temp != (osel_uint8_t)0)
        {
            osel_int_status_t status_two = 0;
            OSEL_ENTER_CRITICAL(status_two);
            
            osel_uint8_t prio = osel_unmap_tbl[temp];
            temp &= ~osel_map_tbl[prio];
            prio += (i << 3);

            if (first_malloc == OSEL_TRUE)
            {
                first_malloc = OSEL_FALSE;
                eblock = fill_eblock(sig, param, event_prio);
            }

            if (eblock != OSEL_NULL)
            {
                eblock->cnt ++;
            }

#if OSEL_USE_MULTIPLES
            epool = osel_epool_malloc(__EPOOL_LINE1);
            OSEL_ASSERT(epool != OSEL_NULL);
            epool->eblock = eblock;
#endif
            OSEL_EXIT_CRITICAL(status_two);
            OSEL_ASSERT(g_task_cb[prio].task.handler != OSEL_NULL);
            
#if OSEL_USE_MULTIPLES
            osel_post_(epool, prio);
#else
            osel_post_(eblock, prio);
#endif
        }

        OSEL_EXIT_CRITICAL(status);
    }
    while (i != (osel_uint8_t)0);
}

osel_uint8_t osel_mutex_lock(osel_uint8_t prio_ceiling)
{
    return prio_ceiling;
}

void osel_mutex_unlock(osel_uint8_t org_prio)
{
}

void osel_schedule(void)
{
    osel_uint8_t x=0,y=0;
    osel_uint8_t prio =0;
    osel_int8_t pin = osel_curr_prio;
    
#if OSEL_USE_MULTIPLES
    event_pool_t *epool = OSEL_NULL;
#else
    event_block_t *eblock = OSEL_NULL;
#endif
    
    while ((osel_rdy_grp > 0)
           && (y = osel_unmap_tbl[osel_rdy_grp]
				, x = osel_unmap_tbl[osel_rdy_tbl[y]]
				, (prio = (y << 3) + x) > pin))
    {
        osel_task_tcb * volatile tcb = &g_task_cb[prio];
//        osel_task_tcb * tcb = &g_task_cb[prio];

        osel_curr_prio = prio;

        while (list_empty(&(tcb->queue)) != OSEL_TRUE)
        {
#if OSEL_USE_MULTIPLES
            epool = list_entry_addr_find(tcb->queue.next, event_pool_t, list);
            
            OSEL_INT_UNLOCK();
            (*(tcb->task.handler))(epool->eblock);
            OSEL_INT_LOCK();
            
            list_del(&epool->list);
            
            if (--(epool->eblock->cnt) == 0)
            {
                osel_eblock_free(&(epool->eblock) __EBLOCK_LINE2);
            }
            osel_epool_free(&epool __EPOOL_LINE2);

#else
            eblock = list_entry_addr_find(tcb->queue.next, event_block_t, list);
            OSEL_INT_UNLOCK();
            (*(tcb->task.handler))(eblock);
            OSEL_INT_LOCK();
            
            list_del(&(eblock->list));
            osel_eblock_free(&(eblock) __EBLOCK_LINE2);
#endif
        }
        if ((osel_rdy_tbl[prio >> 3] &= ~osel_map_tbl[prio & 0x07]) == 0)
        {
            osel_rdy_grp &= ~osel_map_tbl[prio >> 3];
        }
    }

    osel_curr_prio = pin;
}

/**@}**/

