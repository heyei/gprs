/***************************************************************************
* File        : wsnos_eblock.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件是OS实际需要消息缓存池的实现
 *
 * file	: D:\Programs\MPDP_4TH\src\platform\5438_1100_iar_sst\osel\os\source\wsnos_eblock.c
 *
 * Date	: 2011--8--04
 *
**/
#include <wsnos.h>

static list_head_t eblock_list_head;

#if OSEL_EBLOCK_DBG_EN > 0
static osel_uint8_t g_osel_maxeblock_cnt = 0;
osel_uint8_t g_osel_eblock_cnt = 0;
static uint32_t g_eblock_free_cnt = 0;
static event_block_t *eblock_used_p[OSEL_EBLOCK_NUM];
#endif



osel_bool_t osel_eblock_init(osel_uint32_t block_cnts)
{
	event_block_t *eblock = OSEL_NULL;
	osel_uint8_t i =0;

    osel_memset(&eblock_list_head, 0, sizeof(eblock_list_head));
#if OSEL_EBLOCK_DBG_EN > 0
    g_osel_maxeblock_cnt = 0;
    g_osel_eblock_cnt = 0;
    g_eblock_free_cnt = 0;
    osel_memset(&eblock_used_p[0], 0, sizeof(eblock_used_p));
#endif
	list_init(&eblock_list_head);

	eblock = (event_block_t *)osel_mem_alloc(sizeof(event_block_t) * block_cnts);
	OSEL_ASSERT(eblock != OSEL_NULL);

	if(eblock == OSEL_NULL)
	{
		return OSEL_FALSE;
	}
	for(i = 0; i< block_cnts; i++)
	{
		list_add_to_tail(&(eblock + i)->list, &eblock_list_head);
	}

    return OSEL_TRUE;
}

event_block_t *osel_eblock_malloc(_EBLOCK_LINE1_)
{
    osel_int_status_t s = 0;
	event_block_t *eblock = OSEL_NULL;
    OSEL_ENTER_CRITICAL(s);
	eblock = list_entry_decap(&eblock_list_head, event_block_t, list);
    OSEL_EXIT_CRITICAL(s);

	if(eblock != OSEL_NULL)
	{
		osel_memset((void *)eblock, 0,sizeof(event_block_t));
		list_init(&(eblock->list));
		#if OSEL_EBLOCK_DBG_EN > 0
		eblock->alloc_line = line;
        eblock->free_line = 0;

        OSEL_ENTER_CRITICAL(s);
        for(uint8_t i = 0;i<OSEL_EBLOCK_NUM; i++)
        {
            if(eblock_used_p[i] == NULL)
            {
                eblock_used_p[i] = eblock;
                break;
            }
        }

		if(g_osel_maxeblock_cnt < ++g_osel_eblock_cnt)
		{
			g_osel_maxeblock_cnt = g_osel_eblock_cnt;
		}
        OSEL_EXIT_CRITICAL(s);
		#endif
	}

	return eblock;
}

void osel_eblock_free(event_block_t **eblock _EBLOCK_LINE2_)
{
	OSEL_ASSERT(*eblock != OSEL_NULL);
	if(*eblock == OSEL_NULL)
	{
		return;
	}

#if OSEL_EBLOCK_DBG_EN > 0
    g_osel_eblock_cnt--;
    osel_int_status_t s = 0;

    OSEL_ENTER_CRITICAL(s);
    for(uint8_t i = 0;i<OSEL_EBLOCK_NUM; i++)
    {
        if(eblock_used_p[i] == *eblock)
        {
            eblock_used_p[i] = NULL;
            break;
        }
    }
    OSEL_EXIT_CRITICAL(s);
#endif

	osel_memset((void *)(*eblock), 0, sizeof(event_block_t));

	#if OSEL_EBLOCK_DBG_EN > 0
    (*eblock)->alloc_line = 0;
	(*eblock)->free_line = line;
    g_eblock_free_cnt++;
	#endif

	list_add_to_tail(&((*eblock)->list), &eblock_list_head);

	*eblock = OSEL_NULL;
}



