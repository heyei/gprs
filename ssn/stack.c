/***************************************************************************
* File        : stack.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <lib.h>
#include <prim.h>
#include <pbuf.h>
#include <sbuf.h>

#include <stack.h>
#include <mac.h>
#include <nwk.h>
#include <app.h>

void ssn_init(void)
{
    pbuf_init();
    bool_t res = sbuf_init();
    DBG_ASSERT(res != FALSE __DBG_LINE);

//    mac_init();
//    nwk_init();
    app_init();
}




