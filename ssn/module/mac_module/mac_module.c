/***************************************************************************
* File        : mac_module.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	实现了消息与模块之间的绑定
 *
 * file	: mac_module.c
 *  
 * Date	: 2011--8--04
 *  
**/
#include <mac_module.h>

task_event_handler  mac_mode_array[MODULE_NUM_MAX];
module_bind_event_t msg_bind_mode_array[MSG_BIND_MODE_NUM];

static uint8_t mac_mode_cnt  = 0;
static uint16_t msg_mode_cnt = 0;

void module_bind_event(task_event_handler handler, osel_signal_t sig)
{
    DBG_ASSERT(sig > 0 __DBG_LINE);
    uint8_t i = 0;

    for (i=0; i<MODULE_NUM_MAX; i++)
    {
        if (handler == mac_mode_array[i])
        {
            break;
        }
    }
    if (i == MODULE_NUM_MAX)
    {
        DBG_ASSERT(mac_mode_cnt < MODULE_NUM_MAX __DBG_LINE);
        mac_mode_array[mac_mode_cnt++] = handler;
    }
    
    DBG_ASSERT(sig < MSG_BIND_MODE_NUM __DBG_LINE);
    if (sig != msg_bind_mode_array[sig].sig)
    {
        msg_bind_mode_array[sig].sig = sig;
        msg_bind_mode_array[sig].mode_index = mac_mode_cnt - 1;
        msg_mode_cnt++;
    }
    else
    {
        DBG_ASSERT(FALSE __DBG_LINE);
    }
}

void m_module_ctrl_handler(osel_event_t *pmsg)
{
    DBG_ASSERT(pmsg != NULL __DBG_LINE);
    DBG_ASSERT(pmsg->sig < MSG_BIND_MODE_NUM __DBG_LINE);

    if (msg_bind_mode_array[pmsg->sig].sig == pmsg->sig)
    {
        DBG_ASSERT(msg_bind_mode_array[pmsg->sig].mode_index < MODULE_NUM_MAX __DBG_LINE);
        (*(mac_mode_array[msg_bind_mode_array[pmsg->sig].mode_index]))(pmsg);
    }
}

