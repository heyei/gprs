/**
 * @brief       : 应答维护模块
 *
 * @file        : replay_module.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */

#pragma once
#include <data_type_def.h>
#include <global_variable.h>
typedef struct
{
    bool_t used;
    uint16_t nwk_addr;
    uint16_t seq;
    uint16_t code;
    uint32_t h_time; //时间戳
} replay_info_t;

bool_t replay_list_get_remove(uint16_t nwk_addr, replay_info_t *node);
bool_t replay_list_add(replay_info_t replay_info);
void replay_module_init(void);
