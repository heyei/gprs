/**
 * @brief       : 北向处理接口
 *
 * @file        : north_deal.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>
#include <frame_interface.h>

typedef enum
{
    //注册应答指示码
    REGISTER_SUCCESS = 0x0000,  //成功注册
    REGISTER_FAIL,  //注册不成功：参数错误
    REGISTER_ERROR_ID,  //注册不成功：ID不合法
    REGISTER_TIMEOUT,  //注册不成功：注册超时
} register_reply_e;

typedef enum
{
    //配置指令应答码
    C_SUCCESS = 0x0000,  //配置成功
    C_DEVICE_NOFIND,  //设备不存在
    C_VERSION_ERR,  //版本号错误
    C_UNSUCCESS,  //不支持操作
    C_SEND,  //已下发给设备
} config_reply_e;

/**
 * 北向处理接口
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param north_in_data 
 */
void north_deal_interface(north_instruction_data_t *north_in_data);
