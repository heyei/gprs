/**
 * 南向解析
 *
 * @file south_parse.h
 * @author xushenghao
 * @data 2014-2-28 17:02:58
 *
 */

#pragma once
#include <data_type_def.h>
#include <frame_interface.h>
#pragma pack(1)
typedef struct
{
    uint8_t config_name;
    uint8_t len;
    uint32_t value;
} config_list_t;
typedef struct
{
    uint8_t num;
    config_list_t list;
} app_south_config_t;
#pragma pack()

/**
 * 南向数据解析
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param frame 
 * @param size 
 * @param instruction 
 * 
 * @return bool_t 
 */
bool_t south_parse_instruction_frame(const uint8_t *frame, uint8_t size,
    south_instruction_data_t *instruction);
