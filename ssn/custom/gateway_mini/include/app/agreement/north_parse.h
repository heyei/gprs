/**
 * 北向解析
 *
 * @file north_parse.h
 * @author xushenghao
 * @data 2014-2-28 17:00:56
 *
 */
#pragma once
#include <data_type_def.h>
#include <frame_interface.h>


/**
 * 北向数据解析
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param frame 
 * @param size 
 * @param data 
 * 
 * @return bool_t 
 */
bool_t north_parse_instruction_frame(const uint8_t *frame, uint8_t size,
    north_instruction_data_t *data);
