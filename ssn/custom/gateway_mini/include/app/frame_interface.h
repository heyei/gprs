
/**
 * @brief       : 南北向数据接口
 *
 * @file        : frame_interface.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */

#pragma once
#include <data_type_def.h>
#include <pbuf.h>
#include <sbuf.h>
#include <nwk_global_agreement.h>

//北向
#pragma pack(1)
typedef struct
{
    //心跳控制域
    uint8_t energy_support: 2,  //能量支持
    topology: 1,  //拓扑支持
    link_quality: 1,  //链路质量
    device_type: 2,  //节点类型
    successful_rate: 1; //传输成功率
} ctrl_domain_t;

typedef struct
{
    //心跳指令结构体
    ctrl_domain_t ctrl_domain; //控制域
    uint8_t energy_remain; //能量剩余
    uint8_t superior_id[8]; //上级设备ID
    int8_t uplink_quality; //上行链路质量
    int8_t downlink_quality; //下行链路质量
    uint8_t successful_rate; //传输成功率
} heartbeat_command_t;

typedef struct
{
    uint16_t sensor_type;
    uint32_t time;
} data_report_cycle_t;

typedef struct
{
    uint16_t code; //配置码
    union
    {
        uint32_t heart_cycle;
        data_report_cycle_t data_cycle;
    };
}

config_t;

typedef struct
{
    uint8_t type;
    uint16_t seq;
    uint16_t code; //配置码
    uint16_t result; //指示码
} replay_pload_t;

typedef struct
{
    //北向查询指令结构体
    uint16_t select_range: 1,  //查询范围
    select_type: 1,  //查询类型
    reserve: 9,  //保留
    relevance: 1,  //关联
    link_quality: 1,  //链路质量
    device_electricity: 1,  //设备电量
    data_cycle: 1,  //数据周期
    heartbeat_cycle: 1; //心跳周期
} select_command_t;

typedef struct
{
    select_command_t code;
    uint8_t device_associate[8]; //设备关联
    uint8_t link_quality; //链路质量
    uint8_t device_electricity; //设备电量
    uint32_t data_cycle; //数据周期
    uint32_t heartbeat_cycle; //心跳周期
} select_state_reply_t;

typedef struct
{
    // 通用指令结构的内容
    uint8_t version; //版本号
    uint8_t type; //类型
    uint16_t length; //报文长度
    uint16_t seq; //序列号
    uint8_t dest_id[8]; //目的ID
    uint8_t src_id[8]; //源ID
    union
    {
        config_t config;
        select_command_t select_command;

        replay_pload_t replay;
        uint8_t *pload;
    }; //指令内容
}
north_instruction_data_t;
#pragma pack()

//南向
typedef struct
{
    //app帧格式头
    uint16_t frame_type: 3, ack_request: 1, data_type: 4, seq_num: 8;
} app_head_t;

typedef struct
{
    pbuf_t *pbuf;
} user_data_t;

typedef struct
{
    uint16_t line;
    fp32_t join_delay;
} alarm_rest_t;

typedef struct
{
    fp32_t join_delay;
} alarm_rjoin_t;

typedef struct
{
    fp32_t time; //时间差
    uint16_t tm; //温度
    uint16_t rh; //湿度
} temperature_hunidity_t;

typedef struct
{
    fp32_t time; //时间差
    uint16_t x; //x
    uint16_t y; //y
    uint16_t z; //z
} acceler_t;

typedef struct
{
    uint8_t type;
    union
    {
        user_data_t user_data;
        alarm_rest_t alarm_rest;
        alarm_rjoin_t alarm_rjoin;
        temperature_hunidity_t temperature_hunidity;
        acceler_t acceler;
    };
}

ssn_app_data_t;

typedef struct
{
    uint16_t seq;
    uint16_t code;
    uint16_t result;
} config_request_t;

typedef struct
{
    uint16_t seq;
    uint32_t heartbeat_cycle;
    uint8_t electricity;
} select_request_t;

typedef struct
{
    uint8_t ssn_type;
    union
    {
        ssn_app_data_t app_data;
        heart_frame_t heart_frame;
        config_request_t config_request;
        select_request_t select_request;
    };
}

ssn_data_t;

typedef struct
{
    uint16_t nwk_id;
    uint64_t id;
    uint8_t nwk_type;
    ssn_data_t ssn_data;
} south_instruction_data_t;

/**
 * 南向解析接口
 *
 * @author xushenghao (2014-2-28)
 *
 * @param sbuf
 */
void south_parse_interface(sbuf_t *sbuf);
/**
 * 北向解析接口
 *
 * @author xushenghao (2014-2-28)
 *
 * @param pbuf
 */
void north_parse_interface(pbuf_t *pbuf);
