/**
 * @brief       : 南向处理接口
 *
 * @file        : south_deal.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>
#include <frame_interface.h>

/**
 * 南向处理接口
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param instruction 
 */
void south_deal_interface(south_instruction_data_t *instruction);
