/**
 * @brief       : 
 *
 * @file        : nwk_global_agreement.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>

#define NWK_HEAD_CTRL_SIZE	(1u)		//网络头控制域长度
#define NWK_ADDR_SHORT_SIZE	(2u)		//网络短地址长度
#define NWK_ADDR_LONG_SIZE	(8u)		//网络长地址长度

enum nwk_frame_type
{//nwk帧类型
	NWK_DATA = 0x00,	//数据帧
	NWK_JOIN,			//入网请求帧
	NWK_REPLY,			//入网应答帧
	NWK_HEART,			//心跳帧
};

enum nwk_addr_mode
{//nwk地址模式
	NWK_ADDR_NO_EXIST = 0,		//地址域不存在
	NWK_ADDR_RESERVED = 1,		//预留
	NWK_ADDR_16BITS	=2,			//16bits短地址
	NWK_ADDR_64BITS	=3,			//64bits长地址
};

enum nwk_heart_device_type
{
	TERMINAL = 0,				//终端
	RELAY,						//中继
	SINK,						//网关
};

enum alarm_info_state
{
	NON_ALARM_INFO = 0,			//没有报警信息
	DEVICE_REST,				//设备复位
	DEVICE_REJOIN,				//设备重入网
};

enum energy_support_e
{
	E_UNSUPPORT = 0,		//不支持能量上报
	E_SUPPORT_WITHOUT,		//支持，有源供电
	E_SUPPORT_SELF,			//支持，电池供电
};

#pragma pack(1)
typedef struct
{
	bool_t htb_alarm_info;		//mark a mark after send heartbeat alarm info 
}nwk_info_t;

typedef struct
{//nwk head
	uint8_t frame_type	:3,			//帧类型
			dest_addr_mode	:2,		//目的地址模式
			source_addr_mode	:2,	//源地址模式
			reserved	:1;
}nwk_nhr_ctrl_t;

typedef struct
{//nwk帧头
	uint8_t frame_type	:3,			//帧类型
			dest_addr_mode	:2,		//目的地址模式
			source_addr_mode	:2,	//源地址模式
			reserved	:1;
	uint64_t dest_addr;
	uint64_t source_addr;
}nwk_nhr_t;

typedef struct
{//入网请求载荷
	uint8_t device_type	:1,			//设备类型
			reserved	:7;
	uint32_t	join_time;			//发送入网请求的时间戳
}join_payload_t;

typedef struct
{
	nwk_nhr_t		head;				//nwk头
	union
	{//载荷
		join_payload_t join_payload;	//入网请求载荷
	};
}nwk_frame_t;

typedef struct
{
	uint8_t device_type    :2,			//设备类型
	energy_report_support   :2,			//能量上报支持状态
	alarm_type  :2,						//报警信息状态
	localization_support    :1,			//定位支持状态
    overhear_mode_support	:1;			//混杂侦听模式
}frame_ctrl_t;

typedef struct
{
	uint16_t    source_addr;	//统计lqi对应帧的发送源地址
	uint8_t     lqi;			//链路质量
	uint32_t    timestamp;		//更新时间戳
}localization_variable_t;

typedef struct
{
	uint8_t     tuple_num;		//定位辅助信息元组个数
	localization_variable_t variable[5];	//定位元素信息
}localization_info_t;

typedef struct
{
	uint16_t    dest_addr;		//统计lqi对应帧的接收目的地址
	uint16_t    source_addr;	//统计lqi对应帧的发送源地址
	uint8_t     lqi;			//链路质量
	uint32_t    timestamp;		//更新时间戳
}neighbor_variable_t;

typedef struct
{
	uint8_t     tuple_num;		//定位辅助信息元组个数
	neighbor_variable_t neighbor_variable;	//邻居信息
}neighbor_info_t;

typedef struct
{
	frame_ctrl_t        frame_ctrl;
	uint16_t            farther_id;						//父设备ID
	uint8_t				successful_rate;				//平均传输成功率
	uint8_t             residual_energy;				//剩余能量
	uint16_t            alarm_info;						//报警信息
	localization_info_t localization_info;				//定位辅助信息
	neighbor_info_t     neighbor_info;					//邻居设备信息
}heart_frame_t;

#pragma pack()

extern nwk_info_t nwk_info;
