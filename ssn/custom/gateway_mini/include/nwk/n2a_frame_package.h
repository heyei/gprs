/**
 * @brief       : 网络层到app层接口
 *
 * @file        : n2a_frame_package.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>

void n2a_frame_heartbeat_package(void);
