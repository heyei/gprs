/**
 * @brief       : 路由表
 *
 * @file        : route_table.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>
#include <nwk_global_agreement.h>
#define RTB_DEEPNESS		(128u)

typedef struct
{
	uint8_t isused  :1,		//是否被使用
	life    :3,				//生存周期
	if_register :1,			//是否注册
	mode	:1,				//连接模式(TRUE:直连;FALSE:通过中继)
	device_type :1;			//设备类型
}rtb_info_t;

typedef struct
{//路由表成员结构体
	uint16_t next_mac_addr;				//下一跳的短地址
	uint16_t self_nwk_addr;				//自己的网络短地址
	uint64_t self_long_addr;			//自己的长地址
	fp32_t join_time;					//入网时间差
	rtb_info_t info;

	int8_t uplinkq;						//上行链路质量
	int8_t downlinkq;					//下行链路质量
	uint32_t timestamp;					//更新时间戳
}rtb_t;


bool_t device_if_register(int index);
bool_t device_if_exist(int index);
uint64_t device_long_addr(int index);
uint16_t convert_mac_short_addr(uint64_t mac_addr);


void rtb_device_register(uint64_t long_id);
void rtb_device_unregister();
int rtb_find_index(uint64_t long_addr);
int rtb_find_index_through_nwk_addr(uint16_t nwk_addr);
int rtb_find_index_through_mac_addr(uint16_t mac_addr);
rtb_t *rtb_find_info(int index);
int8_t rtb_get_uprssi(int index);
int8_t rtb_get_downrssi(int index);
void update_node_cycle(int index);
bool_t rtb_empty();
bool_t rtb_find_long_addr(uint16_t nwk_addr, uint64_t *long_addr);
bool_t rtb_find_long_addr_through_mac_short_addr(uint16_t mac_short_addr,uint64_t *long_addr);
bool_t rtb_add(nwk_frame_t *nwk_frame_t,uint16_t mac_next_addr,bool_t mode);
void rtb_timer_start(void);
void rtb_init();
