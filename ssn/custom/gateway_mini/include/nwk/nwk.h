/**
 * @brief       : 网络层接口
 *
 * @file        : nwk.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once

void heartbeat_reset();
void nwk_init(void);
