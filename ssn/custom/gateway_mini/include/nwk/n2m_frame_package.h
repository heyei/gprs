/**
 * @brief       : 网络层到mac层接口
 *
 * @file        : n2m_frame_package.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <data_type_def.h>
#include <nwk_global_agreement.h>

void n2m_frame_data_package(sbuf_t *sbuf);
void n2m_frame_join_reply_package(uint64_t long_addr);
