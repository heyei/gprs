/**
 * @brief       : mac到nwk帧解析
 *
 * @file        : m2n_frame_parse.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */

#pragma once
#include <data_type_def.h>

void m2n_frame_parse(sbuf_t *sbuf);
