#ifndef __PHY_PACKET_H
#define __PHY_PACKET_H

#include <mac_module.h>

#define PHY_HEAD_SIZE       3u  //物理帧帧头长度
#define PHY_LEN_FEILD_SIZE  1u  //物理帧长度域帧头长度
#define PHY_FCS_SIZE        0u

typedef uint8_t             phy_addr_t;

/**
 * 将芯片缓冲中数据拷贝到协议数据包缓冲单元
 *
 * @return 已填入数据的数据包缓冲单元
 */
pbuf_t *phy_get_packet(void);


/**
 * 向物理芯片写入数据
 *
 * @param buf:  待写入的数据起始地址
 * @param ptr:  待写入的数据的长度
 *
 * @return  ture or false
 */
bool_t phy_write_buf(pbuf_t *pbuf, uint8_t stamp_size);


/**
 * 操作RF发送芯片缓冲中的数据
 *
 * @return  void
 */
bool_t phy_send_data(void);

#endif
