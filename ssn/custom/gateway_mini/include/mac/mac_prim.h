#pragma once
#include <sbuf.h>
/**
 * 把M_MAC_PRIM_EVENT消息跟事件绑定
 *
 * @param  空
 *
 * @return 空
 */
void m_prim_init(void);

/**
 * 处理网络层发送过来的消息
 *
 * @param  pmsg 消息参数
 *
 * @return 空
 */
void mac_prim_event_handler(const event_block_t *const pmsg);

/**
 * 把数据帧通过消息发送给网络层
 *
 * @param  sbuf 数据内容
 *
 * @return 空
 */
void mac_prim_data_indicate(sbuf_t *sbuf);

/**
 * 数据重传
 *
 * @param  sbuf 数据内容
 *
 * @return 空
 */
void mac_data_resend(sbuf_t *sbuf);