#pragma once
#include <lib.h>
#include <mac_global_variables.h>

#if SYSTEM_PLAN_NUM == 1    //方案一
#define BEACON_INTERVAL_ORDER           (11u)
#define BEACON_DURATION_ORDER           (5u)
#define GTS_DURATION_ORDER              (6u)
#define INTRA_GTS_NUM                   (30u)
#define CLUSTER_FRAME_NUM               (9u)
#elif SYSTEM_PLAN_NUM == 2    //方案二
#define BEACON_INTERVAL_ORDER           (13u)
#define BEACON_DURATION_ORDER           (5u)
#define GTS_DURATION_ORDER              (6u)
#define INTRA_GTS_NUM                   (64u)
#define CLUSTER_FRAME_NUM               (5u)
#elif SYSTEM_PLAN_NUM == 3    //方案三
#define BEACON_INTERVAL_ORDER           (13u)
#define BEACON_DURATION_ORDER           (5u)
#define GTS_DURATION_ORDER              (6u)
#define INTRA_GTS_NUM                   (10u)
#define CLUSTER_FRAME_NUM               (11u)
#endif

#define MAC_RESEND_NUM                  (3u)
#define GW_SO_NUM                       (1u)


typedef struct
{
	uint8_t mac_beacon_seq_num;
    uint8_t super_frame_index;
    uint8_t intercom_index;
    uint8_t intra_gts_index;
    uint8_t inter_gts_index;
	uint64_t mac_coord_long_addr;  // coord IEEE address
    uint16_t mac_coord_short_addr;
	uint64_t mac_long_addr;        // self IEEE address
    uint16_t mac_short_addr;
    supf_spec_t supf_cfg_arg;
    uint8_t mac_max_resend_cnt;
    bool_t mac_over_hear_flag;
} mac_pib_t;

extern mac_pib_t mac_pib;

void pib_table_init(void);

void operation_pib_beacon_seq_num(int8_t para);

void write_pib_short_addr(uint16_t para);

void write_pib_long_addr(uint64_t para);

void write_pib_super_frame_index(uint8_t para);

void write_pib_intra_gts_index(uint8_t para);

void write_pib_intercom_index(uint8_t para);

uint8_t get_pib_beacon_seq_num(void);

uint8_t read_pib_super_frame_index(void);

uint8_t read_pib_intra_gts_index(void);

uint8_t read_pib_resend_max_num(void);

uint8_t read_pib_intercom_index(void);

uint16_t read_pib_short_addr(void);

uint64_t read_pib_long_addr(void);

supf_spec_t read_pib_supf_cfg_arg(void);

bool_t read_pib_mac_over_hear_flag(void);