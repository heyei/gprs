#pragma once
#include <lib.h>
#include <pbuf.h>

#pragma pack(1)


#pragma pack()

/**
 * 生成一个不释放用来循环发beacon帧的pbuf并初始化信标有效期数组。
 *
 * @param  空
 *
 * @return 空
 */
void mac_beacon_init(void);

/**
 * 组装beacon帧并调用传输模块发送。
 *
 * @param  空
 *
 * @return 空
 */
void mac_send_beacon(void);

/**
 * 解析beacon帧
 *
 * @param  *pbuf 指针指向内容为收到的beacon帧
 *
 * @return 空
 */
void recv_beacon(pbuf_t *pbuf);