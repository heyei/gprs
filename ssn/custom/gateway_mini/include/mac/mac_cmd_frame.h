#pragma once
#include <pbuf.h>

#define MAC_CMD_ASSOC_REQ               1U
#define MAC_CMD_ASSOC_RESP              2U
#define MAC_CMD_RESERVED                4U

#define DEVICE_TYPE_FLAG                0x0001U
#define SECURITY_CAP_FLAG               0x0004U
#define TIMEB_BITMAP_FLAG               0xFFF8U

/**
 * MAC ASSOCIATE RESULTs
 */
#define ASSOC_STATUS_SUCCESS            0U      //�����ɹ�
#define ASSOC_STATUS_FULL               1U      //������������
#define ASSOC_STATUS_REFUSE             2U      //�ܾ�����
#define ASSOC_STATUS_RESERVED           3U      //Ԥ��

typedef struct
{
	uint16_t
            type		:   1,
            reserved    :   1,    
            sec_cap	    :   1,
            bitmap		:   13;
    uint32_t assoc_apply_time;
} mac_assoc_req_arg_t;

typedef struct
{
	uint8_t  associat_status     :   4,
			 intra_cluster_index :   4;
} mac_assoc_res_arg_t;

#pragma pack()
/**
 * ��������֡
 *
 * @param  *pbuf ֡�غ�
 *
 * @return ��
 */
void mac_cmd_frame_parse(pbuf_t *pbuf);

void bcn_aging_update(void);