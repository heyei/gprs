/**
 * mac到nwk帧解析
 */
#include <pbuf.h>
#include <sbuf.h>
#include <mac_pib.h>
#include <assoc_table.h>
#include <m2n_frame_parse.h>
#include <n2m_frame_package.h>
#include <nwk_global_agreement.h>
#include <route_table.h>


static bool_t head_data_deal(sbuf_t *sbuf, nwk_frame_t *nwk_frame)
{
	m2n_data_indication_t *m2n_data = &sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg;
	if(nwk_frame->head.dest_addr_mode == NWK_ADDR_16BITS && nwk_frame->head.source_addr_mode == NWK_ADDR_16BITS)
	{
		uint8_t *p = m2n_data->msdu;
		p += NWK_HEAD_CTRL_SIZE;		//将指针偏移到nwk目标地址
		osel_memcpy(&nwk_frame->head.dest_addr, p, NWK_ADDR_SHORT_SIZE);
		p += NWK_ADDR_SHORT_SIZE;		//将指针偏移到nwk源地址
		osel_memcpy(&nwk_frame->head.source_addr, p, NWK_ADDR_SHORT_SIZE);
		p += NWK_ADDR_SHORT_SIZE;		//将指针偏移到载荷
		uint8_t payload_len = m2n_data->msdu_length - NWK_HEAD_CTRL_SIZE - NWK_ADDR_SHORT_SIZE*2;

		//组装往app发的数据
		n2a_data_indication_t *n2a_data = &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg;
		n2a_data->rssi = sbuf->primargs.pbuf->attri.rssi_dbm;
		n2a_data->src_addr = nwk_frame->head.source_addr;
		n2a_data->dst_addr = nwk_frame->head.dest_addr;
		n2a_data->nsdu_length = payload_len;
		n2a_data->nsdu = p;
		sbuf->primtype = N2A_DATA_INDICATION;

		//更新最后一跳设备的上行rssi
		int index = rtb_find_index_through_nwk_addr(n2a_data->src_addr);
		if(index != -1)
		{
			rtb_t *rtb = rtb_find_info(index);
			rtb->uplinkq = sbuf->primargs.pbuf->attri.rssi_dbm;

			osel_post(NWK2APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);

            //更新拓扑
            index = rtb_find_index_through_mac_addr(m2n_data->src_addr);
            if(index != -1)
            {
                update_node_cycle(index);
            }
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}

static bool_t head_heartbeat_deal(sbuf_t *sbuf, nwk_frame_t *nwk_frame)
{
	m2n_data_indication_t *m2n_data = &sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg;
	if(nwk_frame->head.dest_addr_mode == NWK_ADDR_16BITS && nwk_frame->head.source_addr_mode == NWK_ADDR_16BITS)
	{
		uint8_t *p = m2n_data->msdu;
		p += NWK_HEAD_CTRL_SIZE;		//将指针偏移到nwk目标地址
		osel_memcpy(&nwk_frame->head.dest_addr, p, NWK_ADDR_SHORT_SIZE);
		if(NWK_ADDR != (uint16_t)nwk_frame->head.dest_addr)
		{
			return FALSE;
		}
		p += NWK_ADDR_SHORT_SIZE;		//将指针偏移到nwk源地址
		osel_memcpy(&nwk_frame->head.source_addr, p, NWK_ADDR_SHORT_SIZE);
		p += NWK_ADDR_SHORT_SIZE;		//将指针偏移到载荷
		uint8_t payload_len = m2n_data->msdu_length - NWK_HEAD_CTRL_SIZE - NWK_ADDR_SHORT_SIZE*2;

		//组装往app发的数据
		n2a_data_indication_t *n2a_data = &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg;
		n2a_data->rssi = sbuf->primargs.pbuf->attri.rssi_dbm;

		n2a_data->src_addr = nwk_frame->head.source_addr;
		n2a_data->dst_addr = nwk_frame->head.dest_addr;
		n2a_data->nsdu_length = payload_len;
		n2a_data->nsdu = p;
		sbuf->primtype = N2A_HEARTBEAT;

        int index = rtb_find_index_through_nwk_addr(n2a_data->src_addr);
		if(index != -1)
		{
			rtb_t *rtb = rtb_find_info(index);
            //更新最后一跳设备的上行rssi
			rtb->uplinkq = sbuf->primargs.pbuf->attri.rssi_dbm;
            //更新route_table信息
            update_node_cycle(index);
			osel_post(NWK2APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}

static bool_t head_join_deal(sbuf_t *sbuf, nwk_frame_t *nwk_frame)
{
	m2n_data_indication_t *m2n_data = &sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg;
	if(nwk_frame->head.dest_addr_mode != NWK_ADDR_16BITS)
	{
		return FALSE;
	}
	if(m2n_data->src_mode == NWK_ADDR_64BITS || m2n_data->src_mode == NWK_ADDR_16BITS)
	{
		uint16_t mac_next_addr = 0;		//下一跳的mac地址
		bool_t mode = FALSE;
		uint8_t *p = m2n_data->msdu;
		p += NWK_HEAD_CTRL_SIZE + NWK_ADDR_SHORT_SIZE;		//将指针偏移到nwk源地址
		if(m2n_data->src_mode == NWK_ADDR_64BITS)
		{//直连
			osel_memcpy(&nwk_frame->head.source_addr, p, NWK_ADDR_LONG_SIZE);
			p += NWK_ADDR_LONG_SIZE;			//将指针偏移到载荷
			mac_next_addr = convert_mac_short_addr(nwk_frame->head.source_addr);
			mode = TRUE;
		}
		else
		{//中继转发
			osel_memcpy(&nwk_frame->head.source_addr, p, NWK_ADDR_LONG_SIZE);
			p += NWK_ADDR_LONG_SIZE;			//将指针偏移到载荷
			mac_next_addr = m2n_data->src_addr;
			mode = FALSE;
            //更新拓扑
            int8_t index = rtb_find_index_through_mac_addr(m2n_data->src_addr);
            if(index != -1)
            {
                update_node_cycle(index);
            }
		}

		osel_memcpy(&nwk_frame->join_payload, p, sizeof(join_payload_t));

		//1.加入路由表
		if(!rtb_add(nwk_frame,mac_next_addr,mode))
		{
			PRINTF(("[head_join_deal]加入路由表失败\r\n"));
			return FALSE;
		}
		else
		{
			uint16_t mac_short_addr = convert_mac_short_addr(nwk_frame->head.source_addr);
			assoc_table_device_updata_addr(nwk_frame->head.source_addr,mac_short_addr);
            //2.发送入网请求应答
			n2m_frame_join_reply_package(nwk_frame->head.source_addr);
			//3.向app通知有设备注册
            osel_post(APP_GW_REGISTER, NULL, OSEL_EVENT_PRIO_LOW);
			return TRUE;
		}
	}
	else
	{

	}
	return FALSE;
}

static bool_t nwk_head_parse(sbuf_t *sbuf)
{//帧头解析
	bool_t result = FALSE;
	nwk_frame_t nwk_frame;
	m2n_data_indication_t *m2n_data = &sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg;
	osel_memcpy(&nwk_frame.head, (uint8_t *)m2n_data->msdu, NWK_HEAD_CTRL_SIZE);
	switch(nwk_frame.head.frame_type)
	{
		case NWK_DATA:
			{
				result = head_data_deal(sbuf,&nwk_frame);
				if(result)
				{//数据已经发送到app,这里sbuf不释放
					return TRUE;
				}
			}
			break;
		case NWK_JOIN:
			result = head_join_deal(sbuf,&nwk_frame);
			break;
		case NWK_HEART:
			{//下面设备的心跳
				result = head_heartbeat_deal(sbuf,&nwk_frame);
				if(result)
				{//数据已经发送到app,这里sbuf不释放
					return TRUE;
				}
			}
			break;
		default:

			break;
	}
	pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
	sbuf_free(&sbuf __SLINE2);
	return result;
}

void m2n_frame_parse(sbuf_t *sbuf)
{//帧解析
	DBG_ASSERT(NULL != sbuf __DBG_LINE);
	nwk_head_parse(sbuf);
}
