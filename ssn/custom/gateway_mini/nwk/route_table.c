#include <osel_arch.h>
#include <route_table.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <assoc_table.h>
#include <mac_schedule.h>
#define RTB_LIFE_CYCLE		(7u)

static rtb_t rtb[RTB_DEEPNESS];			//路由表数组
static hal_timer_t *rtb_timer =NULL;	//路由表维护定时器
static volatile bool_t lock = FALSE;	//查询路由表的锁

static bool_t state_lock()
{
	return lock;
}

static void open_lock()
{
	uint32_t i = 0;
	while (!state_lock())
	{
		if (i++ >= 700000)
		{
			DBG_ASSERT(FALSE __DBG_LINE);
		}
	}
	lock = FALSE;
}

static void close_lock()
{
	lock = TRUE;
}

static void rtb_alone_clear(int index)
{
	rtb[index].info.life = 0;
	rtb[index].info.device_type = 0;
	rtb[index].info.if_register = FALSE;
	rtb[index].next_mac_addr = 0;
	rtb[index].self_nwk_addr = 0;
	rtb[index].self_long_addr = 0;
	rtb[index].join_time = 0;
	rtb[index].info.isused = TRUE;
}

static void rtb_timer_cb(void *p)
{
	rtb_timer = NULL;
	osel_post(NWK_ROUTE_TABLE_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void rtb_timer_start(void)
{
	if (rtb_timer == NULL)
	{
        device_info_t device_info = hal_board_info_look();
        uint32_t time = device_info.heartbeat_cycle*1000;
		HAL_TIMER_SET_REL(MS_TO_TICK(time),
						  rtb_timer_cb,
						  NULL,
						  rtb_timer);
		DBG_ASSERT(NULL != rtb_timer __DBG_LINE);
	}

	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].info.life!=0)
			{
				rtb[i].info.life--;
			}
			else
			{
				rtb_alone_clear(i);
				if (rtb[i].info.mode == TRUE)
				{
					assoc_table_device_del(rtb[i].self_long_addr);
				}
			}
		}
	}
}

bool_t device_if_register(int index)
{
	return rtb[index].info.if_register;
}

bool_t device_if_exist(int index)
{
	return rtb[index].info.isused;
}

uint64_t device_long_addr(int index)
{
	return rtb[index].self_long_addr;
}

int rtb_find_index_through_nwk_addr(uint16_t nwk_addr)
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].self_nwk_addr == nwk_addr)
			{
				return i;
			}
		}
	}
	return -1;
}

int rtb_find_index_through_mac_addr(uint16_t mac_addr)
{
    for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
            uint16_t temp = convert_mac_short_addr(rtb[i].self_long_addr);
			if (temp == mac_addr)
			{
				return i;
			}
		}
	}
	return -1;
}

int rtb_find_index(uint64_t long_addr)
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].self_long_addr == long_addr)
			{
				return i;
			}
		}
	}
	return -1;
}

rtb_t *rtb_find_info(int index)
{
	return &rtb[index];
}

bool_t rtb_empty()
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == TRUE)
		{
			return TRUE;
		}
	}
	return FALSE;
}

void rtb_device_register(uint64_t long_id)
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].self_long_addr == long_id)
			{
				rtb[i].info.if_register = TRUE;
				return;
			}
		}
	}
}

void rtb_device_unregister()
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		rtb[i].info.if_register = FALSE;
	}
}

int8_t rtb_get_uprssi(int index)
{
	if (index == -1)
	{
		return 0;
	}
	return rtb[index].uplinkq;
}

int8_t rtb_get_downrssi(int index)
{
	if (index == -1)
	{
		return 0;
	}
	return rtb[index].downlinkq;
}

uint16_t convert_mac_short_addr(uint64_t mac_addr)
{
	return hal_board_get_device_short_addr(((uint8_t *)&mac_addr));
}

uint16_t convert_nwk_short_addr(uint64_t nwk_addr)
{
	return hal_board_get_device_short_addr(((uint8_t *)&nwk_addr));
}

static void update_node(int index,nwk_frame_t *nwk_frame_t,uint16_t mac_next_addr,bool_t mode)
{
	rtb[index].info.isused = FALSE;
	rtb[index].info.life = RTB_LIFE_CYCLE;
	rtb[index].info.mode = mode;
	rtb[index].info.if_register = FALSE;
	rtb[index].info.device_type = nwk_frame_t->join_payload.device_type;
	rtb[index].next_mac_addr = mac_next_addr;
	rtb[index].self_nwk_addr = convert_nwk_short_addr(nwk_frame_t->head.source_addr);
	rtb[index].self_long_addr = nwk_frame_t->head.source_addr;
	hal_time_t now = hal_timer_now();
	rtb[index].join_time = TICK_TO_US(now.w - nwk_frame_t->join_payload.join_time) / 1000.0;
}

void update_node_cycle(int index)
{
	rtb[index].info.life = RTB_LIFE_CYCLE;
}

bool_t rtb_find_long_addr_through_mac_short_addr(uint16_t mac_short_addr,uint64_t *long_addr)
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			uint16_t addr = convert_mac_short_addr(rtb[i].self_long_addr);
			if (addr == mac_short_addr)
			{
				*long_addr = rtb[i].self_long_addr;
				return TRUE;
			}
		}
	}
	return FALSE;
}

bool_t rtb_find_long_addr(uint16_t nwk_addr, uint64_t *long_addr)
{//通过短地址查长地址
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].self_nwk_addr == nwk_addr)
			{
				*long_addr = rtb[i].self_long_addr;
				return TRUE;
			}
		}
	}
	return FALSE;
}

bool_t rtb_add(nwk_frame_t *nwk_frame_t,uint16_t mac_next_addr,bool_t mode)
{
	open_lock();
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == FALSE)
		{
			if (rtb[i].self_long_addr == nwk_frame_t->head.source_addr)
			{
				update_node(i,nwk_frame_t,mac_next_addr,mode);
				close_lock();
				return TRUE;
			}
		}
	}
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		if (rtb[i].info.isused == TRUE)
		{
			update_node(i,nwk_frame_t,mac_next_addr,mode);
			close_lock();
			return TRUE;
		}
	}
	close_lock();
	return FALSE;
}

void rtb_init()
{
	for (int i=0; i<RTB_DEEPNESS; i++)
	{
		rtb_alone_clear(i);
	}
	lock = TRUE;
	//启动路由表维护定时器
	rtb_timer = NULL;
	rtb_timer_start();
}
