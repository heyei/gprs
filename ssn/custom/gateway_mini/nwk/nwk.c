#include <osel_arch.h>
#include <pbuf.h>
#include <sbuf.h>
#include <mac_pib.h>
#include <m2n_frame_parse.h>
#include <n2m_frame_package.h>
#include <n2a_frame_package.h>
#include <nwk_global_agreement.h>
#include <route_table.h>
#include <hal_board.h>
#include <hal_timer.h>

static hal_timer_t *htb_timer = NULL;					//心跳定时器

static void htb_timer_start(void);
static void htb_timer_cb(void *p)
{
	//send heartbeat frame to app
    htb_timer = NULL;
	osel_post(NWK_HEART_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}
static void htb_timer_start(void)
{//start heartbeat
	if (htb_timer == NULL)
	{
		device_info_t device_info = hal_board_info_look();
		HAL_TIMER_SET_REL(MS_TO_TICK(device_info.heartbeat_cycle*1000),
						  htb_timer_cb,
						  NULL,
						  htb_timer);
		DBG_ASSERT(NULL != htb_timer __DBG_LINE);
	}
}

static void nwk_prim_handler(sbuf_t *sbuf)
{
	DBG_ASSERT(NULL != sbuf __DBG_LINE);
	DBG_ASSERT(NULL != sbuf->primargs.pbuf __DBG_LINE);
	switch (sbuf->primtype)
	{
		case M2N_DATA_INDICATION:	//from mac's data
			{
				m2n_frame_parse(sbuf);
			}
			break;
		case M2N_DATA_RESEND:		//from mac's resend data
			{
				if (sbuf->orig_layer == APP_LAYER)
				{//app layer data
					sbuf->primtype = N2A_DATA_RESEND;
					osel_post(NWK2APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
					return;
				}
				else if (sbuf->orig_layer == NWK_LAYER)
				{//nwk layer data
					pbuf_t *pbuf = sbuf->primargs.pbuf;
					if (pbuf->attri.already_send_times.nwk_send_times < 3)
					{
						pbuf->attri.already_send_times.nwk_send_times++;
						sbuf->primtype = N2M_DATA_REQUEST;
						osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
                        return;
					}
					else
					{
						pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
						sbuf_free(&sbuf __SLINE2);
					}
				}
				else
				{
					DBG_ASSERT(FALSE __DBG_LINE);
				}
			}
			break;
		case A2N_DATA_REQUEST:		//from app's data
			{//这里的数据都是发给下面设备的，自己的数据在app层已经处理了
				//1.遍历设备是否在路由表中
				uint64_t dst_id = sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg.dst_addr;

				if (dst_id == read_pib_long_addr())
				{//这是网关的
					n2a_frame_heartbeat_package();
					pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
					sbuf_free(&sbuf __SLINE2);
					return;
				}
				else
				{
					if (rtb_find_index(dst_id)==-1)
					{
						pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
						sbuf_free(&sbuf __SLINE2);
						return;
					}
					//2.把数据发给mac
					n2m_frame_data_package(sbuf);
				}
			}
			break;
		default:
			pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
			sbuf_free(&sbuf __SLINE2);
			break;
	}
}

static void nwk_task(void *e)
{
	DBG_ASSERT(e != NULL __DBG_LINE);
	osel_event_t *p = (osel_event_t *)e;
	switch (p->sig)
	{
		case MAC2NWK_PRIM_EVENT:
		case APP2NWK_PRIM_EVENT:
			nwk_prim_handler((sbuf_t *)(p->param));
			break;
		case NWK_HEART_EVENT:
			htb_timer_start();
			n2a_frame_heartbeat_package();
			break;
		case NWK_ROUTE_TABLE_EVENT:
			rtb_timer_start();
			break;
		default:
			DBG_ASSERT(FALSE __DBG_LINE);
			break;
	}
}

void heartbeat_reset()
{
	if(htb_timer!=NULL)
	{
		hal_timer_cancel(&htb_timer);
	}
    htb_timer = NULL;
	htb_timer_start();
}

void nwk_init(void)
{
	osel_task_tcb *nwk_task_handle = osel_task_create(&nwk_task, NWK_TASK_PRIO);

	osel_subscribe(nwk_task_handle, MAC2NWK_PRIM_EVENT);
	osel_subscribe(nwk_task_handle, APP2NWK_PRIM_EVENT);
	osel_subscribe(nwk_task_handle, NWK_HEART_EVENT);
	osel_subscribe(nwk_task_handle, NWK_ROUTE_TABLE_EVENT);
	rtb_init();

	htb_timer = NULL;
	nwk_info.htb_alarm_info = TRUE;

	if (htb_timer == NULL)
	{
		HAL_TIMER_SET_REL(MS_TO_TICK(5*1000),
						  htb_timer_cb,
						  NULL,
						  htb_timer);
		DBG_ASSERT(NULL != htb_timer __DBG_LINE);
	}
}
