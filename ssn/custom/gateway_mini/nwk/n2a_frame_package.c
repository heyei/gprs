#include <mac_pib.h>
#include <sbuf.h>
#include <pbuf.h>
#include <hal_nvmem.h>
#include <n2a_frame_package.h>
#include <nwk_global_agreement.h>

static uint16_t misc_get_reset_code_line(void)
{
	return debug_get_info();
}

void n2a_frame_heartbeat_package(void)
{//assemble heartbeat frame
	frame_ctrl_t frame_ctrl;
	heart_frame_t heart_frame;
	n2a_data_indication_t *n2a_data;

	sbuf_t *sbuf = sbuf_alloc(__SLINE1);
	DBG_ASSERT(NULL != sbuf __DBG_LINE);
	sbuf->primtype = N2A_HEARTBEAT;
	sbuf->orig_layer = NWK_LAYER;
	n2a_data = &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg;
	n2a_data->src_addr = NWK_ADDR;
	n2a_data->dst_addr = NWK_ADDR;
	n2a_data->rssi = 0;
	if (nwk_info.htb_alarm_info == TRUE)
	{//send alarm info
		nwk_info.htb_alarm_info = FALSE;
		frame_ctrl.device_type = SINK;
		frame_ctrl.alarm_type = DEVICE_REST;
		frame_ctrl.energy_report_support = E_SUPPORT_WITHOUT;
		frame_ctrl.localization_support = FALSE;
		frame_ctrl.overhear_mode_support = FALSE;

		heart_frame.farther_id = 0xffff;
		heart_frame.successful_rate = 100;
		heart_frame.residual_energy = 100;
		heart_frame.alarm_info = misc_get_reset_code_line();
        
        debug_clr_info();
	}
	else
	{//send nomal
		frame_ctrl.device_type = SINK;
		frame_ctrl.alarm_type = NON_ALARM_INFO;
		frame_ctrl.energy_report_support = E_SUPPORT_WITHOUT;
		frame_ctrl.localization_support = FALSE;
		frame_ctrl.overhear_mode_support = FALSE;

		heart_frame.farther_id = 0xffff;
		heart_frame.successful_rate = 100;
		heart_frame.residual_energy = 100;
	}

	pbuf_t *pbuf = pbuf_alloc(LARGE_PBUF_BUFFER_SIZE __PLINE1);
	DBG_ASSERT(NULL != pbuf __DBG_LINE);
	sbuf->primargs.pbuf = pbuf;
	n2a_data->nsdu = pbuf->head;
	pbuf->data_p = n2a_data->nsdu;
	osel_memcpy(pbuf->data_p, &frame_ctrl, sizeof(frame_ctrl_t));
	pbuf->data_p++;

	osel_memcpy(pbuf->data_p, &heart_frame.farther_id, 2);
	pbuf->data_p+=2;
	osel_memcpy(pbuf->data_p, &heart_frame.successful_rate, 1);
	pbuf->data_p+=1;

	if (frame_ctrl.energy_report_support != E_UNSUPPORT)
	{
		osel_memcpy(pbuf->data_p, &heart_frame.residual_energy, 1);
		pbuf->data_p++;
	}
	if (frame_ctrl.alarm_type != NON_ALARM_INFO)
	{
		osel_memcpy(pbuf->data_p, &heart_frame.alarm_info, 2);
		pbuf->data_p+=2;
	}

	n2a_data->nsdu_length = pbuf->data_len = pbuf->data_p - pbuf->head;
	osel_post(NWK2APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}
