#include <pbuf.h>
#include <sbuf.h>
#include <phy_packet.h>
#include <mac_frames.h>
#include <mac_pib.h>
#include <n2m_frame_package.h>
#include <node_cfg.h>
#include <hal_board.h>
#include <route_table.h>

#define MAC_SIZE	(MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE+ MAC_ADDR_SHORT_SIZE + MAC_ADDR_SHORT_SIZE)

void n2m_frame_data_package(sbuf_t *sbuf)
{
	sbuf->primtype = N2M_DATA_REQUEST;
	n2m_data_request_t *n2m_data = &sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg;
	a2n_data_request_t *a2n_data = &sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
	pbuf_t *pbuf = sbuf->primargs.pbuf;
	n2m_data->dst_mode = NWK_ADDR_16BITS;
	n2m_data->src_mode = NWK_ADDR_16BITS;

    int index = rtb_find_index(a2n_data->dst_addr);
	if(index == -1)
	{
		return;
	}
	rtb_t *info = rtb_find_info(index);
	n2m_data->dst_addr = info->next_mac_addr;

	n2m_data->msdu_length = a2n_data->nsdu_length + NWK_HEAD_CTRL_SIZE + NWK_ADDR_SHORT_SIZE*2;
	n2m_data->msdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE;

	nwk_nhr_ctrl_t nwk_nhr;
	nwk_nhr.frame_type = NWK_DATA;
	nwk_nhr.dest_addr_mode = NWK_ADDR_16BITS;
	nwk_nhr.source_addr_mode = NWK_ADDR_16BITS;

	uint16_t dst_id = info->self_nwk_addr;
	uint16_t src_id = NWK_ADDR;
	osel_memcpy(pbuf->data_p,&nwk_nhr,NWK_HEAD_CTRL_SIZE);
	pbuf->data_p+=NWK_HEAD_CTRL_SIZE;
	osel_memcpy(pbuf->data_p,&dst_id,NWK_ADDR_SHORT_SIZE);
	pbuf->data_p+=NWK_ADDR_SHORT_SIZE;
	osel_memcpy(pbuf->data_p,&src_id,NWK_ADDR_SHORT_SIZE);
	pbuf->data_p+=NWK_ADDR_SHORT_SIZE;

	osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void n2m_frame_join_reply_package(uint64_t long_addr)
{//入网应答帧
    uint16_t sink_short_addr = NWK_ADDR;
	nwk_nhr_ctrl_t head;
	int index = rtb_find_index(long_addr);
	if(index == -1)
	{
		return;
	}
	rtb_t *info = rtb_find_info(index);

    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
	DBG_ASSERT(NULL != pbuf __DBG_LINE);
	sbuf_t *sbuf = sbuf_alloc(__SLINE1);
	DBG_ASSERT(NULL != sbuf __DBG_LINE);
	sbuf->primtype = N2M_DATA_REQUEST;
	sbuf->orig_layer = NWK_LAYER;
	sbuf->primargs.pbuf = pbuf;
    
	n2m_data_request_t *n2m_data_request = &sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg;
	n2m_data_request->msdu_length = NWK_HEAD_CTRL_SIZE + NWK_ADDR_LONG_SIZE + NWK_ADDR_SHORT_SIZE + MAC_ADDR_SHORT_SIZE +NWK_ADDR_SHORT_SIZE;
    n2m_data_request->src_mode = NWK_ADDR_16BITS;
	if(info->info.mode == TRUE)//直连设备
	{
		n2m_data_request->dst_mode = NWK_ADDR_64BITS;
		n2m_data_request->dst_addr = info->self_long_addr;
        n2m_data_request->msdu = pbuf->head +PHY_HEAD_SIZE +MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE
		+ MAC_ADDR_LONG_SIZE + MAC_ADDR_SHORT_SIZE;
	}
	else
	{
		n2m_data_request->dst_mode = NWK_ADDR_16BITS;
		n2m_data_request->dst_addr = info->next_mac_addr;
        n2m_data_request->msdu = pbuf->head +PHY_HEAD_SIZE +MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE
		+ MAC_ADDR_SHORT_SIZE + MAC_ADDR_SHORT_SIZE;
	}
    head.frame_type = NWK_REPLY;	
	head.source_addr_mode = NWK_ADDR_16BITS;
    head.dest_addr_mode = NWK_ADDR_64BITS;

	pbuf->data_p = n2m_data_request->msdu;
	osel_memcpy(pbuf->data_p,&head,NWK_HEAD_CTRL_SIZE);
	pbuf->data_p += NWK_HEAD_CTRL_SIZE;

	osel_memcpy(pbuf->data_p,&info->self_long_addr,NWK_ADDR_LONG_SIZE);
	pbuf->data_p += NWK_ADDR_LONG_SIZE;
	osel_memcpy(pbuf->data_p,&sink_short_addr,NWK_ADDR_SHORT_SIZE);
	pbuf->data_p += NWK_ADDR_SHORT_SIZE;

	uint16_t mac_short_addr = convert_mac_short_addr(info->self_long_addr);
	osel_memcpy(pbuf->data_p,&mac_short_addr,NWK_ADDR_SHORT_SIZE);
	pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	osel_memcpy(pbuf->data_p,&info->self_nwk_addr,NWK_ADDR_SHORT_SIZE);
	pbuf->data_p += NWK_ADDR_SHORT_SIZE;

	pbuf->attri.already_send_times.nwk_send_times = 0;

	osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}
