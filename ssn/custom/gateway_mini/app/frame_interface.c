#include <frame_interface.h>
#include <north_frame.h>
#include <south_frame.h>
#include <north_parse.h>
#include <south_parse.h>
#include <north_deal.h>
#include <south_deal.h>
#include <route_table.h>
#include <mac_pib.h>
static bool_t misc_pbuf_to_tempbuf(uint8_t *frame, uint16_t max_size,
                                   uint16_t*p_len, pbuf_t *pbuf)
{
    if (pbuf->data_len > max_size)
    {
        return FALSE;
    }
    pbuf->data_p = pbuf->head;
    *p_len = pbuf->data_len;
    osel_memcpy(&frame[0], pbuf->data_p, pbuf->data_len);
    return TRUE;
}

static bool_t sbuf_to_tempbuf(uint8_t *frame, uint16_t max_size, uint16_t*p_len,
                              sbuf_t *sbuf, south_instruction_data_t
                                  *instruction)
{
    n2a_data_indication_t *n2a_data = &sbuf
        ->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg;
    if (n2a_data->nsdu_length > max_size)
    {
        return FALSE;
    }
    int index = rtb_find_index_through_nwk_addr(n2a_data->src_addr);
    rtb_t *rtb_info;
    instruction->nwk_id = n2a_data->src_addr;
    if (NWK_ADDR != instruction->nwk_id)
    {
        if (index ==  - 1)
        {
            return FALSE;
        }
        else
        {
            rtb_info = rtb_find_info(index);
            instruction->id = rtb_info->self_long_addr;
        }
    }
    else
    {
        instruction->id = read_pib_long_addr();
    }
    *p_len = n2a_data->nsdu_length;
    osel_memcpy(&frame[0], &((uint8_t*)n2a_data->nsdu)[0], n2a_data
                ->nsdu_length);
    instruction->nwk_type = sbuf->primtype;
    return TRUE;
}

#include <stdio.h>
static void printf_info(pbuf_t *pbuf)
{
    static uint16_t seq = 1;
    uint16_t id = pbuf->attri.src_id;
    int8_t rssi = pbuf->attri.rssi_dbm;
    
    if(id != 0)
    {
        seq++;
        //        printf("Recv %d frames: ", seq);
        //        printf("ID is 0x%4X, RSSI is %d\r\n", id, rssi);
    }
}

//南向解析接口
void south_parse_interface(sbuf_t *sbuf)
{
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    DBG_ASSERT(NULL != sbuf->primargs.pbuf __DBG_LINE);
    
    south_instruction_data_t instruction;
    uint8_t in_frame[PKT_LEN_MAX];
    uint16_t in_frame_len;
    
    if (sbuf->primtype == N2A_DATA_RESEND)
        //数据是来自网络层的重传
    {
        if (sbuf->orig_layer == APP_LAYER)
        {
            pbuf_t *pbuf = sbuf->primargs.pbuf;
            if (pbuf->attri.already_send_times.app_send_times < 3)
            {
                pbuf->attri.already_send_times.app_send_times++;
                sbuf->primtype = A2N_DATA_REQUEST;
                osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
                return ;
            }
            else
            {
                pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
                sbuf_free(&sbuf __SLINE2);
                return ;
            }
        }
        else
        {
            DBG_ASSERT(FALSE __DBG_LINE);
        }
    }
    //1.将pbuf中的数据拷贝到临时数组中，并释放pbuf、sbuf
    if (!sbuf_to_tempbuf(&in_frame[0], ARRAY_LEN(in_frame), &in_frame_len, sbuf,
                         &instruction))
    {
        pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
        sbuf_free(&sbuf __SLINE2);
        return ;
    }
    
    printf_info(sbuf->primargs.pbuf);
    
    pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
    sbuf_free(&sbuf __SLINE2);
    
    //2.解析报文
    if (FALSE == south_parse_instruction_frame(in_frame, in_frame_len,
                                               &instruction))
    {
        return ;
    }
    //3.事件处理
    south_deal_interface(&instruction);
}


//北向解析接口
void north_parse_interface(pbuf_t *pbuf)
{
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(pbuf->head != NULL __DBG_LINE);
    DBG_ASSERT(pbuf->data_len > 0 __DBG_LINE);
    
    uint8_t in_frame[UART_LEN_MAX];
    uint16_t in_frame_len;
    
    //1.将pbuf中的数据拷贝到临时数组中，并释放pbuf
    if (!misc_pbuf_to_tempbuf(&in_frame[0], ARRAY_LEN(in_frame), &in_frame_len,
                              pbuf))
    {
        pbuf_free(&pbuf __PLINE2);
        return ;
    }
    pbuf_free(&pbuf __PLINE2);
    
    //2.解析报文
    north_instruction_data_t north_in_data;
    if (FALSE == north_parse_instruction_frame(in_frame, in_frame_len,
                                               &north_in_data))
    {
        return ;
    }
    
    //3.事件处理
    north_deal_interface(&north_in_data);
}
