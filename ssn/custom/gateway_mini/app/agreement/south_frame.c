#include <south_frame.h>
#include <phy_packet.h>
#include <app_func.h>
#include <south_parse.h>

sbuf_t *south_app_request_package(app_head_t *request, uint8_t *dest_id)
{
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);

    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;
    sbuf->primargs.pbuf = pbuf;
    pbuf->attri.already_send_times.app_send_times = 0;

    a2n_data_request_t *a2n_data = &sbuf
        ->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
    uint8_t id[8];
    osel_memcpy(&id[0], dest_id, 8);

    osel_memcpy(&a2n_data->dst_addr, &id[0], 8);

    a2n_data->nsdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE +
        NWK_SIZE;
    osel_memcpy(pbuf->data_p, request, sizeof(app_head_t));
    pbuf->data_p += sizeof(app_head_t);

    a2n_data->nsdu_length = sizeof(app_head_t);

    return sbuf;
}

sbuf_t *south_config_package(north_instruction_data_t *data)
{
    app_south_config_t app_south_config;
    uint16_t app_type = 0;
    app_head_t head;
    head.frame_type = APP_FRAME_TYPE_CONFIG;
    head.ack_request = TRUE;
    head.data_type = APP_DATA_TYPE_NOT_DATA;
    head.seq_num = data->seq;

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;
    sbuf->primargs.pbuf = pbuf;
    pbuf->attri.already_send_times.app_send_times = 0;
    a2n_data_request_t *a2n_data = &sbuf
        ->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
    uint8_t id[8];
    osel_memcpy(&id[0], &data->dest_id[0], 8);
    reverse_id_endian(id);
    osel_memcpy(&a2n_data->dst_addr, &id[0], 8);

    switch (data->config.code)
    {
        case CONFIG_HEARTBEAT_CYCLE:
            app_south_config.list.config_name = HEARTBEAT_CYCLE;
            app_south_config.list.value = data->config.heart_cycle;
            break;
        case CONFIG_DATA_CYCLE:
            {
                app_type = data->config.data_cycle.sensor_type;
                if (app_type == DATA_CYCLE_TM_HM)
                {
                    app_south_config.list.config_name = TEM_HUM_DATA_CYCLE;
                }
                else if (app_type == DATA_CYCLE_ACCELER)
                {
                    app_south_config.list.config_name = ACCEL_SPEED_DATA_CYCLE;
                }
                else
                {
                    return NULL;
                }
                app_south_config.list.value = data->config.data_cycle.time;
            }
            break;
        case CONFIG_DATA_DETECTION_CYCLE:
            {
                app_type = data->config.data_cycle.sensor_type;
                if (app_type == DATA_DETECTION_TM_HM)
                {
                    app_south_config.list.config_name = TEM_HUM_DETECTION_CYCLE;
                }
                else if (app_type == DATA_DETECTION_ACCELER)
                {
                    app_south_config.list.config_name =
                        ACCEL_SPEED_DETECTION_CYCLE;
                }
                else
                {
                    return NULL;
                }
                app_south_config.list.value = data->config.data_cycle.time;
            }
            break;
        default:
            DBG_ASSERT(FALSE __DBG_LINE);
            break;
    }

    app_south_config.num = 1;
    app_south_config.list.len = 4;
    a2n_data->nsdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE +
        NWK_SIZE;
    osel_memcpy(pbuf->data_p, &head, sizeof(app_head_t));
    pbuf->data_p += sizeof(app_head_t);
    osel_memcpy(pbuf->data_p, &app_south_config, sizeof(app_south_config_t));
    a2n_data->nsdu_length = sizeof(app_head_t) + sizeof(app_south_config_t);

    return sbuf;
}

sbuf_t *south_select_package(north_instruction_data_t *data)
{
    app_head_t head;
    head.frame_type = APP_FRAME_TYPE_SELECT;
    head.ack_request = FALSE;
    head.data_type = APP_DATA_TYPE_NOT_DATA;
    head.seq_num = data->seq;

    select_command_t cmd;
    cmd = data->select_command;
    if (cmd.select_type == S_STATE)
    {
        sbuf_t *sbuf = sbuf_alloc(__SLINE1);
        DBG_ASSERT(NULL != sbuf __DBG_LINE);
        pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
        DBG_ASSERT(NULL != pbuf __DBG_LINE);
        sbuf->primtype = A2N_DATA_REQUEST;
        sbuf->orig_layer = APP_LAYER;
        sbuf->primargs.pbuf = pbuf;
        pbuf->attri.already_send_times.app_send_times = 0;

        a2n_data_request_t *a2n_data = &sbuf
            ->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
        a2n_data->nsdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE +
            NWK_SIZE;
        uint8_t id[8];
        osel_memcpy(&id[0], &data->dest_id[0], 8);
        reverse_id_endian(id);
        osel_memcpy(&a2n_data->dst_addr, &id[0], 8);
        osel_memcpy(pbuf->data_p, &head, sizeof(app_head_t));
        pbuf->data_p += sizeof(app_head_t);

        int num = 0;
        uint8_t *begin = pbuf->data_p;
        uint8_t *p = pbuf->data_p;
        p++;
        if (cmd.heartbeat_cycle)
        {
            num++;
            *p = APP_SELECT_HEARTBEAT_CYCLE;
            p++;
        }
        if (cmd.device_electricity)
        {
            num++;
            *p = APP_SELECT_DEVICE_ELECTRICITY;
            p++;
        }
        *begin = num;
        a2n_data->nsdu_length = sizeof(app_head_t) + (1+num);
        return sbuf;
    }
    else
    {
        return NULL;
    }
}

sbuf_t *south_app_data_package(north_instruction_data_t *data)
{
    app_head_t head;
    head.frame_type = APP_FRAME_TYPE_REST;
    head.ack_request = FALSE;
    head.data_type = APP_DATA_TYPE_NOT_DATA;
    head.seq_num = data->seq;

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);

    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;
    sbuf->primargs.pbuf = pbuf;
    pbuf->attri.already_send_times.app_send_times = 0;

    a2n_data_request_t *a2n_data = &sbuf
        ->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
    uint8_t id[8];
    osel_memcpy(&id[0], &data->dest_id[0], 8);
    reverse_id_endian(id);
    osel_memcpy(&a2n_data->dst_addr, &id[0], 8);

    a2n_data->nsdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE +
        NWK_SIZE;
    osel_memcpy(pbuf->data_p, &head, sizeof(app_head_t));
    pbuf->data_p += sizeof(app_head_t);

    int pload_len = data->length - NORTH_INSTRUCTION_HEAD_LEN;
    osel_memcpy(pbuf->data_p, data->pload, pload_len);
    pbuf->data_p += pload_len;
    a2n_data->nsdu_length = sizeof(app_head_t) + pload_len;

    return sbuf;
}

sbuf_t *south_instruction_package(north_instruction_data_t *data)
{
    app_head_t head;
    head.ack_request = FALSE;
    head.data_type = APP_DATA_TYPE_NOT_DATA;
    head.seq_num = data->seq;
    switch (data->type)
    {
        case NM_DEVICE_REST:
            head.frame_type = APP_FRAME_TYPE_REST;
            break;
        case NM_LINK_TEST:
            head.frame_type = APP_FRAME_TYPE_LINKTEST;
            break;
        default:
            return NULL;
            break;
    }

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);

    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;
    sbuf->primargs.pbuf = pbuf;
    pbuf->attri.already_send_times.app_send_times = 0;

    a2n_data_request_t *a2n_data = &sbuf
        ->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg;
    uint8_t id[8];
    osel_memcpy(&id[0], &data->dest_id[0], 8);
    reverse_id_endian(id);
    osel_memcpy(&a2n_data->dst_addr, &id[0], 8);

    a2n_data->nsdu = pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_SIZE +
        NWK_SIZE;
    osel_memcpy(pbuf->data_p, &head, sizeof(app_head_t));
    pbuf->data_p += sizeof(app_head_t);
    a2n_data->nsdu_length = sizeof(app_head_t);

    return sbuf;
}
