#include <hal_board.h>
#include <north_parse.h>
#include <global_variable.h>
#include <hal_socket.h>
#include <app_func.h>
#include <north_frame.h>

static bool_t config(const uint8_t *frame, uint8_t size,
                     north_instruction_data_t *data)
{
    if (size != 28 && size != 30)
    {
        return FALSE;
    }
    osel_memcpy(&data->config.code, frame, 2);
    frame += 2;
    data->config.code = S2B_UINT16(data->config.code);
    switch (data->config.code)
    {
    case CONFIG_HEARTBEAT_CYCLE:
        {
            osel_memcpy(&data->config.heart_cycle, frame, 4);
            data->config.heart_cycle = S2B_UINT32(data->config.heart_cycle);
        }
        break;
    case CONFIG_DATA_CYCLE:
    case CONFIG_DATA_DETECTION_CYCLE:
        {
            osel_memcpy(&data->config.data_cycle.sensor_type, frame, 2);
            frame += 2;
            osel_memcpy(&data->config.data_cycle.time, frame, 4);
            data->config.data_cycle.sensor_type = S2B_UINT16(data
                                                             ->config.data_cycle.sensor_type);
            data->config.data_cycle.time = S2B_UINT32(data
                                                      ->config.data_cycle.time);
        }
        break;
    default:
        data->config.code = CONFIG_ERROR;
        break;
    }
    return TRUE;
}

static bool_t replay_deal(const uint8_t *frame, uint8_t size,
                          north_instruction_data_t *data)
{
    if (size != (NORTH_INSTRUCTION_HEAD_LEN + 2))
    {
        return FALSE;
    }
    data->replay.code = ((uint16_t)(*frame) << 8) + (uint16_t)*(frame + 1);
    return TRUE;
}

static bool_t select(const uint8_t *frame, uint8_t size,
                     north_instruction_data_t *data)
{
    if (size != 24)
    {
        return FALSE;
    }
    osel_memcpy(&data->select_command, frame, sizeof(select_command_t));
    return TRUE;
}

static bool_t link_test(const uint8_t *frame, uint8_t size,
                        north_instruction_data_t *data)
{
    if (size != 22)
    {
        return FALSE;
    }
    return TRUE;
}

static bool_t app_data(const uint8_t *frame, uint8_t size,
                       north_instruction_data_t *data)
{
    if (size > NORTH_INSTRUCTION_HEAD_LEN)
    {
        data->pload = (uint8_t*) &frame[0];
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static void device_info(const uint8_t *frame, uint8_t size,
                        north_instruction_data_t *data)
{
    if(0x17 == size)
    {
        device_info_t device_info;
        if(*frame == 0) //查询
        {
            device_info = hal_board_info_get();
            north_instruction_data_t data = north_frame_device_info(device_info.device_id, sizeof(device_info_t));
            data.pload = &device_info.device_id[0];
            north_frame_send(&data);
        }
        else if(*frame == 1)    //配置
        {
            
        }
    }
}

bool_t north_parse_instruction_frame(const uint8_t *frame, uint8_t size,
                                     north_instruction_data_t *data)
{
    osel_memcpy(&data->version, frame, NORTH_INSTRUCTION_HEAD_LEN);
    //拷贝头部信息
    data->length = S2B_UINT16(data->length);
    data->seq = S2B_UINT16(data->seq);
    frame += NORTH_INSTRUCTION_HEAD_LEN;
    
    if ((data->type &IS_REPLY) == IS_REPLY)
        //是应答帧
    {
        if ((data->type &GET_DATA_TYPE) != NM_APP_DATA)
        {
            uint16_t seq = data->seq;
            hal_socket_cache_remove(seq);
        }
    }
    
    switch (data->type &GET_DATA_TYPE)
    {
    case NM_HEARTBEAT:
        break;
    case NM_APP_DATA:
        return app_data(frame, size, data);
        break;
    case NM_REGISTER:
        return replay_deal(frame, size, data);
        break;
    case NM_CONFIG:
        return config(frame, size, data);
        break;
    case NM_SELECT:
        return select(frame, size, data);
        break;
    case NM_LINK_TEST:
        return link_test(frame, size, data);
        break;
    case NM_DEVICE_REST:
        {
            if (size != 0x16)
            {
                return FALSE;
            }
        }
        break;
    case NM_SELECT_EXTEND:
        break;
    case NM_DEVICE_INFO:
        device_info(frame, size, data);
        return FALSE;
        break;
    default:
        return FALSE;
    }
    return TRUE;
}
