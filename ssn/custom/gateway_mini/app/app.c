#include <osel_arch.h>
#include <socket.h>
#include <hal_socket.h>
#include <hal_clock.h>
#include <app.h>
#include <app_func.h>
#include <frame_interface.h>
#include <replay_module.h>
#include <driver.h>
#include <north_frame.h>
#include <hal_board.h>
#include <hal_uart.h>
#include <gprs.h>
#define  GPRS_AT                     "hello,world"//"ATaaaa"//"aaaa"
app_info_t _app_info;
static void app_cycle_timer_cb(void *p)
{
	_app_info.cycle_timer_handle = NULL;
	osel_post(APP_CYCLE_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

/*static void read_data(const uint8_t *const buf, const uint8_t len)
{
	_NOP();
}*/

static void app_cycle_timer_start(void)
{
	if (_app_info.cycle_timer_handle == NULL)
	{
		hal_wdt_clear(16000);
		hal_hardware_wdt_clear();

		gprs_info_t gprs_info;
		gprs_driver.get(&gprs_info);
		if(gprs_info.gprs_state == WORK_ON)
		{
            hal_led_open(HAL_LED_BLUE);
            gprs_driver.write(GPRS_AT);
		}
		else    hal_led_close(HAL_LED_BLUE);
        
		HAL_TIMER_SET_REL(MS_TO_TICK(4000),
						  app_cycle_timer_cb,
						  NULL,
						  _app_info.cycle_timer_handle);
		DBG_ASSERT(NULL != _app_info.cycle_timer_handle __DBG_LINE);
	}
	else
	{
		_NOP();
		
	}
}


static void app_task(void *e)
{
	DBG_ASSERT(e != NULL __DBG_LINE);
	osel_event_t *p = (osel_event_t*)e;
	switch (p->sig)
	{
	case APP_CYCLE_TIMER_EVENT:
		app_cycle_timer_start();
		break;
	default:
        DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
}

void app_init(void)
{
	osel_task_tcb *app_task_handle = osel_task_create(&app_task, APP_TASK_PRIO);
	
	osel_subscribe(app_task_handle, APP_CYCLE_TIMER_EVENT);
    gprs_driver.init();

    app_cycle_timer_start();
}
