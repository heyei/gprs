#include <serial.h>
#include <socket.h>
#include <pbuf.h>
#include <sbuf.h>
#include <node_cfg.h>
#include <hal_board.h>
#include <hal_socket.h>
#include <data_type_def.h>
#include <app.h>
#include <frame_interface.h>
#include <north_frame.h>
#include <route_table.h>
static volatile bool_t cnn_state = FALSE; //platform cnn state
#define REGISTER_TIME   (60u)                       //注册间隔(秒)
#define AT_START        "AT"            //!< 命令的起始字符串
#define CMD_LINK_CHAR   "&"             //!< 命令间隔连接字符
#define CMD_DEV_ID_TYPE  "ID"        //!<DEVICE ID的配置指令
#define AT_OK           "OK\r\n"        //!< 指令输入成功以后的返回
void reverse_id_endian(uint8_t *id)
{
    for (uint8_t i = 0, j = 7; i < j; i++, j--)
    {
        uint8_t temp;
        temp = id[i];
        id[i] = id[j];
        id[j] = temp;
    }
}

static void north_frame_cb(void)
{
    osel_post(APP_UART_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void uart_frame_cfg(void)
{
	serial_reg_t app_serial_reg;
	
    app_serial_reg.sd.valid = TRUE;
    app_serial_reg.sd.len = 2;
    app_serial_reg.sd.pos = 0;
    app_serial_reg.sd.data[0] = 'A';
    app_serial_reg.sd.data[1] = 'T';
	
    app_serial_reg.ld.valid = FALSE;
	
    app_serial_reg.argu.len_max = UART_LEN_MAX;
    app_serial_reg.argu.len_min = 4;
	
    app_serial_reg.ed.valid = TRUE;
    app_serial_reg.ed.len = 1;
    app_serial_reg.ed.data[0] = 0x0D;   // 'CR', enter
	
    app_serial_reg.echo_en = FALSE;
    app_serial_reg.func_ptr = north_frame_cb;
	
    serial_fsm_init(SERIAL_1);
    serial_reg(SERIAL_1, app_serial_reg);
	
}

bool_t platform_state(void)
{
    return cnn_state;
}

void cnn_platform(void)
{
    cnn_state = TRUE;
}

void discnn_platform(void)
{
    cnn_state = FALSE;
}

static void gw_register_cb(void *p)
{
    _app_info.register_timer = NULL;
    osel_post(APP_GW_REGISTER, NULL, OSEL_EVENT_PRIO_LOW);
}

void gw_register_handle(void)
{
    device_info_t device_info = hal_board_info_look();
    bool_t has_register = TRUE;
    north_instruction_data_t data;
    uint32_t time = REGISTER_TIME;
    //查询网关是否向平台注册
    if (!platform_state())
    {
        has_register = FALSE;
        data = north_frame_register(&device_info.device_id[0]);
        north_frame_send(&data);
    }
    else
    {
		//注册设备
		uint8_t has_send = 0;
		for (int i = 0; i < RTB_DEEPNESS; i++)
		{
			if (device_if_exist(i) == FALSE)
			{
				//发送该设备的注册帧
				if (device_if_register(i) == FALSE)
				{
					uint8_t id[8] =
					{
						0
					};
					uint64_t long_addr = device_long_addr(i);
					osel_memcpy(&id[0], &long_addr, 8);
					data = north_frame_register(&id[0]);
					north_frame_send(&data);
					has_register = FALSE;
					
					//默认注册上
					rtb_device_register(long_addr);
				}
				if (has_send++ >= 5)
				{
					time = 3;
					break;
				}
			}
		}
    }
    if (_app_info.register_timer == NULL)
    {
        if (has_register == FALSE)
        {
            HAL_TIMER_SET_REL(MS_TO_TICK(time *1000), gw_register_cb, NULL,
                              _app_info.register_timer);
            DBG_ASSERT(NULL != _app_info.register_timer __DBG_LINE);
        }
    }
}

uint8_t converthexchar(uint8_t ch)
{
    if ( (ch >= '0') && (ch <= '9'))
    {
        return (ch - 0x30);
    }
    else if ((ch >= 'A') && (ch <= 'F'))
    {
        return (ch - 'A' + 10);
    }
    else if ((ch >= 'a') && (ch <= 'f'))
    {
        return (ch - 'a' + 10);
    }
    else
    {
        return -1;
    }
}

void auto_dev_id_set(uint8_t const * const buf)
{
    device_info_t device_info = hal_board_info_get();
    uint8_t buf_temp_1[16] = {0x00};
    uint8_t buf_temp_2[8] = {0x00};
    osel_memcpy(buf_temp_1, buf, 2*sizeof(uint64_t));
    
    for (uint8_t i = 0;i < 16;i++)
    {
        buf_temp_1[i] = converthexchar(buf[i]);
    }
       
    buf_temp_2[7] = (buf_temp_1[0] << 4) + buf_temp_1[1];
    buf_temp_2[6] = (buf_temp_1[2] << 4) + buf_temp_1[3];
    buf_temp_2[5] = (buf_temp_1[4] << 4) + buf_temp_1[5];
    buf_temp_2[4] = (buf_temp_1[6] << 4) + buf_temp_1[7];
    buf_temp_2[3] = (buf_temp_1[8] << 4) + buf_temp_1[9];
    buf_temp_2[2] = (buf_temp_1[10] << 4) + buf_temp_1[11];
    buf_temp_2[1] = (buf_temp_1[12] << 4) + buf_temp_1[13];
    buf_temp_2[0] = (buf_temp_1[14] << 4) + buf_temp_1[15];
    
    osel_memcpy(device_info.device_id, buf_temp_2, sizeof(uint64_t));
	osel_int_status_t s;
    OSEL_ENTER_CRITICAL(s);
    hal_board_info_save(&device_info, TRUE);
	serial_write(HAL_UART_1, AT_OK, sizeof(AT_OK) - 1);  
	OSEL_EXIT_CRITICAL(s);
}

static int8_t auto_device_id_cfg_parse(uint8_t const * const buf, uint8_t len)
{
    uint8_t offset = 0;           
    if(osel_memcmp((uint8_t *)&buf[offset], CMD_DEV_ID_TYPE, sizeof(CMD_DEV_ID_TYPE) - 1))
    {
        offset += sizeof(CMD_DEV_ID_TYPE)-1;
        if(!osel_memcmp((uint8_t *)&buf[offset], CMD_LINK_CHAR, sizeof(CMD_LINK_CHAR) - 1))
        {             
            return -1;
        }
        offset += sizeof(CMD_LINK_CHAR)-1;

        auto_dev_id_set((uint8_t *)&buf[offset]);         
        hal_board_reset();
        return TRUE;  
    }
    else
    {
        return -3;
    }    
}

void auto_cmd_parse(uint8_t *buf, uint8_t len)
{
	 uint8_t offset = 0;
    // AT
    if(!osel_memcmp((uint8_t *)&buf[offset], AT_START, sizeof(AT_START) - 1))
    {
        return;  //!< 
    }
    offset += sizeof(AT_START)-1;   
    // &
    if(!osel_memcmp((uint8_t *)&buf[offset], CMD_LINK_CHAR, sizeof(CMD_LINK_CHAR) - 1))
    {
        return;
    }

    offset += sizeof(CMD_LINK_CHAR)-1;
    
//    if(auto_rf_ch_parse(&buf[offset], len) != -3)return;
    if(auto_device_id_cfg_parse(&buf[offset], len) != -3)return;
    
}

void uart_event_handle(void)
{
	uint8_t frame_len = 0;
    uint8_t read_data = 0;
    
    static uint8_t cmd_recv_array[50];
    osel_memset(cmd_recv_array, 0x00, sizeof(cmd_recv_array));
    
    while (serial_read(HAL_UART_1, &read_data, sizeof(uint8_t)))
    {
        cmd_recv_array[frame_len++] = read_data;
        if (read_data == 0x0A)
        {
            break;
        }
    }
	auto_cmd_parse(&cmd_recv_array[0], frame_len);
}

bool_t judge_self_id(uint8_t *judge_id)
{
    device_info_t device_info = hal_board_info_look();
    uint8_t id[8] =
    {
        0
    };
    osel_memcpy(&id[0], &device_info.device_id[0], 8);
    reverse_id_endian(&id[0]);
    if (osel_memcmp(&judge_id[0], &id[0], 8))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
