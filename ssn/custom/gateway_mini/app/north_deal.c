#include <osel_arch.h>
#include <hal_board.h>
#include <north_deal.h>
#include <app_func.h>
#include <north_parse.h>
#include <north_frame.h>
#include <south_frame.h>
#include <mac_pib.h>
#include <app.h>
#include <nwk.h>
#include <route_table.h>
#include <global_variable.h>
#include <frame_interface.h>
#include <replay_module.h>

static bool_t find_device(north_instruction_data_t *data)
{
	uint64_t dev = 0;
	uint8_t id[8];
	osel_memcpy(&id[0], &data->dest_id[0], 8);
	reverse_id_endian(id);
	osel_memcpy(&dev, &id[0], 8);
	int index = rtb_find_index(dev);
	if (index !=  - 1)
	{
		return TRUE;
	}
	return FALSE;
}

static void replay_deal(north_instruction_data_t *data, uint16_t code, uint16_t
						result)
{
	device_info_t device_info = hal_board_info_look();
	uint8_t id[8];
	osel_memcpy(&id[0], &device_info.device_id[0], 8);
	replay_pload_t pload;
	pload.code = S2B_UINT16(code);
	pload.result = S2B_UINT16(result);
	pload.seq = data->seq;
	pload.type = data->type;
    
	north_instruction_data_t instruction = north_frame_replay(&id[0], &pload);
	north_frame_send(&instruction);
}

static bool_t add_replay(north_instruction_data_t *data, uint16_t code)
{
	uint8_t id[8];
	uint64_t dst = 0;
	osel_memcpy(&id[0], &data->dest_id[0], 8);
	reverse_id_endian(id);
	osel_memcpy(&dst, &id[0], 8);
	int index = rtb_find_index(dst);
	if (index !=  - 1)
	{
		rtb_t *rtb = rtb_find_info(index);
		replay_info_t node;
		node.nwk_addr = rtb->self_nwk_addr;
		node.seq = data->seq;
		node.code = code;
		if (replay_list_add(node))
		{
			return TRUE;
		}
	}
	return FALSE;
}
static void send_down(north_instruction_data_t *north_in_data)
{
	if (!find_device(north_in_data))
	{
		return ;
	}
	sbuf_t *sbuf = south_instruction_package(north_in_data);
	if (sbuf != NULL)
	{
		osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
	}
	else
	{
		return;
	}
}

static void register_deal(north_instruction_data_t *north_in_data)
{
    switch (north_in_data->replay.code)
    {
    case REGISTER_SUCCESS:
        {
            uint64_t long_addr = 0;
            osel_memcpy(&long_addr, &north_in_data->dest_id[0], 8);
            if (judge_self_id(north_in_data->dest_id))
            {
                cnn_platform();	//网关与平台注册成功
                _app_info.heartbeat_send_num = 0;
            }
            else
            {
                reverse_id_endian((uint8_t *)&long_addr);
                rtb_device_register(long_addr);
            }
        }
        break;
    case REGISTER_FAIL:
    case REGISTER_ERROR_ID:
    case REGISTER_TIMEOUT:
        break;
    default:
        break;
    }
    
}

static void config_deal(north_instruction_data_t *north_in_data)
{
	device_info_t device_info = hal_board_info_look();
	config_t *config = &north_in_data->config;
	if (judge_self_id(north_in_data->dest_id))
	{
		//配置是发给自己的
		uint16_t result = C_SUCCESS;
		switch (config->code)
		{
		case CONFIG_HEARTBEAT_CYCLE:
			device_info.heartbeat_cycle = config->heart_cycle;
			hal_board_info_save(&device_info, FALSE);
			heartbeat_reset();
			break;
		default:
			result = C_UNSUCCESS;
			break;
		}
		replay_deal(north_in_data, config->code, result);
	}
	else
	{
		if (!find_device(north_in_data))
		{
			replay_deal(north_in_data, config->code, C_DEVICE_NOFIND);
			return ;
		}
		if (north_in_data->config.code == CONFIG_ERROR)
		{
			replay_deal(north_in_data, config->code, C_UNSUCCESS);
			return ;
		}
		sbuf_t *sbuf = south_config_package(north_in_data);
		if (sbuf != NULL)
		{
			//加入缓存队列
			if (add_replay(north_in_data, config->code))
			{
				osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
				replay_deal(north_in_data, config->code, C_SEND);
			}
			else
			{
				pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
				sbuf_free(&sbuf __SLINE2);
			}
		}
		else
		{
			replay_deal(north_in_data, config->code, C_UNSUCCESS);
		}
	}
}

static void rest_deal(north_instruction_data_t *north_in_data)
{
	if (judge_self_id(north_in_data->dest_id))
	{
		hal_board_reset();
	}
	else
	{
		send_down(north_in_data);
	}
}

static void select_self(north_instruction_data_t *north_in_data)
{
	select_command_t *cmd = &north_in_data->select_command;
	if (cmd->select_type == S_STATE)
	{
		if (cmd->select_range == 0)
		{
			device_info_t device_info = hal_board_info_look();
			select_state_reply_t state;
			osel_memset(&state, 0x00, sizeof(select_state_reply_t));
			state.code = north_in_data->select_command;
			osel_memset(&state.device_associate[0], 0xff, 8);
			state.device_electricity = 0x64;
			state.data_cycle = 0;
			state.heartbeat_cycle = device_info.heartbeat_cycle;
            
			state.data_cycle = S2B_UINT32(state.data_cycle);
			state.heartbeat_cycle = S2B_UINT32(state.heartbeat_cycle);
			uint16_t pload_len = sizeof(select_state_reply_t);
			north_instruction_data_t instruction = north_select_frame_replay
                (north_in_data, pload_len);
			instruction.pload = (uint8_t*) &state.code;
			north_frame_send(&instruction);
		}
		else
		{
			//查询下属设备
			return ;
		}
	}
	else
	{
		//网关没有数据
		return ;
	}
}

static void select_deal(north_instruction_data_t *north_in_data)
{
	if (judge_self_id(north_in_data->dest_id))
	{
		select_self(north_in_data);
	}
	else
	{
		if (!find_device(north_in_data))
		{
			uint16_t code = 0xffff;
			replay_deal(north_in_data, code, C_DEVICE_NOFIND);
			return ;
		}
		sbuf_t *sbuf = south_select_package(north_in_data);
		if (sbuf != NULL)
		{
			if (add_replay(north_in_data, north_in_data->type))
			{
				osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
			}
			else
			{
				pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
				sbuf_free(&sbuf __SLINE2);
			}
		}
	}
}

static void link_test_deal(north_instruction_data_t *north_in_data)
{
	if (judge_self_id(north_in_data->dest_id))
	{
#include <n2a_frame_package.h>
		n2a_frame_heartbeat_package();
	}
	else
	{
		send_down(north_in_data);
	}
}

static void app_data_deal(north_instruction_data_t *north_in_data)
{
	if (judge_self_id(north_in_data->dest_id))
	{
		return ;
	}
	else
	{
		send_down(north_in_data);
	}
}

void north_deal_interface(north_instruction_data_t *north_in_data)
{
	switch (north_in_data->type &GET_DATA_TYPE)
	{
	case NM_HEARTBEAT:
		_app_info.heartbeat_send_num = 0;
		break;
	case NM_APP_DATA:
		app_data_deal(north_in_data); //OK
		break;
	case NM_REGISTER:
		register_deal(north_in_data); //OK
		break;
	case NM_CONFIG:
		config_deal(north_in_data);	//OK
		break;
	case NM_SELECT:
		select_deal(north_in_data);	//OK
		break;
	case NM_LINK_TEST:
		link_test_deal(north_in_data); //OK
		break;
	case NM_DEVICE_REST:
		rest_deal(north_in_data); //OK
		break;
	case NM_SELECT_EXTEND:
		break;
	default:
		break;
	}
}
