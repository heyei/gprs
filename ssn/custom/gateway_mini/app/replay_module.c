#include <replay_module.h>
#include <hal_timer.h>
#define REPLAY_NUM      (5u)
#define MAX_TIME        (30000u)    //应答信息最大存在时间(毫秒)

static replay_info_t replay_info[REPLAY_NUM];

static void node_clear(replay_info_t *node)
{
    node->nwk_addr = 0;
    node->seq = 0;
    node->code = CONFIG_HEARTBEAT_CYCLE;
    node->h_time = 0;
    node->used = TRUE;
}

static void clear_replay()
{
    fp32_t time_stamp = 0;
    for (int i = 0; i < REPLAY_NUM; i++)
    {
        if (replay_info[i].used == FALSE)
        {
            time_stamp = (TICK_TO_US(replay_info[i].h_time)) / 1000.0;
            if (time_stamp >= MAX_TIME)
            {
                node_clear(&replay_info[i]);
            }
        }
    }
}

bool_t replay_list_get_remove(uint16_t nwk_addr, replay_info_t *node)
{
    for (int i = 0; i < REPLAY_NUM; i++)
    {
        if (replay_info[i].used == FALSE)
        {
            if (replay_info[i].nwk_addr == nwk_addr)
            {
                node->seq = replay_info[i].seq;
                node->code = replay_info[i].code;
                node_clear(&replay_info[i]);
                return TRUE;
            }
        }
    }
    return FALSE;
}


bool_t replay_list_add(replay_info_t node)
{
    for (int i = 0; i < REPLAY_NUM; i++)
    {
        if (replay_info[i].used == TRUE)
        {
            replay_info[i] = node;
            replay_info[i].h_time = hal_timer_now().w;
            replay_info[i].used = FALSE;
            return TRUE;
        }
    }
    //缓存队列已经满了
    clear_replay();
    for (int i = 0; i < REPLAY_NUM; i++)
    {
        if (replay_info[i].used == TRUE)
        {
            replay_info[i] = node;
            replay_info[i].h_time = hal_timer_now().w;
            replay_info[i].used = FALSE;
            return TRUE;
        }
    }
    return FALSE;
}

void replay_module_init(void)
{
    for (int i = 0; i < REPLAY_NUM; i++)
    {
        node_clear(&replay_info[i]);
    }
}
