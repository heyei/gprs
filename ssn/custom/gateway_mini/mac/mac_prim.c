#include <stack.h>
#include <lib.h>
#include <wsnos.h>
#include <mac_pib.h>
#include <mac_module.h>
#include <mac_frames.h>
#include <mac_global_variables.h>
#include <assoc_table.h>
#include <phy_packet.h>
#include <hal_board.h>

static void mac_fill_resend_datafrm_pbuf(sbuf_t *sbuf)
{
    uint8_t beacon_seq_num = 0;
    uint16_t pib_short_addr = 0;
    uint64_t pib_long_addr = 0;
	n2m_data_request_t *data_req = 
	&(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);	

	DBG_ASSERT(sbuf != NULL __DBG_LINE);
	DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

	pbuf_t *pbuf = sbuf->primargs.pbuf;
	pbuf->data_p = pbuf->head;
	pbuf->data_p += PHY_HEAD_SIZE;
	
	mac_frm_ctrl_t mac_ctl;
    
    mac_ctl.frm_type        = MAC_FRAME_TYPE_DATA;
	mac_ctl.sec_enable      = FALSE;
	mac_ctl.frm_pending     = FALSE;
	mac_ctl.ack_req         = TRUE;
	mac_ctl.des_addr_mode   = data_req->dst_mode; 
	mac_ctl.src_addr_mode   = data_req->src_mode;
	mac_ctl.reseverd 		 = 0;

	osel_memcpy(pbuf->data_p, &mac_ctl, sizeof(mac_frm_ctrl_t));
	pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    beacon_seq_num = get_pib_beacon_seq_num()-1;
	osel_memcpy(pbuf->data_p, &beacon_seq_num, sizeof(uint8_t));
	
	pbuf->data_p += MAC_HEAD_SEQ_SIZE;

	if(mac_ctl.des_addr_mode == MAC_MHR_ADDR_MODE_SHORT)
	{
        if(list_empty(&pend_addr_list_free[0]))
        {
            if(sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&sbuf->primargs.pbuf __PLINE2);
            }
            return;             //����7����
        }
		osel_memcpy(pbuf->data_p, &(data_req->dst_addr), MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
        pbuf->attri.dst_id = (uint16_t )(data_req->dst_addr);
        if(assoc_table_short_addr_find_device_type((uint16_t )(data_req->dst_addr)) == NODE_TYPE_TAG)
        {
            pend_short_addr_list_t *pend_short_addr;  
            pend_short_addr = list_entry_decap(&pend_addr_list_free[0], pend_short_addr_list_t, list);
            pend_short_addr->short_addr = (uint16_t )(data_req->dst_addr);
            list_add_to_tail(&pend_short_addr->list, &pend_addr_list_alloc[0]);
        }
	}
	else if(mac_ctl.des_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
        if(list_empty(&pend_addr_list_free[1]))
        {
            if(sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&sbuf->primargs.pbuf __PLINE2);
            }
            return;             //����7����
        }
		osel_memcpy(pbuf->data_p, &(data_req->dst_addr), MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
        pbuf->attri.dst_id = MAC_BROADCAST_ADDR;
        if(assoc_table_long_addr_find_device_type(data_req->dst_addr) == NODE_TYPE_TAG)
        {
            pend_long_addr_list_t *pend_long_addr;
            pend_long_addr = list_entry_decap(&pend_addr_list_free[1], pend_long_addr_list_t, list);  
            pend_long_addr->long_addr = data_req->dst_addr;
            list_add_to_tail(&pend_long_addr->list, &pend_addr_list_alloc[1]);
        }   
	}
    
    pib_short_addr = read_pib_short_addr();
    pib_long_addr = read_pib_long_addr();
	if(mac_ctl.src_addr_mode == MAC_MHR_ADDR_MODE_SHORT)
	{
		osel_memcpy(pbuf->data_p, &pib_short_addr, MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}
	else if(mac_ctl.src_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy(pbuf->data_p, &pib_long_addr, MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
	}

    pbuf->attri.seq = beacon_seq_num;
	pbuf->attri.already_send_times.mac_send_times++;
	pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.need_ack = TRUE;
    pbuf->data_len = pbuf->data_p - (pbuf->head+PHY_HEAD_SIZE) + MAC_FCS_SIZE;
}

static void mac_fill_datafrm_pbuf(sbuf_t *sbuf)
{
    uint8_t beacon_seq_num = 0;
    uint16_t pib_short_addr = 0;
    uint64_t pib_long_addr = 0;
	n2m_data_request_t *data_req = 
	&(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);	

	DBG_ASSERT(sbuf != NULL __DBG_LINE);
	DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

	pbuf_t *pbuf = sbuf->primargs.pbuf;
	pbuf->data_p = pbuf->head;
	pbuf->data_p += PHY_HEAD_SIZE;
	
	mac_frm_ctrl_t mac_ctl;
    
    mac_ctl.frm_type        = MAC_FRAME_TYPE_DATA;
	mac_ctl.sec_enable      = FALSE;
	mac_ctl.frm_pending     = FALSE;
	mac_ctl.ack_req         = TRUE;
	mac_ctl.des_addr_mode   = data_req->dst_mode;
	mac_ctl.src_addr_mode   = data_req->src_mode;
	mac_ctl.reseverd 		 = 0;

	osel_memcpy(pbuf->data_p, &mac_ctl, sizeof(mac_frm_ctrl_t));
	pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    beacon_seq_num = get_pib_beacon_seq_num();
    operation_pib_beacon_seq_num(1);
	osel_memcpy(pbuf->data_p, &beacon_seq_num, sizeof(uint8_t));
	
	pbuf->data_p += MAC_HEAD_SEQ_SIZE;

	if(mac_ctl.des_addr_mode == MAC_MHR_ADDR_MODE_SHORT)
	{
        if(list_empty(&pend_addr_list_free[0]))
        {
            if(sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&sbuf->primargs.pbuf __PLINE2);
            }
            return;             //����7����
        }
		osel_memcpy(pbuf->data_p, &(data_req->dst_addr), MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
        pbuf->attri.dst_id = (uint16_t)(data_req->dst_addr);
        if(assoc_table_short_addr_find_device_type((uint16_t)(data_req->dst_addr)) == NODE_TYPE_TAG)
        {
            pend_short_addr_list_t *pend_short_addr;
            pend_short_addr = list_entry_decap(&pend_addr_list_free[0], pend_short_addr_list_t, list);
            pend_short_addr->short_addr = (uint16_t)data_req->dst_addr;
            list_add_to_tail(&pend_short_addr->list, &pend_addr_list_alloc[0]);
        }

	}
	else if(mac_ctl.des_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
        if(list_empty(&pend_addr_list_free[1]))
        {
            if(sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&sbuf->primargs.pbuf __PLINE2);
            }
            return;             //����7����
        }
		osel_memcpy(pbuf->data_p, &(data_req->dst_addr), MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
        pbuf->attri.dst_id = MAC_BROADCAST_ADDR;                    //�豸����ǰphy���㲥��
        if(assoc_table_long_addr_find_device_type(data_req->dst_addr) == NODE_TYPE_TAG)
        {
            pend_long_addr_list_t *pend_long_addr;
            pend_long_addr = list_entry_decap(&pend_addr_list_free[1], pend_long_addr_list_t, list);
            pend_long_addr->long_addr = data_req->dst_addr;
            list_add_to_tail(&pend_long_addr->list, &pend_addr_list_alloc[1]);
        }
	}
    pib_short_addr = read_pib_short_addr();
    pib_long_addr = read_pib_long_addr();
	if(mac_ctl.src_addr_mode == MAC_MHR_ADDR_MODE_SHORT)
	{
		osel_memcpy(pbuf->data_p, &pib_short_addr, MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}
	else if(mac_ctl.src_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy(pbuf->data_p, &pib_long_addr, MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
	}
    
    pbuf->attri.seq = beacon_seq_num;
    pbuf->data_p += data_req->msdu_length;              //ƫ�Ƶ������غ����
	pbuf->attri.already_send_times.mac_send_times= 0;
	pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.need_ack = TRUE;
    pbuf->data_len = pbuf->data_p - (pbuf->head+PHY_HEAD_SIZE) + MAC_FCS_SIZE;
}

static void send_data_forwards_in_queue(sbuf_t *sbuf)
{
    supf_spec_t supf_cfg;
    n2m_data_request_t *n2m_data = NULL;
    uint8_t dev_type;
    n2m_data = &(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);
    supf_cfg = read_pib_supf_cfg_arg();
    uint8_t intercom_index = read_pib_intercom_index();
    if(n2m_data->dst_mode == MAC_MHR_ADDR_MODE_LONG)
    {
        dev_type = assoc_table_long_addr_find_device_type(n2m_data->dst_addr);
    }
    if(n2m_data->dst_mode == MAC_MHR_ADDR_MODE_SHORT)
    {
        dev_type = assoc_table_short_addr_find_device_type(n2m_data->dst_addr);
    }
    
    if(dev_type == ADDRESS_FALSE)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
    }
    
    if(dev_type == NODE_TYPE_ROUTER)
    {
        sbuf->slot_seq = srandom(0, supf_cfg.inter_gts_number[intercom_index-1]-1);
        list_insert_backwards(&sbuf->list, &inter_gts_buf);
    }
    else
    {
        list_insert_backwards(&sbuf->list, &intra_gts_buf);
        list_count(&intra_gts_buf, sbuf->slot_seq);
    }
}

static void send_data_backwards_in_queue(sbuf_t *sbuf)
{
    supf_spec_t supf_cfg;
    n2m_data_request_t *n2m_data = NULL;
    uint8_t dev_type;
    n2m_data = &(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);
    supf_cfg = read_pib_supf_cfg_arg();
    uint8_t intercom_index = read_pib_intercom_index();
    if(n2m_data->dst_mode == MAC_MHR_ADDR_MODE_LONG)
    {
        dev_type = assoc_table_long_addr_find_device_type(n2m_data->dst_addr);
    }
    if(n2m_data->dst_mode == MAC_MHR_ADDR_MODE_SHORT)
    {
        dev_type = assoc_table_short_addr_find_device_type(n2m_data->dst_addr);
    }
    if(dev_type == ADDRESS_FALSE)
    {
        if(sbuf->primargs.pbuf != NULL)
        {
            pbuf_free(&sbuf->primargs.pbuf __SLINE2);
            if(sbuf != NULL)
            {
                sbuf_free(&sbuf __SLINE2);
            }
            return;
        }
    }
    if(dev_type == NODE_TYPE_ROUTER)
    {
        sbuf->slot_seq = srandom(0, supf_cfg.inter_gts_number[intercom_index-2]-1);
        list_add_to_tail(&sbuf->list, &inter_gts_buf);
    }
    else
    {
        uint8_t assoc_cnt = 0;
        uint8_t data_cnt = 0;
        if(!list_empty(&assoc_buf))
        {
            list_count(&assoc_buf, assoc_cnt);
        }
        if(!list_empty(&intra_gts_buf))
        {
            list_count(&intra_gts_buf, data_cnt);
            if(data_cnt > 7)
            {
                data_cnt = data_cnt - 7;
            }
            sbuf->slot_seq = data_cnt + assoc_cnt;
        }
        list_add_to_tail(&sbuf->list, &intra_gts_buf);
    }
    
}

static void mac_data_request(sbuf_t *sbuf)
{
 	mac_fill_datafrm_pbuf(sbuf);
    if(sbuf->primargs.pbuf == NULL)
    {
        if(sbuf != NULL)
        {
            sbuf_free(&sbuf __SLINE2);
        }
        return;
    }
	send_data_backwards_in_queue(sbuf);
}

static void mac_resend_handle(sbuf_t *sbuf)
{
   mac_fill_resend_datafrm_pbuf(sbuf);
   if(sbuf->primargs.pbuf == NULL)
    {
        if(sbuf != NULL)
        {
            sbuf_free(&sbuf __SLINE2);
        }
        return;
    }
   send_data_forwards_in_queue(sbuf);
}

static void mac_prim_handler(sbuf_t *sbuf)
{
	DBG_ASSERT(sbuf != NULL __DBG_LINE);
	switch(sbuf->primtype)
	{
	case N2M_DATA_REQUEST:
		mac_data_request(sbuf);
		break;
        
    case M2M_DATA_RESEND:
        mac_resend_handle(sbuf);
        
	default:
		break;
	}
}

void mac_prim_event_handler(const event_block_t *const pmsg)
{
	DBG_ASSERT(pmsg != NULL __DBG_LINE);
	switch(pmsg->sig)
	{
	case M_MAC_PRIM_EVENT:
		mac_prim_handler((sbuf_t *)(pmsg->param));
		break;

	default:
		break;
	}
}

void mac_data_resend(sbuf_t *sbuf)
{
    if(sbuf->primargs.pbuf->attri.already_send_times.mac_send_times < read_pib_resend_max_num())
    {
        mac_resend_handle(sbuf);
    }
}

void mac_prim_data_indicate(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);
    osel_post( MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void m_prim_init(void)
{
    module_bind_event(mac_prim_event_handler, M_MAC_PRIM_EVENT);
}