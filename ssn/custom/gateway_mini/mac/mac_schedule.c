#include <stack.h>
#include <lib.h>
#include <hal.h>
#include <mac_module.h>
#include <mac_frames.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <mac_cmd_frame.h>
#include <mac_prim.h>
#include <mac_global_variables.h>
#include <mac_pib.h>

#define MAC_BASE_SLOT_DURATION          (2.5)
#define SLEEP_DURAION_FALSE             (100u)
#define SEQ_FALSE                       (0xff)

static slot_cfg_t   mac_beacon_slot, intra_gts_slot, mac_super_frame_slot,     \
mac_intracom_slot, mac_sub_intercom_slot[MAX_HOP_NUM],     \
mac_intercom_slot, sleep_slot, mac_beacon_interval,        \
inter_gts_slot[MAX_HOP_NUM],mac_intra_sub_slot;

static uint8_t locate_intra_so_seq = 0xff;
static uint8_t locate_inter_so_seq = 0;

uint32_t mac_beacon_interval_cycle(void)
{
    return (TICK_TO_US(mac_beacon_interval.slot_duration)/1000);
}

/***
**  获取此时在第几个超帧序列号
***/
uint8_t get_now_gts_seq(void)
{
    uint8_t gts_seq = 0;
    uint8_t intra_com_num = 0;
    uint8_t inter_com_num = 0;
    uint8_t cluster_slot_num = mac_beacon_slot.slot_repeat_cnt
                               + intra_gts_slot.slot_repeat_cnt;
    intra_com_num = mac_super_frame_slot.slot_repeat_cnt * cluster_slot_num;
    for (uint8_t i = 0; i < MAX_HOP_NUM; i++)
    {
        inter_com_num += mac_sub_intercom_slot[i].slot_repeat_cnt;
    }
    uint8_t slot_seq = m_slot_get_seq();

    if (slot_seq > intra_com_num)
    {
        slot_seq = slot_seq - intra_com_num;
        for (uint8_t i = 0; i < MAX_HOP_NUM; i++)
        {
            if (slot_seq > mac_sub_intercom_slot[i].slot_repeat_cnt)
            {
                slot_seq = slot_seq - mac_sub_intercom_slot[i].slot_repeat_cnt;
            }
            else
            {
                gts_seq = slot_seq;
            }
        }
    }
    else
    {
        gts_seq = (slot_seq % cluster_slot_num) - mac_beacon_slot.slot_repeat_cnt;
    }
    return gts_seq - 1 ;
}

/***
**  gts_txok回调函数
***/
void gts_txok_cb(sbuf_t *sbuf, bool_t res)
{
    if (res == FALSE)                                                               //发送失败处理
    {
        if ((sbuf->orig_layer == NWK_LAYER) || (sbuf->orig_layer == APP_LAYER))     //mac层以上的帧处理
        {
            sbuf->primtype = M2N_DATA_RESEND;
            osel_post( MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);                //待修改发往nwk层
        }
        else
        {
            //mac层以下的帧处理
            DBG_ASSERT((sbuf != NULL) && (sbuf->primargs.pbuf != NULL) __DBG_LINE);
            if (sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&sbuf->primargs.pbuf __PLINE2);
            }
            if (sbuf != NULL)
            {
                sbuf_free(&sbuf __SLINE2);
            }
            return;
        }
    }
    else
    {
        //发送成功处理
        DBG_ASSERT((sbuf != NULL) && (sbuf->primargs.pbuf != NULL) __DBG_LINE);
        if (sbuf->primargs.pbuf != NULL)
        {
            pbuf_free(&sbuf->primargs.pbuf __PLINE2);
        }
        if (sbuf != NULL)
        {
            sbuf_free(&sbuf __SLINE2);
        }
    }
}

/***
**  整个大超帧时隙处理函数
***/
static void gl_super_frame_handle(void *seq_p)
{
    last_beacon_map = loc_beacon_map;
    loc_beacon_map = 0;
    alloc_bcn_bitmap = 0x0001;
    locate_inter_so_seq = 0;
}

/***
**  sleep时隙处理函数
***/
static void sleep_slot_handle(void *seq_p)
{
    m_tran_sleep();
    hal_board_info_delay_save();
}



static void mac_intra_sub_slot_handle(void *seq_p)
{
    ;
}
/***
**  簇内设备单元时隙处理函数
***/
static void super_frame_slot_handle(void *seq_p)
{
    locate_intra_so_seq = *(uint8_t *)seq_p + 1;
}

/***
**  簇间设备单元时隙处理函数
***/
static void mac_sub_intercom_slot_handle(void *seq_p)
{
    locate_inter_so_seq ++;
}

static void mac_intercom_slot_handle(void *seq_p)
{
//    hal_led_open(BLUE);
}

static void mac_intracom_slot_handle(void *seq_p)
{
    ;
}

/***
**  beacon时隙处理函数
***/
static void beacon_slot_handle(void *seq_p)
{
    if (locate_intra_so_seq == read_pib_super_frame_index())
    {
        delay_us(1500);
        mac_send_beacon();
//        hal_led_open(BLUE);
    }
    else
    {
        if (read_pib_mac_over_hear_flag())
        {
            m_tran_recv();
        }
        else
        {
            m_tran_sleep();
        }
    }
    hal_led_toggle(RED);
}

/***
**  簇间GTS时隙处理函数
***/
static void inter_gts_slot_handle(void *seq_p)
{
    uint8_t resend_num = read_pib_resend_max_num();
    uint8_t seq = *(uint8_t *)seq_p;
    sbuf_t *sbuf_data = NULL;
    if (locate_inter_so_seq == read_pib_intercom_index())
    {
        if (!list_empty(&assoc_buf))
        {
            sbuf_data = list_entry_decap(&assoc_buf, sbuf_t, list);
            delay_us(1100);
            m_tran_send(sbuf_data, gts_txok_cb, 1);
        }
        else
        {
            m_tran_recv();
        }
    }
    else if (locate_inter_so_seq == (read_pib_intercom_index() - 1))
    {
        sbuf_t *pos1 = NULL;
        sbuf_t *pos2 = NULL;
        hal_int_state_t s = 0;
        HAL_ENTER_CRITICAL(s);
        list_entry_for_each_safe( pos1, pos2 , &inter_gts_buf, sbuf_t, list)
        {
            if(pos1 != NULL && seq == pos1->slot_seq)
            {
                break;
            }
        }
        HAL_EXIT_CRITICAL(s);
        if (pos1 != NULL && seq == pos1->slot_seq)
        {
            list_del(&pos1->list);
            delay_us(1100);
            m_tran_send(pos1, gts_txok_cb, 1);
        }
        else
        {
            m_tran_sleep();
        }
    }
    else
    {
        m_tran_sleep();
    }
}

/***
**  簇内GTS时隙处理函数
***/
static void intra_gts_slot_handle(void *seq_p)
{
    uint8_t resend_num = read_pib_resend_max_num();
    uint8_t seq = *(uint8_t *)seq_p;
    supf_spec_t  supf_cfg;
    supf_cfg = read_pib_supf_cfg_arg();
    m_tran_stop();
    if (locate_intra_so_seq == read_pib_super_frame_index())
    {
        sbuf_t *sbuf_data = NULL;
        if (!list_empty(&assoc_buf))
        {
            sbuf_data = list_entry_decap(&assoc_buf, sbuf_t, list);
            delay_us(1100);
            m_tran_send(sbuf_data, gts_txok_cb, 1);
        }
        else
        {
            sbuf_data = list_entry_addr_find(list_first_elem_look(&intra_gts_buf),
                                             sbuf_t, list);
            if (sbuf_data != NULL && seq == sbuf_data->slot_seq)
            {
                list_del(&sbuf_data->list);
                delay_us(1100);
                m_tran_send(sbuf_data, gts_txok_cb, 1);
            }
            else
            {
                m_tran_recv();
            }
        }
    }
    else
    {
        m_tran_sleep();
    }
    if (seq == (supf_cfg.intra_gts_number - 1))
    {
        bcn_aging_update();
    }
}

/***
**  时隙单元配置
***/
static void slot_node_cfg(slot_cfg_t *node,
                          uint32_t duration,
                          uint8_t repeat_cnt,
                          func_t func,
                          slot_cfg_t *parent,
                          slot_cfg_t *first_child,
                          slot_cfg_t *next_sibling)
{
    node->slot_duration = duration;
    node->slot_repeat_cnt = repeat_cnt;
    node->func = func;
    node->parent = parent;
    node->first_child = first_child;
    node->next_sibling = next_sibling;
    node->slot_start = 0;
    node->slot_repeat_seq = 0;
}

/***
**  配置超帧结构
***/
void super_frame_cfg(void)
{
    supf_spec_t  supf_cfg;
    supf_cfg = read_pib_supf_cfg_arg();
    uint32_t sleep_duration = SLEEP_DURAION_FALSE;
    uint32_t mac_beacon_interval_false;
    uint32_t beacon_duration = MS_TO_TICK(MAC_BASE_SLOT_DURATION
                                          * supf_cfg.beacon_duration_order);
    uint32_t beacon_interval = MS_TO_TICK(MAC_BASE_SLOT_DURATION *
                                          ((uint32_t )0x01 << supf_cfg.beacon_interv_order));
    uint32_t gts_duration = MS_TO_TICK(MAC_BASE_SLOT_DURATION
                                       * supf_cfg.gts_duration_order);

    uint8_t intra_gts_repeat_cnt = supf_cfg.intra_gts_number;
    uint8_t cluster_number =  supf_cfg.cluster_number;
    uint8_t inter_gts_repeat_cnt[MAX_HOP_NUM];

    for (uint8_t i = 0; i < MAX_HOP_NUM; i++)
    {
        inter_gts_repeat_cnt[i] = supf_cfg.inter_gts_number[i];
    }
    uint8_t intercom_repeat_number =  supf_cfg.inter_unit_number;

    slot_node_cfg(&mac_beacon_slot, beacon_duration, 1,
                  &beacon_slot_handle, &mac_super_frame_slot,
                  NULL, &mac_intra_sub_slot);
    slot_node_cfg(&mac_intra_sub_slot, 0, 1,
                  &mac_intra_sub_slot_handle, &mac_super_frame_slot,
                  &intra_gts_slot, NULL);
    slot_node_cfg(&intra_gts_slot, gts_duration, intra_gts_repeat_cnt,
                  &intra_gts_slot_handle, &mac_intra_sub_slot, NULL, NULL);
    slot_node_cfg(&mac_super_frame_slot, 0, cluster_number,
                  &super_frame_slot_handle, &mac_intracom_slot,
                  &mac_beacon_slot, NULL);
    slot_node_cfg(&mac_intracom_slot, 0, 1, &mac_intracom_slot_handle,
                  &mac_beacon_interval, &mac_super_frame_slot,
                  &mac_intercom_slot);
    for (uint8_t i = 0; i < intercom_repeat_number; i++)
    {
        slot_node_cfg(&inter_gts_slot[i],
                      gts_duration,
                      inter_gts_repeat_cnt[i],
                      &inter_gts_slot_handle,
                      &mac_sub_intercom_slot[i],
                      NULL,
                      NULL);
    }
    for (uint8_t i = 0; i < intercom_repeat_number; i++)
    {
        if (i == intercom_repeat_number - 1)
        {
            slot_node_cfg(&mac_sub_intercom_slot[i], 0, 1,
                          &mac_sub_intercom_slot_handle,
                          &mac_intercom_slot, &inter_gts_slot[i], NULL);
        }
        else
        {
            slot_node_cfg(&mac_sub_intercom_slot[i], 0, 1,
                          &mac_sub_intercom_slot_handle,
                          &mac_intercom_slot, &inter_gts_slot[i],
                          &mac_sub_intercom_slot[i + 1]);
        }
    }
    slot_node_cfg(&mac_intercom_slot, 0, 1, &mac_intercom_slot_handle,
                  &mac_beacon_interval, &mac_sub_intercom_slot[0],
                  &sleep_slot);
    slot_node_cfg(&sleep_slot, sleep_duration, 1, &sleep_slot_handle,
                  &mac_beacon_interval, NULL, NULL);
    slot_node_cfg(&mac_beacon_interval, 0, 0, &gl_super_frame_handle,
                  NULL, &mac_intracom_slot, NULL);
    m_slot_cfg(&mac_beacon_interval, SLOT_LOCAL_TIME);                                      //利用时隙模块计算簇内+簇间+错误的sleep的时间
    mac_beacon_interval_false = mac_beacon_interval.slot_duration;                          //加了错误睡眠时间的大超帧时间
    mac_beacon_interval.slot_duration = beacon_interval;                                    //修正大超帧时间
    sleep_slot.slot_duration = mac_beacon_interval.slot_duration                            //计算出正确的sleep时隙的时间
                             - mac_beacon_interval_false + SLEEP_DURAION_FALSE;
}

/***
**  同步配置
***/
static void device_sync_cfg(void)
{
    sync_cfg_t cfg;
    cfg.background_compute = FALSE;
    cfg.sync_source = TRUE;
    cfg.sync_target = 0x00;

    cfg.flag_byte_pos = 0x03;
    cfg.flag_byte_msk = 0x07;
    cfg.flag_byte_val = MAC_FRAME_TYPE_BEACON;

    cfg.len_pos     = 0;
    cfg.len_modfiy  = TRUE;

    cfg.stamp_len   = 4;
    cfg.stamp_byte_pos = 0;

    cfg.tx_sfd_cap = FALSE;
    cfg.rx_sfd_cap = TRUE;

    cfg.tx_offset = 67;
    cfg.rx_offset = 0;

    m_sync_cfg(&cfg);
}

void sys_enter_lpm_handler(void *p)
{
//    LPM3;
}

/***
**  时隙表初始化
***/
void mac_schedule_init(void)
{
    if (mac_pib.supf_cfg_arg.beacon_interv_order == 0)        //pib属性表还未数据时不启动时隙
    {
        return;
    }
    hal_time_t now;

    device_sync_cfg();
    super_frame_cfg();

    now = hal_timer_now();
    now.w += MS_TO_TICK(3000);
    m_slot_run(&now);
    
    osel_idle_hook(sys_enter_lpm_handler);
}