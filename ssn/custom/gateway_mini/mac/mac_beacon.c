#include <lib.h>
#include <stack.h>
#include <hal_board.h>
#include <mac_frames.h>
#include <mac_beacon.h>
#include <mac_module.h>
#include <mac_global_variables.h>
#include <mac_pib.h>
#include <phy_packet.h>
#include <hal.h>

#pragma pack(1)
typedef struct
{
    uint8_t  index   :4,        /* 所使用的信标发送时隙 */
             hops    :4;        /* 距离sink的跳数 */
    uint32_t time_stamp;        /* 携带的时间戳 */
} bcn_payload_t;
#pragma pack()

typedef struct
{
    uint8_t short_addr_num      :   3,
            addr1_reserved      :   1,
            long_addr_num       :   3,
            addr2_reserved      :   1;
} pend_addr_spec_t;

static sbuf_t beacon_sbuf;

static uint8_t gts_seq_cnt = 0;

static void pend_addr_seq_clr(void)
{
    gts_seq_cnt = 0;
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    hal_int_state_t s = 0;
    HAL_ENTER_CRITICAL(s);
    list_entry_for_each_safe( pos1, pos2 , &intra_gts_buf, sbuf_t, list)
    {
        if(pos1 != NULL)
        {
            pos1->slot_seq = gts_seq_cnt++;
        }
    }
    HAL_EXIT_CRITICAL(s);
}

/**
** 信标帧发送完成回调
**/
static void beacon_tx_done(sbuf_t* packet,bool_t result)
{    
    if(result == TRUE)
    {
        ;
    }
    else
    {
        ;
    }
    pbuf_t *pbuf = packet->primargs.pbuf;
    osel_memset(pbuf->head, 0 ,pbuf->data_len);
}

static void beacon_pend_add_short_addr(uint8_t **p, uint8_t short_num)
{
    for(uint8_t i=0; i<short_num; i++)
    {
        pend_short_addr_list_t *pend_short_addr;
        pend_short_addr = list_entry_decap(&pend_addr_list_alloc[0], 
                                    pend_short_addr_list_t, list);
        osel_memcpy(*p, (uint8_t *)&(pend_short_addr->short_addr), MAC_ADDR_SHORT_SIZE);
        list_add_to_tail(&pend_short_addr->list, &pend_addr_list_free[0]);
        *p += MAC_ADDR_SHORT_SIZE;
    }
}

static void beacon_pend_add_long_addr(uint8_t **p, uint8_t long_num)
{
    for(uint8_t j=0; j<long_num; j++)
    {
        pend_long_addr_list_t *pend_long_addr;
        pend_long_addr = list_entry_decap(&pend_addr_list_alloc[1], 
                                       pend_long_addr_list_t, list);
        osel_memcpy(*p, (uint8_t *)&(pend_long_addr->long_addr), MAC_ADDR_LONG_SIZE);
        list_add_to_tail(&pend_long_addr->list, &pend_addr_list_free[1]);
        *p += MAC_ADDR_LONG_SIZE;
    }
}

/**
** 信标帧加pending地址
**/
static void beacon_pend_addr(uint8_t **p)
{
    uint8_t short_addr_num = 0;
    uint8_t long_addr_num = 0;
    pend_addr_spec_t pend_addr = {0,0,0,0};

    if(!list_empty(&pend_addr_list_alloc[0]))
    {
        list_count(&pend_addr_list_alloc[0],short_addr_num);
    }
    if(!list_empty(&pend_addr_list_alloc[1]))
    {
        list_count(&pend_addr_list_alloc[1],long_addr_num);
    }  
    if((0==short_addr_num) && (0==long_addr_num))
    {
        DBG_ASSERT(FALSE __DBG_LINE);
    }
    short_addr_num = short_addr_num>7? 7:short_addr_num;    //最大111为7
    long_addr_num = long_addr_num>7? 7:long_addr_num;
    
    pend_addr.short_addr_num = short_addr_num;
    pend_addr.long_addr_num = long_addr_num;
    osel_memcpy(*p, &pend_addr, sizeof(pend_addr_spec_t));   
    *p += sizeof(pend_addr_spec_t);  
    
    pend_addr_seq_clr();
    beacon_pend_add_short_addr(p, short_addr_num);
    beacon_pend_add_long_addr(p, long_addr_num);
}

/**
** 发送信标帧
**/
void mac_send_beacon(void)
{    
    bool_t pending_flag = TRUE;
    uint8_t beacon_seq_num = 0;
    uint8_t resend_num = read_pib_resend_max_num();
    pbuf_t *beacon_packet = beacon_sbuf.primargs.pbuf;    
    beacon_packet->data_p = beacon_packet->head + PHY_HEAD_SIZE;       
    mac_frm_ctrl_t *frm_ctrl = (mac_frm_ctrl_t *)(beacon_packet->data_p);     
   // TODO: pend功能后续更改
    if(list_empty(&pend_addr_list_alloc[0])
       &&list_empty(&pend_addr_list_alloc[1]))
    {
       pending_flag = FALSE; 
    }
    frm_ctrl->frm_type = MAC_FRAME_TYPE_BEACON;
    frm_ctrl->sec_enable = FALSE;    
    frm_ctrl->frm_pending = pending_flag;     
    frm_ctrl->ack_req = FALSE;
    frm_ctrl->reseverd = 0;
    frm_ctrl->des_addr_mode = MAC_MHR_ADDR_MODE_FALSE;
    frm_ctrl->src_addr_mode = MAC_MHR_ADDR_MODE_SHORT;

    beacon_packet->data_p += sizeof(mac_frm_ctrl_t);
    beacon_seq_num = get_pib_beacon_seq_num();
    operation_pib_beacon_seq_num(1);
    *(beacon_packet->data_p) = beacon_seq_num++; 
    beacon_packet->data_p += MAC_HEAD_SEQ_SIZE;
    uint16_t src_addr = read_pib_short_addr();
    osel_memcpy(beacon_packet->data_p, &src_addr, MAC_ADDR_SHORT_SIZE);
    beacon_packet->data_p += MAC_ADDR_SHORT_SIZE; 
    
    supf_spec_t supf_config = read_pib_supf_cfg_arg();
    uint8_t short_addr_num = 0;
    uint8_t long_addr_num = 0;
    uint8_t addr_num = 0;
    if(!list_empty(&pend_addr_list_alloc[0]))
    {
        list_count(&pend_addr_list_alloc[0],short_addr_num);
    }
    
    if(!list_empty(&pend_addr_list_alloc[1]))
    {
        list_count(&pend_addr_list_alloc[1],long_addr_num);
    }
    addr_num = short_addr_num + long_addr_num;
    addr_num = addr_num>7? 7:addr_num;
    supf_config.down_link_slot_length = short_addr_num + long_addr_num;
    
    osel_memcpy(beacon_packet->data_p, &supf_config, sizeof(supf_spec_t));    
    beacon_packet->data_p += sizeof(supf_spec_t);
    
    if(pending_flag)
    {
        beacon_pend_addr(&(beacon_packet->data_p));
    } 
    bcn_payload_t beacon_payload;
    beacon_payload.index = read_pib_super_frame_index();
    beacon_payload.hops = read_pib_intercom_index()-MAX_HOP_NUM+1;
    uint32_t start_time = m_slot_get_root_begin();
    beacon_payload.time_stamp = start_time;
    osel_memcpy(beacon_packet->data_p, &beacon_payload, sizeof(bcn_payload_t));    
    beacon_packet->data_p += sizeof(bcn_payload_t);
    
	beacon_packet->attri.need_ack = FALSE;
	beacon_packet->attri.already_send_times.mac_send_times= 0;
	beacon_packet->attri.send_mode = TDMA_SEND_MODE;  
    beacon_packet->attri.dst_id = MAC_BROADCAST_ADDR;

    /* 计算mac帧的总长度 */
	beacon_packet->data_len =  beacon_packet->data_p - beacon_packet->head - PHY_HEAD_SIZE;  
    uint8_t mac_resend_cnt = read_pib_resend_max_num();
    m_tran_send(&beacon_sbuf, beacon_tx_done, 1);
}

/****解析beacon帧******************************/
void recv_beacon(pbuf_t *pbuf)
{   
    bcn_payload_t *bcn_pld = NULL;
    bool_t pend_flag = FALSE;
    uint8_t *datap = NULL;
    
    uint8_t pend_list_len = 0;
    mac_frm_ctrl_t frm_ctrl;
    
    datap = pbuf->head + PHY_HEAD_SIZE;
    osel_memcpy(&frm_ctrl, datap, MAC_HEAD_CTRL_SIZE);
    pend_flag = frm_ctrl.frm_pending;

    datap += MAC_HEAD_CTRL_SIZE;
    datap += MAC_HEAD_SEQ_SIZE;
    
    uint16_t des_addr;
    osel_memcpy(&des_addr, datap, MAC_ADDR_SHORT_SIZE);
    datap += MAC_ADDR_SHORT_SIZE;    
    datap += sizeof(supf_spec_t);
    
    if(pend_flag == TRUE)
    {
        pend_list_len = pbuf->data_len - PHY_HEAD_SIZE - MAC_HEAD_CTRL_SIZE 
                        - MAC_HEAD_SEQ_SIZE - MAC_ADDR_SHORT_SIZE 
                        - sizeof(supf_spec_t) - sizeof(bcn_payload_t)
                        - MAC_FCS_SIZE; 
        datap += pend_list_len;
    }
    
    bcn_pld = (bcn_payload_t *)(datap);                      
    loc_beacon_map |= ((uint16_t)0x01<<(bcn_pld->index-1));
}

/* 生成一个不释放的pbuf并初始化信标有效期数组 */
void mac_beacon_init(void)
{
    pbuf_t *beacon_packet = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    
    DBG_ASSERT(beacon_packet != NULL __DBG_LINE);
    
    if (beacon_packet != NULL)
    {
        beacon_sbuf.primargs.pbuf = beacon_packet;
    }
}
