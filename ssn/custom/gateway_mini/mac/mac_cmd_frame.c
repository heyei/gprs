#include <lib.h>
#include <pbuf.h>
#include <stack.h>
#include <mac_global_variables.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>
#include <mac_pib.h>
#include <mac_schedule.h>
#include <mac_module.h>
#include <phy_packet.h>
#include <node_cfg.h>
#include <assoc_table.h>
#include <hal_board.h>

#define DEVICE_TYPE_MASK            0x0001u
#define TIMEB_BITMAP_MASK           0xFFF8u

static void make_assoc_response(uint64_t dev_addr, uint8_t dev_type, 
                                uint8_t index, uint8_t state)
{ 
/*
    当设备为anchor时，选择超帧序号为index的单元的随即GTS发送应答；
    当设备为Tag时，选择自己的测距交互时段发送应答，GTS时隙随即选择；   
*/
	pbuf_t *pbuf = NULL;
	sbuf_t *sbuf = NULL;
	mac_frm_ctrl_t mac_frm_ctrl;
    uint8_t beacon_seq_num= 0;
    uint16_t mac_pib_dst_addr = 0;
	pbuf = pbuf_alloc(32 __PLINE1);
	if(pbuf == NULL)
    {
       DBG_ASSERT(FALSE __DBG_LINE); 
    }
	pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;
	
	mac_frm_ctrl.frm_type        = MAC_FRAME_TYPE_COMMAND;
	mac_frm_ctrl.sec_enable      = FALSE;
	mac_frm_ctrl.frm_pending     = FALSE;
	mac_frm_ctrl.ack_req         = TRUE;
	mac_frm_ctrl.des_addr_mode   = MAC_MHR_ADDR_MODE_LONG; 
	mac_frm_ctrl.src_addr_mode   = MAC_MHR_ADDR_MODE_SHORT;
	mac_frm_ctrl.reseverd 		 = 0;

	osel_memcpy(pbuf->data_p, &mac_frm_ctrl, sizeof(mac_frm_ctrl_t));
	pbuf->data_p += MAC_HEAD_CTRL_SIZE;			
	beacon_seq_num = get_pib_beacon_seq_num();
    operation_pib_beacon_seq_num(1);
	osel_memcpy(pbuf->data_p, &beacon_seq_num, sizeof(uint8_t));
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;
	osel_memcpy(pbuf->data_p, &dev_addr, MAC_ADDR_LONG_SIZE);
	pbuf->data_p += MAC_ADDR_LONG_SIZE;
    mac_pib_dst_addr = read_pib_short_addr();
	osel_memcpy(pbuf->data_p, &mac_pib_dst_addr, MAC_ADDR_SHORT_SIZE);
	pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    *(uint8_t *)pbuf->data_p = MAC_CMD_ASSOC_RESP;
    pbuf->data_p += 1;
	mac_assoc_res_arg_t assoc_res_arg;
	assoc_res_arg.associat_status = state;
	assoc_res_arg.intra_cluster_index = index;
	osel_memcpy(pbuf->data_p, &assoc_res_arg, sizeof(mac_assoc_res_arg_t));
	pbuf->data_p += sizeof(mac_assoc_res_arg_t);

	// attribs
    pbuf->attri.seq = beacon_seq_num;
    pbuf->attri.need_ack = mac_frm_ctrl.ack_req;
    pbuf->attri.already_send_times.mac_send_times= 0;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.dst_id = MAC_BROADCAST_ADDR;                    //设备入网前phy给广播
    pbuf->data_len = pbuf->data_p - pbuf->head - PHY_HEAD_SIZE;
	
	sbuf = sbuf_alloc(__SLINE1);
    if(sbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE); 
    }
	sbuf->primargs.pbuf = pbuf;
    sbuf->orig_layer = MAC_LAYER;
    
	// 选择时隙;
    supf_spec_t supf_cfg;
    supf_cfg = read_pib_supf_cfg_arg();
    uint8_t intercom_index = read_pib_intercom_index();
    if(dev_type == NODE_TYPE_ROUTER)
    {
        sbuf->slot_seq = srandom(0, supf_cfg.inter_gts_number[intercom_index-1]-1);
        list_insert_backwards(&sbuf->list, &assoc_buf);
    }
    else
    {
        list_insert_backwards(&sbuf->list, &assoc_buf);
    }
}

static void assoc_req_handler(uint64_t src_addr, uint8_t *payload)
{
    mac_assoc_req_arg_t req_val;
    uint8_t dev_type = 0;
    uint16_t beacon_bitmap = 0;
    uint16_t beacon_res = 0;
    uint8_t assoc_st = ASSOC_STATUS_RESERVED;
    uint8_t i = 0;
    supf_spec_t supf_cfg;
    supf_cfg = read_pib_supf_cfg_arg();

    osel_memcpy(&req_val, payload, sizeof(req_val));
    dev_type = req_val.type;
 
    if(!assoc_table_device_add(src_addr, dev_type))      //更新关联表
    {
        assoc_st = ASSOC_STATUS_FULL;
        make_assoc_response(src_addr, dev_type, i+1, assoc_st);  
        return;
    }
    beacon_bitmap = req_val.bitmap;
    beacon_res = beacon_bitmap | last_beacon_map | loc_beacon_map;
    bcn_alloc_aging_map();
    alloc_bcn_bitmap |= last_beacon_map | loc_beacon_map;
    for (i = 0; i<supf_cfg.cluster_number; i++)
    {
        if(!(beacon_res &((uint16_t)0x01<<i)))
        {
            if(!(alloc_bcn_bitmap &((uint16_t)0x01<<i)))
            {
                alloc_bcn_bitmap |= ((uint16_t)0x01<<i);
                bcn_aging_set_map(i);
                break;
            }
        }
    }

    if(i == supf_cfg.cluster_number)
    {
        assoc_st = ASSOC_STATUS_FULL;
        
        if(dev_type == NODE_TYPE_ROUTER)
        {
            for (i = 0; i<BEACON_AGE_ARRAY_NUM; i++)
            {
                if(!(beacon_bitmap &((uint16_t)0x01<<i)))
                {
                    break;
                }
            }
        
            if( i==supf_cfg.cluster_number )
            {
                i = srandom(0, supf_cfg.cluster_number-1);
            }
        }
        else
        {
            assoc_st = ASSOC_STATUS_SUCCESS;
            i = read_pib_super_frame_index() - 1;
        }
    }
    else
    {
        assoc_st = ASSOC_STATUS_SUCCESS;
        if(dev_type == NODE_TYPE_TAG)
        {
            i = read_pib_super_frame_index() - 1;
        }
    }
    make_assoc_response(src_addr, dev_type, i+1, assoc_st);  
}

static void mac_assoc_request_frame_parse(uint64_t dev_addr, void *payload)
{
	//给出关联应答
	assoc_req_handler(dev_addr, payload);
}

void mac_cmd_frame_parse(pbuf_t *pbuf)
{
    uint8_t frm_type = MAC_CMD_RESERVED;    
	uint8_t *payload_info = NULL;

	payload_info = (uint8_t *)(pbuf->head + PHY_HEAD_SIZE + mac_frm_head_info.mhr_size);
	
	frm_type = *(uint8_t *)(payload_info);
	payload_info++;

	switch (frm_type)
    {
    case MAC_CMD_ASSOC_REQ:
		// 处理关联请求帧
		mac_assoc_request_frame_parse(mac_frm_head_info.addr_info.src_addr, payload_info);
        break;
    case MAC_CMD_ASSOC_RESP:
		// 处理关联应答帧
        break;

    default:
        break;
    } 
}