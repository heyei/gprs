#include <assoc_table.h>
#include <mac_pib.h>
#include <mac_global_variables.h>
#include <lib.h>

#define FALSE_ADDR                  0xff
#define DEV_ASSOC_MAX_NUM           60

dev_assoc_t dev_assoc_table[DEV_ASSOC_MAX_NUM];
static uint8_t alloc_bcn_aging[BEACON_AGE_ARRAY_NUM] = {0};

void bcn_aging_update(void)
{
    for(uint8_t i=1;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		if(alloc_bcn_aging[i] > 0)
		{
			alloc_bcn_aging[i]--;
		}
	}
}

void bcn_alloc_aging_map(void)
{
    for(uint8_t i=1;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		if(alloc_bcn_aging[i] == 0)
		{
			alloc_bcn_bitmap &= ~(((uint16_t)0x01)<<i);
		}
		else
		{
			alloc_bcn_bitmap |= ((uint16_t)0x01)<<i;
		}
	}
}

void bcn_aging_set_map(uint8_t index)
{
    supf_spec_t supf_cfg_arg;
    supf_cfg_arg = read_pib_supf_cfg_arg();
    if(supf_cfg_arg.cluster_number != 0)    // stone beacon map resource 6 cycles
    {
        alloc_bcn_aging[index] = 6*supf_cfg_arg.cluster_number;
    }
	else
    {
        alloc_bcn_aging[index] = 6*CLUSTER_FRAME_NUM;
    }
}

bool_t assoc_table_device_updata_addr(uint64_t long_dev_addr, uint16_t short_dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].mac_long_addr == long_dev_addr)
        {
            dev_assoc_table[i].mac_short_addr = short_dev_addr;
            
            return TRUE;
        }
	}
    return FALSE;
}
bool_t assoc_table_device_add(uint64_t dev_addr, uint8_t dev_type)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].mac_long_addr == 0 || dev_assoc_table[i].mac_long_addr == dev_addr)   //地址没变,属性更新了,也更新关联表
        {
            dev_assoc_table[i].device_type = dev_type;
            dev_assoc_table[i].mac_long_addr = dev_addr;
            return TRUE;
        }
	}
    return FALSE;
}

bool_t assoc_table_device_del(uint64_t dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].mac_long_addr == dev_addr)
        {
            dev_assoc_table[i].device_type = 0;
            dev_assoc_table[i].mac_long_addr = 0;
            dev_assoc_table[i].mac_short_addr = 0;
            dev_assoc_table[i].assoc_device_inter_channel = 0;
            dev_assoc_table[i].assoc_device_intra_channel = 0;
            return TRUE;
        }
	}
    return FALSE;
}

uint8_t  assoc_table_short_addr_find_device_type(uint16_t dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].mac_short_addr == dev_addr)
        {
            return dev_assoc_table[i].device_type;
        }
	}
    return ADDRESS_FALSE;
}

uint8_t  assoc_table_long_addr_find_device_type(uint64_t dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].mac_long_addr == dev_addr)
        {
            return dev_assoc_table[i].device_type;
        }
	}
    return ADDRESS_FALSE;
}

void beacon_aging_init(void)
{
    for(uint8_t i=0;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		alloc_bcn_aging[i] = 0;
	}
}

void assoc_table_init(void)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		dev_assoc_table[i].device_type = 0;
        dev_assoc_table[i].mac_long_addr = 0;
        dev_assoc_table[i].mac_short_addr = 0;
        dev_assoc_table[i].assoc_device_inter_channel = 0;
        dev_assoc_table[i].assoc_device_intra_channel = 0;
	}
}
