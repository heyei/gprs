/***************************************************************************
* File        : mac_beacon.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <mac_beacon.h>
#include <mac_cmd_frame.h>
#include <mac_global.h>
#include <hal_timer.h>
#include <m_sync.h>
#include <m_slot.h>
#include <mac_schedule.h>
#include <mac_neighbors.h>
#include <mac_frames.h>
#include <hal_board.h>
#include <mac_moniter.h>

static bool_t pend_me_flag       = FALSE;
static uint8_t  lost_corrd_cnt   = 0;
static uint16_t local_beacon_map = 0x0000;
static uint16_t last_beacon_map  = 0x0000;
static superframe_spec_t superfrm_cfg_arg;

uint32_t beacon_interval_time = 0;

uint8_t inter_gts_num_array[MAX_HOPS];

static void mac_get_superframe_cfg_arg(pbuf_t *pbuf)
{
    osel_memcpy((uint8_t *)&superfrm_cfg_arg, pbuf->data_p, sizeof(superframe_spec_t));
    pbuf->data_p += sizeof(superframe_spec_t);

    if (mac_state.ms != MAIN_STATE_SCHEDULING)
    {
        mac_pib.supfrm_cfg_arg.bcn_interval_order = superfrm_cfg_arg.bcn_interval_order;
        mac_pib.supfrm_cfg_arg.bcn_duration_order = superfrm_cfg_arg.bcn_duration_order;

        mac_pib.supfrm_cfg_arg.gts_duration_order = superfrm_cfg_arg.gts_duration_order;

        mac_pib.supfrm_cfg_arg.cluster_num        = superfrm_cfg_arg.cluster_num;
        mac_pib.supfrm_cfg_arg.intra_gts_num      = superfrm_cfg_arg.intra_gts_num;

        mac_pib.supfrm_cfg_arg.inter_unit_num     = superfrm_cfg_arg.inter_unit_num;
    }
    else
    {
        mac_pib.supfrm_cfg_arg.downlink_slots_len = superfrm_cfg_arg.downlink_slots_len;
    }
    
    // 1 sec add for float change to int :( int(1.5) + 1 == 2 )
    beacon_interval_time =
        (uint32_t)(MAC_BASE_SLOT_DURATION * ((uint32_t)0x01 <<
                   mac_pib.supfrm_cfg_arg.bcn_interval_order))/1000 + 1; 

//    if(beacon_interval_time != mac_moniter_scan_time_get()) // this make dynamic sync time
//    {
//        mac_moniter_scan_time_set(beacon_interval_time);
//        
//        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x01, OSEL_EVENT_PRIO_LOW);
//    }

    osel_memcpy(&inter_gts_num_array, pbuf->data_p, superfrm_cfg_arg.inter_unit_num);
    pbuf->data_p += superfrm_cfg_arg.inter_unit_num;
}

/**
 * [mac_get_pend_filter : mac filter beacon frame, judge get downlink flag]
 *
 * @param pbuf [description]
 * @return [none]
 */
static void mac_get_pend_filter(pbuf_t *pbuf)
{
    uint16_t short_addr = EMPTY_ADDR;
    uint64_t long_addr = (uint64_t)EMPTY_ADDR;
    pend_addr_spec_t pend_add_arg;

    if (!mac_frames_get_pend_flag())
    {
        return;
    }

    osel_memcpy((uint8_t *)&pend_add_arg, pbuf->data_p, sizeof(pend_addr_spec_t));
    pbuf->data_p += sizeof(pend_addr_spec_t);

    pend_me_flag = FALSE;
    for (uint8_t i = 0; i < pend_add_arg.short_addr_nums; i++)
    {
        osel_memcpy((uint8_t *)&short_addr, pbuf->data_p, sizeof(uint16_t));
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
        if (short_addr == mac_pib.mac_short_addr)
        {
            pend_me_flag = TRUE;
        }
    }

    for (uint8_t i = 0; i < pend_add_arg.exter_addr_nums; i++)
    {
        osel_memcpy((uint8_t *)&long_addr, pbuf->data_p, sizeof(uint64_t));
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
        if (long_addr == mac_pib.mac_long_addr)
        {
            pend_me_flag = TRUE;
        }
    }
}


static void mac_get_superframe_payload(pbuf_t *pbuf)
{
    bcn_payload_t *bcn_pld = (bcn_payload_t *)(pbuf->data_p);
    neighbor_node_t neighbor_node;

    local_beacon_map |= ((uint16_t)0x01 << (bcn_pld->index));

    if(bcn_pld->hops < DEBUG_COORD_HOP)
    {
        return;
    }

    neighbor_node.dev_id     = pbuf->attri.src_id;
    neighbor_node.rssi       = pbuf->attri.rssi_dbm;
    neighbor_node.hops       = bcn_pld->hops;
    neighbor_node.time_stamp = bcn_pld->time_stamp;
    neighbor_node.beacon_ind = bcn_pld->index;
    neighbor_node.life_cycle = LIVING_TIME;
    neighbor_node.intra_channel = superfrm_cfg_arg.intra_channel;
    mac_neighbor_node_add(&neighbor_node);
}

void mac_clr_pend_me_flag(void)
{
    pend_me_flag = FALSE;
}

bool_t mac_get_pend_me_flag(void)
{
    return pend_me_flag;
}

uint16_t mac_get_beacon_map(void)
{
    return last_beacon_map;
}

bool_t mac_clr_beacon_map(void)
{
    last_beacon_map = local_beacon_map;
    local_beacon_map = 0;
    return TRUE;
}

/* pbuf->data_p has pointer to MAC Payload */
void mac_recv_beacon(pbuf_t *pbuf)
{
//    hal_led_toggle(RED); // LED已作为货押状态显示灯使用
    mac_get_superframe_cfg_arg(pbuf);

    mac_get_pend_filter(pbuf);

    mac_get_superframe_payload(pbuf);
}

void mac_sync_config(uint16_t target_id)
{
    sync_cfg_t cfg;

    cfg.background_compute = FALSE;
    cfg.sync_source        = FALSE;
    cfg.sync_target        = target_id;
    
    cfg.flag_byte_pos      = 0x03;
    cfg.flag_byte_msk      = 0x07;
    cfg.flag_byte_val      = MAC_FRAME_TYPE_BEACON;
    
    cfg.len_pos            = 0;
    cfg.len_modfiy         = TRUE;
    
    cfg.stamp_len          = 4;
    cfg.stamp_byte_pos     = 0;
    
    cfg.tx_sfd_cap         = FALSE;
    cfg.rx_sfd_cap         = TRUE;
    
    cfg.tx_offset          = 67;
    cfg.rx_offset          = 0;

    m_sync_cfg(&cfg);
}

bool_t mac_lost_net_jodge(void)
{
    if (mac_pib.self_index == 0xFF)
    {
        return FALSE;
    }

    if (!(local_beacon_map & (0x01 << mac_pib.self_index)))
    {
        if (++lost_corrd_cnt >= LOST_CORRD_CNT_MAX)
        {
            lost_corrd_cnt = 0;
            return TRUE;
        }
    }
    else
    {
        lost_corrd_cnt = 0;
    }
    return FALSE;
}

uint8_t mac_get_mine_hops(void)
{
    return mac_pib.hops;
}

void mac_beacon_init(void)
{
    mac_sync_config(0xFFFF);
}






