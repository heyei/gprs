/***************************************************************************
* File        : mac_schedule.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <stdlib.h>
#include <sbuf.h>
#include <pbuf.h>
#include <hal_board.h>
#include <phy_packet.h>
#include <m_slot.h>
#include <m_tran.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <mac_global.h>
#include <mac_frames.h>
#include <mac_databuf.h>
#include <mac_neighbors.h>
#include <mac_moniter.h>
#include <gprs.h>

typedef void (*func_t)(void *args);

extern bool_t gprs_power_on;
extern bool_t escort_gps_is_enabled;

#define MAX_SYNC_BEACON_INTERVAL_CNT        1u
static uint8_t sync_beacon_interval_cnt = 0;
static bool_t mac_can_recv_flag = FALSE;

// tag never recv or send data in inter_com_slot;
static slot_cfg_t beacon_interval_slot;
static slot_cfg_t sleep_slot;   // here sleep contain inter and sleep
static slot_cfg_t cluster_slot;
static slot_cfg_t beacon_slot, cluster_gts_slot;

/*****************************************************************************/
static uint8_t srandom(uint8_t min, uint8_t max)
{
    DBG_ASSERT(max >= min __DBG_LINE);
    srand(TA1R);
    return (min + rand() % (max - min + 1));
}

uint16_t slot_get_availeable_seq(void)
{
    uint8_t slot_index = srandom(mac_pib.supfrm_cfg_arg.downlink_slots_len,
                                 mac_pib.supfrm_cfg_arg.intra_gts_num - 1);

    return slot_index;
}

/*****************************************************************************/
static void slot_node_cfg(slot_cfg_t *node,
                          uint32_t   duration,
                          uint8_t    repeat_cnt,
                          func_t     func,
                          slot_cfg_t *parent,
                          slot_cfg_t *first_child,
                          slot_cfg_t *next_sibling)
{
    node->slot_duration   = duration;
    node->slot_repeat_cnt = repeat_cnt;
    node->func            = func;
    node->parent          = parent;
    node->first_child     = first_child;
    node->next_sibling    = next_sibling;
    node->slot_start      = 0;
    node->slot_repeat_seq = 0;
}
/*****************************************************************************/
static bool_t is_sync_beacon_interval(void)
{
    if (sync_beacon_interval_cnt == 0)
    {
        return TRUE;
    }

    return FALSE;
}

static bool_t is_coord_cluster_index(void)
{
    if (mac_pib.self_index == cluster_slot.slot_repeat_seq)
    {
        return TRUE;
    }

    return FALSE;
}
/*****************************************************************************/
void gts_txok_frame_parse(sbuf_t *sbuf, bool_t res)
{
    mac_frm_ctrl_t mac_frm_ctrl;
    pbuf_t *pbuf = sbuf->primargs.pbuf;
    uint8_t *datap = pbuf->head;

    datap += PHY_HEAD_SIZE;
    osel_memcpy(&mac_frm_ctrl, datap, MAC_HEAD_CTRL_SIZE);
    datap += MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE;

    m2n_data_confirm_t *m2n_data_confirm =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg);

    if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy(&(m2n_data_confirm->dst_addr), datap, MAC_ADDR_LONG_SIZE);
        datap += MAC_ADDR_LONG_SIZE;
        m2n_data_confirm->dst_mode = ADDR_MODE_LONG;
    }
    else
    {
        osel_memcpy(&(m2n_data_confirm->dst_addr), datap, MAC_ADDR_SHORT_SIZE);
        datap += MAC_ADDR_SHORT_SIZE;
        m2n_data_confirm->dst_mode = ADDR_MODE_SHORT;
    }

    if (mac_frm_ctrl.src_addr_mode == ADDR_MODE_LONG)
    {
        datap += MAC_ADDR_LONG_SIZE;
        m2n_data_confirm->src_mode = ADDR_MODE_LONG;
    }
    else
    {
        datap += MAC_ADDR_SHORT_SIZE;
        m2n_data_confirm->src_mode = ADDR_MODE_SHORT;
    }
    uint8_t mac_len = datap - pbuf->head - PHY_HEAD_SIZE;
    sbuf->primtype = M2N_DATA_CONFIRM;
    m2n_data_confirm->msdu_length = sbuf->primargs.pbuf->data_len - mac_len;
    m2n_data_confirm->msdu        = datap;
    m2n_data_confirm->status      = res;
    sbuf->primargs.pbuf->data_len -= mac_len;

    osel_post(MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void cluster_gts_txok_cb(sbuf_t *sbuf, bool_t res)
{
    if (sbuf->orig_layer == MAC_LAYER)
    {
        if ((sbuf != NULL) && (sbuf->used))
        {
            if (sbuf->primargs.pbuf->used)
            {
                pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
            }
            sbuf_free(&sbuf __SLINE2);
        }

        mac_can_recv_flag = TRUE;
    }
    else
    {
        gts_txok_frame_parse(sbuf, res);
    }
}

static void mac_gts_send_data(void)
{
    sbuf_t *sbuf = mac_databuf_remove(cluster_gts_slot.slot_repeat_seq);
    if (sbuf != NULL)
    {
        if (cluster_gts_slot.slot_repeat_seq == 0)   // 因为时隙配置的层数不一致，定时器的定时消耗
        {
            delay_us(1000);
        }
        else
        {
            delay_us(1000);
        }

        m_tran_send(sbuf, cluster_gts_txok_cb, MAC_MAX_SEND_TIMES);
    }
    else
    {
        m_tran_sleep();
    }
}
/*****************************************************************************/
static void beacon_slot_handle(void *seq_p)
{
    m_tran_stop();
    mac_frames_clr_pend_flag();
    mac_clr_pend_me_flag();
    mac_pib.supfrm_cfg_arg.downlink_slots_len = 0;

    if (!is_sync_beacon_interval())
    {
        m_tran_sleep();
        return;
    }

    if (is_coord_cluster_index())
    {
        m_tran_recv();
    }
    else
    {
        m_tran_sleep();
    }
}


static void cluster_gts_slot_handle(void *seq_p)
{
    m_tran_stop();
    if (!is_sync_beacon_interval())
    {
        m_tran_sleep();
        return;
    }

    if (is_coord_cluster_index())
    {
        if (mac_frames_get_pend_flag())
        {
            if (cluster_gts_slot.slot_repeat_seq
                    < mac_pib.supfrm_cfg_arg.downlink_slots_len)
            {
                if (mac_get_pend_me_flag())
                {
                    m_tran_recv();
                }
                else
                {
                    m_tran_sleep();
                }
            }
            else
            {
//                mac_gts_send_data();
                if (mac_can_recv_flag)
                {
                    /* assoc req, when assoc send, next gts will recv assoc req */
                    mac_can_recv_flag = FALSE;
                    m_tran_recv();
                }
                else
                {
                    mac_gts_send_data();
                }
            }
        }
        else
        {
            if (mac_can_recv_flag)
            {
                /* assoc req, when assoc send, next gts will recv assoc req */
                mac_can_recv_flag = FALSE;
                m_tran_recv();
            }
            else
            {
                mac_gts_send_data();
            }
        }
    }
    else
    {
        m_tran_sleep();
    }
}

static void cluster_slot_handle(void *seq_p)
{
//    hal_led_open(BLUE);
}

#if PRINT_SYNC_PARA_EN == 0
static void sleep_slot_handle(void *seq_p)
{
//    hal_led_close(BLUE);
    m_tran_sleep();
    mac_neighbor_table_update();
    if (mac_lost_net_jodge())
    {
        osel_post(APP_INLINE_EVENT, (uint32_t *)(FALSE), OSEL_EVENT_PRIO_LOW);
        osel_post(NWK_INLINE_EVENT, (uint32_t *)(FALSE), OSEL_EVENT_PRIO_LOW);
        // 如果在时隙回调里面执行这个函数，会把时隙停止，导致退出时隙回调以后，定时器无法正常定时；
        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x05, OSEL_EVENT_PRIO_LOW);
    }
    mac_clr_beacon_map();

    hal_board_info_delay_save();
    debug_delay_clr_info();
}
#else
#include <stdio.h>
extern uint32_t txtime_x;
extern uint32_t rxtime_y;
extern bool_t need_printf;
extern fp32_t k;
extern int32_t x0;
extern int32_t y0;
static void sleep_slot_handle(void *seq_p)
{
    if (need_printf)
    {
        need_printf = FALSE;
        printf("%lu\t%lu\t%.8E\t%d\t%d", txtime_x, rxtime_y, k, x0, y0);
    }
//    hal_led_close(BLUE);
    m_tran_sleep();
    mac_neighbor_table_update();
    if (mac_lost_net_jodge())
    {
//        printf("----------\r\n");
        osel_post(APP_INLINE_EVENT, (uint32_t *)(FALSE), OSEL_EVENT_PRIO_LOW);
        osel_post(NWK_INLINE_EVENT, (uint32_t *)(FALSE), OSEL_EVENT_PRIO_LOW);
        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x05, OSEL_EVENT_PRIO_LOW);
    }
    mac_clr_beacon_map();

    hal_board_info_delay_save();
    debug_delay_clr_info();
}
#endif
static void beacon_interval_slot_handle(void *seq_p)
{
//    hal_led_open(BLUE);
    if (++sync_beacon_interval_cnt >= MAX_SYNC_BEACON_INTERVAL_CNT)
    {
        sync_beacon_interval_cnt = 0;
    }
}
/******************************************************************************/

void sys_enter_lpm_handler(void *p)
{
    if((gprs_power_on == FALSE)&& (escort_gps_is_enabled == FALSE))
    {
        gprs_enter_sleep_io_set();
        LPM3;
    }
}

void mac_schedule_init(void)
{
    sync_beacon_interval_cnt = 0;
    osel_idle_hook(sys_enter_lpm_handler);
}

void mac_super_frame_cfg(void)
{
    uint32_t beacon_interval =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * ((uint32_t)0x01 <<
                   mac_pib.supfrm_cfg_arg.bcn_interval_order));
    uint32_t beacon_duration =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * mac_pib.supfrm_cfg_arg.bcn_duration_order);
    uint32_t cluster_gts_duration =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * mac_pib.supfrm_cfg_arg.gts_duration_order);
    uint8_t cluster_gts_cnt = mac_pib.supfrm_cfg_arg.intra_gts_num;
    uint8_t cluster_num     = mac_pib.supfrm_cfg_arg.cluster_num;

    uint32_t sleep_duration =  beacon_interval - cluster_num * (beacon_duration + \
                               cluster_gts_duration * cluster_gts_cnt);

    slot_node_cfg(&beacon_slot, beacon_duration, 1, &beacon_slot_handle,
                  &cluster_slot, NULL, &cluster_gts_slot);
    slot_node_cfg(&cluster_gts_slot, cluster_gts_duration, cluster_gts_cnt,
                  &cluster_gts_slot_handle, &cluster_slot, NULL, NULL);
    slot_node_cfg(&cluster_slot, 0, cluster_num, &cluster_slot_handle,
                  &beacon_interval_slot, &beacon_slot, &sleep_slot);
    slot_node_cfg(&sleep_slot, sleep_duration, 1, &sleep_slot_handle,
                  &beacon_interval_slot, NULL, NULL);
    slot_node_cfg(&beacon_interval_slot, 0, 0, &beacon_interval_slot_handle,
                  NULL, &cluster_slot, NULL);

    m_slot_cfg(&beacon_interval_slot, SLOT_GLOBAL_TIME);

    if (beacon_interval != beacon_interval_slot.slot_duration)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
    }
}

uint32_t mac_get_global_superfrm_time(void)
{
    return beacon_interval_slot.slot_duration;
}




