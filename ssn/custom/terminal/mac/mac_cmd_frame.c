/***************************************************************************
* File        : mac_cmd_frame.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <mac_cmd_frame.h>
#include <mac_global.h>
#include <mac_beacon.h>
#include <mac_frames.h>
#include <phy_packet.h>
#include <mac_neighbors.h>
#include <mac_moniter.h>
#include <mac_databuf.h>
#include <hal_timer.h>
#include <mac_prim.h>
#include <m_sync.h>

static void mac_fill_assoc_req_buf(pbuf_t *pbuf, uint16_t dst_addr)
{
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;

    mac_frm_ctrl_t mac_ctrl;
    mac_ctrl.frm_type      = MAC_FRAME_TYPE_COMMAND;
    mac_ctrl.sec_enable    = FALSE;
    mac_ctrl.frm_pending   = FALSE;
    mac_ctrl.ack_req       = FALSE;
    mac_ctrl.des_addr_mode = ADDR_MODE_SHORT;
    mac_ctrl.src_addr_mode = ADDR_MODE_LONG;
    mac_ctrl.reseverd      = 0x00;
    osel_memcpy(pbuf->data_p, &mac_ctrl, sizeof(mac_frm_ctrl_t));
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    osel_memcpy(pbuf->data_p, &mac_pib.mac_bsn, sizeof(uint8_t));
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;

    osel_memcpy(pbuf->data_p, (uint8_t *)&dst_addr, MAC_ADDR_SHORT_SIZE);
    pbuf->data_p += MAC_ADDR_SHORT_SIZE;

    osel_memcpy(pbuf->data_p, (void *)&mac_pib.mac_long_addr, MAC_ADDR_LONG_SIZE);
    pbuf->data_p += MAC_ADDR_LONG_SIZE;

    //mac payload
    *(uint8_t *)pbuf->data_p = MAC_CMD_ASSOC_REQ;
    pbuf->data_p += sizeof(uint8_t);

    mac_assoc_req_arg_t associ_req_args;
    associ_req_args.device_type = NODE_TYPE_TAG;
    associ_req_args.sec_cap = SECU_FALSE;
    associ_req_args.beacon_bitmap = mac_get_beacon_map();

    hal_time_t now = hal_timer_now();
    m_sync_l2g(&now);
    associ_req_args.assoc_apply_time = now.w;
    osel_memcpy(pbuf->data_p, (uint8_t *)&associ_req_args, sizeof(mac_assoc_req_arg_t));
    pbuf->data_p += sizeof(mac_assoc_req_arg_t);

    //pbuf attri
    pbuf->attri.seq       = mac_pib.mac_bsn++;
    pbuf->attri.need_ack  = mac_ctrl.ack_req;
    pbuf->attri.already_send_times.mac_send_times 
                          = 0;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.dst_id    = dst_addr;
    pbuf->data_len        = pbuf->data_p - pbuf->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
}

void mac_make_assoc_request(neighbor_node_t *infor)
{
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    if (NULL == pbuf)
    {
        return;
    }
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    if (NULL == sbuf)
    {
        pbuf_free(&pbuf __PLINE2);
        return;
    }

    mac_fill_assoc_req_buf(pbuf, infor->dev_id);

    sbuf->primargs.pbuf = pbuf;
    sbuf->orig_layer = MAC_LAYER;

    mac_databuf_insert(sbuf, TRUE);
}

static void mac_assoc_resp_frame_parse(pbuf_t *pbuf)
{
    mac_assoc_res_arg_t mac_assoc_res_arg;
    osel_memcpy((uint8_t *)&mac_assoc_res_arg,
                pbuf->data_p,
                sizeof(mac_assoc_res_arg_t));
    pbuf->data_p += sizeof(mac_assoc_res_arg_t);

    if (mac_assoc_res_arg.status != ASSOC_STATUS_SUCCESS)
    {
        mac_neighbors_node_set_state(pbuf->attri.src_id, FALSE);
        mac_state.ss = SUB_STATE_IDLE;
        mac_assoc_timer_cancel();

        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x02, OSEL_EVENT_PRIO_LOW);
        return;
    }
    
    mac_assoc_timer_cancel();
    
    mac_neighbors_node_set_state(pbuf->attri.src_id, TRUE);
    
    mac_pib.mac_coord_short_addr = pbuf->attri.src_id;
    mac_pib.self_index           = mac_assoc_res_arg.index;
    mac_state.ss                 = SUB_STATE_ASSOCIATION_OK;
  
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    if (sbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }

    m2n_assoc_confirm_t *assoc_cfm =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_assoc_confirm_arg);

    sbuf->primtype = M2N_ASSOC_CONFIRM;
    assoc_cfm->status = TRUE;

    osel_post(MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void mac_cmd_frame_parse(pbuf_t *pbuf)
{
    uint8_t cmd_frm_type = *(pbuf->data_p);
    pbuf->data_p += sizeof(uint8_t);

    switch (cmd_frm_type)
    {
    case MAC_CMD_ASSOC_REQ:
        // do nothing if get assoc request
        break;

    case MAC_CMD_ASSOC_RESP:
        mac_assoc_resp_frame_parse(pbuf);
        break;

    default:
        break;
    }
}


