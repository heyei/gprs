/***************************************************************************
* File        : mac_prim.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <osel_arch.h>
#include <sbuf.h>
#include <debug.h>
#include <hal_timer.h>
#include <phy_packet.h>
#include <mac_prim.h>
#include <mac_module.h>
#include <mac_global.h>
#include <mac_cmd_frame.h>
#include <mac_frames.h>
#include <mac_databuf.h>
#include <mac_moniter.h>
#include <mac_schedule.h>
#include <hal_uart.h>

static hal_timer_t *assoc_timeout_timer   = NULL;
static uint8_t assoc_request_cnt = 0;

static void assoc_timeout_timer_cb(void *p)
{
    assoc_timeout_timer = NULL;

    assoc_request_cnt++;
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    sbuf->primtype = M2M_ASSOC_REQUETS;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void mac_assoc_timer_cancel(void)
{
    assoc_request_cnt = 0;
    if (NULL != assoc_timeout_timer)
    {
        hal_timer_cancel(&assoc_timeout_timer);
    }
}

static void m2m_assoc_request(void)
{
    if (assoc_request_cnt >= ASSOC_REQUEST_CNT_MAX)
    {
        assoc_request_cnt = 0;
        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x03, OSEL_EVENT_PRIO_LOW);
        return;
    }

    if ((NULL == assoc_timeout_timer) && (mac_state.ss == SUB_STATE_IDLE))
    {
        HAL_TIMER_SET_REL(mac_get_global_superfrm_time() * 1,
                          assoc_timeout_timer_cb,
                          NULL,
                          assoc_timeout_timer);
        DBG_ASSERT(assoc_timeout_timer != NULL __DBG_LINE);

        mac_make_assoc_request(&coord);
    }
}

/**
 * [mac_fill_dataframe_buf : fill pbuf in sbuf by sbuf prim args]
 * @param sbuf [the sbuf to be fill]
 */
static void mac_fill_dataframe_buf(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    n2m_data_request_t *n2m_data_req =
        &(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);
    pbuf_t *pbuf = sbuf->primargs.pbuf;
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;

    mac_frm_ctrl_t mac_frm_ctrl;
    mac_frm_ctrl.frm_type        = MAC_FRAME_TYPE_DATA;
    mac_frm_ctrl.sec_enable      = FALSE;    //这个根据要求设置
    mac_frm_ctrl.frm_pending     = FALSE;
    mac_frm_ctrl.ack_req         = pbuf->attri.need_ack;
    mac_frm_ctrl.reseverd        = 0;
    mac_frm_ctrl.des_addr_mode   = n2m_data_req->dst_mode;
    mac_frm_ctrl.src_addr_mode   = n2m_data_req->src_mode;

    osel_memcpy(pbuf->data_p, &mac_frm_ctrl, sizeof(mac_frm_ctrl_t));
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    osel_memcpy(pbuf->data_p, &mac_pib.mac_bsn, sizeof(uint8_t));
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;

    if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy(pbuf->data_p, &(n2m_data_req->dst_addr), MAC_ADDR_SHORT_SIZE);
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    }
    else if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy(pbuf->data_p, &(n2m_data_req->dst_addr), MAC_ADDR_LONG_SIZE);
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
    }

    if (mac_frm_ctrl.src_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy(pbuf->data_p, &mac_pib.mac_short_addr, MAC_ADDR_SHORT_SIZE);
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    }
    else if (mac_frm_ctrl.src_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy(pbuf->data_p, &mac_pib.mac_long_addr, MAC_ADDR_LONG_SIZE);
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
    }

    pbuf->attri.seq = mac_pib.mac_bsn++;
    pbuf->attri.already_send_times.mac_send_times = 0;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.dst_id = (uint16_t)n2m_data_req->dst_addr;
    pbuf->data_len += pbuf->data_p - pbuf->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
}

static void mac_data_request(sbuf_t *sbuf)
{
    if (mac_state.ss == SUB_STATE_ASSOCIATION_OK)
    {
        mac_fill_dataframe_buf(sbuf);
        mac_databuf_insert(sbuf, FALSE);
    }
    else
    {
        /* return data back to nwk */
        sbuf->primtype = M2N_DATA_CONFIRM;
        
        m2n_data_confirm_t *m2n_data_confirm =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg);
        
        n2m_data_request_t n2m_data_req_arg =
            sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg;
        
        m2n_data_confirm->status      = FALSE;
        m2n_data_confirm->msdu_length = n2m_data_req_arg.msdu_length;
        m2n_data_confirm->msdu        = n2m_data_req_arg.msdu;
        m2n_data_confirm->src_mode    = n2m_data_req_arg.src_mode;
        
        osel_post(MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
    }
}

static void mac_prim_handler(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    switch (sbuf->primtype)
    {
    case N2M_DATA_REQUEST:
        mac_data_request(sbuf);
        break;

    case M2M_ASSOC_REQUETS:
        m2m_assoc_request();
        sbuf_free(&sbuf __SLINE2);
        break;

    default:
        break;
    }
}

static void m_prim_event_handler(const osel_event_t *const pmsg)
{
    DBG_ASSERT(pmsg != NULL __DBG_LINE);
    switch (pmsg->sig)
    {
    case M_MAC_PRIM_EVENT:
        mac_prim_handler((sbuf_t *)(pmsg->param));
        break;

    default:
        break;
    }
}

void m_prim_init(void)
{
    module_bind_event(m_prim_event_handler, M_MAC_PRIM_EVENT);
}
