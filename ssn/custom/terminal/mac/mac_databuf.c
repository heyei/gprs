/***************************************************************************
* File        : mac_databuf.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <lib.h>
#include <hal.h>
#include <phy_packet.h>
#include <mac_databuf.h>
#include <mac_schedule.h>
#include <mac_frames.h>
#include <mac_global.h>

static list_head_t active_list_head;
static list_head_t pause_list_head;

bool_t mac_databuf_init(void)
{
    list_init(&active_list_head);
    list_init(&pause_list_head);

    return TRUE;
}

void mac_databuf_insert(sbuf_t *sbuf, bool_t flag)
{
    hal_int_state_t s;

    sbuf->slot_seq = slot_get_availeable_seq();
    
    HAL_ENTER_CRITICAL(s);
    list_add_to_tail(&(sbuf->list), &active_list_head);
    HAL_EXIT_CRITICAL(s);
}

sbuf_t *mac_databuf_remove(uint16_t index)
{
    sbuf_t *sbuf = NULL;

    if (list_empty(&active_list_head))
    {
        return NULL;
    }

    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    list_entry_for_each_safe( pos1, pos2 , &active_list_head, sbuf_t, list)
    {
        if (pos1->slot_seq == index)
        {
            hal_int_state_t s = 0;
            HAL_ENTER_CRITICAL(s);
            sbuf = pos1;
            list_del(&(pos1->list));
            HAL_EXIT_CRITICAL(s);

            break;
        }
    }

    return sbuf;
}

static void mac_change_dst_addr(pbuf_t *pbuf)
{
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE +
                   MAC_HEAD_SEQ_SIZE;
    osel_memcpy(pbuf->data_p, &(mac_pib.mac_coord_short_addr), MAC_ADDR_SHORT_SIZE);
}

/**
 * [mac_databuf_active : get temp buf into active buf]
 * @return  [description]
 */
bool_t mac_databuf_active(void)
{
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    
    if(list_empty(&pause_list_head))
    {
        return FALSE;
    }
    
    hal_int_state_t s = 0;
    HAL_ENTER_CRITICAL(s);
    list_entry_for_each_safe( pos1, pos2 , &pause_list_head, sbuf_t, list)
    {
        list_del(&(pos1->list));
        mac_change_dst_addr(pos1->primargs.pbuf);
        list_add_to_tail(&(pos1->list), &active_list_head);
    }
    HAL_EXIT_CRITICAL(s);
    
    return TRUE;
}

/**
 * [mac_databuf_pause : pause active databuf into temp buf]
 * @return  [description]
 */
bool_t mac_databuf_pause(void)
{
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;

    hal_int_state_t s = 0;
    HAL_ENTER_CRITICAL(s);
    list_entry_for_each_safe( pos1, pos2 , &active_list_head, sbuf_t, list)
    {
        list_del(&(pos1->list));
        gts_txok_frame_parse(pos1, FALSE);
//        list_add_to_tail(&(pos1->list), &pause_list_head);
    }
    HAL_EXIT_CRITICAL(s);

    return TRUE;
}



