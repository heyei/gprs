/***************************************************************************
* File        : mac_neighbors.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <node_cfg.h>
#include <debug.h>
#include <mac_neighbors.h>
#include <osel_arch.h>
#include <mac_prim.h>

#define INVAILD_ID                      0xFFFF
#define INDEX                           16
#define MAX_HOP_NUMBER                  0xFF

#define LIVE_TIMEOUT                    (MAX_HOPS * ASSOC_REQUEST_CNT_MAX * MAX_HOPS)

static neighbor_node_t neighbor_node_array[DEV_COUNT];
static uint8_t max_index = 0;

void mac_neighbor_table_init(void)
{
    for (uint8_t i = 0; i < DEV_COUNT; i++)
    {
        neighbor_node_array[i].dev_id        = INVAILD_ID;
        neighbor_node_array[i].rssi          = -127;
        neighbor_node_array[i].hops          = MAX_HOP_NUMBER;
        neighbor_node_array[i].time_stamp    = 0x00000000;
        neighbor_node_array[i].beacon_ind    = INDEX;
        neighbor_node_array[i].life_cycle    = LIVING_TIME;
        neighbor_node_array[i].intra_channel = 0;
        neighbor_node_array[i].assoc_state   = TRUE;  // can be assoc
    }

    max_index = 0;
}

void mac_neighbor_table_update(void)
{
    uint8_t i = 0;

    for (i = max_index; i > 0; i--)
    {
        if (neighbor_node_array[i - 1].dev_id != INVAILD_ID)
        {
            neighbor_node_array[i - 1].life_cycle++;
            if (neighbor_node_array[i - 1].life_cycle >= LIVE_TIMEOUT)
            {
                neighbor_node_array[i - 1].dev_id        = INVAILD_ID;
                neighbor_node_array[i - 1].rssi          = -127;
                neighbor_node_array[i - 1].hops          = MAX_HOP_NUMBER;
                neighbor_node_array[i - 1].time_stamp    = 0x00000000;
                neighbor_node_array[i - 1].beacon_ind    = INDEX;
                neighbor_node_array[i - 1].life_cycle    = LIVING_TIME;
                neighbor_node_array[i - 1].intra_channel = 0;
                neighbor_node_array[i - 1].assoc_state   = TRUE; // can be assoc

                if (i >= max_index)
                {
                    max_index--;
                }
            }
        }
    }
}


bool_t mac_neighbor_node_add(neighbor_node_t *neighbor_device)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == neighbor_device->dev_id)
        {
            neighbor_node_array[i].hops          = neighbor_device->hops;
            neighbor_node_array[i].rssi          = neighbor_device->rssi;
            neighbor_node_array[i].time_stamp    = neighbor_device->time_stamp;
            neighbor_node_array[i].beacon_ind    = neighbor_device->beacon_ind;
            neighbor_node_array[i].life_cycle    = LIVING_TIME;
            neighbor_node_array[i].intra_channel = neighbor_device->intra_channel;
            return TRUE;
        }
    }

    for (i = 0; i < DEV_COUNT; i++)
    {
        if (neighbor_node_array[i].dev_id == INVAILD_ID)
        {
            neighbor_node_array[i].dev_id        = neighbor_device->dev_id;
            neighbor_node_array[i].hops          = neighbor_device->hops;
            neighbor_node_array[i].rssi          = neighbor_device->rssi;
            neighbor_node_array[i].time_stamp    = neighbor_device->time_stamp;
            neighbor_node_array[i].beacon_ind    = neighbor_device->beacon_ind;
            neighbor_node_array[i].life_cycle    = LIVING_TIME;
            neighbor_node_array[i].intra_channel = neighbor_device->intra_channel;

            if (i >= max_index)
            {
                max_index++;
            }

            return TRUE;
        }
    }

    if (i == DEV_COUNT)
    {
        //DBG_ASSERT(FALSE __DBG_LINE);
        return FALSE;
    }

    return TRUE;
}

bool_t mac_neighbors_node_set_state(uint16_t id, bool_t assoc_state)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == id)
        {
            neighbor_node_array[i].assoc_state = assoc_state;
            return TRUE;
        }
    }

    return FALSE;
}

/**
 * [mac_neighbor_node_delete : ]
 * @param  neighbor_device [description]
 * @return                 [TRUE -- success]
 */
bool_t mac_neighbor_node_delete(neighbor_node_t *neighbor_device)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == neighbor_device->dev_id)
        {
            neighbor_node_array[i].dev_id        = INVAILD_ID;
            neighbor_node_array[i].rssi          = -127;
            neighbor_node_array[i].hops          = MAX_HOP_NUMBER;
            neighbor_node_array[i].time_stamp    = 0x00000000;
            neighbor_node_array[i].beacon_ind    = INDEX;
            neighbor_node_array[i].life_cycle    = LIVING_TIME;
            neighbor_node_array[i].intra_channel = 0;
            neighbor_node_array[i].assoc_state   = TRUE;  // can be assoc

            if (i == (max_index - 1))
            {
                max_index--;
            }

            return TRUE;
        }
    }

    return FALSE;
}

bool_t mac_get_coord(neighbor_node_t *coord)
{
    bool_t  res = FALSE;
    uint8_t beast_index = DEV_COUNT;
    uint8_t beast_hops = MAX_HOPS;
    int8_t  beast_rssi = RSSI_QUERY_FAILED;

    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].rssi >= RSSI_QUERY_THRESHOLD)
        {
            if (neighbor_node_array[i].hops < beast_hops)
            {
                beast_hops = neighbor_node_array[i].hops;
                beast_index = i;
                res = TRUE;
            }
            else if (neighbor_node_array[i].hops == beast_hops)
            {
                if (neighbor_node_array[i].assoc_state == TRUE)
                {
                    beast_index = i;
                    res = TRUE;
                }
            }
        }
    }
    if (!res)
    {
        if (beast_index == DEV_COUNT)
        {
            for (uint8_t i = 0; i < max_index; i++)
            {
                if (neighbor_node_array[i].hops < beast_hops)
                {
                    if (neighbor_node_array[i].rssi > beast_rssi)
                    {
                        beast_rssi = neighbor_node_array[i].rssi;
                        beast_index = i;
                        res = TRUE;
                    }
                    else if (neighbor_node_array[i].rssi == beast_rssi)
                    {
                        if (neighbor_node_array[i].assoc_state == TRUE)
                        {
                            beast_index = i;
                            res = TRUE;
                        }
                    }
                }
            }
        }
    }

    if (res)
    {
        osel_memcpy((uint8_t *)coord,
                    (uint8_t *)&neighbor_node_array[beast_index],
                    sizeof(neighbor_node_t));
    }
    return res;
}
