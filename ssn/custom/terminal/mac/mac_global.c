/***************************************************************************
* File        : mac_global.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <mac_global.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <m_sync.h>
#include <mac_neighbors.h>

mac_pib_t mac_pib;
mac_state_t mac_state;
uint64_t node_nui = 0;

static local_info_t local_info;
/*************************************************************************/
void mac_global_init(void)
{
    mac_state.ms = MAIN_STATE_IDLE;
    mac_state.ss = SUB_STATE_IDLE;

    osel_memset(&mac_pib, 0x00, sizeof(mac_pib));
    mac_pib.self_index = 0xFF;

    device_info_t device_info = hal_board_info_look();
    uint8_t id_size = sizeof(device_info.device_id);

    osel_memcpy((uint8_t *) & (mac_pib.mac_long_addr),
                device_info.device_id,
                id_size);
    
    mac_pib.mac_short_addr = hal_board_get_device_short_addr(
                                 device_info.device_id);

    osel_memcpy((uint8_t *)&node_nui, device_info.device_id, id_size);
}

void mac_global_reset(void)
{
    mac_state.ms = MAIN_STATE_IDLE;
    mac_state.ss = SUB_STATE_IDLE;

    mac_beacon_init();
    mac_neighbor_table_init();
}

void mac_set_local_info(pbuf_t *pbuf)
{
    hal_time_t now = hal_timer_now();
    local_info.num = 1;
    local_info.src_addr = mac_pib.mac_coord_short_addr;
    local_info.lqi = pbuf->attri.rssi_dbm;
    m_sync_l2g(&now);
    local_info.lqi_timestamp = now.w;
}

local_info_t mac_get_local_info(void)
{
    return local_info;
}
