/***************************************************************************
* File        : mac_moniter.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <debug.h>
#include <osel_arch.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <phy_packet.h>
#include <phy_state.h>
#include <m_tran.h>
#include <m_slot.h>
#include <mac_moniter.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <mac_neighbors.h>
#include <mac_global.h>
#include <mac_module.h>
#include <mac_databuf.h>
#include <nfc_module.h>

neighbor_node_t coord;
static uint8_t mac_intra_ch_index = 0;
static uint32_t mac_scan_interval_time = MAC_SCAN_CYCLE_TIME;
static uint32_t mac_moniter_interval_time = MAC_MONITER_INTERVAL_TIME;

static hal_timer_t *moniter_sync_timer = NULL;
static hal_timer_t *moniter_scan_timer = NULL;
static hal_timer_t *moniter_startup_timer = NULL;

//static hal_timer_t *moniter_delay_timer = NULL;

bool_t moniter_state = FALSE;       //!< 默认是关闭状态


void mac_moniter_start(void);

uint32_t mac_moniter_scan_time_get(void)
{
    return mac_scan_interval_time;
}

void mac_moniter_scan_time_set(uint32_t time)
{
    mac_scan_interval_time = time;
}

/*
----|----------------------------------------------------------------------|---
    |--moniter_scan_timer---|--moniter_startup_timer------|---sleep time---|
    |----------------------------moniter_sync_timer------------------------|
*/

static void moniter_sync_timer_cb(void *p)
{
    moniter_sync_timer = NULL;
    osel_post(M_MAC_MONI_SYNC_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void mac_moniter_sync_timer_start(void)
{
    PRINTF("moniter:sync timer start\r\n");
    if (NULL == moniter_sync_timer)
    {
        HAL_TIMER_SET_REL(  MS_TO_TICK(mac_moniter_interval_time*1000ul),
                            moniter_sync_timer_cb,
                            NULL,
                            moniter_sync_timer);
        DBG_ASSERT(moniter_sync_timer != NULL __DBG_LINE);
    }
}

static void mac_moniter_sync_timer_stop(void)
{
    PRINTF("moniter:sync timer stop\r\n");
    if (moniter_sync_timer != NULL)
    {
        hal_timer_cancel(&moniter_sync_timer);
    }
}

static void mac_moniter_sync_handler(void)
{
    PRINTF("moniter:sync handler\r\n");
    if (m_slot_get_state()) // slot running
    {
        DBG_ASSERT(moniter_scan_timer == NULL __DBG_LINE);
        return;
    }

    mac_moniter_start();
}

static void moniter_scan_timer_cb(void *p)
{
    moniter_scan_timer = NULL;
    osel_post(M_MAC_MONI_SCAN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void mac_moniter_scan_timer_start(void)
{
    PRINTF("moniter:scan timer start\r\n");
    if (NULL == moniter_scan_timer)
    {
        HAL_TIMER_SET_REL(  MS_TO_TICK(mac_scan_interval_time*1000ul),
                            moniter_scan_timer_cb,
                            NULL,
                            moniter_scan_timer);
        DBG_ASSERT(moniter_scan_timer != NULL __DBG_LINE);
    }
}

static void mac_moniter_scan_timer_stop(void)
{
    PRINTF("moniter:scan timer stop\r\n");
    if (moniter_scan_timer != NULL)
    {
        hal_timer_cancel(&moniter_scan_timer);
    }
}

static void moniter_startup_timer_cb(void *p)
{
    moniter_startup_timer = NULL;
    osel_post(M_MAC_MONI_STARTUP_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

//static void moniter_delay_timer_cb(void *p)
//{
////    moniter_delay_timer = NULL;
//    osel_post(M_MAC_MONI_START_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
//}

static bool_t find_sys_coord(void)
{
    if (!mac_get_coord(&coord))
    {
        m_tran_sleep();
        PRINTF("moniter:find sys coord failed\r\n");
        return FALSE;
    }

    mac_pib.hops                         = coord.hops + 1;
    mac_pib.self_index                   = coord.beacon_ind;
    mac_pib.mac_coord_short_addr         = coord.dev_id;
    mac_pib.supfrm_cfg_arg.intra_channel = coord.intra_channel;

    if(m_slot_get_state())
    {
        return FALSE;
    }
    
    mac_super_frame_cfg();
    mac_sync_config(coord.dev_id);
    phy_set_channel(coord.intra_channel);

    if (NULL == moniter_startup_timer)
    {
        HAL_TIMER_SET_REL(  mac_get_global_superfrm_time() * MAC_STARTUP_INTER_CNT,
                            moniter_startup_timer_cb,
                            NULL,
                            moniter_startup_timer);
        DBG_ASSERT(moniter_startup_timer != NULL __DBG_LINE);
    }
    
    PRINTF("moniter:find sys coord ok\r\n");
    m_tran_recv();
    return TRUE;
}

static void mac_mointer_scan_handler(void)
{
    PRINTF("moniter:scan handler\r\n");
    device_info_t device_info = hal_board_info_look();
    if (mac_intra_ch_index >= device_info.intra_ch_cnt)
    {
        mac_intra_ch_index = 0;
        find_sys_coord();
    }
    else
    {
        mac_moniter_scan_timer_start();
        phy_set_channel(device_info.intra_ch[mac_intra_ch_index++]);
        m_tran_recv();
    }
}

static void mac_moniter_startup_handler(void)
{
    PRINTF("moniter:startup, slot running\r\n");

    hal_time_t slot_run_time;
    if (!m_slot_get_state())
    {
        slot_run_time.w = coord.time_stamp;
        m_slot_run((hal_time_t *) &slot_run_time);
        mac_state.ms = MAIN_STATE_SCHEDULING;
    }

    mac_moniter_sync_timer_stop();

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    sbuf->primtype = M2M_ASSOC_REQUETS;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void mac_moniter_start(void)
{
    moniter_state = TRUE;
    
    PRINTF("moniter: start\r\n");
    /* sync timer start */
    mac_moniter_sync_timer_start();

    /* scan timer start */
    mac_moniter_scan_timer_start();

    mac_intra_ch_index = 0;

    m_sync_enable(FALSE);
    
    device_info_t device_info = hal_board_info_look();
    phy_set_channel(device_info.intra_ch[mac_intra_ch_index++]);
    phy_set_power(device_info.power_sn);

    m_tran_recv();
}

void mac_moniter_stop(void)
{
    PRINTF("moniter: stop\r\n");
    moniter_state = FALSE;
    m_slot_stop();
    m_tran_sleep();
    
    m_sync_enable(FALSE);

    mac_moniter_sync_timer_stop();
    mac_moniter_scan_timer_stop();

    mac_global_reset();
    mac_databuf_pause();
}

void mac_moniter_restart(void)
{
    mac_intra_ch_index = 0;
    
    PRINTF("moniter: restart\r\n");
    m_slot_stop();
    m_tran_sleep();
    
    mac_moniter_sync_timer_stop();
    mac_moniter_scan_timer_stop();

    mac_global_reset();
    mac_databuf_pause();
    
    // when ack send, tran module will change rf to sleep state,
//    if (NULL == moniter_delay_timer)
//    {
//        HAL_TIMER_SET_REL(  MS_TO_TICK(mac_scan_interval_time*1000),
//                            moniter_delay_timer_cb,
//                            NULL,
//                            moniter_delay_timer);
//        DBG_ASSERT(moniter_delay_timer != NULL __DBG_LINE);
//    }
//    else
//    {
//        DBG_ASSERT(FALSE __DBG_LINE);
//    }
}


static void m_mac_moniter_handler(const osel_event_t *const pmsg)
{
    DBG_ASSERT(pmsg != NULL __DBG_LINE);
    switch (pmsg->sig)
    {
    case M_MAC_MONI_SCAN_EVENT:
        mac_mointer_scan_handler();
        break;

    case M_MAC_MONI_SYNC_EVENT:
        mac_moniter_sync_handler();
        break;

    case M_MAC_MONI_STARTUP_EVENT:
        mac_moniter_startup_handler();
        break;

    case M_MAC_MONI_STOP_EVENT:
        mac_moniter_stop();
        break;

    case M_MAC_MONI_START_EVENT:
        mac_moniter_start();
        break;

    case M_MAC_MONI_RESTART_EVENT:
        // 寻SSN网一次，成功则保持在线，失败则关闭SSN
    	mac_moniter_restart();
    	break;

    default:
        break;
    }
}

static bool_t check_vaild_channel(uint8_t ch)
{
    if((ch <= 20) && (ch >= 1))
    {
        return TRUE;
    }
    
    return FALSE;
}


void mac_moniter_init(void)
{
    mac_intra_ch_index = 0;

    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_SCAN_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_SYNC_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_STARTUP_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_STOP_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_START_EVENT);
	module_bind_event(m_mac_moniter_handler, M_MAC_MONI_RESTART_EVENT);
    
    nfc_data_info_t nfc_data;
    device_info_t device_info = hal_board_info_look();
    
    nfc_read_info(&nfc_data, APP_TYPE_SSN_DEV_INFO, PARA_TYPE_ALL_SSN_INFO);
    if((nfc_data.nfc_device_ssn_info_pld.ssn_info.monitor_timer_len != 0xFF)
        && (nfc_data.nfc_device_ssn_info_pld.ssn_info.monitor_timer_len != 0))
    {
        mac_scan_interval_time = 
            nfc_data.nfc_device_ssn_info_pld.ssn_info.monitor_timer_len;
    }
    
    if((nfc_data.nfc_device_ssn_info_pld.ssn_info.duty_ratio != 0xFF) && 
        (nfc_data.nfc_device_ssn_info_pld.ssn_info.duty_ratio != 0))
    {
        mac_moniter_interval_time = mac_scan_interval_time *
            device_info.intra_ch_cnt *
            nfc_data.nfc_device_ssn_info_pld.ssn_info.duty_ratio;
    }

    // read rf ch from nfc data
    for(uint8_t i=0;i<sizeof(device_info.intra_ch); i++)
    {
        if(check_vaild_channel(nfc_data.nfc_device_ssn_info_pld.ssn_info.intra_ch[i]))
        {
            device_info.intra_ch[i] = 
                nfc_data.nfc_device_ssn_info_pld.ssn_info.intra_ch[i];
            device_info.intra_ch_cnt = i+1;
        }
    }
    device_info.intra_ch_cnt = 1;
    
    hal_board_info_save(&device_info, TRUE);
    
//    mac_moniter_start();
}

uint32_t mac_moniter_interval_time_get(void)
{
    return (uint32_t)(1.5*mac_moniter_interval_time);
}
