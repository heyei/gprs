/***************************************************************************
* File        : app_frames.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __APP_FRAME_H__
#define __APP_FRAME_H__

#include <phy_packet.h>
#include <mac_frames.h>
#include <nwk_frames.h>


#define FRAME_APP_OFFSET_LEN    (PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE +       \
                                 MAC_HEAD_SEQ_SIZE + MAC_ADDR_SHORT_SIZE*2  \
                                 + NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE*2)

#define APP_FRM_TYPE_DATA                   (0x00u)
#define APP_FRM_TYPE_ACK                    (0x01u)
#define APP_FRM_TYPE_CONFIG                 (0x02u)
#define APP_FRM_TYPE_QUIRE                  (0x03u)
#define APP_FRM_TYPE_QUIRE_REPLY            (0x04u)
#define APP_FRM_TYPE_TEST                   (0x05u)
#define APP_FRM_TYPE_RESET                  (0X06u)

#define APP_TYPE_DATA_NONE                  (0x00u)
#define APP_TYPE_DATA_DHT                   (0x01u)
#define APP_TYPE_DATA_ACCE                  (0x02u)
#define APP_TYPE_DATA_USER                  (0x03u)

#define APP_TYPE_CONFIG_HEART_CYCLE         (0x00u)

#define APP_TYPE_QUIRE_HEART_CYCLE          (0x00u)
#define APP_TYPE_QUIRE_ENERGY               (0x01u)

typedef struct _app_frm_ctrl_t_
{
    uint8_t frm_type    : 3,
            ack_request : 1,
            data_type   : 4;
    uint8_t seq;
} app_frm_ctrl_t;

void app_frm_data_handler(uint8_t *buf, uint8_t len, uint8_t data_type);

void app_frm_ack_handler(uint8_t seq);

void app_frm_config_handler(uint8_t *buf, uint8_t len);

void app_frm_quire_handler(uint8_t *buf, uint8_t len, uint8_t seq);

void app_send_ack(uint8_t seq);

void app_send_data(uint8_t *buf, uint8_t len, uint8_t type, uint8_t mode);
#endif
