/***************************************************************************
* File        : app_cmd.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __APP_CMD_H__
#define __APP_CMD_H__

#include <data_type_def.h>

#define APP_CMD_SIZE_MAX                    42u

#define SERIAL_LEN_MAX                     (44u)   // 协议中最大帧长
#define SERIAL_LEN_MIN                     (6u)    // 协议中最小帧长

#define CMD_AT_HEAD_SIZE                    2u
#define CMD_CR                              0x0D
#define CMD_LF                              0x0A

#define CMD_SYSSTART                        "SYSSTART"
#define CMD_OK                              "OK"

#define CMD_ERROR00                         "ERROR00"
#define CMD_ERROR01                         "ERROR01"
#define CMD_ERROR02                         "ERROR02"
#define CMD_ERROR03                         "ERROR03"

#define CMD_TEST_TYPE                       0x0D

#define CMD_INTRA_TYPE                      'S'
#define CMD_EXTER_TYPE                      '&'

#define CMD_EXTER_VERSION_TYPE              "V"
#define CMD_EXTER_RESTORE_TYPE              "F"
#define CMD_EXTER_RESTART_TYPE              "R"
#define CMD_EXTER_WAKEUP_TYPE               "WO"
#define CMD_EXTER_SLEEP_TYPE                "WS"
#define CMD_EXTER_SEND_DATA_TYPE            "SD"
#define CMD_EXTER_RECV_DATA_TYPE            "RD"

/**
 * CMD_EXTER_VERSION_TYPE
 */
#define CMD_EXTER_VERSION_UNIQUE_ID         "Unique ID:"

#define CMD_INTRA_POWER_TYPE                "100"
#define CMD_INTRA_CHANNEL_TYPE              "198"
#define CMD_INTRA_ID_TYPE                   "199"

void app_cmd_send(uint8_t *buf, uint8_t len);

void app_serial_recv_handler(void);

void app_interface_config(void);

#endif

 