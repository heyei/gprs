/***************************************************************************
* File        : app.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __APP_H__
#define __APP_H__

void app_init(void);

#endif
