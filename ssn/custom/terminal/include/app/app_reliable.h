#ifndef __APP_RELIABLE_H__
#define __APP_RELIABLE_H__

#include <lib.h>
#include <sbuf.h>
#include <hal_timer.h>

#define RELIABLE_TRANS_BUFF_SIZES           (5)

typedef struct _reliable_buf_t_
{
    list_head_t list;

    sbuf_t *sbuf;
    uint8_t *data_p;
    uint8_t data_len;

    hal_timer_t *timeout_timer;     // indicate that retran data when no repond
    uint8_t reliable_type;          // reliable or unreliable flag
    uint8_t seq;
} reliable_buf_t;


void reliable_module_init(void);

void reliable_module_stop(void);

void reliable_module_send(uint8_t *buf, uint8_t len, uint8_t type, uint8_t seq);

void reliable_module_tx_handle(sbuf_t *sbuf);

void reliable_ack_handle(uint8_t seq);

void reliable_timeout_timer_handle(void *p);

#endif
