/***************************************************************************
* File        : app_handles.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __APP_HEANDLES_H__
#define __APP_HEANDLES_H__

#include <sbuf.h>

void app_set_inline_status(bool_t res);

bool_t app_get_inline_status(void);

/**
 * [ascii_to_hex :change ascii to hex]
 * @param  hi  [the hi of ascii]
 * @param  lo  [the low of asscii]
 * @param  hex [reuslt]
 * @return     []
 */
bool_t ascii_to_hex(uint8_t hi, uint8_t lo, uint8_t *hex);

/**
 * [hex_to_ascii :change hex to ascii, save in buf]
 * @param  buf [where the result save]
 * @param  dat [which hex ]
 * @return     []
 */
bool_t hex_to_ascii(uint8_t *buf, uint8_t dat);

/**
 * [data_remove_char :remove ch from src_buf, and save the result into dst_buf]
 * @param  dst_buf [the databuf to save result]
 * @param  src_buf [the src_buf input]
 * @param  ch      [which char will to be remove]
 * @return uint8_t []
 */
uint8_t data_remove_char(uint8_t *dst_buf, uint8_t *src_buf, uint8_t ch);

/**
 * [data_find_char :find char in the databuf, and return the index]
 * @param  src_buf [the buf which save data]
 * @param  ch      [the char for find]
 * @param  len     [the index of ch in the data]
 * @param  return  [TRUE :mean find]
 */
bool_t data_find_char(uint8_t *src_buf, uint8_t ch, uint8_t *len);

void nwk2app_data_confirm(sbuf_t *sbuf);

void nwk2app_data_indication(sbuf_t *sbuf);

void app_data_resend(sbuf_t *sbuf);

#endif
