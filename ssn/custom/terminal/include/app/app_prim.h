/***************************************************************************
* File        : app_prim.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __APP_PRIM_H__
#define __APP_PRIM_H__

#include <prim.h>
#include <sbuf.h>

void app_prim_handler(sbuf_t *sbuf);

#endif

