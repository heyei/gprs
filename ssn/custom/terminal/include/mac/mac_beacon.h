/***************************************************************************
* File        : mac_beacon.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_BEACON_H__
#define __MAC_BEACON_H__

#include <data_type_def.h>
#include <pbuf.h>

#define LOST_CORRD_CNT_MAX          (3u)

#pragma pack(1)

typedef struct _superframe_spec_t_
{
    uint8_t  bcn_interval_order     : 5,    // ms
             bcn_duration_order     : 3;    // GTS1 ~ GTSn, exponent

    uint32_t downlink_slots_len     : 3,
             gts_duration_order     : 5,
             intra_channel          : 4,
             cluster_num            : 4,
             intra_gts_num          : 7,
             inter_channel          : 4,
             inter_unit_num         : 3,
             reserverd              : 2;
} superframe_spec_t;

typedef struct
{
    uint8_t short_addr_nums     : 3,
            addr1_reserved      : 1,
            exter_addr_nums     : 3,
            addr2_reserved      : 1;
} pend_addr_spec_t;

typedef struct
{
    uint8_t  index   : 4,       /* 所使用的信标发送时隙 */
             hops    : 4;       /* 距离sink的跳数 */
    uint32_t time_stamp;        /* 携带的时间戳 */
} bcn_payload_t;

#pragma pack()

extern uint8_t inter_gts_num_array[MAX_HOPS];

void mac_clr_pend_me_flag(void);

/**
 * [mac_get_pend_flag : this superframe wheather can get downlink]
 *
 * @return  [true : can get downlink]
 */
bool_t mac_get_pend_me_flag(void);

/**
 * [mac_recv_beacon description]
 * @param pbuf [description]
 */
void mac_recv_beacon(pbuf_t *pbuf);

/**
 * [mac_get_beacon_map : ]
 * @return  [beacon map]
 */
uint16_t mac_get_beacon_map(void);

bool_t mac_clr_beacon_map(void);

bool_t mac_lost_net_jodge(void);

/**
 * [mac_sync_config : DEV just sync from target]
 * @param target_id [description]
 */
void mac_sync_config(uint16_t target_id);

uint8_t mac_get_mine_hops(void);
/**
 * [mac_beacon_init : ]
 */
void mac_beacon_init(void);


#endif
