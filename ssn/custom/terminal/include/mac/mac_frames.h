/***************************************************************************
* File        : mac_frames.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_FRAMES_H__
#define __MAC_FRAMES_H__

#include <data_type_def.h>
/**
 * MAC frame type definition
 */

#define MAC_FRAME_TYPE_BEACON           0u
#define MAC_FRAME_TYPE_DATA             1u
#define MAC_FRAME_TYPE_ACK              2u
#define MAC_FRAME_TYPE_COMMAND          3u
#define MAC_FRAME_TYPE_RESERVED         4u

/**
 * Stack
 */
#define ADDR_MODE_SHORT                 0x02
#define ADDR_MODE_LONG                  0x03



#define MAC_FCS_SIZE                    0u
#define MAC_HEAD_CTRL_SIZE              2u
#define MAC_HEAD_SEQ_SIZE               1u
#define MAC_ADDR_SHORT_SIZE             2U
#define MAC_ADDR_LONG_SIZE              8U

#define MAC_BROADCAST_ADDR              0xFFFF

#pragma pack(1)

/**
 * MAC frame control field struct definition
 */
typedef struct
{
    uint16_t frm_type           :    3,             // frame type;
             sec_enable         :    1,             // security enalbled;
             frm_pending        :    1,             // frame pending;
             ack_req            :    1,             // ACK request;
             des_addr_mode      :    2,             // dest addressing mode;
             src_addr_mode      :    2,             // soure addressing mode
             reseverd           :    6;             // reseverd
} mac_frm_ctrl_t;


#pragma pack()

void mac_frames_clr_pend_flag(void);

bool_t mac_frames_get_pend_flag(void);

void mac_frames_init(void);

#endif
