/***************************************************************************
* File        : mac.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_H
#define __MAC_H

#include <hal.h>

void mac_init(void);

#endif

