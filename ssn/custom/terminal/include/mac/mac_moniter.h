/***************************************************************************
* File        : mac_moniter.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_MONITER_H__
#define __MAC_MONITER_H__

#include <mac_neighbors.h>

#define RESTART_DELAY_TIME          (20)

#define MAC_SCAN_CYCLE_TIME         (6)
#define MAC_STARTUP_INTER_CNT       (3)

#define MAC_MONITER_INTERVAL_TIME   (MAC_SCAN_CYCLE_TIME*15)

extern neighbor_node_t coord;

extern bool_t moniter_state;

uint32_t mac_moniter_scan_time_get(void);

void mac_moniter_scan_time_set(uint32_t time);

void mac_moniter_init(void);

uint32_t mac_moniter_interval_time_get(void);

#endif
