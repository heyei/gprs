/***************************************************************************
* File        : mac_prim.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 * @DESCRIPTION: mac层原语解析头文件
 *
 * @FILE: $FILE_FNAME$
 *
 * @CREATED: 2011-8-18, by chenggang
 *
 * @{
 */
#ifndef __MAC_PRIM_H__
#define __MAC_PRIM_H__

#define ASSOC_REQUEST_CNT_MAX       (4u)

void m_prim_init(void);

void mac_assoc_timer_cancel(void);
 
#endif
