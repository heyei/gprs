/***************************************************************************
* File        : mac_global.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_GLOBAL_H__
#define __MAC_GLOBAL_H__

#include <sbuf.h>
#include <mac_beacon.h>

#define EMPTY_ADDR                      0x0000
#define MAX_PHY_PACKET_SIZE             127

typedef enum
{
    MAIN_STATE_IDLE = 0,
    MAIN_STATE_SCAN,
    MAIN_STATE_SCHEDULING,
} mac_mainstates_t;

typedef enum
{
    SUB_STATE_IDLE = 0,
    SUB_STATE_ASSOCIATING,
    SUB_STATE_ASSOCIATION_OK,
} mac_substates_t;

typedef struct
{
    mac_mainstates_t ms;
    mac_substates_t ss;
} mac_state_t;

/* mac_pib.h*/
typedef struct
{
    uint8_t self_index;
    uint8_t mac_bsn;
    uint8_t mac_dsn; 
    uint8_t hops;
    uint64_t mac_coord_long_addr;  // coord IEEE address
    uint16_t mac_coord_short_addr;
    uint64_t mac_long_addr;        // self IEEE address
    uint16_t mac_short_addr;
    superframe_spec_t supfrm_cfg_arg;
} mac_pib_t;


typedef struct
{
    list_head_t list;
    sbuf_t *sbuf;
} data_buf_t;

#pragma pack(1)
typedef struct 
{
    uint8_t     num;
    uint16_t    src_addr;
    int8_t      lqi;
    uint32_t    lqi_timestamp;
} local_info_t;
#pragma pack()


extern list_head_t gts_buf[1];          // for my gts

extern mac_pib_t mac_pib;
extern mac_state_t mac_state;
extern uint64_t node_nui;

void mac_global_init(void);
void mac_global_reset(void);
void mac_set_local_info(pbuf_t *pbuf);
local_info_t mac_get_local_info(void);

#endif

