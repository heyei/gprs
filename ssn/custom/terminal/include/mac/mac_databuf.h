/***************************************************************************
* File        : mac_databuf.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_DATABUF_H__
#define __MAC_DATABUF_H__

#include <sbuf.h>

/**
 * [mac_databuf_insert : insert sbuf into GTS buff list]
 * @param sbuf [the data to be insert]
 * @param flag [true : insert forward; false : insert tail]
 */
void mac_databuf_insert(sbuf_t *sbuf, bool_t flag);

/**
 * [mac_databuf_remove : get data from buf, and remove it from list]
 * @param  index [the GTS number]
 * @return       [pointer to sbuf]
 */
sbuf_t *mac_databuf_remove(uint16_t index);

/**
 * [mac_databuf_init : init databuf module]
 */
bool_t mac_databuf_init(void);

bool_t mac_databuf_active(void);

bool_t mac_databuf_pause(void);

#endif
