/***************************************************************************
* File        : nwk_handles.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __NWK_HANDLES_H__
#define __NWK_HANDLES_H__

#include <sbuf.h>
#include <data_type_def.h>

extern uint16_t send_success_cnt;

void nwk_set_join_state(bool_t res);

bool_t nwk_get_join_state(void);

void nwk_heart_restart(void);

void app2nwk_data_request(sbuf_t *sbuf);

void mac2nwk_data_indication(sbuf_t *sbuf);

void mac2nwk_data_confirm(sbuf_t *sbuf);

void mac2nwk_assoc_confirm(sbuf_t *sbuf);

bool_t heart_beat_send(uint8_t heart_alarm_type);

bool_t heart_beat_handler(uint8_t heart_alarm_type);

#endif
