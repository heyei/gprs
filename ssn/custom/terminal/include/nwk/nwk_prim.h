/***************************************************************************
* File        : nwk_prim.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __NWK_PRIM_H__
#define __NWK_PRIM_H__

#include <sbuf.h>

void nwk_prim_handler(sbuf_t *sbuf);

#endif

