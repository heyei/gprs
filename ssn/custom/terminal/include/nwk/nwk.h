/***************************************************************************
* File        : nwk.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __NWK_H__
#define __NWK_H__

#include <data_type_def.h>

extern nwk_id_t my_nwk_addr;

void nwk_init(void);

#endif
