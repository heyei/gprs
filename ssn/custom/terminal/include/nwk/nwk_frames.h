/***************************************************************************
* File        : nwk_frames.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __NWK_FRAMES_H
#define __NWK_FRAMES_H

#include <lib.h>
#include <data_type_def.h>
#include <sbuf.h>

#define NWK_HEAD_SIZE                   1
#define NWK_ADDR_SHORT_SIZE             2
#define NWK_ADDR_LONG_SIZE              8

#define MAX_NWK_PAYLOAD                 100

#define NWK_SINK_ADDR                   0xFFFE
#define NWK_EMPTY_ADDR                  0xFFFF
#define NWK_BROADCAST_ADDR              0xFFFF

#define NWK_FRM_TYPE_DATA               0
#define NWK_FRM_TYPE_JOIN_REQ           1
#define NWK_FRM_TYPE_JOIN_REPLY         2
#define NWK_FRM_TYPE_HEART_BEAT         3
#define NWK_FRM_TYPE_RESERVED           4

#define NWK_ADDR_MODE_NONE              0
#define NWK_ADDR_MODE_SHORT             2
#define NWK_ADDR_MODE_LONG              3

/**
 * HEART 
 */
#define NWK_HEART_ALARM_SIZE            2
#define NWK_HEART_RES_ENERGY_SIZE       1
#define NWK_HEART_TOPOLOGY_SIZE         2

#define NWK_HEART_ALARM_NONE            0
#define NWK_HEART_ALARM_RESTART         1
#define NWK_HEART_ALARM_REJOIN          2

#define NWK_HEART_ENERGY_NONE           0
#define NWK_HEART_ENERGY_ACTIVE         1
#define NWK_HEART_ENERGY_BATTERY        2

#pragma pack(1)

typedef struct
{
    uint8_t  frm_ctrl        : 3,
             dst_mode        : 2,
             src_mode        : 2,
             reserved        : 1;
} nwk_hd_ctl_t;

typedef struct
{
    uint8_t device_type     : 1,
            reserved        : 7;
    uint32_t join_apply_time;
} join_req_t;

typedef struct
{
    uint8_t device_type      : 2,
            energy_support   : 2,
            alarm_type       : 2,
            local_support    : 1,
            overhear_support : 1;
} hb_ctrl_t;

#pragma pack()

typedef struct
{
    nwk_hd_ctl_t nwk_hd_ctl;
    uint64_t dst_addr;
    uint64_t src_addr;
} nwk_hd_t;

void nwk_resend_data_packet(sbuf_t *sbuf);

void nwk_send_data_packet(sbuf_t *sbuf, uint8_t mac_src_mode, uint8_t len);

uint8_t nwk_fill_data_buf(pbuf_t *pbuf);

uint8_t nwk_fill_join_req(pbuf_t *pbuf);

uint8_t nwk_fill_heart_beat(pbuf_t *pbuf, uint8_t heart_alarm_type);

void nwk_frame_parse(nwk_hd_t *nwk_hd, sbuf_t *sbuf);

#endif
