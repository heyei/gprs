/***************************************************************************
* File        : app.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <osel_arch.h>
#include <debug.h>
#include <sbuf.h>
#include <app.h>
#include <app_prim.h>
#include <app_handles.h>
#include <app_cmd.h>
#include <app_reliable.h>
/*#include <app_sensor.h>*/
#include <ssn.h>

static void app_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t *)e;
    uint8_t status = app_get_inline_status();
    
    switch (p->sig)
    {
    case APP_PRIM_EVENT:
        app_prim_handler((sbuf_t *)(p->param));
        break;

    case APP_INLINE_EVENT:
        app_set_inline_status((bool_t)((uint32_t)(p->param)));
        if(app_get_inline_status())
        {
            if(status == FALSE)
            {
                ssn_open_handle(TRUE);
            }
        }
        else
        {
            //reliable_module_stop();
            ssn_close_handle(TRUE);
        }
        break;

    case APP_SERIAL_DATA_EVENT:
        app_serial_recv_handler();
        break;
        
    case APP_RELIABLE_EVENT:
        reliable_timeout_timer_handle(p->param);
        break;

    default:
        break;
    }
}

//static void ssn_send_cb(uint8_t *data, uint8_t len, uint8_t res, uint8_t mode)
//{
//    _NOP();
//}

//static void ssn_receive_cb(uint8_t *data, uint8_t len)
//{
//    ;
//}

void app_init(void)
{
    osel_task_tcb *app_task_handle = osel_task_create(&app_task, APP_TASK_PRIO);

    osel_subscribe(app_task_handle, APP_PRIM_EVENT);
    osel_subscribe(app_task_handle, APP_INLINE_EVENT);
    osel_subscribe(app_task_handle, APP_SERIAL_DATA_EVENT);
    osel_subscribe(app_task_handle, APP_RELIABLE_EVENT);

    /*app_interface_config();*/
    app_set_inline_status(FALSE);
    
    // for test
    reliable_module_init();
}