/***************************************************************************
* File        : app_frames.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <hal_board.h>
#include <hal_timer.h>
#include <pbuf.h>
#include <m_sync.h>
#include <nwk_handles.h>
#include <app_cmd.h>
#include <app_handles.h>
#include <app_reliable.h>
#include <app_frames.h>
/*#include <app_sensor.h>*/
#include <ssn.h>
#include <hal_energy.h>

typedef struct
{
    uint8_t hb_flag          : 1,
            energy_flag      : 1,
            reserved         : 6;
} app_quire_param_t;

static uint8_t app_seq_num = 0;

void app_frm_data_handler(uint8_t *buf, uint8_t len, uint8_t data_type)
{
    if (data_type == APP_TYPE_DATA_USER)
    {
        ssn_read_handle(buf, len);
    }
}

void app_frm_ack_handler(uint8_t seq)
{
    reliable_ack_handle(seq);
}


void app_frm_config_handler(uint8_t *buf, uint8_t len)
{
    uint8_t *data_p = buf;

    uint8_t num = *data_p;
    data_p += sizeof(uint8_t); len -= sizeof(uint8_t);

    device_info_t board_info = hal_board_info_look();

    for (uint8_t i = 0; i < num; i++)
    {
        uint8_t config_type = *data_p;
        data_p += sizeof(uint8_t); len -= sizeof(uint8_t);
        uint8_t config_len = *data_p;
        data_p += sizeof(uint8_t); len -= sizeof(uint8_t);

        if (config_type == APP_TYPE_CONFIG_HEART_CYCLE)
        {
            if (config_len <= sizeof(board_info.heartbeat_cycle))
            {
                osel_memcpy((uint8_t *) & (board_info.heartbeat_cycle), data_p
                            , config_len);
                hal_board_info_save(&board_info, FALSE);
                nwk_heart_restart();
            }
        }
        else
        {
            ;
        }

        data_p += config_len; len -= config_len;
    }
}

static void app_send_quire_reply(uint8_t seq, app_quire_param_t *quire_param)
{
    device_info_t device_info = hal_board_info_look();

    app_frm_ctrl_t app_frm_ctrl;
    app_frm_ctrl.frm_type = APP_FRM_TYPE_QUIRE_REPLY;
    app_frm_ctrl.ack_request = FALSE;
    app_frm_ctrl.data_type = 0x00;
    app_frm_ctrl.seq = seq;

    uint8_t app_payload[APP_CMD_SIZE_MAX];
    uint8_t *data_p = &app_payload[0];
    osel_memcpy(data_p, (uint8_t *)&app_frm_ctrl, sizeof(app_frm_ctrl_t));
    data_p += sizeof(app_frm_ctrl_t);

    uint8_t *num_p = data_p;
    data_p += sizeof(uint8_t); *num_p = 0;

    if (quire_param->hb_flag)
    {
        (*num_p)++;
        *data_p = APP_TYPE_QUIRE_HEART_CYCLE; data_p += sizeof(uint8_t);
        *data_p = sizeof(device_info.heartbeat_cycle); data_p += sizeof(uint8_t);
        osel_memcpy(data_p,
                    (uint8_t *) & (device_info.heartbeat_cycle),
                    sizeof(device_info.heartbeat_cycle));
        data_p += sizeof(device_info.heartbeat_cycle);
    }

    if (quire_param->energy_flag)
    {
        (*num_p)++;
        *data_p = APP_TYPE_QUIRE_ENERGY; data_p += sizeof(uint8_t);
        *data_p = sizeof(uint8_t); data_p += sizeof(uint8_t);
        *data_p = hal_energy_get(); data_p += sizeof(uint8_t);
    }

    reliable_module_send(app_payload, (data_p - app_payload), FALSE, seq);
}


void app_frm_quire_handler(uint8_t *buf, uint8_t len, uint8_t seq)
{
    uint8_t *data_p = buf;
    uint8_t num = *data_p;
    data_p += sizeof(uint8_t); len -= sizeof(uint8_t);

    app_quire_param_t qpp_quire_param_flag;
    osel_memset(&qpp_quire_param_flag, 0x00, sizeof(app_quire_param_t));

    uint8_t temp_type = 0;
    for (uint8_t i = 0; i < num; i++)
    {
        temp_type = *data_p; data_p++;
        if (temp_type == APP_TYPE_QUIRE_HEART_CYCLE)
        {
            qpp_quire_param_flag.hb_flag = TRUE;
        }
        else if (temp_type == APP_TYPE_QUIRE_ENERGY)
        {
            qpp_quire_param_flag.energy_flag = TRUE;
        }
    }

    app_send_quire_reply(seq, &qpp_quire_param_flag);
}


void app_send_ack(uint8_t seq)
{
    app_frm_ctrl_t app_frm_ctrl;
    app_frm_ctrl.frm_type = APP_FRM_TYPE_ACK;
    app_frm_ctrl.ack_request = FALSE;
    app_frm_ctrl.data_type = 0x00;
    app_frm_ctrl.seq = seq;

    reliable_module_send((uint8_t *)&app_frm_ctrl, sizeof(app_frm_ctrl_t), FALSE, seq);
}

void app_send_data(uint8_t *buf, uint8_t len, uint8_t type, uint8_t mode)
{
    app_frm_ctrl_t app_frm_ctrl;
    app_frm_ctrl.frm_type = APP_FRM_TYPE_DATA;
    app_frm_ctrl.ack_request = mode;   
    app_frm_ctrl.data_type = type;
    app_frm_ctrl.seq = app_seq_num++;

    uint8_t app_payload[APP_CMD_SIZE_MAX];
    uint8_t *data_p = &app_payload[0];

    osel_memcpy(data_p, (uint8_t *)&app_frm_ctrl, sizeof(app_frm_ctrl_t));
    data_p += sizeof(app_frm_ctrl_t);

    // add stamp
    hal_time_t now = hal_timer_now();
    m_sync_l2g(&now);
    osel_memcpy(data_p, (uint8_t *) & (now.w), sizeof(hal_time_t));
    data_p += sizeof(hal_time_t);

    osel_memcpy(data_p, buf, len);

    reliable_module_send(app_payload, sizeof(app_frm_ctrl_t) + sizeof(hal_time_t) + len, mode, app_frm_ctrl.seq);
}

