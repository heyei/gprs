/***************************************************************************
* File        : app_handles.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <app_handles.h>
#include <app_frames.h>
#include <nwk_handles.h>
#include <app_cmd.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <ssn.h>
#include <app_reliable.h>

static bool_t app_inline_flag = FALSE;

void app_set_inline_status(bool_t res)
{
    app_inline_flag = res;
}

bool_t app_get_inline_status(void)
{
    return app_inline_flag;
}

bool_t ascii_to_hex(uint8_t hi, uint8_t lo, uint8_t *hex)
{
    *hex = 0;
    if ((hi >= 0x30) && (hi <= 0x39))
    {
        hi -= 0x30;
    }
    else if ((hi >= 0x41) && (hi <= 0x46))
    {
        hi -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= (hi << 4);

    if ((lo >= 0x30) && (lo <= 0x39))
    {
        lo -= 0x30;
    }
    else if ((lo >= 0x41) && (lo <= 0x46))
    {
        lo -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= lo;

    return TRUE;
}

bool_t hex_to_ascii(uint8_t *buf, uint8_t dat)
{
    uint8_t dat_buff;

    dat_buff = dat;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[1] = dat;

    dat = dat_buff;
    dat >>= 4;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[0] = dat;

    return TRUE;
}

uint8_t data_remove_char(uint8_t *dst_buf, uint8_t *src_buf, uint8_t ch)
{
    uint8_t *dst_p = dst_buf;
    uint8_t *src_p = src_buf;

    while (*src_p != 0x00)
    {
        if (*src_p != ch)
        {
            *dst_p++ = *src_p;
        }
        src_p++;
    }
    return (dst_p - dst_buf);
}

bool_t data_find_char(uint8_t *src_buf, uint8_t ch, uint8_t *len)
{
    uint8_t *src_p = src_buf;
    bool_t res = FALSE;
    while (*src_p != 0x00)
    {
        if (*src_p++ == ch)
        {
            res = TRUE;
            break;
        }
    }

    *len = (src_p - src_buf);

    return res;
}

void app_data_resend(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    n2a_data_confirm_t *n2a_data_confirm =
        &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_confirm_arg;

    uint8_t *datap   = n2a_data_confirm->nsdu;
    uint8_t data_len = n2a_data_confirm->nsdu_length;

    // this make data alway resend ,never giveup
    sbuf->primargs.pbuf->attri.already_send_times.app_send_times++;

    sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg.nsdu_length = data_len;
    sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg.nsdu = datap;

    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;

    osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}


void nwk2app_data_confirm(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    reliable_module_tx_handle(sbuf);
}

void nwk2app_data_indication(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    n2a_data_indication_t *n2a_data_ind =
        &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg;
    uint8_t *datap = n2a_data_ind->nsdu;
    uint8_t len = n2a_data_ind->nsdu_length;

    app_frm_ctrl_t app_frm_ctrl;
    osel_memcpy((uint8_t *)&app_frm_ctrl, datap, sizeof(app_frm_ctrl_t));
    datap += sizeof(app_frm_ctrl_t);
    len -= sizeof(app_frm_ctrl_t);

    switch (app_frm_ctrl.frm_type)
    {
    case APP_FRM_TYPE_DATA:
        app_frm_data_handler(datap, len, app_frm_ctrl.data_type);
        break;

    case APP_FRM_TYPE_ACK:
        app_frm_ack_handler(app_frm_ctrl.seq);
        break;

    case APP_FRM_TYPE_CONFIG:
        app_frm_config_handler(datap, len);
        break;

    case APP_FRM_TYPE_QUIRE:
        app_frm_quire_handler(datap, len, app_frm_ctrl.seq);
        break;

    case APP_FRM_TYPE_TEST:
        heart_beat_send(NWK_HEART_ALARM_NONE);
        break;

    case APP_FRM_TYPE_RESET:
        hal_board_reset();
        break;

    default:
        break;
    }

    if (app_frm_ctrl.ack_request)
    {
        app_send_ack(app_frm_ctrl.seq);
    }

}

