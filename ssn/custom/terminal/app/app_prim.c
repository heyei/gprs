/***************************************************************************
* File        : app_prim.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <app_prim.h>
#include <debug.h>
#include <app_handles.h>

void app_prim_handler(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    switch (sbuf->primtype)
    {
    case N2A_DATA_CONFIRM:      // handle of send data
        nwk2app_data_confirm(sbuf);
        break;

    case N2A_DATA_INDICATION:   // new data
        nwk2app_data_indication(sbuf);

        pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
        sbuf_free(&sbuf __SLINE2);
        break;

    default:
        pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
        sbuf_free(&sbuf __SLINE2);
        break;
    }
}
