/***************************************************************************
* File        : app_reliable.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <hal_board.h>
#include <hal_timer.h>
#include <pbuf.h>
#include <m_sync.h>
#include <nwk_handles.h>
#include <app_handles.h>
#include <app_frames.h>
#include <ssn.h>
#include <app_reliable.h>
#include <mac_schedule.h>
#include <mac_beacon.h>
#include <hal.h>

static reliable_buf_t reliable_buf[RELIABLE_TRANS_BUFF_SIZES];
static list_head_t free_reliable_buf_list;
static list_head_t alloc_reliable_buf_list;

void reliable_module_init(void)
{
    list_init(&free_reliable_buf_list);
    list_init(&alloc_reliable_buf_list);

    for (uint8_t i = 0; i < RELIABLE_TRANS_BUFF_SIZES; i++)
    {
        list_add_to_tail(&reliable_buf[i].list, &free_reliable_buf_list);
    }
}


static reliable_buf_t *relialbe_module_buf_alloc(void)
{
    hal_int_state_t s;
    if (list_empty(&free_reliable_buf_list))
    {
        return NULL;
    }

    HAL_ENTER_CRITICAL(s);
    reliable_buf_t *buf = list_entry_decap(&free_reliable_buf_list, reliable_buf_t, list);
    list_add_to_tail(&(buf->list), &alloc_reliable_buf_list);
    HAL_EXIT_CRITICAL(s);

    return buf;
}

static void reliable_module_buf_free(reliable_buf_t *buf)
{
    hal_int_state_t s;
    HAL_ENTER_CRITICAL(s);
    list_move_to_another_tail(&(buf->list), &free_reliable_buf_list);
    HAL_EXIT_CRITICAL(s);

    if (buf->timeout_timer != NULL)
    {
        hal_timer_cancel(&(buf->timeout_timer));
    }

    if(buf->sbuf->list.next != NULL)
    {
        list_del(&(buf->sbuf->list));
    }
    
    pbuf_free(&(buf->sbuf->primargs.pbuf) __PLINE2);
    sbuf_free(&(buf->sbuf) __SLINE2);
}


static reliable_buf_t *relialbe_module_buf_find(uint8_t seq)
{
    list_head_t *pos = NULL;

    list_for_each_forwards(pos, &alloc_reliable_buf_list)
    {
        reliable_buf_t *buf = list_entry_addr_find(pos, reliable_buf_t, list);

        if (buf->seq == seq)
        {
            return buf;
        }
    }

    //DBG_ASSERT(FALSE __DBG_LINE);
    return NULL;
}

void reliable_module_stop(void)
{
    if (list_empty(&alloc_reliable_buf_list))
    {
        return;
    }

    list_head_t *pos = NULL;
    list_head_t *temp = NULL;
    list_for_each_safe(pos, temp, &alloc_reliable_buf_list)
    {
        reliable_buf_t *buf = list_entry_decap(&alloc_reliable_buf_list, reliable_buf_t, list);
        uint8_t *datap  = buf->data_p;
        uint8_t len     = buf->data_len;

        app_frm_ctrl_t app_frm_ctrl;
        osel_memcpy((uint8_t *)&app_frm_ctrl, datap, sizeof(app_frm_ctrl_t));
        datap += sizeof(app_frm_ctrl_t); len -= sizeof(app_frm_ctrl_t);

        datap += sizeof(hal_time_t); len -= sizeof(hal_time_t);

        ssn_send_handle(datap, len, FALSE, UNRELIABLE_MODE);

        reliable_module_buf_free(buf);
    }
}

static void reliable_timeout_timer_cb(void *p)
{
    reliable_buf_t *buf = (reliable_buf_t *)p;
    buf->timeout_timer = NULL;

    osel_post(APP_RELIABLE_EVENT, buf, OSEL_EVENT_PRIO_LOW);
}

void reliable_timeout_timer_handle(void *p)
{
    reliable_buf_t *buf = (reliable_buf_t *)p;
    DBG_ASSERT(buf != NULL __DBG_LINE);
    DBG_ASSERT(buf->sbuf != NULL __DBG_LINE);

    if (!app_get_inline_status())
    {
        uint8_t *datap  = buf->data_p;
        uint8_t len     = buf->data_len;

        app_frm_ctrl_t app_frm_ctrl;
        osel_memcpy((uint8_t *)&app_frm_ctrl, datap, sizeof(app_frm_ctrl_t));
        datap += sizeof(app_frm_ctrl_t); len -= sizeof(app_frm_ctrl_t);

        datap += sizeof(hal_time_t); len -= sizeof(hal_time_t);

        ssn_send_handle(datap, len, FALSE, RELIABLE_MODE);

        reliable_module_buf_free(buf);

        return;
    }

    if (NULL == buf->timeout_timer)
    {
        HAL_TIMER_SET_REL(mac_get_global_superfrm_time() *
                          (mac_get_mine_hops() - 1) * 4,
                          reliable_timeout_timer_cb,
                          buf,
                          buf->timeout_timer);
        DBG_ASSERT(buf->timeout_timer != NULL __DBG_LINE);
    }

    if(list_empty(&(buf->sbuf->list)))
    {
        buf->sbuf->primargs.pbuf->attri.already_send_times.app_send_times = 0;
        app_data_resend(buf->sbuf);
    }
    else
    {
        // 数据还在mac队列里面排队为发送，如果重发会导致数据链表插入错误，
        // mac的发送队列陷入无限循环；
        _NOP(); 
    } 
}


void reliable_module_send(uint8_t *buf, uint8_t len, uint8_t type, uint8_t seq)
{
    uint8_t packet_len = FRAME_APP_OFFSET_LEN + len;
    pbuf_t *pbuf = pbuf_alloc(packet_len __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    if (sbuf == NULL)
    {
        pbuf_free(&pbuf __PLINE2);
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }

    uint8_t *datap = pbuf->head + FRAME_APP_OFFSET_LEN;

    osel_memcpy(datap, buf, len);

    pbuf->attri.already_send_times.app_send_times++;
    pbuf->data_len = len;

    sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg.nsdu_length = len;
    sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg.nsdu =
        pbuf->head + FRAME_APP_OFFSET_LEN;
    sbuf->primargs.pbuf = pbuf;
    sbuf->primtype = A2N_DATA_REQUEST;
    sbuf->orig_layer = APP_LAYER;

    reliable_buf_t *rbuf = relialbe_module_buf_alloc();
    DBG_ASSERT(NULL != rbuf __DBG_LINE);
    if (rbuf == NULL)
    {
        pbuf_free(&pbuf __PLINE2);
        sbuf_free(&sbuf __SLINE2);
        return;
    }

    rbuf->sbuf          = sbuf;
    rbuf->reliable_type = type;
    rbuf->seq           = seq;
    rbuf->data_p        = datap;
    rbuf->data_len      = len;

    if (type == RELIABLE_MODE)
    {
        if (NULL == rbuf->timeout_timer)
        {
            HAL_TIMER_SET_REL(mac_get_global_superfrm_time() *
                              (mac_get_mine_hops() - 1) * 4,
                              reliable_timeout_timer_cb,
                              rbuf,
                              rbuf->timeout_timer);
            DBG_ASSERT(rbuf->timeout_timer != NULL __DBG_LINE);
        }
    }

    osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}


void reliable_module_tx_handle(sbuf_t *sbuf)
{
    n2a_data_confirm_t *n2a_data_confirm =
        &sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_confirm_arg;

    bool_t status   = n2a_data_confirm->status;
    uint8_t *datap  = n2a_data_confirm->nsdu;
    uint8_t len     = n2a_data_confirm->nsdu_length;
    uint8_t app_times = sbuf->primargs.pbuf->attri.already_send_times.app_send_times;

    app_frm_ctrl_t app_frm_ctrl;
    osel_memcpy((uint8_t *)&app_frm_ctrl, datap, sizeof(app_frm_ctrl_t));
    datap += sizeof(app_frm_ctrl_t);
    len -= sizeof(app_frm_ctrl_t);

    datap += sizeof(hal_time_t);
    len -= sizeof(hal_time_t);

    if (app_frm_ctrl.ack_request == RELIABLE_MODE)
    {
        if (!status)
        {
            if (app_times < APP_MAX_SEND_TIMES)
            {
                app_data_resend(sbuf);
            }
        }
    }
    else
    {
        if (status)
        {
            if ((app_frm_ctrl.frm_type == APP_FRM_TYPE_DATA)
                    && (app_frm_ctrl.data_type == APP_TYPE_DATA_USER))
            {
                ssn_send_handle(datap, len, status, UNRELIABLE_MODE);
            }

            reliable_buf_t *buf = relialbe_module_buf_find(app_frm_ctrl.seq);
            if (buf != NULL)
            {
                reliable_module_buf_free(buf);
            }
        }
        else
        {
            if (app_times >= APP_MAX_SEND_TIMES)
            {
                if ((app_frm_ctrl.frm_type == APP_FRM_TYPE_DATA)
                        && (app_frm_ctrl.data_type == APP_TYPE_DATA_USER))
                {
                    ssn_send_handle(datap, len, status, UNRELIABLE_MODE);
                }

                reliable_buf_t *buf = relialbe_module_buf_find(app_frm_ctrl.seq);
                if (buf != NULL)
                {
                    reliable_module_buf_free(buf);
                }
            }
            else
            {
                app_data_resend(sbuf);
            }
        }
    }
}

void reliable_ack_handle(uint8_t seq)
{
    reliable_buf_t *buf = relialbe_module_buf_find(seq);
    
    if (buf != NULL)
    {
        uint8_t *datap  = buf->data_p;
        uint8_t len     = buf->data_len;

        app_frm_ctrl_t app_frm_ctrl;
        osel_memcpy((uint8_t *)&app_frm_ctrl, datap, sizeof(app_frm_ctrl_t));
        datap += sizeof(app_frm_ctrl_t); len -= sizeof(app_frm_ctrl_t);

        datap += sizeof(hal_time_t); len -= sizeof(hal_time_t);

        ssn_send_handle(datap, len, TRUE, RELIABLE_MODE);
        reliable_module_buf_free(buf);
    }
}




