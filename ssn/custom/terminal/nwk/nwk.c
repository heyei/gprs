/***************************************************************************
* File        : nwk.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <debug.h>
#include <osel_arch.h>
#include <nwk.h>
#include <nwk_prim.h>
#include <nwk_frames.h>
#include <nwk_handles.h>

nwk_id_t my_nwk_addr = 0x0000;

static void nwk_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t *)e;
    switch (p->sig)
    {
    case MAC2NWK_PRIM_EVENT:
    case APP2NWK_PRIM_EVENT:
        nwk_prim_handler((sbuf_t *)(p->param));
        break;

    case NWK_HEART_EVENT:
        heart_beat_handler(NWK_HEART_ALARM_NONE);
        break;

    case NWK_INLINE_EVENT:
        nwk_set_join_state((bool_t)((uint32_t)(p->param)));
        break;

    default:
        break;
    }
}

void nwk_init(void)
{
    osel_task_tcb *nwk_task_handle = osel_task_create(&nwk_task, NWK_TASK_PRIO);

    osel_subscribe(nwk_task_handle, MAC2NWK_PRIM_EVENT);
    osel_subscribe(nwk_task_handle, APP2NWK_PRIM_EVENT);
    osel_subscribe(nwk_task_handle, NWK_HEART_EVENT);
    osel_subscribe(nwk_task_handle, NWK_INLINE_EVENT);
}
