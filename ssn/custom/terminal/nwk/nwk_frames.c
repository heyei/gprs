/***************************************************************************
* File        : nwk_frames.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <hal_timer.h>
#include <hal_board.h>
#include <phy_packet.h>
#include <m_sync.h>
#include <mac_frames.h>
#include <mac_global.h>
#include <mac_databuf.h>
#include <nwk.h>
#include <nwk_frames.h>
#include <nwk_handles.h>
#include <hal_energy.h>

static uint16_t send_frames_cnt = 0;

static uint16_t sink_nwk_address = NWK_SINK_ADDR;

void nwk_send_data_packet(sbuf_t *sbuf, uint8_t mac_src_mode, uint8_t len)
{
    uint8_t mac_src_addr_len = 0;
    uint8_t mac_dst_addr_len = MAC_ADDR_SHORT_SIZE;

    n2m_data_request_t *data_req =
        &(sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg);
    data_req->src_mode = mac_src_mode;
    data_req->dst_mode = ADDR_MODE_SHORT;
    data_req->dst_addr = mac_pib.mac_coord_short_addr;

    if (mac_src_mode == ADDR_MODE_SHORT)
    {
        mac_src_addr_len = MAC_ADDR_SHORT_SIZE;
    }
    else if (mac_src_mode == ADDR_MODE_LONG)
    {
        mac_src_addr_len = MAC_ADDR_LONG_SIZE;
    }
    pbuf_t *pbuf = sbuf->primargs.pbuf;
    pbuf->attri.already_send_times.nwk_send_times++;
    data_req->msdu = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE +
                     MAC_HEAD_SEQ_SIZE + mac_dst_addr_len + mac_src_addr_len;
    data_req->msdu_length = len;

    send_frames_cnt++;
    sbuf->primtype = N2M_DATA_REQUEST;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

void nwk_resend_data_packet(sbuf_t *sbuf)
{
    uint8_t len =
        sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg.msdu_length;
    uint8_t mac_src_mode =
        sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg.src_mode;

    nwk_send_data_packet(sbuf, mac_src_mode, len);
}

uint8_t nwk_fill_data_buf(pbuf_t *pbuf)
{
    uint8_t len = 0;
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE
                   + MAC_HEAD_SEQ_SIZE + MAC_ADDR_SHORT_SIZE * 2;

    nwk_hd_ctl_t nwk_hd_ctl;
    nwk_hd_ctl.frm_ctrl = NWK_FRM_TYPE_DATA;
    nwk_hd_ctl.dst_mode = NWK_ADDR_MODE_SHORT;
    nwk_hd_ctl.src_mode = NWK_ADDR_MODE_SHORT;
    osel_memcpy(pbuf->data_p, &nwk_hd_ctl, sizeof(nwk_hd_ctl_t));
    pbuf->data_p += NWK_HEAD_SIZE;

    osel_memcpy(pbuf->data_p, &sink_nwk_address, NWK_ADDR_SHORT_SIZE);
    pbuf->data_p += NWK_ADDR_SHORT_SIZE;
    osel_memcpy(pbuf->data_p, &my_nwk_addr, NWK_ADDR_SHORT_SIZE);
    pbuf->data_p += NWK_ADDR_SHORT_SIZE;

    len = NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE + NWK_ADDR_SHORT_SIZE;
    pbuf->data_len += len;

    pbuf->attri.need_ack = TRUE;

    return len;
}

uint8_t nwk_fill_join_req(pbuf_t *pbuf)
{
    uint8_t len = 0;
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE
                   + MAC_HEAD_SEQ_SIZE + MAC_ADDR_SHORT_SIZE + MAC_ADDR_LONG_SIZE;

    nwk_hd_ctl_t nwk_hd_ctl;
    nwk_hd_ctl.frm_ctrl = NWK_FRM_TYPE_JOIN_REQ;
    nwk_hd_ctl.dst_mode = NWK_ADDR_MODE_SHORT;
    nwk_hd_ctl.src_mode = NWK_ADDR_MODE_LONG;
    osel_memcpy(pbuf->data_p, &nwk_hd_ctl, sizeof(nwk_hd_ctl_t));
    pbuf->data_p += NWK_HEAD_SIZE;

    osel_memcpy(pbuf->data_p, &sink_nwk_address, NWK_ADDR_SHORT_SIZE);
    pbuf->data_p += NWK_ADDR_SHORT_SIZE;

    osel_memcpy(pbuf->data_p, &node_nui, NWK_ADDR_LONG_SIZE);
    pbuf->data_p += NWK_ADDR_LONG_SIZE;

    join_req_t join_req;
    join_req.device_type = NODE_TYPE_TAG;
    hal_time_t now = hal_timer_now();
    m_sync_l2g(&now);
    join_req.join_apply_time = now.w;
    osel_memcpy(pbuf->data_p, (uint8_t *)&join_req, sizeof(join_req_t));
    pbuf->data_p += sizeof(join_req_t);

    len = NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE + NWK_ADDR_LONG_SIZE
          + sizeof(join_req_t);
    pbuf->data_len += len;

    pbuf->attri.need_ack = TRUE;

    return len;
}

static uint8_t heart_beat_fill(pbuf_t *pbuf, uint8_t heart_alarm_type)
{
    uint8_t len = 0;
    uint16_t debug_assert_info = 0;
    hb_ctrl_t hb_ctrl;

    hb_ctrl.device_type = 0; // tag device_type
    hb_ctrl.energy_support = NWK_HEART_ENERGY_BATTERY;
    hb_ctrl.alarm_type = heart_alarm_type;
    hb_ctrl.local_support = TRUE;
    hb_ctrl.overhear_support = FALSE;

    osel_memcpy(pbuf->data_p, (uint8_t *)&hb_ctrl, sizeof(hb_ctrl_t));
    pbuf->data_p += sizeof(hb_ctrl_t); len += sizeof(hb_ctrl_t);

    osel_memcpy(pbuf->data_p, (uint8_t *)&mac_pib.mac_coord_short_addr, sizeof(uint16_t));
    pbuf->data_p += sizeof(uint16_t); len += sizeof(uint16_t);

    // one byte for transmission successful rate
    fp32_t rate = ((fp32_t)send_success_cnt) / send_frames_cnt;
    uint8_t tran_succ_rate = (send_frames_cnt > 0) ? (uint8_t)(rate * 100) : (100);
    tran_succ_rate = tran_succ_rate>100 ? 100 : tran_succ_rate;
    send_success_cnt = 0; send_frames_cnt = 0;
    
    osel_memcpy(pbuf->data_p, (uint8_t *)&tran_succ_rate, sizeof(uint8_t));
    pbuf->data_p += sizeof(uint8_t); len += sizeof(uint8_t);

    if (hb_ctrl.energy_support == NWK_HEART_ENERGY_BATTERY)
    {
        uint8_t energy_power = hal_energy_get();
        osel_memcpy(pbuf->data_p, &energy_power, NWK_HEART_RES_ENERGY_SIZE);
        pbuf->data_p += NWK_HEART_RES_ENERGY_SIZE;
        len += NWK_HEART_RES_ENERGY_SIZE;
    }

    if (hb_ctrl.alarm_type == NWK_HEART_ALARM_RESTART)
    {
        debug_assert_info = debug_get_info();
        osel_memcpy(pbuf->data_p, (uint8_t *)&debug_assert_info, NWK_HEART_ALARM_SIZE);
        pbuf->data_p += NWK_HEART_ALARM_SIZE;
        len += NWK_HEART_ALARM_SIZE;
        debug_clr_info();
    }

    if (hb_ctrl.local_support == TRUE)
    {
        local_info_t nwk_local_info = mac_get_local_info();
        osel_memcpy(pbuf->data_p,
                    (uint8_t *)&nwk_local_info,
                    sizeof(local_info_t));
        pbuf->data_p += sizeof(local_info_t);
        len += sizeof(local_info_t);
    }

    if (hb_ctrl.overhear_support)
    {
        ; // add neighbors info
    }

    return len;
}

uint8_t nwk_fill_heart_beat(pbuf_t *pbuf, uint8_t heart_alarm_type)
{
    uint8_t len = 0;
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE
                   + MAC_HEAD_SEQ_SIZE + MAC_ADDR_SHORT_SIZE * 2;

    nwk_hd_ctl_t nwk_hd_ctl;
    nwk_hd_ctl.frm_ctrl = NWK_FRM_TYPE_HEART_BEAT;
    nwk_hd_ctl.dst_mode = NWK_ADDR_MODE_SHORT;
    nwk_hd_ctl.src_mode = NWK_ADDR_MODE_SHORT;
    osel_memcpy(pbuf->data_p, &nwk_hd_ctl, sizeof(nwk_hd_ctl_t));
    pbuf->data_p += NWK_HEAD_SIZE;

    osel_memcpy(pbuf->data_p, &sink_nwk_address, NWK_ADDR_SHORT_SIZE);
    pbuf->data_p += NWK_ADDR_SHORT_SIZE;
    osel_memcpy(pbuf->data_p, &my_nwk_addr, NWK_ADDR_SHORT_SIZE);
    pbuf->data_p += NWK_ADDR_SHORT_SIZE;

    uint8_t hb_payload_size = heart_beat_fill(pbuf, heart_alarm_type);

    len = NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE + NWK_ADDR_SHORT_SIZE + hb_payload_size;
    pbuf->data_len += len;

    pbuf->attri.need_ack = TRUE;

    return len;
}

/******************************************************************************/
static void nwk_frm_data_handler(sbuf_t *sbuf)
{
    m2n_data_indication_t *m2n_data_ind =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg);
    uint16_t msdu_length = m2n_data_ind->msdu_length;

    pbuf_t *pbuf = sbuf->primargs.pbuf;
    uint8_t nwk_head_size = NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE + NWK_ADDR_SHORT_SIZE;
    pbuf->data_p += nwk_head_size;

    n2a_data_indication_t *n2a_data_ind =
        &(sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_indication_arg);
    n2a_data_ind->nsdu_length = msdu_length - nwk_head_size;
    n2a_data_ind->nsdu = pbuf->data_p;

    sbuf->primtype = N2A_DATA_INDICATION;

    osel_post(APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void nwk_frm_join_response_handler(sbuf_t *sbuf)
{
    static bool_t first_startup = TRUE;

    m2n_data_indication_t *m2n_data_ind =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg);

    osel_memcpy(&my_nwk_addr, (uint8_t *)m2n_data_ind->msdu + NWK_HEAD_SIZE
                + NWK_ADDR_SHORT_SIZE + NWK_ADDR_LONG_SIZE,
                NWK_ADDR_SHORT_SIZE);

    osel_memcpy(&mac_pib.mac_short_addr, (uint8_t *)m2n_data_ind->msdu + NWK_HEAD_SIZE
                + NWK_ADDR_SHORT_SIZE + NWK_ADDR_LONG_SIZE + sizeof(my_nwk_addr),
                NWK_ADDR_SHORT_SIZE);

#include <rf.h>
    rf_write_burst_reg(CHECK_HEADER3, (uint8_t *)&mac_pib.mac_short_addr, 2);

    nwk_set_join_state(TRUE);

    mac_databuf_active();

    if (first_startup)
    {
        first_startup = FALSE;
        heart_beat_handler(NWK_HEART_ALARM_RESTART);
    }
    else
    {
        heart_beat_handler(NWK_HEART_ALARM_REJOIN);
    }

    osel_post(APP_INLINE_EVENT, (uint32_t *)(TRUE), OSEL_EVENT_PRIO_LOW);
}

void nwk_frame_parse(nwk_hd_t *nwk_hd, sbuf_t *sbuf)
{
    bool_t free_flag = TRUE;

    switch (nwk_hd->nwk_hd_ctl.frm_ctrl)
    {
    case NWK_FRM_TYPE_DATA:
        free_flag = FALSE;
        nwk_frm_data_handler(sbuf);
        break;

    case NWK_FRM_TYPE_JOIN_REQ:
        break;

    case NWK_FRM_TYPE_JOIN_REPLY:
        nwk_frm_join_response_handler(sbuf);
        break;

    default:
        break;
    }

    if (free_flag)
    {
        if (sbuf->primargs.pbuf != NULL)
        {
            pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
        }
        sbuf_free(&sbuf __SLINE2);
    }
}






