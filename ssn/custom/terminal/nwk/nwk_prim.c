/***************************************************************************
* File        : nwk_prim.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <nwk_prim.h>
#include <nwk_frames.h>
#include <nwk_handles.h>


void nwk_prim_handler(sbuf_t *sbuf)
{
    DBG_ASSERT(NULL != sbuf __DBG_LINE);

    switch (sbuf->primtype)
    {
    case A2N_DATA_REQUEST:
        app2nwk_data_request(sbuf);
        break;

    case M2N_DATA_INDICATION:
        mac2nwk_data_indication(sbuf);
        break;

    case M2N_DATA_CONFIRM:
        mac2nwk_data_confirm(sbuf);
        break;

    case M2N_ASSOC_CONFIRM:
        mac2nwk_assoc_confirm(sbuf);
        break;

    default:
        if (sbuf->primargs.pbuf != NULL)
        {
            pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
        }
        sbuf_free(&sbuf __SLINE2);
        break;
    }
}
