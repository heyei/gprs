/***************************************************************************
* File        : nwk_handles.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#include <hal_timer.h>
#include <hal_board.h>
#include <mac_global.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <mac_moniter.h>
#include <mac_frames.h>
#include <nwk_handles.h>
#include <nwk_frames.h>
#include <nwk.h>

static volatile hal_timer_t *join_timeout_timer = NULL;
static uint8_t nwk_re_join_cnt = 0;
static bool_t nwk_join_state = FALSE;
static hal_timer_t *heart_beat_timer = NULL;
uint16_t send_success_cnt = 0;

void nwk_set_join_state(bool_t res)
{
    nwk_join_state = res;
}

bool_t nwk_get_join_state(void)
{
    return nwk_join_state;
}

/******************************************************************************/
void app2nwk_data_request(sbuf_t *sbuf)
{
    pbuf_t *pbuf = sbuf->primargs.pbuf;

    a2n_data_request_t *a2n_data_request =
        &(sbuf->primargs.prim_arg.nwk_prim_arg.a2n_data_request_arg);

    uint8_t len = nwk_fill_data_buf(pbuf);

    nwk_send_data_packet(sbuf, ADDR_MODE_SHORT, len + a2n_data_request->nsdu_length);
}

/******************************************************************************/
static void heart_beat_time_cb(void *p)
{
    heart_beat_timer = NULL;
    osel_post(NWK_HEART_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static bool_t nwk_heart_start(void)
{
    device_info_t board_info = hal_board_info_look();
    if (heart_beat_timer == NULL)
    {
        HAL_TIMER_SET_REL(MS_TO_TICK(board_info.heartbeat_cycle * 1000),
                          heart_beat_time_cb,
                          NULL,
                          heart_beat_timer);

        DBG_ASSERT(heart_beat_timer != NULL __DBG_LINE);

        return TRUE;
    }

    return FALSE;
}

void nwk_heart_restart(void)
{
    if (heart_beat_timer != NULL)
    {
        hal_timer_cancel(&heart_beat_timer);
    }

    nwk_heart_start();
}

bool_t heart_beat_send(uint8_t heart_alarm_type)
{
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    if (sbuf == NULL)
    {
        return FALSE;
    }

    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    if (pbuf == NULL)
    {
        sbuf_free(&sbuf __SLINE2);
        return FALSE;
    }
    sbuf->primargs.pbuf = pbuf;
    sbuf->orig_layer = NWK_LAYER;
    
    uint8_t len = nwk_fill_heart_beat(pbuf, heart_alarm_type);
    
    nwk_send_data_packet(sbuf, ADDR_MODE_SHORT, len);
    
    return TRUE;
}

bool_t heart_beat_handler(uint8_t heart_alarm_type)
{
    if ((!nwk_get_join_state())
            || (!nwk_heart_start()))
    {
        return FALSE;
    }

    return heart_beat_send(heart_alarm_type);
}

/******************************************************************************/
static nwk_hd_t nwk_get_head(sbuf_t *sbuf)
{
    nwk_hd_t nwk_hd;
    uint8_t nwk_dst_addr_len = 0;
    m2n_data_indication_t *m2n_data_ind =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg);

    osel_memcpy(&(nwk_hd.nwk_hd_ctl), m2n_data_ind->msdu, NWK_HEAD_SIZE);

    if (nwk_hd.nwk_hd_ctl.dst_mode == NWK_ADDR_MODE_LONG)
    {
        nwk_dst_addr_len = NWK_ADDR_LONG_SIZE;
        osel_memcpy((uint8_t *)(&(nwk_hd.dst_addr)),
                    (uint8_t *)(m2n_data_ind->msdu) + NWK_HEAD_SIZE,
                    NWK_ADDR_LONG_SIZE);
    }
    else
    {
        nwk_dst_addr_len = NWK_ADDR_SHORT_SIZE;
        osel_memcpy(&(nwk_hd.dst_addr),
                    (uint8_t *)(m2n_data_ind->msdu) + NWK_HEAD_SIZE, 
                    NWK_ADDR_SHORT_SIZE);
    }

    if (nwk_hd.nwk_hd_ctl.src_mode == NWK_ADDR_MODE_LONG)
    {
        osel_memcpy(&(nwk_hd.src_addr),
                    (uint8_t *)(m2n_data_ind->msdu) + NWK_HEAD_SIZE + nwk_dst_addr_len,
                    NWK_ADDR_LONG_SIZE);
    }
    else
    {
        osel_memcpy(&(nwk_hd.src_addr),
                    (uint8_t *)(m2n_data_ind->msdu) + NWK_HEAD_SIZE + nwk_dst_addr_len,
                    NWK_ADDR_SHORT_SIZE);
    }

    return nwk_hd;
}

static bool_t nwk_is_to_local(nwk_hd_t *nwk_hd)
{
    if ((nwk_hd->nwk_hd_ctl.dst_mode == NWK_ADDR_MODE_LONG)
            && (nwk_hd->dst_addr == node_nui))
    {
        return TRUE;
    }
    else if ((nwk_hd->nwk_hd_ctl.dst_mode == NWK_ADDR_MODE_SHORT)
             && ((uint16_t)(nwk_hd->dst_addr) == my_nwk_addr))
    {
        return TRUE;
    }

    return FALSE;
}

void mac2nwk_data_indication(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    DBG_ASSERT(sbuf->primargs.pbuf != NULL __DBG_LINE);

    nwk_hd_t nwk_hd = nwk_get_head(sbuf);

    if (nwk_is_to_local(&nwk_hd))
    {
        nwk_frame_parse(&nwk_hd, sbuf);
    }
}

/******************************************************************************/
static void nwk_data_confirm_to_app(sbuf_t *sbuf)
{
    m2n_data_confirm_t m2n_data_confirm_arg;
    osel_memcpy((uint8_t *)&m2n_data_confirm_arg,
                &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg),
                sizeof(m2n_data_confirm_t));

    sbuf->primargs.pbuf->data_p = (uint8_t *)(m2n_data_confirm_arg.msdu) 
                                   + NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE * 2;

    n2a_data_confirm_t *n2a_data_confirm =
        &(sbuf->primargs.prim_arg.nwk_prim_arg.n2a_data_confirm_arg);
    n2a_data_confirm->status = m2n_data_confirm_arg.status;
    n2a_data_confirm->nsdu_length = m2n_data_confirm_arg.msdu_length -
                                    (NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE * 2);
    n2a_data_confirm->nsdu = sbuf->primargs.pbuf->data_p;
    sbuf->primargs.pbuf->data_len -= NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE * 2;

    sbuf->primtype = N2A_DATA_CONFIRM;
    osel_post(APP_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void mac2nwk_data_confirm_layer_handler(sbuf_t *sbuf)
{
    if (sbuf->orig_layer == APP_LAYER)
    {
        nwk_data_confirm_to_app(sbuf);
    }
    else
    {
        if (sbuf->used)
        {
            if (sbuf->primargs.pbuf != NULL)
            {
                pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
            }
            sbuf_free(&sbuf __SLINE2);
        }
    }
}

void mac2nwk_data_confirm(sbuf_t *sbuf)
{
    DBG_ASSERT(NULL != sbuf __DBG_LINE);

    bool_t res =
        sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_confirm_arg.status;
    uint8_t cnt =
        sbuf->primargs.pbuf->attri.already_send_times.nwk_send_times;

    if (res)
    {
        send_success_cnt++;
        mac2nwk_data_confirm_layer_handler(sbuf);
    }
    else
    {
        if (cnt >= NWK_MAX_SEND_TIMES)
        {
            mac2nwk_data_confirm_layer_handler(sbuf);
        }
        else
        {
            nwk_resend_data_packet(sbuf);
        }
    }
}

/******************************************************************************/
static void join_timeout_timer_cb(void *p)
{
    join_timeout_timer = NULL;

    if (!nwk_get_join_state())
    {
        sbuf_t *sbuf = sbuf_alloc(__SLINE1);
        if (sbuf == NULL)
        {
            DBG_ASSERT(FALSE __DBG_LINE);
            return;
        }

        m2n_assoc_confirm_t *assoc_cfm =
            &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_assoc_confirm_arg);

        sbuf->primtype = M2N_ASSOC_CONFIRM;
        assoc_cfm->status = TRUE;

        osel_post(MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
    }
}

static void nwk_join_timeout_set(void)
{
    if (NULL == join_timeout_timer)
    {
        HAL_TIMER_SET_REL( (mac_get_global_superfrm_time()
                                      * mac_get_mine_hops() * 2),
                           join_timeout_timer_cb,
                           NULL,
                           join_timeout_timer);
        DBG_ASSERT(join_timeout_timer != NULL __DBG_LINE);
    }
}

static bool_t nwk_join_request_packed(void)
{
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    if (sbuf == NULL)
    {
        return FALSE;
    }

    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    if (pbuf == NULL)
    {
        sbuf_free(&sbuf __SLINE2);
        return FALSE;
    }
    sbuf->primargs.pbuf = pbuf;
    sbuf->orig_layer = NWK_LAYER;
    
    uint8_t len = nwk_fill_join_req(pbuf);

    nwk_send_data_packet(sbuf, ADDR_MODE_LONG, len);
    
    return TRUE;
}


static void nwk_join_request(void)
{
    if (nwk_re_join_cnt++ < MAX_JION_NET_CNT)
    {
        nwk_join_request_packed();

        nwk_join_timeout_set();
    }
    else
    {
        nwk_re_join_cnt = 0;
        osel_post(M_MAC_MONI_RESTART_EVENT, (void *)0x06, OSEL_EVENT_PRIO_LOW);
    }
}


void mac2nwk_assoc_confirm(sbuf_t *sbuf)
{
    m2n_assoc_confirm_t *assoc_cfm =
        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_assoc_confirm_arg);

    if (assoc_cfm->status)
    {
        if (!nwk_get_join_state())
        {
            nwk_join_request();
        }
    }

    if (sbuf != NULL)
    {
        if (sbuf->primargs.pbuf != NULL)
        {
            pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
        }
        sbuf_free(&sbuf __SLINE2);
    }
}

/******************************************************************************/

