#ifndef __MAC_FRAMES_H__
#define __MAC_FRAMES_H__

#include <lib.h>

#define MAC_FRAME_TYPE_BEACON					0U
#define MAC_FRAME_TYPE_DATA						1U
#define MAC_FRAME_TYPE_ACK	     				2U
#define MAC_FRAME_TYPE_COMMAND					3U
#define MAC_FRAME_TYPE_RESERVED					4U

#define MAC_MHR_ADDR_MODE_FALSE                 0U
#define MAC_MHR_ADDR_MODE_RESERVED              1U
#define MAC_MHR_ADDR_MODE_SHORT                 2U
#define MAC_MHR_ADDR_MODE_LONG                  3U

#define MAC_FCS_SIZE                            0U
#define MAC_HEAD_CTRL_SIZE                      2U
#define MAC_HEAD_SEQ_SIZE                       1U
#define MAC_ADDR_SHORT_SIZE                     2U
#define MAC_ADDR_LONG_SIZE                      8U
#define MAC_BROADCAST_ADDR                      0xffff

#define MAC_FRM_VERSION                         0x01
#define SUP_FRM_SPEC_SIZE                       2U

#pragma pack(1)
/**
 * MAC frame control field struct definition
 */
typedef struct
{
	uint16_t frm_type			:	 3,             // frame type;
             sec_enable		    :	 1,			    // security enalbled;
			 frm_pending		:	 1,			    // frame pending;
			 ack_req	        :	 1,			    // ACK request;
             des_addr_mode      :    2,             // dest addressing mode;
             src_addr_mode      :    2,              //soure addressing mode
             reseverd           :    6;             //reseverd
} mac_frm_ctrl_t;

#pragma pack()

/**
 * mac_frames_init description
 */
void mac_frames_cb_init(void);


#endif