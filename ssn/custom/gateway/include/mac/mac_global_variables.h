#pragma once
#include <lib.h>

#define MAC_HEAD_SIZE                   (18u)

#if SYSTEM_PLAN_NUM == 1    //方案一
#define MAX_HOP_NUM                     (3u)
#elif SYSTEM_PLAN_NUM == 2  //方案二
#define MAX_HOP_NUM                     (5u)
#elif SYSTEM_PLAN_NUM == 3  //方案三
#define MAX_HOP_NUM                     (3u)
#endif

#pragma pack(1)
typedef struct
{
    uint8_t
            beacon_interv_order         :   5,          //大超帧时间指数       （可外部配置）
            beacon_duration_order       :   3;          //beacon时隙时间指数   （可外部配置）
    uint32_t
            down_link_slot_length       :   3,          //有pengdding时下发时隙的长度
            gts_duration_order          :   5,          //GTS时隙长度指数      （可外部配置）
            intra_channel               :   4,          //簇内信道             （可外部配置）
            cluster_number              :   4,          //簇内时隙个数         （可外部配置）
            intra_gts_number            :   7,          //簇内时隙GTS的个数    （可外部配置）
            inter_channel               :   4,          //簇间信道             （可外部配置）
            inter_unit_number           :   3,          //簇间时隙个数         （可外部配置）
            reserved                    :   2;
     uint8_t inter_gts_number[MAX_HOP_NUM];             //簇间单元内的GTS时隙个数（可外部配置）
} supf_spec_t;

#pragma pack()

typedef struct
{
	uint8_t mhr_size;

	struct
	{
		uint8_t frm_type;			// frame type;
		bool_t	frm_pending;		// frame pending;
		bool_t	ack_req;			// ACK request;
		bool_t	ie_list_en;			// IE list enable;
		uint8_t frm_version;		// frame version;
	}frm_ctrl;

	uint8_t mac_seq;				// mac seq no

	struct
	{
		bool_t	 pan_id_compress;

		uint16_t dst_pan_id;
		uint8_t  dst_addr_m;		// destination address mode;
		uint64_t dst_addr;

		uint16_t src_pan_id;
		uint8_t  src_addr_m;		// source address mode;
		uint64_t src_addr;
	}addr_info;

	struct
	{
		bool_t  sec_en;				// security enable;
		uint8_t sec_level;
		//...
	}security_info;

} mac_frm_head_info_t;

typedef struct __pend_short_addr_list
{
    list_head_t list;
    uint16_t short_addr;
}pend_short_addr_list_t;

typedef struct __pend_long_addr_list
{
    list_head_t list;
    uint64_t long_addr;
}pend_long_addr_list_t;


/**
 * 初始化需要被全局调用的变量，pib属性表
 *
 * @param  空
 *
 * @return 空
 */
void mac_global_variables_init(void);

/**
 * 取随机数
 *
 * @param  min最小值 max最大值
 *
 * @return 随机数
 */
uint8_t srandom(uint8_t min, uint8_t max);

extern list_head_t pend_addr_list_free[2];
extern list_head_t pend_addr_list_alloc[2];
extern list_head_t intra_gts_buf;          // for intra gts
extern list_head_t inter_gts_buf;          // for inter gts
extern list_head_t assoc_buf;
extern uint16_t loc_beacon_map;
extern uint16_t last_beacon_map;
extern uint16_t alloc_bcn_bitmap;
extern mac_frm_head_info_t mac_frm_head_info;
extern uint32_t track_slot_remain_time;
extern uint32_t track_slot_time;
