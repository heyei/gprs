#pragma once
#include <data_type_def.h>
#include <nwk_global_agreement.h>

void n2m_frame_data_package(sbuf_t *sbuf);
void n2m_frame_join_reply_package(uint64_t long_addr);
