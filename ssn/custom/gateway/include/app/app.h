/**
 * @brief       : 
 *
 * @file        : app.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once
#include <hal_timer.h>

typedef struct
{
    hal_timer_t *register_timer; //网关注册定时器
    hal_timer_t *cycle_timer_handle;
    uint8_t heartbeat_send_num;
} app_info_t;

extern app_info_t _app_info;

void app_init(void);
