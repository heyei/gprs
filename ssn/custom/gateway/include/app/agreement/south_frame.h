/**
 * 南向组帧
 *
 * @file south_frame.h
 * @author xushenghao
 * @data 2014-2-28 17:02:11
 *
 */

#pragma once
#include <data_type_def.h>
#include <pbuf.h>
#include <sbuf.h>
#include <global_variable.h>
#include <frame_interface.h>

/**
 * 组南向指令帧
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param data 
 * 
 * @return sbuf_t* 
 */
sbuf_t *south_instruction_package(north_instruction_data_t *data);
/**
 * 
 * 组南向配置帧
 * @author xushenghao (2014-2-28)
 * 
 * @param data 
 * 
 * @return sbuf_t* 
 */
sbuf_t *south_config_package(north_instruction_data_t *data);
/**
 * 组南向查询帧
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param data 
 * 
 * @return sbuf_t* 
 */
sbuf_t *south_select_package(north_instruction_data_t *data);
/**
 * 组南向应用数据帧
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param data 
 * 
 * @return sbuf_t* 
 */
sbuf_t *south_app_data_package(north_instruction_data_t *data);

/*南向应答帧*/
sbuf_t *south_app_request_package(app_head_t *request, uint8_t *dest_id);
