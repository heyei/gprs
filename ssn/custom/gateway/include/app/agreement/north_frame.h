/**
 * 北向组帧
 *
 * @file north_frame.h
 * @author xushenghao
 * @data 2014-2-28 17:00:05
 *
 */
#pragma once
#include <data_type_def.h>
#include <frame_interface.h>

/**
 * 组注册帧
 *
 * @author xushenghao (2014-2-28)
 *
 * @param id
 *
 * @return north_instruction_data_t
 */
north_instruction_data_t north_frame_register(const uint8_t *id);
/**
 * 组应答帧
 *
 * @author xushenghao (2014-2-28)
 *
 * @param id
 * @param pload 应答内容
 *
 * @return north_instruction_data_t
 */
north_instruction_data_t north_frame_replay(const uint8_t *id, const
    replay_pload_t *pload);
/**
 * 组通用帧
 *
 * @author xushenghao (2014-2-28)
 *
 * @param id
 * @param type
 * @param ploadlen 数据载荷长度
 *
 * @return north_instruction_data_t
 */
north_instruction_data_t north_frame_common(const uint8_t *id, uint8_t type,
    uint16_t ploadlen);
/**
 * 组查询应答帧
 *
 * @author xushenghao (2014-2-28)
 *
 * @param data
 * @param ploadlen
 *
 * @return north_instruction_data_t
 */
north_instruction_data_t north_select_frame_replay(north_instruction_data_t
    *data, uint16_t ploadlen);
/**
 * 数据发送
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param data 
 */
void north_frame_send(north_instruction_data_t *data);
/**
 * 用户数据发送
 * 
 * @author xushenghao (2014-2-28)
 * 
 * @param frame 
 * @param length 
 */
void north_user_data_send(uint8_t *frame, uint16_t length);

north_instruction_data_t north_frame_device_info(const uint8_t *id,  uint16_t len);