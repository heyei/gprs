#pragma once
#include <mac_frames.h>

#define MAC_SIZE	(MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE+ MAC_ADDR_SHORT_SIZE +MAC_ADDR_SHORT_SIZE)
#define NWK_SIZE	(NWK_HEAD_CTRL_SIZE + NWK_ADDR_SHORT_SIZE +NWK_ADDR_SHORT_SIZE )

#define NORTH_INSTRUCTION_HEAD_LEN	(22u)
#define IS_REPLY         	(0x80u)
#define GET_DATA_TYPE       (0x7Fu)


typedef enum
{
    //配置指令配置码
    CONFIG_HEARTBEAT_CYCLE = 0x0001,  //心跳周期配置
    CONFIG_DATA_CYCLE,  //数据上报周期配置
    CONFIG_DATA_DETECTION_CYCLE,  //数据采集周期配置
    CONFIG_ERROR,
} config_e;

typedef enum
{
    //网络管理指令
    NM_REGISTER = 0x01,  //注册指令
    NM_CONFIG,  //配置指令
    NM_SELECT,  //查询指令
    NM_HEARTBEAT,  //心跳指令
    NM_LINK_TEST,  //链路测试指令
    NM_DEVICE_REST,  //设备复位指令
    NM_APP_DATA,  //应用数据
    NM_DEVICE_INFO,     //设备信息

    NM_SELECT_EXTEND = 0x21,  //查询扩展指令

    NM_SSN_DATA = 0x0f,  //SSN应用数据
    NM_END,
} network_manage_command_e;

typedef enum
{
    SSN_USER = 0x01,  //用户数据帧
    SSN_REST,  //复位报警帧
    SSN_REJOIN,  //重入网报警
    SSN_LOCATION,  //定位数据
    SSN_TM_HM,  //温湿度
    SSN_ACCELE,  //加速度
} ssn_command_e;

typedef enum
{
    APP_FRAME_TYPE_DATA = 0x00,  //app数据帧
    APP_FRAME_TYPE_REQUEST,  //app应答帧
    APP_FRAME_TYPE_CONFIG,  //app配置帧
    APP_FRAME_TYPE_SELECT,  //app查询帧
    APP_FRAME_TYPE_SELECT_REQUEST,  //app查询应答帧
    APP_FRAME_TYPE_LINKTEST,  //app链路测试帧
    APP_FRAME_TYPE_REST,  //app复位
    APP_FRAME_TYPE_ERROR,  //错误帧
} south_app_frame_type_e;

typedef enum
{
    APP_DATA_TYPE_NOT_DATA = 0x00,  //非app数据帧
    APP_DATA_TYPE_DATA_TEMPERATURE_HUNIDITY,  //温湿度数据帧
    APP_DATA_TYPE_DATA_ACCELERATION,  //加速度数据帧
    APP_DATA_TYPE_USER,  //用户数据帧
} south_app_data_type_e;

typedef enum
{
    //数据上报周期应用代码
    DATA_CYCLE_TM_HM = 0x0001,  //温湿度
    DATA_CYCLE_ACCELER,  //加速度
} data_cycle_e;

typedef enum
{
    //查询指令类型
    S_DATA = 0,  //数据查询
    S_STATE,  //状态查询
} select_cmd_type_e;

typedef enum
{
    //查询扩展查询码
    HEARTBEAT_CYCLE = 0x0000, TEM_HUM_DATA_CYCLE, TEM_HUM_DETECTION_CYCLE,
        ACCEL_SPEED_DATA_CYCLE, ACCEL_SPEED_DETECTION_CYCLE,
} select_extend_e;

typedef enum
{
    //数据采集周期配置应用代码
    DATA_DETECTION_TM_HM = 0x0001,  //温湿度
    DATA_DETECTION_ACCELER,  //加速度
} data_detection_e;

typedef enum
{
    //APP查询帧参数表
    APP_SELECT_HEARTBEAT_CYCLE = 0x00, APP_SELECT_DEVICE_ELECTRICITY,
        APP_SELECT_END,
} app_select_e;
