/**
 * @brief       : 
 *
 * @file        : app_func.h
 * @author      : shenghao.xu
 * @version     : v0.0.1
 * @date        : 2015/5/7
 *
 * Change Logs  :
 *
 * Date        Version      Author      Notes
 * 2015/5/7    v0.0.1      shenghao.xu    some notes
 */
#pragma once

void uart_frame_cfg(void);
void north_register_func(void);
bool_t platform_state(void);
void cnn_platform(void);
void discnn_platform(void);
void gw_register_handle(void);
void reverse_id_endian(uint8_t *id);
void uart_event_handle(void);
bool_t judge_self_id(uint8_t *judge_id);
