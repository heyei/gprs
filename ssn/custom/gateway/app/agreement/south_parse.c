#include <hal_timer.h>
#include <south_parse.h>
#include <route_table.h>
#include <global_variable.h>
#include <north_frame.h>
#include <replay_module.h>
#include <app_func.h>
#include <app.h>
#include <south_frame.h>
static bool_t south_parse_heartbeat(const uint8_t *frame, uint8_t size,
                                    south_instruction_data_t *data)
{
    frame_ctrl_t ctrl;
    rtb_t *rtb_info;
    osel_memcpy(&ctrl, &frame[0], sizeof(frame_ctrl_t));

#if PLATFORM == 0
    if (ctrl.alarm_type != NON_ALARM_INFO)
    {
        data->ssn_data.ssn_type = NM_SSN_DATA;
        fp32_t time = 0;
        int index = rtb_find_index(data->id);
        if (index !=  - 1)
        {
            rtb_info = rtb_find_info(index);
            time = rtb_info->join_time;
        }

        frame++; //跳过APP头
        frame += 2; //跳过2个字节父节点
        frame++; //跳过成功率
        if (ctrl.energy_report_support)
        {
            frame++; //跳过电量
        }
        if (ctrl.alarm_type == DEVICE_REST)
        {
            data->ssn_data.app_data.type = SSN_REST;
            osel_memcpy(&data->ssn_data.app_data.alarm_rest.line, frame, 2);
            frame += 2;
            osel_memcpy(&data->ssn_data.app_data.alarm_rest.join_delay,  &time,
                        4);
        }
        else if (ctrl.alarm_type == DEVICE_REJOIN)
        {
            data->ssn_data.app_data.type = SSN_REJOIN;
            osel_memcpy(&data->ssn_data.app_data.alarm_rjoin.join_delay,  &time,
                        4);
        }
        return TRUE;
    }
#endif
    if (data->nwk_id != NWK_ADDR)
    {
        _NOP();
    }
    data->ssn_data.ssn_type = NM_HEARTBEAT;
    heart_frame_t *hp = &data->ssn_data.heart_frame;
    osel_memcpy(&hp->frame_ctrl, frame, sizeof(frame_ctrl_t));
    frame += sizeof(frame_ctrl_t);
    osel_memcpy(&hp->farther_id, frame, 2);
    frame += 2;
    osel_memcpy(&hp->successful_rate, frame, 1);
    if (hp->successful_rate > 0x64)
    {
        _NOP();
    }
    frame++;
    if (hp->frame_ctrl.energy_report_support)
    {
        osel_memcpy(&hp->residual_energy, frame, 1);
        frame++;
    }
    hp->localization_info.tuple_num = 0;

    if (size != 5)
    {
        //网关心跳没有定位数据
        hp->localization_info.tuple_num =  *frame;
        frame++;
        int len = hp->localization_info.tuple_num *sizeof
            (localization_variable_t);
        if (len > 0)
        {
            osel_memcpy(&hp->localization_info.variable[0], frame, len);
            frame += len;
        }
    }

    return TRUE;
}

static bool_t app_user_data(const uint8_t *frame, uint8_t size,
                            south_instruction_data_t *data)
{
    uint8_t len = size - sizeof(app_head_t) - sizeof(uint32_t);
    //跳过APP头和时间戳
    frame += sizeof(uint32_t);
    data->ssn_data.app_data.type = SSN_USER;
    data->ssn_data.app_data.user_data.pbuf = pbuf_alloc(size __PLINE1);
    DBG_ASSERT(data->ssn_data.app_data.user_data.pbuf != NULL __DBG_LINE);
    pbuf_t *pbuf = data->ssn_data.app_data.user_data.pbuf;
    pbuf->data_len = len;
    pbuf->data_p = pbuf->head;

    osel_memcpy(pbuf->data_p, frame, size);
    return TRUE;
}

static bool_t parse_temperature_hunidity(const uint8_t *frame, uint8_t size,
    south_instruction_data_t *data)
{
    data->ssn_data.app_data.type = SSN_TM_HM;
    temperature_hunidity_t *th = &data->ssn_data.app_data.temperature_hunidity;
    uint32_t time = 0;
    osel_memcpy(&time, frame, sizeof(uint32_t));
    frame += sizeof(uint32_t);
    hal_time_t now = hal_timer_now();
    time = now.w - time;
    th->time = TICK_TO_US(time) / 1000.0;

    osel_memcpy(&th->tm, frame, sizeof(uint16_t));
    frame += sizeof(uint16_t);
    th->tm = S2B_UINT16(th->tm);

    osel_memcpy(&th->rh, frame, sizeof(uint16_t));
    frame += sizeof(uint16_t);
    th->rh = S2B_UINT16(th->rh);
    return TRUE;
}

static bool_t parse_acceleration(const uint8_t *frame, uint8_t size,
                                 south_instruction_data_t *data)
{
    data->ssn_data.app_data.type = SSN_ACCELE;
    acceler_t *acceler = &data->ssn_data.app_data.acceler;
    uint32_t time = 0;
    osel_memcpy(&time, frame, sizeof(uint32_t));
    frame += sizeof(uint32_t);
    hal_time_t now = hal_timer_now();
    time = now.w - time;
    acceler->time = TICK_TO_US(time) / 1000.0;

    osel_memcpy(&acceler->x, frame, sizeof(uint16_t));
    frame += sizeof(uint16_t);
    acceler->x = S2B_UINT16(acceler->x);

    osel_memcpy(&acceler->y, frame, sizeof(uint16_t));
    frame += sizeof(uint16_t);
    acceler->y = S2B_UINT16(acceler->y);

    osel_memcpy(&acceler->z, frame, sizeof(uint16_t));
    frame += sizeof(uint16_t);
    acceler->z = S2B_UINT16(acceler->z);
    return TRUE;
}

static bool_t parse_app_data(const uint8_t *frame, uint8_t size,
                             south_instruction_data_t *data)
{
    app_head_t head;
    osel_memcpy(&head, frame, 1);
    frame += sizeof(app_head_t);
    switch (head.data_type)
    {
        case APP_DATA_TYPE_DATA_TEMPERATURE_HUNIDITY:
            return parse_temperature_hunidity(frame, size, data);
            break;
        case APP_DATA_TYPE_DATA_ACCELERATION:
            return parse_acceleration(frame, size, data);
            break;
        case APP_DATA_TYPE_USER:
            return app_user_data(frame, size, data);
            break;
        default:
            break;
    }
    return FALSE;
}

static bool_t parse_config_request(const uint8_t *frame, uint8_t size,
                                   south_instruction_data_t *data)
{
    int index = rtb_find_index_through_nwk_addr(data->nwk_id);
    if (index !=  - 1)
    {
        rtb_t *rtb = rtb_find_info(index);
        uint8_t id[8];
        osel_memcpy(&id[0], &rtb->self_long_addr, 8);
        reverse_id_endian(&id[0]);

        replay_info_t node;
        if (replay_list_get_remove(rtb->self_nwk_addr, &node))
        {
            switch (node.code)
            {
                case CONFIG_HEARTBEAT_CYCLE:
                    break;
                case CONFIG_DATA_CYCLE:
                    break;
                case CONFIG_DATA_DETECTION_CYCLE:
                    break;
                default:
                    return FALSE;
                    break;
            }
        }
        data->ssn_data.config_request.seq = node.seq;
        data->ssn_data.config_request.code = node.code;
        data->ssn_data.config_request.result = 0x00; //默认的应答都是成功的
        return TRUE;
    }
    return FALSE;
}

static uint8_t parse_data_head(const uint8_t *frame,south_instruction_data_t *data)
{
    app_head_t head;
    uint8_t result = APP_FRAME_TYPE_ERROR;
    osel_memcpy(&head, frame, sizeof(app_head_t));
    switch (head.frame_type)
    {
        case APP_FRAME_TYPE_DATA:
            break;
        case APP_FRAME_TYPE_REQUEST:
            break;
        case APP_FRAME_TYPE_SELECT_REQUEST:
            break;
        default:
            return result;
    }
    
    if(head.ack_request)
    {
        app_head_t request;
        request.frame_type = APP_FRAME_TYPE_REQUEST;
        request.ack_request = FALSE;
        request.data_type =platform_state();
        request.seq_num = head.seq_num;

        sbuf_t *sbuf = south_app_request_package(&request, (uint8_t *)&data->id);
        if (sbuf != NULL)
        {
            osel_post(APP2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
        }
        else
        {
            pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
            sbuf_free(&sbuf __SLINE2);
        }
    }
    else
    {
        _NOP();
    }
    
    return head.frame_type;
}

static bool_t parse_select_request(const uint8_t *frame, uint8_t size,
                                   south_instruction_data_t *data)
{
    select_request_t *hp = &data->ssn_data.select_request;
    frame += sizeof(app_head_t);
    int num =  *frame;
    frame++;
    for (int i = 0; i < num; i++)
    {
        if (*frame == APP_SELECT_HEARTBEAT_CYCLE)
        {
            frame += 2; //跳过类型和长度
            osel_memcpy(&hp->heartbeat_cycle, frame, 4);
            frame += 4;
        }
        else if (*frame == APP_SELECT_DEVICE_ELECTRICITY)
        {
            frame += 2;
            hp->electricity =  *frame;
            frame++;
        }
    }

    int index = rtb_find_index_through_nwk_addr(data->nwk_id);
    if (index !=  - 1)
    {
        rtb_t *rtb = rtb_find_info(index);
        uint8_t id[8];
        osel_memcpy(&id[0], &rtb->self_long_addr, 8);
        reverse_id_endian(&id[0]);

        replay_info_t node;
        if (replay_list_get_remove(rtb->self_nwk_addr, &node))
        {
            data->ssn_data.select_request.seq = node.seq;
            return TRUE;
        }
    }
    return FALSE;
}

static bool_t south_parse_data(const uint8_t *frame, uint8_t size,
                               south_instruction_data_t *data)
{
    uint8_t app_type = parse_data_head(frame,data);
    switch (app_type)
    {
        case APP_FRAME_TYPE_DATA:
            data->ssn_data.ssn_type = NM_SSN_DATA;
            return parse_app_data(frame, size, data);
            break;
        case APP_FRAME_TYPE_REQUEST:
            data->ssn_data.ssn_type = NM_CONFIG;
            return parse_config_request(frame, size, data);
            break;
        case APP_FRAME_TYPE_SELECT_REQUEST:
            data->ssn_data.ssn_type = NM_SELECT;
            return parse_select_request(frame, size, data);
            break;
        default:
            break;
    }

    return FALSE;
}

bool_t south_parse_instruction_frame(const uint8_t *frame, uint8_t size,
                                     south_instruction_data_t *instruction)
{
    switch (instruction->nwk_type)
    {
        case N2A_DATA_INDICATION:
            {
                if (size < 2)
                {
                    return FALSE;
                }
                return south_parse_data(frame, size, instruction);
            }
            break;
        case N2A_HEARTBEAT:
            return south_parse_heartbeat(frame, size, instruction);
            break;
        default:
            break;
    }
    return FALSE;
}
