#include <socket.h>
#include <hal_uart.h>
#include <hal_socket.h>
#include <north_frame.h>
#include <app_func.h>
#include <frame_interface.h>
#include <global_variable.h>
#include <route_table.h>
#include <hal_board.h>
static volatile uint16_t north_seq = 0;
static uint16_t get_seq()
{
    return north_seq++;
}

static void fill_head(north_instruction_data_t *instruction, const
                      uint8_t*const src_id, uint16_t pload_length)
{
    device_info_t device_info = hal_board_info_look();
    instruction->version = 1;
    instruction->length = NORTH_INSTRUCTION_HEAD_LEN + pload_length;
    osel_memcpy(&instruction->dest_id[0], &device_info.device_id[0], 8);
    instruction->seq = get_seq();
    osel_memcpy(&instruction->src_id[0], &src_id[0], 8);
    reverse_id_endian(&instruction->src_id[0]);
    reverse_id_endian(&instruction->dest_id[0]);
}

//北向注册帧
north_instruction_data_t north_frame_register(const uint8_t *id)
{
    north_instruction_data_t instruction;
    instruction.type = NM_REGISTER;
    fill_head(&instruction, id, 0);
    return instruction;
}

north_instruction_data_t north_frame_device_info(const uint8_t *id, uint16_t len)
{
    north_instruction_data_t instruction;
    instruction.type = NM_DEVICE_INFO;
    fill_head(&instruction, id, len);
    return instruction;
}

//应答帧
north_instruction_data_t north_frame_replay(const uint8_t *id, const
                                            replay_pload_t *pload)
{
    north_instruction_data_t instruction;
    instruction.type = (pload->type | IS_REPLY);
    fill_head(&instruction, id, 4);
    instruction.seq = pload->seq;
    instruction.pload = (uint8_t*) &pload->code;
    return instruction;
}

north_instruction_data_t north_select_frame_replay
(north_instruction_data_t*data, uint16_t ploadlen)
{
    north_instruction_data_t instruction;
    instruction.type = (data->type | IS_REPLY);
    reverse_id_endian(&data->dest_id[0]);
    fill_head(&instruction, &data->dest_id[0], ploadlen);
    instruction.seq = data->seq;
    return instruction;
}

//组通用帧
north_instruction_data_t north_frame_common(const uint8_t *id, uint8_t type,
                                            uint16_t ploadlen)
{
    north_instruction_data_t instruction;
    instruction.type = type;
    fill_head(&instruction, id, ploadlen);
    return instruction;
}

static void send_socket(uint8_t *pload, uint16_t len,
                        north_instruction_data_t*data)
{
    //网口发送
    hal_socket_buf_t sdata;
    sdata.buf = pload;
    sdata.length = len;
    sdata.ag_send = FALSE;
    sdata.seq = data->seq;
    if (data->type == NM_REGISTER || data->type == NM_HEARTBEAT || data->type
        == NM_APP_DATA)
    {
        if (platform_state())
        {
            sdata.ag_send = FALSE;
        }
    }
    hal_socket_send(&sdata, CARD_1);
}

void north_frame_send(north_instruction_data_t *data)
{
    uint8_t pload[UART_LEN_MAX];
    uint16_t len = data->length;
    data->length = S2B_UINT16(data->length);
    data->seq = S2B_UINT16(data->seq);
    osel_memcpy(&pload[0], data, NORTH_INSTRUCTION_HEAD_LEN);
    if (len > NORTH_INSTRUCTION_HEAD_LEN)
    {
        osel_memcpy(&pload[NORTH_INSTRUCTION_HEAD_LEN], data->pload, (len -
                                                                      NORTH_INSTRUCTION_HEAD_LEN));
    }
    
    send_socket(pload, len, data);
    
    //串口发送
    uint8_t uart_head[4] =
    {
        0xd5, 0xc8
    };
    osel_memcpy(&uart_head[2], &data->length, 2);
    
    hal_uart_send_string(HAL_UART_1, &uart_head[0], 4);
    hal_uart_send_string(HAL_UART_1, pload, len);
}

void north_user_data_send(uint8_t *frame, uint16_t length)
{
    //网口发送
    hal_socket_buf_t sdata;
    sdata.buf = frame;
    sdata.length = length;
    sdata.ag_send = FALSE;
    sdata.seq = 0;
    hal_socket_send(&sdata, CARD_1);
    
    //串口发送
    uint8_t uart_head[4] =
    {
        0xd5, 0xc8
    };
    length = S2B_UINT16(length);
    osel_memcpy(&uart_head[2], &length, 2);
    
    hal_uart_send_string(HAL_UART_1, &uart_head[0], 4);
    hal_uart_send_string(HAL_UART_1, (uint8_t*)frame, sdata.length);
}
