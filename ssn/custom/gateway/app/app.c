#include <osel_arch.h>
#include <socket.h>
#include <hal_socket.h>
#include <app.h>
#include <app_func.h>
#include <frame_interface.h>
#include <replay_module.h>
#include <driver.h>
extern w5100_info_t w5100_dev[CARD_NUM];
app_info_t _app_info;

static void app_cycle_timer_cb(void *p)
{
	_app_info.cycle_timer_handle = NULL;
	osel_post(APP_CYCLE_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void app_cycle_timer_start()
{
	if (_app_info.cycle_timer_handle == NULL)
	{
        hal_wdt_clear(16000);
		HAL_TIMER_SET_REL(MS_TO_TICK(2000),
						  app_cycle_timer_cb,
						  NULL,
						  _app_info.cycle_timer_handle);
		DBG_ASSERT(NULL != _app_info.cycle_timer_handle __DBG_LINE);
	}
    else
    {
        _NOP();
    }
}

static void gw_register_start(void)
{
    gw_register_handle();
}

static void app_lan_event_handle(void *param)
{
    DBG_ASSERT(param != NULL __DBG_LINE);
    hal_socket_msg_t *lan_msg = (hal_socket_msg_t*)param;
    switch (lan_msg->msg)
    {
    case SOCKET_RX_OK_INT:
        if(lan_msg->port == TCP_SERVICE_PORT)
        {
            hal_tcp_service_read(lan_msg);
        }
        else
        {
            hal_socket_read(lan_msg);
        }
        hal_socket_recv_enable();
        break;
    case SOCKET_TX_OK_INT:
        cnn_platform();
        break;
    case SOCKET_CON_OK_INT:
        cnn_platform();
        break;
    case SOCKET_DISCON_OK_INT:
        discnn_platform();
        break;
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
}

static void app_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t*)e;
    switch (p->sig)
    {
    case NWK2APP_PRIM_EVENT:
        south_parse_interface((sbuf_t*)(p->param));
        break;
    case APP_NORTH_DATA_EVENT:
        north_parse_interface((pbuf_t*)(p->param));
        break;
    case APP_LAN_EVENT:
        app_lan_event_handle(p->param);
        break;
    case APP_UART_EVENT:
        uart_event_handle();
        break;
    case APP_GW_REGISTER:
        gw_register_start();
        break;
    case APP_CYCLE_TIMER_EVENT:
        app_cycle_timer_start();
        break;
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
}

void app_init(void)
{
    osel_task_tcb *app_task_handle = osel_task_create(&app_task, APP_TASK_PRIO);
    
    osel_subscribe(app_task_handle, NWK2APP_PRIM_EVENT);
    osel_subscribe(app_task_handle, APP_NORTH_DATA_EVENT);
    osel_subscribe(app_task_handle, APP_LAN_EVENT);
    osel_subscribe(app_task_handle, APP_UART_EVENT);
    osel_subscribe(app_task_handle, APP_GW_REGISTER);
    osel_subscribe(app_task_handle, APP_CYCLE_TIMER_EVENT);
    uart_frame_cfg();
    
    replay_module_init();
    
    discnn_platform();
    _app_info.register_timer = NULL;
    app_cycle_timer_start();
    
    delay_ms(2000);
    osel_post(APP_GW_REGISTER, NULL, OSEL_EVENT_PRIO_LOW);
}
