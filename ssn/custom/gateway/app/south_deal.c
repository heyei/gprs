#include <south_deal.h>
#include <north_frame.h>
#include <global_variable.h>
#include <frame_interface.h>
#include <route_table.h>
#include <app_func.h>
#include <app.h>
#include <mac_pib.h>
#include <hal_board.h>
static void app_data_deal(south_instruction_data_t *instruction)
{
    ssn_app_data_t *app_data = &instruction->ssn_data.app_data;
    uint8_t pload[PKT_LEN_MAX];
    uint16_t len = 0;
    uint8_t *p = pload;
    *p = app_data->type;
    p++;

    switch (app_data->type)
    {
        case SSN_USER:
            {
                uint8_t frame[PKT_LEN_MAX];
                osel_memcpy(&frame[0], &app_data->user_data.pbuf->head[0], app_data
                            ->user_data.pbuf->data_len);
                device_info_t device_info = hal_board_info_get();
                reverse_id_endian(&device_info.device_id[0]);
                osel_memcpy(&frame[6],device_info.device_id,8);
                north_user_data_send(&frame[0], app_data->user_data.pbuf
                                     ->data_len);
                pbuf_free(&(app_data->user_data.pbuf)__PLINE2);
                return ;
            }
            break;
        case SSN_REST:
            {
                len = sizeof(alarm_rest_t);
                app_data->alarm_rest.line = S2B_UINT16(app_data
                    ->alarm_rest.line);
                osel_memcpy(p, &app_data->alarm_rest, len);
            }
            break;
        case SSN_REJOIN:
            {
                len = sizeof(alarm_rjoin_t);
                osel_memcpy(p, &app_data->alarm_rjoin, len);
            }
            break;
        case SSN_TM_HM:
            {
                len = sizeof(temperature_hunidity_t);
                osel_memcpy(p, &app_data->temperature_hunidity, len);
            }
            break;
        case SSN_ACCELE:
            {
                len = sizeof(acceler_t);
                osel_memcpy(p, &app_data->acceler, len);
            }
            break;
        default:
            return ;
    }

    len = len + 1;
    north_instruction_data_t data = north_frame_common((uint8_t*) &instruction
        ->id, instruction->ssn_data.ssn_type, len);
    data.pload = &pload[0];
    north_frame_send(&data);
}

static void update_rssi(south_instruction_data_t *instruction)
{
    heart_frame_t *p = &instruction->ssn_data.heart_frame;
    if (p->frame_ctrl.localization_support == TRUE)
    {
        int index = rtb_find_index_through_nwk_addr(instruction->nwk_id);
        if (index ==  - 1)
        {
            return ;
        }
        rtb_t *rtb_info = rtb_find_info(index);
        if (p->frame_ctrl.device_type == TERMINAL)
        {
            if (p->localization_info.tuple_num == 1)
            {
                rtb_info->downlinkq = p->localization_info.variable[0].lqi;
            }
            else
            {
                return ;
            }
        }
        else if (p->frame_ctrl.device_type == RELAY)
        {
            for (int i = 0; i < p->localization_info.tuple_num; i++)
            {
                localization_variable_t *lp = &p->localization_info.variable[i];
                if (lp->source_addr != instruction->nwk_id)
                {
                    //是通过中继上来的
                    index = rtb_find_index_through_nwk_addr(lp->source_addr);
                    if (index !=  - 1)
                    {
                        rtb_t *rtb_info2 = rtb_find_info(index);
                        rtb_info2->downlinkq = lp->lqi;
                    }
                }
                else
                {
                    rtb_info->downlinkq = lp->lqi;
                }
            }
        }
    }
}

static void heartbeat_deal(south_instruction_data_t *instruction)
{
    uint8_t pload[PKT_LEN_MAX];
    uint8_t *p;
    p = &pload[0];
    heart_frame_t *snn_heart = &instruction->ssn_data.heart_frame;
    heartbeat_command_t heartbeat_command;
    heartbeat_command.ctrl_domain.device_type = snn_heart
        ->frame_ctrl.device_type;
    heartbeat_command.ctrl_domain.energy_support = snn_heart
        ->frame_ctrl.energy_report_support;
    heartbeat_command.ctrl_domain.link_quality = TRUE;
    heartbeat_command.ctrl_domain.topology = TRUE;
    uint16_t len = 0;

    update_rssi(instruction); //更新RSSI值

    if (_app_info.heartbeat_send_num++ == 20)
    {
        discnn_platform();
        osel_post(APP_GW_REGISTER, NULL, OSEL_EVENT_PRIO_LOW);
    }

    if (NWK_ADDR != instruction->nwk_id)
    {
        uint64_t father_mac_lond_addr = 0;
        if (read_pib_short_addr() != snn_heart->farther_id)
        {
            if (!rtb_find_long_addr_through_mac_short_addr(snn_heart
                ->farther_id, &father_mac_lond_addr))
            {
                return ;
            }
        }
        else
        {
            father_mac_lond_addr = read_pib_long_addr();
        }
        osel_memcpy(&heartbeat_command.superior_id[0], &father_mac_lond_addr, 8)
                    ;
        reverse_id_endian(&heartbeat_command.superior_id[0]);
    }
    else
    {
        osel_memset(&heartbeat_command.superior_id[0], 0xff, 8);
    }

    heartbeat_command.successful_rate = FALSE;
    heartbeat_command.energy_remain = snn_heart->residual_energy;
    int index = rtb_find_index_through_nwk_addr(instruction->nwk_id);
    if (index !=  - 1)
    {
//        if (device_if_register(index) == FALSE)
//        {
//            return ;
//        }
        heartbeat_command.downlink_quality = rtb_get_downrssi(index);
        heartbeat_command.uplink_quality = rtb_get_uprssi(index);
    }


    osel_memcpy(p, &heartbeat_command.ctrl_domain, sizeof(ctrl_domain_t));
    p++;

    *p++ = heartbeat_command.energy_remain;
    osel_memcpy(p, &heartbeat_command.superior_id[0], 8);
    p += 8;
    *p++ = heartbeat_command.uplink_quality;
    *p++ = heartbeat_command.downlink_quality;


    len = p - &pload[0];
    north_instruction_data_t data = north_frame_common((uint8_t*) &instruction
        ->id, instruction->ssn_data.ssn_type, len);
    data.pload = &pload[0];
    north_frame_send(&data);
}

static void config_request_deal(south_instruction_data_t *instruction)
{
    uint8_t id[8];
    osel_memcpy(&id[0], &instruction->id, 8);

    replay_pload_t replay_pload;
    replay_pload.type = instruction->ssn_data.ssn_type;
    replay_pload.seq = instruction->ssn_data.config_request.seq;
    replay_pload.code = instruction->ssn_data.config_request.code;
    replay_pload.result = instruction->ssn_data.config_request.result;

    replay_pload.code = S2B_UINT16(replay_pload.code);
    replay_pload.result = S2B_UINT16(replay_pload.result);
    north_instruction_data_t data = north_frame_replay(&id[0], &replay_pload);
    north_frame_send(&data);
}

static void select_request_deal(south_instruction_data_t *instruction)
{
    north_instruction_data_t north_in_data;
    osel_memcpy(&north_in_data.dest_id[0], &instruction->id, 8);
    reverse_id_endian(&north_in_data.dest_id[0]);
    select_command_t select_command;
    select_command.select_type = 1;
    select_command.select_range = 0;
    select_command.relevance = 1;
    select_command.link_quality = 1;
    select_command.device_electricity = 1;
    select_command.data_cycle = 1;
    select_command.heartbeat_cycle = 1;

    select_state_reply_t state;
    osel_memset(&state, 0x00, sizeof(select_state_reply_t));
    state.code = select_command;
    int index = rtb_find_index_through_nwk_addr(instruction->nwk_id);
    if (index !=  - 1)
    {
        rtb_t *rtb = rtb_find_info(index);
        uint64_t parent = 0;
        if (rtb->next_mac_addr == rtb->self_nwk_addr)
        {
            parent = read_pib_long_addr();
        }
        else
        {
            if (!rtb_find_long_addr_through_mac_short_addr(rtb->next_mac_addr,
                &parent))
            {
                return ;
            }
        }
        osel_memcpy(&state.device_associate[0], &parent, 8);
        reverse_id_endian(&state.device_associate[0]);

        state.link_quality = rtb->uplinkq;
        state.device_electricity = instruction
            ->ssn_data.select_request.electricity;
        state.data_cycle = 0;
        state.data_cycle = S2B_UINT32(state.data_cycle);
        state.heartbeat_cycle = instruction
            ->ssn_data.select_request.heartbeat_cycle;
        state.heartbeat_cycle = S2B_UINT32(state.heartbeat_cycle);
        north_in_data.seq = instruction->ssn_data.select_request.seq;
        north_in_data.type = instruction->ssn_data.ssn_type;
        int pload_len = sizeof(select_state_reply_t);
        north_instruction_data_t data = north_select_frame_replay
            (&north_in_data, pload_len);

        data.pload = (uint8_t*) &state.code;
        north_frame_send(&data);
    }
}

void south_deal_interface(south_instruction_data_t *instruction)
{
    ssn_data_t *ssn_data = &instruction->ssn_data;
    switch (ssn_data->ssn_type)
    {
        case NM_SSN_DATA:
            app_data_deal(instruction);
            break;
        case NM_HEARTBEAT:
            heartbeat_deal(instruction);
            break;
        case NM_CONFIG:
            //配置应答
            config_request_deal(instruction);
            break;
        case NM_SELECT:
            //查询应答
            select_request_deal(instruction);
            break;
        default:
            return ;
    }
}
