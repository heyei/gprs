#include <lib.h>
#include <wsnos.h>
#include <mac_pib.h>
#include <hal_board.h>

#if SYSTEM_PLAN_NUM == 1    //方案一
enum
{
    INTER_GTS_NUM1 = 1,
    INTER_GTS_NUM2 = 20,
    INTER_GTS_NUM3 = 40,
} inter_gts_num_t;
static uint8_t inter_gts_num[MAX_HOP_NUM] = {INTER_GTS_NUM1, INTER_GTS_NUM2, INTER_GTS_NUM3};
#elif SYSTEM_PLAN_NUM == 2    //方案二
enum
{
    INTER_GTS_NUM1 = 128,
    INTER_GTS_NUM2 = 128,
    INTER_GTS_NUM3 = 128,
    INTER_GTS_NUM4 = 128,
    INTER_GTS_NUM5 = 128
} inter_gts_num_t;
static uint8_t inter_gts_num[MAX_HOP_NUM] = {INTER_GTS_NUM1, INTER_GTS_NUM2, INTER_GTS_NUM3,
                                            INTER_GTS_NUM4, INTER_GTS_NUM5};
#elif SYSTEM_PLAN_NUM == 3    //方案三
enum
{
    INTER_GTS_NUM1 = 10,
    INTER_GTS_NUM2 = 40,
    INTER_GTS_NUM3 = 100,
} inter_gts_num_t;
static uint8_t inter_gts_num[MAX_HOP_NUM] = {INTER_GTS_NUM1, INTER_GTS_NUM2, INTER_GTS_NUM3};
#endif
mac_pib_t mac_pib;

void pib_table_init(void)
{
    osel_memset(&mac_pib, 0, sizeof(mac_pib));
#if DEBUG_DEVICE_INFO_EN == 1    //调试用
    mac_pib.supf_cfg_arg.beacon_duration_order = BEACON_DURATION_ORDER;
    mac_pib.supf_cfg_arg.beacon_interv_order = BEACON_INTERVAL_ORDER;
    mac_pib.supf_cfg_arg.gts_duration_order = GTS_DURATION_ORDER;
    mac_pib.supf_cfg_arg.intra_gts_number = INTRA_GTS_NUM;
    mac_pib.supf_cfg_arg.cluster_number = CLUSTER_FRAME_NUM;
    mac_pib.supf_cfg_arg.inter_unit_number = MAX_HOP_NUM;
    mac_pib.supf_cfg_arg.intra_channel = CH_SN;
    mac_pib.supf_cfg_arg.inter_channel = CH_SN;
    mac_pib.mac_long_addr = NODE_ID;
    for(uint8_t i=0; i<MAX_HOP_NUM; i++)
    {
        mac_pib.supf_cfg_arg.inter_gts_number[i] = inter_gts_num[i];
    }
    mac_pib.intercom_index = MAX_HOP_NUM;               //自己的簇间时隙
    mac_pib.mac_short_addr = hal_board_get_device_short_addr((uint8_t *)&mac_pib.mac_long_addr);
#else
    mac_pib.supf_cfg_arg.beacon_duration_order = device_info.supf_cfg_arg.beacon_duration_order;
    mac_pib.supf_cfg_arg.beacon_interv_order = device_info.supf_cfg_arg.beacon_interv_order;
    mac_pib.supf_cfg_arg.gts_duration_order = device_info.supf_cfg_arg.gts_duration_order;
    mac_pib.supf_cfg_arg.intra_gts_number = device_info.supf_cfg_arg.intra_gts_number;
    mac_pib.supf_cfg_arg.cluster_number = device_info.supf_cfg_arg.cluster_number;
    mac_pib.supf_cfg_arg.inter_unit_number = device_info.supf_cfg_arg.inter_unit_number;
    mac_pib.supf_cfg_arg.intra_channel = device_info.supf_cfg_arg.intra_channel;
    mac_pib.supf_cfg_arg.inter_channel = device_info.supf_cfg_arg.inter_channel;
    osel_memcpy((uint8_t *)&mac_pib.mac_long_addr,
                (uint8_t *)&device_info.device_id[0], 8);
    for(uint8_t i=0; i<MAX_HOP_NUM; i++)
    {
        mac_pib.supf_cfg_arg.inter_gts_number[MAX_HOP_NUM-i-1] =
                device_info.supf_cfg_arg.inter_gts_number[i];
    }
    mac_pib.intercom_index = device_info.supf_cfg_arg.inter_unit_number;
    mac_pib.mac_short_addr = hal_board_get_device_short_addr((uint8_t *)&mac_pib.mac_long_addr);
#endif
    mac_pib.super_frame_index = GW_SO_NUM;              //自己的簇内时隙
    mac_pib.mac_max_resend_cnt = MAC_RESEND_NUM;
    mac_pib.mac_over_hear_flag = TRUE;                 //收听簇内其他beacon
}

uint8_t get_pib_beacon_seq_num(void)
{
    return mac_pib.mac_beacon_seq_num;
}

void operation_pib_beacon_seq_num(int8_t para)
{
    mac_pib.mac_beacon_seq_num += para;
}

uint16_t read_pib_short_addr(void)
{
    return mac_pib.mac_short_addr;
}

void write_pib_short_addr(uint16_t para)
{
    mac_pib.mac_short_addr = para;
}

uint64_t read_pib_long_addr(void)
{
    return mac_pib.mac_long_addr;
}

void write_pib_long_addr(uint64_t para)
{
    mac_pib.mac_short_addr = para;
}

supf_spec_t read_pib_supf_cfg_arg(void)
{
    return mac_pib.supf_cfg_arg;
}

uint8_t read_pib_super_frame_index(void)
{
    return mac_pib.super_frame_index;
}

void write_pib_super_frame_index(uint8_t para)
{
    mac_pib.super_frame_index = para;
}

uint8_t read_pib_intercom_index(void)
{
    return mac_pib.intercom_index;
}

void write_pib_intercom_index(uint8_t para)
{
    mac_pib.intercom_index = para;
}

uint8_t read_pib_intra_gts_index(void)
{
    return mac_pib.intra_gts_index;
}

void write_pib_intra_gts_index(uint8_t para)
{
    mac_pib.intra_gts_index = para;
}

uint8_t read_pib_resend_max_num(void)
{
    return mac_pib.mac_max_resend_cnt;
}

bool_t read_pib_mac_over_hear_flag(void)
{
    return mac_pib.mac_over_hear_flag;
}
