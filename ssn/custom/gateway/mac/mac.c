#include <debug.h>
#include <phy_state.h>
#include <mac_module.h>
#include <mac_prim.h>
#include <mac_global_variables.h>
#include <mac_frames.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <wsnos.h>

static void mac_task(void *e)
{
	DBG_ASSERT(e != NULL __DBG_LINE);

	m_module_ctrl_handler((event_block_t *)e);
}


void mac_init(void)
{
    phy_set_channel(CH_SN);
    phy_set_power(POWER_SN);
    
	osel_task_tcb *mac_task_tcb = osel_task_create(&mac_task, MAC_TASK_PRIO);

    osel_subscribe(mac_task_tcb, M_TRAN_CSMA_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_RXOK_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TXOK_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TX_ERROR_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TXUND_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_ACK_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_BACKOFF_SUCCESS_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_BACKOFF_FAIL_EVENT);
    osel_subscribe(mac_task_tcb, M_SLOT_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_MAC_PRIM_EVENT);
    
    m_tran_init();
    m_slot_init();
    m_sync_init();
    m_prim_init();

    mac_global_variables_init();
    mac_frames_cb_init();
    mac_beacon_init();
    mac_schedule_init();
}