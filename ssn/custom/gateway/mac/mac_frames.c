#include <stack.h>
#include <lib.h>
#include <prim.h>
#include <mac_pib.h>
#include <mac_beacon.h>
#include <mac_module.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>
#include <mac_global_variables.h>
#include <phy_packet.h>
#include <hal_board.h>

uint8_t mac_seqno = 0;

#if RECV_RF_RATE_EN
static bool_t mac_recv_success_rate(void)
{
    uint8_t rate = 0;
    rate = srandom(1,100);
    if(rate <= RECV_RF_SUCESS_RATE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
#endif

static pbuf_t *mac_frame_get(void)
{
    pbuf_t *frame = NULL;

    frame = phy_get_packet();

    return frame;
}

/***
**  地址过滤
**/
static bool_t mac_addr_filter(uint8_t frm_type, void * dst_addr, uint8_t addr_mode)
{
    uint64_t dst_long_addr;
    uint64_t pib_dst_long_addr;
    uint16_t dst_short_addr;
    uint16_t pib_dst_short_addr;
    
    if((frm_type != MAC_FRAME_TYPE_BEACON)&&(frm_type != MAC_FRAME_TYPE_ACK))
    {
        pib_dst_short_addr = read_pib_short_addr();
        if(addr_mode == MAC_MHR_ADDR_MODE_SHORT)
        {
            osel_memcpy(&dst_short_addr, dst_addr, MAC_ADDR_SHORT_SIZE);
            
            if(dst_short_addr != pib_dst_short_addr)
            {                 
                return FALSE;
            }
        }
        
        if(addr_mode == MAC_MHR_ADDR_MODE_LONG)
        {
            pib_dst_long_addr = read_pib_long_addr();
            osel_memcpy(&dst_long_addr, dst_addr, MAC_ADDR_LONG_SIZE);
            if(dst_long_addr != pib_dst_long_addr)
            {
                return FALSE;
            }           
        }
    }
    
#if RECV_RF_RATE_EN
    return mac_recv_success_rate();
#else
    return TRUE;
#endif
}

/***
**  解析mac头
**/
static bool_t mac_frame_head_info(pbuf_t *pbuf)
{
    bool_t result = TRUE;
    uint8_t type_code;
    uint8_t dst_addr_mode;    

    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;
	mac_frm_ctrl_t mac_frm_ctrl;
	osel_memcpy(&mac_frm_ctrl, pbuf->data_p, MAC_HEAD_CTRL_SIZE);
    type_code = mac_frm_ctrl.frm_type;
	mac_frm_head_info.frm_ctrl.frm_type 	= mac_frm_ctrl.frm_type;
	mac_frm_head_info.frm_ctrl.frm_pending 	= mac_frm_ctrl.frm_pending;
	mac_frm_head_info.frm_ctrl.ack_req 		= mac_frm_ctrl.ack_req;
	mac_frm_head_info.addr_info.dst_addr_m 	= mac_frm_ctrl.des_addr_mode;
	mac_frm_head_info.addr_info.src_addr_m 	= mac_frm_ctrl.src_addr_mode;
	dst_addr_mode = mac_frm_ctrl.des_addr_mode;
	pbuf->data_p += MAC_HEAD_CTRL_SIZE;
	mac_frm_head_info.mac_seq = *pbuf->data_p;
	pbuf->data_p += MAC_HEAD_SEQ_SIZE;

	if(mac_frm_head_info.addr_info.dst_addr_m == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy((void *)&mac_frm_head_info.addr_info.dst_addr, pbuf->data_p, 
                    MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
	}
	else if(mac_frm_head_info.addr_info.dst_addr_m == MAC_MHR_ADDR_MODE_SHORT)
	{
		osel_memcpy((void *)&mac_frm_head_info.addr_info.dst_addr, pbuf->data_p,
                    MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}
	if(mac_frm_head_info.addr_info.src_addr_m == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy((void *)&mac_frm_head_info.addr_info.src_addr, pbuf->data_p,
                    MAC_ADDR_LONG_SIZE);
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
	}
	else if(mac_frm_head_info.addr_info.src_addr_m == MAC_MHR_ADDR_MODE_SHORT)
	{
		osel_memcpy((void *)&mac_frm_head_info.addr_info.src_addr, pbuf->data_p, 
                    MAC_ADDR_SHORT_SIZE);
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}
	mac_frm_head_info.mhr_size = (pbuf->data_p - pbuf->head) - PHY_HEAD_SIZE;
	pbuf->attri.need_ack = mac_frm_ctrl.ack_req;
	if(mac_frm_ctrl.frm_type == MAC_FRAME_TYPE_ACK)
	{
		pbuf->attri.is_ack = TRUE;		
	}
    else
    {
        pbuf->attri.is_ack = FALSE;
    }

	pbuf->attri.seq = mac_frm_head_info.mac_seq;	
	pbuf->attri.src_id = mac_frm_head_info.addr_info.src_addr;
	if(!mac_addr_filter(type_code, 
                        (void *)&mac_frm_head_info.addr_info.dst_addr, 
                        dst_addr_mode))
	{
		result = FALSE;
	}	
	return result;
}
/***
**  mac层收到normal数据帧，发送接收数据原语data_ind
***/
static void mac_data_frame_parse(pbuf_t *pbuf)
{
    // 向网络层通知，获取到了数据
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    if(pbuf == NULL)
    {
        return;
    }
	sbuf_t *sbuf = sbuf_alloc(__SLINE1);
	if (sbuf == NULL)
	{
		DBG_ASSERT(FALSE __DBG_LINE);
	}

	mac_frm_ctrl_t mac_frm_ctrl;
	m2n_data_indication_t *mcps_data_ind = 
		        &(sbuf->primargs.prim_arg.mac_prim_arg.m2n_data_indication_arg);
	osel_memcpy(&mac_frm_ctrl, (pbuf->head + PHY_HEAD_SIZE), MAC_HEAD_CTRL_SIZE);
    
    mcps_data_ind->src_mode = mac_frm_ctrl.src_addr_mode;
	mcps_data_ind->dst_mode = mac_frm_ctrl.des_addr_mode;
    mcps_data_ind->msdu_length = pbuf->data_len - PHY_HEAD_SIZE - 
                                 mac_frm_head_info.mhr_size - MAC_FCS_SIZE;
	sbuf->primtype = M2N_DATA_INDICATION;		// 接收到data帧，往nwk传
	sbuf->primargs.pbuf = pbuf;

	pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE;
	if(mac_frm_ctrl.des_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy(&(mcps_data_ind->dst_addr), pbuf->data_p, MAC_ADDR_LONG_SIZE);
		mcps_data_ind->dst_mode = MAC_MHR_ADDR_MODE_LONG;
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
		
	}
	else if(mac_frm_ctrl.des_addr_mode == MAC_MHR_ADDR_MODE_SHORT)
	{
		osel_memcpy(&(mcps_data_ind->dst_addr), pbuf->data_p, MAC_ADDR_SHORT_SIZE);
		mcps_data_ind->dst_mode = MAC_MHR_ADDR_MODE_SHORT;
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}

	if(mac_frm_ctrl.src_addr_mode == MAC_MHR_ADDR_MODE_LONG)
	{
		osel_memcpy(&(mcps_data_ind->src_addr), pbuf->data_p, MAC_ADDR_LONG_SIZE);
		mcps_data_ind->src_mode = MAC_MHR_ADDR_MODE_LONG;
		pbuf->data_p += MAC_ADDR_LONG_SIZE;
	}
	else if(mac_frm_ctrl.src_addr_mode == MAC_ADDR_SHORT_SIZE)
	{
		osel_memcpy(&(mcps_data_ind->src_addr), pbuf->data_p, MAC_ADDR_SHORT_SIZE);
		mcps_data_ind->src_mode = MAC_ADDR_SHORT_SIZE;
		pbuf->data_p += MAC_ADDR_SHORT_SIZE;
	}
	mcps_data_ind->msdu = pbuf->data_p; // 忽略 密码 区
	osel_post(MAC2NWK_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void ack_tx_ok_callback(sbuf_t *sbuf, bool_t res)
{
    if(!m_slot_get_state())
    {
        m_tran_recv();
    }
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
	sbuf_free(&sbuf __SLINE2);
}

static void mac_agreement_fill_ack(pbuf_t *pbuf, uint8_t seqno)
{
    uint8_t resend_num = read_pib_resend_max_num();
    
    mac_frm_ctrl_t frm_ctrl;
    uint8_t *datap = NULL;
    frm_ctrl.frm_type = MAC_FRAME_TYPE_ACK;
    frm_ctrl.sec_enable = FALSE;
    frm_ctrl.frm_pending = FALSE;
    frm_ctrl.des_addr_mode = MAC_MHR_ADDR_MODE_FALSE;
    frm_ctrl.src_addr_mode = MAC_MHR_ADDR_MODE_FALSE;
    frm_ctrl.reseverd = 0;
    frm_ctrl.ack_req = FALSE;
    
    datap = pbuf->head + PHY_HEAD_SIZE;
    osel_memcpy(datap, &frm_ctrl, MAC_HEAD_CTRL_SIZE);
    datap += MAC_HEAD_CTRL_SIZE;
    *datap = seqno;
    datap++;
    uint8_t size = MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE + PHY_FCS_SIZE;        

    pbuf->data_len = size;
    pbuf->attri.seq = seqno;
    pbuf->attri.need_ack = FALSE;
    pbuf->attri.is_ack = TRUE;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    if(mac_frm_head_info.addr_info.src_addr_m == MAC_MHR_ADDR_MODE_LONG)
    {
        pbuf->attri.dst_id = 0xffff;
    }
    else
    {
        pbuf->attri.dst_id = (uint16_t)mac_frm_head_info.addr_info.src_addr;
    }
}

/***
**  发送ack
***/
static void mac_send_ack(uint8_t seqno)
{
    pbuf_t *pbuf = pbuf_alloc(32 __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    if(pbuf == NULL)
    {
        return;
    }
    
    mac_agreement_fill_ack(pbuf, seqno);
 
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    
    if(sbuf !=NULL)
    {
        sbuf->primargs.pbuf = pbuf;
        
        if(m_tran_can_send())
        {
            m_tran_send(sbuf, ack_tx_ok_callback, 1);
        }
        else
        {
            pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
            sbuf_free(&sbuf __SLINE2);
        }
    }
    else
    {
        pbuf_free(&pbuf __PLINE2);
    }
}

/***
**  解析载荷
***/
static bool_t mac_frame_parse(pbuf_t *pbuf)
{
	mac_frm_ctrl_t *mac_frm_ctrl;
    uint8_t frm_type = MAC_FRAME_TYPE_RESERVED;

	mac_frm_ctrl =(mac_frm_ctrl_t *)(pbuf->head + PHY_HEAD_SIZE);
    
	frm_type = mac_frm_ctrl->frm_type;

	switch(frm_type)
	{
		case MAC_FRAME_TYPE_BEACON:
            //收到信标帧
            recv_beacon(pbuf);
            pbuf_free(&pbuf __PLINE2);
			break;
            
        case MAC_FRAME_TYPE_DATA:
            //收到数据帧
            mac_data_frame_parse(pbuf);
            break;
            
        case MAC_FRAME_TYPE_COMMAND:
            //收到命令帧
            mac_cmd_frame_parse(pbuf);
            pbuf_free(&pbuf __PLINE2);
            break;
            
        case MAC_FRAME_TYPE_ACK:
            //收到ACK
            pbuf_free(&pbuf __PLINE2);
           
            break;
            
		default:
			// 未知类型帧;
			pbuf_free(&pbuf __PLINE2);
			break;
	}

	return TRUE;
}

static void mac_tx_finish(sbuf_t *const sbuf, bool_t result)
{
    ;
}

void mac_frames_cb_init(void)
{
    tran_cfg_t mac_tran_cb;
    mac_tran_cb.frm_get             = mac_frame_get;
    mac_tran_cb.frm_head_parse      = mac_frame_head_info;
    mac_tran_cb.frm_payload_parse   = mac_frame_parse;
    mac_tran_cb.tx_finish           = mac_tx_finish;
    mac_tran_cb.send_ack            = mac_send_ack;
    
    m_tran_cfg(&mac_tran_cb);       // 传输模块回调函数初始化  
}