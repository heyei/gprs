#include <stdlib.h>
#include <stack.h>
#include <lib.h>
#include <wsnos.h>
#include <node_cfg.h>
#include <mac_global_variables.h>
#include <mac_cmd_frame.h>
#include <mac_pib.h>
#include <hal_timer.h>
#include <assoc_table.h>


#define PEND_ADDR_NUM                   (7u)

uint16_t loc_beacon_map = 0;
uint16_t last_beacon_map;
uint16_t alloc_bcn_bitmap = 0x0001; 

list_head_t intra_gts_buf;
list_head_t inter_gts_buf;
list_head_t assoc_buf;
list_head_t pend_addr_list_free[2];
list_head_t pend_addr_list_alloc[2];
pend_short_addr_list_t short_addr_list[PEND_ADDR_NUM];
pend_long_addr_list_t long_addr_list[PEND_ADDR_NUM];

mac_frm_head_info_t mac_frm_head_info;

uint32_t track_slot_remain_time;
uint32_t track_slot_time;

void mac_global_variables_init(void)
{
    uint8_t i = 0;
    loc_beacon_map = 0;
    
    list_init(&intra_gts_buf);        
    list_init(&inter_gts_buf);
    list_init(&assoc_buf);
    for(i =0; i<2; i++)
    {
        list_init(&pend_addr_list_free[i]);
        list_init(&pend_addr_list_alloc[i]);
    }
    
    for (uint8_t j=0; j<PEND_ADDR_NUM; j++)
    {
        list_init(&short_addr_list[j].list);
        list_init(&long_addr_list[j].list);
        list_add_to_tail(&short_addr_list[j].list, &pend_addr_list_free[0]);
        list_add_to_tail(&long_addr_list[j].list, &pend_addr_list_free[1]);
    }
    assoc_table_init();
    beacon_aging_init();
    pib_table_init();
}

uint8_t srandom(uint8_t min, uint8_t max)         //����stdlib.h ͷ�ļ�
{
    DBG_ASSERT(max >= min __DBG_LINE);
    srand(TA0R);
    return (min + rand()%(max-min+1));
}