#ifndef __APP_PRIM_H__
#define __APP_PRIM_H__

#include <prim.h>
#include <sbuf.h>

void app_prim_handler(pbuf_t *sbuf);

#endif

