#ifndef __APP_H__
#define __APP_H__
#include <hal_timer.h>
typedef struct
{
    hal_timer_t *cycle_timer_handle;
}app_info_t;

void app_init(void);

#endif
