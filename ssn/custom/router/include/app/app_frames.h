#ifndef __APP_FRAME_H__
#define __APP_FRAME_H__

#include <phy_packet.h>
#include <mac_frames.h>
#include <nwk_frames.h>

#define FRAME_APP_OFFSET_LEN    (PHY_HEAD_SIZE                                 \
            + MAC_HEAD_CTRL_SIZE + MAC_HEAD_SEQ_SIZE + MAC_ADDR_SHORT_SIZE*2   \
            + NWK_HEAD_SIZE + NWK_ADDR_SHORT_SIZE*2)

#endif
