#ifndef __APP_NWK_H
#define __APP_NWK_H

/// 获取NWK层所依赖的APP的接口函数
/// 参数：
///     无
/// 返回：
///     NWK层所依赖的APP的接口函数集
nwk_dependent_t *app_get_nwk_dependent();

#endif