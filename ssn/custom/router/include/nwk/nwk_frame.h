#ifndef __NWK_FRAMES_H
#define __NWK_FRAMES_H

/// NWK发送入网请求所需的信息
typedef struct
{
    nwk_node_addr_t dest_addr;       // 目的地址
    nwk_node_addr_t src_addr;        // 源地址
    uint8_t         device_type;     // 设备类型
    uint32_t        join_apply_time; // 入网请求时间
}nwk_join_request_t;

/// NWK数据帧载荷内容
typedef struct
{
    const uint8_t *payload;
    uint8_t        payload_len;
}nwk_data_payload_t;

/// NWK入网请求帧载荷内容
typedef struct
{
    uint8_t  device_type;     // 设备类型
    uint32_t join_apply_time; // 入网请求的时间
}nwk_join_request_payload_t;

/// NWK入网应答帧载荷内容
typedef struct
{
    uint16_t mac_short_addr; // MAC短地址
    uint16_t short_addr;     // NWK短地址
}nwk_join_resp_payload_t;

/// NWK帧头控制域的结构
typedef struct
{
    uint8_t type : 3;      // 帧类型
    uint8_t dest_mode : 2; // 目的地址模式
    uint8_t src_mode : 2;  // 源地址模式
    uint8_t reserved : 1;
}nwk_frame_control_t;

/// NWK帧的结构
typedef struct
{
    uint8_t         frame_type;     // 帧类型
    nwk_node_addr_t dest_addr;      // 目的地址
    nwk_node_addr_t src_addr;       // 源地址
    union
    {
        nwk_data_payload_t         data_payload; // 数据帧的载荷
        nwk_join_request_payload_t join_request; // 入网请求帧的载荷内容
        nwk_join_resp_payload_t    join_resp;    // 入网应答帧的载荷内容
    };
}nwk_frame_t;

/// NWK心跳帧控制域
typedef struct
{
    uint8_t device_type : 2;   // 设备类型
    uint8_t energy_report : 2; // 能量上报类型
    uint8_t alarm_type : 2;    // 警报信息类型
    uint8_t location : 1;      // 是否携带定位信息
    uint8_t overhead_mode : 1; // 是否携带邻居信息
}nwk_hb_control_t;

/// Localization Info 链路质量元组结构
typedef struct
{
    uint16_t src_addr;    // 源地址
    uint8_t  lqi;         // 链路质量
    uint32_t time_stamp;  // 时戳
}nwk_location_lqi_tuple_t;

/// Neighbour Info 链路质量元组
typedef struct
{
    uint16_t dest_addr;  // 目的地址
    uint16_t src_addr;   // 源地址
    uint8_t  lqi;        // 链路质量
    uint32_t time_stamp; // 时戳
}nwk_neighbour_lqi_tuple_t;

/// NWK发送心跳帧所需的信息
typedef struct
{
    nwk_node_addr_t                  dest_addr;             // 目的地址
    nwk_node_addr_t                  src_addr;              // 源地址
    nwk_hb_control_t                 frame_control;         // 心跳帧控制域
    uint16_t                         parent_addr;           // 上级地址
    uint8_t                          tx_success_rate;       // 传输成功率
    uint8_t                          residual_energy;       // 剩余能量
    uint16_t                         alarm_info;            // 警报信息
    uint8_t                          location_tuple_num;    // 定位信息数量
    const nwk_location_lqi_tuple_t  *location_tuple_group;  // 定位信息组
    uint8_t                          neighbour_tuple_num;   // 邻居信息数量
    const nwk_neighbour_lqi_tuple_t *neighbour_tuple_group; // 邻居信息组
}nwk_hb_t;

/// 组装NWK数据帧
/// 参数：
///     data            数据信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_data_frame(const nwk_data_t *data,
                           uint8_t          *frame,
                           uint8_t           max_frame_len);

/// 组装NWK入网请求帧
/// 参数：
///     data            入网请求信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_join_frame(const nwk_join_request_t *join,
                           uint8_t                  *frame,
                           uint8_t                   max_frame_len);

/// 组装NWK心跳帧
/// 参数：
///     hb              心跳信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_hb_frame(const nwk_hb_t *hb,
                         uint8_t        *frame,
                         uint8_t         max_frame_len);

/// 解析MAC数据载荷
/// 参数：
///     frame       MAC载荷内容的地址
///     frame_len   MAC载荷内容的长度
///     nwk_frame   输出已解析的内容
/// 返回：
///     TRUE   解析成功
///     FALSE  解析失败
bool_t nwk_parse_frame(const uint8_t *frame,
                       uint8_t        frame_len,
                       nwk_frame_t   *nwk_frame);

#endif
