#ifndef __NWK_H__
#define __NWK_H__

#define NWK_JOIN_REQUEST_FRAME_LEN  16 // 入网请求的网络帧长度

/// NWK帧类型
typedef enum
{
    NWK_DATA = 0,      // 数据帧
    NWK_JOIN = 1,      // 入网帧
    NWK_JOIN_RESP = 2, // 入网应答帧
    NWK_HB = 3,        // 心跳帧
}nwk_frame_type_t;

/// NWK地址模式
typedef enum
{    
    NWK_NO_ADDR = 0,     // 无效的地址
    NWK_SHORT_ADDR = 2,  // 短地址类型
    NWK_LONG_ADDR = 3,   // 长地址类型
}nwk_addr_mode_t;

/// NWK的枚举类型，表示结果或状态等等
typedef enum
{
    // [0, 10], MAC和NWK共同定义
    NWK_SUCESS = 0,
    // [11, 20], MAC单独定义
    NWK_MAC_NO_ACK = 11,
    // [21, 30], NWK单独定义
}nwk_enum_t;

/// 网络地址
typedef struct
{
    nwk_addr_mode_t mode;       // 地址模式
    union
    {
        uint8_t   long_addr[8]; // 8字节长地址
        uint16_t  short_addr;   // 2字节短地址
    };
}nwk_node_addr_t;

/// NWK发送数据所需的信息
typedef struct
{
    nwk_node_addr_t dest_addr;      // 目的地址
    nwk_node_addr_t src_addr;       // 源地址
    uint8_t         payload_len;    // 载荷长度
    const uint8_t  *payload;        // 载荷内容
}nwk_data_t;

/// NWK通知APP的数据信息
typedef struct
{
    nwk_node_addr_t dest_addr;      // 目的地址
    nwk_node_addr_t src_addr;       // 源地址
    uint8_t         payload_len;    // 载荷长度
    const uint8_t  *payload;        // 载荷内容
}nwk_data_indication_t;

/// NWK层所依赖的上层接口函数原形
typedef struct
{
    void (*nwk_data_confirm) (nwk_enum_t status,
                              uint8_t    handle);        // NWK发送数据后返回确认的回调函数
    void (*nwk_data_indicate) (const nwk_data_indication_t *data);  
                                                         // NWK收到数据后通知APP的回调函数
}nwk_dependent_t;

/// NWK协议信息属性的结构
typedef struct
{
    nwk_node_addr_t short_addr;           // 本设备短地址
    nwk_node_addr_t long_addr;            // 本设备长地址
    nwk_node_addr_t parent_short_addr;    // 上级短地址
    nwk_node_addr_t sink_addr;            // Sink地址
    bool_t          is_joined;            // 入网状态
    bool_t          is_mac_assoc;         // MAC是否关联
    bool_t          has_reset_alarm;      // 是否上电后从未入网
    bool_t          has_rejoin_alarm;     // 是否是重入网
    const nwk_dependent_t *nwk_dependent; // NWK依赖的上层接口
    uint32_t        hb_period;            // 心跳帧周期，单位ms
}nwk_pib_t;

extern nwk_pib_t nwk_pib; //NWK协议信息属性

/// 请求NWK发送数据，异步
/// 参数：
///     data        要发送的数据信息
///     handle      此次发送请求的标识
/// 返回：
///     无
void nwk_data_request(const nwk_data_t     *data,
                      uint8_t               handle);

/// 判断两个地址是否相同
bool_t nwk_is_node_addr_equal(const nwk_node_addr_t *addr_1,
                              const nwk_node_addr_t *addr_2);

/// 请求NWK进入入网请求流程
void nwk_join_request(void);

/// 请求NWK发送心跳帧
void nwk_hb_request();

/// 初始化NWK协议栈
/// 参数：
///     dependent NWK层所依赖的上层接口函数集
void nwk_stack_init(const nwk_dependent_t *dependent);

#endif
