#ifndef __NWK_MAC_H
#define __NWK_MAC_H

/// 获取MAC层所依赖的NWK的接口函数
/// 参数：
///     无
/// 返回：
///     MAC层所依赖的NWK的接口函数集
const mac_dependent_t *nwk_get_mac_dependent();

#endif