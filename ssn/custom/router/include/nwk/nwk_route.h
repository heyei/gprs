#ifndef __NWK_ROUTE_H
#define __NWK_ROUTE_H

#define NWK_MAX_ROUTE_TABLE_SIZE    128 // 路由表的最大容量
#define NWK_MAX_ROUTE_LIFE          10   // 路径的生存时间，以NWK心跳周期为单位

/// 初始化路由表
/// 参数：
///     无
/// 返回：
///     TRUE  成功
///     FALSE 失败
bool_t nwk_route_init();

/// 在邻居表中查找下一跳MAC地址
/// 参数：
///     dest_addr 目的NWK地址
///     next_hop  输出下一跳的MAC地址
/// 返回：
///     TRUE  成功找到
///     FALSE 没有找到
bool_t nwk_route_get_next_hop_in_neighbour(const nwk_node_addr_t  *dest_addr,
                                           mac_node_addr_t        *next_hop);

/// 查找下行的下一跳MAC地址
/// 参数：
///     dest_addr 目的NWK地址
///     next_hop  输出下一跳的MAC地址
/// 返回：
///     TRUE  成功找到
///     FALSE 没有找到
bool_t nwk_route_get_next_hop_downward(const nwk_node_addr_t  *dest_addr,
                                       mac_node_addr_t        *next_hop);

/// 添加路由表项
/// 参数：
///     dest_addr 目的NWK地址
///     next_hop  下一跳的MAC地址
/// 返回：
///     TRUE  添加成功
///     FALSE 添加失败
bool_t nwk_route_add_item(const nwk_node_addr_t  *dest_addr,
                          const mac_node_addr_t  *next_hop);

/// 更新路由表项的剩余生存期
void nwk_route_update_lifecycle();

#endif