#include <data_type_def.h>
#include <sbuf.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_route.h>

typedef struct
{
    nwk_node_addr_t dest_addr;
    mac_node_addr_t next_hop;
    uint8_t         life;
}nwk_route_t;

static nwk_route_t nwk_route_table[NWK_MAX_ROUTE_TABLE_SIZE];

/// 初始化路由表
bool_t nwk_route_init()
{
    for (uint8_t i = 0; i < NWK_MAX_ROUTE_TABLE_SIZE; i++)
    {
        nwk_route_table[i].life = 0;
    }
    
    return TRUE;
}

/// 在邻居表中查找下一跳MAC地址
bool_t nwk_route_get_next_hop_in_neighbour(const nwk_node_addr_t  *dest_addr,
                                           mac_node_addr_t        *next_hop)
{
    if (NWK_SHORT_ADDR != dest_addr->mode) // 目前只能获取到邻居的短地址, NWK短地址等于MAC短地址
    {
        return FALSE;
    }
    mac_node_addr_t dest;
    dest.mode = MAC_SHORT_ADDR;
    dest.short_addr = dest_addr->short_addr;
    mac_node_info_t neighbour;
    if (TRUE == mac_find_neighbour(&dest, &neighbour))
    {
        *next_hop = neighbour.short_addr;
        return TRUE;
    }
    
    return FALSE;
}

/// 查找下行的下一跳MAC地址
bool_t nwk_route_get_next_hop_downward(const nwk_node_addr_t  *dest_addr,
                                       mac_node_addr_t        *next_hop)
{
    for (uint8_t i = 0; i < NWK_MAX_ROUTE_TABLE_SIZE; i++)
    {
        if (0 != nwk_route_table[i].life)
        {
            if (TRUE == nwk_is_node_addr_equal(&nwk_route_table[i].dest_addr, dest_addr))
            {
                *next_hop = nwk_route_table[i].next_hop;
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

/// 添加路由表项
bool_t nwk_route_add_item(const nwk_node_addr_t  *dest_addr,
                          const mac_node_addr_t  *next_hop)
{
    uint8_t invalid = NWK_MAX_ROUTE_TABLE_SIZE;
    
    for (uint8_t i = 0; i < NWK_MAX_ROUTE_TABLE_SIZE; i++)
    {
        if (0 != nwk_route_table[i].life)
        {
            if (TRUE == nwk_is_node_addr_equal(&nwk_route_table[i].dest_addr, dest_addr))
            {
                nwk_route_table[i].next_hop = *next_hop; // 更新已有路径
                nwk_route_table[i].life = NWK_MAX_ROUTE_LIFE;
                return TRUE;
            }
        }
        else
        {
            if (NWK_MAX_ROUTE_TABLE_SIZE == invalid)
            {
                invalid = i;
            }
        }
    }
    if (NWK_MAX_ROUTE_TABLE_SIZE != invalid)
    { // 添加新路径
        nwk_route_table[invalid].dest_addr = *dest_addr;
        nwk_route_table[invalid].next_hop = *next_hop;
        nwk_route_table[invalid].life = NWK_MAX_ROUTE_LIFE;
        return TRUE;
    }
    
    return FALSE;
}

/// 更新路由表项的生存期
void nwk_route_update_lifecycle()
{
    for (uint8_t i = 0; i < NWK_MAX_ROUTE_TABLE_SIZE; i++)
    {
        if (0 != nwk_route_table[i].life)
        {
            nwk_route_table[i].life--;
        }
    }
}
