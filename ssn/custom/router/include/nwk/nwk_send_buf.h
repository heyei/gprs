#ifndef __NWK_SEND_BUF_H
#define __NWK_SEND_BUF_H

#define NWK_HB_SEND_HANDLE      0xF0
#define NWK_JOIN_SEND_HANDLE    0xF1

/// 判断是否是心跳帧的标识
/// 参数：
///     handle NWK层向MAC层进行数据发送请求时使用的标识，
///             用来区分不同的数据请求
/// 返回：
///     TRUE  是
///     FALSE 不是
bool_t nwk_is_hb_handle(uint8_t handle);

/// 判断是否是入网请求帧的标识
/// 参数：
///     handle NWK层向MAC层进行数据发送请求时使用的标识，
///             用来区分不同的数据请求
/// 返回：
///     TRUE  是
///     FALSE 不是
bool_t nwk_is_join_handle(uint8_t handle);

/// 判断是否是数据帧的标识
/// 参数：
///     handle NWK层向MAC层进行数据发送请求时使用的标识，
///             用来区分不同的数据请求
/// 返回：
///     TRUE  是
///     FALSE 不是
bool_t nwk_is_data_handle(uint8_t handle);

#endif