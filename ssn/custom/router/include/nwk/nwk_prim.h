#ifndef __NWK_PRIM_H__
#define __NWK_PRIM_H__

#include <sbuf.h>

/// 处理协议原语事件的函数
void nwk_prim_event_handler(pbuf_t *pbuf);

#endif

