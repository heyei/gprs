#ifndef __NWK_JOIN_H
#define __NWK_HOIN_H

#define NWK_REJOIN_CNT_MAX  3 // 启动入网请求流程后，重复发送入网请求的最大次数

/// 设置成入网状态，会同时启动心跳流程
void nwk_set_join_state();

/// 处理入网响应
/// 参数：
///     short_addr 入网响应中的网络短地址
void nwk_join_response_handler(uint16_t short_addr);

/// 入网请求事件的处理函数，会启动入网请求流程
void nwk_join_request_event_handler();

/// 入网请求流程中的定时器事件的处理函数
void nwk_join_request_timer_event_handler();

#endif