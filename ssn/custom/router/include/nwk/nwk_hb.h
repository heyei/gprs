#ifndef __NWK_HB_H
#define __NWK_HB_H

#define NWK_MAX_LOCATION_TUPLE_NUM      5

/// 启动发送NWK心跳的流程
/// 参数：
///     period 心跳周期，单位ms
/// 返回：
///    无
void nwk_hb_start(uint32_t period);

/// 处理心跳帧流程的定时器事件
void nwk_hb_timer_event_handler();

/// 添加发送成功率所需的信息
/// 参数：
///     tx_success  某条数据MAC层是否发送成功
///     send_times  某条数据MAC层总共发送了多少次
/// 返回：
///     无
void nwk_hb_add_tx_info(bool_t  tx_success,
                        uint8_t send_times);

/// 清除心跳帧历史信息
void nwk_hb_clear();

/// 添加定位信息
/// 参数：
///     tuple 定位信息
/// 返回：
///     无
void nwk_add_location_tuple(const nwk_location_lqi_tuple_t *tuple);

/// 获取定位信息表的地址和定位信息的数量
/// 参数：
///     p_group 用来输出定位信息表的地址
/// 返回：
///     定位信息的数量
uint8_t nwk_get_location_tuple_group(const nwk_location_lqi_tuple_t **p_group);

#endif
