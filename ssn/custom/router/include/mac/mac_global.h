#ifndef __MAC_GLOBAL_H__
#define __MAC_GLOBAL_H__

#define EMPTY_ADDR                      0x0000
#define MAX_PHY_PACKET_SIZE             127
#define CLUSTER_FRAME_NUM               (8u)
#define MAX_PENDING_SHORT_ADDR_NUM      (7u)
#define MAX_PENDING_LONG_ADDR_NUM       (2u)

typedef enum
{
    MAIN_STATE_IDLE = 0,
    MAIN_STATE_SCAN,
    MAIN_STATE_SCHEDULING,
} mac_mainstates_t;

typedef enum
{
    SUB_STATE_IDLE = 0,
    SUB_STATE_ASSOCIATING,
    SUB_STATE_ASSOCIATION_OK,
} mac_substates_t;

typedef struct
{
    mac_mainstates_t ms;
    mac_substates_t ss;
} mac_state_t;

/* mac_pib.h*/
typedef struct
{
    uint8_t self_index;
    uint8_t mac_bsn;
    uint8_t mac_dsn; 
    uint8_t hops;
    uint64_t mac_coord_long_addr;  // coord IEEE address
    uint16_t mac_coord_short_addr;
    uint8_t  mac_coord_beacon_index;
    uint64_t mac_long_addr;        // self IEEE address
    uint16_t mac_short_addr;
    bool_t beacon_enabled;
    superframe_spec_t supfrm_cfg_arg;
    uint8_t beacon_seq;
    pend_addr_spec_t pending_spec;
    uint16_t pending_short_addr_list[MAX_PENDING_SHORT_ADDR_NUM];
    uint64_t pending_long_addr_list[MAX_PENDING_LONG_ADDR_NUM];
    const mac_dependent_t *mac_dependent;
    uint8_t nwk_depth;
} mac_pib_t;


typedef struct
{
    list_head_t list;
    sbuf_t *sbuf;
} data_buf_t;


extern list_head_t gts_buf[1];          // for my gts

extern mac_pib_t mac_pib;
extern mac_state_t mac_state;
extern uint64_t node_nui;
extern uint16_t loc_beacon_map;
extern uint16_t last_beacon_map;
extern uint16_t alloc_bcn_bitmap;

void mac_global_init(void);
void mac_global_reset(void);

#endif

