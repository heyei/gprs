#ifndef __MAC_MONITER_H__
#define __MAC_MONITER_H__

#define MAC_MONITER_CYCLE_TIME      (8000ul)

extern neighbor_node_t coord;

void mac_moniter_timer_stop(void);

void mac_moniter_start(void);

void mac_moniter_stop(void);

void mac_moniter_init(void);

#endif
