/**
 * @DESCRIPTION: mac层原语解析头文件
 *
 * @FILE: $FILE_FNAME$
 *
 * @CREATED: 2011-8-18, by chenggang
 *
 * @{
 */
#ifndef __MAC_PRIM_H__
#define __MAC_PRIM_H__

#define ASSOC_REQUEST_CNT_MAX       (3u)

void m_prim_init(void);
 
#endif
