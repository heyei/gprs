#ifndef __MAC_STACK_H
#define __MAC_STACK_H

/// MAC帧类型
typedef enum
{
    MAC_BEACON,  // Beacon帧
    MAC_DATA,    // MAC数据帧
    MAC_ACK,     // MAC应答帧
    MAC_COMMAND, // MAC命令帧
}mac_frame_type_t;

/// 设备类型
typedef enum
{
    END_NODE_TYPE, // 终端
    ROUTER_TYPE,   // 中继（或称路由，骨干设备）
    SINK_TYPE,     // Sink（或称网关，网络管理设备）
}device_type_t;

/// MAC地址模式
typedef enum
{
    MAC_NO_ADDR = 0,     // 无效的地址
    MAC_SHORT_ADDR = 2,  // 短地址类型
    MAC_LONG_ADDR = 3,   // 长地址类型
}mac_addr_mode_t;

/// MAC的枚举类型，表示结果或状态等等
typedef enum
{
    // [0, 10], MAC和NWK共同定义
    MAC_SUCCESS = 0,
    // [11, 20], MAC单独定义
    MAC_NO_ACK = 11,
    MAC_UNASSOCIATION_STATUS = 12,
}mac_enum_t;

/// 发送选项
typedef enum
{
    INTRA_DURATION_SENDING, // 簇内时隙发送
    INTER_DURATION_SENDING, // 簇间时隙发送
}send_option_t;

/// MAC地址信息
typedef struct
{
    mac_addr_mode_t mode;       // 地址模式
    union
    {
        uint8_t   long_addr[8]; // 8字节长地址
        uint16_t  short_addr;   // 2字节短地址
    };
}mac_node_addr_t;

/// 节点信息
typedef struct
{
    device_type_t   type;         // 节点设备类型
    mac_node_addr_t short_addr;   // 节点MAC短地址
    mac_node_addr_t long_addr;    // 节点MAC长地址
    uint8_t         depth;        // 在网络中的深度
    uint8_t         beacon_index; // 节点在簇内工作的序号
}mac_node_info_t;

/// 安全处理信息
typedef struct
{
    uint8_t *empty; // 暂不定义
}aux_security_header_t;

/// MAC发送数据所需的信息
typedef struct
{
    bool_t                  security;               // 是否使能安全机制
    aux_security_header_t   aux_security_header;    // 安全处理信息
    mac_node_addr_t         dest_addr;              // 目的地址
    mac_node_addr_t         src_addr;               // 源地址
    uint8_t                 payload_len;            // 载荷长度
    uint8_t                *payload;                // 载荷内容
}mac_data_t;

/// MAC通知NWK的数据信息
typedef struct
{
    bool_t                  security;                      // 是否使能安全机制
    aux_security_header_t   aux_security_header;           // 安全处理信息
    mac_node_addr_t         dest_addr;                     // 目的地址
    mac_node_addr_t         src_addr;                      // 源地址
    uint8_t                 payload_len;                   // 载荷长度
    uint8_t                *payload;                       // 载荷内容
    uint8_t                 rssi;                          // rssi值
}mac_data_indication_t;

/// MAC向NWK返回数据发送的确认信息
typedef struct
{
    uint8_t    handle;
    mac_enum_t status;
    uint8_t    send_times;
}mac_data_confirm_t;

/// 接收到的Beacon的一些信息，这些信息需要通知NWK
typedef struct
{
    mac_node_addr_t neighbour_addr;     // 邻居短地址
    uint8_t         neighbour_depth;    // 邻居网络深度
}beacon_indication_t;

/// 已建立关联的一些信息，这些信息需要通知NWK
typedef struct
{
    mac_node_addr_t parent_addr;    // 上级短地址
    uint8_t         parent_depth;   // 上级网络深度
}association_confirm_t;

/// 需要通知NWK与上级失去关联的信息
typedef struct
{
    bool_t reverse; // 暂无内容
}unassociation_indication_t;

/// 已允许的关联请求的信息，这些信息需要通知NWK
typedef struct
{
    mac_node_addr_t child_addr; // 下级长地址
    device_type_t   child_type; // 下级设备类型
    bool_t          security;   // 下级是否使用安全机制
}association_indication_t;

typedef struct
{
    void (*mac_data_indicate)(const mac_data_indication_t *data);
                                                           // MAC通知NWK收到数据的接口
    void (*mac_data_confirm) (const mac_data_confirm_t *data_confirm);          
                                                           // MAC发送数据后返回确认的回调函数
    void (*mac_unassociation_indicate)(const unassociation_indication_t *unassoc);
                                                           // MAC通知NWK与上级设备失去关联的接口
    void (*mac_association_confirm)(const association_confirm_t *asso_confirm);
                                                           // MAC通知NWK已成功与上级设备建立关联的接口，关联请求由MAC自行发起
}mac_dependent_t;

/// 初始化MAC协议栈
/// 参数：
///     dependent MAC层所依赖的上层接口函数集
void mac_stack_init(const mac_dependent_t *dependent);

/// 判断两个MAC地址是否相同
bool_t mac_is_node_addr_equal(const mac_node_addr_t *addr_1,
                              const mac_node_addr_t *addr_2);

/// 请求MAC与上级关联
void mac_association_request();

/// 请求MAC发送数据，异步
/// 参数：
///     data        要发送的数据信息
///     handle      此次发送请求的标识
/// 返回：
///     无
void mac_data_request(const mac_data_t     *data,
                      uint8_t               handle);

////////////////////////////////////////////////////////////////////////////////
/// 说明：被NWK任务调用的函数，这些函数在内部会访问MAC数据，需要在内部做好互斥。
///
/// 判断是否是下级节点
bool_t mac_is_child(const mac_node_addr_t *child_addr);
    
/// 查找下级节点
bool_t mac_find_child(const mac_node_addr_t *child_addr,
                      mac_node_info_t       *child_info);

/// 添加下级节点
bool_t mac_add_child(const mac_node_info_t *child);

/// 查找邻居节点
bool_t mac_find_neighbour(const mac_node_addr_t *neighbour_addr,
                          mac_node_info_t       *neighbour_info);

/// 获取上级节点信息
bool_t mac_get_parent(mac_node_info_t *parent);

/// 获取同步的全局时间
uint32_t mac_get_global_time();

/// 使能MAC的Beacon
/// 参数：
///     short_addr  入网应答中的MAC短地址
void mac_beacon_enable(const mac_node_addr_t *short_addr);

/// 获取MAC长地址
void mac_get_long_addr(mac_node_addr_t *long_addr);

/// 获取超帧周期
bool_t mac_get_beacon_interval(uint32_t *interval);
///
////////////////////////////////////////////////////////////////////////////////

#endif

