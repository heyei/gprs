#ifndef __MAC_FRAMES_H__
#define __MAC_FRAMES_H__

/**
 * MAC frame type definition
 */

#define MAC_FRAME_TYPE_BEACON           0u
#define MAC_FRAME_TYPE_DATA             1u
#define MAC_FRAME_TYPE_ACK              2u
#define MAC_FRAME_TYPE_COMMAND          3u
#define MAC_FRAME_TYPE_RESERVED         4u

/**
 * Stack
 */
#define ADDR_MODE_NONE                  0x00
#define ADDR_MODE_RESERVED              0x01
#define ADDR_MODE_SHORT                 0x02
#define ADDR_MODE_LONG                  0x03



#define MAC_FCS_SIZE                    0u
#define MAC_HEAD_CTRL_SIZE              2u
#define MAC_HEAD_SEQ_SIZE               1u
#define MAC_ADDR_SHORT_SIZE             2U
#define MAC_ADDR_LONG_SIZE              8U

#define MAC_BROADCAST_ADDR              0xFFFF

#pragma pack(1)

/**
 * MAC frame control field struct definition
 */
typedef struct
{
    uint16_t frm_type           :    3,             // frame type;
             sec_enable         :    1,             // security enalbled;
             frm_pending        :    1,             // frame pending;
             ack_req            :    1,             // ACK request;
             des_addr_mode      :    2,             // dest addressing mode;
             src_addr_mode      :    2,             // soure addressing mode
             reseverd           :    6;             // reseverd
} mac_frm_ctrl_t;

#pragma pack()

// ---------------mac frame head info ----------//
typedef struct
{
    uint8_t         mhr_size;
    mac_frm_ctrl_t  frm_ctrl;
    uint8_t         mac_seq;
    struct
    {
        uint64_t    dst_addr;
        uint64_t    src_addr;
    } addr_info;
} mac_frm_head_info_t;

extern mac_frm_head_info_t mac_frm_head_info;

void mac_frames_clr_pend_flag(void);

bool_t mac_frames_get_pend_flag(void);

void mac_frames_init(void);

#endif
