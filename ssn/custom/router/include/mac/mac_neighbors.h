#ifndef __NEIGHBORTABLE_H__
#define __NEIGHBORTABLE_H__

#include <data_type_def.h>

#define LIVING_TIME                     0x00

#define EMPTY_LONG_ADDR                 0xFFFFFFFFFFFFFFFF

enum
{
    ASSOC_SUCCESS = 0,
    ASSOC_FULL,
    ASSOC_REFUSED,

    ASSOC_STATE_JOINING,    // 正在加入；
    ASSOC_STATE_JOIN_OK,    // 加入成功；

    ASSOC_LIVING,
    ASSOC_LIVING_TIME_OUT,

    ASSOC_RESERVED
};

#pragma pack(1)
typedef struct _neighbor_node
{
    uint16_t    dev_id;
    int8_t      rssi;
    uint8_t     hops;
    uint32_t    time_stamp;
    uint8_t     beacon_ind;
    uint8_t     life_cycle;
    uint8_t     intra_channel;
    uint8_t     inter_channel;
    uint8_t     unassoc_life_cycle;
} neighbor_node_t;

#pragma pack()

/*邻居表的初始化，ID为无效值，生存周期为最大值*/
void mac_neighbor_table_init(void);


/*邻居表的更新*/
void mac_neighbor_table_update(void);


/*邻居节点插入、更新*/
bool_t mac_neighbor_node_add(neighbor_node_t *neighbor_device);
/**
 * [mac_neighbors_node_set_state description]
 * @param  id          [neighbor id]
 * @param  assoc_state [can or not be assoc]
 * @return             [description]
 */
bool_t mac_neighbors_node_set_state(uint16_t id, bool_t assoc_state);

/*邻居节点的删除*/
bool_t mac_neighbor_node_delete(neighbor_node_t *neighbor_device);

/**
 * [mac_get_coord : the func of get coord]
 * @param  coord [the pointer to the coord]
 * @return       [true : get the coord]
 */
bool_t mac_get_coord(neighbor_node_t *coord);

bool_t mac_get_neighbour_by_beacon_index(uint8_t          beacon_index,
                                         neighbor_node_t *neighbour);

bool_t mac_get_neighbour_by_short_addr(uint16_t         short_addr,
                                       neighbor_node_t *neighbour);

uint16_t mac_get_neighbour_beacon_bitmap();

#endif



















