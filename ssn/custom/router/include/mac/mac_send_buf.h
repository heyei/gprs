#ifndef __MAC_DATABUF_H__
#define __MAC_DATABUF_H__

extern list_head_t inter_send_list_head;
extern list_head_t intra_send_list_head;

/// 初始化发送链表
void mac_send_list_init();

/// 把要发送的内容加入簇间发送链表
void mac_inter_send_list_add(sbuf_t *sbuf,
                             uint8_t inter_unit_i);

/// 把要发送的内容加入簇内发送链表
void mac_intra_send_list_add(sbuf_t *sbuf);

/// 从簇间发送链表中根据簇间序号和分配的GTS序号取出要发送的内容
bool_t mac_inter_send_list_find(uint8_t  inter_unit_i,
                                uint8_t  gts_i,
                                sbuf_t **p_sbuf);

/// 从簇内发送链表中根据分配的GTS序号取出要发送的内容
bool_t mac_intra_send_list_find(uint8_t  gts_i,
                                sbuf_t **p_sbuf);

/// 从发送链表中删除一项内容
void mac_send_list_remove(sbuf_t *sbuf);

/// 把数据放入对应的发送时隙
void mac_slot_send_request(sbuf_t       *sbuf,
                           send_option_t option,
                           uint8_t       depth);

void mac_intra_assoc_resp_list_add(sbuf_t *sbuf);
void mac_inter_assoc_resp_list_add(sbuf_t *sbuf,
                                   uint8_t inter_unit_i);

#endif
