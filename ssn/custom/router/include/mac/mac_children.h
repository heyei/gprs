#ifndef __MAC_CHILDREN_H
#define __MAC_CHILDREN_H

#define MAC_CHILD_MAX_LIFE      (NODE_NWK_HB_DURATION * 10)
#define MAC_MAX_CHILDREN_NUM    32

typedef struct
{
    mac_node_info_t node; //节点信息
    uint8_t         life; // 生存期
}mac_child_t;

/// 初始化子节点信息
void mac_children_init();

/// 获取所有子节点的Beacon占用位图
uint16_t mac_children_get_beacon_bitmap();

/// 添加子节点
bool_t mac_children_add(const mac_node_info_t *child);

/// 查找子节点
bool_t mac_children_find(const mac_node_addr_t *addr,
                         mac_node_info_t       *child);

/// 更新路由表项的生存期
void mac_children_update_lifecycle();

#endif