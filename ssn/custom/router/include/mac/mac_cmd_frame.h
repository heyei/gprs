#ifndef __MAC_CMD_FRAME_H
#define __MAC_CMD_FRAME_H

#define SECU_FALSE                      (0)
#define SECU_TRUE                       (1)

#define MAX_ASSOC_PENDING_NUM           (4)

/**
 * MAC ASSOCIATE RESULTs
 */
#define ASSOC_STATUS_SUCCESS            0U
#define ASSOC_STATUS_FULL               1U
#define ASSOC_STATUS_REFUSE             2U
#define ASSOC_STATUS_RESERVED           3U

/**
 * MAC CMD FRAME IDs
 */
#define MAC_CMD_ASSOC_REQ               1U
#define MAC_CMD_ASSOC_RESP              2U
#define MAC_CMD_RESERVED                3U

#pragma pack(1)

typedef struct
{
    uint16_t device_type    : 1,
             reserved       : 1,
             sec_cap        : 1,
             beacon_bitmap  : 13;
    uint32_t assoc_apply_time;
} mac_assoc_req_arg_t;

typedef struct
{
    uint8_t  status     : 4,
             index      : 4;
} mac_assoc_res_arg_t;

#pragma pack()


void mac_cmd_frame_parse(pbuf_t *pbuf);

void mac_make_assoc_request(neighbor_node_t *infor);

#endif