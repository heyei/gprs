#ifndef __MAC_SCHEDULE_H__
#define __MAC_SCHEDULE_H__

#define RATE_2MBPS          1
#define RATE_1MBPS          2
#define RATE_500KBPS        3
#define RATE_250KBPS        4
#define RATE_125KBPS        5

#define MAC_BASE_SLOT_DURATION          (2.5)    // ms


#if(PHY_RATE == RATE_250KBPS)

#define MACGTSDURATION                  14787
#define MACDOWNSTREAMDURATION           2976
#define MACGTSNUM                       10

#endif


typedef struct
{
    uint16_t boud_rate;
    uint8_t  gtsnum;
    uint16_t downstreamduration;
    uint16_t gtsduration;
} frame_length_config_t;

typedef enum
{
    GLOBLE_SUPER_FRAME   = 0,
    SUPER_FRAME_SLOT     = 10,
    SLEEP_SLOT           = 11,
    BEACON_SLOT          = 20,
    SUB_GTS_SLOT         = 21,
    INVAILD_SLOT         = 0xff,
} supframe_t;

typedef enum
{
    ONE_SECOND       = 1,
    TWO_SECONDS      = 2,
    FIVER_SECONDS    = 5,
    TEN_SECONDS      = 10,
    ONE_MINTER       = 60,
    TEN_MINTERS      = 600,
    INVAILD_INTERNAL,
} gl_schedule_internal_t;

void mac_schedule_init(void);

/**
 * [mac_super_frame_cfg : set superframe config]
 */
void mac_super_frame_cfg(void);

/**
 * [mac_get_global_superfrm_time get the mutil-superframe time]
 * @return  [the ticks of multi-superfrm]
 */
uint32_t mac_get_global_superfrm_time(void);
#endif
