#pragma once
#include <lib.h>

#define BEACON_AGE_ARRAY_NUM                16u

typedef struct __dev_assoc_t
{
    uint8_t device_type;
    uint64_t long_addr;
    uint16_t short_addr;
    uint8_t assoc_device_intra_channel;
    uint8_t assoc_device_inter_channel;
}dev_assoc_t;

/***
***功能：beacon_age_map成员的初始化
***参数： 无    
***返回： 无
***/
void beacon_aging_init(void);

/***
***功能：beacon_age_map表的更新
***参数： 无    
***返回： 无
***/
void bcn_aging_update(void);

/***
***功能：alloc_bcn_bitmap表的更新,用来查找当前分配出去的时隙
***参数： 无    
***返回： 无
***/
void bcn_alloc_aging_map(void);

/***
***功能：bcn_alloc_map表成员生存周期的设定
***参数： index时隙编号    
***返回： 无
***/
void bcn_aging_set_map(uint8_t index);

/***
***功能：assoc_table表的初始化
***参数： 无    
***返回： 无
***/
void assoc_table_init(void);

/***
***功能：assoc_table表成员添加
***参数： 无    
***返回： 无
***/
bool_t assoc_table_device_add(uint64_t dev_addr, uint8_t dev_type);

/***
***功能：assoc_table表成员删除
***参数： 无    
***返回： 无
***/
bool_t assoc_table_device_del(uint64_t dev_addr);

/***
***功能：根据设备ID查找assoc_table表的设备类型
***参数： 无    
***返回： 无
***/
uint8_t  assoc_table_device_type_find(uint64_t dev_addr);

/***
***功能：assoc_table表成员添加短地址
***参数： 无    
***返回： 无
***/
void assoc_table_device_add_short_addr(uint64_t dev_addr, uint16_t short_addr);
