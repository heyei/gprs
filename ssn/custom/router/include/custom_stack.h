/**
 * @defgroup STACK STACK - SenStack WSN Protocol Stack
 *
 * @file stack.h
 *
 * This is the main include file of STACK module, one should
 * include this file if intend to use STACK functions.
 */
#ifndef __CUSTOM_STACK_H
#define __CUSTOM_STACK_H

#include <mac.h>
#include <mac_module.h>
#include <mac_frames.h>
#include <mac_beacon.h>
#include <mac_cmd_frame.h>
#include <mac_global.h>
#include <mac_neighbors.h>
#include <mac_schedule.h>
#include <mac_prim.h>

#include <nwk.h>
#include <nwk_frame.h>
#include <nwk_prim.h>
#include <route_table.h>

#include <app.h>
#include <app_frames.h>
#include <app_handles.h>
#include <app_prim.h>

#endif
