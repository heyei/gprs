#include <data_type_def.h>
#include <sbuf.h>
#include <mac_stack.h>
#include <mac_children.h>

static mac_child_t mac_children[MAC_MAX_CHILDREN_NUM];

/// 初始化子节点信息
void mac_children_init()
{
    for (uint8_t i = 0; i < MAC_MAX_CHILDREN_NUM; i++)
    {
        mac_children[i].life = 0;
        mac_children[i].node.short_addr.mode = MAC_NO_ADDR;
        mac_children[i].node.long_addr.mode = MAC_NO_ADDR;
        mac_children[i].node.beacon_index = 0;
    }
}

/// 获取所有子节点的Beacon占用位图
uint16_t mac_children_get_beacon_bitmap()
{
    uint16_t beacon_bitmap = 0;
    
    for (uint8_t i = 0; i < MAC_MAX_CHILDREN_NUM; i++)
    {
        if ((0 != mac_children[i].life) && (ROUTER_TYPE == mac_children[i].node.type))
        {
            DBG_ASSERT(mac_children[i].node.beacon_index >= 1 __DBG_LINE);
            beacon_bitmap |= 1 << (mac_children[i].node.beacon_index - 1);
        }
    }
    
    return beacon_bitmap;
}

/// 添加子节点
bool_t mac_children_add(const mac_node_info_t *child)
{
    uint8_t invalid = MAC_MAX_CHILDREN_NUM;
    
    for (uint8_t i = 0; i < MAC_MAX_CHILDREN_NUM; i++)
    {
        if (0 == mac_children[i].life)
        {
            if (MAC_MAX_CHILDREN_NUM == invalid)
            {
                invalid = i;
            }
        }
        else if(   (TRUE == mac_is_node_addr_equal(&child->short_addr, &mac_children[i].node.short_addr))
                || (TRUE == mac_is_node_addr_equal(&child->long_addr, &mac_children[i].node.long_addr)))
        {
            mac_children[i].node = *child;
            mac_children[i].life = MAC_CHILD_MAX_LIFE;
            return TRUE;
        }
    }
    if (MAC_MAX_CHILDREN_NUM != invalid)
    {
        mac_children[invalid].node = *child;
        mac_children[invalid].life = MAC_CHILD_MAX_LIFE;
        return TRUE;
    }
    return FALSE;
}

/// 查找子节点
bool_t mac_children_find(const mac_node_addr_t *addr,
                         mac_node_info_t       *child)
{
    for (uint8_t i = 0; i < MAC_MAX_CHILDREN_NUM; i++)
    {
        if (0 == mac_children[i].life)
        {
            continue;
        }
        if(   (TRUE == mac_is_node_addr_equal(addr, &mac_children[i].node.short_addr))
           || (TRUE == mac_is_node_addr_equal(addr, &mac_children[i].node.long_addr)))
        {
            *child = mac_children[i].node;
            return TRUE;
        }
    }
    return FALSE;
}

/// 更新路由表项的生存期
void mac_children_update_lifecycle()
{
    for (uint8_t i = 0; i < MAC_MAX_CHILDREN_NUM; i++)
    {
        if (0 != mac_children[i].life)
        {
            mac_children[i].life--;
        }
    }
}

