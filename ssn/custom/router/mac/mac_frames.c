#include <data_type_def.h>
#include <sbuf.h>
#include <phy_packet.h>
#include <m_tran.h>
#include <m_slot.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_schedule.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>

mac_frm_head_info_t mac_frm_head_info;

/*****************************************************************************/
static pbuf_t *mac_frame_get(void)
{
    pbuf_t *frame = NULL;
    frame = phy_get_packet();
    
//    hal_wdt_clear(130000);

    return frame;
}


static bool_t mac_addr_filter(uint8_t frm_type, void *dst_addr, uint8_t addr_mode)
{
    uint64_t dst_long_addr;
    uint16_t dst_short_addr;


    if ((frm_type != MAC_FRAME_TYPE_BEACON) && (frm_type != MAC_FRAME_TYPE_ACK))
    {
        if (addr_mode == ADDR_MODE_SHORT)
        {
            osel_memcpy(&dst_short_addr, dst_addr, MAC_ADDR_SHORT_SIZE);

            if ((dst_short_addr != mac_pib.mac_short_addr)
                    && (dst_short_addr != MAC_BROADCAST_ADDR))
            {
                return FALSE;
            }
        }

        if (addr_mode == ADDR_MODE_LONG)
        {
            osel_memcpy(&dst_long_addr, dst_addr, MAC_ADDR_LONG_SIZE);
            if (dst_long_addr != mac_pib.mac_long_addr)
            {
                return FALSE;
            }
        }
    }

    return TRUE;
}


static bool_t mac_frame_head_info_parse(pbuf_t *pbuf)
{
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;

    osel_memcpy(&(mac_frm_head_info.frm_ctrl), pbuf->data_p, MAC_HEAD_CTRL_SIZE);
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    mac_frm_head_info.mac_seq = *pbuf->data_p;
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;

    if (mac_frm_head_info.frm_ctrl.des_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy((uint8_t *)&mac_frm_head_info.addr_info.dst_addr,
                    pbuf->data_p, MAC_ADDR_LONG_SIZE);
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
    }
    else if (mac_frm_head_info.frm_ctrl.des_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy((uint8_t *)&mac_frm_head_info.addr_info.dst_addr,
                    pbuf->data_p, MAC_ADDR_SHORT_SIZE);
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    }

    if (mac_frm_head_info.frm_ctrl.src_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy((uint8_t *)&mac_frm_head_info.addr_info.src_addr,
                    pbuf->data_p, MAC_ADDR_LONG_SIZE);
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
    }
    else if (mac_frm_head_info.frm_ctrl.src_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy((uint8_t *)&mac_frm_head_info.addr_info.src_addr,
                    pbuf->data_p, MAC_ADDR_SHORT_SIZE);
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    }
    pbuf->attri.src_id = (uint16_t)mac_frm_head_info.addr_info.src_addr;

    if (!mac_addr_filter(mac_frm_head_info.frm_ctrl.frm_type,
                         (void *) & (mac_frm_head_info.addr_info.dst_addr),
                         mac_frm_head_info.frm_ctrl.des_addr_mode))
    {
        return FALSE;
    }
    // mac head size
    mac_frm_head_info.mhr_size = (pbuf->data_p - pbuf->head) - PHY_HEAD_SIZE;
    // fill pbuf attri
    pbuf->attri.need_ack    = mac_frm_head_info.frm_ctrl.ack_req;
    pbuf->attri.seq         = mac_frm_head_info.mac_seq;
    pbuf->attri.src_id      = mac_frm_head_info.addr_info.src_addr;
    if (mac_frm_head_info.frm_ctrl.frm_type == MAC_FRAME_TYPE_ACK)
    {
        pbuf->attri.is_ack = TRUE;
    }
    else
    {
        pbuf->attri.is_ack = FALSE;
    }

    pbuf->data_p = pbuf->head + pbuf->data_len - PHY_FCS_SIZE;
    return TRUE;
}

static void mac_data_frame_parse(pbuf_t *pbuf)
{
    if (mac_state.ss != SUB_STATE_ASSOCIATION_OK)
    {
        return;
    }
    
    mac_data_indication_t mac_data;

    if (ADDR_MODE_SHORT == mac_frm_head_info.frm_ctrl.des_addr_mode)
    {
        mac_data.dest_addr.mode = MAC_SHORT_ADDR;
        mac_data.dest_addr.short_addr = mac_frm_head_info.addr_info.dst_addr;
    }
    else
    {
        mac_data.dest_addr.mode = MAC_LONG_ADDR;
        osel_memcpy(mac_data.dest_addr.long_addr, 
                   &mac_frm_head_info.addr_info.dst_addr, 8);
    }
    
    if (ADDR_MODE_SHORT == mac_frm_head_info.frm_ctrl.src_addr_mode)
    {
        mac_data.src_addr.mode = MAC_SHORT_ADDR;
        mac_data.src_addr.short_addr = mac_frm_head_info.addr_info.src_addr;
    }
    else
    {
        mac_data.src_addr.mode = MAC_LONG_ADDR;
        osel_memcpy(mac_data.src_addr.long_addr, 
                   &mac_frm_head_info.addr_info.src_addr, 8);
    }
    
    mac_data.payload_len = pbuf->data_len - PHY_HEAD_SIZE - PHY_FCS_SIZE
                           - mac_frm_head_info.mhr_size - MAC_FCS_SIZE;
    mac_data.payload = pbuf->data_p;
    mac_data.rssi = pbuf->attri.rssi_dbm;
    if (   NULL != mac_pib.mac_dependent
        && NULL != mac_pib.mac_dependent->mac_data_indicate)
    {
        mac_pib.mac_dependent->mac_data_indicate(&mac_data);
    }
    
    // 为子节点恢复满额的生存期
    mac_node_info_t child_info;
    if (TRUE == mac_children_find(&mac_data.src_addr, &child_info))
    {
        mac_children_add(&child_info);
    }
}


static bool_t mac_frame_parse(pbuf_t *pbuf)
{
    if (NULL == pbuf)
    {
        return FALSE;
    }

    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE + mac_frm_head_info.mhr_size;
    // when slot does not work, only can recv bacon, other frames will free.
    if ( (!m_slot_get_state())
            && (mac_frm_head_info.frm_ctrl.frm_type != MAC_FRAME_TYPE_BEACON))
    {
        pbuf_free(&pbuf __PLINE2);
        m_tran_recv();
        return FALSE;
    }

    switch (mac_frm_head_info.frm_ctrl.frm_type)
    {
    case MAC_FRAME_TYPE_BEACON:
        mac_recv_beacon(pbuf);
        m_tran_recv();
        pbuf_free(&pbuf __PLINE2);
        break;

    case MAC_FRAME_TYPE_DATA:
        mac_data_frame_parse(pbuf);
        pbuf_free(&pbuf __PLINE2);
        break;

    case MAC_FRAME_TYPE_COMMAND:
        mac_cmd_frame_parse(pbuf);
        pbuf_free(&pbuf __PLINE2);
        break;

    case MAC_FRAME_TYPE_ACK:
        m_tran_recv();
        pbuf_free(&pbuf __PLINE2);
        break;

    default:
        m_tran_recv();
        pbuf_free(&pbuf __PLINE2);
        break;
    }

    //m_tran_recv();
    return TRUE;
}


static void mac_tx_finish_tmp(sbuf_t *sbuf, bool_t result)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    pbuf_free(&(sbuf->primargs.pbuf) __PLINE2 );
    sbuf_free(&sbuf __SLINE2 );
}


static void ack_tx_ok_callback(sbuf_t *sbuf, bool_t res)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
    sbuf_free(&sbuf __SLINE2);
}


static void mac_agreement_fill_ack(pbuf_t *pbuf, uint8_t seqno)
{
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;
    mac_frm_ctrl_t frm_ctrl;
    frm_ctrl.frm_type = MAC_FRAME_TYPE_ACK;
    frm_ctrl.sec_enable = FALSE;
    frm_ctrl.frm_pending = FALSE;
    frm_ctrl.des_addr_mode = 0;
    frm_ctrl.src_addr_mode = ADDR_MODE_SHORT;
    frm_ctrl.reseverd = 0;
    frm_ctrl.ack_req = FALSE;

    osel_memcpy(pbuf->data_p, &frm_ctrl, MAC_HEAD_CTRL_SIZE);
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;
    /* sequence num */
    osel_memcpy(pbuf->data_p, &seqno, MAC_HEAD_SEQ_SIZE);
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;
    
    /* dst id */
    //osel_memcpy(pbuf->data_p, &mac_frm_head_info.addr_info.src_addr, MAC_ADDR_SHORT_SIZE);
    //pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    /* src id */
    //osel_memcpy(pbuf->data_p, &mac_pib.mac_short_addr, MAC_ADDR_SHORT_SIZE);
    //pbuf->data_p += MAC_ADDR_SHORT_SIZE;

    pbuf->data_len = pbuf->data_p - pbuf->head - PHY_HEAD_SIZE;

    pbuf->attri.seq = seqno;
    pbuf->attri.is_ack = TRUE;
    pbuf->attri.need_ack = FALSE;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    if (ADDR_MODE_SHORT == mac_frm_head_info.frm_ctrl.src_addr_mode)
    {
        pbuf->attri.dst_id = mac_frm_head_info.addr_info.src_addr;
    }
    else
    {        
        pbuf->attri.dst_id = MAC_BROADCAST_ADDR;
    }
}


static void mac_send_ack(uint8_t seqno)
{
    if (!m_slot_get_state()) // 失去同步后，接收到需要返回应答的帧，如果发送应答后，在mac_frame_parse里调用m_tran_recv，会导致DBG_ASSERT(rf_current_state != RF_TX_STATE __DBG_LINE);
    {
        return;
    }
    pbuf_t *pbuf = pbuf_alloc(SMALL_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    if (pbuf == NULL)
    {
        return;
    }
    mac_agreement_fill_ack(pbuf, seqno);
    DBG_ASSERT((pbuf->data_p - pbuf->head) <= SMALL_PBUF_BUFFER_SIZE __DBG_LINE);
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    if (sbuf != NULL)
    {
        sbuf->primargs.pbuf = pbuf;

        if (m_tran_can_send())
        {
            m_tran_send(sbuf, ack_tx_ok_callback, 1);
        }
        else
        {
            pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
            sbuf_free(&sbuf __SLINE2);
        }
    }
    else
    {
        pbuf_free(&pbuf __PLINE2);
    }
}

void mac_frames_clr_pend_flag(void)
{
    mac_frm_head_info.frm_ctrl.frm_pending = 0;
}

bool_t mac_frames_get_pend_flag(void)
{
    return mac_frm_head_info.frm_ctrl.frm_pending;
}

void mac_frames_init(void)
{
    tran_cfg_t mac_tran_cb;

    mac_tran_cb.frm_get             = mac_frame_get;
    mac_tran_cb.frm_head_parse      = mac_frame_head_info_parse;
    mac_tran_cb.frm_payload_parse   = mac_frame_parse;
    mac_tran_cb.tx_finish           = mac_tx_finish_tmp;
    mac_tran_cb.send_ack            = mac_send_ack;

    m_tran_cfg(&mac_tran_cb);       // m_tran module call back func init

    osel_memset((uint8_t *)&mac_frm_head_info, 0x00, sizeof(mac_frm_head_info_t));

    mac_beacon_init();
}