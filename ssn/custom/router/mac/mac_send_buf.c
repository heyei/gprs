#include <sbuf.h>
#include <hal.h>
#include <phy_packet.h>
#include <m_sync.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_frames.h>

list_head_t inter_send_list_head;
list_head_t intra_send_list_head;
list_head_t intra_assoc_resp_list;
list_head_t inter_assoc_resp_list;

/// 初始化发送链表
void mac_send_list_init()
{
    list_init(&inter_send_list_head);
    list_init(&intra_send_list_head);
    list_init(&intra_assoc_resp_list);
    list_init(&inter_assoc_resp_list);
    
    return;
}

/// 把要发送的内容加入簇间发送链表
void mac_inter_send_list_add(sbuf_t *sbuf,
                             uint8_t inter_unit_i)
{
    hal_int_state_t s;

    sbuf->slot_seq = 0xFFFF; // 在对应簇间单元的首个时隙进行发送时隙的Slot Aloha选择
    sbuf->inter_unit_seq = inter_unit_i;
    sbuf->is_intra = FALSE;
    
    HAL_ENTER_CRITICAL(s);
    list_add_to_tail(&(sbuf->list), &inter_send_list_head);
    HAL_EXIT_CRITICAL(s);
}

/// 把要发送的内容加入簇内发送链表
void mac_intra_send_list_add(sbuf_t *sbuf)
{
    hal_int_state_t s;

    sbuf->slot_seq = 0xFF; // 在对应簇间单元的首个时隙进行发送时隙的Slot Aloha选择
    sbuf->inter_unit_seq = 0xFF;
    sbuf->is_intra = TRUE;
    
    HAL_ENTER_CRITICAL(s);
    list_add_to_tail(&(sbuf->list), &intra_send_list_head);
    HAL_EXIT_CRITICAL(s);
}

/// 从簇间发送链表中根据簇间序号和分配的GTS序号取出要发送的内容
bool_t mac_inter_send_list_find(uint8_t  inter_unit_i,
                                uint8_t  gts_i,
                                sbuf_t **p_sbuf)
{
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    hal_int_state_t s = 0;
    
    *p_sbuf = NULL;
    
    *p_sbuf = list_entry_get_head(&inter_assoc_resp_list, sbuf_t, list); // 如果有关联应答需要发送，那么此时应该处在收到关联请求的下一个GTS
    if (NULL != *p_sbuf)
    {
        return TRUE;
    }
    
    if (list_empty(&inter_send_list_head))
    {
        return FALSE;
    }
    HAL_ENTER_CRITICAL(s);
    list_entry_for_each_safe( pos1, pos2 , &inter_send_list_head, sbuf_t, list)
    {
        if (   (pos1->inter_unit_seq == inter_unit_i)
            && (pos1->slot_seq == gts_i))
        {
            *p_sbuf = pos1;
//            list_del(&(pos1->list));
            break;
        }
    }
    HAL_EXIT_CRITICAL(s);
    
    return (*p_sbuf != NULL);
}

/// 从簇内发送链表中根据分配的GTS序号取出要发送的内容
bool_t mac_intra_send_list_find(uint8_t  gts_i,
                                sbuf_t **p_sbuf)
{
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    hal_int_state_t s = 0;
    
    *p_sbuf = NULL;
    
    *p_sbuf = list_entry_get_head(&intra_assoc_resp_list, sbuf_t, list); // 如果有关联应答需要发送，那么此时应该处在收到关联请求的下一个GTS
    if (NULL != *p_sbuf)
    {
        return TRUE;
    }
    
    if (list_empty(&intra_send_list_head))
    {
        return FALSE;
    }
    HAL_ENTER_CRITICAL(s);
    list_entry_for_each_safe( pos1, pos2 , &intra_send_list_head, sbuf_t, list)
    {
        if (pos1->slot_seq == gts_i)
        {
            *p_sbuf = pos1;
//            list_del(&(pos1->list));
            break;
        }
    }
    HAL_EXIT_CRITICAL(s);
    
    return (*p_sbuf != NULL);
}

/// 从发送链表中删除一项内容
void mac_send_list_remove(sbuf_t *sbuf)
{
    list_del(&(sbuf->list));
}

/// 把数据放入对应的发送时隙
void mac_slot_send_request(sbuf_t       *sbuf,
                           send_option_t option,
                           uint8_t       depth)
{
    uint8_t inter_unit_i;
    
    sbuf->primargs.pbuf->attri.already_send_times.mac_send_times = 0;
    if (INTER_DURATION_SENDING == option)
    {
        inter_unit_i = mac_pib.supfrm_cfg_arg.inter_unit_num - depth + 1;
        mac_inter_send_list_add(sbuf, inter_unit_i);
    }
    else
    {
        mac_intra_send_list_add(sbuf);
    }
}

void mac_intra_assoc_resp_list_add(sbuf_t *sbuf)
{
    hal_int_state_t s;

    sbuf->slot_seq = 0xFF; // 在对应簇间单元的首个时隙进行发送时隙的Slot Aloha选择
    sbuf->inter_unit_seq = 0xFF;
    sbuf->is_intra = TRUE;
    
    HAL_ENTER_CRITICAL(s);
    list_add_to_tail(&(sbuf->list), &intra_assoc_resp_list);
    HAL_EXIT_CRITICAL(s);
}

void mac_inter_assoc_resp_list_add(sbuf_t *sbuf,
                                   uint8_t inter_unit_i)
{
    hal_int_state_t s;

    sbuf->slot_seq = 0xFFFF; // 在对应簇间单元的首个时隙进行发送时隙的Slot Aloha选择
    sbuf->inter_unit_seq = inter_unit_i;
    sbuf->is_intra = FALSE;
    
    HAL_ENTER_CRITICAL(s);
    list_add_to_tail(&(sbuf->list), &inter_assoc_resp_list);
    HAL_EXIT_CRITICAL(s);
}
