#include <stdlib.h>
#include <data_type_def.h>
#include <osel_arch.h>
#include <sbuf.h>
#include <pbuf.h>
#include <hal.h>
#include <hal_board.h>
#include <phy_packet.h>
#include <phy_state.h>
#include <m_slot.h>
#include <m_tran.h>
#include <mac_stack.h>
#include <mac_beacon.h>
#include <mac_schedule.h>
#include <mac_global.h>
#include <mac_frames.h>
#include <mac_send_buf.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_moniter.h>

typedef void (*func_t)(void *args);

static bool_t is_waiting_response;

static int8_t current_intra_sub_slot_index;
static int8_t current_intra_cluster_gts_slot_index;
static int8_t current_inter_sub_slot_index;
static int8_t current_inter_sub_gts_slot_index;


static slot_cfg_t beacon_interval_slot;
static slot_cfg_t intra_slot;
static slot_cfg_t intra_sub_slot;
static slot_cfg_t intra_beacon_slot;
static slot_cfg_t intra_cluster_slot;
static slot_cfg_t intra_cluster_gts_slot;
static slot_cfg_t inter_slot;
static slot_cfg_t inter_sub_slot[MAX_HOPS];
static slot_cfg_t inter_sub_gts_slot[MAX_HOPS];
static slot_cfg_t sleep_slot;
//
//static uint8_t srandom(uint8_t min, uint8_t max)
//{
//    DBG_ASSERT(max >= min __DBG_LINE);
//    srand(TA0R);
//    return (min + rand() % (max - min + 1));
//}

/*****************************************************************************/
static void slot_node_cfg(slot_cfg_t *node,
                          uint32_t duration,
                          uint8_t repeat_cnt,
                          func_t func,
                          slot_cfg_t *parent,
                          slot_cfg_t *first_child,
                          slot_cfg_t *next_sibling)
{
    node->slot_duration = duration;
    node->slot_repeat_cnt = repeat_cnt;
    node->func = func;
    node->parent = parent;
    node->first_child = first_child;
    node->next_sibling = next_sibling;
    node->slot_start = 0;
    node->slot_repeat_seq = 0;
}

static void cluster_gts_txok_cb(sbuf_t *sbuf, bool_t res)
{
    uint8_t allowed_times = MAX_INTER_SEND_TIMES;
    
    if (sbuf->orig_layer == MAC_LAYER)
    {
        allowed_times = 1;
    }
    else if (sbuf->is_intra)
    {
        allowed_times = MAX_INTRA_SEND_TIMES;
    }
    sbuf->primargs.pbuf->attri.already_send_times.mac_send_times++;
    if (   (sbuf->primargs.pbuf->attri.already_send_times.mac_send_times >= allowed_times)
        || (res))
    {
        if (sbuf->orig_layer != MAC_LAYER)
        {
            mac_data_confirm_t confirm;
            confirm.handle = sbuf->handle;
            confirm.send_times = sbuf->primargs.pbuf->attri.already_send_times.mac_send_times;
            if (res)
            {
                confirm.status = MAC_SUCCESS;
            }
            else
            {
                confirm.status = MAC_NO_ACK;
            }
            mac_pib.mac_dependent->mac_data_confirm(&confirm);
        }
        else
        {
            is_waiting_response = TRUE;
        }
        mac_send_list_remove(sbuf);
        if ((sbuf != NULL) && (sbuf->used))
        {
            if (sbuf->primargs.pbuf->used)
            {
                pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
            }
            sbuf_free(&sbuf __SLINE2);
        }
    }
}

static void beacon_interval_slot_handle(void *seq_p)
{
//    hal_led_toggle(HAL_LED_RED);
}

static void intra_slot_handle(void *seq_p)
{
    current_intra_sub_slot_index = 0; // 有效序号从1开始
    phy_set_channel(mac_pib.supfrm_cfg_arg.intra_channel);
}

static void intra_sub_slot_handle(void *seq_p)
{    
    hal_led_open(HAL_LED_BLUE);
    hal_led_open(HAL_LED_RED);
    current_intra_sub_slot_index++;
    // 为待发送数据分配时隙，设置Beacon帧的Pending
    if (   (mac_pib.beacon_enabled == TRUE)
        && (mac_pib.self_index == current_intra_sub_slot_index))
    {
        hal_led_open(HAL_LED_RED);
        uint8_t gts_i = 1;
        mac_pib.pending_spec.short_addr_nums = 0;
        mac_pib.pending_spec.exter_addr_nums = 0;
        sbuf_t *pos1 = NULL;
        sbuf_t *pos2 = NULL;
        hal_int_state_t s = 0;
        if (!list_empty(&intra_send_list_head))
        {
            HAL_ENTER_CRITICAL(s);
            list_entry_for_each_safe( pos1, pos2 , &intra_send_list_head, sbuf_t, list)
            {
                if (pos1->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_mode == ADDR_MODE_SHORT)
                {
                    pos1->slot_seq = gts_i++;
                    mac_pib.pending_short_addr_list[mac_pib.pending_spec.short_addr_nums] = 
                        pos1->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr;
                    mac_pib.pending_spec.short_addr_nums++;
                    if (   (gts_i > mac_pib.supfrm_cfg_arg.intra_gts_num)
                        || (mac_pib.pending_spec.short_addr_nums >= MAX_PENDING_SHORT_ADDR_NUM))
                    {
                        break;
                    }
                }
            }
            HAL_EXIT_CRITICAL(s);
        }
        
        if (gts_i > mac_pib.supfrm_cfg_arg.intra_gts_num)
        {
            return;
        }
        
        pos1 = NULL;
        pos2 = NULL;
        if (!list_empty(&intra_send_list_head))
        {
            HAL_ENTER_CRITICAL(s);
            list_entry_for_each_safe( pos1, pos2 , &intra_send_list_head, sbuf_t, list)
            {
                if (pos1->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_mode == ADDR_MODE_LONG)
                {
                    pos1->slot_seq = gts_i++;
                    mac_pib.pending_long_addr_list[mac_pib.pending_spec.exter_addr_nums] = 
                        pos1->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr;
                    mac_pib.pending_spec.exter_addr_nums++;
                    if (   (gts_i > mac_pib.supfrm_cfg_arg.intra_gts_num)
                        || (mac_pib.pending_spec.exter_addr_nums >= MAX_PENDING_LONG_ADDR_NUM))
                    {
                        break;
                    }
                }
            }
            HAL_EXIT_CRITICAL(s);
        }
        hal_led_close(HAL_LED_RED);
    }
}

static void intra_beacon_slot_handle(void *seq_p)
{    
    if (   (mac_pib.beacon_enabled == TRUE)
        && (mac_pib.self_index == current_intra_sub_slot_index))
    {
        delay_us(1500);
        mac_send_beacon();
        hal_led_close(HAL_LED_BLUE);
        hal_led_close(HAL_LED_RED);
    }
    else
    {
        if (current_intra_sub_slot_index == 1)
        {
//            hal_led_open(HAL_LED_RED);
        }
        m_tran_stop();
        m_tran_recv();
    }
}

static void intra_cluster_slot_handle(void *seq_p)
{
    current_intra_cluster_gts_slot_index = 0; // 有效序号从1开始
}

static void intra_cluster_gts_slot_handle(void *seq_p)
{
    current_intra_cluster_gts_slot_index++;
    
    if (current_intra_sub_slot_index != mac_pib.self_index)
    {
        // 在最后一个sleep的时隙提前醒过来
        uint8_t former_index_1 = 0xFF;
        uint8_t former_index_2 = 0xFF;
        if (1 == (mac_pib.supfrm_cfg_arg.inter_unit_num - mac_pib.hops + 1))
        { // 如果簇间第一个时隙要工作, 则在最后一个簇内的最后一个GTS提前醒来
            former_index_1 = mac_pib.supfrm_cfg_arg.cluster_num;
        }
        if (mac_pib.self_index > 1)
        {
            former_index_2 = mac_pib.self_index - 1;
        }
        if (   (  (current_intra_sub_slot_index == former_index_1)
               || (current_intra_sub_slot_index == former_index_2))
            && (current_intra_cluster_gts_slot_index == mac_pib.supfrm_cfg_arg.intra_gts_num))
        {
            m_tran_recv();
        }
        else
        {
            m_tran_sleep();
        }
        return;
    }
    sbuf_t *sbuf = NULL;
    if (TRUE == mac_intra_send_list_find(current_intra_cluster_gts_slot_index,
                                         &sbuf))
    {
        delay_ms(1);
        m_tran_send(sbuf, cluster_gts_txok_cb, 1);
    }
    else
    {
        m_tran_recv();
    }
}

static void inter_slot_handle(void *seq_p)
{    
    hal_led_open(HAL_LED_BLUE);
    hal_led_open(HAL_LED_RED);
    current_inter_sub_slot_index = 0; // 有效序号从1开始
    phy_set_channel(mac_pib.supfrm_cfg_arg.inter_channel);
}

/// 获取不重复的随机数，随机数范围1~max，其中max<=255
static uint32_t rand_history[8];
static uint16_t max_num;
static uint16_t left_count; // 剩余的可用的随机数个数
static void exclusive_rand_init(int16_t seed,
                                uint16_t max)
{
    max_num = max;
    left_count = max;
    srand(seed);
    for (uint8_t i = 0; i < 8; i++)
    {
        rand_history[i] = 0;
    }
    if (inter_gts_num_array[current_inter_sub_slot_index - 1] > 20)
    {
        rand_history[0] |= BIT0; // 第一个GTS专门用来随机分配GTS序号
        left_count--;
    }
}

static bool_t exclusive_rand_next(uint16_t *num)
{
    if (left_count == 0)
    {
        return FALSE;
    }
    uint16_t index_in_left = rand() % left_count;
    uint16_t index = 0;
    uint16_t current_index_in_left = 0;
    for (index = 0; index < max_num; index++)
    {
        if (0 == (rand_history[index / 32] & (1 << (index % 32))))
        {
            if (index_in_left == current_index_in_left)
            {
                break;
            }
            current_index_in_left++;
        }
    }
    if (index == max_num)
    {
        *num = 0xFF;
        return FALSE;
    }
    rand_history[index / 32] |= 1 << (index % 32);
    left_count--;
    *num = index + 1;
    
    return TRUE;
}

// 为簇间待发送数据分配时隙
static void alloc_inter_tx_gts_slots(void)
{    
    sbuf_t *pos1 = NULL;
    sbuf_t *pos2 = NULL;
    hal_int_state_t s = 0;
    if (!list_empty(&inter_send_list_head))
    {
//        hal_led_open(HAL_LED_RED);
        ////////////////////////////////////////
//        HAL_ENTER_CRITICAL(s);
//        exclusive_rand_init(TA0R, inter_gts_num_array[current_inter_sub_slot_index - 1]);
//        list_entry_for_each_safe( pos1, pos2 , &inter_send_list_head, sbuf_t, list)
//        {
//            if (pos1->inter_unit_seq == current_inter_sub_slot_index)
//            {
//                if (FALSE == exclusive_rand_next(&pos1->slot_seq))
//                {
//                    break;
//                }
//            }
//        }
//        HAL_EXIT_CRITICAL(s);
        ///////////////////////////////////////
        HAL_ENTER_CRITICAL(s);
        list_entry_for_each_safe( pos1, pos2 , &inter_send_list_head, sbuf_t, list)
        {
            if (pos1->inter_unit_seq == current_inter_sub_slot_index)
            {
                pos1->slot_seq = (rand() % inter_gts_num_array[current_inter_sub_slot_index - 1]) + 1;
            }
        }
        HAL_EXIT_CRITICAL(s);
 //       hal_led_close(HAL_LED_RED);
    }
    return;
}

static void inter_sub_slot_handle(void *seq_p)
{
    current_inter_sub_gts_slot_index = 0; // 有效序号从1开始
    current_inter_sub_slot_index++;
    is_waiting_response = FALSE;
    alloc_inter_tx_gts_slots();
}

static void inter_sub_gts_slot_handle(void *seq_p)
{    
    current_inter_sub_gts_slot_index++;
    sbuf_t *sbuf;
    if (TRUE == mac_inter_send_list_find(current_inter_sub_slot_index,
                                         current_inter_sub_gts_slot_index,
                                         &sbuf))
    {
        delay_ms(1);
        m_tran_send(sbuf, cluster_gts_txok_cb, 1);
    }
    else if (   (mac_pib.supfrm_cfg_arg.inter_unit_num - mac_pib.hops + 1
                    == current_inter_sub_slot_index)
             || (is_waiting_response == TRUE))
    {
        m_tran_recv();
    }
    else
    {
        // 在最后一个sleep的时隙提前醒过来
        uint8_t former_index_1 = 0xFF;
        uint8_t former_index_2 = 0xFF;
        if (1 == mac_pib.self_index)
        { // 如果簇内的第一个单元要工作，则在簇间的最后一个单元的最后一个GTS提前醒来
            former_index_1 = mac_pib.supfrm_cfg_arg.inter_unit_num;
        }
        if (1 < (mac_pib.supfrm_cfg_arg.inter_unit_num - mac_pib.hops + 1))
        {
            former_index_2 = mac_pib.supfrm_cfg_arg.inter_unit_num - mac_pib.hops;
        }
        if (   (  (current_inter_sub_slot_index == former_index_1)
               || (current_inter_sub_slot_index == former_index_2))
            && (current_inter_sub_gts_slot_index == inter_gts_num_array[current_inter_sub_slot_index - 1]))
        {
            m_tran_recv();
        }
        else
        {
            m_tran_sleep();
        }
    }
}

#if PRINT_SYNC_PARA_EN == 0
static void sleep_slot_handle(void *seq_p)
{
    hal_led_close(HAL_LED_BLUE);
    hal_led_close(HAL_LED_RED);
//    alloc_inter_tx_gts_slots();
    m_tran_sleep();
    mac_neighbor_table_update();
    mac_children_update_lifecycle();
    if (mac_lost_net_jodge())
    {
        hal_led_open(HAL_LED_GREEN);
        unassociation_indication_t unassoc;
        unassoc.reverse = 0;
        
        hal_board_reset();
        
        osel_post(M_MAC_MONI_STOP_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        osel_post(M_MAC_MONI_START_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        mac_pib.mac_dependent->mac_unassociation_indicate(&unassoc);
    }
    mac_clr_beacon_map();
}
#else
#include <stdio.h>
extern uint32_t txtime_x;
extern uint32_t rxtime_y;
extern bool_t need_printf;
extern fp32_t k;
extern uint32_t x0;
extern uint32_t y0;
static void sleep_slot_handle(void *seq_p)
{
    m_tran_sleep();
    if (need_printf)
    {
        need_printf = FALSE;
        printf("%lu\t%lu\t%.8E\t%lu\t%lu\r\n", txtime_x, rxtime_y, k, x0, y0);
    }
    hal_led_close(HAL_LED_BLUE);
    hal_led_close(HAL_LED_RED);
    alloc_inter_tx_gts_slots();
    mac_neighbor_table_update();
    mac_children_update_lifecycle();
    if (mac_lost_net_jodge())
    {
        hal_led_open(HAL_LED_GREEN);
        
        hal_board_reset();
        
        osel_post(M_MAC_MONI_STOP_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        osel_post(M_MAC_MONI_START_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        unassociation_indication_t unassoc;
        unassoc.reverse = 0;
        mac_pib.mac_dependent->mac_unassociation_indicate(&unassoc);
    }
    mac_clr_beacon_map();
}
#endif
/******************************************************************************/
void sys_enter_lpm_handler(void *p)
{
    LPM3;
}

void mac_schedule_init(void)
{
    osel_idle_hook(sys_enter_lpm_handler);
}

void mac_super_frame_cfg(void)
{
    uint32_t beacon_interval =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * ((uint32_t)0x01 <<
                   mac_pib.supfrm_cfg_arg.bcn_interval_order)); // 超帧时长
//    uint32_t beacon_duration =
//        MS_TO_TICK(MAC_BASE_SLOT_DURATION * ((uint32_t)0x01 << 
//                   mac_pib.supfrm_cfg_arg.bcn_duration_order)); // 簇内Beacon时长
    uint32_t beacon_duration =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * mac_pib.supfrm_cfg_arg.bcn_duration_order); // 簇内Beacon时长
    uint32_t gts_duration =
        MS_TO_TICK(MAC_BASE_SLOT_DURATION * mac_pib.supfrm_cfg_arg.gts_duration_order); // 单个GTS时长
    uint32_t inter_duration = 0; // 簇间时隙时长
    for (uint8_t i = 0; i < mac_pib.supfrm_cfg_arg.inter_unit_num; i++)
    {
        inter_duration += gts_duration * inter_gts_num_array[i];
    }

    uint32_t sleep_duration =
        beacon_interval - 
            mac_pib.supfrm_cfg_arg.cluster_num * 
                (beacon_duration + gts_duration * mac_pib.supfrm_cfg_arg.intra_gts_num) - 
                    inter_duration;
    
    // 含有子时隙的时隙的长度设为0，之后由m_slot_cfg计算
    // 配置簇内时隙
    slot_node_cfg(&intra_cluster_gts_slot, gts_duration, mac_pib.supfrm_cfg_arg.intra_gts_num,
                  &intra_cluster_gts_slot_handle, 
                  &intra_cluster_slot, NULL, NULL); // Cluster时隙内的GTS时隙
    slot_node_cfg(&intra_cluster_slot, 0, 1,
                  &intra_cluster_slot_handle, 
                  &intra_sub_slot, &intra_cluster_gts_slot, NULL); // 簇内子时隙内的Cluster时隙
    slot_node_cfg(&intra_beacon_slot, beacon_duration, 1, &intra_beacon_slot_handle,
                  &intra_sub_slot, NULL, &intra_cluster_slot); // 簇内子时隙内的Beacon时隙
    slot_node_cfg(&intra_sub_slot, 0, mac_pib.supfrm_cfg_arg.cluster_num, 
                  &intra_sub_slot_handle,
                  &intra_slot, &intra_beacon_slot, NULL); // 簇内子时隙
    slot_node_cfg(&intra_slot, 0, 1,
                  &intra_slot_handle,
                  &beacon_interval_slot, &intra_sub_slot, &inter_slot); // 簇内时隙
    
    // 配置簇间时隙,簇间子时隙和簇间子时隙内的GTS时隙
    uint8_t inter_unit_num = mac_pib.supfrm_cfg_arg.inter_unit_num;
    for (uint8_t i = 0; i < inter_unit_num - 1; i++)
    {
        slot_node_cfg(&inter_sub_gts_slot[i], gts_duration, inter_gts_num_array[i],
                      &inter_sub_gts_slot_handle,
                      &inter_sub_slot[i], NULL, NULL); // 簇间子时隙内的GTS时隙
        slot_node_cfg(&inter_sub_slot[i], 0, 1,
                      &inter_sub_slot_handle,
                      &inter_slot, &inter_sub_gts_slot[i], &inter_sub_slot[i + 1]); // 簇间子时隙
    }
    slot_node_cfg(&inter_sub_gts_slot[inter_unit_num - 1], 
                  gts_duration, inter_gts_num_array[inter_unit_num - 1],
                  &inter_sub_gts_slot_handle,
                  &inter_sub_slot[inter_unit_num - 1], NULL, NULL); // 最后一个簇间子时隙内的GTS时隙
    slot_node_cfg(&inter_sub_slot[inter_unit_num - 1], 0, 1,
                  &inter_sub_slot_handle,
                  &inter_slot, &inter_sub_gts_slot[inter_unit_num - 1], NULL); // 最后一个簇间子时隙    
    slot_node_cfg(&inter_slot, 0, 1,
                  inter_slot_handle,
                  &beacon_interval_slot, &inter_sub_slot[0], &sleep_slot); //簇间时隙
    
    slot_node_cfg(&sleep_slot, sleep_duration, 1,
                  &sleep_slot_handle,
                  &beacon_interval_slot, NULL, NULL); // Sleep时隙
    
    slot_node_cfg(&beacon_interval_slot, 0, 0, 
                  &beacon_interval_slot_handle,
                  NULL, &intra_slot, NULL); // 超帧时隙

    m_slot_cfg(&beacon_interval_slot, SLOT_GLOBAL_TIME);

    if (beacon_interval != beacon_interval_slot.slot_duration)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
    }
}

uint32_t mac_get_global_superfrm_time(void)
{
    return beacon_interval_slot.slot_duration;
}

