#include <assoc_table.h>
#include <mac_global.h>
#include <lib.h>

#define FALSE_ADDR                  0xff
#define DEV_ASSOC_MAX_NUM           30
#define ADDRESS_FALSE               0xff   

dev_assoc_t dev_assoc_table[DEV_ASSOC_MAX_NUM];
static uint8_t alloc_bcn_aging[BEACON_AGE_ARRAY_NUM] = {0};

void bcn_aging_update(void)
{
    for(uint8_t i=1;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		if(alloc_bcn_aging[i] > 0)
		{
			alloc_bcn_aging[i]--;
		}
	}
}

void bcn_alloc_aging_map(void)
{
    for(uint8_t i=1;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		if(alloc_bcn_aging[i] == 0)
		{
			alloc_bcn_bitmap &= ~(((uint16_t)0x01)<<i);
		}
		else
		{
			alloc_bcn_bitmap |= ((uint16_t)0x01)<<i;
		}
	}
}

void bcn_aging_set_map(uint8_t index)
{
    if(mac_pib.supfrm_cfg_arg.cluster_num != 0)
    {
        alloc_bcn_aging[index] = mac_pib.supfrm_cfg_arg.cluster_num;
    }
	else
    {
        alloc_bcn_aging[index] = CLUSTER_FRAME_NUM;
    }
}

bool_t assoc_table_device_add(uint64_t dev_addr, uint8_t dev_type)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].long_addr == 0 || dev_assoc_table[i].long_addr == dev_addr)   //地址没变属性更新了也更新关联表
        {
            dev_assoc_table[i].device_type = dev_type;
            dev_assoc_table[i].long_addr = dev_addr;
            return TRUE;
        }
	}
    return FALSE;
}

bool_t assoc_table_device_del(uint64_t dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].long_addr == dev_addr)
        {
            dev_assoc_table[i].device_type = 0;
            dev_assoc_table[i].long_addr = 0;
            dev_assoc_table[i].assoc_device_inter_channel = 0;
            dev_assoc_table[i].assoc_device_intra_channel = 0;
            return TRUE;
        }
	}
    return FALSE;
}

uint8_t  assoc_table_device_type_find(uint64_t dev_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].long_addr == dev_addr)
        {
            return dev_assoc_table[i].device_type;
        }
	}
    return ADDRESS_FALSE;
}

void beacon_aging_init(void)
{
    for(uint8_t i=0;i<BEACON_AGE_ARRAY_NUM;i++)
	{
		alloc_bcn_aging[i] = 0;
	}
}

void assoc_table_init(void)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		dev_assoc_table[i].device_type = 0;
        dev_assoc_table[i].long_addr = 0;
        dev_assoc_table[i].assoc_device_inter_channel = 0;
        dev_assoc_table[i].assoc_device_intra_channel = 0;
	}
}
            
/***
***功能：assoc_table表成员添加短地址
***参数： 无    
***返回： 无
***/
void assoc_table_device_add_short_addr(uint64_t dev_addr, uint16_t short_addr)
{
    for(uint8_t i=0;i<DEV_ASSOC_MAX_NUM;i++)
	{
		if(dev_assoc_table[i].long_addr == dev_addr)
        {
            dev_assoc_table[i].short_addr = short_addr;
        }
	}
}

