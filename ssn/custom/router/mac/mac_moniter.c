#include <debug.h>
#include <osel_arch.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <phy_packet.h>
#include <phy_state.h>
#include <m_tran.h>
#include <m_slot.h>
#include <m_sync.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_schedule.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>
#include <mac_moniter.h>
#include <mac_module.h>

extern device_info_t device_info;

neighbor_node_t coord;
static uint8_t mac_intra_ch_index = 0;
static uint32_t mac_sync_interval_time = MAC_MONITER_CYCLE_TIME;

static hal_timer_t *moniter_sync_timer = NULL;
static hal_timer_t *moniter_cycle_timer = NULL;
static hal_timer_t *moniter_protect_timer = NULL;

static void mac_moniter_timer_stop(void);


static void moniter_protect_timer_cb(void *p)
{
    moniter_protect_timer = NULL;
    if(mac_pib.beacon_enabled == FALSE)
    {
        hal_board_reset();
    }
}

static void mac_moniter_protect_start(void)
{
    if (NULL == moniter_protect_timer)
    {
        HAL_TIMER_SET_REL(  MS_TO_TICK(600*1000.0),
                            moniter_protect_timer_cb,
                            NULL,
                            moniter_protect_timer);
        DBG_ASSERT(moniter_protect_timer != NULL __DBG_LINE);
    }
}

static void moniter_timer_cb(void *p)
{
    moniter_cycle_timer = NULL;
    osel_post(M_MAC_MONI_START_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}


static void mac_moniter_timer_start(void)
{
    if (NULL == moniter_cycle_timer)
    {
        HAL_TIMER_SET_REL(  MS_TO_TICK(mac_sync_interval_time),
                            moniter_timer_cb,
                            NULL,
                            moniter_cycle_timer);
        DBG_ASSERT(moniter_cycle_timer != NULL __DBG_LINE);
    }
}

static bool_t mac_moniter_is_started(void)
{
    if(NULL != moniter_cycle_timer)
    {
        return TRUE;
    }
    return FALSE;
}

void mac_moniter_timer_stop(void)
{
    if (NULL != moniter_cycle_timer)
    {
        hal_timer_cancel(&moniter_cycle_timer);
    }
}

static void mac_moniter_sync_cfg(void)
{
    hal_time_t slot_run_time;
    if (!m_slot_get_state())
    {
        slot_run_time.w = coord.time_stamp;
        m_slot_run((hal_time_t *) &slot_run_time);
        mac_state.ms = MAIN_STATE_SCHEDULING;
    }

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    sbuf->primtype = M2M_ASSOC_REQUETS;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void moniter_sync_timer_cb(void *p)
{
    moniter_sync_timer = NULL;
    osel_post(M_MAC_MONI_SYNC_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}



static void find_sys_coord(void)
{
    if (!mac_get_coord(&coord))
    {
//        mac_neighbor_table_init();
        mac_state.ms = MAIN_STATE_IDLE;
        return;
    }
    mac_moniter_timer_stop();

    mac_pib.hops                         = coord.hops + 1;
//    mac_pib.self_index                   = coord.beacon_ind;
    mac_pib.mac_coord_short_addr         = coord.dev_id;
    mac_pib.mac_coord_beacon_index       = coord.beacon_ind;
    mac_pib.supfrm_cfg_arg.intra_channel = coord.intra_channel;
    mac_pib.supfrm_cfg_arg.inter_channel = coord.inter_channel;

    mac_super_frame_cfg();
    mac_sync_config(coord.dev_id);
    phy_set_channel(coord.intra_channel);

    if (NULL == moniter_sync_timer)
    {
        HAL_TIMER_SET_REL(  mac_get_global_superfrm_time() * 4 ,
                            moniter_sync_timer_cb,
                            NULL,
                            moniter_sync_timer);
        DBG_ASSERT(moniter_sync_timer != NULL __DBG_LINE);
    }
}

void mac_moniter_start(void)
{
    if(mac_moniter_is_started())
    {
        return;
    }
    mac_moniter_timer_start();
    phy_set_channel(device_info.intra_ch[mac_intra_ch_index++]);
    phy_set_power(device_info.power_sn);
    if (mac_intra_ch_index >= device_info.intra_ch_cnt)
    {
        mac_intra_ch_index = 0;
        find_sys_coord();
    }

    m_tran_recv();
}

void mac_moniter_stop(void)
{
    mac_moniter_timer_stop();
    m_slot_stop();
    m_sync_enable(FALSE);
    
    mac_global_reset();
}

static void m_mac_moniter_handler(const osel_event_t *const pmsg)
{
    DBG_ASSERT(pmsg != NULL __DBG_LINE);
    switch (pmsg->sig)
    {
    case M_MAC_MONI_START_EVENT:
        mac_moniter_start();
        break;

    case M_MAC_MONI_STOP_EVENT:
        mac_moniter_stop();
        break;

    case M_MAC_MONI_SYNC_EVENT:
        mac_moniter_sync_cfg();
        break;
    default:
        break;
    }
}

void mac_moniter_init(void)
{
    mac_intra_ch_index = 0;

    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_START_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_STOP_EVENT);
    module_bind_event(m_mac_moniter_handler, M_MAC_MONI_SYNC_EVENT);
    mac_moniter_start();
    hal_led_open(HAL_LED_RED);
    
    mac_moniter_protect_start();
}
