#include <data_type_def.h>
#include <pbuf.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <phy_packet.h>
#include <m_tran.h>
#include <m_sync.h>
#include <m_slot.h>
#include <mac_stack.h>
#include <mac_schedule.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_global.h>
#include <mac_cmd_frame.h>
#include <mac_frames.h>

static bool_t pend_me_flag       = FALSE;
static uint8_t  lost_corrd_cnt   = 0;
static uint16_t local_beacon_map = 0x0000;
static uint16_t last_beacon_map  = 0x0000;
static superframe_spec_t superfrm_cfg_arg;

uint8_t inter_gts_num_array[MAX_HOPS];

static void mac_get_superframe_cfg_arg(pbuf_t *pbuf)
{
    osel_memcpy((uint8_t *)&superfrm_cfg_arg, pbuf->data_p, sizeof(superframe_spec_t));
    pbuf->data_p += sizeof(superframe_spec_t);

    if (mac_state.ms != MAIN_STATE_SCHEDULING)
    {
        mac_pib.supfrm_cfg_arg.bcn_interval_order = superfrm_cfg_arg.bcn_interval_order;
        mac_pib.supfrm_cfg_arg.bcn_duration_order = superfrm_cfg_arg.bcn_duration_order;

        mac_pib.supfrm_cfg_arg.gts_duration_order = superfrm_cfg_arg.gts_duration_order;

        mac_pib.supfrm_cfg_arg.cluster_num        = superfrm_cfg_arg.cluster_num;
        mac_pib.supfrm_cfg_arg.intra_gts_num      = superfrm_cfg_arg.intra_gts_num;

        mac_pib.supfrm_cfg_arg.inter_unit_num     = superfrm_cfg_arg.inter_unit_num;
    }
    else
    {
        mac_pib.supfrm_cfg_arg.downlink_slots_len = superfrm_cfg_arg.downlink_slots_len;
    }

    osel_memcpy(&inter_gts_num_array, pbuf->data_p, superfrm_cfg_arg.inter_unit_num);
    pbuf->data_p += superfrm_cfg_arg.inter_unit_num;
}

/**
 * [mac_get_pend_filter : mac filter beacon frame, judge get downlink flag]
 *
 * @param pbuf [description]
 * @return [none]
 */
static void mac_get_pend_filter(pbuf_t *pbuf)
{
    uint16_t short_addr = EMPTY_ADDR;
    uint64_t long_addr = (uint64_t)EMPTY_ADDR;
    pend_addr_spec_t pend_add_arg;

    if (!mac_frames_get_pend_flag())
    {
        return;
    }

    osel_memcpy((uint8_t *)&pend_add_arg, pbuf->data_p, sizeof(pend_addr_spec_t));
    pbuf->data_p += sizeof(pend_addr_spec_t);

    pend_me_flag = FALSE;
    for (uint8_t i = 0; i < pend_add_arg.short_addr_nums; i++)
    {
        osel_memcpy((uint8_t *)&short_addr, pbuf->data_p, sizeof(uint16_t));
        pbuf->data_p += MAC_ADDR_SHORT_SIZE;
        if (short_addr == mac_pib.mac_short_addr)
        {
            pend_me_flag = TRUE;
        }
    }

    for (uint8_t i = 0; i < pend_add_arg.exter_addr_nums; i++)
    {
        osel_memcpy((uint8_t *)&long_addr, pbuf->data_p, sizeof(uint64_t));
        pbuf->data_p += MAC_ADDR_LONG_SIZE;
        if (long_addr == mac_pib.mac_long_addr)
        {
            pend_me_flag = TRUE;
        }
    }
}


static void mac_get_superframe_payload(pbuf_t *pbuf)
{
    bcn_payload_t *bcn_pld = (bcn_payload_t *)(pbuf->data_p);
    neighbor_node_t neighbor_node;

    local_beacon_map |= ((uint16_t)0x01 << (bcn_pld->index));

    neighbor_node.dev_id     = pbuf->attri.src_id;
    neighbor_node.rssi       = pbuf->attri.rssi_dbm;
    neighbor_node.hops       = bcn_pld->hops;
    neighbor_node.time_stamp = bcn_pld->time_stamp;
    neighbor_node.beacon_ind = bcn_pld->index;
    neighbor_node.life_cycle = LIVING_TIME;
    neighbor_node.intra_channel = superfrm_cfg_arg.intra_channel;
    neighbor_node.inter_channel = superfrm_cfg_arg.inter_channel;
    mac_neighbor_node_add(&neighbor_node);
}

void mac_clr_pend_me_flag(void)
{
    pend_me_flag = FALSE;
}

bool_t mac_get_pend_me_flag(void)
{
    return pend_me_flag;
}

uint16_t mac_get_beacon_map(void)
{
    return last_beacon_map;
}

bool_t mac_clr_beacon_map(void)
{
    last_beacon_map = local_beacon_map;
    local_beacon_map = 0;
    return TRUE;
}

/* pbuf->data_p has pointer to MAC Payload */
void mac_recv_beacon(pbuf_t *pbuf)
{
    hal_led_toggle(HAL_LED_BLUE);
    hal_led_toggle(HAL_LED_RED);
//    hal_led_close(HAL_LED_RED);
    mac_get_superframe_cfg_arg(pbuf);
    mac_get_pend_filter(pbuf);
    mac_get_superframe_payload(pbuf);
}

void mac_sync_config(uint16_t dst_addr)
{
    sync_cfg_t cfg;

    cfg.background_compute = FALSE;
    cfg.sync_source = FALSE;
    cfg.sync_target = dst_addr;

    cfg.flag_byte_pos = 0x03;
    cfg.flag_byte_msk = 0x07;
    cfg.flag_byte_val = MAC_FRAME_TYPE_BEACON;

    cfg.len_pos = 0;
    cfg.len_modfiy = TRUE;

    cfg.stamp_len = 4;
    cfg.stamp_byte_pos = 0;

    cfg.tx_sfd_cap = FALSE;
    cfg.rx_sfd_cap = TRUE;

    cfg.tx_offset = 67;
    cfg.rx_offset = 0;

    m_sync_cfg(&cfg);
}

bool_t mac_lost_net_jodge(void)
{
    if (!(local_beacon_map & (0x01 << mac_pib.mac_coord_beacon_index)))
    {
        if (++lost_corrd_cnt >= LOST_CORRD_CNT_MAX)
        {
            lost_corrd_cnt = 0;
            return TRUE;
        }
    }
    else
    {
        lost_corrd_cnt = 0;
    }
    return FALSE;
}


void mac_beacon_init(void)
{
    lost_corrd_cnt = 0;
    mac_sync_config(0xFFFF);
}

static void beacon_tx_done(sbuf_t* sbuf,bool_t result)
{
//    hal_led_close(HAL_LED_RED);
    if ((sbuf != NULL) && (sbuf->used))
    {
        if (sbuf->primargs.pbuf->used)
        {
            pbuf_free(&(sbuf->primargs.pbuf) __PLINE2);
        }
        sbuf_free(&sbuf __SLINE2);
    }
}

/// �����ű�֡
void mac_send_beacon(void)
{
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    if (NULL == pbuf)
    {
        return;
    }
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    if (NULL == sbuf)
    {
        pbuf_free(&pbuf __PLINE2);
        return;
    }

    sbuf->primargs.pbuf = pbuf;
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;

    mac_frm_ctrl_t mac_ctrl;
    mac_ctrl.frm_type      = MAC_FRAME_TYPE_BEACON;
    mac_ctrl.sec_enable    = FALSE;
    mac_ctrl.frm_pending   = mac_pib.pending_spec.short_addr_nums > 0 
                             || mac_pib.pending_spec.exter_addr_nums > 0;
    mac_ctrl.ack_req       = FALSE;
    mac_ctrl.des_addr_mode = ADDR_MODE_NONE;
    mac_ctrl.src_addr_mode = ADDR_MODE_SHORT;
    mac_ctrl.reseverd      = 0x00;
    osel_memcpy(pbuf->data_p, &mac_ctrl, sizeof(mac_frm_ctrl_t));
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;

    *(uint8_t *)pbuf->data_p = mac_pib.beacon_seq;
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;

    osel_memcpy(pbuf->data_p, (void *)&mac_pib.mac_short_addr, MAC_ADDR_LONG_SIZE);
    pbuf->data_p += MAC_ADDR_SHORT_SIZE;

    //mac payload
    
    mac_pib.supfrm_cfg_arg.downlink_slots_len = mac_pib.pending_spec.short_addr_nums
                                                + mac_pib.pending_spec.exter_addr_nums;
    
    osel_memcpy(pbuf->data_p, &mac_pib.supfrm_cfg_arg, sizeof(superframe_spec_t));
    pbuf->data_p += sizeof(superframe_spec_t);
    osel_memcpy(pbuf->data_p, inter_gts_num_array, superfrm_cfg_arg.inter_unit_num);
    pbuf->data_p += superfrm_cfg_arg.inter_unit_num;
    
    if (0 != mac_ctrl.frm_pending)
    {
        osel_memcpy(pbuf->data_p, &mac_pib.pending_spec, sizeof(pend_addr_spec_t));
        pbuf->data_p += sizeof(pend_addr_spec_t);
        for (int8_t i = 0; i < mac_pib.pending_spec.short_addr_nums; i++)
        {
            osel_memcpy(pbuf->data_p, &mac_pib.pending_short_addr_list[i], MAC_ADDR_SHORT_SIZE);
            pbuf->data_p += MAC_ADDR_SHORT_SIZE;
        }
        for (int8_t i = 0; i < mac_pib.pending_spec.exter_addr_nums; i++)
        {
            osel_memcpy(pbuf->data_p, &mac_pib.pending_long_addr_list[i], MAC_ADDR_LONG_SIZE);
            pbuf->data_p += MAC_ADDR_LONG_SIZE;
        }
    }

    bcn_payload_t beacon_payload;
    beacon_payload.index = mac_pib.self_index;
    beacon_payload.hops = mac_pib.hops;
    beacon_payload.time_stamp = m_slot_get_root_begin();
    osel_memcpy(pbuf->data_p, &beacon_payload, sizeof(bcn_payload_t));
    pbuf->data_p += sizeof(bcn_payload_t);
    
    pbuf->attri.seq = mac_pib.beacon_seq++;
    pbuf->attri.need_ack = FALSE;
    pbuf->attri.already_send_times.mac_send_times = 0;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.dst_id = MAC_BROADCAST_ADDR;
    pbuf->data_len = pbuf->data_p - pbuf->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
    DBG_ASSERT((pbuf->data_p - pbuf->head) <= MEDIUM_PBUF_BUFFER_SIZE __DBG_LINE);
    
    sbuf->orig_layer = MAC_LAYER;
//    hal_led_open(HAL_LED_RED);
    m_tran_send(sbuf, beacon_tx_done, 1);
}





