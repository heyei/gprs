#include <stdlib.h>
#include <string.h>
#include <osel_arch.h>
#include <sbuf.h>
#include <hal.h>
#include <phy_packet.h>
#include <m_sync.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_frames.h>
#include <mac_schedule.h>

/// 判断两个MAC地址是否相同
bool_t mac_is_node_addr_equal(const mac_node_addr_t *addr_1,
                              const mac_node_addr_t *addr_2)
{
    if (addr_1->mode != addr_2->mode)
    {
        return FALSE;
    }
    if (addr_1->mode == MAC_SHORT_ADDR)
    {
        return (addr_1->short_addr == addr_2->short_addr);
    }
    else if (addr_1->mode == MAC_LONG_ADDR)
    {
        return (0 == memcmp(addr_1->long_addr, addr_2->long_addr, 8));
    }
    else
    {
        return FALSE;
    }
}

/// 请求MAC与上级关联
void mac_association_request()
{
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    sbuf->primtype = M2M_ASSOC_REQUETS;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

/// 请求MAC发送数据，异步
/// 参数：
///     data        要发送的数据信息
///     handle      此次发送请求的标识
/// 返回：
///     无
void mac_data_request(const mac_data_t     *data,
                      uint8_t               handle)
{
    pbuf_t *pbuf = pbuf_alloc(data->payload_len __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }

    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    if (sbuf == NULL)
    {
        pbuf_free(&pbuf __PLINE2);
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    sbuf->handle = handle;
    sbuf->primargs.pbuf = pbuf;
    n2m_data_request_t *n2m_data_request = &sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg;
    n2m_data_request->dst_mode = data->dest_addr.mode;
    if (MAC_SHORT_ADDR == data->dest_addr.mode)
    {
        n2m_data_request->dst_addr = data->dest_addr.short_addr;
    }
    else
    {
        osel_memcpy(&n2m_data_request->dst_addr, data->dest_addr.long_addr, 8);
    }
    n2m_data_request->src_mode = data->src_addr.mode;
    if (MAC_SHORT_ADDR == data->src_addr.mode)
    {
        n2m_data_request->src_addr = data->src_addr.short_addr;
    }
    else
    {
        osel_memcpy(&n2m_data_request->src_addr, data->src_addr.long_addr, 8);
    }
    osel_memcpy(pbuf->data_p, data->payload, data->payload_len);
    n2m_data_request->msdu_length = data->payload_len;
    n2m_data_request->msdu = pbuf->data_p;
    sbuf->orig_layer = NWK_LAYER;
    sbuf->primtype = N2M_DATA_REQUEST;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

/// 判断是否是下级节点
bool_t mac_is_child(const mac_node_addr_t *child_addr)
{
    mac_node_info_t child;
    
    return mac_children_find(child_addr, &child);
}

/// 查找下级节点
bool_t mac_find_child(const mac_node_addr_t *child_addr,
                      mac_node_info_t       *child_info)
{
    hal_int_state_t s = 0;
    bool_t res;
    
    HAL_ENTER_CRITICAL(s);
    res = mac_children_find(child_addr, child_info);
    HAL_EXIT_CRITICAL(s);
    
    return res;
}

/// 添加下级节点
bool_t mac_add_child(const mac_node_info_t *child)
{
    hal_int_state_t s = 0;
    bool_t res;
    
    HAL_ENTER_CRITICAL(s);
    res = mac_children_add(child);
    HAL_EXIT_CRITICAL(s);
    
    return res;
}

/// 查找邻居节点
bool_t mac_find_neighbour(const mac_node_addr_t *neighbour_addr,
                          mac_node_info_t       *neighbour_info)
{
    hal_int_state_t s = 0;
    neighbor_node_t neighbour;
      
    if (MAC_SHORT_ADDR != neighbour_addr->mode)
    {
        return FALSE;
    }
    
    HAL_ENTER_CRITICAL(s);
    if (TRUE == mac_get_neighbour_by_short_addr(neighbour_addr->short_addr, &neighbour))
    {
        neighbour_info->beacon_index = neighbour.beacon_ind;
        neighbour_info->depth = neighbour.hops;
        neighbour_info->long_addr.mode = MAC_NO_ADDR;
        neighbour_info->short_addr.mode = MAC_SHORT_ADDR;
        neighbour_info->short_addr.short_addr = neighbour.dev_id;
        if (1 == neighbour.hops)
        {
            neighbour_info->type = SINK_TYPE;
        }
        else
        {
            neighbour_info->type = ROUTER_TYPE;
        }
        HAL_EXIT_CRITICAL(s);
        return TRUE;
    }
    HAL_EXIT_CRITICAL(s);
    
    return FALSE;
}

/// 获取上级节点信息
bool_t mac_get_parent(mac_node_info_t *parent)
{
    hal_int_state_t s = 0;
    neighbor_node_t neighbour;
    
    if (mac_state.ss == SUB_STATE_ASSOCIATION_OK)
    {
        HAL_ENTER_CRITICAL(s);
        if (TRUE == mac_get_neighbour_by_short_addr(mac_pib.mac_coord_short_addr,
                                                    &neighbour))
        {
            parent->type = ROUTER_TYPE;
            parent->short_addr.mode = MAC_SHORT_ADDR;
            parent->short_addr.short_addr = neighbour.dev_id;
            parent->depth = neighbour.hops;
            HAL_EXIT_CRITICAL(s);
            return TRUE;
        }
        HAL_EXIT_CRITICAL(s);
    }
    
    return FALSE;
}

/// 获取同步的全局时间
uint32_t mac_get_global_time()
{
    hal_int_state_t s = 0;
    
    HAL_ENTER_CRITICAL(s);
    hal_time_t now = hal_timer_now();
    m_sync_l2g(&now);
    HAL_EXIT_CRITICAL(s);
    
    return now.w;
}

/// 使能MAC的Beacon
/// 参数：
///     short_addr  入网应答中的MAC短地址
void mac_beacon_enable(const mac_node_addr_t *mac_short_addr)
{
    hal_int_state_t s = 0;
    
    HAL_ENTER_CRITICAL(s);
    mac_pib.beacon_enabled = TRUE;
    mac_pib.mac_short_addr = mac_short_addr->short_addr;
    HAL_EXIT_CRITICAL(s);
}

/// 获取MAC长地址
void mac_get_long_addr(mac_node_addr_t *long_addr)
{
    long_addr->mode = MAC_LONG_ADDR;
    osel_memcpy(long_addr->long_addr, &mac_pib.mac_long_addr, 8);
}

/// 获取超帧周期
bool_t mac_get_beacon_interval(uint32_t *interval)
{
    hal_int_state_t s = 0;
    bool_t result;
    
    HAL_ENTER_CRITICAL(s);
    if (mac_state.ss == SUB_STATE_ASSOCIATION_OK)
    {
        *interval = (uint32_t)(MAC_BASE_SLOT_DURATION * ((uint32_t)0x01 <<
                                              mac_pib.supfrm_cfg_arg.bcn_interval_order));
        result = TRUE;
    }
    else
    {
        result = FALSE;
    }
    HAL_EXIT_CRITICAL(s);
    
    return result;
}

/// 初始化MAC栈
void mac_stack_init(const mac_dependent_t *dependent)
{
    mac_pib.mac_dependent = dependent;
}

