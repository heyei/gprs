#include <lib.h>
#include <pbuf.h>
#include <sbuf.h>
#include <hal_timer.h>
#include <phy_packet.h>
#include <m_sync.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>

static void mac_fill_assoc_req_buf(pbuf_t *pbuf, uint16_t dst_addr)
{
    pbuf->data_p = pbuf->head + PHY_HEAD_SIZE;
    
    mac_frm_ctrl_t mac_ctrl;
    mac_ctrl.frm_type      = MAC_FRAME_TYPE_COMMAND;
    mac_ctrl.sec_enable    = FALSE;
    mac_ctrl.frm_pending   = FALSE;
    mac_ctrl.ack_req       = FALSE;
    mac_ctrl.des_addr_mode = ADDR_MODE_SHORT;
    mac_ctrl.src_addr_mode = ADDR_MODE_LONG;
    mac_ctrl.reseverd      = 0x00;
    osel_memcpy(pbuf->data_p, &mac_ctrl, sizeof(mac_frm_ctrl_t));
    pbuf->data_p += MAC_HEAD_CTRL_SIZE;
    
    osel_memcpy(pbuf->data_p, &mac_pib.mac_bsn, sizeof(uint8_t));
    pbuf->data_p += MAC_HEAD_SEQ_SIZE;
    
    osel_memcpy(pbuf->data_p, (uint8_t *)&dst_addr, MAC_ADDR_SHORT_SIZE);
    pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    
    osel_memcpy(pbuf->data_p, (void *)&mac_pib.mac_long_addr, MAC_ADDR_LONG_SIZE);
    pbuf->data_p += MAC_ADDR_LONG_SIZE;
    
    //mac payload
    *(uint8_t *)pbuf->data_p = MAC_CMD_ASSOC_REQ;
    pbuf->data_p += sizeof(uint8_t);
    
    mac_assoc_req_arg_t associ_req_args;
    associ_req_args.device_type = NODE_TYPE_ROUTER;
    associ_req_args.sec_cap = SECU_FALSE;
    associ_req_args.beacon_bitmap = mac_get_neighbour_beacon_bitmap();
    
    hal_time_t now = hal_timer_now();
    m_sync_l2g(&now);
    associ_req_args.assoc_apply_time = now.w;
    osel_memcpy(pbuf->data_p, (uint8_t *)&associ_req_args, sizeof(mac_assoc_req_arg_t));
    pbuf->data_p += sizeof(mac_assoc_req_arg_t);
    
    //pbuf attri
    pbuf->attri.seq = mac_pib.mac_bsn++;
    pbuf->attri.need_ack = mac_ctrl.ack_req;
    pbuf->attri.already_send_times.mac_send_times = 0;
    pbuf->attri.send_mode = TDMA_SEND_MODE;
    pbuf->attri.dst_id = dst_addr;
    pbuf->data_len = pbuf->data_p - pbuf->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
}

void mac_make_assoc_request(neighbor_node_t *infor)
{
    pbuf_t *pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    if (NULL == pbuf)
    {
        return;
    }
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    if (NULL == sbuf)
    {
        pbuf_free(&pbuf __PLINE2);
        return;
    }
    
    mac_fill_assoc_req_buf(pbuf, infor->dev_id);
    DBG_ASSERT((pbuf->data_p - pbuf->head) <= MEDIUM_PBUF_BUFFER_SIZE __DBG_LINE);
    
    sbuf->primargs.pbuf = pbuf;
    sbuf->orig_layer = MAC_LAYER;
    sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_mode = ADDR_MODE_SHORT;
    sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr = infor->dev_id;
    
    mac_inter_send_list_add(sbuf, mac_pib.supfrm_cfg_arg.inter_unit_num - infor->hops + 1);
}

static void mac_assoc_resp_frame_parse(pbuf_t *pbuf)
{
    mac_assoc_res_arg_t mac_assoc_res_arg;
    osel_memcpy((uint8_t *)&mac_assoc_res_arg,
                pbuf->data_p,
                sizeof(mac_assoc_res_arg_t));
    pbuf->data_p += sizeof(mac_assoc_res_arg_t);
    
    if (mac_pib.mac_coord_short_addr != pbuf->attri.src_id)
    { // 在关联请求时已经设置了上级信息
        return;
    }
    
    if (mac_assoc_res_arg.status != ASSOC_STATUS_SUCCESS)
    {
        mac_neighbors_node_set_state(pbuf->attri.src_id, FALSE);
        mac_state.ss = SUB_STATE_IDLE;
        return;
    }
    
    mac_neighbors_node_set_state(pbuf->attri.src_id, TRUE); // 在发送关联请求前，已设置成FALSE，
                                                            // 如果收不到关联应答，就相当于这个邻居不可关联    
    mac_pib.self_index = mac_assoc_res_arg.index;
    mac_state.ss = SUB_STATE_ASSOCIATION_OK;
    
    if (   NULL != mac_pib.mac_dependent
        && NULL != mac_pib.mac_dependent->mac_association_confirm)
    {
        association_confirm_t confirm;
        confirm.parent_addr.mode = MAC_SHORT_ADDR;
        confirm.parent_addr.short_addr = mac_pib.mac_coord_short_addr;
        confirm.parent_depth = mac_pib.hops - 1;
        mac_pib.mac_dependent->mac_association_confirm(&confirm);
    }
}

static void mac_assoc_request_frame_parse(pbuf_t *pbuf)
{
    mac_assoc_req_arg_t assoc_req;
    mac_assoc_res_arg_t assoc_resp;
    mac_node_info_t child;
    
    if (ADDR_MODE_LONG != mac_frm_head_info.frm_ctrl.src_addr_mode)
    {
        return;
    }
    // 准备关联响应的数据
    osel_memcpy(&assoc_req, pbuf->data_p, sizeof(mac_assoc_req_arg_t));
	pbuf->data_p += sizeof(mac_assoc_req_arg_t);
    assoc_resp.status = ASSOC_STATUS_SUCCESS;
    mac_node_addr_t child_addr;
    child_addr.mode = MAC_LONG_ADDR;
    osel_memcpy(child_addr.long_addr, &mac_frm_head_info.addr_info.src_addr, 8);
    if (TRUE == mac_children_find(&child_addr, &child))  // 是已关联的子节点
    {
        assoc_resp.index = child.beacon_index;
    }
    else
    {
        if (assoc_req.device_type == NODE_TYPE_ROUTER)
        {
            if (mac_pib.hops == mac_pib.supfrm_cfg_arg.inter_unit_num)  // 如果自己是最后一跳，不允许ROUTER关联
            {
                assoc_resp.status = ASSOC_STATUS_REFUSE;
            }
            else
            {
                child.type = ROUTER_TYPE;
                uint16_t beacon_bitmap = 0;
                beacon_bitmap |= mac_get_neighbour_beacon_bitmap();
                beacon_bitmap |= mac_children_get_beacon_bitmap();
                beacon_bitmap |= (1 << (mac_pib.self_index - 1));
                uint8_t i;
                for (i = 0; i < mac_pib.supfrm_cfg_arg.cluster_num; i++)
                {
                    if (0 == (beacon_bitmap & (1 << i)))
                    {
                        assoc_resp.index = i + 1;
                        child.beacon_index = assoc_resp.index;
                        break;
                    }
                }
                if (i == mac_pib.supfrm_cfg_arg.cluster_num)
                {
                    assoc_resp.status = ASSOC_STATUS_REFUSE;
                }
            }
        }
        else if (assoc_req.device_type == NODE_TYPE_TAG)
        {
            child.type = END_NODE_TYPE;
            child.beacon_index = mac_pib.self_index;
            assoc_resp.index = mac_pib.self_index;
        }
        else
        {
            return;
        }
        
        if (ASSOC_STATUS_SUCCESS == assoc_resp.status)
        {
            child.depth = mac_pib.hops + 1;
            child.short_addr.mode = MAC_NO_ADDR;
            child.long_addr.mode = MAC_LONG_ADDR;
            osel_memcpy(child.long_addr.long_addr, &mac_frm_head_info.addr_info.src_addr, MAC_ADDR_LONG_SIZE);
            if (FALSE == mac_children_add(&child))
            {
                assoc_resp.status = ASSOC_STATUS_FULL;
            }
        }
    }
    
    // 发送关联响应
    pbuf_t *new_pbuf = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    DBG_ASSERT(NULL != new_pbuf __DBG_LINE);
    if (NULL == new_pbuf)
    {
        return;
    }
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(NULL != sbuf __DBG_LINE);
    if (NULL == sbuf)
    {
        pbuf_free(&new_pbuf __PLINE1);
        return;
    }
    sbuf->primargs.pbuf = new_pbuf;
    new_pbuf->data_p = new_pbuf->head + PHY_HEAD_SIZE;
    
    mac_frm_ctrl_t mac_ctrl;
    mac_ctrl.frm_type      = MAC_FRAME_TYPE_COMMAND;
    mac_ctrl.sec_enable    = FALSE;
    mac_ctrl.frm_pending   = FALSE;
    mac_ctrl.ack_req       = TRUE;
    mac_ctrl.des_addr_mode = ADDR_MODE_LONG;
    mac_ctrl.src_addr_mode = ADDR_MODE_SHORT;
    mac_ctrl.reseverd      = 0x00;
    osel_memcpy(new_pbuf->data_p, &mac_ctrl, sizeof(mac_frm_ctrl_t));
    new_pbuf->data_p += MAC_HEAD_CTRL_SIZE;
    
    osel_memcpy(new_pbuf->data_p, &mac_pib.mac_bsn, sizeof(uint8_t));
    new_pbuf->data_p += MAC_HEAD_SEQ_SIZE;
    
    osel_memcpy(new_pbuf->data_p, &mac_frm_head_info.addr_info.src_addr, MAC_ADDR_LONG_SIZE);
    new_pbuf->data_p += MAC_ADDR_LONG_SIZE;
    
    osel_memcpy(new_pbuf->data_p, &mac_pib.mac_short_addr, MAC_ADDR_SHORT_SIZE);
    new_pbuf->data_p += MAC_ADDR_SHORT_SIZE;
    
    //mac payload
    *(uint8_t *)new_pbuf->data_p = MAC_CMD_ASSOC_RESP;
    new_pbuf->data_p += sizeof(uint8_t);
    
    osel_memcpy(new_pbuf->data_p, &assoc_resp, sizeof(mac_assoc_res_arg_t));
    new_pbuf->data_p += sizeof(mac_assoc_res_arg_t);
    
    //pbuf attri
    new_pbuf->attri.seq = mac_pib.mac_bsn++;
    new_pbuf->attri.need_ack = mac_ctrl.ack_req;
    new_pbuf->attri.already_send_times.mac_send_times = 0;
    new_pbuf->attri.send_mode = TDMA_SEND_MODE;
    new_pbuf->attri.dst_id = MAC_BROADCAST_ADDR;
    new_pbuf->data_len = new_pbuf->data_p - new_pbuf->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
    DBG_ASSERT((new_pbuf->data_p - new_pbuf->head) <= MEDIUM_PBUF_BUFFER_SIZE __DBG_LINE);
    
    sbuf->orig_layer = MAC_LAYER;
    sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_mode = ADDR_MODE_LONG;
    sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr = mac_frm_head_info.addr_info.src_addr;
    sbuf->primargs.pbuf->attri.already_send_times.mac_send_times = 0;
    
    if (assoc_req.device_type == NODE_TYPE_ROUTER)
    {
        mac_inter_assoc_resp_list_add(sbuf, mac_pib.supfrm_cfg_arg.inter_unit_num - mac_pib.hops);
    }
    else
    {
        mac_intra_assoc_resp_list_add(sbuf);
    }
}

void mac_cmd_frame_parse(pbuf_t *pbuf)
{
    uint8_t cmd_frm_type = *(pbuf->data_p);
    pbuf->data_p += sizeof(uint8_t);
    
    switch (cmd_frm_type)
    {
    case MAC_CMD_ASSOC_REQ: // 处理关联请求帧
		mac_assoc_request_frame_parse(pbuf);
        break;
        
    case MAC_CMD_ASSOC_RESP:
        mac_assoc_resp_frame_parse(pbuf);
        break;
        
    default:
        break;
    }
}


