#include <osel_arch.h>
#include <sbuf.h>
#include <debug.h>
#include <hal_timer.h>
#include <phy_packet.h>
#include <mac_stack.h>
#include <mac_children.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_send_buf.h>
#include <mac_global.h>
#include <mac_schedule.h>
#include <mac_frames.h>
#include <mac_cmd_frame.h>
#include <mac_moniter.h>
#include <mac_module.h>
#include <mac_prim.h>

static hal_timer_t *assoc_timeout_timer = NULL;
static volatile uint8_t assoc_request_cnt = 0;

static void assoc_timeout_timer_cb(void *p)
{
    assoc_timeout_timer = NULL;

    assoc_request_cnt++;
    sbuf_t *sbuf = sbuf_alloc(__SLINE1);
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    sbuf->primtype = M2M_ASSOC_REQUETS;
    osel_post(M_MAC_PRIM_EVENT, sbuf, OSEL_EVENT_PRIO_LOW);
}

static void m2m_assoc_request(void)
{
    if(assoc_request_cnt >= ASSOC_REQUEST_CNT_MAX)
    {
        assoc_request_cnt = 0;
        mac_moniter_stop();
        mac_moniter_start();
        return;
    }

    if ((NULL == assoc_timeout_timer) && (mac_state.ss == SUB_STATE_IDLE))
    {
        HAL_TIMER_SET_REL(mac_get_global_superfrm_time() * 3,
                          assoc_timeout_timer_cb,
                          NULL,
                          assoc_timeout_timer);
        DBG_ASSERT(assoc_timeout_timer != NULL __DBG_LINE);

        if (!mac_get_coord(&coord))
        {
            return;
        }
        mac_neighbors_node_set_state(coord.dev_id, FALSE);  // 置成不可关联状态，等收到关联应答再还原成可关联
        mac_make_assoc_request(&coord);
    }
}

void n2m_data_request_handler(sbuf_t *sbuf)
{
    mac_data_t data;    
    n2m_data_request_t *n2m_data_request = &sbuf->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg;

    if (ADDR_MODE_SHORT == n2m_data_request->dst_mode)
    {
        data.dest_addr.mode = MAC_SHORT_ADDR;
        data.dest_addr.short_addr = n2m_data_request->dst_addr;
    }
    else
    {
        data.dest_addr.mode = MAC_LONG_ADDR;
        osel_memcpy(data.dest_addr.long_addr, &n2m_data_request->dst_addr, 8);
    }
    if (ADDR_MODE_SHORT == n2m_data_request->src_mode)
    {
        data.src_addr.mode = MAC_SHORT_ADDR;
        data.src_addr.short_addr = n2m_data_request->src_addr;
    }
    else
    {
        data.src_addr.mode = MAC_LONG_ADDR;
        osel_memcpy(data.src_addr.long_addr, &n2m_data_request->src_addr, 8);
    }
    data.payload_len = n2m_data_request->msdu_length;
    data.payload = n2m_data_request->msdu;
    //
    if (mac_state.ss != SUB_STATE_ASSOCIATION_OK)
    {
        if (   NULL != mac_pib.mac_dependent
            && NULL != mac_pib.mac_dependent->mac_data_confirm)
        {
            mac_data_confirm_t confirm;
            confirm.handle = sbuf->handle;
            confirm.status = MAC_UNASSOCIATION_STATUS;
            confirm.send_times = 0;
            mac_pib.mac_dependent->mac_data_confirm(&confirm);
        }
        return;
    }
    
    send_option_t option;
    mac_node_info_t dest_node;
    
    if (   ((TRUE == mac_get_parent(&dest_node))
            && (   (TRUE == mac_is_node_addr_equal(&dest_node.short_addr, &data.dest_addr))
                || (TRUE == mac_is_node_addr_equal(&dest_node.long_addr, &data.dest_addr))))
        || (TRUE == mac_find_neighbour(&data.dest_addr, &dest_node)))
    {
        option = INTER_DURATION_SENDING;
    }
    else
    {
        if (TRUE == mac_find_child(&data.dest_addr, &dest_node))
        {
            if (ROUTER_TYPE == dest_node.type)
            {
                option = INTER_DURATION_SENDING;
            }
            else if (END_NODE_TYPE == dest_node.type)
            {
                option = INTRA_DURATION_SENDING;
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
    }
    
    pbuf_t *pbuf_new = pbuf_alloc(MEDIUM_PBUF_BUFFER_SIZE __PLINE1);
    if (pbuf_new == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }

    sbuf_t *sbuf_new = sbuf_alloc(__SLINE1);
    if (sbuf_new == NULL)
    {
        pbuf_free(&pbuf_new __PLINE2);
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    
    sbuf_new->primargs.pbuf = pbuf_new;
    sbuf_new->handle = sbuf->handle;
    pbuf_new->data_p = pbuf_new->head + PHY_HEAD_SIZE;

    mac_frm_ctrl_t mac_frm_ctrl;
    mac_frm_ctrl.frm_type        = MAC_FRAME_TYPE_DATA;
    mac_frm_ctrl.sec_enable      = FALSE;
    mac_frm_ctrl.frm_pending     = FALSE;
    mac_frm_ctrl.ack_req         = TRUE;
    mac_frm_ctrl.reseverd        = 0;
    mac_frm_ctrl.des_addr_mode   = data.dest_addr.mode;
    mac_frm_ctrl.src_addr_mode   = data.src_addr.mode;

    osel_memcpy(pbuf_new->data_p, &mac_frm_ctrl, sizeof(mac_frm_ctrl_t));
    pbuf_new->data_p += MAC_HEAD_CTRL_SIZE;

    osel_memcpy(pbuf_new->data_p, &mac_pib.mac_bsn, sizeof(uint8_t));
    pbuf_new->data_p += MAC_HEAD_SEQ_SIZE;

    if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy(pbuf_new->data_p, &(data.dest_addr.short_addr), MAC_ADDR_SHORT_SIZE);
        pbuf_new->data_p += MAC_ADDR_SHORT_SIZE;
    }
    else if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy(pbuf_new->data_p, data.dest_addr.long_addr, MAC_ADDR_LONG_SIZE);
        pbuf_new->data_p += MAC_ADDR_LONG_SIZE;
    }

    if (mac_frm_ctrl.src_addr_mode == ADDR_MODE_SHORT)
    {
        osel_memcpy(pbuf_new->data_p, &mac_pib.mac_short_addr, MAC_ADDR_SHORT_SIZE);
        pbuf_new->data_p += MAC_ADDR_SHORT_SIZE;
    }
    else if (mac_frm_ctrl.src_addr_mode == ADDR_MODE_LONG)
    {
        osel_memcpy(pbuf_new->data_p, &mac_pib.mac_long_addr, MAC_ADDR_LONG_SIZE);
        pbuf_new->data_p += MAC_ADDR_LONG_SIZE;
    }
    
    osel_memcpy(pbuf_new->data_p, data.payload, data.payload_len);
    pbuf_new->data_p += data.payload_len;
    
    pbuf_new->data_len = pbuf_new->data_p - pbuf_new->head - PHY_HEAD_SIZE + MAC_FCS_SIZE;
    DBG_ASSERT((pbuf_new->data_p - pbuf_new->head) <= MEDIUM_PBUF_BUFFER_SIZE __DBG_LINE);
    
    pbuf_new->attri.need_ack = TRUE;
    pbuf_new->attri.seq = mac_pib.mac_bsn++;
    pbuf_new->attri.already_send_times.mac_send_times = 0;
    pbuf_new->attri.send_mode = TDMA_SEND_MODE;
    if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_SHORT)
    {
        pbuf_new->attri.dst_id = data.dest_addr.short_addr;
    }
    else if (mac_frm_ctrl.des_addr_mode == ADDR_MODE_LONG)
    {
        pbuf_new->attri.dst_id = MAC_BROADCAST_ADDR;
    }
    
    sbuf_new->orig_layer = NWK_LAYER;
    sbuf_new->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_mode = data.dest_addr.mode;
    if (MAC_SHORT_ADDR == data.dest_addr.mode)
    {
        sbuf_new->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr = data.dest_addr.short_addr;
    }
    else if (MAC_LONG_ADDR == data.dest_addr.mode)
    {
        osel_memcpy(&sbuf_new->primargs.prim_arg.mac_prim_arg.n2m_data_request_arg.dst_addr,
                    data.dest_addr.long_addr, MAC_ADDR_LONG_SIZE);
    }
    
    mac_slot_send_request(sbuf_new, option, dest_node.depth);
}

static void mac_prim_handler(sbuf_t *sbuf)
{
    DBG_ASSERT(sbuf != NULL __DBG_LINE);
    switch (sbuf->primtype)
    {
    case N2M_DATA_REQUEST:
        n2m_data_request_handler(sbuf);
        break;
    case M2M_ASSOC_REQUETS:
        m2m_assoc_request();
        break;
    default:
        break;
    }
    if (sbuf->primargs.pbuf != NULL)
    {
        pbuf_free(&(sbuf->primargs.pbuf)__PLINE2);
    }
    sbuf_free(&sbuf __SLINE2);
}

static void m_prim_event_handler(const osel_event_t *const pmsg)
{
    DBG_ASSERT(pmsg != NULL __DBG_LINE);
    switch (pmsg->sig)
    {
    case M_MAC_PRIM_EVENT:
        mac_prim_handler((sbuf_t *)(pmsg->param));
        break;

    default:
        break;
    }
}

void m_prim_init(void)
{
    module_bind_event(m_prim_event_handler, M_MAC_PRIM_EVENT);
}
