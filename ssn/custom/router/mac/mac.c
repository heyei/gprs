#include <osel_arch.h>
#include <sbuf.h>
#include <hal_board.h>
#include <mac.h>
#include <mac_stack.h>
#include <mac_prim.h>
#include <mac_frames.h>
#include <mac_module.h>
#include <mac_neighbors.h>
#include <mac_beacon.h>
#include <mac_moniter.h>
#include <mac_global.h>
#include <mac_send_buf.h>
#include <mac_schedule.h>
#include <mac_stack.h>
#include <mac_children.h>

static void mac_task(void *e)
{
    DBG_ASSERT(NULL != e __DBG_LINE);

    m_module_ctrl_handler((osel_event_t *)e);
}


void mac_init(void)
{
    uint64_t temp_node_id = 0xFFFFFFFFFFFFFFFF;
    if(osel_memcmp(hal_board_info_get().device_id, (uint8_t *)&temp_node_id, sizeof(uint64_t)))
    {
        return;
    }
    osel_task_tcb *mac_task_tcb = osel_task_create(&mac_task, MAC_TASK_PRIO);

    osel_subscribe(mac_task_tcb, M_TRAN_RESEND_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_CSMA_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_RXOK_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_RXOVR_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TXOK_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TXUND_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_TX_ERROR_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_ACK_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_BACKOFF_SUCCESS_EVENT);
    osel_subscribe(mac_task_tcb, M_TRAN_BACKOFF_FAIL_EVENT);
    osel_subscribe(mac_task_tcb, M_SYNC_BACKGROUND);
    osel_subscribe(mac_task_tcb, M_SLOT_TIMEOUT_EVENT);
    osel_subscribe(mac_task_tcb, M_MAC_PRIM_EVENT);
    osel_subscribe(mac_task_tcb, M_MAC_MONI_START_EVENT);
    osel_subscribe(mac_task_tcb, M_MAC_MONI_STOP_EVENT);
    osel_subscribe(mac_task_tcb, M_MAC_MONI_SYNC_EVENT);

    m_tran_init();
    m_slot_init();
    m_sync_init();
    m_prim_init();
    mac_moniter_init();

    mac_frames_init();
    mac_global_init();
    mac_children_init();
    mac_neighbor_table_init();
    mac_send_list_init();
    mac_schedule_init();
}
