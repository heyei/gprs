#include <node_cfg.h>
#include <osel_arch.h>
#include <sbuf.h>
#include <debug.h>
#include <mac_stack.h>
#include <mac_beacon.h>
#include <mac_global.h>
#include <mac_neighbors.h>

#define INVAILD_ID                      0xFFFF
#define INDEX                           16
#define MAX_HOP_NUMBER                  0xFF

#define LIVE_TIMEOUT                    0x0A
#define MAX_UNASSOC_LIFE                0x10

static neighbor_node_t neighbor_node_array[DEV_COUNT];
static uint8_t max_index = 0;

void mac_neighbor_table_init(void)
{
    for (uint8_t i = 0; i < DEV_COUNT; i++)
    {
        neighbor_node_array[i].dev_id = INVAILD_ID;
        neighbor_node_array[i].rssi   = -127;
        neighbor_node_array[i].hops   = MAX_HOP_NUMBER;
        neighbor_node_array[i].time_stamp = 0x00000000;
        neighbor_node_array[i].beacon_ind = INDEX;
        neighbor_node_array[i].life_cycle = LIVING_TIME;
        neighbor_node_array[i].intra_channel = 0;
        neighbor_node_array[i].unassoc_life_cycle = 0;  // can be assoc
    }

    max_index = 0;
}

void mac_neighbor_table_update(void)
{
    uint8_t i = 0;

    for (i = max_index; i > 0; i--)
    {
        if (neighbor_node_array[i - 1].dev_id != INVAILD_ID)
        {
            neighbor_node_array[i - 1].life_cycle++;
            if (neighbor_node_array[i - 1].life_cycle >= LIVE_TIMEOUT)
            {
                neighbor_node_array[i - 1].dev_id = INVAILD_ID;

                if (i >= max_index)
                {
                    max_index--;
                }
            }
        }
    }
}


bool_t mac_neighbor_node_add(neighbor_node_t *neighbor_device)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == neighbor_device->dev_id)
        {
            neighbor_node_array[i].hops = neighbor_device->hops;
            neighbor_node_array[i].rssi = neighbor_device->rssi;
            neighbor_node_array[i].time_stamp = neighbor_device->time_stamp;
            neighbor_node_array[i].beacon_ind = neighbor_device->beacon_ind;
            neighbor_node_array[i].life_cycle = LIVING_TIME;
            neighbor_node_array[i].intra_channel = neighbor_device->intra_channel;
            neighbor_node_array[i].inter_channel = neighbor_device->inter_channel;
            return TRUE;
        }
    }

    for (i = 0; i < DEV_COUNT; i++)
    {
        if (neighbor_node_array[i].dev_id == INVAILD_ID)
        {
            neighbor_node_array[i].dev_id = neighbor_device->dev_id;
            neighbor_node_array[i].hops = neighbor_device->hops;
            neighbor_node_array[i].rssi = neighbor_device->rssi;
            neighbor_node_array[i].time_stamp = neighbor_device->time_stamp;
            neighbor_node_array[i].beacon_ind = neighbor_device->beacon_ind;
            neighbor_node_array[i].intra_channel = neighbor_device->intra_channel;
            neighbor_node_array[i].inter_channel = neighbor_device->inter_channel;
            neighbor_node_array[i - 1].unassoc_life_cycle = 0; // can be assoc

            if (i >= max_index)
            {
                max_index++;
            }

            return TRUE;
        }
    }

    if (i == DEV_COUNT)
    {
        //DBG_ASSERT(FALSE __DBG_LINE);
        return FALSE;
    }

    return TRUE;
}

bool_t mac_neighbors_node_set_state(uint16_t id, bool_t assoc_state)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == id)
        {
            if (assoc_state == TRUE)
            {
                neighbor_node_array[i].unassoc_life_cycle = 0;
            }
            else
            {
                neighbor_node_array[i].unassoc_life_cycle = MAX_UNASSOC_LIFE;
            }
            return TRUE;
        }
    }

    return FALSE;
}

/**
 * [mac_neighbor_node_delete : ]
 * @param  neighbor_device [description]
 * @return                 [TRUE -- success]
 */
bool_t mac_neighbor_node_delete(neighbor_node_t *neighbor_device)
{
    uint8_t i = 0;
    for (i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == neighbor_device->dev_id)
        {
            neighbor_node_array[i].dev_id = INVAILD_ID;

            if (i == (max_index - 1))
            {
                max_index--;
            }

            return TRUE;
        }
    }

    return FALSE;
}

bool_t mac_get_coord(neighbor_node_t *coord)
{
    bool_t  res = FALSE;
    uint8_t best_index = DEV_COUNT;
    uint8_t best_index_by_rssi = DEV_COUNT;
    uint8_t best_hops = MAX_HOPS;
    int8_t  best_rssi = RSSI_QUERY_FAILED;
    bool_t has_kind_neighbor = FALSE;
    bool_t has_neighbor_in_rigth_depth = FALSE;

    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == INVAILD_ID)
        {
            continue;
        }
        if ((mac_pib.nwk_depth != 0)
            && (neighbor_node_array[i].hops != mac_pib.nwk_depth - 1))
        {
            continue;
        }
        has_neighbor_in_rigth_depth = TRUE;
        if (neighbor_node_array[i].unassoc_life_cycle > 0)  // 更新不可关联的时间，让节点一段时间后可被重新尝试关联
        {
            neighbor_node_array[i].unassoc_life_cycle--;
            continue;
        }
        has_kind_neighbor = TRUE;
    }
    
    if(has_neighbor_in_rigth_depth == FALSE
       || has_kind_neighbor == FALSE)
    {
        return res;
    }
    
    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == INVAILD_ID)
        {
            continue;
        }
        if ((mac_pib.nwk_depth != 0)
            && (neighbor_node_array[i].hops != mac_pib.nwk_depth - 1))
        {
            continue;
        }
        if (neighbor_node_array[i].unassoc_life_cycle > 0)
        {
            continue;
        }
        if (neighbor_node_array[i].rssi >= RSSI_QUERY_THRESHOLD)
        {
            if (neighbor_node_array[i].hops < best_hops)
            {
                best_hops = neighbor_node_array[i].hops;
                best_index = i;
                res = TRUE;
            }
        }
        if (neighbor_node_array[i].rssi > best_rssi)
        {
            best_rssi = neighbor_node_array[i].rssi;
            best_index_by_rssi = i;
            res = TRUE;
        }
    }
    
    if (res)
    {
        if (best_index == DEV_COUNT)
        {
            best_index = best_index_by_rssi;
        }
        osel_memcpy((uint8_t *)coord,
                    (uint8_t *)&neighbor_node_array[best_index],
                    sizeof(neighbor_node_t));
    }
    
    return res;
}

bool_t mac_get_neighbour_by_beacon_index(uint8_t          beacon_index,
                                         neighbor_node_t *neighbour)
{
    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].beacon_ind == beacon_index)
        {
            osel_memcpy((uint8_t *)neighbour,
                        (uint8_t *)&neighbor_node_array[i],
                        sizeof(neighbor_node_t));
            return TRUE;
        }
    }
    
    return FALSE;
}

bool_t mac_get_neighbour_by_short_addr(uint16_t         short_addr,
                                       neighbor_node_t *neighbour)
{
    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id == short_addr)
        {
            osel_memcpy((uint8_t *)neighbour,
                        (uint8_t *)&neighbor_node_array[i],
                        sizeof(neighbor_node_t));
            return TRUE;
        }
    }
    
    return FALSE;
}

uint16_t mac_get_neighbour_beacon_bitmap()
{
    uint16_t beacon_bitmap = 0;
    
    for (uint8_t i = 0; i < max_index; i++)
    {
        if (neighbor_node_array[i].dev_id != INVAILD_ID)
        {
            DBG_ASSERT(neighbor_node_array[i].beacon_ind >= 1 __DBG_LINE);
            beacon_bitmap |= 1 << (neighbor_node_array[i].beacon_ind - 1);
        }
    }
    
    return beacon_bitmap;
}
