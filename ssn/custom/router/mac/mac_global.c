#include <sbuf.h>
#include <mac_stack.h>
#include <mac_beacon.h>
#include <mac_global.h>
#include <hal_board.h>
#include <mac_neighbors.h>
#include <mac_children.h>

extern device_info_t device_info;
mac_pib_t mac_pib;
mac_state_t mac_state;
uint64_t node_nui = 0;
uint16_t loc_beacon_map = 0;
uint16_t last_beacon_map;
uint16_t alloc_bcn_bitmap = 0x0001; 

void mac_global_init(void)
{
    mac_state.ms = MAIN_STATE_IDLE;
    mac_state.ss = SUB_STATE_IDLE;

    osel_memset(&mac_pib, 0x00, sizeof(mac_pib_t));
    mac_pib.self_index = 0xFF;

    uint8_t id_size = sizeof(device_info.device_id);

    osel_memcpy((uint8_t *)&(mac_pib.mac_long_addr), device_info.device_id, id_size);
    mac_pib.mac_short_addr = hal_board_get_device_short_addr(device_info.device_id);

    osel_memcpy((uint8_t *)&node_nui, device_info.device_id, id_size);
    
    loc_beacon_map = 0;
    alloc_bcn_bitmap = 0x0001;
    mac_pib.beacon_enabled = FALSE;
    mac_pib.nwk_depth = device_info.nwk_depth;
}

void mac_global_reset(void)
{
    mac_state.ms = MAIN_STATE_IDLE;
    mac_state.ss = SUB_STATE_IDLE;
    mac_pib.beacon_enabled = FALSE;
    mac_pib.beacon_seq = 0;
    
    mac_beacon_init();
    mac_children_init();
}
