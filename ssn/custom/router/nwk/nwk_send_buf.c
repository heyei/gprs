#include <data_type_def.h>
#include <nwk_send_buf.h>

/// 判断是否是心跳帧的标识
bool_t nwk_is_hb_handle(uint8_t handle)
{
    return FALSE;
}

/// 判断是否是入网请求帧的标识
bool_t nwk_is_join_handle(uint8_t handle)
{
    return FALSE;
}

/// 判断是否是数据帧的标识
bool_t nwk_is_data_handle(uint8_t handle)
{
    return FALSE;
}
