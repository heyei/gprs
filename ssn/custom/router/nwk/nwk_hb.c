#include <data_type_def.h>
#include <debug.h>
#include <sbuf.h>
#include <hal_timer.h>
#include <hal_energy.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_frame.h>
#include <nwk_send_buf.h>
#include <nwk_route.h>
#include <nwk_hb.h>

static hal_timer_t *hb_timer = NULL;
static uint32_t hb_timer_period = 0;
static uint32_t tx_success_times = 0;
static uint32_t tx_total_times = 0;
static nwk_location_lqi_tuple_t location_tuple_group[NWK_MAX_LOCATION_TUPLE_NUM];
static uint8_t valid_location_tuple_num = 0;

static void nwk_send_hb()
{
    nwk_hb_t hb;
    uint8_t frame[PKT_LEN_MAX];
    uint8_t frame_len;
    
    hb.dest_addr = nwk_pib.sink_addr;
    hb.src_addr = nwk_pib.short_addr;
    
    hb.frame_control.device_type = ROUTER_TYPE;
    hb.frame_control.energy_report = 2;
    hb.residual_energy = hal_energy_get();
    if (nwk_pib.has_reset_alarm)
    {
        hb.frame_control.alarm_type = 1;
        hb.alarm_info = debug_get_info();
    }
    else if(nwk_pib.has_rejoin_alarm)
    {
        hb.frame_control.alarm_type = 2;
    }
    else
    {
        hb.frame_control.alarm_type = 0;
    }
    
    hb.frame_control.location =  1;
    hb.location_tuple_num = nwk_get_location_tuple_group(&hb.location_tuple_group);
    if (hb.location_tuple_num == 0)
    {
        hb.frame_control.location = 0;
    }
    hb.frame_control.overhead_mode = 0;
    mac_node_info_t parent;
    if (FALSE == mac_get_parent(&parent))
    {
        return;
    }
    hb.parent_addr = parent.short_addr.short_addr;
    if (tx_success_times == 0 || tx_total_times == 0)
    {
        hb.tx_success_rate = 0;
    }
    else
    {
        hb.tx_success_rate = tx_success_times * 100 / tx_total_times;
    }
    frame_len = nwk_get_hb_frame(&hb, frame, PKT_LEN_MAX);
    if (frame_len == 0)
    {
        return;
    }
    
    mac_data_t mac_data;
    mac_data.dest_addr.mode = MAC_SHORT_ADDR;
    mac_data.dest_addr.short_addr = parent.short_addr.short_addr;
    mac_data.src_addr.mode = MAC_SHORT_ADDR;
    mac_data.src_addr.short_addr = nwk_pib.short_addr.short_addr;
    mac_data.security = 0;
    mac_data.payload = frame;
    mac_data.payload_len = frame_len;
    mac_data_request(&mac_data, NWK_HB_SEND_HANDLE);
}

static void hb_timer_cb(void *p)
{
    if (NULL != hb_timer)
    {
        hb_timer = NULL;
        osel_post(NWK_HEART_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
    }
}

void nwk_hb_start(uint32_t period)
{
    valid_location_tuple_num = 0;
    hb_timer_period = period;
    osel_post(NWK_HEART_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void nwk_hb_timer_event_handler()
{
    if (nwk_pib.is_joined)
    {
        nwk_route_update_lifecycle(); // 更新下行路由表生命期，是生命递减
        nwk_send_hb();
        if (hb_timer == NULL)
        {
            HAL_TIMER_SET_REL(MS_TO_TICK(hb_timer_period),
                              hb_timer_cb,
                              NULL,
                              hb_timer);            
            DBG_ASSERT(hb_timer != NULL __DBG_LINE);
        }
    }
}

void nwk_hb_add_tx_info(bool_t  tx_success,
                        uint8_t send_times)
{
    if (tx_success)
    {
        tx_success_times++;
    }
    tx_total_times += send_times;
}

void nwk_hb_clear()
{
    valid_location_tuple_num = 0;
    tx_success_times = 0;
    tx_total_times = 0;
    nwk_pib.has_rejoin_alarm = FALSE;
    nwk_pib.has_reset_alarm = FALSE;
}

void nwk_add_location_tuple(const nwk_location_lqi_tuple_t *tuple)
{
    for (uint8_t i = 0; i < valid_location_tuple_num; i++)
    {
        if (location_tuple_group[i].src_addr == tuple->src_addr)
        {
            location_tuple_group[i] = *tuple;
            return;
        }
    }
    if (valid_location_tuple_num < NWK_MAX_LOCATION_TUPLE_NUM)
    {
        location_tuple_group[valid_location_tuple_num] = *tuple;
        valid_location_tuple_num++;
    }
    return;
}

uint8_t nwk_get_location_tuple_group(const nwk_location_lqi_tuple_t **p_group)
{
    *p_group = location_tuple_group;
    return valid_location_tuple_num;
}
