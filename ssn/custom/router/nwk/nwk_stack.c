#include <string.h>
#include <data_type_def.h>
#include <osel_arch.h>
#include <debug.h>
#include <sbuf.h>
#include <hal_timer.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_route.h>
#include <nwk_mac.h>
#include <nwk_prim.h>
#include <nwk_frame.h>

nwk_id_t my_nwk_addr = 0x0000;
nwk_pib_t nwk_pib;

/// 请求NWK发送数据，异步
/// 参数：
///     data        要发送的数据信息
///     handle      此次发送请求的标识
///     confirm_cb  返回确认的回调函数
/// 返回：
///     无
void nwk_data_request(const nwk_data_t     *data,
                      uint8_t               handle)
{
}

/// 判断两个地址是否相同
bool_t nwk_is_node_addr_equal(const nwk_node_addr_t *addr_1,
                              const nwk_node_addr_t *addr_2)
{
    if (addr_1->mode != addr_2->mode)
    {
        return FALSE;
    }
    if (addr_1->mode == NWK_SHORT_ADDR)
    {
        return (addr_1->short_addr == addr_2->short_addr);
    }
    else if (addr_1->mode == NWK_LONG_ADDR)
    {
        return (0 == memcmp(addr_1->long_addr, addr_2->long_addr, 8));
    }
    else
    {
        return FALSE;
    }
}

void nwk_join_request(void)
{
    osel_post(NWK_JOIN_REQUEST_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void nwk_hb_request()
{
    osel_post(NWK_HEART_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

/// 初始化NWK协议栈
void nwk_stack_init(const nwk_dependent_t *dependent)
{
    mac_node_addr_t mac_addr;
    mac_get_long_addr(&mac_addr);
    nwk_pib.sink_addr.mode = NWK_SHORT_ADDR;
    nwk_pib.sink_addr.short_addr = NWK_ADDR;
    nwk_pib.long_addr.mode = NWK_LONG_ADDR;
    memcpy(nwk_pib.long_addr.long_addr, mac_addr.long_addr, 8);
    nwk_pib.is_joined = FALSE;
    nwk_pib.has_reset_alarm = TRUE;
    nwk_pib.has_rejoin_alarm = FALSE;
    nwk_pib.nwk_dependent = dependent;
    nwk_route_init();
    mac_stack_init(nwk_get_mac_dependent());
    nwk_pib.hb_period = 10000;
}
