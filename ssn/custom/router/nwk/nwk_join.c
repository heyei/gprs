#include <data_type_def.h>
#include <sbuf.h>
#include <rf.h>
#include <hal_timer.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_frame.h>
#include <nwk_hb.h>
#include <nwk_send_buf.h>
#include <nwk_join.h>

static hal_timer_t *join_timer = NULL;
static uint8_t nwk_rejoin_cnt = 0;

/// 发送入网请求
static void nwk_send_join_request()
{
    nwk_join_request_t join_request;
    uint8_t frame[NWK_JOIN_REQUEST_FRAME_LEN];
    uint8_t frame_len;
    
    join_request.dest_addr = nwk_pib.sink_addr;
    join_request.src_addr = nwk_pib.long_addr;
    join_request.device_type = NODE_TYPE_ROUTER;
    join_request.join_apply_time = mac_get_global_time();
    
    frame_len = nwk_get_join_frame(&join_request, frame, NWK_JOIN_REQUEST_FRAME_LEN);
    if (NWK_JOIN_REQUEST_FRAME_LEN != frame_len)
    {
        return;
    }
    
    mac_data_t mac_data;
    mac_node_info_t parent;
    if (FALSE == mac_get_parent(&parent))
    {
        return;
    }
    mac_data.dest_addr.mode = MAC_SHORT_ADDR;
    mac_data.dest_addr.short_addr = parent.short_addr.short_addr;
    mac_data.src_addr.mode = MAC_LONG_ADDR;
    mac_get_long_addr(&mac_data.src_addr);
    mac_data.security = 0;
    mac_data.payload = frame;
    mac_data.payload_len = frame_len;
    mac_data_request(&mac_data, NWK_JOIN_SEND_HANDLE);
}

static void join_timer_cb(void *p)
{
    if (NULL != join_timer)
    {
        join_timer = NULL;
        osel_post(NWK_JOIN_REQUEST_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
    }
}

void nwk_join_request_event_handler()
{
    uint32_t interval;
    mac_node_info_t parent;
        
    if (TRUE == nwk_pib.is_mac_assoc)
    {
        nwk_rejoin_cnt = 0;
        nwk_send_join_request();
        if (NULL == join_timer)
        {
            if (TRUE == mac_get_beacon_interval(&interval))
            {
                if (TRUE == mac_get_parent(&parent))
                {
                    HAL_TIMER_SET_REL(MS_TO_TICK(interval * (MAX_INTER_SEND_TIMES + parent.depth)), // 上行和下行所需时间
                                      join_timer_cb,
                                      NULL,
                                      join_timer);
                    DBG_ASSERT(join_timer != NULL __DBG_LINE);
                }
            }
        }
    }
    else
    {
        mac_association_request();
    }
}

void nwk_join_request_timer_event_handler()
{
    uint32_t interval;
    
    if (FALSE == nwk_pib.is_joined)
    {
        if (   TRUE == nwk_pib.is_mac_assoc
            && nwk_rejoin_cnt++ < NWK_REJOIN_CNT_MAX)
        {
            nwk_send_join_request();
            if (NULL == join_timer)
            {
                if (TRUE == mac_get_beacon_interval(&interval))
                {
                    HAL_TIMER_SET_REL(MS_TO_TICK(interval * MAX_INTER_SEND_TIMES),
                                      join_timer_cb,
                                      NULL,
                                      join_timer);
                    DBG_ASSERT(join_timer != NULL __DBG_LINE);
                }
            }
        }
        else
        {
            mac_association_request();
        }
    }
}

void nwk_set_join_state()
{
    uint32_t interval;
    
    nwk_pib.is_joined = TRUE;
    if (TRUE == mac_get_beacon_interval(&interval))
    {
        nwk_hb_start(interval * NODE_NWK_HB_DURATION);
    }
}

/// 处理入网响应
void nwk_join_response_handler(uint16_t short_addr)
{
    nwk_pib.short_addr.mode = NWK_SHORT_ADDR;
    nwk_pib.short_addr.short_addr = short_addr;
    nwk_set_join_state();

    mac_node_addr_t mac_short_addr;
    mac_short_addr.mode = MAC_SHORT_ADDR;
    mac_short_addr.short_addr = short_addr;
    mac_beacon_enable(&mac_short_addr);
    rf_write_burst_reg(CHECK_HEADER3, (uint8_t *)&short_addr, 2);
}
