#include <data_type_def.h>
#include <sbuf.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_mac.h>


static mac_dependent_t mac_dependent;

/// MAC发送数据后返回确认的回调函数
void mac_data_confirm (const mac_data_confirm_t *data_confirm)
{
    pbuf_t *pbuf = pbuf_alloc(1 + sizeof(mac_data_confirm_t) __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    pbuf->data_p[0] = M2N_DATA_CONFIRM;
    pbuf->data_p++;
    osel_memcpy(pbuf->data_p, data_confirm, sizeof(mac_data_confirm_t));

    osel_post(MAC2NWK_PRIM_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

/// MAC通知NWK收到数据的接口，在MAC任务中调用
void mac_data_indicate(const mac_data_indication_t *data)
{
    pbuf_t *pbuf = pbuf_alloc((1 + sizeof(mac_data_indication_t) + data->payload_len) __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    pbuf->data_p[0] = M2N_DATA_INDICATION;
    pbuf->data_p++;
    osel_memcpy(pbuf->data_p, data, sizeof(mac_data_indication_t));
    pbuf->data_p += sizeof(mac_data_indication_t);
    osel_memcpy(pbuf->data_p, data->payload, data->payload_len);

    osel_post(MAC2NWK_PRIM_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

/// MAC通知NWK有设备成功关联的接口，关联响应已由MAC处理
void mac_child_association_indicate(const association_indication_t *asso_request)
{
}

/// MAC通知NWK有下级设备失去关联的接口
void mac_child_unassociation_indicate(const mac_node_addr_t *child_addr)
{
}

/// MAC通知NWK与上级设备失去关联的接口
void mac_unassociation_indicate(const unassociation_indication_t *unassoc)
{
    pbuf_t *pbuf = pbuf_alloc(1 + sizeof(unassociation_indication_t) __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    pbuf->data_p[0] = M2N_UNASSOC_INDICATION;
    pbuf->data_p++;
    osel_memcpy(pbuf->data_p, unassoc, sizeof(unassociation_indication_t));

    osel_post(MAC2NWK_PRIM_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

/// MAC通知NWK已成功与上级设备建立关联的接口，关联请求由MAC自行发起
void mac_association_confirm(const association_confirm_t *asso_confirm)
{
    pbuf_t *pbuf = pbuf_alloc(1 + sizeof(association_confirm_t) __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    pbuf->data_p[0] = M2N_ASSOC_CONFIRM;
    pbuf->data_p++;
    osel_memcpy(pbuf->data_p, asso_confirm, sizeof(association_confirm_t));

    osel_post(MAC2NWK_PRIM_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

/// MAC通知NWK收到的邻居Beacon信息
void mac_beacon_indicate(const beacon_indication_t *beacon_indication)
{
}

const mac_dependent_t *nwk_get_mac_dependent()
{
    mac_dependent.mac_association_confirm = mac_association_confirm;
    mac_dependent.mac_unassociation_indicate = mac_unassociation_indicate;
    mac_dependent.mac_data_confirm = mac_data_confirm;
    mac_dependent.mac_data_indicate = mac_data_indicate;
    return &mac_dependent;
}
