#include <data_type_def.h>
#include <sbuf.h>
#include <hal_timer.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_route.h>
#include <nwk_frame.h>
#include <nwk_join.h>
#include <nwk_hb.h>
#include <nwk_send_buf.h>
#include <nwk_prim.h>

static void m2n_data_indication_handler(pbuf_t *pbuf)
{
    mac_data_indication_t mac_data_ind;
    
    osel_memcpy(&mac_data_ind, pbuf->data_p, sizeof(mac_data_indication_t));
    pbuf->data_p += sizeof(mac_data_indication_t);
    mac_data_ind.payload = pbuf->data_p;
    // 
    nwk_frame_t nwk_frame;
    
    if (FALSE == nwk_parse_frame(mac_data_ind.payload, mac_data_ind.payload_len, &nwk_frame))
    {
        return;
    }
    
    if (  (TRUE == nwk_is_node_addr_equal(&nwk_pib.short_addr, &nwk_frame.dest_addr))
       || (TRUE == nwk_is_node_addr_equal(&nwk_pib.long_addr, &nwk_frame.dest_addr))
           ) // 网络目的地址是否是本节点
    {
        if (NWK_DATA == nwk_frame.frame_type)
        {
            if (NULL != nwk_pib.nwk_dependent && NULL != nwk_pib.nwk_dependent->nwk_data_indicate)
            {
                nwk_data_indication_t nwk_data;
                nwk_data.dest_addr = nwk_frame.dest_addr;
                nwk_data.src_addr = nwk_frame.src_addr;
                nwk_data.payload = nwk_frame.data_payload.payload;
                nwk_data.payload_len = nwk_frame.data_payload.payload_len;
                nwk_pib.nwk_dependent->nwk_data_indicate(&nwk_data);
            }
        }
        else if (NWK_JOIN_RESP == nwk_frame.frame_type)
        {
            nwk_join_response_handler(nwk_frame.join_resp.short_addr);
        }
        return;
    }
    
    if (FALSE == nwk_pib.is_joined)
    {
        return;
    }
    
    mac_node_info_t parent_info;
    mac_get_parent(&parent_info);
    mac_node_addr_t parent;
    parent = parent_info.short_addr;
    
    if (TRUE == mac_is_child(&mac_data_ind.src_addr)) // 是否是下级节点
    {
        nwk_route_add_item(&nwk_frame.src_addr, &mac_data_ind.src_addr);
    }
    else if (FALSE == mac_is_node_addr_equal(&parent, &mac_data_ind.src_addr)) //是否是上级节点
    {
        return;
    }
    
    // 转发
    mac_node_addr_t next_hop;
    next_hop.mode = MAC_SHORT_ADDR;
    next_hop.short_addr = parent.short_addr;
    if (FALSE == nwk_is_node_addr_equal(&nwk_frame.dest_addr, &nwk_pib.sink_addr))
    {
        if (FALSE == nwk_route_get_next_hop_in_neighbour(&nwk_frame.dest_addr,
                                                         &next_hop))
        {
            if(FALSE == nwk_route_get_next_hop_downward(&nwk_frame.dest_addr,
                                                        &next_hop))
            {
                if (FALSE == mac_is_child(&mac_data_ind.src_addr))
                {
                    return;
                }
            }
        }
    }
    
    if (NWK_JOIN_RESP == nwk_frame.frame_type)
    {
        mac_node_addr_t child_addr;
        mac_node_info_t child_info;
        child_addr.mode = MAC_LONG_ADDR;
        osel_memcpy(child_addr.long_addr, nwk_frame.dest_addr.long_addr, 8);
        if (TRUE == mac_find_child(&child_addr, &child_info))
        {
            child_info.short_addr.mode = MAC_SHORT_ADDR;
            child_info.short_addr.short_addr = nwk_frame.join_resp.short_addr;
            mac_add_child(&child_info);
        }
    }
    else if (   (NWK_DATA == nwk_frame.frame_type)
             || (NWK_HB == nwk_frame.frame_type))
    {
        // 添加Location LQI Tuple
        nwk_location_lqi_tuple_t location_tuple;
        location_tuple.src_addr = mac_data_ind.src_addr.short_addr;
        location_tuple.lqi = mac_data_ind.rssi;
        location_tuple.time_stamp = mac_get_global_time();
        nwk_add_location_tuple(&location_tuple);
    }
    
    mac_data_t t_data;
    t_data.aux_security_header = mac_data_ind.aux_security_header;
    t_data.payload = mac_data_ind.payload;
    t_data.payload_len = mac_data_ind.payload_len;
    t_data.security = mac_data_ind.security;
    t_data.dest_addr = next_hop;
    t_data.src_addr.mode = MAC_SHORT_ADDR;
    mac_data_request(&t_data, 0);
}

static void m2n_data_confirm_handler(pbuf_t *pbuf)
{
    mac_data_confirm_t confirm;
    
    osel_memcpy(&confirm, pbuf->data_p, sizeof(mac_data_confirm_t));
    if (confirm.status == MAC_SUCCESS)
    {
        if (NWK_HB_SEND_HANDLE == confirm.handle)
        {
            nwk_hb_clear();
        }
        nwk_hb_add_tx_info(TRUE, confirm.send_times);
    }
    else
    {
        nwk_hb_add_tx_info(FALSE, confirm.send_times);
    }
}

static void m2n_unassoc_indication_handler(pbuf_t *pbuf)
{
    if (nwk_pib.is_joined == TRUE)
    {
        nwk_pib.has_rejoin_alarm = TRUE;
    }
    nwk_pib.is_joined = FALSE;
    nwk_pib.is_mac_assoc = FALSE;
}

static void m2n_assoc_confirm_handler(pbuf_t *pbuf)
{
    association_confirm_t confirm;
    osel_memcpy(&confirm, pbuf->data_p, sizeof(association_confirm_t));
    
    nwk_pib.parent_short_addr.mode = NWK_SHORT_ADDR;
    nwk_pib.parent_short_addr.short_addr = confirm.parent_addr.short_addr;
    nwk_pib.is_mac_assoc = TRUE;
    nwk_pib.is_joined = FALSE;
    nwk_join_request();
}

void nwk_prim_event_handler(pbuf_t *pbuf)
{
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    pbuf->data_p = pbuf->head;
    uint8_t primtype = pbuf->data_p[0];
    pbuf->data_p++;
    switch (primtype)
    {
    case M2N_DATA_INDICATION:
        m2n_data_indication_handler(pbuf);
        break;
    case M2N_DATA_CONFIRM:
        m2n_data_confirm_handler(pbuf);
        break;
    case M2N_ASSOC_CONFIRM:
        m2n_assoc_confirm_handler(pbuf);
        break;
    case M2N_UNASSOC_INDICATION:
        m2n_unassoc_indication_handler(pbuf);
    default:
        break;
    }
    pbuf_free(&pbuf __PLINE2);
}
