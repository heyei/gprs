#include <data_type_def.h>
#include <sbuf.h>
#include <hal_timer.h>
#include <mac_stack.h>
#include <nwk.h>
#include <nwk_stack.h>
#include <nwk_frame.h>

/// 组装NWK数据帧
/// 参数：
///     data            数据信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_data_frame(const nwk_data_t *data,
                           uint8_t          *frame,
                           uint8_t           max_frame_len)
{
    return 0;
}

/// 组装NWK入网请求帧
/// 参数：
///     data            入网请求信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_join_frame(const nwk_join_request_t *join,
                           uint8_t                  *frame,
                           uint8_t                   max_frame_len)
{
    nwk_frame_control_t control;
    uint8_t i = 0;
    
    control.type = NWK_JOIN;
    control.dest_mode = join->dest_addr.mode;
    control.src_mode = join->src_addr.mode;
    if (control.dest_mode != NWK_SHORT_ADDR || control.src_mode != NWK_LONG_ADDR)
    {
        return 0;
    }
    if (max_frame_len - i < NWK_JOIN_REQUEST_FRAME_LEN)
    {        
        return 0;
    }
    frame[i++] = *((uint8_t *)&control);
    frame[i++] = LO_UINT16(join->dest_addr.short_addr);
    frame[i++] = HI_UINT16(join->dest_addr.short_addr);
    osel_memcpy(&frame[i], join->src_addr.long_addr, 8);
    i += 8;
    frame[i++] = join->device_type;
    osel_memcpy(&frame[i], &join->join_apply_time, 4);
    i += 4;
    
    return i;
}

/// 组装NWK心跳帧
/// 参数：
///     hb              心跳信息
///     frame           输出帧内容的地址
///     max_frame_len   允许生成的最大帧长度
/// 返回：
///     组装的帧的长度
uint8_t nwk_get_hb_frame(const nwk_hb_t *hb,
                         uint8_t        *frame,
                         uint8_t         max_frame_len)
{
    uint8_t i = 0;    
    nwk_frame_control_t control;
    
    control.type = NWK_HB;
    control.dest_mode = hb->dest_addr.mode;
    control.src_mode = hb->src_addr.mode;
    if (control.dest_mode != NWK_SHORT_ADDR || control.src_mode != NWK_SHORT_ADDR)
    {
        return 0;
    }
    
    if (max_frame_len - i < 5)
    {        
        return 0;
    }
    frame[i++] = *((uint8_t *)&control);
    frame[i++] = LO_UINT16(hb->dest_addr.short_addr);
    frame[i++] = HI_UINT16(hb->dest_addr.short_addr);
    frame[i++] = LO_UINT16(hb->src_addr.short_addr);
    frame[i++] = HI_UINT16(hb->src_addr.short_addr);
    
    if (max_frame_len - i < 4)
    {        
        return 0;
    }
    frame[i++] = *(uint8_t *)&hb->frame_control;
    frame[i++] = LO_UINT16(hb->parent_addr);
    frame[i++] = HI_UINT16(hb->parent_addr);
    frame[i++] = hb->tx_success_rate;
    if (hb->frame_control.energy_report == 2)
    {
        if (max_frame_len - i < 1)
        {        
            return 0;
        }
        frame[i++] = hb->residual_energy;
    }
    if (hb->frame_control.alarm_type == 1)
    {
        if (max_frame_len - i < 2)
        {
            return 0;
        }
        frame[i++] = LO_UINT16(hb->alarm_info);
        frame[i++] = HI_UINT16(hb->alarm_info);
    }
    if (hb->frame_control.location != 0)
    {
        if (max_frame_len - i < 1 + hb->location_tuple_num * 7)
        {
            return 0;
        }
        frame[i++] = hb->location_tuple_num;
        for (uint8_t k = 0; k < hb->location_tuple_num; k++)
        {
            frame[i++] = LO_UINT16(hb->location_tuple_group[k].src_addr);
            frame[i++] = HI_UINT16(hb->location_tuple_group[k].src_addr);
            frame[i++] = hb->location_tuple_group[k].lqi;
            frame[i++] = HI_4_UINT32(hb->location_tuple_group[k].time_stamp);
            frame[i++] = HI_3_UINT32(hb->location_tuple_group[k].time_stamp);
            frame[i++] = HI_2_UINT32(hb->location_tuple_group[k].time_stamp);
            frame[i++] = HI_1_UINT32(hb->location_tuple_group[k].time_stamp);
        }
    }
    if (hb->frame_control.device_type == 1 && hb->frame_control.overhead_mode != 0)
    {
        if (max_frame_len - i < 1 + hb->neighbour_tuple_num * 9)
        {
            return 0;
        }
        frame[i++] = hb->neighbour_tuple_num;
        for (uint8_t k = 0; k < hb->neighbour_tuple_num; k++)
        {
            frame[i++] = LO_UINT16(hb->neighbour_tuple_group[k].dest_addr);
            frame[i++] = HI_UINT16(hb->neighbour_tuple_group[k].dest_addr);
            frame[i++] = LO_UINT16(hb->neighbour_tuple_group[k].src_addr);
            frame[i++] = HI_UINT16(hb->neighbour_tuple_group[k].src_addr);
            frame[i++] = hb->neighbour_tuple_group[k].lqi;
            frame[i++] = HI_4_UINT32(hb->neighbour_tuple_group[k].time_stamp);
            frame[i++] = HI_3_UINT32(hb->neighbour_tuple_group[k].time_stamp);
            frame[i++] = HI_2_UINT32(hb->neighbour_tuple_group[k].time_stamp);
            frame[i++] = HI_1_UINT32(hb->neighbour_tuple_group[k].time_stamp);
        }
    }
    
    return i;
}

/// 解析MAC数据载荷
bool_t nwk_parse_frame(const uint8_t *frame,
                       uint8_t        frame_len,
                       nwk_frame_t   *nwk_frame)
{
    uint8_t i = 0;
    if (frame_len - i < 1)
    {        
        return FALSE;
    }
    nwk_frame_control_t control;
    control = *(nwk_frame_control_t *)&frame[i];
    i++;
    nwk_frame->frame_type = control.type;
    if (NWK_SHORT_ADDR == control.dest_mode)
    {
        if (frame_len - i < 2)
        {        
            return FALSE;
        }
        nwk_frame->dest_addr.mode = NWK_SHORT_ADDR;
        nwk_frame->dest_addr.short_addr = BUILD_UINT16(frame[i], frame[i + 1]);
        i += 2;
    }
    else if (NWK_LONG_ADDR == control.dest_mode)
    {
        if (frame_len - i < 8)
        {        
            return FALSE;
        }
        nwk_frame->dest_addr.mode = NWK_LONG_ADDR;
        osel_memcpy(nwk_frame->dest_addr.long_addr, &frame[i], 8);
        i += 8;
    }
    
    if (NWK_SHORT_ADDR == control.src_mode)
    {
        if (frame_len - i < 2)
        {        
            return FALSE;
        }
        nwk_frame->src_addr.mode = NWK_SHORT_ADDR;
        nwk_frame->src_addr.short_addr = BUILD_UINT16(frame[i], frame[i + 1]);
        i += 2;
    }
    else if (NWK_LONG_ADDR == control.src_mode)
    {
        if (frame_len - i < 8)
        {        
            return FALSE;
        }
        nwk_frame->src_addr.mode = NWK_LONG_ADDR;
        osel_memcpy(nwk_frame->src_addr.long_addr, &frame[i], 8);
        i += 8;
    }
    
    if (NWK_DATA == nwk_frame->frame_type)
    {
        nwk_frame->data_payload.payload = &frame[i];
        nwk_frame->data_payload.payload_len = frame_len - i;
        return TRUE;
    }
    else if (NWK_JOIN == nwk_frame->frame_type)
    {
        if (frame_len - i != 5)
        {        
            return FALSE;
        }
        nwk_frame->join_request.device_type = frame[i];
        i += 1;
        osel_memcpy(&nwk_frame->join_request.join_apply_time, &frame[i], 4);
        i += 4;
        return TRUE;
    }
    else if (NWK_JOIN_RESP == nwk_frame->frame_type)
    {
        if (frame_len - i != 4)
        {        
            return FALSE;
        }
        nwk_frame->join_resp.mac_short_addr = BUILD_UINT16(frame[i], frame[i + 1]);
        nwk_frame->join_resp.short_addr = BUILD_UINT16(frame[i + 2], frame[i + 3]);
        return TRUE;
    }
    else if (NWK_HB == nwk_frame->frame_type)
    {
        return TRUE;
    }
    
    return FALSE;
}
