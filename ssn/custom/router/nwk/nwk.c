#include <data_type_def.h>
#include <debug.h>
#include <sbuf.h>
#include <osel_arch.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <nwk_frame.h>
#include <nwk_hb.h>
#include <nwk_join.h>
#include <nwk_prim.h>
#include <nwk.h>

static void nwk_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t *)e;
    switch (p->sig)
    {
    case MAC2NWK_PRIM_EVENT:
    case APP2NWK_PRIM_EVENT:
        nwk_prim_event_handler((pbuf_t *)(p->param));
        break;
    case NWK_HEART_TIMER_EVENT:
        nwk_hb_timer_event_handler();
        break;
    case NWK_JOIN_REQUEST_EVENT:
        nwk_join_request_event_handler();
        break;
    case NWK_JOIN_REQUEST_TIMER_EVENT:
        nwk_join_request_timer_event_handler();
        break;

    default:
        break;
    }
}

void nwk_init(void)
{
    osel_task_tcb *nwk_task_handle = osel_task_create(&nwk_task, NWK_TASK_PRIO);

    osel_subscribe(nwk_task_handle, MAC2NWK_PRIM_EVENT);
    osel_subscribe(nwk_task_handle, APP2NWK_PRIM_EVENT);
    osel_subscribe(nwk_task_handle, NWK_HEART_TIMER_EVENT);
    osel_subscribe(nwk_task_handle, NWK_JOIN_REQUEST_EVENT);
    osel_subscribe(nwk_task_handle, NWK_JOIN_REQUEST_TIMER_EVENT);
}
