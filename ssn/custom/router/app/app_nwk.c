#include <data_type_def.h>
#include <sbuf.h>
#include <mac_stack.h>
#include <nwk_stack.h>
#include <app_nwk.h>

static nwk_dependent_t nwk_dependent;

static void nwk_data_confirm(nwk_enum_t status,
                             uint8_t    handle)
{
}

static void nwk_data_indicate(const nwk_data_indication_t *data)
{
    pbuf_t *pbuf = pbuf_alloc((1 + sizeof(nwk_data_indication_t) + data->payload_len) __PLINE1);
    if (pbuf == NULL)
    {
        DBG_ASSERT(FALSE __DBG_LINE);
        return;
    }
    pbuf->data_p[0] = N2A_DATA_INDICATION;
    pbuf->data_p++;
    osel_memcpy(pbuf->data_p, data, sizeof(nwk_data_indication_t));
    pbuf->data_p += sizeof(nwk_data_indication_t);
    osel_memcpy(pbuf->data_p, data->payload, data->payload_len);

    osel_post(APP_PRIM_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

/// 获取NWK层所依赖的APP的接口函数
/// 参数：
///     无
/// 返回：
///     NWK层所依赖的APP的接口函数集
nwk_dependent_t *app_get_nwk_dependent()
{
    nwk_dependent.nwk_data_confirm = nwk_data_confirm;
    nwk_dependent.nwk_data_indicate = nwk_data_indicate;
    
    return &nwk_dependent;
}
