#include <osel_arch.h>
#include <debug.h>
#include <sbuf.h>
#include <nwk_stack.h>
#include <app_nwk.h>
#include <app.h>
#include <app_prim.h>
//#include <app_handles.h>
#include <app_cmd.h>

app_info_t app_info;

static void app_cycle_timer_cb(void *p)
{
	app_info.cycle_timer_handle = NULL;
	osel_post(APP_CYCLE_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void app_cycle_timer_start()
{
	if (app_info.cycle_timer_handle == NULL)
	{
        hal_wdt_clear(16000);
		HAL_TIMER_SET_REL(MS_TO_TICK(2000),
						  app_cycle_timer_cb,
						  NULL,
						  app_info.cycle_timer_handle);
		DBG_ASSERT(NULL != app_info.cycle_timer_handle __DBG_LINE);
	}
}

static void app_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t *)e;

    switch (p->sig)
    {
    case APP_PRIM_EVENT:
        app_prim_handler((pbuf_t *)(p->param));
        break;

//    case APP_INLINE_EVENT:
//        app_set_inline_status((bool_t)((uint32_t)(p->param)));
//        break;
//
    case APP_SERIAL_DATA_EVENT:
        app_serial_recv_handler();
        break;
    case APP_CYCLE_TIMER_EVENT:
        app_cycle_timer_start();
        break;
    default:
        break;
    }
}

void app_init(void)
{
    osel_task_tcb *app_task_handle = osel_task_create(&app_task, APP_TASK_PRIO);

    osel_subscribe(app_task_handle, APP_PRIM_EVENT);
//    osel_subscribe(app_task_handle, APP_INLINE_EVENT);
    osel_subscribe(app_task_handle, APP_SERIAL_DATA_EVENT);
    osel_subscribe(app_task_handle, APP_CYCLE_TIMER_EVENT);
    
    app_interface_config();
//    app_set_inline_status(FALSE);
    nwk_stack_init(app_get_nwk_dependent());
     app_cycle_timer_start();
}
