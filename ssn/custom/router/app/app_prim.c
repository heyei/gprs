#include <hal_board.h>
#include <app_prim.h>
#include <debug.h>
#include <nwk_stack.h>
#include <app_prim.h>

static void n2a_data_confirm_handler(pbuf_t *pbuf)
{
}

static void n2a_data_indication_handler(pbuf_t *pbuf)
{
    nwk_data_indication_t nwk_data_ind;
    
    osel_memcpy(&nwk_data_ind, pbuf->data_p, sizeof(nwk_data_indication_t));
    pbuf->data_p += sizeof(nwk_data_indication_t);
    nwk_data_ind.payload = pbuf->data_p;
    switch(nwk_data_ind.payload[0])
    {
    case 0x05:
        nwk_hb_request();
        break;
    case 0x06:
        hal_board_reset();
        break;
    default :
        break;
    }
}

void app_prim_handler(pbuf_t *pbuf)
{
    DBG_ASSERT(NULL != pbuf __DBG_LINE);
    pbuf->data_p = pbuf->head;
    uint8_t primtype = pbuf->data_p[0];
    pbuf->data_p++;
    switch (primtype)
    {
    case N2A_DATA_CONFIRM:
        n2a_data_confirm_handler(pbuf);
        break;
    case N2A_DATA_INDICATION:
        n2a_data_indication_handler(pbuf);
        break;
    default:
        break;
    }
    pbuf_free(&pbuf __PLINE2);
}
