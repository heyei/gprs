#include <osel_arch.h>
#include <serial.h>
#include <data_type_def.h>
#include <node_cfg.h>
#include <hal.h>
#include <hal_board.h>
#include <phy_state.h>
//#include <app_handles.h>
#include <app_cmd.h>
//#include <app_frames.h>

static uint8_t const power_array[] = {"0,1,2,3,4,5,6,7"};
static uint8_t const channel_array[] = {"0...199"};
static uint8_t const id_array[] = {"0000000000000000-FFFFFFFFFFFFFFFF"};
static uint8_t const nwk_depth_array[] = {"0,2,3,4,5,6,7,8,9"};
static uint8_t const version[3][50] =
{
    "WSN design: router-network\r\n",
    "Hardware Version: ITS-RP-A04\r\n",
    "Software Version: 0.8"
};

static uint8_t cmd_send_array[APP_CMD_SIZE_MAX];
static uint8_t cmd_recv_array[APP_CMD_SIZE_MAX];

static bool_t ascii_to_hex(uint8_t hi, uint8_t lo, uint8_t *hex)
{
    *hex = 0;
    if ((hi >= 0x30) && (hi <= 0x39))
    {
        hi -= 0x30;
    }
    else if ((hi >= 0x41) && (hi <= 0x46))
    {
        hi -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= (hi << 4);

    if ((lo >= 0x30) && (lo <= 0x39))
    {
        lo -= 0x30;
    }
    else if ((lo >= 0x41) && (lo <= 0x46))
    {
        lo -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= lo;

    return TRUE;
}

static bool_t hex_to_ascii(uint8_t *buf, uint8_t dat)
{
    uint8_t dat_buff;

    dat_buff = dat;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[1] = dat;

    dat = dat_buff;
    dat >>= 4;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[0] = dat;

    return TRUE;
}

static bool_t data_find_char(uint8_t *src_buf, uint8_t ch, uint8_t *len)
{
    uint8_t *src_p = src_buf;
    bool_t res = FALSE;
    while (*src_p != 0x00)
    {
        if (*src_p++ == ch)
        {
            res = TRUE;
            break;
        }
    }

    *len = (src_p - src_buf);

    return res;
}

void app_cmd_send(uint8_t *buf, uint8_t len)
{
    serial_write(SERIAL_1, buf, len);

    uint8_t echo_array[] = "\r\n";
    serial_write(SERIAL_1, echo_array, sizeof(echo_array) - 1);
}

static void app_cmd_start_handle(void)
{
    uint8_t echo_array[] = "\r\n";
    serial_write(SERIAL_1, echo_array, sizeof(echo_array) - 1);

    serial_write(SERIAL_1, CMD_SYSSTART, sizeof(CMD_SYSSTART) - 1);

    serial_write(SERIAL_1, echo_array, sizeof(echo_array) - 1);
}

/***************************************************************************/
static void app_cmd_test_parse(void)
{
    app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
}

/***************************************************************************/
static void app_cmd_intra_power_handle(uint8_t *buf, uint8_t len)
{
    uint8_t power = 0;
    device_info_t board_info = hal_board_info_look();

    if (*buf == '=')
    {
        if (len == 3)
        {
            if (buf[1] == '?')
            {
                app_cmd_send((uint8_t *)power_array, sizeof(power_array) - 1);
                app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
            }
            else if ((buf[1] >= 0x30) && (buf[1] <= 0x37))
            {
                power = buf[1] - 0x30;
                phy_set_power(power);
                board_info.power_sn = power;
                hal_board_info_save(&board_info, TRUE);
                app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
            }
        }
        else
        {
            app_cmd_send(CMD_ERROR01, sizeof(CMD_ERROR01) - 1);
        }
    }
    else if (*buf == '?')
    {
        power = board_info.power_sn + 0x30; // digital
        app_cmd_send(&power, sizeof(uint8_t));
        app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
        phy_set_power(power);
    }
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}

static uint8_t get_channel_from_buf(uint8_t *buf, uint8_t len)
{
    uint8_t channel = 0;

    if (len == 1)
    {
        channel = buf[len - 1] - 0x30;
    }
    else if (len == 2)
    {
        channel = (buf[len - 2] - 0x30) * 10 + (buf[len - 1] - 0x30);
    }
    else if (len == 3)
    {
        channel = (buf[len - 3] - 0x30) * 100 + (buf[len - 2] - 0x30) * 10
                  + (buf[len - 1] - 0x30);
    }
    else
    {
        channel = 255;
    }

    return channel;
}

static uint8_t set_channel_to_buf(uint8_t *buf, uint8_t dat)
{
    uint8_t hundrad_bit = dat/100;
    uint8_t ten_bit     = (dat-100*hundrad_bit)/10;
    uint8_t piece_bit   = dat-100*hundrad_bit-10*ten_bit;

    uint8_t len = 0;
    if(hundrad_bit>0)
    {
        buf[len++] = hundrad_bit + 0x30;
    }
    if(ten_bit>0)
    {
        buf[len++] = ten_bit + 0x30;
    }
    if(piece_bit>0)
    {
        buf[len++] = piece_bit + 0x30;
    }

    return len;
}

static void intra_channel_send(uint8_t *buf, uint8_t len)
{
    uint8_t i = 0;
    uint8_t ch_len = 0;
    for (; i < len - 1; i++)
    {
        ch_len += set_channel_to_buf(&cmd_send_array[ch_len], buf[i]);

        cmd_send_array[ch_len] = ',';
        ch_len += 1;
    }

    ch_len += set_channel_to_buf(&cmd_send_array[ch_len], buf[i]);

    app_cmd_send(cmd_send_array, ch_len);
    app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
}

static void app_cmd_intra_channel_handle(uint8_t *buf, uint8_t len)
{
    uint8_t channel = 0;
    uint8_t channel_index = 0;

    uint8_t *datap = buf;
    bool_t res = FALSE;
    uint8_t ch_len = 0;

    device_info_t board_info = hal_board_info_look();

    if (*datap == '=')
    {
        datap += 1;

        if (*datap == '?')
        {
            app_cmd_send((uint8_t *)channel_array, sizeof(channel_array) - 1);
            app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
        }
        else
        {
            for (uint8_t i = 0; i < 8; i++)
            {
                res = data_find_char(datap, ',', &ch_len);
                channel = get_channel_from_buf(datap, ch_len - 1);
                datap += ch_len;
                if (channel <= 199)
                {
                    board_info.intra_ch[channel_index++] = channel;
                }
                else
                {
                    app_cmd_send(CMD_ERROR01, sizeof(CMD_ERROR01) - 1);
                    return;
                }

                if (!res)
                {
                    break;
                }
            }
            board_info.intra_ch_cnt = channel_index;
            hal_board_info_save(&board_info, TRUE);
            app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
            hal_board_reset();
        }
    }
    else if (*datap == '?')
    {
        intra_channel_send((uint8_t *)(board_info.intra_ch), board_info.intra_ch_cnt);
    }
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}

static void app_cmd_intra_id_handle(uint8_t *buf, uint8_t len)
{
    device_info_t board_info = hal_board_info_get();

    if (*buf == '=')
    {
        if (len == 18)
        {
            for (uint8_t i = 0; i < len - 2; i += 2)
            {
                if (!ascii_to_hex(buf[i + 1], buf[i + 2],
                                  &board_info.device_id[sizeof(board_info.device_id) - 1 - (i / 2)]))
                {
                    app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
                    return;
                }
            }
            hal_board_info_save(&board_info, TRUE);
            app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
            hal_board_reset();
        }
        else if ((buf[1] == '?') && (len == 3))
        {
            app_cmd_send((uint8_t *)id_array, sizeof(id_array) - 1);
            app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
        }
        else
        {
            app_cmd_send(CMD_ERROR01, sizeof(CMD_ERROR01) - 1);
        }
    }
    else if (*buf == '?')
    {
        for (uint8_t i = 0; i < sizeof(board_info.device_id); i++)
        {
            hex_to_ascii(&cmd_send_array[i * 2],
                         board_info.device_id[sizeof(board_info.device_id) - 1 - i]);
        }
        app_cmd_send(cmd_send_array, sizeof(board_info.device_id) * 2);
        app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
    }
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}

static void app_cmd_intra_nwk_depth_handle(uint8_t *buf, uint8_t len)
{
    uint8_t depth = 0;
    device_info_t board_info = hal_board_info_look();

    if (*buf == '=')
    {
        if (len == 3)
        {
            if (buf[1] == '?')
            {
                app_cmd_send((uint8_t *)nwk_depth_array, sizeof(nwk_depth_array) - 1);
                app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
            }
            else if ((buf[1] >= 0x30) && (buf[1] <= 0x39))
            {
                depth = buf[1] - 0x30;
                board_info.nwk_depth = depth;
                hal_board_info_save(&board_info, TRUE);
                app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
                hal_board_reset();
            }
        }
        else
        {
            app_cmd_send(CMD_ERROR01, sizeof(CMD_ERROR01) - 1);
        }
    }
    else if (*buf == '?')
    {
        depth = board_info.nwk_depth + 0x30; // digital
        app_cmd_send(&depth, sizeof(uint8_t));
        app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
    }
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}

static void app_cmd_intra_parse(uint8_t *buf, uint8_t len)
{
    if (osel_memcmp(buf, CMD_INTRA_POWER_TYPE, sizeof(CMD_INTRA_POWER_TYPE) - 1))
    {
        app_cmd_intra_power_handle(buf + sizeof(CMD_INTRA_POWER_TYPE) - 1,
                                   len - (sizeof(CMD_INTRA_POWER_TYPE) - 1));
    }
    else if (osel_memcmp(buf, CMD_INTRA_CHANNEL_TYPE, sizeof(CMD_INTRA_CHANNEL_TYPE) - 1))
    {
        app_cmd_intra_channel_handle(buf + sizeof(CMD_INTRA_CHANNEL_TYPE) - 1,
                                     len - (sizeof(CMD_INTRA_CHANNEL_TYPE) - 1));
    }
    else if (osel_memcmp(buf, CMD_INTRA_ID_TYPE, sizeof(CMD_INTRA_ID_TYPE) - 1))
    {
        app_cmd_intra_id_handle(buf + sizeof(CMD_INTRA_ID_TYPE) - 1,
                                len - (sizeof(CMD_INTRA_ID_TYPE) - 1));
    }
    else if (osel_memcmp(buf, CMD_INTRA_NWK_DEPTH_TYPE, sizeof(CMD_INTRA_NWK_DEPTH_TYPE) - 1))
    {
        app_cmd_intra_nwk_depth_handle(buf + sizeof(CMD_INTRA_NWK_DEPTH_TYPE) - 1,
                                len - (sizeof(CMD_INTRA_NWK_DEPTH_TYPE) - 1));
    }
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}


/***************************************************************************/
static void app_cmd_exter_version_handle(uint8_t *buf, uint8_t len)
{
    device_info_t board_info = hal_board_info_get();

    if (len != 1)
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
        return;
    }

    app_cmd_send((uint8_t *)&version[0][0], sizeof(version) - 1);
    osel_memcpy(cmd_send_array, CMD_EXTER_VERSION_UNIQUE_ID,
                sizeof(CMD_EXTER_VERSION_UNIQUE_ID) - 1);
    uint8_t cmd_len = sizeof(CMD_EXTER_VERSION_UNIQUE_ID) - 1;
    uint8_t id_len = sizeof(board_info.device_id);
    for (uint8_t i = 0; i < id_len; i++)
    {
        hex_to_ascii(&cmd_send_array[cmd_len + i * 2], board_info.device_id[id_len - 1 - i]);
    }
    cmd_len += id_len * 2;

    app_cmd_send(cmd_send_array, cmd_len);
    app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
}

static void app_cmd_exter_restore_handle(uint8_t *buf, uint8_t len)
{
    if (len != 1)
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
        return;
    }

    hal_board_info_clr();
    app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
    hal_board_reset();
}

static void app_cmd_exter_restart_handle(uint8_t *buf, uint8_t len)
{
    if (len != 1)
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
        return;
    }

    app_cmd_send(CMD_OK, sizeof(CMD_OK) - 1);
    hal_board_reset();
}

uint8_t converthexchar(uint8_t ch)
{
    if ( (ch >= '0') && (ch <= '9'))
    {
        return (ch - 0x30);
    }
    else if ((ch >= 'A') && (ch <= 'F'))
    {
        return (ch - 'A' + 10);
    }
    else if ((ch >= 'a') && (ch <= 'f'))
    {
        return (ch - 'a' + 10);
    }
    else
    {
        return -1;
    }
}

static void auto_dev_id_set(uint8_t const * const buf)
{
    device_info_t device_info = hal_board_info_get();
    uint8_t buf_temp_1[16] = {0x00};
    uint8_t buf_temp_2[8] = {0x00};
    osel_memcpy(buf_temp_1, buf, 2*sizeof(uint64_t));
    
    for (uint8_t i = 0;i < 16;i++)
    {
        buf_temp_1[i] = converthexchar(buf[i]);
    }
       
    buf_temp_2[7] = (buf_temp_1[0] << 4) + buf_temp_1[1];
    buf_temp_2[6] = (buf_temp_1[2] << 4) + buf_temp_1[3];
    buf_temp_2[5] = (buf_temp_1[4] << 4) + buf_temp_1[5];
    buf_temp_2[4] = (buf_temp_1[6] << 4) + buf_temp_1[7];
    buf_temp_2[3] = (buf_temp_1[8] << 4) + buf_temp_1[9];
    buf_temp_2[2] = (buf_temp_1[10] << 4) + buf_temp_1[11];
    buf_temp_2[1] = (buf_temp_1[12] << 4) + buf_temp_1[13];
    buf_temp_2[0] = (buf_temp_1[14] << 4) + buf_temp_1[15];
    
    osel_memcpy(device_info.device_id, buf_temp_2, sizeof(uint64_t));
    
    osel_int_status_t s;
    OSEL_ENTER_CRITICAL(s);
    hal_board_info_save(&device_info, TRUE);
	serial_write(HAL_UART_1, AT_OK, sizeof(AT_OK) - 1);  
	OSEL_EXIT_CRITICAL(s);
}

static int8_t auto_device_id_cfg_parse(uint8_t const * const buf, uint8_t len)
{
    uint8_t offset = 0;           
    if(osel_memcmp((uint8_t *)&buf[offset], CMD_DEV_ID_TYPE, sizeof(CMD_DEV_ID_TYPE) - 1))
    {
        offset += sizeof(CMD_DEV_ID_TYPE)-1;
        if(!osel_memcmp((uint8_t *)&buf[offset], CMD_LINK_CHAR, sizeof(CMD_LINK_CHAR) - 1))
        {             
            return -1;
        }
        offset += sizeof(CMD_LINK_CHAR)-1;

        auto_dev_id_set((uint8_t *)&buf[offset]);   
        hal_board_reset();
        return TRUE;  
    }
    else
    {
        return -3;
    }    
}

static void app_cmd_exter_parse(uint8_t *buf, uint8_t len)
{
    if (osel_memcmp(buf, CMD_EXTER_VERSION_TYPE, sizeof(CMD_EXTER_VERSION_TYPE) - 1))
    {
        app_cmd_exter_version_handle(buf + sizeof(CMD_EXTER_VERSION_TYPE) - 1,
                                     len - (sizeof(CMD_EXTER_VERSION_TYPE) - 1));
    }
    else if (osel_memcmp(buf, CMD_EXTER_RESTORE_TYPE, sizeof(CMD_EXTER_RESTORE_TYPE) - 1))
    {
        app_cmd_exter_restore_handle(buf + sizeof(CMD_EXTER_RESTORE_TYPE) - 1,
                                     len - (sizeof(CMD_EXTER_RESTORE_TYPE) - 1));
    }
    else if (osel_memcmp(buf, CMD_EXTER_RESTART_TYPE, sizeof(CMD_EXTER_RESTART_TYPE) - 1))
    {
        app_cmd_exter_restart_handle(buf + sizeof(CMD_EXTER_RESTART_TYPE) - 1,
                                     len - (sizeof(CMD_EXTER_RESTART_TYPE) - 1));
    }
	else if(osel_memcmp(buf, CMD_DEV_ID_TYPE, sizeof(CMD_DEV_ID_TYPE) - 1))
	{
		auto_device_id_cfg_parse(buf, len - 1);
	}
    else
    {
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
    }
}

static void app_cmd_parse(uint8_t *buf, uint8_t len)
{
    switch (*buf)
    {
    case CMD_TEST_TYPE:
        app_cmd_test_parse();
        break;

    case CMD_INTRA_TYPE:
        app_cmd_intra_parse(buf + 1, len - 1);
        break;

    case CMD_EXTER_TYPE:
        app_cmd_exter_parse(buf + 1, len - 1);
        break;

    default:
        app_cmd_send(CMD_ERROR00, sizeof(CMD_ERROR00) - 1);
        break;
    }
}

void app_cmd_exter_recv_data_handle(uint8_t *buf, uint8_t len)
{
    uint8_t cmd_len = 0;
    if ((len * 2) > APP_CMD_SIZE_MAX)
    {
        return;
    }

    cmd_send_array[cmd_len] = 'A';
    cmd_len += sizeof('A');
    cmd_send_array[cmd_len] = 'T';
    cmd_len += sizeof('T');

    cmd_send_array[cmd_len] = CMD_EXTER_TYPE;
    cmd_len += sizeof(CMD_EXTER_TYPE);

    osel_memcpy(&cmd_send_array[cmd_len], CMD_EXTER_SEND_DATA_TYPE,
                sizeof(CMD_EXTER_SEND_DATA_TYPE));
    cmd_len += sizeof(CMD_EXTER_SEND_DATA_TYPE);

    cmd_send_array[cmd_len] = '=';
    cmd_len += sizeof('=');

    for (uint8_t i = 0; i < len; i++)
    {
        cmd_len += i * 2;
        hex_to_ascii(&cmd_send_array[cmd_len], buf[i]);
    }

    app_cmd_send(cmd_send_array, cmd_len);
}

void app_serial_recv_handler(void)
{
    uint8_t frame_len = 0;
    uint8_t read_data = 0;
    osel_memset(cmd_recv_array, 0x00, sizeof(cmd_recv_array));
    osel_memset(cmd_send_array, 0x00, sizeof(cmd_send_array));

    while (serial_read(SERIAL_1, &read_data, sizeof(uint8_t)))
    {
        cmd_recv_array[frame_len++] = read_data;
        if (read_data == CMD_CR)
        {
            break;
        }
    }

    uint8_t echo_array[] = "\r\n";
    serial_write(SERIAL_1, echo_array, sizeof(echo_array) - 1);

    if (frame_len > CMD_AT_HEAD_SIZE)
    {
        app_cmd_parse(&cmd_recv_array[CMD_AT_HEAD_SIZE], frame_len - CMD_AT_HEAD_SIZE);
    }
}

/******************************************************************************/
static void app_serial_cb(void)
{
    osel_post(APP_SERIAL_DATA_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void app_interface_config(void)
{
    serial_reg_t app_serial_reg;

    app_serial_reg.sd.valid = TRUE;
    app_serial_reg.sd.len = 2;
    app_serial_reg.sd.pos = 0;
    app_serial_reg.sd.data[0] = 'A';
    app_serial_reg.sd.data[1] = 'T';

    app_serial_reg.ld.valid = FALSE;

    app_serial_reg.argu.len_max = SERIAL_LEN_MAX;
    app_serial_reg.argu.len_min = SERIAL_LEN_MIN;

    app_serial_reg.ed.valid = TRUE;
    app_serial_reg.ed.len = 1;
    app_serial_reg.ed.data[0] = 0x0D;   // 'CR', enter

    app_serial_reg.echo_en = FALSE;
    app_serial_reg.func_ptr = app_serial_cb;

    serial_fsm_init(SERIAL_1);
    serial_reg(SERIAL_1, app_serial_reg);

    app_cmd_start_handle();
}

/******************************************************************************/


