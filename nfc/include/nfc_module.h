/***************************************************************************
* File        : nfc_module.h
* Summary     : 
* Version     : v0.1
* Author      : wangjifang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            wangjifang        first version
***************************************************************************/

#pragma once
#include <debug.h>
#include <hal_board.h>
#include <hal_nfc_driver.h>

//应用类型
#define APP_TYPE_ALG_PARA_CFG           0x11				//算法参数配置
#define APP_TYPE_ALG_PARA_CFG_ACK       0x81				//算法参数配置确认应答 
#define APP_TYPE_SENSER_OFFSET          0x12				//传感器偏移补偿
#define APP_TYPE_SENSER_OFFSET_ACK      0x82				//传感器偏移补偿确认应答 
#define APP_TYPE_BIND_INFO_CFG          0x13				//绑定物信息配置 
#define APP_TYPE_BIND_INFO_CFG_ACK      0x83				//绑定物信息配置应答
#define APP_TYPE_READ_DEV_LOG           0x14				//设备日志信息读取 
#define APP_TYPE_READ_DEV_LOG_ACK       0x84				//设备日志信息读取应答
#define APP_TYPE_DEVICE_INFO            0x15				//设备信息配置 
#define APP_TYPE_DEVICE_INFO_ACK        0x85				//设备信息配置应答
#define APP_TYPE_SSN_DEV_INFO           0x16				//
#define APP_TYPE_SSN_DEV_INFO_ACK       0x86				//

//参数类型
#define PARA_TYPE_OVERRUN_DETEC_ALG     0x01				//超限检测算法参数
#define PARA_TYPE_QUIETMOVE_DETEC_ALG   0x02				//静动检测算法参数 
#define PARA_TYPE_DROP_DETEC_ALG        0x03				//跌落检测算法参数 
#define PARA_TYPE_DUMP_DETEC_ALG        0x04				//倾倒检测算法参数  
#define PARA_TYPE_TURN_DETEC_ALG        0x05				//转向检测算法参数  
#define PARA_TYPE_PLUS_OFFSET           0x10				//加补偿 
#define PARA_TYPE_REDUCE_OFFSET         0x11				//减补偿  
#define PARA_TYPE_SYS_OFFSET            0x12				//系数补偿
#define PARA_TYPE_CARRIAGE_NUM          0x01				//车架号
#define PARA_TYPE_USER_NUM              0x02				//用户编号+订单号
#define PARA_TYPE_UNBIND_INFO           0xFF				//解除绑定

#define PARA_TYPE_DEVICE_NUI            0x00				//设备唯一标示
#define PARA_TYPE_BIND_INFO             0x01				//绑定信息
#define PARA_TYPE_SERVICE_INFO          0x02				//服务信息
#define PARA_TYPE_PROFILE_INFO          0x11				//profile信息
//SSN参数类型
#define PARA_TYPE_ALL_SSN_INFO          0x01				//所有参数配置 

//日志参数类型
#define TYPE_REG_DATA                   0x00u
#define TYPE_FORCE_RELEASE_DATA         0x01u
#define TYPE_OVERRUN_DATA               0x02u
#define TYPE_HB_DATA                    0x03u
#define TYPE_QUIET2MOVE_DATA            0x04u
#define TYPE_MOVE2QUIET_DATA            0x05u

//配置结果标示
#define RESULE_FALG_SUCCESS             0x00				//成功 
#define RESULE_FALG_FALL                0xFF				//失败  
#define RESULE_FALG_CRC_FALL            0xCC				//校验错误

//待修改，地址未确认
//存储数据的地址

#define OVERRUN_DETEC_ALG_ADDRESS       0x0060
#define QUIETMOVE_DETEC_ALG_ADDRESS     0x0090
#define DROP_DETEC_ALG_ADDRESS          0x00C0
#define DUMP_DETEC_ALG_ADDRESS          0x00F0
#define TURN_DETEC_ALG_ADDRESS          0x0120
#define PLUS_OFFSET_ADDRESS             0x0150
#define REDUCE_OFFSET_ADDRESS           0x0180
#define SYS_OFFSET_ADDRESS              0x01B0
#define CARRIAGE_NUM_ADDRESS            0x01E0
#define READ_DEV_LOG_ADDRESS            0x0210
#define SSN_DEV_ALL_INFO_ADDRESS    	0x0240
#define PROFILE_ID_ADDRESS    		    0x0270
#define USER_NUM_ADDRESS    		    0x02A0
#define UNBIND_INFO_ADDRESS    		    0x02D0
#define DEVICE_NUI_ADDRESS    		    0x0300
#define BIND_INFO_ADDRESS    		    0x0330
#define SERVICE_INFO_ADDRESS    	    0x0360
#define SERVICE_IP_ADDR_ADDRESS    	    0x0390

#define ACK_OVERRUN_DETEC_ALG_ADDRESS   0x0500
#define ACK_QUIETMOVE_DETEC_ALG_ADDRESS 0x0504
#define ACK_DROP_DETEC_ALG_ADDRESS      0x0508
#define ACK_DUMP_DETEC_ALG_ADDRESS      0x050C
#define ACK_TURN_DETEC_ALG_ADDRESS      0x0510
#define ACK_PLUS_OFFSET_ADDRESS         0x0514
#define ACK_REDUCE_OFFSET_ADDRESS       0x0518
#define ACK_SYS_OFFSET_ADDRESS          0x051C
#define ACK_BIND_INFO_CFG_ADDRESS       0x0520
#define ACK_READ_DEV_LOG_ADDRESS        0x0524
#define ACK_DEV_ALL_INFO_ADDRESS    	0x0544
#define ACK_USER_NUM_ADDRESS    	    0x0574
#define ACK_UNBIND_INFO_CFG_ADDRESS    	0x0578

#define MUTUAL_INFO_FALG_ADDRESS        0x0800          //存储信息标志地址

#define LOG_INFO_ADDRESS                0X0A20          //存储日志信息

//nfc帧协议
#define NFC_APP_TYPE_SIZE               (1u)				//应用类型域
#define NFC_PARA_TYPE_SIZE              (1u)				//参数（信息）类型域
#define NFC_VECTOR_DIM_SIZE             (1u)				//向量维度域
#define NFC_CFG_RESULT_SIZE             (1u)				//配置结果域
#define NFC_DATA_MAX                    (60u)				//最大nfc帧长
#define NFC_DATA_PRE                    (3u)				//nfc帧头长（头到参数向量维度）
#define CRC_LEN                         (1u)				//CRC域
#define PARA_SIZE                       (2u)				//单个参数长度
#define CARRIAGE_SIZE                   (17u)				//车架号17字节
#define SSN_ALL_PARA_SIZE               (10u)				//SSN所有参数长度
#define NFC_INFO_FLAG_SIZE              (32u)				//NFC标志位信息长度
#define NFC_INFO_PROFILE_ID_SIZE        (12u)				//PROFILE ID所有参数长度
#define NFC_INFO_NUI_SIZE               (8u)				//NUI长度

#define FIRST_START                     0xA5                //第一次启动标志
#define FIRST_START_ADDR                0x0A00

//info flag
#define FLAG_OVERRUN_DETEC_ALG          0u
#define FLAG_QUIETMOVE_DETEC_ALG        1u
#define FLAG_DROP_DETEC_ALG             2u
#define FLAG_DUMP_DETEC_ALG             3u
#define FLAG_TURN_DETEC_ALG             4u
#define FLAG_PLUS_OFFSET                5u
#define FLAG_REDUCE_OFFSET              6u
#define FLAG_SYS_OFFSET                 7u
#define FLAG_BING_INFO                  8u
#define FLAG_DEV_LOG                    9u
#define FLAG_SSN_PARA                   10u
#define FLAG_PROFILE_INFO               11u
#define FLAG_USER_NUM                   12u
#define FLAG_UNBIND_INFO                13u
typedef enum _result_t
{
    NFC_READ_SUCCESS = 0,
    NFC_READ_FALSE   = 1,
    NFC_READ_NULL    = 2,
    PARA_TYPE_FALSE  = 3,
    NFC_CRC_FALSE    = 4
} result_t;

/*****************4维参数内容******************/
typedef struct _para_vector4_t_
{
    uint16_t para_vector[4];
} para_vector4_t;

/*****************11维参数内容******************/
typedef struct _para_vector11_t_
{
    uint16_t para_vector[11];
} para_vector11_t;

/*****************3维参数内容******************/
typedef struct _para_vector14_t_
{
    uint16_t para_vector[14];
} para_vector14_t;

/*****************4维参数内容******************/
typedef struct _para_vector15_t_
{
    uint16_t para_vector[15];
} para_vector15_t;

/*****************算法参数配置******************/
typedef struct _nfc_alg_para_cfg_t_
{
    uint8_t para_type;                                      //参数类型
    uint8_t para_vector_dim;                                //参数向量维度
    union
    {                                                       //参数向量值
        para_vector4_t para_vector4;                        //4维内容
        para_vector11_t para_vector11;                      //11维内容
        para_vector14_t para_vector14;                      //14维内容
        para_vector15_t para_vector15;                      //15维内容
    };
} nfc_alg_para_cfg_t;

/*****************车架号***************/
typedef struct _carriage_info_t_
{
    uint8_t carriage_info[CARRIAGE_SIZE];                   //车架号
} carriage_info_t;

/*****************绑定物信息配置*****************/
typedef struct _nfc_bound_info_cfg_t_
{
    uint8_t info_type;                                      //信息类型
    union
    {                                                       //信息内容
        carriage_info_t carriage_info;                      //车架信息内容
    };
} nfc_bound_info_cfg_t;

/*****************设备日志信息*******************/
typedef struct _nfc_device_info_cfg_t_
{
    uint8_t info_type;                                      //信息类型
    uint8_t log_numb;                                       //日志数量
} nfc_device_info_cfg_t;

/*****************算法参数配置应答***************/
typedef struct _nfc_alg_para_cfg_ack_t_
{
    uint8_t info_type;                                      //参数类型
    uint8_t result;                                         //配置结果
} nfc_alg_para_cfg_ack_t;

/*****************设备绑定配置应答***************/
typedef struct _nfc_bound_info_cfg_ack_t_
{
	uint8_t info_type;                                      //参数类型
	uint8_t result;                                         //配置结果
} nfc_bound_info_cfg_ack_t;

typedef struct _nfc_device_info_log_t_
{
	uint8_t info_type;                                      //信息类型
	uint8_t log_num;                                        //日志数量
} nfc_device_info_log_t;

typedef struct _nfc_device_info_log_ack_t_
{
	uint8_t info_type;                                      //信息类型
	union
	{                                                       //信息内容
		carriage_info_t carriage_info;                      //车架信息内容
	};
} nfc_device_info_log_ack_t;
//增加ssn配置信息
typedef struct _nfc_ssn_info_t_
{
    uint8_t monitor_timer_len;//侦听时间长度
    uint8_t duty_ratio;//占空比
    uint8_t intra_ch[8];
} ssn_cfg_info_t;

typedef struct _nfc_device_ssn_info_t_
{
	uint8_t info_type;                                      //信息类型
	union
	{                                                       //信息内容
//		device_info_t device_info;                          
        ssn_cfg_info_t ssn_info;
	};
} nfc_device_ssn_info_t;

typedef struct _nfc_device_ssn_info_ack_t_
{
	uint8_t info_type;                                      //信息类型
    uint8_t result;
} nfc_device_ssn_info_ack_t;

typedef struct _nfc_device_profile_id_t_
{
    uint8_t profile_id[8];                                  //profile_id
    uint8_t version[4];                                     //版本号
} nfc_device_profile_info_t;

typedef struct _nfc_device_info_t_
{
	uint8_t info_type;                                      //信息类型
    union
	{                                                       //信息内容
		uint8_t     device_nui[NFC_INFO_NUI_SIZE];                          //NUI
        nfc_device_profile_info_t profile_info; 
	};
} nfc_device_info_t;

/*****************nfc数据内容*******************/
typedef struct _nfc_data_info_t_
{
    uint8_t app_type;
    union
    {
        nfc_alg_para_cfg_t          nfc_alg_para_pld;           //算法参数配置
        nfc_alg_para_cfg_ack_t      nfc_alg_para_cfg_ack;       //算法参数配置应答
        nfc_bound_info_cfg_t        nfc_bound_info_pld;         //绑定物信息配置
        nfc_bound_info_cfg_ack_t    nfc_bound_info_cfg_ack;     //绑定物信息配置应答
        nfc_device_info_log_t       nfc_device_info_log_pld;    //设备日志读取
        nfc_device_info_log_ack_t   nfc_device_info_log_ack;    //设备日志读取应答
        nfc_device_info_t           nfc_device_info_pld;        //设备信息
        nfc_device_ssn_info_t       nfc_device_ssn_info_pld;    //SSN设备信息
        nfc_device_ssn_info_ack_t   nfc_device_ssn_info_ack;    //SSN设备信息应答
    };
    uint8_t crc;
} nfc_data_info_t;

typedef struct _nfc_info_flag_t_
{
	uint32_t    overrun_detec_alg   :   1,
                quietmove_detec_alg :   1,
                drop_detec_alg      :   1,
                dump_detec_alg      :   1,
                turn_detec_alg      :   1,
                plus_offset         :   1,
                reduce_offset       :   1,
                sys_offset          :   1,
                carriage_num        :   1,
                read_dev_log        :   1,
                ssn_para            :   1,
                profile_id          :   1,
                user_num            :   1,
                unbind              :   1,
                reserved            :   18;
} nfc_info_flag_t;

typedef struct _nfc_ip_t_
{
    uint8_t ip[4]; 
    uint16_t port;
} nfc_ip_t;

#pragma pack(1)
typedef struct _nfc_log_info_t_
{
    uint8_t log_type; 
    uint32_t log_time;
} nfc_log_info_t;
#pragma pack()
/***********************************************/
/*****************接口函数**********************/
/***********************************************/
/**
* nfc模块初始化
*
* @param void
*
* @return void
*/
void nfc_init(void);

/**
* 读取nfc标志位信息
*
* @param nfc_info_flag          读取后数据存放的地址
*
* @return void
*/
bool_t nfc_read_info_flag(nfc_info_flag_t *nfc_info_flag);

/**
* 写nfc模块数据
*
* @param nfc_data:     需要写的数据内容
*
* @return void
*/
uint8_t nfc_write_info(const nfc_data_info_t *const nfc_data);

/**
* 读nfc模块数据信息
*
* @param nfc_data:     读出来的数据内容存放的地方
* @param nfc_app_type: 要读的数据是什么应用类型
* @param para_type:    要读的数据是什么应用类型子类型
*
* @return TRUE or FALSE
*/
result_t nfc_read_info(nfc_data_info_t *const nfc_data, uint8_t nfc_app_type, 
                     uint8_t para_type);

/**
* 清除nfc模块数据信息
*
* @param nfc_app_type: 要清除的数据是什么应用类型
* @param para_type:    要清除的数据是什么应用类型子类型
*
* @return TRUE or FALSE
*/
result_t nfc_clr_info(uint8_t nfc_app_type, uint8_t para_type);

/**
* 清除nfc模块标志位数据信息
*
* @param flag_bit: 要清除的数据是什么应用类型
*
* @return TRUE or FALSE
*/
bool_t nfc_clr_info_flag(uint8_t flag_bit);

/**
* 从NFC中读取IP信息
*
* @param nfc_ip: IP地址
*
* @return TRUE or FALSE
*/
bool_t nfc_read_ip(nfc_ip_t *nfc_ip);

/**
* 向NFC存储日志
*
* @param nfc_log_type: 要存储的数据是什么类型
*
* @return 成功或失败
*/
result_t nfc_save_log_info(uint8_t nfc_log_type);