/***************************************************************************
* File        : hal_nfc_driver.h
* Summary     : 
* Version     : v0.1
* Author      : wangjifang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            wangjifang        first version
***************************************************************************/

#pragma once
#include <debug.h>


/**
 * i2c向NFC连续读数据
 *
 * @param data_buf  读出来的数据存放的地址
 * @param data_len  读出来的数据长度
 * @param word_addr 从NFC的哪个地址读数据
 *
 * @return void
 */
bool_t hal_read_nfc_data(uint8_t *const data_buf, 
                      uint8_t const data_len, 
                      uint16_t const word_addr);

/**
 * i2c向NFC连续写同个一个扇区的数据
 *
 * @param data_buf  读出来的数据存放的地址
 * @param data_len  读出来的数据长度
 * @param word_addr 从NFC的哪个地址读数据
 *
 * @return void
 */
bool_t hal_write_nfc_area_data(uint8_t *const data_buf, 
                            uint8_t const data_len, 
                            uint16_t const word_addr);

/**
 * i2c向NFC连续写数据，可同可不同个扇区
 *
 * @param data_buf  读出来的数据存放的地址
 * @param data_len  读出来的数据长度
 * @param word_addr 从NFC的哪个地址读数据
 *
 * @return void
 */
bool_t hal_write_nfc_data(uint8_t *const data_buf,
                       uint8_t const data_len,
                       uint16_t const word_addr);