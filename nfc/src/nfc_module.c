/***************************************************************************
* File        : nfc_module.c
* Summary     : 
* Version     : v0.1
* Author      : wangjifang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            wangjifang        first version
***************************************************************************/
#include <nfc_module.h>
#include <nfc_driver.h>
#include <osel_arch.h>
#include <debug.h>
#include <sbuf.h>
#include <hal_timer.h>

#define CLR_DATA_LEN                        48u
#define LOG_FLAG_LEN                        6u
#define LOG_INFO_FLAG                       0X0A10          //存储日志标志信息
#define REG_DATA_LEN                        5u
#define FORCE_RELEASE_DATA_LEN              5u
#define OVERRUN_DATA_LEN                    20u
#define HB_DATA_LEN                         50u
#define QUIET2MOVE_DATA_LEN                 50u
#define MOVE2QUIET_DATA_LEN                 50u

uint8_t clr_data_buf[CLR_DATA_LEN];
uint8_t log_flag_buf[LOG_FLAG_LEN];


/***
*** 设备是否第一次启动
***/
static bool_t is_nfc_fist_start(void)
{
    bool_t result = FALSE;
    uint8_t start_flag = 0;
    hal_read_nfc_data(&start_flag, 1, FIRST_START_ADDR);
    if(start_flag != FIRST_START)
    {
        result = TRUE;
    }
    return result;
}

/***
*** 参数配置数据拷贝
***/
static result_t nfc_para_cfg_data_copy(nfc_data_info_t *const nfc_data, uint8_t *data_p)
{
    uint8_t vector_dim = 0;
    result_t result = NFC_READ_SUCCESS;
    nfc_data->app_type = *data_p ++;
    if(nfc_data->app_type == 0x00)
    {
        result = NFC_READ_NULL;
        return result;
    }
    nfc_data->nfc_alg_para_pld.para_type = *data_p ++;
    nfc_data->nfc_alg_para_pld.para_vector_dim = *data_p ++;
    vector_dim = nfc_data->nfc_alg_para_pld.para_vector_dim;
    switch(vector_dim)
    {
    case 4:                                         //算法参数4维
        osel_memcpy((uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector4.para_vector),
                    data_p, PARA_SIZE*vector_dim);
        data_p += PARA_SIZE*vector_dim;
        break;
        
    case 11:                                         //算法参数11维
        osel_memcpy((uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector11.para_vector),
                    data_p, PARA_SIZE*vector_dim);
        data_p += PARA_SIZE*vector_dim;
        break;
        
    case 14:                                         //算法参数14维
        osel_memcpy((uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector14.para_vector),
                    data_p, PARA_SIZE*vector_dim);
        data_p += PARA_SIZE*vector_dim;
        break;
        
    case 15:                                         //算法参数15维
        osel_memcpy((uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector15.para_vector),
                    data_p, PARA_SIZE*vector_dim);
        data_p += PARA_SIZE*vector_dim;
        break;
        
    default:
        break;
    }
    nfc_data->crc = *data_p++;
    return result;
}

/***
*** 读算法参数
***/
static result_t read_nfc_alg_para(nfc_data_info_t *const nfc_data, uint8_t para_type)
{
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    uint8_t nfc_data_temp[NFC_DATA_MAX];
    uint8_t *nfc_data_temp_p = nfc_data_temp;
    uint8_t vector_dim;
    uint8_t nfc_data_len;
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_OVERRUN_DETEC_ALG:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      OVERRUN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        DBG_ASSERT((1 <= vector_dim && vector_dim <= 15) __DBG_LINE);
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len, 
                                      OVERRUN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;

    case PARA_TYPE_QUIETMOVE_DETEC_ALG:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      QUIETMOVE_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        DBG_ASSERT((1 <= vector_dim && vector_dim <= 15) __DBG_LINE);
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      QUIETMOVE_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    case PARA_TYPE_DROP_DETEC_ALG:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      DROP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        DBG_ASSERT((1 <= vector_dim && vector_dim <= 15) __DBG_LINE);
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len, 
                                      DROP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    case PARA_TYPE_DUMP_DETEC_ALG:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      DUMP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        DBG_ASSERT((1 <= vector_dim && vector_dim <= 15) __DBG_LINE);
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len, 
                                      DUMP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    case PARA_TYPE_TURN_DETEC_ALG:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      TURN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        DBG_ASSERT((1 <= vector_dim && vector_dim <= 15) __DBG_LINE);
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      TURN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        break;
    }
    
    if(nfc_data_temp[nfc_data_len-1] == crc_compute(nfc_data_temp, nfc_data_len-1))
    {
        read_result = nfc_para_cfg_data_copy(nfc_data, nfc_data_temp_p);
    }
    else
    {
        read_result = NFC_CRC_FALSE;
    }
    return read_result;
}

/***
*** 读传感器偏移
***/
static result_t read_nfc_senser_offset(nfc_data_info_t *const nfc_data, uint8_t para_type)
{
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    uint8_t nfc_data_temp[NFC_DATA_MAX];
    uint8_t *nfc_data_temp_p = nfc_data_temp;
    uint8_t vector_dim;
    uint8_t nfc_data_len;
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_PLUS_OFFSET:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      PLUS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      PLUS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    case PARA_TYPE_REDUCE_OFFSET:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      REDUCE_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      REDUCE_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    case PARA_TYPE_SYS_OFFSET:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, NFC_DATA_PRE,
                                      SYS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        vector_dim = nfc_data_temp[2];
        nfc_data_len = NFC_DATA_PRE + PARA_SIZE*vector_dim + CRC_LEN;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      SYS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    if(nfc_data_temp[nfc_data_len-1] == crc_compute(nfc_data_temp, nfc_data_len-1))
    {
        read_result = nfc_para_cfg_data_copy(nfc_data, nfc_data_temp_p);
    }
    else
    {
        read_result = NFC_CRC_FALSE;
    }
    return read_result;   
}

/***
*** 读绑定信息
***/
static result_t read_nfc_bing_info(nfc_data_info_t *const nfc_data, uint8_t para_type)
{
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    result_t read_result = NFC_READ_SUCCESS;
    uint8_t nfc_data_temp[NFC_DATA_MAX];
    uint8_t *nfc_data_temp_p = nfc_data_temp;
    uint8_t nfc_data_len = NFC_APP_TYPE_SIZE + NFC_PARA_TYPE_SIZE + CRC_LEN;
    
    switch(para_type)
    {
    case PARA_TYPE_CARRIAGE_NUM:
        nfc_data_len += CARRIAGE_SIZE;
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      CARRIAGE_NUM_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        if(nfc_data_temp[nfc_data_len-1] != 
           crc_compute(nfc_data_temp, nfc_data_len-1))
        {
            read_result = NFC_CRC_FALSE;
            return read_result;
        } 
        nfc_data->app_type = *nfc_data_temp_p ++;
        if(nfc_data->app_type == 0x00)
        {
            read_result = NFC_READ_NULL;
            return read_result;
        }
        nfc_data->nfc_bound_info_pld.info_type = *nfc_data_temp_p ++;
        
        osel_memcpy((uint8_t *)(nfc_data->nfc_bound_info_pld.carriage_info.carriage_info),
                    nfc_data_temp_p, CARRIAGE_SIZE);
        nfc_data_temp_p += CARRIAGE_SIZE;
        
        nfc_data->crc = *nfc_data_temp_p++;
        break;
        
    case PARA_TYPE_USER_NUM:
        //功能待定
        break;
        
    case PARA_TYPE_UNBIND_INFO:
        if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      UNBIND_INFO_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
            return read_result;
        }
        if(nfc_data_temp[nfc_data_len-1] != 
           crc_compute(nfc_data_temp, nfc_data_len-1))
        {
            read_result = NFC_CRC_FALSE;
            return read_result;
        } 
        nfc_data->app_type = *nfc_data_temp_p ++;
        if(nfc_data->app_type == 0x00)
        {
            read_result = NFC_READ_NULL;
            return read_result;
        }
        nfc_data->nfc_bound_info_pld.info_type = *nfc_data_temp_p ++;   
        nfc_data->crc = *nfc_data_temp_p++;
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return read_result;
}

/***
*** 读设备ssn参数
***/
static result_t read_nfc_dev_ssn_para(nfc_data_info_t *const nfc_data, uint8_t para_type)
{
	DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	result_t read_result = NFC_READ_SUCCESS;
	uint8_t nfc_data_temp[NFC_DATA_MAX];
    uint8_t *nfc_data_temp_p = nfc_data_temp;
	uint8_t nfc_data_len = NFC_APP_TYPE_SIZE + NFC_PARA_TYPE_SIZE + CRC_LEN;  //不算载荷长度
    
	switch(para_type)
	{
	case PARA_TYPE_ALL_SSN_INFO:
		nfc_data_len += SSN_ALL_PARA_SIZE;
		if(ERROR == hal_read_nfc_data(nfc_data_temp, nfc_data_len,
                                      SSN_DEV_ALL_INFO_ADDRESS))
		{
			read_result = NFC_READ_FALSE;
			return read_result;
		}
		break;
        
	default:
        read_result = PARA_TYPE_FALSE;
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	if(nfc_data_temp[nfc_data_len-1] == crc_compute(nfc_data_temp, nfc_data_len-1))
	{
        nfc_data->app_type = *nfc_data_temp_p ++;
        if(nfc_data->app_type == 0x00)
        {
            read_result =NFC_READ_NULL;
            return read_result;
        }
        nfc_data->nfc_device_ssn_info_pld.info_type = *nfc_data_temp_p ++;
        nfc_data->nfc_device_ssn_info_pld.ssn_info.monitor_timer_len = *nfc_data_temp_p ++;
        nfc_data->nfc_device_ssn_info_pld.ssn_info.duty_ratio = *nfc_data_temp_p ++;
        osel_memcpy((uint8_t *)(nfc_data->nfc_device_ssn_info_pld.ssn_info.intra_ch),
                    nfc_data_temp_p, 8);
        nfc_data_temp_p += 8;        
               
//        osel_memcpy((uint8_t *)(nfc_data->nfc_device_ssn_info_pld.device_info.intra_ch),
//                    nfc_data_temp_p, 8);
//        nfc_data_temp_p += 8;
//        osel_memcpy((uint8_t *)(nfc_data->nfc_device_ssn_info_pld.device_info.exter_ch),
//                    nfc_data_temp_p, 8);
//        nfc_data_temp_p += 8;
//        nfc_data->nfc_device_ssn_info_pld.device_info.power_sn =
//            *nfc_data_temp_p++;
//        nfc_data->nfc_device_ssn_info_pld.device_info.intra_ch_cnt =
//            *nfc_data_temp_p++;
//        nfc_data->nfc_device_ssn_info_pld.device_info.intra_data_simu =
//            *nfc_data_temp_p++;
//        osel_memcpy((uint8_t *)&(nfc_data->nfc_device_ssn_info_pld.device_info.heartbeat_cycle),
//                    nfc_data_temp_p, 4);
//        nfc_data_temp_p += 4;
        
        nfc_data->crc = *nfc_data_temp_p++;
        
        nfc_data_len = nfc_data->crc;
	}
    else
    {
        read_result = NFC_CRC_FALSE;
    }
	return read_result;
}

/***
*** 读nfc信息接口函数
***/
result_t nfc_read_info(nfc_data_info_t *const nfc_data,
                     uint8_t nfc_app_type, uint8_t para_type)
{
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    result_t result = NFC_READ_SUCCESS;
    
    switch (nfc_app_type)
    {
    case APP_TYPE_ALG_PARA_CFG:
        result = read_nfc_alg_para(nfc_data, para_type);
        break;
        
    case APP_TYPE_SENSER_OFFSET:
        result = read_nfc_senser_offset(nfc_data, para_type);
        break;
        
    case APP_TYPE_BIND_INFO_CFG:
        result = read_nfc_bing_info(nfc_data, para_type);
        break;
        
    case APP_TYPE_READ_DEV_LOG:
        //手机读设备日志
        break;
        
	case APP_TYPE_DEVICE_INFO:
		//手机读设备信息;
		break;
        
    case APP_TYPE_SSN_DEV_INFO:
		result = read_nfc_dev_ssn_para(nfc_data, para_type);
		break;
        
    default:
        break;
    }
    return result;
}

/***
*** 获取算法参数地址
***/
static uint16_t get_alg_para_cfg_addr(uint8_t info_type)
{
    uint16_t nfc_ack_data_addr = 0x0000;
    
    switch(info_type)
    {
    case PARA_TYPE_OVERRUN_DETEC_ALG:
        nfc_ack_data_addr = OVERRUN_DETEC_ALG_ADDRESS;
        break;
        
    case PARA_TYPE_QUIETMOVE_DETEC_ALG:
        nfc_ack_data_addr = QUIETMOVE_DETEC_ALG_ADDRESS;
        break;
        
    case PARA_TYPE_DROP_DETEC_ALG:
        nfc_ack_data_addr = DROP_DETEC_ALG_ADDRESS;
        break;
        
    case PARA_TYPE_DUMP_DETEC_ALG:
        nfc_ack_data_addr = DUMP_DETEC_ALG_ADDRESS;
        break;
        
    case PARA_TYPE_TURN_DETEC_ALG:
        nfc_ack_data_addr = TURN_DETEC_ALG_ADDRESS;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return nfc_ack_data_addr;
}

/***
*** 获取算法参数ack地址
***/
static uint16_t get_alg_para_cfg_ack_addr(uint8_t info_type)
{
	uint16_t nfc_ack_data_addr = 0x0000;
    
	switch(info_type)
	{
	case PARA_TYPE_OVERRUN_DETEC_ALG:
		nfc_ack_data_addr = ACK_OVERRUN_DETEC_ALG_ADDRESS;
		break;
        
	case PARA_TYPE_QUIETMOVE_DETEC_ALG:
		nfc_ack_data_addr = ACK_QUIETMOVE_DETEC_ALG_ADDRESS;
		break;
        
	case PARA_TYPE_DROP_DETEC_ALG:
		nfc_ack_data_addr = ACK_DROP_DETEC_ALG_ADDRESS;
		break;
        
	case PARA_TYPE_DUMP_DETEC_ALG:
		nfc_ack_data_addr = ACK_DUMP_DETEC_ALG_ADDRESS;
		break;
        
	case PARA_TYPE_TURN_DETEC_ALG:
		nfc_ack_data_addr = ACK_TURN_DETEC_ALG_ADDRESS;
		break;
        
	default:
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	return nfc_ack_data_addr;
}

/***
*** 获取传感器偏移地址
***/
static uint16_t get_senser_offset_addr(uint8_t info_type)
{
    uint16_t nfc_ack_data_addr = 0x0000;
    
    switch(info_type)
    {
    case PARA_TYPE_PLUS_OFFSET:
        nfc_ack_data_addr = PLUS_OFFSET_ADDRESS;
        break;
        
    case PARA_TYPE_REDUCE_OFFSET:
        nfc_ack_data_addr = REDUCE_OFFSET_ADDRESS;
        break;
        
    case PARA_TYPE_SYS_OFFSET:
        nfc_ack_data_addr = SYS_OFFSET_ADDRESS;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return nfc_ack_data_addr;
}

/***
*** 获取传感器偏移ack地址
***/
static uint16_t get_senser_offset_ack_addr(uint8_t info_type)
{
	uint16_t nfc_ack_data_addr = 0x0000;
    
	switch(info_type)
	{
	case PARA_TYPE_PLUS_OFFSET:
		nfc_ack_data_addr = ACK_PLUS_OFFSET_ADDRESS;
		break;
        
	case PARA_TYPE_REDUCE_OFFSET:
		nfc_ack_data_addr = ACK_REDUCE_OFFSET_ADDRESS;
		break;
        
	case PARA_TYPE_SYS_OFFSET:
		nfc_ack_data_addr = ACK_SYS_OFFSET_ADDRESS;
		break;
        
	default:
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	return nfc_ack_data_addr;
}

/***
*** 获取绑定信息配置地址
***/
static uint16_t get_bind_info_cfg_addr(uint8_t info_type)
{
	uint16_t nfc_ack_data_addr = 0x0000;
    
	switch(info_type)
	{
	case PARA_TYPE_CARRIAGE_NUM:
		nfc_ack_data_addr = CARRIAGE_NUM_ADDRESS;
		break;
        
	default:
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	return nfc_ack_data_addr;
}

/***
*** 获取读设备日志ack地址
***/
static uint16_t get_read_dev_log_ack_addr(uint8_t info_type)
{
	uint16_t nfc_ack_data_addr = 0x0000;
    
	switch(info_type)
	{
	case PARA_TYPE_CARRIAGE_NUM:
		nfc_ack_data_addr = ACK_READ_DEV_LOG_ADDRESS;
		break;
        
	default:
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	return nfc_ack_data_addr;
}

/***
*** 获取读设备配置信息ack地址
***/
static uint16_t get_read_dev_info_ack_addr(uint8_t info_type)
{
	uint16_t nfc_ack_data_addr = 0x0000;
    
	switch(info_type)
	{
	case PARA_TYPE_DEVICE_NUI:
		nfc_ack_data_addr = DEVICE_NUI_ADDRESS;
		break;
    
    case PARA_TYPE_BIND_INFO:
		nfc_ack_data_addr = BIND_INFO_ADDRESS;
		break;
        
    case PARA_TYPE_SERVICE_INFO:
		nfc_ack_data_addr = SERVICE_INFO_ADDRESS;
		break;
        
    case PARA_TYPE_PROFILE_INFO:
		nfc_ack_data_addr = PROFILE_ID_ADDRESS;
		break;
        
	default:
		DBG_ASSERT(FALSE __DBG_LINE);
		break;
	}
	return nfc_ack_data_addr;
}

/***
*** 获取读绑定信息配置ack地址
***/
static uint16_t get_bind_info_cfg_ack_addr(uint8_t info_type)
{
    uint16_t nfc_ack_data_addr = 0x0000;
    
    switch(info_type)
    {
    case PARA_TYPE_CARRIAGE_NUM:
        nfc_ack_data_addr = ACK_BIND_INFO_CFG_ADDRESS;
        break;
        
    case PARA_TYPE_USER_NUM:
        nfc_ack_data_addr = ACK_USER_NUM_ADDRESS;
        break;
        
    case PARA_TYPE_UNBIND_INFO:
        nfc_ack_data_addr = ACK_UNBIND_INFO_CFG_ADDRESS;
        break;

    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return nfc_ack_data_addr;
}

/***
*** 获取SSN设备配置信息地址
***/
static uint16_t get_ssn_dev_info_cfg_addr(uint8_t info_type)
{
    uint16_t nfc_data_addr = 0x0000;
    
    switch(info_type)
    {
    case PARA_TYPE_ALL_SSN_INFO:
        nfc_data_addr = SSN_DEV_ALL_INFO_ADDRESS;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return nfc_data_addr;
}

/***
*** 获取SSN设备配置信息ack地址
***/
static uint16_t get_ssn_dev_info_cfg_ack_addr(uint8_t info_type)
{
    uint16_t nfc_ack_data_addr = 0x0000;
    
    switch(info_type)
    {
    case PARA_TYPE_ALL_SSN_INFO:
        nfc_ack_data_addr = ACK_DEV_ALL_INFO_ADDRESS;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return nfc_ack_data_addr;
}

/***
*** nfc参数配置拷贝
***/
static uint8_t nfc_data_alg_para_cfg_copy(uint8_t *nfc_data_temp_p, 
                                          const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    uint8_t vector_temp = nfc_data->nfc_alg_para_pld.para_vector_dim;
    uint8_t *head_p = nfc_data_temp_p;
    *nfc_data_temp_p = nfc_data->nfc_alg_para_pld.para_type;
    nfc_data_temp_p++;
    *nfc_data_temp_p = vector_temp;
    nfc_data_temp_p++;
    
    switch(vector_temp)
    {
    case 4:
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector4.para_vector),
                    PARA_SIZE * vector_temp);
        nfc_data_temp_p += PARA_SIZE * vector_temp;
        break;
        
    case 11:
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector11.para_vector),
                    PARA_SIZE * vector_temp);
        nfc_data_temp_p += PARA_SIZE * vector_temp;
        break;
        
    case 14:
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector14.para_vector),
                    PARA_SIZE * vector_temp);
        nfc_data_temp_p += PARA_SIZE * vector_temp;
        break;
        
    case 15:
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)(nfc_data->nfc_alg_para_pld.para_vector15.para_vector),
                    PARA_SIZE * vector_temp);
        nfc_data_temp_p += PARA_SIZE * vector_temp;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return (nfc_data_temp_p - head_p);
}

/***
*** nfc参数配置ack拷贝
***/
static uint8_t nfc_data_alg_para_cfg_ack_copy(uint8_t *nfc_data_temp_p, 
                                              const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
    *nfc_data_temp_p = nfc_data->nfc_alg_para_cfg_ack.info_type;
    nfc_data_temp_p++;
    *nfc_data_temp_p = nfc_data->nfc_alg_para_cfg_ack.result;
    nfc_data_temp_p++;
    return (nfc_data_temp_p - head_p);
}

/***
*** nfc传感器偏移补偿拷贝
***/
static uint8_t nfc_data_senser_offset_ack_copy(uint8_t *nfc_data_temp_p, 
                                               const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_alg_para_cfg_ack.info_type;
	nfc_data_temp_p++;
	*nfc_data_temp_p = nfc_data->nfc_alg_para_cfg_ack.result;
	nfc_data_temp_p++;
	return (nfc_data_temp_p - head_p);
}

/***
*** 绑定信息配置ack拷贝
***/
static uint8_t nfc_data_bind_info_cfg_ack_copy(uint8_t *nfc_data_temp_p, 
                                               const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_bound_info_cfg_ack.info_type;
	nfc_data_temp_p++;
	*nfc_data_temp_p = nfc_data->nfc_bound_info_cfg_ack.result;
	nfc_data_temp_p++;
	return (nfc_data_temp_p - head_p);
}

/***
*** 读设备日志ack拷贝
***/
static uint8_t nfc_data_read_dev_log_ack_copy(uint8_t *nfc_data_temp_p, 
                                              const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_device_info_log_ack.info_type;
	nfc_data_temp_p++;
	osel_memcpy(nfc_data_temp_p, 
                (uint8_t *)(nfc_data->nfc_device_info_log_ack.carriage_info.carriage_info),
                CARRIAGE_SIZE);
	nfc_data_temp_p += CARRIAGE_SIZE;
	return (nfc_data_temp_p - head_p);
}

/***
*** 读设备信息ack拷贝
***/
static uint8_t nfc_data_read_dev_info_ack_copy(uint8_t *nfc_data_temp_p, 
                                              const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_device_info_pld.info_type;
	nfc_data_temp_p++;
    switch(*(nfc_data_temp_p-1))
	{
    case PARA_TYPE_DEVICE_NUI:
        osel_memcpy(nfc_data_temp_p, 
                (uint8_t *)(nfc_data->nfc_device_info_pld.device_nui), 
                NFC_INFO_NUI_SIZE);
        nfc_data_temp_p += NFC_INFO_NUI_SIZE;
        break;
        
    case PARA_TYPE_BIND_INFO:
        break;
        
    case PARA_TYPE_SERVICE_INFO:
        break;
        
	case PARA_TYPE_PROFILE_INFO:
        osel_memcpy(nfc_data_temp_p, 
                (uint8_t *)(nfc_data->nfc_device_info_pld.profile_info.profile_id), 
                8);
        nfc_data_temp_p += 8;
        osel_memcpy(nfc_data_temp_p, 
                (uint8_t *)(nfc_data->nfc_device_info_pld.profile_info.version), 
                4);
        nfc_data_temp_p += 4;
        break;
        
    default:
        break;
    }
    return (nfc_data_temp_p - head_p);
}

/***
*** 设备SSN信息拷贝
***/
static uint8_t nfc_data_ssn_dev_info_copy(uint8_t *nfc_data_temp_p,
                                          const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.info_type;
	nfc_data_temp_p++;
	switch(*(nfc_data_temp_p-1))
	{
    case PARA_TYPE_ALL_SSN_INFO:   
        *nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.ssn_info.monitor_timer_len;
        nfc_data_temp_p++;
        *nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.ssn_info.duty_ratio;
        nfc_data_temp_p++; 
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)(nfc_data->nfc_device_ssn_info_pld.ssn_info.intra_ch),
                    8);
        nfc_data_temp_p += 8; 
        
        
//        osel_memcpy(nfc_data_temp_p, 
//                    (uint8_t *)(nfc_data->nfc_device_ssn_info_pld.device_info.intra_ch),
//                    8);
//        nfc_data_temp_p += 8;
//        osel_memcpy(nfc_data_temp_p, 
//                    (uint8_t *)(nfc_data->nfc_device_ssn_info_pld.device_info.exter_ch),
//                    8);
//        nfc_data_temp_p += 8;
//        
//        *nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.device_info.power_sn;
//        nfc_data_temp_p++;
//        
//        *nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.device_info.intra_ch_cnt;
//        nfc_data_temp_p++;
//        
//        *nfc_data_temp_p = nfc_data->nfc_device_ssn_info_pld.device_info.intra_data_simu;
//        nfc_data_temp_p++;
//        
//        osel_memcpy(nfc_data_temp_p, 
//                    (uint8_t *)&(nfc_data->nfc_device_ssn_info_pld.device_info.heartbeat_cycle),
//                    4);
//        nfc_data_temp_p += 4;
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
	}
	
	return (nfc_data_temp_p - head_p);
}

/***
*** 设备SSN信息ack拷贝
***/
static uint8_t nfc_data_ssn_dev_info_ack_copy(uint8_t *nfc_data_temp_p,
                                              const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
	uint8_t *head_p = nfc_data_temp_p;
	*nfc_data_temp_p = nfc_data->nfc_device_ssn_info_ack.info_type;
	nfc_data_temp_p++;
	*nfc_data_temp_p = nfc_data->nfc_device_ssn_info_ack.result;
	nfc_data_temp_p++;
	return (nfc_data_temp_p - head_p);
}

/***
*** 设备绑定信息拷贝
***/
static uint8_t nfc_data_bind_info_copy(uint8_t *nfc_data_temp_p, 
                                       const nfc_data_info_t *nfc_data)
{
    DBG_ASSERT(nfc_data_temp_p != NULL __DBG_LINE);
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    uint8_t *head_p = nfc_data_temp_p;
    *nfc_data_temp_p = nfc_data->nfc_bound_info_pld.info_type;
    nfc_data_temp_p++;
    
    switch(nfc_data->nfc_bound_info_pld.info_type)
    {
    case APP_TYPE_BIND_INFO_CFG:
        osel_memcpy(nfc_data_temp_p, 
                    (uint8_t *)&(nfc_data->nfc_bound_info_pld.carriage_info),
                    sizeof(nfc_data->nfc_bound_info_pld.carriage_info));
        nfc_data_temp_p += sizeof(nfc_data->nfc_bound_info_pld.carriage_info);
        break;
        
    default:
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return (nfc_data_temp_p - head_p);
}

/***
*** nfc写设备信息
***/
uint8_t nfc_write_info(const nfc_data_info_t *const nfc_data)
{
    DBG_ASSERT(nfc_data != NULL __DBG_LINE);
    
    uint16_t nfc_info_addr = 0x0000;
    uint8_t nfc_data_temp[NFC_DATA_MAX];
    uint8_t *nfc_data_temp_p = nfc_data_temp;
    *nfc_data_temp_p = nfc_data->app_type;
    nfc_data_temp_p++;
    
    switch (nfc_data->app_type)
    {
    case APP_TYPE_ALG_PARA_CFG:
        nfc_data_temp_p += nfc_data_alg_para_cfg_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_alg_para_cfg_addr(nfc_data->nfc_alg_para_pld.para_type);
        break;
        
    case APP_TYPE_ALG_PARA_CFG_ACK:
        nfc_data_temp_p += nfc_data_alg_para_cfg_ack_copy(nfc_data_temp_p, 
                                                          nfc_data);
        nfc_info_addr = 
            get_alg_para_cfg_ack_addr(nfc_data->nfc_alg_para_cfg_ack.info_type);
        break;
        
    case APP_TYPE_SENSER_OFFSET:
        nfc_data_temp_p += nfc_data_alg_para_cfg_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_senser_offset_addr(nfc_data->nfc_alg_para_pld.para_type);
        break;
        
    case APP_TYPE_SENSER_OFFSET_ACK:
        nfc_data_temp_p += 
            nfc_data_senser_offset_ack_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_senser_offset_ack_addr(nfc_data->nfc_alg_para_cfg_ack.info_type);
        break;
        
    case APP_TYPE_BIND_INFO_CFG:
        nfc_data_temp_p += nfc_data_bind_info_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_bind_info_cfg_addr(nfc_data->nfc_bound_info_pld.info_type);
        break;
        
    case APP_TYPE_BIND_INFO_CFG_ACK:
        nfc_data_temp_p += 
            nfc_data_bind_info_cfg_ack_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_bind_info_cfg_ack_addr(nfc_data->nfc_bound_info_cfg_ack.info_type);
        break;
        
    case APP_TYPE_READ_DEV_LOG_ACK:
        nfc_data_temp_p += 
            nfc_data_read_dev_log_ack_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_read_dev_log_ack_addr(nfc_data->nfc_device_info_log_ack.info_type);
        break;
        
    case APP_TYPE_DEVICE_INFO_ACK:
        nfc_data_temp_p += 
            nfc_data_read_dev_info_ack_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_read_dev_info_ack_addr(nfc_data->nfc_device_info_log_ack.info_type);
        break;
        
    case APP_TYPE_SSN_DEV_INFO:
        nfc_data_temp_p += nfc_data_ssn_dev_info_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_ssn_dev_info_cfg_addr(nfc_data->nfc_device_ssn_info_pld.info_type);
        break;
        
    case APP_TYPE_SSN_DEV_INFO_ACK:
        nfc_data_temp_p += 
            nfc_data_ssn_dev_info_ack_copy(nfc_data_temp_p, nfc_data);
        nfc_info_addr = 
            get_ssn_dev_info_cfg_ack_addr(nfc_data->nfc_device_ssn_info_pld.info_type);
        break;
        
    default:
        break;
    }
    *nfc_data_temp_p = crc_compute(nfc_data_temp, nfc_data_temp_p - nfc_data_temp);
    nfc_data_temp_p++;
    return hal_write_nfc_data(nfc_data_temp, nfc_data_temp_p - nfc_data_temp, 
                       nfc_info_addr);
}

/***
*** 清除算法参数信息
***/
static result_t clr_nfc_alg_para(uint8_t para_type)
{
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_OVERRUN_DETEC_ALG:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       OVERRUN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_QUIETMOVE_DETEC_ALG:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       QUIETMOVE_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_DROP_DETEC_ALG:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       DROP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_DUMP_DETEC_ALG:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       DUMP_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_TURN_DETEC_ALG:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       TURN_DETEC_ALG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    
    return read_result;
}

/***
*** 清除传感器偏移补偿
***/
static result_t clr_nfc_senser_offset(uint8_t para_type)
{
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_PLUS_OFFSET:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       PLUS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_REDUCE_OFFSET:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       REDUCE_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_SYS_OFFSET:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       SYS_OFFSET_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    
    return read_result;
}

/***
*** 清除绑定信息
***/
static result_t clr_nfc_bing_info(uint8_t para_type)
{
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_CARRIAGE_NUM:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       CARRIAGE_NUM_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_USER_NUM:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       USER_NUM_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    case PARA_TYPE_UNBIND_INFO:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       UNBIND_INFO_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    return read_result;
}

/***
*** 清除设备日志
***/
static result_t clr_nfc_dev_log(uint8_t para_type)
{
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_CARRIAGE_NUM:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       READ_DEV_LOG_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    
    return read_result;
}

//static bool_t clr_nfc_dev_info(uint8_t para_type)
//{
//    
//    
//    return read_result;
//}

/***
*** 清除设备SSN参数
***/
static result_t clr_nfc_dev_ssn_para(uint8_t para_type)
{
    result_t read_result = NFC_READ_SUCCESS;
    
    switch(para_type)
    {
    case PARA_TYPE_ALL_SSN_INFO:
        if(ERROR == hal_write_nfc_data(clr_data_buf, CLR_DATA_LEN, 
                                       SSN_DEV_ALL_INFO_ADDRESS))
        {
            read_result = NFC_READ_FALSE;
        }
        break;
        
    default:
        read_result = PARA_TYPE_FALSE;
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
    
    return read_result;
}

/***
*** 清除设备信息
***/
result_t nfc_clr_info(uint8_t nfc_app_type, uint8_t para_type)
{
    result_t result = NFC_READ_SUCCESS;
    
    switch (nfc_app_type)
    {
    case APP_TYPE_ALG_PARA_CFG:
        result = clr_nfc_alg_para(para_type);
        break;
        
    case APP_TYPE_SENSER_OFFSET:
        result = clr_nfc_senser_offset(para_type);
        break;
        
    case APP_TYPE_BIND_INFO_CFG:
        result = clr_nfc_bing_info(para_type);
        break;
        
    case APP_TYPE_READ_DEV_LOG:
        result = clr_nfc_dev_log(para_type);
        break;
        
    case APP_TYPE_DEVICE_INFO_ACK:
//        result = clr_nfc_dev_info(para_type);
        break;
        
	case APP_TYPE_SSN_DEV_INFO:
		result = clr_nfc_dev_ssn_para(para_type);
		break;
        
    default:
        break;
    }
    return result;
}

uint16_t get_log_addr(uint8_t para_type, uint8_t *log_flag_buf)
{
    uint16_t log_addr = 0x0000;
    switch (para_type)
    {
    case TYPE_REG_DATA:
        if(log_flag_buf[TYPE_REG_DATA] == REG_DATA_LEN)
        {
            log_flag_buf[TYPE_REG_DATA] = 0;
        }
        log_addr = LOG_INFO_ADDRESS + 
            sizeof(nfc_log_info_t)*log_flag_buf[TYPE_REG_DATA];
        log_flag_buf[TYPE_REG_DATA] ++;
        break;
        
    case TYPE_FORCE_RELEASE_DATA:
        if(log_flag_buf[TYPE_FORCE_RELEASE_DATA] == FORCE_RELEASE_DATA_LEN)
        {
            log_flag_buf[TYPE_FORCE_RELEASE_DATA] = 0;
        }
        log_addr = LOG_INFO_ADDRESS + sizeof(nfc_log_info_t) * REG_DATA_LEN
            + sizeof(nfc_log_info_t)*log_flag_buf[TYPE_FORCE_RELEASE_DATA];
        log_flag_buf[TYPE_FORCE_RELEASE_DATA]++;
        break;
        
    case TYPE_OVERRUN_DATA:
        if(log_flag_buf[TYPE_OVERRUN_DATA] == OVERRUN_DATA_LEN)
        {
            log_flag_buf[TYPE_OVERRUN_DATA] = 0;
        }
        log_addr = LOG_INFO_ADDRESS + sizeof(nfc_log_info_t) * (REG_DATA_LEN + FORCE_RELEASE_DATA_LEN)
            + sizeof(nfc_log_info_t)*log_flag_buf[TYPE_OVERRUN_DATA];
        log_flag_buf[TYPE_OVERRUN_DATA]++;
        break;
        
    case TYPE_HB_DATA:
        if(log_flag_buf[TYPE_HB_DATA] == HB_DATA_LEN)
        {
            log_flag_buf[TYPE_HB_DATA] = 0;
        }
        log_addr = LOG_INFO_ADDRESS + sizeof(nfc_log_info_t) * (REG_DATA_LEN + FORCE_RELEASE_DATA_LEN 
            + OVERRUN_DATA_LEN )+ sizeof(nfc_log_info_t)*log_flag_buf[TYPE_HB_DATA];
        log_flag_buf[TYPE_HB_DATA] ++;
        break;
        
    case TYPE_QUIET2MOVE_DATA:
        if(log_flag_buf[TYPE_QUIET2MOVE_DATA] == QUIET2MOVE_DATA_LEN)
        {
            log_flag_buf[TYPE_QUIET2MOVE_DATA] = 0;
        }
        log_addr = LOG_INFO_ADDRESS + sizeof(nfc_log_info_t) * (REG_DATA_LEN + FORCE_RELEASE_DATA_LEN 
            + OVERRUN_DATA_LEN + HB_DATA_LEN) 
            + sizeof(nfc_log_info_t)*log_flag_buf[TYPE_QUIET2MOVE_DATA];
        log_flag_buf[TYPE_QUIET2MOVE_DATA] ++;
        break;
        
	case TYPE_MOVE2QUIET_DATA:
        if(log_flag_buf[TYPE_MOVE2QUIET_DATA] == MOVE2QUIET_DATA_LEN)
        {
            log_flag_buf[TYPE_MOVE2QUIET_DATA] = 0;
        }
		log_addr = LOG_INFO_ADDRESS + sizeof(nfc_log_info_t) * (REG_DATA_LEN + FORCE_RELEASE_DATA_LEN 
            + OVERRUN_DATA_LEN + HB_DATA_LEN + QUIET2MOVE_DATA_LEN)
            + sizeof(nfc_log_info_t)*log_flag_buf[TYPE_MOVE2QUIET_DATA];
        log_flag_buf[TYPE_MOVE2QUIET_DATA] ++;
		break;
        
    default:
        break;
    }
    return log_addr;
}

/***
*** 存储日志信息
***/
result_t nfc_save_log_info(uint8_t nfc_log_type)
{
    uint8_t result = 0;
    uint16_t write_addr = 0x0000;
    if(is_nfc_fist_start())
    {
        hal_write_nfc_data(log_flag_buf, LOG_FLAG_LEN, LOG_INFO_FLAG);
    }
    hal_read_nfc_data(log_flag_buf, LOG_FLAG_LEN, LOG_INFO_FLAG);
    
    hal_time_t now = hal_timer_now();
    nfc_log_info_t nfc_log;
    nfc_log.log_time = now.w;
    nfc_log.log_type = nfc_log_type;
    write_addr = get_log_addr(nfc_log_type, log_flag_buf);
    
    result = hal_write_nfc_data((uint8_t *)&nfc_log, sizeof(nfc_log),
                                    write_addr);
    hal_write_nfc_data(log_flag_buf, LOG_FLAG_LEN, LOG_INFO_FLAG);
    if(result == ERROR)
    {
        return NFC_READ_FALSE;
    }
    else
    {
        return NFC_READ_SUCCESS;
    }
}

/***
*** 读信息标志位区
***/
bool_t nfc_read_info_flag(nfc_info_flag_t *nfc_info_flag)
{
    uint8_t flag_buf[32] = {0};
    uint8_t *flag_p = flag_buf;
    DBG_ASSERT(nfc_info_flag != NULL __DBG_LINE);
    if (TRUE == hal_read_nfc_data(flag_p, NFC_INFO_FLAG_SIZE, 
                     MUTUAL_INFO_FALG_ADDRESS))
    {
        nfc_info_flag->overrun_detec_alg    = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->quietmove_detec_alg  = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->drop_detec_alg       = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->dump_detec_alg       = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->turn_detec_alg       = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->plus_offset          = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->reduce_offset        = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->sys_offset           = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->carriage_num         = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->read_dev_log         = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->ssn_para             = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->profile_id           = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->user_num             = (*flag_p++ == 0x01) ? 1:0;
        nfc_info_flag->unbind               = (*flag_p++ == 0x01) ? 1:0;        
    }
    else
    {
        return FALSE;
    }
    
    return TRUE;
}

/*** 
*** 清除信息标志位区
***/
bool_t nfc_clr_info_flag(uint8_t flag_bit)
{
    uint8_t flag_buf[32] = {0};
    uint8_t *flag_p = flag_buf;
	
    DBG_ASSERT(flag_bit < 32 __DBG_LINE);
	
	if (FALSE == hal_read_nfc_data(flag_p, NFC_INFO_FLAG_SIZE, 
                     MUTUAL_INFO_FALG_ADDRESS))
	{
	  	return FALSE;
	}
	
    flag_buf[flag_bit] = 0;
	if (FALSE == hal_write_nfc_data(flag_p, NFC_INFO_FLAG_SIZE, 
                     MUTUAL_INFO_FALG_ADDRESS))
	{
	  	return FALSE;
	}
	
	return TRUE;
}

/***
*** nfc第一次写标志位
***/
static void nfc_write_first_flag(void)
{
    uint8_t start_flag = FIRST_START;
    hal_write_nfc_data(&start_flag, 1, FIRST_START_ADDR);
}



/***
*** nfc信息标志位初始化
***/
static void nfc_info_flag_init(void)
{    
    if(is_nfc_fist_start())
    {
        if(ERROR == hal_write_nfc_data(clr_data_buf, NFC_INFO_FLAG_SIZE, 
                                       MUTUAL_INFO_FALG_ADDRESS))
        {
            DBG_ASSERT(FALSE __DBG_LINE);
        }
        nfc_write_first_flag();
    }
}

bool_t nfc_read_ip(nfc_ip_t *nfc_ip)
{
    uint8_t ip_buf[32] = {0};
    uint8_t *flag_p = ip_buf;
    uint8_t i = 0;
    uint8_t j = 0;
    uint8_t k = 0;
    bool_t result = FALSE;

    DBG_ASSERT(nfc_ip != NULL __DBG_LINE);
    hal_read_nfc_data(flag_p, NFC_INFO_FLAG_SIZE, 
                     SERVICE_IP_ADDR_ADDRESS);
    uint16_t u = 0;

    if(ip_buf[0] == '.')
    {
        return FALSE;
    }
    for(i = 0; i<20; i++)
    {
        if(k == 0)
        {
            u = (ip_buf[i] - 0x30);
        }
        else if(k == 1)
        {
            u = (ip_buf[i-1] - 0x30)*10 + (ip_buf[i] - 0x30);
        }
        else if(k == 2)
        {
            u = (ip_buf[i -2] - 0x30)*100 + (ip_buf[i-1] - 0x30)*10 
                + (ip_buf[i] - 0x30);
        }
        k++;
        if(ip_buf[i+1] == '.')
        {
            nfc_ip->ip[j] = u;
            if(nfc_ip->ip[0] == 0xff || nfc_ip->ip[0] == 0x00)
            {
                return FALSE;
            }
            j++;
            i++;
            k = 0;
        }
        if(ip_buf[i+1] == ':')
        {
            nfc_ip->ip[j] = u;
            i++;
            k = 0;
            break;
        }
    }
    i++;
    for(; i<20; i++)
    {
        if(k == 0)
        {
            u = (ip_buf[i] - 0x30);
        }
        else if(k == 1)
        {
            u = (ip_buf[i-1] - 0x30)*10 + (ip_buf[i] - 0x30);
        }
        else if(k == 2)
        {
            u = (ip_buf[i -2] - 0x30)*100 + (ip_buf[i-1] - 0x30)*10 
                + (ip_buf[i] - 0x30);
        }
        else if(k == 3)
        {
            u = (ip_buf[i-3] - 0x30)*1000 + (ip_buf[i-2] - 0x30)*100 
        + (ip_buf[i-1] - 0x30)*10 + (ip_buf[i] - 0x30);
        }
        k++;
        if(ip_buf[i+1] == ';')                 //结束符
        {
            nfc_ip->port = u;
            result = TRUE;
            break;
        }
    }
   
    return result;
}

/***
*** nfc初始化
***/
void nfc_init(void)
{
    osel_memset(clr_data_buf, 0x00, CLR_DATA_LEN);
    osel_memset(log_flag_buf, 0x00, LOG_FLAG_LEN);
	m24lr64e_init();
    nfc_info_flag_init();
}