/***************************************************************************
* File        : hal_nfc_driver.c
* Summary     : 
* Version     : v0.1
* Author      : wangjifang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            wangjifang        first version
***************************************************************************/

#include <nfc_driver.h>

/**
 * i2c向NFC连续读数据
 *
 * @param data_buf  读出来的数据存放的地址
 * @param data_len  读出来的数据长度
 * @param word_addr 从NFC的哪个地址读数据
 *
 * @return void
 */
bool_t hal_read_nfc_data(uint8_t *const data_buf, 
                      uint8_t const data_len, 
                      uint16_t const word_addr)
{
	return read_nfc_data(data_buf, data_len, word_addr);
}

/***
*** 写nfc块区
***/
bool_t hal_write_nfc_area_data(uint8_t *const data_buf, 
                            uint8_t const data_len, 
                            uint16_t const word_addr)
{
    return write_nfc_area_data(data_buf, data_len, word_addr);
}

/***
*** 写nfc数据
***/
bool_t hal_write_nfc_data(uint8_t *const data_buf,
                       uint8_t const data_len,
                       uint16_t const word_addr)
{
    return  write_nfc_data(data_buf, data_len, word_addr);
}