/***************************************************************************
* File        : nfc_driver.c
* Summary     : 
* Version     : v0.1
* Author      : wangjifang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            wangjifang        first version
***************************************************************************/


#include <nfc_driver.h>
#include <driver.h>
#include <debug.h>
#include <wsnos.h>

//SDA 数据线定义
#define SDAOUT P10DIR |= BIT1
#define SDAIN  P10DIR &= ~BIT1
#define SDA1   P10OUT |= BIT1
#define SDA0   P10OUT &= ~BIT1

//SCL 时钟线定义
#define SCLOUT P10DIR |= BIT2
#define SCLIN  P10DIR &= ~BIT2
#define SCL1   P10OUT |= BIT2
#define SCL0   P10OUT &= ~BIT2

#define SECTION_SIZE        0x80            //一个扇区大小128byte
#define SECTION_AREA_SIZE   0x20            //一个扇区大小32个区域
#define AREA_SIZE           0x04            //一个区域大小4byte
#define NFC_MAX_ADDR        0x2000          //NFC最大地址

#define NFC_POWER_PROC_FLAG					1
#define NFC_POWER_PROC()                                                \
    do                                                              \
    {                                                               \
	  	P9OUT &= ~BIT7;												\
		delay_us(10);												\
		P9OUT |= BIT7;												\
    } while(__LINE__ == -1)
	  
	
//#include <serial.h>
//static void NFC_POWER_PROC(void)  
//{
//  do                                                              
//    {                                                              
//	  	P9OUT &= ~BIT7;
//		hal_uart_send_char(HAL_UART_2, 0xF0);
////		delay_us(10);												
//		P9OUT |= BIT7;												
//    } while(__LINE__ == -1);
//}
											
static uint8_t i2c_add = 0;

//*****************i2c初始化****************************************************
void i2c_init(void)
{
    P10SEL &= ~BIT2;
    SCLOUT;
    SCL1;
    for(uint8_t i = 0; i < 9; i++ )
    {
        SCL1;
        delay_ms(1);
        SCL0;
        delay_ms(1);
    }
    P10SEL |= (BIT1 + BIT2);
    UCB3CTL1 |= UCSWRST;
    UCB3CTL0 = UCMST + UCMODE_3 + UCSYNC ;          // I2C主机模式
    UCB3CTL1 |= UCSSEL_2;                           // 选择SMCLK
    UCB3BR0 = 40;
    UCB3BR1 = 0;
    UCB3CTL0 &= ~UCSLA10;                           // 7位地址模式
//    UCB3I2CSA = i2c_slave_dev;                       // NFC
    UCB3CTL1 &= ~UCSWRST;
}

//****************i2c写nfc一个字节***************************************
static uint8_t i2c_send_byte(uint16_t const word_addr, uint8_t const word_value)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    
    UCB3I2CSA = i2c_add;
    osel_int_status_t status = 0;
    
    OSEL_ENTER_CRITICAL(status);
    while( UCB3CTL1 & UCTXSTP );
    UCB3CTL1 |= UCTR;                               // 写模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位
    UCB3TXBUF = (uint8_t)(word_addr>>8);            // 发送字节高地址
    
    while(!(UCB3IFG & UCTXIFG))
    {  	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    
    UCB3TXBUF = (uint8_t)(word_addr);               // 发送字节低地址
    while(!(UCB3IFG & UCTXIFG))
    {		  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }   
    
    UCB3TXBUF = word_value;                         // 发送字节内容
    while(!(UCB3IFG & UCTXIFG));                    // 等待UCTXIFG=1
    
    UCB3CTL1 |= UCTXSTP;
    while(UCB3CTL1 & UCTXSTP);                      // 等待发送完成
    OSEL_EXIT_CRITICAL(status);
    return RIGHT;
}

//*****************i2c读nfc一个字节****************************************
static uint8_t i2c_recv_byte(uint16_t const word_addr , uint8_t *const pword_buf)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    DBG_ASSERT(pword_buf != NULL __DBG_LINE);
    
    UCB3I2CSA = i2c_add;
    osel_int_status_t status = 0;
    
    OSEL_ENTER_CRITICAL(status);
    UCB3CTL1 |= UCTR;                               // 写模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位和写控制字节
    
    UCB3TXBUF = (uint8_t)(word_addr>>8);            // 发送字节高地址
    while(!(UCB3IFG & UCTXIFG))
    {
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    
    UCB3TXBUF = (uint8_t)(word_addr);               // 发送字节低地址
    while(!(UCB3IFG & UCTXIFG))
    {  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }                        
    
    UCB3CTL1 &= ~UCTR;                              // 读模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位和读控制字节
    
    while(UCB3CTL1 & UCTXSTT)                       // 等待UCTXSTT=0
    {	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    UCB3CTL1 |= UCTXSTP;                            // 先发送停止位
    while(!(UCB3IFG & UCRXIFG));                    // 读取字节内容
    *pword_buf = UCB3RXBUF;                         // 读取BUF寄存器在发送停止位之后
    while( UCB3CTL1 & UCTXSTP );
    OSEL_EXIT_CRITICAL(status);
    return RIGHT; 
}

//*******************读nfc数据**************************************************
bool_t read_nfc_data(uint8_t *const data_buf, uint8_t const data_len, 
                      uint16_t const word_addr)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    
    DBG_ASSERT(data_buf != NULL __DBG_LINE);
    DBG_ASSERT(data_len != 0 __DBG_LINE);
    
    UCB3I2CSA = i2c_add;
    uint8_t *data_temp = data_buf;
    
    osel_int_status_t status = 0;
    
    OSEL_ENTER_CRITICAL(status);
    
    UCB3CTL1 |= UCTR;                               // 写模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位和写控制字节
    
    UCB3TXBUF = (uint8_t)(word_addr>>8);            // 发送字节高地址
    while(!(UCB3IFG & UCTXIFG))
    {	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    
    UCB3TXBUF = (uint8_t)(word_addr);               // 发送字节低地址
    while(!(UCB3IFG & UCTXIFG))
    {	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }                        
    
    UCB3CTL1 &= ~UCTR;                              // 读模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位和读控制字节
    
    while(UCB3CTL1 & UCTXSTT)                       // 等待UCTXSTT=0
    {	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    for( uint8_t i = 0 ; i < data_len - 1 ; i++ )
    {
        while(!(UCB3IFG & UCRXIFG));                // 读取字节内容
        *data_temp++ = UCB3RXBUF;                    // 发送寄存器内容
    }
    UCB3CTL1 |= UCTXSTP;
    while(!(UCB3IFG & UCRXIFG));                    // 读取最后一个字节内容
    *data_temp = UCB3RXBUF;
    while(UCB3CTL1 & UCTXSTP);                      // 等待发送完成
    
    OSEL_EXIT_CRITICAL(status);
    
    return RIGHT;
}

//******************************************************************************
//写nfc数据。写数据只能一块一块写，一块4byte。
static uint8_t write_nfc_4byte_data(uint8_t *const data_buf, 
                                    uint8_t const data_len,
                                    uint16_t const word_addr)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    DBG_ASSERT(data_buf != NULL __DBG_LINE);
    DBG_ASSERT(data_len != 0 __DBG_LINE);
    
    UCB3I2CSA = i2c_add;
    uint8_t *data_temp = data_buf;
    osel_int_status_t status = 0;
    OSEL_ENTER_CRITICAL(status);
    
    while( UCB3CTL1 & UCTXSTP );
    UCB3CTL1 |= UCTR;                               // 写模式
    UCB3CTL1 |= UCTXSTT;                            // 发送启动位
    
    UCB3TXBUF = (uint8_t)(word_addr>>8);            // 发送字节高地址
    while(!(UCB3IFG & UCTXIFG))
    {	  
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    
    UCB3TXBUF = (uint8_t)(word_addr);               // 发送字节低地址
    while(!(UCB3IFG & UCTXIFG))
    { 
        if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        {
#if	NFC_POWER_PROC_FLAG
	  		NFC_POWER_PROC();
#endif			  
            UCB3CTL1 |= UCTXSTP;
            OSEL_EXIT_CRITICAL(status);
            return ERROR;
        }
    }
    
    for( uint8_t i = 0 ; i < data_len ; i++ )
    {
        UCB3TXBUF = *data_temp ++;                   // 发送寄存器内容
		while(!(UCB3IFG & UCTXIFG))
    	{ 
        	if( UCB3IFG & UCNACKIFG )                   // 若无应答 UCNACKIFG=1
        	{
#if	NFC_POWER_PROC_FLAG
	  			NFC_POWER_PROC();
#endif			  
            	UCB3CTL1 |= UCTXSTP;
            	OSEL_EXIT_CRITICAL(status);
            	return ERROR;
        	}
    	}		
    }
    
    UCB3CTL1 |= UCTXSTP;
    while(UCB3CTL1 & UCTXSTP);                      // 等待发送完成
    
    OSEL_EXIT_CRITICAL(status);
    return RIGHT;                                   //返回操作成败标志  
}

//******************************************************************************
//连续写nfc一块扇区内存储区的数据
bool_t write_nfc_area_data(uint8_t *const data_buf,
                            uint8_t const data_len,
                            uint16_t const word_addr)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    DBG_ASSERT(data_buf != NULL __DBG_LINE);
    DBG_ASSERT(data_len != 0 __DBG_LINE);
    
    uint8_t *data_temp = data_buf;
    uint16_t word_addr_temp = word_addr;
    //该起始地址所在区域还剩多少字节可写
    uint8_t remain_data_pro = AREA_SIZE - (word_addr_temp % AREA_SIZE);
    uint8_t remain_data_end = 0;
    if(data_len >= remain_data_pro)
    {
        //最后一个区域
        remain_data_end = (data_len-remain_data_pro)%AREA_SIZE;
    }
    if(remain_data_pro != 0)
    {
        if(data_len <= remain_data_pro)
        {
            remain_data_pro = data_len;
        }
        if(RIGHT == write_nfc_4byte_data(data_temp, remain_data_pro, word_addr_temp))
        {
            data_temp += remain_data_pro;
            word_addr_temp += remain_data_pro;
            delay_ms(5);
        }
    }
    //写完开始的后还剩几个完整区域
    uint8_t data_area_numb = (data_len-remain_data_pro)/AREA_SIZE;
    for(uint8_t i=0; i<data_area_numb; i++)
    {
        if(RIGHT == write_nfc_4byte_data(data_temp, AREA_SIZE, word_addr_temp))
        {
            data_temp += AREA_SIZE;
            word_addr_temp += AREA_SIZE;
            delay_ms(5);
        }
        else
        {
            return ERROR;
        }
    }
	
    if(remain_data_end != 0)                                      //写最后的区域
    {
        write_nfc_4byte_data(data_temp, remain_data_end, word_addr_temp);
        delay_ms(5);
    }
	
    return RIGHT;
}

bool_t write_nfc_data(uint8_t *const data_buf,
                       uint8_t const data_len,
                       uint16_t const word_addr)
{
    DBG_ASSERT(word_addr <= NFC_MAX_ADDR __DBG_LINE);
    DBG_ASSERT(data_buf != NULL __DBG_LINE);
    DBG_ASSERT(data_len != 0 __DBG_LINE);
    
    uint8_t *data_temp = data_buf;
    uint16_t word_addr_temp = word_addr;
    uint8_t remain_data_area_pro = SECTION_SIZE - (word_addr_temp % SECTION_SIZE);  
    uint8_t remain_data_area_end = 0;
    if(data_len >= remain_data_area_pro)
    {
        remain_data_area_end = (data_len-remain_data_area_pro)%SECTION_SIZE;
    }
    if(remain_data_area_pro != 0)
    {
        if(data_len <= remain_data_area_pro)
        {
            remain_data_area_pro = data_len;
        }
        if(RIGHT == write_nfc_area_data(data_temp, remain_data_area_pro,
                                        word_addr_temp))
        {
            data_temp  += remain_data_area_pro;
            word_addr_temp += remain_data_area_pro;
        }
    }
    //写完开始的后还剩几个整扇区
    uint8_t data_section_numb = (data_len-remain_data_area_pro)/SECTION_SIZE;
    for(uint8_t i=0;i<data_section_numb; i++)
    {
        if(RIGHT == write_nfc_area_data(data_temp, SECTION_SIZE, word_addr_temp))
        {
            data_temp += SECTION_SIZE;
            word_addr_temp += SECTION_SIZE;
        }
        else
        {
            return ERROR;
        }
    }
	
    if(remain_data_area_end != 0)
    {
        if(ERROR == write_nfc_area_data(data_temp, remain_data_area_end, word_addr_temp))
        {
            return ERROR;
        }   
    }
	
    return RIGHT;
}

void m24lr64e_port_init(void)
{
 	TA0CCTL0 &= ~CCIE; 
  	P8SEL |= BIT0;   //config P8.0 as input capture io
	P8DIR &= ~BIT0;  //config P8.0 as input
    TA0CCTL0 = CM_1 + CCIS_1 + SCS + CAP;  //CM_1上升沿捕获，CCIS_1选择CCIxB,
                                            //SCS同步捕获，CAP捕获模式  
}

void m24lr64e_int_cfg(void)
{
  	//设置中断P8.0
  	TA0CCTL0 &= ~CCIFG;//初始为中断未挂起
	TA0CCTL0 |= CCIE;// 开启中断使能
}

void m24lr64e_reg_cfg(void)
{
  	uint8_t oldreg = 0x00;
	uint8_t newreg = 0x00;
	
  	i2c_recv_byte(0x910,&oldreg);
	
	newreg = oldreg | 0x08;//RF WIP/BUSY 设置为1，即WRITE模式
	
	i2c_send_byte(0x910,newreg);
}

void m24lr64e_init(void)
{
 	P9SEL &= ~BIT7;
	P9DIR |= BIT7;
	P9OUT |= BIT7;
	
    i2c_init();//I2C模块的初始化:包括使用前I2C的释放操作和寄存器的配置

    m24lr64e_port_init();//配置硬件接口
	
	i2c_add = NFC_ADDRESS_E2_1;
	m24lr64e_reg_cfg();//rfid寄存器配置	
	i2c_add = NFC_ADDRESS_E2_0;

    m24lr64e_int_cfg();//中断配置
}