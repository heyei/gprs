/***************************************************************************
* File        : debug.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <debug.h>
#include <sqqueue.h>
#include <hal_nvmem.h>
#include <hal_board.h>
#include <wsnos.h>

static bool_t debug_delay_clr_flag = FALSE;

static __no_init uint16_t dbg_line;

#if DEBUG_INFO_PRINT_EN > 0
void DBG_ASSERT(bool_t cond _DBG_LINE_)
{
    do
    {
        if ((cond) == FALSE)
        {
            DISABLE_INT();
            hal_led_open(HAL_LED_BLUE);
            while (1);
        }
    }
    while (__LINE__ == -1);
}

#else

void DBG_ASSERT(bool_t cond _DBG_LINE_)
{
    extern void board_reset(void);
    do
    {
        if ((cond) == FALSE)
        {
            _DINT();
            dbg_line = line;
            board_reset();
        }
    } while (__LINE__ == -1);
}

#endif

uint16_t debug_get_info(void)
{
        uint16_t line;
    line = dbg_line;
    return line;
}

void debug_clr_info(void)
{
     dbg_line = 0xFFFF;
}

void debug_delay_clr_info(void)
{
    if(debug_delay_clr_flag)
    {
        debug_delay_clr_flag = FALSE;
        hal_nvmem_erase((uint8_t *)DEBUG_INFO_ADDR, FLASH_SEG_ERASE);
    }
}