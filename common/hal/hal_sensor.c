/***************************************************************************
* File        : hal_sensor.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <data_type_def.h>
#include <hal_sensor.h>
#include <driver.h>

bool_t hal_sensor_read(void *sensor_data_p, uint8_t sensor_type, uint8_t type)
{
    if (sensor_type == SENSOR_DHT)
    {
        if (type == DHT_SENSOR_TYPE_H)
        {
            return humiture_sensor_read_h((uint16_t *)sensor_data_p);
        }
        else if (type == DHT_SENSOR_TYPE_T)
        {
            return humiture_sensor_read_t((uint16_t *)sensor_data_p);
        }
        else
        {
            return FALSE;
        }
    }
    else if (sensor_type == SENSOR_ACCE)
    {
        return adxl345_read_buff(0x32, (uint8_t *)sensor_data_p, 6);
    }
    else
    {
        return FALSE;
    }
}


void hal_sensor_init(void)
{
//    humiture_sensor_init();
//    accelerated_sensor_init();
}

