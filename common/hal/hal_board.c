/***************************************************************************
* File        : hal_board.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <driver.h>
#include <hal.h>
#include <osel_arch.h>
#include <hal_acc_sensor.h>
#if (NODE_TYPE == NODE_TYPE_TAG)
#include <nfc_module.h>
#include <hal_sensor.h>
#endif

#define CH_SN_1         4
#define CH_SN_2         4
#define CH_SN_3         4
#define CH_SN_4         4
#define CH_SN_5         4
#define CH_SN_6         4
#define CH_SN_7         4
#define CH_SN_8         4

device_info_t device_info;
static bool_t delay_save_flag = FALSE;

void hal_led_init(void)
{
    led_init();
}


void hal_led_open(uint8_t color)
{
    switch (color)
    {
    case HAL_LED_BLUE:
        LED_OPEN(BLUE);
        break;
    case HAL_LED_RED:
        LED_OPEN(RED);
        break;
    case HAL_LED_GREEN:
        LED_OPEN(GREEN);
        break;
    case HAL_LED_POWER:
        LED_OPEN(RED);
        break;
    case HAL_LED_ERROR:
        LED_OPEN(BLUE);
        break;
    default :
        break;
    }
}

void hal_led_close(uint8_t color)
{
    switch (color)
    {
    case HAL_LED_BLUE:
        LED_CLOSE(BLUE);
        break;
    case HAL_LED_RED:
        LED_CLOSE(RED);
        break;
    case HAL_LED_GREEN:
        LED_CLOSE(GREEN);
        break;
    case HAL_LED_POWER:
        LED_CLOSE(RED);
        break;
    case HAL_LED_ERROR:
        LED_CLOSE(BLUE);
        break;
    default :
        break;
    }
}

void hal_led_toggle(uint8_t color)
{
    switch (color)
    {
    case HAL_LED_BLUE:
        LED_TOGGLE(BLUE);
        break;
    case HAL_LED_RED:
        LED_TOGGLE(RED);
        break;
    case HAL_LED_GREEN:
        LED_TOGGLE(GREEN);
        break;
    case HAL_LED_POWER:
        LED_TOGGLE(RED);
        break;
    case HAL_LED_ERROR:
        LED_TOGGLE(BLUE);
        break;
    default :
        break;
    }
}

bool_t hal_hex_to_ascii(uint8_t *buf, uint8_t dat)
{
    uint8_t dat_buff;

    dat_buff = dat;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[1] = dat;

    dat = dat_buff;
    dat >>= 4;
    dat = dat & 0x0f;
    if (dat <= 9)
    {
        dat += 0x30;
    }
    else
    {
        dat += 0x37;
    }

    buf[0] = dat;

    return TRUE;
}

bool_t hal_ascii_to_hex(uint8_t hi, uint8_t lo, uint8_t *hex)
{
    *hex = 0;
    if ((hi >= 0x30) && (hi <= 0x39))
    {
        hi -= 0x30;
    }
    else if ((hi >= 0x41) && (hi <= 0x46))
    {
        hi -= 0x37;
    }
    else if((hi >= 0x61) && (hi <= 0x66))
    {
        hi -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= (hi << 4);

    if ((lo >= 0x30) && (lo <= 0x39))
    {
        lo -= 0x30;
    }
    else if ((lo >= 0x41) && (lo <= 0x46))
    {
        lo -= 0x37;
    }
    else if((lo >= 0x61) && (lo <= 0x66))
    {
        lo -= 0x37;
    }
    else
    {
        return FALSE;
    }
    *hex |= lo;

    return TRUE;
}

uint16_t hal_board_get_device_short_addr(uint8_t id[])
{
    uint16_t short_id;
    short_id = BUILD_UINT16(id[0], id[1]);
    short_id &= ~0xC000;
    short_id |= (((uint16_t)(id[2])) << 14);

    return short_id;
}

device_info_t hal_board_info_get(void)
{
    hal_nvmem_read((uint8_t *)DEVICE_INFO_ADDR,
                   (uint8_t *)&device_info, sizeof(device_info));

    return device_info;
}

device_info_t hal_board_info_look(void)
{
    return device_info;
}

void hal_board_info_clr(void)
{
    osel_memset((uint8_t *)&device_info, 0xFF, sizeof(device_info_t));

    hal_nvmem_erase((uint8_t *)DEVICE_INFO_ADDR, 0);
}

bool_t hal_board_info_save(device_info_t *p_info, bool_t flag)
{
    osel_memcpy((uint8_t *)&device_info, (uint8_t *)p_info, sizeof(device_info_t));

    if (flag)
    {
        hal_nvmem_erase((uint8_t *)DEVICE_INFO_ADDR, 0);
        hal_nvmem_write((uint8_t *)DEVICE_INFO_ADDR,
                        (uint8_t *)p_info, sizeof(device_info_t));
    }
    else
    {
        delay_save_flag = TRUE;
    }

    return TRUE;
}

bool_t hal_board_info_delay_save(void)
{
    if (delay_save_flag)
    {
        delay_save_flag = FALSE;
        hal_nvmem_erase((uint8_t *)DEVICE_INFO_ADDR, 0);
        hal_nvmem_write((uint8_t *)DEVICE_INFO_ADDR,
                        (uint8_t *)&device_info, sizeof(device_info_t));
    }

    return TRUE;
}

void hal_board_reset(void)
{
    board_reset();
}

void hal_board_info_init(void)
{
    device_info = hal_board_info_get();

    uint64_t temp_node_id = 0xFFFFFFFFFFFFFFFF;
    if (osel_memcmp(device_info.device_id, (uint8_t *)&temp_node_id, sizeof(uint64_t)))
    {
        temp_node_id = NODE_ID;
        osel_memcpy(device_info.device_id, (uint8_t *)&temp_node_id, sizeof(uint64_t));
        device_info.intra_ch[0] = CH_SN;
        device_info.power_sn = POWER_SN;
#if (NODE_TYPE == NODE_TYPE_ROUTER)
        device_info.nwk_depth = NODE_NWK_DEPTH;
#endif
        device_info.intra_ch_cnt    = 1;
#if (NODE_TYPE == NODE_TYPE_TAG)
        device_info.intra_ch[0] = CH_SN_1;
        device_info.intra_ch[1] = CH_SN_2;
        device_info.intra_ch[2] = CH_SN_3;
        device_info.intra_ch[3] = CH_SN_4;
        device_info.intra_ch[4] = CH_SN_5;
        device_info.intra_ch[5] = CH_SN_6;
        device_info.intra_ch[6] = CH_SN_7;
        device_info.intra_ch[7] = CH_SN_8;
        device_info.intra_ch_cnt    = 8;
#endif
        if (device_info.power_sn == 0xFF)
        {
            device_info.power_sn = POWER_SN;
        }
#if (NODE_TYPE == NODE_TYPE_ROUTER)
        if (device_info.nwk_depth == 0xFF)
        {
            device_info.nwk_depth = NODE_NWK_DEPTH;
        }
#endif
        
        device_info.heartbeat_cycle = 120;
        hal_board_info_save(&device_info, TRUE);
    }
}

void hal_board_init(void)
{
    hal_gpio_init();

    hal_clk_init(SYSCLK_8MHZ);

#if (NODE_TYPE == NODE_TYPE_TAG)
    nfc_init();
#endif

    hal_board_info_init();

#if (NODE_TYPE == NODE_TYPE_TAG)
//    hal_uart_init(HAL_UART_2, 38400, 0); // 调试时才使用，这路UART使用了MAX3221，会增加功耗
#else
    hal_uart_init(HAL_UART_1, 9600, 0);
#endif
	
#if (NODE_TYPE == NODE_TYPE_GATEWAY)
#include <hal_socket.h>
    hal_socket_init();
#endif
    hal_rf_init();

    hal_led_init();

#if (NODE_TYPE == NODE_TYPE_TAG)
    hal_energy_init();
    acc_init();
//    hal_sensor_init();
#endif

    hal_timer_init();
}
