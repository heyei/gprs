/***************************************************************************
* File        : hal_clock.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <driver.h>

void hal_hardware_wdt_clear()
{
    hardware_wdt_clear();
}

void hal_wdt_start(uint32_t time_ms)
{
    wdt_start(time_ms);
}

void hal_wdt_clear(uint32_t time_ms)
{
    wdt_clear(time_ms);
}

void hal_wdt_stop(uint32_t time_ms)
{
    wdt_stop(time_ms);
}

void hal_clk_init(uint8_t system_clock_speed)
{
    clk_init(system_clock_speed);
}

void hal_clk_xt2_open(void)
{
    clk_xt2_open();
}

void hal_clk_xt2_open_without_wait(void)
{
    clk_xt2_open_without_wait();
}

void hal_clk_xt2_close(void)
{
    clk_xt2_close();
}




