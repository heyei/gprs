/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : the hal of adxl345 drive and app                                                                                           
**  File        : hal_acc_sensor.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/02/19                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是加速度传感器和控制层之间的接口处理                                  
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/02/19   initial version  
*****************************************************************************************/
#include <driver.h>
#include <sensor_accelerated.h>
#include <hal_acc_sensor.h>

acc_config_t acc_config_1;

//void acc_int_cb(bool_t int_type);

/******************************************************************************
Function:
  bool_t acc_init(void)
Description: 
   加速度芯片初始化, 完成引脚设置，中断配置
Input:
	无
Output:
       无
Return: 
    TRUE,  初始化成功
    FALSE, 初始化失败
Others: 
*******************************************************************************/

bool_t acc_init(void)
{
    accelerated_sensor_init();
    return TRUE;
}

/******************************************************************************
Function:
    bool_t acc_set_power_mode(uint8_t power_mode)
Description: 
    设置功耗模式
Input:
    power_mode 功耗模式
    ACC_NORMAL_OPERATION_MODE
    ACC_LOW_POWER_OPERATION_MODE
Output:
       无
Return: 
    TRUE,  设置成功
    FALSE, 设置失败
Others: 
*******************************************************************************/
bool_t acc_set_power_mode(uint8_t power_mode)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    uint8_t power_bit = 0;

    retvalue = adxl345_read_register(ADXL345_BW_RATE,&oldreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    power_bit = (oldreg>>4)&0x01;

    switch(power_mode) 
    {
        case ACC_NORMAL_OPERATION_MODE:
            if (!power_bit)
            {
                return TRUE;//如果当前为正常模式，则返回
            }
            
            newreg = oldreg & (~0x10);//如果当前为低功耗模式，则置位
            break;
           
        case ACC_LOW_POWER_OPERATION_MODE:
            if (power_bit)
            {
                return TRUE;//如果当前为低功耗模式，则返回
            }
            
            newreg = oldreg | 0x10;//如果当前为正常模式，则置位
            break;
            
        default:
            return FALSE;//模式错误
    }
    
    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_BW_RATE, newreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    return TRUE;
}

/******************************************************************************
Function:
    bool_t acc_set_offset(int16_t ofsx,
                                    int16_t ofsy,
                                    int16_t ofsz)
Description: 
     设置偏移值
Input:
        int16_t ofsx, 
        int16_t ofsy,
        int16_t ofsz
Output:
       无
Return: 
           TRUE,  设置成功
            FALSE, 设置失败
Others: 
*******************************************************************************/
bool_t acc_set_offset(int16_t ofsx,
                                      int16_t ofsy,
                                      int16_t ofsz)
{
    uint8_t retvalue = 0;
    
    retvalue = adxl345_write_register(ADXL345_OFSX,ofsx);       // Z轴偏移量
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    retvalue = adxl345_write_register(ADXL345_OFSY,ofsy);       // Y轴偏移量
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    retvalue = adxl345_write_register(ADXL345_OFSZ,ofsz);       // Z轴偏移量
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    return TRUE;
}

/******************************************************************************
Function:
    bool_t acc_set_status(uint8_t status)
Description: 
     设置工作状态，这几种状态都是在0x2d中ADXL345_POWER_CTL 中设置
     D7      |       D6       |       D5       |       D4              
     0               0                 LINK           AUTO_SLEEP

     D3           |    D2       |      D1              D0
     MEASURE    SLEEP         休眠后的采样速率  
Input:
        uint8_t status:
                        ACC_AUTO_SLEEP_STATUS,//这个功能需要与链接位一起设置
                        ACC_MEASURE_STATUS,//测量状态
                        ACC_SLEEP_STATUS,
                        ACC_STANDBY_STATUS,//待机状态
Output:
       无
Return: 
           TRUE,  设置成功
           FALSE, 设置失败
Others: 
*******************************************************************************/
bool_t acc_set_status(uint8_t status)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    uint8_t link_bit = 0;
    uint8_t autosleep_bit = 0;
    uint8_t sleep_bit = 0;
    uint8_t measure_bit = 0;
    uint8_t standby_bit = 0;

    retvalue = adxl345_read_register(ADXL345_POWER_CTL,&oldreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    link_bit = (oldreg>>5)&0x01;
    autosleep_bit = (oldreg>>4)&0x01;
    measure_bit = (oldreg>>3)&0x01;
    sleep_bit = (oldreg>>2)&0x01;
    standby_bit = measure_bit;//待机模式和测量模式是相对的，在同一bit下设置

    switch(status) 
    {
        case ACC_MEASURE_STATUS:
            if (measure_bit)
            {
                return TRUE;//如果当前为测量状态，则返回
            }
            
            newreg = oldreg | 0x08;//如果当前为待机状态，则置位成测量状态
            break;
           
        case ACC_STANDBY_STATUS:
            if (!standby_bit)
            {
                return TRUE;//如果当前为待机状态，则返回
            }
            
            newreg = oldreg & (~0x08);//如果当前为测量状态，则置位成待机状态
            break;
            
        case ACC_AUTO_SLEEP_STATUS:
            if (link_bit && autosleep_bit)
            {
                return TRUE;//如果链接位和自动休眠位同时为1，则返回
            }
            
            newreg = oldreg | 0x30;//链接位和自动休眠位同时为1
            break;
            
        case ACC_SLEEP_STATUS:
            if (sleep_bit)
            {
                return TRUE;
            }

            newreg = oldreg | 0x04;//设置休眠位
            break;
            
        default:
            return FALSE;//模式错误
    }
    
    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_POWER_CTL, newreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    return TRUE;
}

/******************************************************************************
Function:
    bool_t acc_enable_self_test(bool_t enabled)
Description: 
     设置自测试状态,设置0X31  ADXL345_DATA_FORMAT
     D7                 |D6 |D5 |D4 |D3 |D2|D1|D0               
     SELF_TEST              
  
Input:
        bool_t enabled:
                             TRUE, 使能自测试
                             FALSE 禁用自测试
Output:
       无
Return: 
           TRUE,  设置成功
           FALSE, 设置失败
Others: 
*******************************************************************************/
bool_t acc_enable_self_test(bool_t enabled)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    uint8_t selftest_bit = 0;

    retvalue = adxl345_read_register(ADXL345_DATA_FORMAT,&oldreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    selftest_bit = (oldreg>>7)&0x01;

    if (enabled == selftest_bit)
    {
        return TRUE;
    }

    newreg = oldreg^0x80;//把D7位取反

    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_DATA_FORMAT, newreg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    return TRUE;    
}

/******************************************************************************
Function:
    void acc_set_tap_values(const acc_tap_values_t *tap_values)
Description: 
      设置敲击检测的参数             
  
Input:
        const acc_tap_values_t *tap_values   结构体指针
Output:
       无
Return: 
        无
Others: 
*******************************************************************************/
// 设置敲击检测的参数
void acc_set_tap_values(const acc_tap_values_t *tap_values)
{
    uint8_t thresh_tap = 0;
    uint8_t dur = 0;
    uint8_t latent = 0;
    uint8_t window = 0;
    uint8_t retvalue = 0;

    DBG_ASSERT(NULL != tap_values __DBG_LINE);
    
    thresh_tap = tap_values->thresh_tap;
    dur = tap_values->dur;
    latent = tap_values->latent;
    window = tap_values->window;

    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_THRESH_TAP, thresh_tap);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    retvalue = adxl345_write_register(ADXL345_DUR, dur);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    retvalue = adxl345_write_register(ADXL345_LATENT, latent);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    retvalue = adxl345_write_register(ADXL345_WINDOW, window);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);    
}

/******************************************************************************
Function:
    void acc_set_act_values(const acc_act_values_t *act_values)
Description: 
      设置活动检测的参数            
  
Input:
        const acc_act_values_t *act_values   结构体指针
Output:
       无
Return: 
        无
Others: 
*******************************************************************************/
// 设置活动检测的参数
void acc_set_act_values(const acc_act_values_t *act_values)
{
    uint8_t thresh_act = 0;
    uint8_t thresh_inact = 0;
    uint8_t time_inact = 0;
    uint8_t retvalue = 0;

    DBG_ASSERT(NULL != act_values __DBG_LINE);

    thresh_act = act_values->thresh_act;
    thresh_inact = act_values->thresh_inact;
    time_inact = act_values->time_inact;

    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_THRESH_ACT, thresh_act);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    retvalue = adxl345_write_register(ADXL345_THRESH_INACT, thresh_inact);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);   

    retvalue = adxl345_write_register(ADXL345_TIME_INACT, time_inact);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);      
}

/******************************************************************************
Function:
    void acc_set_ff_values(const acc_ff_values_t *ff_values)
Description: 
     设置自由落体检测的参数           
  
Input:
        const acc_ff_values_t *ff_values  结构体指针
Output:
       无
Return: 
        无
Others: 
*******************************************************************************/
// 设置自由落体检测的参数
void acc_set_ff_values(const acc_ff_values_t *ff_values)
{
    uint8_t thresh_ff = 0;
    uint8_t time_ff = 0;
    uint8_t retvalue = 0;

    DBG_ASSERT(NULL != ff_values __DBG_LINE);

    thresh_ff = ff_values->thresh_ff;
    time_ff = ff_values->time_ff;

    // 写入寄存器设置值，写入新值
    retvalue = adxl345_write_register(ADXL345_THRESH_FF, thresh_ff);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    retvalue = adxl345_write_register(ADXL345_TIME_FF, time_ff);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
}

/******************************************************************************
Function:
    void acc_config(const acc_config_t *config)
Description: 
      配置加速度计主要工作参数         
  
Input:
        const acc_config_t *config:加速度计主要工作参数
Output:
       无
Return: 
        无
Others: 
*******************************************************************************/
void acc_config(const acc_config_t *config)
{
    uint8_t      data_rate;       //数据速率
    uint8_t      power_mode;      //功耗模式
    uint8_t      range;           //测量范围
    uint8_t retvalue = 0;
    uint8_t old_bw_rata_reg = 0x00;
    uint8_t new_bw_rata_reg = 0x00;
    uint8_t old_int_enable_reg = 0x00;
    uint8_t new_int_enable_reg = 0x00;
    uint8_t power_rate = 0;

    DBG_ASSERT(NULL != config __DBG_LINE);
    
    data_rate = config->data_rate;
    range = config->range;
    power_mode = 0;
    if (ACC_LOW_POWER_OPERATION_MODE == config->power_mode)
    {
        power_mode = 1;
    }
    
    //1配置功耗模式和速率
    power_rate = (power_mode<<4) | data_rate;
    retvalue = adxl345_write_register(ADXL345_BW_RATE, power_rate);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    //2配置测量范围range
    //先读取当前寄存器值
    retvalue = adxl345_read_register(ADXL345_DATA_FORMAT,&old_bw_rata_reg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    new_bw_rata_reg = old_bw_rata_reg|range;
    
    // 写入寄存器设置值
    retvalue = adxl345_write_register(ADXL345_DATA_FORMAT, new_bw_rata_reg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);
    
    //3配置检测需要的使能位
    //设置0x2E  ADXL345_INT_ENABLE  中的D6~D2
    //先读取当前寄存器值
    retvalue = adxl345_read_register(ADXL345_INT_ENABLE,&old_int_enable_reg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    new_int_enable_reg = old_int_enable_reg\
                                    |(config->enabled_detects.single_tap<<6)\
                                    |(config->enabled_detects.double_tap<<5)\
                                    |(config->enabled_detects.activity<<4)\
                                    |(config->enabled_detects.inactivity<<3)\
                                    |(config->enabled_detects.free_fall<<2);
    
    // 写入寄存器设置值
    retvalue = adxl345_write_register(ADXL345_INT_ENABLE, new_int_enable_reg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    //回调注册  
    acc_config_1.acc_cb = config->acc_cb;
}

/******************************************************************************
Function:
    void acc_process_int_event()
Description: 
      处理中断, 这个函数被APP调用, 在这个函数中会调用acc_cb
Input:       
Output:    
Return: 
Others: 
*******************************************************************************/
void acc_process_int_event()
{
    //读取中断源寄存器
    uint8_t regdata = 0x00;
    uint8_t retvalue = 0;
    uint8_t int_type = 0;
    uint8_t single_tap_bit = 0;
    uint8_t double_tap_bit = 0;
    uint8_t activity_bit = 0;
    uint8_t inactivity_bit = 0;
    uint8_t free_fall_bit = 0;

    retvalue = adxl345_read_register(ADXL345_INT_SOURCE,&regdata);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    single_tap_bit = (regdata>>6)&0x01;
    double_tap_bit = (regdata>>5)&0x01;
    activity_bit = (regdata>>4)&0x01;
    inactivity_bit = (regdata>>3)&0x01;
    free_fall_bit = (regdata>>2)&0x01;

    if (single_tap_bit)
    {
        int_type = ACC_SINGLE_TAP_EVENT_TYPE;
    }
    else if (double_tap_bit)
            {
                int_type = ACC_DOUBLE_TAP_EVENT_TYPE;
            }
        else if (activity_bit)
                {
                    int_type = ACC_ACTIVITY_EVENT_TYPE;
                }
            else if (inactivity_bit)
                    {
                        int_type = ACC_INACTIVITY_EVENT_TYPE;
                    }
                else if (free_fall_bit)
                        {
                            int_type = ACC_FREE_FALL_EVENT_TYPE;
                        }

    acc_event_t acc_ir;
    acc_ir.type = int_type;
    if (NULL != acc_config_1.acc_cb)
    {
        acc_config_1.acc_cb(&acc_ir); 
    }       
}

/******************************************************************************
Function:
    uint8_t acc_get_detect_data(acc_data_t acc_data_buf[],
                                                    uint8_t    max_size)
Description: 
      读取加速度数据       
  
Input:
      无  
Output:
       acc_data_t acc_data_buf[]  用来存放读取的加速度数据
       buf_max_size 在acc_data_buf中最多可以存放的数据条数
Return: 
       TRUE,  读取成功
       FALSE, 读取失败
Others: 
*******************************************************************************/
uint8_t acc_get_detect_data(acc_data_t acc_data_buf[],
                            uint8_t      max_size)
{
    int16_t pacc_x = 0 ;
    int16_t pacc_y = 0 ;
    int16_t pacc_z = 0 ;
    uint8_t retvalue = 0;
    uint8_t fifo_status_reg = 0;
    uint8_t entries = 0;//采样点个数

    //首先获得当前FIFO中有多少个采样点，读取寄存器0x39  
    // ADXL345_FIFO_STATUS  [D5~D0]    
    retvalue = adxl345_read_register(ADXL345_FIFO_STATUS,&fifo_status_reg);
    DBG_ASSERT(I2C_OK == retvalue __DBG_LINE);

    entries = fifo_status_reg&0x3f;
    if (entries > max_size)
    {
        entries = max_size;
    }

    for (uint8_t i = 0;i < entries;i++)
    {
        adxl345_get_xyz( &pacc_x , &pacc_y , &pacc_z);
        acc_data_buf[i].x = pacc_x;
        acc_data_buf[i].y = pacc_y;
        acc_data_buf[i].z = pacc_z;
    }

    return entries;   
}


