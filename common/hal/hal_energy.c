/***************************************************************************
* File        : hal_energy.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <hal_energy.h>
#include <driver.h>

void hal_energy_init(void)
{
    energy_init();
}

uint8_t hal_energy_get(void)
{
    return energy_get();
}