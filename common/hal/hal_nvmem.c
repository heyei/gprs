/***************************************************************************
* File        : hal_nvmem.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#include <hal.h>
void hal_nvmem_write(uint8_t *flash_ptr,
                     const uint8_t *buffer,
                     uint16_t len)
{
    hal_int_state_t status;    
    HAL_ENTER_CRITICAL(status);
    
	uint16_t i = 0;	
	uint32_t uint_ptr = (uint32_t)flash_ptr;
		
	if (uint_ptr == INFO_A_ADDR)
	{
		if(FCTL3 & LOCKA)
		{
			FCTL3 = FWKEY + LOCKA;	
		}
		else
		{
			FCTL3 = FWKEY;
		}
	}
	else
	{
		FCTL3 = FWKEY;
	}

	FCTL1 = FWKEY + WRT;	
	for (i=0; i<len; i++)
	{
		*flash_ptr++ = *buffer++;
	}	
	FCTL1 = FWKEY;
	
	if (uint_ptr == INFO_A_ADDR)
	{
		if(!(FCTL3&LOCKA))
		{
			FCTL3 = FWKEY + LOCKA + LOCK;
			return;
		}
	}
    FCTL3 = FWKEY+LOCK;
    HAL_EXIT_CRITICAL(status);
}

void hal_nvmem_read(const uint8_t *flash_ptr,
                    uint8_t *buffer,
                    uint16_t len)
{
	uint16_t i = 0;
    hal_int_state_t status;    
    HAL_ENTER_CRITICAL(status);
	for (i=0; i<len; i++)
	{
		*buffer++ = *flash_ptr++;
	}
    HAL_EXIT_CRITICAL(status);
}

void hal_nvmem_erase(uint8_t *const flash_ptr,
                     uint8_t erase_type)
{
	uint32_t uint_ptr = (uint32_t)flash_ptr;
    hal_int_state_t status;    
    HAL_ENTER_CRITICAL(status);
    
  	if (uint_ptr == INFO_A_ADDR)
	{
	  	if(FCTL3&LOCKA)
		{
			FCTL3 = FWKEY+LOCKA;	
		}
		else
		{
			FCTL3 = FWKEY;
		}
	}
	else
	{
	  	FCTL3 = FWKEY;
	}
  	
	if (erase_type == FLASH_SEG_ERASE)
	{
	 	FCTL1 = FWKEY+ERASE;	
	}

	if (erase_type == FLASH_BK_ERASE)
	{
	 	FCTL1 = FWKEY+MERAS;
	}

	*flash_ptr = 0;

	while (FCTL3&BUSY);	

	FCTL1 = FWKEY;
	if (uint_ptr == INFO_A_ADDR)
	{
		if(!(FCTL3&LOCKA))
		{
			FCTL3 = FWKEY + LOCKA + LOCK;
			return;
		}
	}	
	FCTL3 = FWKEY+LOCK;
    HAL_EXIT_CRITICAL(status);
}
