#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pbuf.h>
#include <socket.h>
#include <osel_arch.h>
#include <hal_socket.h>
#include <hal_timer.h>
#include <hal_board.h>

#define HEAD                    (0u)
#define HAL_SOCKET_SBUF_NUM		(5u)
#define RESEND_NUM				(2u)
#define RESEND_TIME				(MS_TO_TICK(10000u))		//单位毫秒
#define BUFFER_LENGTH           (500u)

typedef w5100_info_t hal_card_info_t;

static hal_card_info_t *hal_card_info;
static hal_socket_msg_t msg[CARD_NUM];
static hal_timer_t *discon_ok_handle = NULL;


typedef struct
{
	list_head_t     list;
	pbuf_t 			*pbuf;
	uint32_t 		time;				//记录入队列的时间
    uint16_t        seq;                //移除时使用
	uint8_t
used	:1,			//使用标记
index	:2,			//标记网口
count 	:3;			//重传次数
}hal_socket_sbuf_t;

bool_t hal_socket_tcp_client(uint8_t index);
bool_t hal_socket_tcp_server(uint8_t index);
static hal_socket_sbuf_t hal_socket_sbuf[HAL_SOCKET_SBUF_NUM];
static list_head_t cache_head;
static hal_timer_t *resend_handle = NULL;

//const uint8_t html[] ={
//0x3C,0x68,0x74,0x6D,0x6C,0x20,0x78,0x6D,0x6C,0x6E,0x73,0x3D,0x22,0x68,0x74,0x74,0x70,0x3A,0x2F,0x2F,0x77,0x77,0x77,0x2E,0x77,0x33,0x2E,0x6F,0x72,0x67,0x2F,0x31,0x39,0x39,0x39,0x2F,0x78,0x68,0x74,0x6D,0x6C,0x22,0x3E,0x0D,0x0A,0x3C,0x68,0x65,0x61,0x64,0x3E,0x0D,0x0A,0x3C,0x6D,0x65,0x74,0x61,0x20,0x68,0x74,0x74,0x70,0x2D,0x65,0x71,0x75,0x69,0x76,0x3D,0x22,0x43,0x6F,0x6E,0x74,0x65,0x6E,0x74,0x2D,0x54,0x79,0x70,0x65,0x22,0x20,0x63,0x6F,0x6E,0x74,0x65,0x6E,0x74,0x3D,0x22,0x74,0x65,0x78,0x74,0x2F,0x68,0x74,0x6D,0x6C,0x3B,0x20,0x63,0x68,0x61,0x72,0x73,0x65,0x74,0x3D,0x67,0x62,0x32,0x33,0x31,0x32,0x22,0x20,0x2F,0x3E,0x0D,0x0A,0x3C,0x74,0x69,0x74,0x6C,0x65,0x3E,0xCE,0xDE,0xB1,0xEA,0xCC,0xE2,0xCE,0xC4,0xB5,0xB5,0x3C,0x2F,0x74,0x69,0x74,0x6C,0x65,0x3E,0x0D,0x0A,0x3C,0x53,0x63,0x72,0x69,0x70,0x74,0x20,0x74,0x79,0x70,0x65,0x3D,0x22,0x74,0x65,0x78,0x74,0x2F,0x6A,0x61,0x76,0x61,0x73,0x63,0x72,0x69,0x70,0x74,0x22,0x3E,0x0D,0x0A,0x66,0x75,0x6E,0x63,0x74,0x69,0x6F,0x6E,0x20,0x76,0x6F,0x74,0x65,0x73,0x28,0x6B,0x65,0x79,0x29,0x7B,0x0D,0x0A,0x09,0x76,0x61,0x72,0x20,0x69,0x64,0x20,0x3D,0x20,0x64,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x2E,0x67,0x65,0x74,0x45,0x6C,0x65,0x6D,0x65,0x6E,0x74,0x42,0x79,0x49,0x64,0x28,0x27,0x74,0x5F,0x69,0x64,0x27,0x29,0x2E,0x76,0x61,0x6C,0x75,0x65,0x3B,0x0D,0x0A,0x09,0x69,0x66,0x28,0x69,0x64,0x20,0x3D,0x3D,0x20,0x6E,0x75,0x6C,0x6C,0x20,0x7C,0x7C,0x20,0x69,0x64,0x20,0x3D,0x3D,0x20,0x27,0x27,0x29,0x0D,0x0A,0x09,0x7B,0x0D,0x0A,0x09,0x09,0x61,0x6C,0x65,0x72,0x74,0x28,0x22,0x69,0x64,0xB2,0xBB,0xC4,0xDC,0xCE,0xAA,0xBF,0xD5,0x22,0x29,0x3B,0x0D,0x0A,0x09,0x09,0x72,0x65,0x74,0x75,0x72,0x6E,0x20,0x66,0x61,0x6C,0x73,0x65,0x3B,0x0D,0x0A,0x09,0x7D,0x0D,0x0A,0x09,0x64,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x2E,0x66,0x6F,0x72,0x6D,0x73,0x5B,0x27,0x66,0x6F,0x72,0x6D,0x31,0x27,0x5D,0x2E,0x61,0x63,0x74,0x69,0x6F,0x6E,0x20,0x3D,0x27,0x63,0x6F,0x6E,0x66,0x69,0x67,0x2E,0x68,0x74,0x6D,0x6C,0x27,0x20,0x2B,0x20,0x27,0x3F,0x69,0x64,0x3D,0x27,0x20,0x2B,0x20,0x69,0x64,0x3B,0x0D,0x0A,0x09,0x64,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x2E,0x66,0x6F,0x72,0x6D,0x73,0x5B,0x27,0x66,0x6F,0x72,0x6D,0x31,0x27,0x5D,0x2E,0x73,0x75,0x62,0x6D,0x69,0x74,0x28,0x29,0x3B,0x0D,0x0A,0x7D,0x0D,0x0A,0x3C,0x2F,0x53,0x63,0x72,0x69,0x70,0x74,0x3E,0x0D,0x0A,0x3C,0x2F,0x68,0x65,0x61,0x64,0x3E,0x0D,0x0A,0x0D,0x0A,0x3C,0x62,0x6F,0x64,0x79,0x3E,0x0D,0x0A,0x3C,0x66,0x6F,0x72,0x6D,0x20,0x6E,0x61,0x6D,0x65,0x3D,0x22,0x66,0x6F,0x72,0x6D,0x31,0x22,0x20,0x69,0x64,0x3D,0x22,0x66,0x6F,0x72,0x6D,0x31,0x22,0x20,0x74,0x61,0x72,0x67,0x65,0x74,0x3D,0x22,0x5F,0x74,0x6F,0x70,0x22,0x20,0x6D,0x65,0x74,0x68,0x6F,0x64,0x3D,0x22,0x70,0x6F,0x73,0x74,0x22,0x3E,0x0D,0x0A,0x09,0x3C,0x74,0x61,0x62,0x6C,0x65,0x20,0x77,0x69,0x64,0x74,0x68,0x3D,0x22,0x35,0x30,0x30,0x22,0x20,0x62,0x6F,0x72,0x64,0x65,0x72,0x3D,0x22,0x30,0x22,0x20,0x63,0x65,0x6C,0x6C,0x70,0x61,0x64,0x64,0x69,0x6E,0x67,0x3D,0x22,0x32,0x22,0x20,0x63,0x65,0x6C,0x6C,0x73,0x70,0x61,0x63,0x69,0x6E,0x67,0x3D,0x22,0x31,0x22,0x20,0x62,0x67,0x63,0x6F,0x6C,0x6F,0x72,0x3D,0x22,0x23,0x39,0x39,0x39,0x39,0x39,0x39,0x22,0x3E,0x0D,0x0A,0x3C,0x74,0x72,0x3E,0x0D,0x0A,0x20,0x20,0x20,0x20,0x3C,0x74,0x68,0x20,0x63,0x6F,0x6C,0x73,0x70,0x61,0x6E,0x3D,0x22,0x32,0x22,0x3E,0xC5,0xE4,0xD6,0xC3,0x3C,0x2F,0x74,0x68,0x3E,0x0D,0x0A,0x3C,0x2F,0x74,0x72,0x3E,0x0D,0x0A,0x09,0x3C,0x74,0x62,0x6F,0x64,0x79,0x20,0x69,0x64,0x3D,0x22,0x74,0x61,0x62,0x6C,0x65,0x6C,0x73,0x77,0x22,0x3E,0x0D,0x0A,0x20,0x20,0x3C,0x74,0x72,0x20,0x77,0x69,0x64,0x74,0x68,0x3D,0x22,0x35,0x30,0x30,0x22,0x3E,0x0D,0x0A,0x20,0x20,0x20,0x20,0x3C,0x74,0x64,0x20,0x62,0x67,0x63,0x6F,0x6C,0x6F,0x72,0x3D,0x22,0x23,0x46,0x46,0x46,0x46,0x46,0x46,0x22,0x3E,0xC9,0xE8,0xB1,0xB8,0x49,0x44,0x3C,0x2F,0x74,0x64,0x3E,0x0D,0x0A,0x20,0x20,0x20,0x20,0x3C,0x74,0x64,0x20,0x62,0x67,0x63,0x6F,0x6C,0x6F,0x72,0x3D,0x22,0x23,0x46,0x46,0x46,0x46,0x46,0x46,0x22,0x3E,0x0D,0x0A,0x20,0x20,0x20,0x20,0x3C,0x69,0x6E,0x70,0x75,0x74,0x20,0x74,0x79,0x70,0x65,0x3D,0x27,0x74,0x65,0x78,0x74,0x27,0x20,0x76,0x61,0x6C,0x75,0x65,0x3D,0x27,0x27,0x20,0x69,0x64,0x3D,0x22,0x74,0x5F,0x69,0x64,0x22,0x2F,0x3E,0x0D,0x0A,0x20,0x20,0x20,0x20,0x3C,0x2F,0x74,0x64,0x3E,0x0D,0x0A,0x20,0x20,0x3C,0x2F,0x74,0x72,0x3E,0x0D,0x0A,0x20,0x20,0x3C,0x2F,0x74,0x62,0x6F,0x64,0x79,0x3E,0x0D,0x0A,0x3C,0x2F,0x74,0x61,0x62,0x6C,0x65,0x3E,0x0D,0x0A,0x09,0x3C,0x69,0x6E,0x70,0x75,0x74,0x20,0x74,0x79,0x70,0x65,0x3D,0x22,0x73,0x75,0x62,0x6D,0x69,0x74,0x22,0x20,0x76,0x61,0x6C,0x75,0x65,0x3D,0x22,0xB1,0xA3,0xB4,0xE6,0x22,0x20,0x6F,0x6E,0x43,0x6C,0x69,0x63,0x6B,0x3D,0x22,0x76,0x6F,0x74,0x65,0x73,0x28,0x27,0x36,0x63,0x35,0x64,0x66,0x64,0x64,0x33,0x37,0x36,0x38,0x38,0x61,0x36,0x36,0x65,0x35,0x61,0x63,0x66,0x66,0x31,0x61,0x36,0x31,0x61,0x35,0x31,0x30,0x63,0x38,0x61,0x27,0x29,0x3B,0x22,0x3E,0x20,0x0D,0x0A,0x3C,0x2F,0x66,0x6F,0x72,0x6D,0x3E,0x0D,0x0A,0x0D,0x0A,0x3C,0x2F,0x62,0x6F,0x64,0x79,0x3E,0x0D,0x0A,0x3C,0x2F,0x68,0x74,0x6D,0x6C,0x3E,0x0D,0x0A,0x0D,0x0A
//};

const char begin[] = "<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\"content=\"text/html; charset=gb2312\"/><title>无标题文档</title><style type=\"text/css\">.ip{border:none solid#666666;width:340px;font-size:12px}.fld{float:left;width:50px}.fld input{border:none;width:50px}.fldDot{float:left;width:5px}</style>";

const char js1[]="<Script type=\"text/javascript\">function getvalue(){document.IPForm.ipAddressValue.value=ipAddress.GetIPValue();document.IPForm.subnetMaskValue.value=subnetMask.GetIPValue();document.IPForm.defaultGatewayValue.value=defaultGateway.GetIPValue()}function IsByte(str,mark){if(str.replace(/[0-9]/gi,\"\")==\"\"){if(mark){if(parseInt(str)>=0&&parseInt(str)<=255){return true}}else{if(parseInt(str)>=0&&parseInt(str)<=65535){return true}}}return false}function OnPropertyChng(){for(var i=1;i<=5;i++){var e=document.getElementById(this.ipCtrlIdStr+\"_ipFld_\"+i);var str=e.value;if(i<5){if(!IsByte(str,true)){e.value=this.ipFldArr[i-1]}else{this.ipFldArr[i-1]=e.value}}else{if(!IsByte(str,false)){e.value=this.ipFldArr[i-1]}else{this.ipFldArr[i-1]=e.value}}}}function OnIPFldKeyDown(event,curFldIndex){if(event.keyCode==37){this.PrevIPFld(curFldIndex);event.returnValue=false}else if(event.keyCode==39||event.keyCode==110||event.keyCode==32){this.NextIPFld(curFldIndex);event.returnValue=false}}function PrevIPFld(curFldIndex){if(curFldIndex>1){document.getElementById(this.ipCtrlIdStr+\"_ipFld_\"+(--curFldIndex)).select()}}function NextIPFld(curFldIndex){if(curFldIndex<5){document.getElementById(this.ipCtrlIdStr+\"_ipFld_\"+(++curFldIndex)).select()}}";

//SetIPValue、GetIPValue
const char js2[]="function SetIPValue(ipValue){var ipFldArr=ipValue.split(\".\");if(ipFldArr.length!=4){return false}else if(!IsByte(ipFldArr[0],true)||!IsByte(ipFldArr[1],true)||!IsByte(ipFldArr[2],true)||!IsByte(ipFldArr[3],true)){return false}document.getElementById(this.ipCtrlIdStr+\"_ipFld_1\").value=ipFldArr[0];document.getElementById(this.ipCtrlIdStr+\"_ipFld_2\").value=ipFldArr[1];document.getElementById(this.ipCtrlIdStr+\"_ipFld_3\").value=ipFldArr[2];document.getElementById(this.ipCtrlIdStr+\"_ipFld_4\").value=ipFldArr[3];return true}function GetIPValue(){var fld_1=document.getElementById(this.ipCtrlIdStr+\"_ipFld_1\").value;var fld_2=document.getElementById(this.ipCtrlIdStr+\"_ipFld_2\").value;var fld_3=document.getElementById(this.ipCtrlIdStr+\"_ipFld_3\").value;var fld_4=document.getElementById(this.ipCtrlIdStr+\"_ipFld_4\").value;if(IsByte(fld_1,true)&&IsByte(fld_2,true)&IsByte(fld_3,true)&IsByte(fld_4,true)){return fld_1+\".\"+fld_2+\".\"+fld_3+\".\"+fld_4}else{return\"\"}}";

//getElements、inputSelector、input、serializeElement、serializeForm
const char js3[]="function getElements(formId){var form=document.getElementById(formId);var elements=new Array();var tagElements=form.getElementsByTagName('input');for(var j=0;j<tagElements.length;j++){elements.push(tagElements[j])}return elements}function inputSelector(element){if(element.checked)return[element.name,element.value]}function input(element){switch(element.type.toLowerCase()){case'submit':case'hidden':case'password':case'text':return[element.name,element.value];case'checkbox':case'radio':return inputSelector(element)}return false}function serializeElement(element){var method=element.tagName.toLowerCase();var parameter=input(element);if(parameter){var key=encodeURIComponent(parameter[0]);if(key.length==0)return;if(parameter[1].constructor!=Array)parameter[1]=[parameter[1]];var values=parameter[1];var results=[];for(var i=0;i<values.length;i++){results.push(key+'='+encodeURIComponent(values[i]))}return results.join('&')}}function serializeForm(formId){var elements=getElements(formId);var queryComponents=new Array();for(var i=0;i<elements.length;i++){var queryComponent=serializeElement(elements[i]);if(queryComponent)queryComponents.push(queryComponent)}return queryComponents.join('&')}";

//getFormInfo
const char js4[]="function getFormInfo(){document.getElementsByName(\"ipAddressValue\")[0].value=ipAddress.GetIPValue()+'.'+document.getElementById(\"ipAddress_ipFld_5\").value;document.getElementsByName(\"destinationValue\")[0].value=destination.GetIPValue()+'.'+document.getElementById(\"destination_ipFld_5\").value;document.getElementsByName(\"subnetMaskValue\")[0].value=subnetMask.GetIPValue();document.getElementsByName(\"defaultGatewayValue\")[0].value=defaultGateway.GetIPValue();var id=document.getElementById('t_id').value;if(id==null||id==''){alert(\"id不能为空\");return false}if(document.getElementById('t_id').value.length<16){alert(\"id长度不能小于16\");return false}var params=serializeForm('form1');document.forms['form1'].action+=params+\"&\";document.form1.submit()}";

//CIP
const char js_ip[]="function CIP(ipCtrlIdStr,objNameStr,mark,ip){this.ipCtrlIdStr=ipCtrlIdStr;this.ipCtrlId=document.getElementById(this.ipCtrlIdStr);this.objNameStr=objNameStr;var strs=new Array();strs=ip.split(\".\");if(mark){this.ipFldArr=new Array(strs[0],strs[1],strs[2],strs[3],strs[4]);this.Create=CIP_PORT_Create}else{this.ipFldArr=new Array(strs[0],strs[1],strs[2],strs[3]);this.Create=CIPCreate}this.OnPropertyChng=OnPropertyChng;this.OnIPFldKeyDown=OnIPFldKeyDown;this.PrevIPFld=PrevIPFld;this.NextIPFld=NextIPFld;this.SetIPValue=SetIPValue;this.GetIPValue=GetIPValue}";

const char js_CIPCreate[] = "function CIPCreate()\r\n" \
    "{\r\n" \
        "    var str = \"\";\r\n" \
            "    str += \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"2\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_1\\\"\" +\r\n" \
                "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,1);\\\"></div>\" +\r\n" \
                    "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                        "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_2\\\"\" +\r\n" \
                            "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,2);\\\"></div>\" +\r\n" \
                                "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                                    "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_3\\\"\" +\r\n" \
                                        "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,3);\\\"></div>\" +\r\n" \
                                            "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                                                "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_4\\\"\" +\r\n" \
                                                    "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,4);\\\"></div>\"\r\n" \
                                                        "    this.ipCtrlId.innerHTML = str;\r\n" \
                                                            "    setInterval(this.objNameStr + \".OnPropertyChng()\", 10);\r\n" \
                                                                "}\r\n";

const char js_CIP_PORT_Create[] = "function CIP_PORT_Create()\r\n" \
    "{\r\n" \
        "    var str = \"\";\r\n" \
            "    str += \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"2\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_1\\\"\" +\r\n" \
                "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,1);\\\"></div>\" +\r\n" \
                    "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                        "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_2\\\"\" +\r\n" \
                            "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,2);\\\"></div>\" +\r\n" \
                                "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                                    "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_3\\\"\" +\r\n" \
                                        "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,3);\\\"></div>\" +\r\n" \
                                            "           \"<div class=\\\"fldDot\\\">.</div>\" +\r\n" \
                                                "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_4\\\"\" +\r\n" \
                                                    "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,4);\\\"></div>\" +\r\n" \
                                                        "           \"<div class=\\\"fldDot\\\">:</div>\" +\r\n" \
                                                            "           \"<div class=\\\"fld\\\"><input type=\\\"text\\\" size=\\\"3\\\" id=\\\"\" + this.ipCtrlIdStr + \"_ipFld_5\\\"\" +\r\n" \
                                                                "           \" onkeydown=\\\"javascript:\" + this.objNameStr + \".OnIPFldKeyDown(event,5);\\\"></div>\"\r\n" \
                                                                    "    this.ipCtrlId.innerHTML = str;\r\n" \
                                                                        "    setInterval(this.objNameStr + \".OnPropertyChng()\", 10);\r\n" \
                                                                            "}\r\n";

const char body[]="</Script></head><body><form name=\"form1\"id=\"form1\"target=\"\"method=\"post\"action=\"config.html?\"><table width=\"500\"border=\"0\"cellpadding=\"2\"cellspacing=\"1\"bgcolor=\"#999999\"><tr><th colspan=\"2\">配置</th></tr><tbody id=\"tablelsw\"><tr width=\"500\"><td bgcolor=\"#FFFFFF\">设备ID</td><td bgcolor=\"#FFFFFF\"><input type='text'value='%.16s'name=\"t_id\"id=\"t_id\"maxlength=\"16\"/></td></tr><tr width=\"500\"><td bgcolor=\"#FFFFFF\">IP</td><td bgcolor=\"#FFFFFF\"><div class=\"ip\"id=\"ipAddress\"></div><input name=\"ipAddressValue\"type=\"hidden\"value=\"\"></td></tr><tr width=\"500\"><td bgcolor=\"#FFFFFF\">子网掩码</td><td bgcolor=\"#FFFFFF\"><div class=\"ip\"id=\"subnetMask\"></div><input name=\"subnetMaskValue\"type=\"hidden\"value=\"\"></td></tr><tr width=\"500\"><td bgcolor=\"#FFFFFF\">网关IP</td><td bgcolor=\"#FFFFFF\"><div class=\"ip\"id=\"defaultGateway\"></div><input name=\"defaultGatewayValue\"type=\"hidden\"value=\"\"></td></tr><tr width=\"500\"><td bgcolor=\"#FFFFFF\">目的IP</td><td bgcolor=\"#FFFFFF\"><div class=\"ip\"id=\"destination\"></div><input name=\"destinationValue\"type=\"hidden\"value=\"\"></td></tr></tbody></table><input type=\"submit\"name=\"submit\"value=\"保存\"onClick=\"getFormInfo();\"></form>%s&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"text\"readonly=\"true\"value=\"\"id=\"time\"style=\"border:0px;color:#FF0000;\">";

const char end[]="<Script type=\"text/javascript\">var ipAddress=new CIP(\"ipAddress\",\"ipAddress\",1,\"%d.%d.%d.%d.%d\");ipAddress.Create();var subnetMask=new CIP(\"subnetMask\",\"subnetMask\",0,\"%d.%d.%d.%d\");subnetMask.Create();var defaultGateway=new CIP(\"defaultGateway\",\"defaultGateway\",0,\"%d.%d.%d.%d\");defaultGateway.Create();var destination=new CIP(\"destination\",\"destination\",1,\"%d.%d.%d.%d.%d\");destination.Create();var t=%d;var time=document.getElementById(\"time\");function fun(){if(t<999){t--;time.value=t+\" 秒后跳转\";if(t<=0){location.href=\"http://%d.%d.%d.%d:%d\";clearInterval(inter)}}}var inter=setInterval(\"fun()\",1000);</Script>";

const char config_result1[] = "<font color=\"blue\">配置成功</font>";
const char config_result2[] = "<font color=\"red\">配置失败</font>";
const char head[] = "HTTP/1.1 200 OK\r\nServer: XuShengHao's Server <0.1>\r\n" \
    "Accept-Ranges: bytes\r\nContent-Length: %d\r\nConnection: close\r\n" \
        "Accept-Language:zh-cn\r\n" \
            "Content-Type: text/html\r\n\r\n";
const char jump[] = "%d%d%d%d%d%d";
static void http_frame_head_send(uint16_t len)
{
    char response[300];
    uint16_t head_len = sprintf(response, head, len);
    socket_write_fifo_send((uint8_t *)response,head_len,CARD_1,TCP_SERVICE_PORT);
}

static uint16_t  html_config_length()
{
    char response[BUFFER_LENGTH];
    device_info_t device_info = hal_board_info_get();
    uint8_t id[16];
    for (uint8_t i = 0; i < sizeof(device_info.device_id); i++)
    {
        hal_hex_to_ascii(&id[i * 2],
                         device_info.device_id[sizeof(device_info.device_id) - 1 - i]);
    }
    
    uint16_t len = 0;
    uint16_t head_len = sprintf(response, begin);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js1);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js2);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js3);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js4);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js_ip);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js_CIPCreate);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, js_CIP_PORT_Create);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    head_len = sprintf(response, body, id, "");
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    uint16_t port = BUILD_UINT16(device_info.socket_card.port[1],device_info.socket_card.port[0]);
    uint16_t dport = BUILD_UINT16(device_info.socket_card.dport[1],device_info.socket_card.dport[0]);
    head_len = sprintf(response,end,device_info.socket_card.ip_addr[0],device_info.socket_card.ip_addr[1],
                       device_info.socket_card.ip_addr[2],device_info.socket_card.ip_addr[3],port,
                       device_info.socket_card.sub_mask[0],device_info.socket_card.sub_mask[1],
                       device_info.socket_card.sub_mask[2],device_info.socket_card.sub_mask[3],
                       device_info.socket_card.gateway_ip[0],device_info.socket_card.gateway_ip[1],
                       device_info.socket_card.gateway_ip[2],device_info.socket_card.gateway_ip[3],
                       device_info.socket_card.dip_addr[0],device_info.socket_card.dip_addr[1],
                       device_info.socket_card.dip_addr[2],device_info.socket_card.dip_addr[3],dport,
                       10000,127,0,0,1,80);
    
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    len +=head_len;
    
    return len;
}

static uint16_t  html_configReplay_length(bool_t state)
{
    char response[BUFFER_LENGTH] = {0};
    uint16_t head_len = 0;
    if(state)
    {
        head_len = sprintf(response, config_result1);
    }
    else
    {
        head_len = sprintf(response, config_result2);
    }
    device_info_t device_info = hal_board_info_get();
    uint16_t port = BUILD_UINT16(device_info.socket_card.port[1],device_info.socket_card.port[0]);
    head_len += sprintf(response, jump, 11 ,device_info.socket_card.ip_addr[0],device_info.socket_card.ip_addr[1],
                        device_info.socket_card.ip_addr[2],device_info.socket_card.ip_addr[3],port);
    head_len -= sprintf(response,jump,10000,127,0,0,1,80);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    head_len += html_config_length();
    return head_len;
}

static void config_request(uint8_t replat_state)
{
    char response[BUFFER_LENGTH];
    device_info_t device_info = hal_board_info_get();
    uint8_t id[16];
    for (uint8_t i = 0; i < sizeof(device_info.device_id); i++)
    {
        hal_hex_to_ascii(&id[i * 2],
                         device_info.device_id[sizeof(device_info.device_id) - 1 - i]);
    }
    
    uint16_t len = 0;
    uint16_t head_len = sprintf(response, begin);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(50);
    
    head_len = sprintf(response, js1);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(200);
    
    head_len = sprintf(response, js2);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(200);
    
    head_len = sprintf(response, js3);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(50);
    
    head_len = sprintf(response, js4);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(50);
    
    head_len = sprintf(response, js_ip);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(50);
    
    head_len = sprintf(response, js_CIPCreate);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(200);
    
    head_len = sprintf(response, js_CIP_PORT_Create);
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(200);
    
    switch(replat_state)
    {
    case CONFIG_REPLAY_NON:
        head_len = sprintf(response, body, id, "");
        break;
    case CONFIG_REPLAY_SUCCESS:
        head_len = sprintf(response, body, id, config_result1);
        break;
    case CONFIG_REPLAY_FAILED:
        head_len = sprintf(response, body, id, config_result2);
        break;
    default:
        head_len = sprintf(response, body, id, "");
        break;
    }
    
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
    delay_ms(200);
    
    uint16_t port = BUILD_UINT16(device_info.socket_card.port[1],device_info.socket_card.port[0]);
    uint16_t dport = BUILD_UINT16(device_info.socket_card.dport[1],device_info.socket_card.dport[0]);
    if(CONFIG_REPLAY_NON == replat_state)
    {
        head_len = sprintf(response,end,device_info.socket_card.ip_addr[0],device_info.socket_card.ip_addr[1],
                           device_info.socket_card.ip_addr[2],device_info.socket_card.ip_addr[3],port,
                           device_info.socket_card.sub_mask[0],device_info.socket_card.sub_mask[1],
                           device_info.socket_card.sub_mask[2],device_info.socket_card.sub_mask[3],
                           device_info.socket_card.gateway_ip[0],device_info.socket_card.gateway_ip[1],
                           device_info.socket_card.gateway_ip[2],device_info.socket_card.gateway_ip[3],
                           device_info.socket_card.dip_addr[0],device_info.socket_card.dip_addr[1],
                           device_info.socket_card.dip_addr[2],device_info.socket_card.dip_addr[3],dport,
                           10000,127,0,0,1,80);
    }
    else
    {
        head_len = sprintf(response,end,device_info.socket_card.ip_addr[0],device_info.socket_card.ip_addr[1],
                           device_info.socket_card.ip_addr[2],device_info.socket_card.ip_addr[3],port,
                           device_info.socket_card.sub_mask[0],device_info.socket_card.sub_mask[1],
                           device_info.socket_card.sub_mask[2],device_info.socket_card.sub_mask[3],
                           device_info.socket_card.gateway_ip[0],device_info.socket_card.gateway_ip[1],
                           device_info.socket_card.gateway_ip[2],device_info.socket_card.gateway_ip[3],
                           device_info.socket_card.dip_addr[0],device_info.socket_card.dip_addr[1],
                           device_info.socket_card.dip_addr[2],device_info.socket_card.dip_addr[3],dport,
                           11,device_info.socket_card.ip_addr[0],device_info.socket_card.ip_addr[1],
                           device_info.socket_card.ip_addr[2],device_info.socket_card.ip_addr[3],port);
    }
    
    DBG_ASSERT(head_len < BUFFER_LENGTH __DBG_LINE);
    socket_write_fifo_send((uint8_t *)&response,head_len,CARD_1,TCP_SERVICE_PORT);
    len +=head_len;
}


static uint8_t strtok_ip(char *buf, char **ip, char *delim)
{
    uint8_t i = 0;
    char *p = buf;
    while( (ip[i] = strtok(p, delim)) != NULL )
    {
        i++;
        p = NULL;
    }
    return i;
}

static bool_t config_save(char *str)
{
    char *p,temp[50]={0},*ip[6];
    char *addr1,*addr2;
    uint8_t len = 0;
    int i = 0;
    device_info_t device_info = hal_board_info_get();
    
    p = strstr(str, "t_id");
    if(p != 0x00)
    {
        addr1 = strstr(p, "=") + 1;
        osel_memcpy(temp,addr1,16);
        uint8_t key = 0;
        for(i = 0; i < 8; i++)
        {
            hal_ascii_to_hex(temp[i*2], temp[i*2 + 1], &key);
            device_info.device_id[7-i] = key;
        }
        memset(temp,0,50);
    }
    
    p = strstr(str, "ipAddressValue");
    if(p != 0x00)
    {
        addr1 = strstr(p, "=") + 1;
        addr2 = strstr(p, "&");
        len = addr2 - addr1;
        osel_memcpy(temp,addr1,len);
        uint8_t length = strtok_ip(temp, ip, ".");
        if(5 != length)
        {
            return FALSE;
        }
        for(i=0; i<4; i++)
        {
            device_info.socket_card.ip_addr[i] = atoi(ip[i]);
        }
        uint16_t port = S2B_UINT16(atoi(ip[4]));
        osel_memcpy(device_info.socket_card.port, (uint8_t*)&port, 2);
        memset(temp,0,50);
    }
    
    p = strstr(str, "destinationValue");
    if(p != 0x00)
    {
        addr1 = strstr(p, "=") + 1;
        addr2 = strstr(p, "&");
        len = addr2 - addr1;
        osel_memcpy(temp,addr1,len);
        uint8_t length = strtok_ip(temp, ip, ".");
        if(5 != length)
        {
            return FALSE;
        }
        for(i=0; i<4; i++)
        {
            device_info.socket_card.dip_addr[i] = atoi(ip[i]);
        }
        uint16_t dport = S2B_UINT16(atoi(ip[4]));
        osel_memcpy(device_info.socket_card.dport, (uint8_t*)&dport, 2);
        memset(temp,0,50);
    }
    
    p = strstr(str, "defaultGatewayValue");
    if(p != 0x00)
    {
        addr1 = strstr(p, "=") + 1;
        addr2 = strstr(p, "&");
        len = addr2 - addr1;
        osel_memcpy(temp,addr1,len);
        uint8_t length = strtok_ip(temp, ip, ".");
        if(4 != length)
        {
            return FALSE;
        }
        for(i=0; i<4; i++)
        {
            device_info.socket_card.gateway_ip[i] = atoi(ip[i]);
        }
        memset(temp,0,50);
    }
    
    hal_board_info_save(&device_info, TRUE);
    return TRUE;
}


static void http_parse(hal_socket_msg_t *W5100, char *str)
{
    char *p,temp[200];
    char *addr1,*addr2;
    //判断请求类型(get or post)
    p = str;
    addr1 = strstr(p, " ");
    osel_memcpy(temp,p,(addr1-p));
    
    if(osel_memcmp(temp,"GET",3))
    {
        p += 4;
        addr1 = strstr(p, "/");
        p += addr1 - p;
        addr2 = strstr(p, " ");
        if((addr2-addr1) == 1)
        {//请求主页
            http_frame_head_send(html_config_length());
            config_request(CONFIG_REPLAY_NON);
        }
        else
        {
            osel_memcpy(temp,addr1,(addr2-addr1));
            if(osel_memcmp(temp,"/favicon.ico",12))
            {
                http_frame_head_send(html_config_length());
                config_request(CONFIG_REPLAY_NON);
            }
        }
    }
    if(osel_memcmp(temp,"POST",4))
    {
        p = str;
        if(strstr(p, "config.html"))
        {
            bool_t mark = config_save(str);
            http_frame_head_send(html_configReplay_length(mark));
            config_request(CONFIG_REPLAY_SUCCESS);
            hal_board_reset();
        }
        else if(strstr(p, "null.html"))
        {
            
        }
    }
}

//static uint8_t eo_check(uint8_t *data_p, uint8_t len)
//{
//	uint8_t eo_result = 0;
//	uint8_t *temp_p = data_p;
//
//	eo_result = temp_p[0];
//	while (len-- != 1)
//	{
//		eo_result = eo_result ^ temp_p[1];
//		temp_p++;
//	}
//
//	return eo_result;
//}

static void tx_ok_cb(uint8_t index,uint8_t port)
{
	//发送消息
	msg[index].card = index;
	msg[index].msg = SOCKET_TX_OK_INT;
    osel_post(APP_LAN_EVENT, (osel_param_t*)&msg[index], OSEL_EVENT_PRIO_LOW);
    hal_wdt_clear(260000);
}

static void rx_ok_cb(uint8_t index,uint8_t port)
{
	//接收消息
	hal_socket_recv_disenable();
	msg[index].card = index;
    msg[index].msg = SOCKET_RX_OK_INT;
    msg[index].port = port;
    osel_post(APP_LAN_EVENT, (osel_param_t*)&msg[index], OSEL_EVENT_PRIO_LOW);
}

static void con_ok_cb(uint8_t index,uint8_t port)
{
	//发送消息
    if(port == TCP_SERVICE_PORT)
    {
        if(socket_get_rxfifo_cnt(index,port) > 4)
        {
            if(hal_card_info[index].recv_state)
            {
                msg[index].card = index;
                msg[index].msg = SOCKET_RX_OK_INT;
                msg[index].port = port;
                hal_socket_recv_disenable(index);
                osel_post(APP_LAN_EVENT, (osel_param_t*)&msg[index], OSEL_EVENT_PRIO_LOW);
            }
            socket_interrupt_clear(index,port); //在这里补偿清掉中断
        }
    }
    else
    {
        msg[index].card = index;
        msg[index].msg = SOCKET_CON_OK_INT;
        hal_card_info[index].cnn_state = TRUE;
        PRINTF("连接\r\n");
        osel_post(APP_LAN_EVENT, (osel_param_t*)&msg[index], OSEL_EVENT_PRIO_LOW);
    }
}

static void card_cfg_init(uint8_t index,uint8_t port);
static void discon_ok_cb(uint8_t index,uint8_t port);
bool_t hal_socket_udp_client(uint8_t index);
static void discon_timer_cb(void *param)
{
    hal_socket_msg_t *lan_msg = (hal_socket_msg_t *)param;
    uint8_t index = lan_msg->card;
	discon_ok_handle = NULL;
	if(hal_card_info[index].cnn_state != TRUE)
	{
        hal_socket_tcp_client(index);
	}
}

static void discon_ok_cb(uint8_t index,uint8_t port)
{
	//发送消息
    if((hal_card_info[index].msg_mode & TCP_CLIENT_MODE) == TCP_CLIENT_MODE)
    {
        hal_card_info[index].cnn_state = FALSE;
        msg[index].card = index;
        PRINTF("断开连接\r\n");
        if(discon_ok_handle == NULL)
        {
            HAL_TIMER_SET_REL(MS_TO_TICK((5000ul)),
                              discon_timer_cb,&msg[index],discon_ok_handle);
        }
        msg[index].card = index;
        msg[index].msg = SOCKET_DISCON_OK_INT;
        osel_post(APP_LAN_EVENT, (osel_param_t*)&msg[index], OSEL_EVENT_PRIO_LOW);
    }
}

static void card_info_init()
{
	extern w5100_info_t w5100_dev[CARD_NUM];
	hal_card_info = &w5100_dev[0];
    device_info_t device_info = hal_board_info_get();
    if(device_info.socket_card.ip_addr[0] == 0xff)
    {
        uint8_t temp_ip_addr[4] = {192,168,1,5};
        uint8_t temp_sub_mask[4] = {255,255,255,0};
        uint8_t temp_gateway_ip[4] = {192,168,1,2};
        uint8_t temp_port[2] = {0x10,0xe1};
//        uint8_t temp_dip_addr[4] = {255,255,255,255};
        uint8_t temp_dip_addr[4] = {221,6,106,148};
        
        osel_memcpy(hal_card_info[1].ip_addr, temp_ip_addr, 4);
        osel_memcpy(hal_card_info[1].sub_mask, temp_sub_mask, 4);
        osel_memcpy(hal_card_info[1].gateway_ip, temp_gateway_ip, 4);
        osel_memcpy(hal_card_info[1].port, temp_port, 2);
        osel_memcpy(&hal_card_info[1].phy_addr[1], &device_info.device_id[0], 5);	//这里的内网物理地址选择硬件分配地址
        hal_card_info[1].msg_mode = 0;
        osel_memcpy(hal_card_info[1].dip_addr, temp_dip_addr, 4);
        osel_memcpy(hal_card_info[1].dport, temp_port, 2);
        
        
        osel_memcpy(device_info.socket_card.ip_addr, temp_ip_addr, 4);
        osel_memcpy(device_info.socket_card.sub_mask, temp_sub_mask, 4);
        osel_memcpy(device_info.socket_card.gateway_ip, temp_gateway_ip, 4);
        osel_memcpy(device_info.socket_card.port, temp_port, 2);
        osel_memcpy(&device_info.socket_card.phy_addr[1], &device_info.device_id[0], 5);	//这里的内网物理地址选择硬件分配地址
        device_info.socket_card.msg_mode = 0;
        osel_memcpy(device_info.socket_card.dip_addr, temp_dip_addr, 4);
        osel_memcpy(device_info.socket_card.dport, temp_port, 2);
        hal_board_info_save(&device_info, TRUE);
    }
    else
    {
        osel_memcpy(hal_card_info[1].ip_addr, device_info.socket_card.ip_addr, 4);
        osel_memcpy(hal_card_info[1].sub_mask, device_info.socket_card.sub_mask, 4);
        osel_memcpy(hal_card_info[1].gateway_ip, device_info.socket_card.gateway_ip, 4);
        osel_memcpy(hal_card_info[1].port, device_info.socket_card.port, 2);
        osel_memcpy(&hal_card_info[1].phy_addr[1], &device_info.device_id[0], 5);	//这里的内网物理地址选择硬件分配地址
        hal_card_info[1].msg_mode = 0;
        osel_memcpy(hal_card_info[1].dip_addr, device_info.socket_card.dip_addr, 4);
        osel_memcpy(hal_card_info[1].dport, device_info.socket_card.dport, 2);
    }
    
	for(int i=0;i<CARD_NUM;i++)
	{
        hal_card_info[i].cnn_state = FALSE;
		hal_card_info[i].recv_state = TRUE;
	}
}

static void card_cfg_init(uint8_t index,uint8_t port)
{
	socket_conf_init(index,port);
}

static void register_cb()
{
	socket_int_reg[SOCKET_RX_OK_INT] = rx_ok_cb;
	socket_int_reg[SOCKET_TX_OK_INT] = tx_ok_cb;
	socket_int_reg[SOCKET_CON_OK_INT] = con_ok_cb;
	socket_int_reg[SOCKET_DISCON_OK_INT] = discon_ok_cb;
}

static hal_socket_sbuf_t* socket_sbuf_malloc()
{
	hal_socket_sbuf_t *sbuf = NULL;
	for(int i=0;i<HAL_SOCKET_SBUF_NUM;i++)
	{
		if(hal_socket_sbuf[i].used == FALSE)
		{
			hal_socket_sbuf[i].used = TRUE;
			sbuf = &hal_socket_sbuf[i];
			sbuf->time = hal_timer_now().w;
			return sbuf;
		}
	}
	return NULL;
}

static void socket_sbuf_free(hal_socket_sbuf_t *sbuf)
{
	sbuf->count = RESEND_NUM;
	sbuf->time = 0;
	pbuf_free(&sbuf->pbuf __PLINE2);
	sbuf->pbuf = NULL;
	sbuf->index = 0;
	sbuf->used = FALSE;
}

static void cache_init()
{//重传数组初始化
	list_init(&cache_head);
	for(int i=0;i<HAL_SOCKET_SBUF_NUM;i++)
	{
		hal_socket_sbuf[i].count = RESEND_NUM;
		hal_socket_sbuf[i].time = 0;
		hal_socket_sbuf[i].index = 0;
		hal_socket_sbuf[i].pbuf = NULL;
		hal_socket_sbuf[i].used = FALSE;
	}
}
void hal_socket_recv_enable()
{
    IINCHIP_ISR_ENABLE();
}

void hal_socket_recv_disenable()
{
    IINCHIP_ISR_DISABLE();
}

bool_t hal_socket_card_cnn_state(uint8_t index)
{
	return hal_card_info[index].cnn_state;
}

uint16_t hal_socket_rxfifo_cnt(hal_socket_msg_t *W5100)
{
	return socket_get_rxfifo_cnt(W5100->card,W5100->port);
}

void hal_socket_rxfifo_clear(uint8_t index)
{
    
}

bool_t hal_socket_tcp_server(uint8_t index)
{
    card_cfg_init(index,TCP_SERVICE_PORT);
    hal_card_info[index].msg_mode |= TCP_SERVER_MODE;
	return socket_mode(index,hal_card_info[index].msg_mode,TCP_SERVICE_PORT);
}

bool_t hal_socket_tcp_client(uint8_t index)
{
    card_cfg_init(index,PORT_0);
    hal_card_info[index].msg_mode |= TCP_CLIENT_MODE;
	return socket_mode(index,hal_card_info[index].msg_mode,PORT_0);
}

bool_t hal_socket_udp_client(uint8_t index)
{
    hal_card_info[index].cnn_state = TRUE;
    card_cfg_init(index,PORT_0);
//    card_cfg_init(index,PORT_1);
    hal_card_info[index].msg_mode |= UDP_CLIENT_MODE;

	return socket_mode(index,hal_card_info[index].msg_mode,PORT_0);
}

static uint16_t filter_udp(hal_socket_msg_t *W5100)
{
	uint8_t udp_head[8] = {0};
	uint16_t len = 0;
	if (socket_get_rxfifo_cnt(W5100->card,W5100->port) > 8)
	{
		socket_read_fifo(&udp_head[0],8,W5100->card,W5100->port);
		len = BUILD_UINT16(udp_head[7],udp_head[6]);
	}
	return len;
}

static bool_t read_head(hal_socket_msg_t *W5100)
{
	uint8_t head[2]={0};
	while (socket_get_rxfifo_cnt(W5100->card,W5100->port) > 2)
	{
		socket_read_fifo(&head[0],1,W5100->card,W5100->port);
		if (head[0] == 0xd5)
		{
			socket_read_fifo(&head[1],1,W5100->card,W5100->port);
			if (head[1] == 0xc8)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

static bool_t read_len(hal_socket_msg_t *W5100,uint16_t *data_len)
{
	uint8_t len[2]={0};
	if (socket_get_rxfifo_cnt(W5100->card,W5100->port)>2)
	{
		socket_read_fifo(&len[0],2,W5100->card,W5100->port);
		osel_memcpy(data_len, &len[0], 2);
		*data_len = S2B_UINT16(*data_len);	//全部使用大端模式
		if (*data_len < UART_LEN_MAX && *data_len >= 1)
		{
			return TRUE;
		}
	}
	return FALSE;
}

static bool_t read_len_only(hal_socket_msg_t *W5100,uint16_t *data_len)
{
	uint8_t len[4]={0};
	if (socket_get_rxfifo_cnt(W5100->card,W5100->port)>4)
	{
		socket_read_fifo_only(&len[0],4,W5100->card,W5100->port);
		osel_memcpy(data_len, &len[2], 2);
		*data_len = S2B_UINT16(*data_len);	//全部使用大端模式
		if (*data_len < UART_LEN_MAX && *data_len >= 1)
		{
			return TRUE;
		}
	}
	return FALSE;
}

static bool_t read_data(hal_socket_msg_t *W5100,uint8_t *buf,uint16_t *data_len)
{
    uint16_t len= 0;
    uint8_t index = W5100->card;
    len = socket_get_rxfifo_cnt(index,W5100->port);
    if(((*data_len+4) > 16) && (hal_card_info[index].msg_mode == UDP_CLIENT_MODE) && index == CARD_0)
    {//W5100upd 16个字节1个包
        socket_read_fifo(&buf[0],12,index,W5100->port);   //先读取第一包
        uint8_t bags = 0;
        bags = (*data_len - 12)/16;
        if(((*data_len - 12)%16)>0)
        {
            bags+=1;
        }
        for(int i=0; i<bags; i++)
        {
            if (!filter_udp(W5100))
            {
                return FALSE;
            }
            if(i+1 < bags)
            {
                socket_read_fifo(&buf[12+i*16],16,index,W5100->port);
            }
            else
            {
                socket_read_fifo(&buf[12+i*16],(*data_len-12),index,W5100->port);
            }
        }
    }
    else
    {
        if ( len >= *data_len)
        {
            socket_read_fifo(&buf[0],*data_len,index,W5100->port);
            return TRUE;
        }
    }
    
	return TRUE;
}

bool_t hal_tcp_service_read(hal_socket_msg_t *W5100)
{
    char temp[BUFFER_LENGTH];
    uint16_t length = socket_get_rxfifo_cnt(W5100->card,W5100->port);
    if(length < BUFFER_LENGTH)
    {
        socket_read_fifo((uint8_t *)&temp[0],length,W5100->card,W5100->port);
        http_parse(W5100,temp);
    }
    else
    {
        socket_read_fifo((uint8_t *)&temp[0],BUFFER_LENGTH,W5100->card,W5100->port);
    }
    //    for(int i=0;i<length;i++)
    //    {
    //        PRINTF("%c",temp[i]);
    //    }
	return TRUE;
}

bool_t hal_socket_read(hal_socket_msg_t *W5100)
{
	uint16_t data_len = 0;
	if ((hal_card_info[W5100->card].msg_mode & UDP_CLIENT_MODE) == UDP_CLIENT_MODE)			//判断网卡类型
	{
		//过滤UDP包
		data_len = filter_udp(W5100);
		if (data_len == 0)
		{
			return FALSE;
		}
	}
	else
	{//TCP需要判断帧头帧长
        //        uint8_t temp[1024];
        //        uint16_t length = socket_get_rxfifo_cnt(index,PORT_0);
        //        socket_read_fifo((uint8_t *)&temp[0],length,index,PORT_0);
        //        for(int i=0;i<length;i++)
        //        {
        //            PRINTF("%c",temp[i]);
        //        }
#if HEAD == 1
		if (!read_head(W5100))
		{
			return FALSE;
		}
		if (!read_len(W5100,&data_len))
		{
			return FALSE;
		}
#else
        if (!read_len_only(W5100,&data_len))
        {
            return FALSE;
        }
#endif
	}
    
    
	pbuf_t *frm_buf = pbuf_alloc(data_len __PLINE1);
	if (frm_buf == NULL)
	{
		return FALSE;
	}
    
	if (!read_data(W5100,frm_buf->data_p,&data_len))
	{
		return FALSE;
	}
    
	if(W5100->card == CARD_0)	//内网需要验证CRC
	{
        //		if (eo_check(frm_buf->data_p,(data_len-1)) != frm_buf->data_p[data_len-1])	//校验
        //		{
        //			pbuf_free(&frm_buf __PLINE2);
        //			return FALSE;
        //		}
	}
	frm_buf->data_len = data_len;
	if(W5100->card == CARD_0)
	{
        
	}
	else if(W5100->card == CARD_1)
	{
		osel_post(APP_NORTH_DATA_EVENT, frm_buf, OSEL_EVENT_PRIO_LOW);
	}
	return TRUE;
}

static void socket_send_again(hal_socket_sbuf_t *sbuf)
{
	if(sbuf->count != 0)
	{
		osel_int_status_t s;
		socket_write_fifo_send(sbuf->pbuf->head, sbuf->pbuf->data_len, sbuf->index, PORT_0);
		sbuf->count--;
		sbuf->time = hal_timer_now().w;
		OSEL_ENTER_CRITICAL(s);
		list_add_to_tail(&(sbuf->list), &cache_head);
		OSEL_EXIT_CRITICAL(s);
	}
	else
	{
		socket_sbuf_free(sbuf);
	}
}
static void socket_send_again_start();
static void socket_send_again_cb(void *p)
{
	osel_int_status_t s;
	OSEL_ENTER_CRITICAL(s);
	hal_socket_sbuf_t *sbuf = list_entry_decap(&cache_head, hal_socket_sbuf_t, list);
    if(sbuf != NULL)
    {
        socket_send_again(sbuf);
        OSEL_EXIT_CRITICAL(s);
        resend_handle = NULL;
        socket_send_again_start();
    }
    else
    {
        OSEL_EXIT_CRITICAL(s);
    }
}

static void socket_send_again_start()
{
	if(resend_handle == NULL)
	{
		if(!list_empty(&cache_head))
		{
			hal_socket_sbuf_t *first = NULL;
			first = (hal_socket_sbuf_t *)list_first_elem_look(&cache_head);
			uint32_t now = hal_timer_now().w;
			int32_t abs_time = now - first->time;
			if(abs_time < RESEND_TIME)
			{
				uint32_t t = RESEND_TIME - abs_time;
				HAL_TIMER_SET_REL(t,
                                  socket_send_again_cb,
                                  NULL,
                                  resend_handle);
			}
			else
			{
				socket_send_again_cb(NULL);
			}
		}
	}
}

void hal_socket_cnn(uint8_t index, uint8_t port)
{
    socket_cnn(index,port);
}

bool_t hal_socket_send(const hal_socket_buf_t *hal_socket_buf, uint8_t index)
{
	DBG_ASSERT(hal_socket_buf->buf != NULL __DBG_LINE);
	DBG_ASSERT(hal_socket_buf->length != 0 __DBG_LINE);
	if(hal_socket_card_cnn_state(index) == FALSE)
	{
		return FALSE;
	}
    
	uint8_t count = 0;
    if(!list_empty(&cache_head))
    {
        list_count(&cache_head,count);
    }
    
	if(count < HAL_SOCKET_SBUF_NUM && hal_socket_buf->ag_send == TRUE)
	{
		osel_int_status_t s;
		pbuf_t *pbuf = pbuf_alloc(hal_socket_buf->length __PLINE1);
		DBG_ASSERT(pbuf != NULL __DBG_LINE);
		osel_memcpy(pbuf->head,hal_socket_buf->buf,hal_socket_buf->length);
		pbuf->data_len = hal_socket_buf->length;
		hal_socket_sbuf_t *sbuf = socket_sbuf_malloc();
		sbuf->pbuf = pbuf;
		sbuf->index = index;
		sbuf->seq = hal_socket_buf->seq;
		OSEL_ENTER_CRITICAL(s);
		list_add_to_tail(&(sbuf->list), &cache_head);
        socket_send_again_start();
		OSEL_EXIT_CRITICAL(s);
	}
    
    if(hal_card_info[index].recv_state == FALSE)
    {
        hal_socket_recv_enable();
    }
	return socket_write_fifo_send(&hal_socket_buf->buf[0], hal_socket_buf->length, index, PORT_0);
}

bool_t hal_socket_cache_remove(uint16_t seq)
{//从重传队列中移除
	osel_int_status_t s;
	hal_socket_sbuf_t *sbuf = NULL;
	hal_socket_sbuf_t *pos = NULL;
    if(list_empty(&cache_head))
    {
        return FALSE;
    }
    if(resend_handle!=NULL)
    {
        hal_timer_cancel(&resend_handle);
    }
	list_entry_for_each_safe(pos,sbuf,&cache_head,hal_socket_sbuf_t,list)
	{
		if(pos != NULL && pos->seq == seq)
		{
			OSEL_ENTER_CRITICAL(s);
			list_del(&(pos->list));
            socket_sbuf_free(pos);
			OSEL_EXIT_CRITICAL(s);
			socket_send_again_start();
			return TRUE;
		}
	}
    socket_send_again_start();
	return FALSE;
}

void hal_interrupt_maintenance(uint8_t index, uint8_t port)
{
    socket_interrupt_clear(index, port);
}

void hal_socket_init(void)
{
	socket_init();
    
	card_info_init();
    
	register_cb();
    
    
	if(!hal_socket_udp_client(CARD_1))
	{
		return;
	}
    
    //    if(!hal_socket_tcp_client(CARD_1))//该模式下不能开启tcp_server
    //    {
    //        return;
    //    }
    
    //    if(!hal_socket_tcp_server(CARD_1))
    //    {
    //        return;
    //    }
    cache_init();
}
