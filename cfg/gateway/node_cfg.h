#ifndef __NODE_CONFIG_H
#define __NODE_CONFIG_H

#include <data_type_def.h>

/*以下是协议相关配置*/
/******************************************************************************/
#define NODE_TYPE_TAG                   0x00
#define NODE_TYPE_ROUTER                0x01
#define NODE_TYPE_GATEWAY               0x02
#define NODE_TYPE_GATEWAY_MINI          0x03

#define NODE_ID                         0x26B4      // ID从9901=0x26AD开始，录入数据库
#define NODE_TYPE                       (NODE_TYPE_GATEWAY)
#define NWK_ADDR						(0xfffe)
/******************************************************************************/
#define PBUF_TYPE_MAX_NUM               (3u)    // PBUF缓冲类型上限
#define PBUF_NUM_MAX                    (40u)   // 表示各类pbuf个数的上限

#define SMALL_PBUF_BUFFER_SIZE          (40u)
#define MEDIUM_PBUF_BUFFER_SIZE         (62u)
#define LARGE_PBUF_BUFFER_SIZE          (64u)

#define SMALL_PBUF_NUM                  (0u)   // 各类PBUF缓冲体的个数
#define MEDIUM_PBUF_NUM                 (36u)
#define LARGE_PBUF_NUM                  (2u)

#define MAX_SBUF_NUM   			        (36u)   // SBUF最大个数
/******************************************************************************/
#define PKT_LEN_MAX 			        (LARGE_PBUF_BUFFER_SIZE)	// 协议中最大帧长，包含有效
#define PKT_LEN_MIN         			(6u)    // 协议中最小帧长，包含有效
#define UART_LEN_MAX         			(128u)  // RX 串口中最大帧长
/******************************************************************************/
#define TDMA_SEND_MODE                  (0u)    // 数据时隙发送
#define CSMA_SEND_MODE                  (1u)    // 数据退避发送
#define DATA_RATE                       (100u)  // 无线帧速率
#define TRAN_RESEND_TIMES				(0u)	// 传输模块重传间隔基准时间，ms
#define RF_INT_DEAL_FLOW                (1u)    // RF驱动中断调用上/下溢回调
#define RECV_RF_RATE_EN                 (0u)    // 接收数据是否进行成功率的比较
#define RECV_RF_SUCESS_RATE             (60)    // 接收数据的成功率
/******************************************************************************/
#define UART_NUM                        (1u)    // 串口数量
#define CH_SN                           (4u)   	// 无线信道编号
#define POWER_SN                        (5u)   	// 无线功率编号
#define MAX_TIMERS                      (15u)   // 定时器队列定时器数量
/******************************************************************************/
#define DEBUG_INFO_PRINT_EN 	        (0u)	// 是否打印调试信息
#define DEBUG_DEVICE_INFO_EN			(1u)	// 是否配置设备基础调试信息
#define SYSTEM_PLAN_NUM			        (1u)	// 时隙配置方案序号

/******************************************************************************/
#define VIRSION_NUMBER                  (0x10)  //版本号
#define PLATFORM						(0u)
#define PRINT_SYNC_PARA_EN				(0u)    //是否打印同步参数
#endif



