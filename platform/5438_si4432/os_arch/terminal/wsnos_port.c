/***************************************************************************
* File        : wsnos_port.c
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 *	这个文件定义了OS与硬件之间的一些配置
 *
 * file	: wsnos_port.c
 *  
 * Date	: 2011--8--04
 *  
**/

#include <wsnos_port.h>

volatile osel_uint8_t osel_int_nest = 0;

void osel_start(void)
{
    OSEL_INT_UNLOCK();
}

void osel_exit(void)
{
    ;
}

void osel_eoi(void)
{
    ;
}

