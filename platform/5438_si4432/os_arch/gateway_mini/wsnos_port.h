/**
 *	这个文件定义了OS与硬件之间的一些配置以及OS需要的数据类型结构
 *
 * file	: wsnos_port.h
 *  
 * Date	: 2011--8--04
 *  
**/

#ifndef __WSNOS_PORT_H
#define __WSNOS_PORT_H

#include <msp430.h>

typedef unsigned char osel_int_status_t;

#define OSEL_INT_LOCK()                         (_DINT())
#define OSEL_INT_UNLOCK()                       (_EINT())

#define OSEL_ENTER_CRITICAL(x)                  \
do                                              \
{                                               \
    if (__get_SR_register()&GIE)                \
    {                                           \
        x = 1;                                  \
        _DINT();                                \
    }                                           \
    else                                        \
    {                                           \
        x = 0;                                  \
    }                                           \
} while(__LINE__ == -1)

#define OSEL_EXIT_CRITICAL(x)    st(if ((x) == 1) _EINT();)

#define CODE

#include <hal_clock.h>
#define OSEL_ISR_ENTRY()                        st(++osel_int_nest;)

#define OSEL_ISR_EXIT()                         \
do                          	                \
{                                               \
    if(--osel_int_nest == 0)                    \
    {                                           \
        osel_int_status_t status = 0;           \
        OSEL_ENTER_CRITICAL(status);            \
        if(osel_int_nest == 0)                  \
        {                                       \
            osel_schedule();                    \
        }                                       \
        OSEL_EXIT_CRITICAL(status);             \
    }                                           \
} while (__LINE__ == -1)

#define OSEL_POST_EXIT()                        \
do                                              \
{                     	                        \
    osel_int_status_t status = 0;               \
    OSEL_ENTER_CRITICAL(status);                \
    if(osel_int_nest == 0)                      \
    {                                           \
        osel_schedule();                        \
    }                                           \
    OSEL_EXIT_CRITICAL(status);                 \
} while (__LINE__ == -1)

typedef unsigned char                           osel_bool_t;
typedef unsigned char                           osel_uint8_t;
typedef signed   char                           osel_int8_t;
typedef unsigned int                            osel_uint16_t;
typedef signed   int                            osel_int16_t;
typedef unsigned long                           osel_uint32_t;
typedef signed long                             osel_int32_t;

enum
{
    OSEL_FALSE,
    OSEL_TRUE
};

extern volatile osel_uint8_t osel_int_nest;

void osel_start(void);

void osel_exit(void);

void osel_eoi(void);

#endif
