#pragma once
#include <data_type_def.h>

typedef enum
{
	READY_IDLE,
	SIM_ERROR,
	GPRS_NET_ERROR,
	WORK_ON,
    SEND_DATA,
	WORK_DOWN,
}GPRS_STATE_E;

typedef struct
{
	uint8_t gprs_state;
	uint8_t dip[4];
	uint16_t port;
	bool_t mode;
}gprs_info_t;

struct gprs
{
	void (*get)(gprs_info_t *const info);
	void (*set)(const gprs_info_t *const info);
    void (*init)(void);                               
    bool_t (*deinit)(void);                            
    void (*write)(const uint8_t *const payload);   
};
void gprs_uart_inter_recv(uint8_t data);
extern const struct gprs gprs_driver;
