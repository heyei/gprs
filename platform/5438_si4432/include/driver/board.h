/**
 * provides an abstraction for peripheral device.
 *
 * @file board.h
 * @author wanger
 *
 * @addtogroup BOARD HAL Hardware Iinitialization
 * @ingroup HAL
 * @{
 */

#ifndef __BOARD_H
#define __BOARD_H

#include <node_cfg.h>

#define BLUE              (1u)
#define RED               (2u)
#define GREEN             (3u)

#if (NODE_TYPE == NODE_TYPE_TAG || NODE_TYPE == NODE_TYPE_GATEWAY_MINI)
#define LED_OPEN(color)   ((color) == BLUE) ? (P2OUT &= ~BIT0):(((color) == RED) ? (P1OUT &= ~BIT7):(P1OUT &= ~BIT7))
#define LED_CLOSE(color)  ((color) == BLUE) ? (P2OUT |= BIT0):(((color) == RED) ? (P1OUT |= BIT7):(P1OUT |= BIT7))
#define LED_TOGGLE(color) ((color) == BLUE) ? (P2OUT ^= BIT0):(((color) == RED) ? (P1OUT ^= BIT7):(P1OUT ^= BIT7))

#elif (NODE_TYPE == NODE_TYPE_GATEWAY)
#define LED_OPEN(color)   ((color) == BLUE) ? (P1OUT &= ~BIT5):(((color) == RED) ? (P1OUT &= ~BIT6):(P1OUT &= ~BIT7))
#define LED_CLOSE(color)  ((color) == BLUE) ? (P1OUT |= BIT5):(((color) == RED) ? (P1OUT |= BIT6):(P1OUT |= BIT7))
#define LED_TOGGLE(color) ((color) == BLUE) ? (P1OUT ^= BIT5):(((color) == RED) ? (P1OUT ^= BIT6):(P1OUT ^= BIT7))

#elif (NODE_TYPE == NODE_TYPE_ROUTER )
#define LED_OPEN(color)   ((color) == BLUE) ? (P1OUT &= ~BIT5):(((color) == RED) ? (P1OUT &= ~BIT6):(P1OUT &= ~BIT7))
#define LED_CLOSE(color)  ((color) == BLUE) ? (P1OUT |= BIT5):(((color) == RED) ? (P1OUT |= BIT6):(P1OUT |= BIT7))
#define LED_TOGGLE(color) ((color) == BLUE) ? (P1OUT ^= BIT5):(((color) == RED) ? (P1OUT ^= BIT6):(P1OUT ^= BIT7))

#endif

void led_init(void);

/**
 * reset system
 */
void board_reset(void);

/**
 * Initializes mcu clock, peripheral device and enable globle interrupt
 */
void board_init(void);

#endif

/**
 * @}
 */

