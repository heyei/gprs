/**
 * provides an abstraction for timer and sytem frequency
 *
 * @file timer.h
 * @author wanger
 *
 * @addtogroup HAL_TIMER HAL Timer and MCU Frequency
 * @ingroup HAL
 * @{
 */

#ifndef __TIMER_H
#define __TIMER_H

#include <data_type_def.h>

#endif

/**
 * @}
 */
