/**
 * @file hal.h
 * @author wanger
 */
#ifndef __DRIVER_H
#define __DRIVER_H

#include <node_cfg.h>

#define SMCLK           8000000

#define ACLK            32768

#define CPU_F           ((double)SMCLK)
#define delay_us(x)     __delay_cycles((long)(CPU_F*(double)x/1000000.0))
#define delay_ms(x)     __delay_cycles((long)(CPU_F*(double)x/1000.0))

#include <msp430.h>
#include <board.h>
#include <clock.h>
#include <gpio.h>
#include <rf.h>
#include <timer.h>
#include <uart.h>
#include <sensor_humiture.h>
#include <sensor_accelerated.h>
#include <debug.h>
#include <energy.h>
#include <gprs.h>
#endif

