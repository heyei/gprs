/***************************************************************************
* File        : main.c
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

#include <stack.h>
#include <hal.h>
#include <osel_arch.h>

int16_t main(void)
{
    /*看门口保护初始化过程，PUC后ACLK时钟源默认32K,初始化大约需要37ms*/
    //hal_wdt_clear(16000);
	WDTCTL = WDTPW + WDTHOLD;
#ifdef NDEBUG
    bootloader_init();
#endif
    osel_init();
    hal_board_init();
	hal_hardware_wdt_clear();
    ssn_init();
    osel_run();

    return 0;
}
