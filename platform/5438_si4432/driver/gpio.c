
#include <driver.h>
#include <node_cfg.h>
#include <wsnos.h>

static const uint16_t ptin [] = { P1IN_,  P2IN_,  P3IN_,  P4IN_,  P5IN_,
                                  P6IN_, P7IN_, P8IN_
                                };
static const uint16_t ptout[] = { P1OUT_, P2OUT_, P3OUT_, P4OUT_, P5OUT_,
                                  P6OUT_, P7OUT_, P8OUT_
                                };
static const uint16_t ptdir[] = { P1DIR_, P2DIR_, P3DIR_, P4DIR_, P5DIR_,
                                  P6DIR_, P7DIR_, P8DIR_
                                };
static const uint16_t ptsel[] = { P1SEL_, P2SEL_, P3SEL_, P4SEL_, P5SEL_,
                                  P6SEL_, P7SEL_, P8SEL_
                                };
static const uint16_t pties[] = { P1IES_, P2IES_  };
static const uint16_t ptie [] = { P1IE_,  P2IE_   };
static const uint16_t ptifg[] = { P1IFG_, P2IFG_  };

void gpio_set(pin_id_t pin_id)
{
    POUT(pin_id.pin - 1) |= (0x01 << pin_id.bit);
}

void gpio_clr(pin_id_t pin_id)
{
    POUT(pin_id.pin - 1) &= ~(0x01 << pin_id.bit);
}

void gpio_toggle(pin_id_t pin_id)
{
    POUT(pin_id.pin - 1) ^= (0x01 << pin_id.bit);
}

void gpio_make_input(pin_id_t pin_id)
{
    PDIR(pin_id.pin - 1) &= ~(0x01 << pin_id.bit);
}

bool_t gpio_is_input(pin_id_t pin_id)
{
    return ((PDIR(pin_id.pin - 1) & (0x01 << pin_id.bit)) == 0);
}

void gpio_make_output(pin_id_t pin_id)
{
    PDIR(pin_id.pin - 1) |= (0x01 << pin_id.bit);
}

bool_t gpio_is_output(pin_id_t pin_id)
{
    return ((PDIR(pin_id.pin - 1) & (0x01 << pin_id.bit)) != 0);
}

void gpio_sel(pin_id_t pin_id)
{
    PSEL(pin_id.pin - 1) |= (0x01 << pin_id.bit);
}

bool_t gpio_get(pin_id_t pin_id)
{
    return  ((PIN(pin_id.pin - 1)) & (0x01 << pin_id.bit));
}

void gpio_interrupt_enable(pin_id_t pin_id)
{
    PIE(pin_id.pin - 1) |= (0x01 << pin_id.bit);
}

void gpio_interrupt_disable(pin_id_t pin_id)
{
    PIE(pin_id.pin - 1) &= ~(0x01 << pin_id.bit);
}

void gpio_interrupt_edge(pin_id_t pin_id, uint8_t edge)
{
    if (edge == 0)
    {
        PIES(pin_id.pin - 1) &= ~(0x01 << pin_id.bit);
    }
    else
    {
        PIES(pin_id.pin - 1) |= (0x01 << pin_id.bit);
    }
}

void gpio_interrupt_clear(pin_id_t pin_id)
{
    PIFG(pin_id.pin - 1) &= ~(0x01 << pin_id.bit);
}

bool_t gpio_init(void)
{
#if (NODE_TYPE == NODE_TYPE_TAG || NODE_TYPE == NODE_TYPE_GATEWAY_MINI)
//初始化没有使用的管脚，降低功耗
    P1SEL &= ~(BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);
    P1DIR |= (BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);
	P1OUT &= ~(BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);
	
    P2SEL &= ~(BIT2 + BIT6 + BIT7);
    P2DIR |= (BIT2 + BIT6 + BIT7);
    P2OUT &= ~(BIT2 + BIT6 + BIT7);//暂时悬空：输出低
	
    P3SEL &= ~(BIT6);
    P3DIR |= (BIT6);
    P3OUT &= ~(BIT6);//暂时悬空：输出低
	
    P4SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);
    P4DIR |= (BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);
    P4OUT &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6);//暂时悬空：输出低
       
    P5SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT5 + BIT6 + BIT7);
    P5DIR |= (BIT0 + BIT1 + BIT2 + BIT3 + BIT5 + BIT6 + BIT7);
    P5OUT &= ~(BIT0 + BIT1 + BIT2 + BIT3 + BIT5 + BIT6 + BIT7);//暂时悬空：输出低
	
    P6SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3);
    P6DIR |= (BIT0 + BIT1 + BIT2 + BIT3);
    P6OUT &= ~(BIT0 + BIT1 + BIT2 + BIT3);//暂时悬空：输出低
    	
    P7SEL &= ~(BIT2 + BIT5 + BIT6 + BIT7);
    P7DIR |= (BIT2 + BIT5 + BIT6 + BIT7);
    P7OUT &= ~(BIT2 + BIT5 + BIT6 + BIT7);
	//p8.6 暂时未接	
    P8SEL &= ~(BIT6 + BIT7);
    P8DIR |= (BIT6 + BIT7);
    P8OUT &= ~(BIT6 + BIT7);//暂时悬空：输出低
	//P9.1 温度传感器,暂时未接
    P9SEL &= ~(BIT0 + BIT1 + BIT2 + BIT3);
    P9DIR |= (BIT0 + BIT1 + BIT2 + BIT3);
    P9OUT &= ~(BIT0 + BIT1 + BIT2 + BIT3);//暂时悬空：输出低
	
    P10SEL &= ~(BIT3 + BIT6 + BIT7);
    P10DIR |= (BIT3 + BIT6 + BIT7);
    P10OUT &= ~(BIT3 + BIT6 + BIT7);//暂时悬空：输出低
    PJDIR = 0xFF;
    PJOUT = 0x00;

//初始化外部有连接，但不一定使用的管脚
    //AD选择口
    P6SEL |= BIT4;//电源检测

    //gps和gprs
    //gsm_syns   gps_wakeup
	P2SEL &= ~(BIT3 + BIT4);
    P2DIR |= (BIT3 + BIT4);    
    P2OUT &= ~(BIT3 + BIT4);
	P9SEL &= ~(BIT6);
    P9DIR &= ~(BIT6);//SIM_VPP ,硬件已经连接，但功能未使用，置成输入

    P10SEL &= ~(BIT0);//GSM_RST
	P10DIR |= (BIT0);
    P10OUT |= BIT0; //nc未连接，但有上啦电阻
    
    P4SEL &= ~(BIT7);
    P4DIR |= (BIT7);
    P4OUT &= ~(BIT7);//GPS_1PPS,悬空，未连接

    //加速度传感器
    P7SEL &= ~(BIT3);
    P7DIR |= (BIT3);
    P7OUT |= (BIT3);	 //中断1未使用      

    //nfc ,全部放到模块初始化中实现
    //充电芯片bq24070
    P8SEL &= ~BIT5;
	P8DIR &= ~BIT5;//DET_VBUS置成输入      
    
    //硬件看门狗
    //初始化RST
    P1SEL &= ~BIT0;
    P1DIR |= BIT0;
    P1OUT &= ~BIT0;
     	
#endif

    return TRUE;
}

#pragma vector = PORT2_VECTOR
__interrupt void port_2_isr(void)
{
    OSEL_ISR_ENTRY();

    OSEL_ISR_EXIT();
    LPM3_EXIT;
}
