#include <driver.h>
#include <debug.h>



void led_init(void)
{
#if (NODE_TYPE == NODE_TYPE_TAG || NODE_TYPE == NODE_TYPE_GATEWAY_MINI)

    P1SEL &= ~BIT7;
    P1DIR |= BIT7;
    P1OUT |= BIT7;              // RED -- LED1
    
    P2SEL &= ~BIT0;
    P2DIR |= BIT0;
    P2OUT |= BIT0;              // BLUE -- LED2
#elif (NODE_TYPE == NODE_TYPE_ROUTER)
    P1SEL &= ~( BIT5 + BIT6 + BIT7);   // P1.5<-->led-blue
    P1DIR |= BIT5 + BIT6 + BIT7;       // P1.6<-->led-red
    P1OUT |= BIT5 + BIT6 + BIT7;       // P1.7<-->led-green   
#elif (NODE_TYPE == NODE_TYPE_GATEWAY)
    P1SEL &= ~( BIT5 + BIT6 + BIT7);   // P1.5<-->led-blue
    P1DIR |= BIT5 + BIT6 + BIT7;       // P1.6<-->led-red
    P1OUT |= BIT5 + BIT6 + BIT7;       // P1.7<-->led-green    
#endif
}

void board_reset(void)
{
    DISABLE_INT();
    WDTCTL = 0xFFFF;
}

void board_init(void)
{
    ;
}

