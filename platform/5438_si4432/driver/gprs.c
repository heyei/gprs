#include <msp430.h>
#include <wsnos.h>
#include <tfp_printf.h>
#include <hal_uart.h>
#include <hal_timer.h>
#include <gprs.h>

#define SIZE			(128u)
#define CMD_CB_NUM		(20u)
#define	PULL_UP			(P2OUT &= ~BIT1)
#define	PULL_DOWN		(P2OUT |= BIT1)
#define POWER_DOWN		(P6OUT &= ~BIT7)
#define POWER_UP		(P6OUT |= BIT7)

#define SEND_IP         "AT+CIPSTART=%s,%s\r"
#define SEND_DATA_LEN   "AT+CIPSEND=%d\r"

static const uint8_t tcp_mode[] = {"\"TCP\""};     
static const uint8_t udp_mode[] = {"\"UDP\""}; 

typedef enum
{
	E_IDLE,
	E_IDLE_REST,
	E_CNN_REST,
	E_CNN_SEND,
	E_UP,
	E_DOWN,
	E_CLOSE,
}E_STATE_E;

typedef enum
{
    AT =0,
    ATE0,
    CSMINS,
    CGATT,
    CIPSTART,
    CIPCLOSE,
    CIPSEND,   
}RECV_E;
typedef struct
{
    uint8_t *send;
    uint8_t *recv;
}gprs_cmd_t;

typedef struct
{
    uint8_t uart_port;
	uint16_t uart_speed;
}uart_info_t;

typedef struct
{
    uint8_t buf[SIZE];
    uint8_t len;
}send_t;

typedef struct
{
    uint8_t buf[SIZE];
    uint8_t offset;
}recv_t;

static volatile E_STATE_E e_state = E_CLOSE;
static volatile RECV_E recv_index;
static hal_timer_t *gprs_switch_timer = NULL;
static send_t send;
static recv_t recv;
static gprs_info_t gprs_info;
static uart_info_t uart_info;
static gprs_cmd_t gprs_cmd[CMD_CB_NUM] =
{
    "AT\r",                 "OK\r\n",
    "ATE0\r",               "OK\r\n",
    "AT+CSMINS?\r",         "OK\r\n",
    "AT+CGATT?\r",          "OK\r\n",
    "AT+CIPSTART=%s,%s\r",  "CONNECT OK\r\n",
    "AT+CIPCLOSE\r",        "OK\r\n",
    "AT+CIPSEND=%d\r",      ">",
};

static void gprs_init(void);
/**
*实现strstr函数功能
*
*@param: 两个字符串
*@return: 返回在str中出现sub_str之后的字符串。
*
*/
static const uint8_t *my_strstr(const uint8_t *str, const uint8_t *sub_str)
{
    DBG_ASSERT(str != NULL __DBG_LINE);
    DBG_ASSERT(sub_str != NULL __DBG_LINE);
    
    for(int i = 0; str[i] != '\0'; i++)
    {
        int tem = i;
        int j = 0;
        
        while(str[i++] == sub_str[j++])
        {
            if(sub_str[j] == '\0')
                return &str[tem];
        }
        i = tem;
    }
    return NULL;
}

static uint8_t my_strlen(const uint8_t *str)
{
    DBG_ASSERT(str != NULL __DBG_LINE);
    uint8_t len = 0;
    
    while(*str++ != '\0')
    {
        if(len++ >127)
            DBG_ASSERT(FALSE __DBG_LINE);
    }
    return len;
}

static uint8_t *my_strchr(uint8_t *s, const uint8_t *const c)
{
    DBG_ASSERT(s != NULL __DBG_LINE);
    DBG_ASSERT(c != NULL __DBG_LINE);
    
    while(*s != '\0')
    {
        if(*s == *c)
            return s;
        s++;
    }
    return NULL;
}

static uint8_t *ipconfig_get(void)
{
    uint8_t *ptr;
    uint8_t *ip_config = (uint8_t *)osel_mem_alloc(50 * sizeof(uint8_t));
    uint8_t *ip = (uint8_t *)osel_mem_alloc(30 * sizeof(uint8_t));
    
    osel_memset(ip_config, 0x00, 50);
    osel_memset(ip, 0x00, 30);
    
    wsnos_sprintf((char *)ip, "\"%d.%d.%d.%d\",%d", 
                  gprs_info.dip[0], gprs_info.dip[1], gprs_info.dip[2], gprs_info.dip[3], gprs_info.port);
    if(gprs_info.mode)
        wsnos_sprintf((char *)ip_config, (char *)SEND_IP, tcp_mode, ip);
    else
        wsnos_sprintf((char *)ip_config, (char *)SEND_IP, udp_mode, ip);
    
    ptr = ip_config;
    osel_mem_free(ip_config);
    osel_mem_free(ip);
    
    return ptr;
}

static uint8_t *data_len_get(uint8_t len)
{
    uint8_t *ptr;
    uint8_t *data_len = (uint8_t *)osel_mem_alloc(20 * sizeof(uint8_t));
    
    osel_memset(data_len, 0x00, 20);
    wsnos_sprintf((char *)data_len, (char *)SEND_DATA_LEN, len);
    
    ptr = data_len;
    osel_mem_free(data_len);
    
    return ptr;
}

static bool_t gprs_write(const uint8_t * cmd)
{
    OSEL_ASSERT(cmd != OSEL_NULL);
    uint8_t len =  my_strlen(cmd);
    uint8_t i;
    if(*cmd == 'A' && *(cmd+1) == 'T')
    {
        for( i=0; i<CMD_CB_NUM; i++)
        {
            if(osel_memcmp(gprs_cmd[i].send, cmd, len))
            {
                recv_index = (RECV_E)i;
                break;
            }
        }
        if(i >= CMD_CB_NUM)
        {
            return FALSE;
        }
        osel_memset(send.buf, 0, len);
        osel_memcpy(send.buf, cmd, len);
        send.len = len;
        uart_send_string(uart_info.uart_port, send.buf, send.len);
    }
    osel_memset(recv.buf, 0 , SIZE);
	recv.offset = 0;
	//uart_send_string(uart_info.uart_port, send.buf, send.len);
    return TRUE;
}

static void gprs_switch_cb(void *arg)
{
    gprs_switch_timer = NULL;
    osel_post(WSNOS_EVENT, (osel_param_t)GPRS_INTER, OSEL_EVENT_PRIO_LOW);
}
static void gprs_switch(void)
{
    switch(e_state)
    {
        case E_CLOSE:
            e_state = E_DOWN;
            gprs_switch();
            break;
            
        case E_DOWN:
            PULL_DOWN;
            e_state = E_UP;
            HAL_TIMER_SET_REL(MS_TO_TICK(1000), gprs_switch_cb, NULL, gprs_switch_timer);
            DBG_ASSERT(gprs_switch_timer != NULL __DBG_LINE);
            break;
            
        case E_UP:
            PULL_UP;
            e_state = E_IDLE;
            HAL_TIMER_SET_REL(MS_TO_TICK(5000), gprs_switch_cb, NULL, gprs_switch_timer);
            DBG_ASSERT(gprs_switch_timer != NULL __DBG_LINE);
            break;
            
        case E_IDLE:
            gprs_info.gprs_state = READY_IDLE;
            gprs_write(gprs_cmd[AT].send);
            break;
            
        case E_CNN_REST:
            e_state = E_CNN_SEND;
            gprs_info.gprs_state = WORK_DOWN;
            HAL_TIMER_SET_REL(MS_TO_TICK(11000), gprs_switch_cb, NULL, gprs_switch_timer);
            DBG_ASSERT(gprs_switch_timer != NULL __DBG_LINE);
            break;
            
        case E_CNN_SEND:
            if(gprs_info.gprs_state == WORK_ON)
            {
                e_state = E_CLOSE;
            }
            else
            {
                e_state = E_CNN_REST;
                gprs_switch();
                gprs_write(gprs_cmd[CIPSTART].send); 
            }
            break;
            
        case E_IDLE_REST:
            e_state = E_IDLE;
            HAL_TIMER_SET_REL(MS_TO_TICK(2000), gprs_switch_cb, NULL, gprs_switch_timer);
            DBG_ASSERT(gprs_switch_timer != NULL __DBG_LINE);
            break;
            
        default:
            break;
            
    }
}

static void gprs_recv_switch(uint8_t *const cmd)
{
    OSEL_ASSERT(cmd != OSEL_NULL);
    

    if(my_strstr(cmd, gprs_cmd[recv_index].recv) != NULL)
    {
        switch(recv_index)
        {
            case AT:
                gprs_write(gprs_cmd[ATE0].send);
                break;
                
            case ATE0:
                gprs_write(gprs_cmd[CSMINS].send);
                break;
                
            case CSMINS:
                uint8_t *ptr; 
                ptr = my_strchr(cmd, ",");
                if(ptr != NULL)
                {
                    ptr++;
                    if(*ptr == 0x31)
                    {
                        gprs_write(gprs_cmd[CGATT].send);
                        return;
                    }
                }
                gprs_info.gprs_state = SIM_ERROR;
                break;
                
            case CGATT:
                uint8_t *str;
                str = my_strchr(recv.buf, ":");  
                static uint8_t rest_num = 0;
                if(str != NULL)
                {
                    str+=2;
                    if(*str == 0x31)
                    {
                        rest_num = 0;
                        e_state = E_CNN_REST;
                        gprs_switch();
                        gprs_write(gprs_cmd[CIPSTART].send);
                        return;
                    }
                    else
                    {
                        if(rest_num++ < 20)
                        {
                            e_state = E_IDLE_REST;
                            gprs_init();
                        }
                        return;
                    }
                }
                gprs_info.gprs_state = GPRS_NET_ERROR;
                break;
                
            case CIPSTART:
                gprs_info.gprs_state = WORK_ON;
                break;
                
            case CIPCLOSE:
                gprs_info.gprs_state = WORK_DOWN;
                break;
                
            case CIPSEND:
                osel_memset(recv.buf, 0 , SIZE);
                recv.offset = 0;
                uart_send_string(uart_info.uart_port, send.buf, send.len);
                break;
                
            default:
                break;
        }
    }
}

void gprs_write_fifo(const uint8_t *const payload)
{
    OSEL_ASSERT(payload != OSEL_NULL);
    uint8_t len =  my_strlen(payload);
    
    gprs_cmd[CIPSEND].send = data_len_get(len);
    gprs_write(gprs_cmd[CIPSEND].send);

    osel_memset(send.buf, 0x00,SIZE);
    osel_memcpy(send.buf, payload, len);
    send.len = len;
}

static void port_init(void)
{
	P2SEL &= ~BIT1;
	P2DIR |=  BIT1;
    P10DIR |= BIT0;
	P10OUT |= BIT0;
	P6SEL &=~BIT7;//Close power
	P6DIR |= BIT7;
	P6OUT &= ~BIT7;
	
	P6SEL &=~BIT7;//EN_6130 power
	P6DIR |= BIT7;
	P6OUT |= BIT7;
	P10DIR |= BIT0;
	P10OUT |= BIT0; 
}

static void gprs_init(void)
{
    port_init();
    
    gprs_info.dip[0] = 58;
	gprs_info.dip[1] = 214;
	gprs_info.dip[2] = 236;
	gprs_info.dip[3] = 152;
	gprs_info.port = 8056;
	gprs_info.mode = TRUE;
    gprs_cmd[CIPSTART].send = ipconfig_get();
    uart_info.uart_port = HAL_UART_4;
    uart_info.uart_speed = 38400;
    uart_init(uart_info.uart_port, uart_info.uart_speed, 0);
    
    os_sig_bind(GPRS_INTER, (void *)gprs_switch);
    gprs_info.gprs_state = READY_IDLE;
    gprs_switch();
}

void gprs_uart_inter_recv(uint8_t data)
{
    recv.buf[recv.offset++] = data;
    
    if(gprs_info.gprs_state == WORK_ON)
    {
        if((my_strstr(recv.buf, "ERROR\r\n") != NULL) ||
          (my_strstr(recv.buf, "SEND FAIL\r\n") != NULL)) 
        {
            e_state = E_CNN_REST;
            gprs_switch();
            gprs_write(gprs_cmd[CIPSTART].send);
            return;
        }
    }
    gprs_recv_switch(recv.buf);      
}

static void get(gprs_info_t *const info)
{
	osel_memcpy(info, &gprs_info, sizeof(gprs_info_t));
}

static void set(const gprs_info_t *const info)
{
	osel_memcpy(&gprs_info, info, sizeof(gprs_info_t));
}

static bool_t gprs_deinit()
{
	POWER_DOWN;
	e_state = E_CLOSE;
	gprs_info.gprs_state = WORK_DOWN;
	return TRUE;
}

const struct gprs gprs_driver = 
{
	get,
	set,
	gprs_init,
	gprs_deinit,
    gprs_write_fifo,
};