/*******************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : adxl345 drive module                                                                                            
**  File        : adxl345_drive.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/02/11                                                                                                       
**                                                                                                                         
**  Purpose:                                                                                                             
**  This file contains the main functions of adxl345 drive. init adxl345 and 
get the result of x y z                                         
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/02/11   initial version  
*******************************************************************************/
#include <driver.h>
#include <wsnos.h>
#include <sensor_accelerated.h>
#include <hal_acc_sensor.h>
//#include <stdio.h>

extern acc_config_t acc_config_1;

#define ADXL345_DATA_OUT_REG_NUM    6 //X Y Z三轴6个寄存器

#define DEBUG_ADXL345_I2C       0
#if DEBUG_ADXL345_I2C
#include <stdio.h>
#endif

/*****************************************************************************
Function:
  bool_t adxl345_read_register( uint8_t reg_add , uint8_t *pvalue)
Description: 
   MSP430自动发送从机
Input:
	uint8_t reg_add    寄存器地址
Output:
    uint8_t *pvalue    out 寄存器内容
Return: 
	I2C_FAIL或I2C_OK
Others: 
******************************************************************************/
bool_t adxl345_read_register( uint8_t reg_add , uint8_t *pvalue)
{
    UCB1I2CSA = ADXL345_ADDRESS;
    UCB1CTL1 |= UCTR;                 // 写模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    UCB1TXBUF = reg_add;              // 发送数据，必须要先填充TXBUF
    // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
    while(!(UCB1IFG & UCTXIFG))
    {
        if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
        {
            return I2C_FAIL;
        }
    }

    UCB1CTL1 &= ~UCTR;                // 读模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

    while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
    // 若无应答 UCNACKIFG = 1

    UCB1CTL1 |= UCTXSTP;              // 先发送停止位

    while(!(UCB1IFG & UCRXIFG));      // 读取数据，读取数据在发送停止位之后
    *pvalue = UCB1RXBUF;

    while( UCB1CTL1 & UCTXSTP );

    return I2C_OK ;
}
/*******************************************************************************
Function:
  bool_t adxl345_write_register( uint8_t reg_add , uint8_t reg_value)
Description: 
    向寄存器中写入单个字节数据
Input:
	uint8_t reg_add    寄存器地址
	uint8_t reg_value   寄存器内容
Output:
       无
Return: 
	I2C_FAIL或I2C_OK
Others: 
********************************************************************************/
bool_t adxl345_write_register( uint8_t reg_add , uint8_t reg_value)
{
    UCB1I2CSA = ADXL345_ADDRESS;
    while( UCB1CTL1 & UCTXSTP );
    UCB1CTL1 |= UCTR;                 // 写模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位

    UCB1TXBUF = reg_add;             // 发送寄存器地址
    // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
    while(!(UCB1IFG & UCTXIFG))
    {
        if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
        {
            return I2C_FAIL;
        }
    }

    UCB1TXBUF = reg_value;            // 发送寄存器内容
    while(!(UCB1IFG & UCTXIFG));      // 等待UCTXIFG=1

    UCB1CTL1 |= UCTXSTP;
    while(UCB1CTL1 & UCTXSTP);        // 等待发送完成

    return I2C_OK ;
}
/********************************************************************************
Function:
    bool_t adxl345_read_buff(uint8_t reg_add , uint8_t *pregbuf , uint8_t  len)
Description: 
    从传感器寄存器连续读取多个字节
Input:
	uint8_t RegAddr    寄存器地址
	uint8_t Len     读取字节长度
Output:
       uint8_t *pregbuf    out 寄存器内容
Return: 
	I2C_FAIL或I2C_OK
Others: 
********************************************************************************/
bool_t adxl345_read_buff(uint8_t reg_add , uint8_t *pregbuf , uint8_t  len)
{
    UCB1I2CSA = ADXL345_ADDRESS;
    while( UCB1CTL1 & UCTXSTP );
    UCB1CTL1 |= UCTR;                 // 写模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和写控制字节

    UCB1TXBUF = reg_add;             // 发送数据
    // 等待UCTXIFG=1 与UCTXSTT=0 同时变化 等待一个标志位即可
    while(!(UCB1IFG & UCTXIFG))
    {
        if( UCB1IFG & UCNACKIFG )       // 若无应答 UCNACKIFG=1
        {
            return I2C_FAIL;
        }
    }

    UCB1CTL1 &= ~UCTR;                // 读模式
    UCB1CTL1 |= UCTXSTT;              // 发送启动位和读控制字节

    while(UCB1CTL1 & UCTXSTT);        // 等待UCTXSTT=0
    // 若无应答 UCNACKIFG = 1

    for(uint8_t i = 0; i < (len - 1); i++)
    {
        while(!(UCB1IFG & UCRXIFG));    // 读取数据
        *pregbuf++ = UCB1RXBUF;
    }

    UCB1CTL1 |= UCTXSTP;              // 在接收最后一个字节之前发送停止位

    while(!(UCB1IFG & UCRXIFG));      // 读取数据
    *pregbuf = UCB1RXBUF;

    while( UCB1CTL1 & UCTXSTP );

    return I2C_OK;
}
/*******************************************************************************
Function:
    bool_t adxl345_get_xyz( int16_t *pacc_x , int16_t *pacc_y , int16_t *pacc_z)
Description: 
    获得三轴加速度传感器各轴检测结果，转换结果为mg
Input:
	无
Output:
    int16_t *pacc_x      X轴结果
    int16_t *pacc_y      Y轴结果
    int16_t *pacc_z      Z轴结果
Return: 
	I2C_FAIL或I2C_OK
Others: 
********************************************************************************/
bool_t adxl345_get_xyz( int16_t *pacc_x , int16_t *pacc_y , int16_t *pacc_z)
{
    uint8_t accbuf[ADXL345_DATA_OUT_REG_NUM] = {0};
    uint8_t ret = I2C_FAIL;               // 读写返回值
    
    ret = adxl345_read_buff( 0x32 , accbuf , ADXL345_DATA_OUT_REG_NUM );
    DBG_ASSERT(I2C_OK == ret __DBG_LINE);

    *pacc_x = (accbuf[1] << 8 ) | accbuf[0];
    *pacc_y = (accbuf[3] << 8 ) | accbuf[2];
    *pacc_z = (accbuf[5] << 8 ) | accbuf[4];
/*
    // 转换结果调整为mg
    *pacc_x = (fp32_t)( *pacc_x * 3.9);
    *pacc_y = (fp32_t)( *pacc_y * 3.9);
    *pacc_z = (fp32_t)( *pacc_z * 3.9);
*/
    return I2C_OK;
}

/*******************************************************************************
Function:
    static void accelerated_i2c_lock_init(void)
Description: 
    使用前I2C的释放操作
Input:
	无
Output:

Return: 
	
Others: 
********************************************************************************/
static void accelerated_i2c_lock_init(void)
{    
    //P5.4 作为模拟scl，输出9个信号
    P5SEL &= ~BIT4;// P5.4置成IO口模式
	P5DIR |= BIT4; //P5.4做为输出
    P5OUT |= BIT4;
    // 主设备模拟SCL，从高到低，输出9次，使得从设备释放SDA
    for(uint8_t i=0;i<9;i++)
    {
        P5OUT |= BIT4;
        delay_ms(1);
        P5OUT &= ~BIT4;
        delay_ms(1);
    }
}
/********************************************************************************
Function:
    static void accelerated_iic_init(void)
Description: 
    I2C模块的初始化:包括使用前I2C的释放操作和寄存器的配置
Input:
    void
Output:
    void
Return: 
Others: 
*********************************************************************************/
static void accelerated_iic_init(void)
{
    accelerated_i2c_lock_init();
    
    P3SEL |= BIT7;
    P5SEL |= BIT4;
    P5DIR |= BIT4;

    UCB1CTL1 |= UCSWRST;                    //软件复位使能
    UCB1CTL0 = UCMST + UCMODE_3 + UCSYNC ;  // I2C主机模式
    UCB1CTL1 |= UCSSEL_2;                   // 选择SMCLK
    UCB1BR0 = 80;                // I2C时钟约 100hz，F(i2c)=F(smclk)/BR,smclk=8m
    UCB1BR1 = 0;
    UCB1CTL0 &= ~UCSLA10;                   // 7位地址模式
    //UCB1I2CSA = ADXL345_ADDRESS;            // ADXL345
    UCB1CTL1 &= ~UCSWRST;       //软件清除UCSWRST可以释放USCI,使其进入操作状态
}

static void accelerated_port_init(void)
{
    // 配置传感器INT2引脚：TA0 as capture interrupt
    P8SEL |= BIT2;   //config P8.2 as input capture io
    P8DIR &= ~BIT2;  //config P8.2 as input

    TA0CCTL2 = CM_1 + CCIS_1 + SCS + CAP;  //CM_1上升沿捕获，CCIS_1选择CCIxB,
                                            //SCS同步捕获，CAP捕获模式
}
/**********************************************************************
Function:
    static void accelerated_settings(void)
Description: 
    该函数在系统启动后传感器的寄存器初始配置操作
Input:
    void
Output:
    void
Return: 
Others:        
************************************************************************/
static void accelerated_settings(void)
{
#if DEBUG_ADXL345_I2C
//以下部分内容为测试I2C操作的各种函数
    uint8_t deviceid = 0x00;
    uint8_t retid = I2C_FAIL;               // 读写返回值

    // Step1:测试单字节读函数
    retid = adxl345_read_register(0x00, &deviceid);
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);
   
    if (0xE5 == deviceid)
    {
        printf("Read Byte Pass\r\n");
    }

    // Step2:测试单字节写函数
    uint8_t regvalue = 0x00;
    retid = adxl345_write_register(0x31,0x0B);  // 选择一个测试寄存器
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);

    retid = adxl345_read_register(0x31, &regvalue);// 再次读取该测试寄存器内容
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);

    if (0x0B == regvalue)
    {
        printf("Write Byte Pass\r\n");
    }

    // step3:测试多字节读函数
    uint8_t ofsx = 0x01;
    uint8_t ofsy = 0x02;
    uint8_t ofsz = 0x03;
    uint8_t reg_buf2[3] = { 0x00 , 0x00 , 0x00 };
    
    adxl345_write_register(0x1E,ofsx);
    adxl345_write_register(0x1F,ofsy);
    adxl345_write_register(0x20,ofsz);

    retid = adxl345_read_buff( 0x1E , reg_buf2 , 3);
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);

    if((reg_buf2[0] == 0x01)&&(reg_buf2[1] == 0x02)&&(reg_buf2[2] == 0x03))
    {
        printf("Read Buffer Pass\r\n");
    }

#endif
#if 0
    uint8_t device_id = 0;
    adxl345_read_register(ADXL345_DEVID,&device_id);
    DBG_ASSERT(ADXL345_ID == device_id __DBG_LINE);
    
    adxl345_write_register(ADXL345_POWER_CTL,0x00);//待机模式
    adxl345_write_register(ADXL345_DATA_FORMAT,0x19);//测量范围,正负4g，13位模式,
    adxl345_write_register(ADXL345_BW_RATE,0X0A);//输出100Hz,
    adxl345_write_register(ADXL345_INT_MAP,0xFF);//中断引脚映射到interrupt 2
    adxl345_write_register(ADXL345_OFSX,0x00);//X偏移量
    adxl345_write_register(ADXL345_OFSY,0x00);//Y偏移量
    adxl345_write_register(ADXL345_OFSZ,0x00);//Z偏移量
    adxl345_write_register(ADXL345_INT_ENABLE,0x01);//使能DATA_READY中断
    adxl345_write_register(ADXL345_FIFO_CTL,0xBF);
    adxl345_write_register(ADXL345_POWER_CTL,0x08);//进入测量测量模式           
#endif   
#if 1
    uint8_t device_id = 0;
    adxl345_read_register(ADXL345_DEVID,&device_id);
    DBG_ASSERT(ADXL345_ID == device_id __DBG_LINE);
    // ADXL345_REG_POWER_CTL[3]=0设置成待机模式,即清除测试位
    adxl345_write_register(ADXL345_POWER_CTL,0x00); 
    //BW_RATE[4]=1；即设置LOW_POWER位低功耗
    //BW_RATE[3][2][1][0]=0x07，即设置输出速率12.5HZ，Idd=34uA
    //普通，100hz
    adxl345_write_register(ADXL345_BW_RATE,0x07);                                                     
    //adxl345_write_register(ADXL345_BW_RATE,0x0A);  
   //THRESH_TAP: 比例因子为62.5mg/lsb  建议大于3g 
    //2g=0X20,,,4g=0x40,,,8g=0x80,,,16g=0xff   3.5g=0x38
    adxl345_write_register(ADXL345_THRESH_TAP,0x38); 
                                                     
    adxl345_write_register(ADXL345_OFSX,0x00);       // Z轴偏移量
    adxl345_write_register(ADXL345_OFSY,0x00);       // Y轴偏移量
    adxl345_write_register(ADXL345_OFSZ,0x00);       // Z轴偏移量
    //DUR:比例因子为625us/LSB，建议大于10ms
    //6.25ms=0x0A //12.5ms=0x14。
    adxl345_write_register(ADXL345_DUR,0x14); 
    //Latent:比例因子为1.25ms/LSB，建议大于20ms
    //2.5ms=0x02，，20ms=0x10，，，25ms=0x14
    adxl345_write_register(ADXL345_LATENT,0x14);    
    //window:比例因子为1.25ms/LSB，建议大于80ms 
    //10ms=0x08，，80ms=0x40
    adxl345_write_register(ADXL345_WINDOW,0x41);    
    //THRESH_ACT:比例因子为62.5mg/LSB，
    //2g=0X20,,,4g=0x40,,,8g=0x80,,,16g=0xff,,,//1.5g=0x18
    adxl345_write_register(ADXL345_THRESH_ACT,0x18);
    //THRESH_INACT:比例因子为62.5mg/LSB   
    //1g=0x10  //2g=0X20,,,4g=0x40,,,8g=0x80,,,16g=0xff
    adxl345_write_register(ADXL345_THRESH_INACT,0x10);
                                                    
    //TIME_INACT:比例因子为1sec/LSB      //1s=0x01                                           
    adxl345_write_register(ADXL345_TIME_INACT,0x05);
    //设置为直流耦合：当前加速度值直接与门限值比较，以确定是否运动或静止  
    //x,y,z参与检测活动或静止
    adxl345_write_register(ADXL345_ACT_INACT_CTL,0x77);
    //用于自由落地检测，比例因子为62.5mg/LSB   
    //建议设置成300mg~600mg（0x05~0x09）
    adxl345_write_register(ADXL345_THRESH_FF,0x06);       
    //所有轴的值必须小于此设置值，才会触发中断;比例因子5ms/LSB   
    //建议设成100ms到350ms(0x14~~0x46),200ms=0x28
    adxl345_write_register(ADXL345_TIME_FF,0x28);         
    //TAP_AXES:单击/双击轴控制寄存器； 
    //1）不抑制双击  2）使能x.y,z进行敲击检查
    adxl345_write_register(ADXL345_TAP_AXES,0x07);  
    // 中断使能   
    //1）DATA_READY[7]   2)SINGLE_TAP[6]  3)DOUBLE_TAP[5]  4)Activity[4]
    //5)inactivity[3]    6)FREE_FALL[2]   7)watermark[1]   8)overrun[0]
    adxl345_write_register(ADXL345_INT_ENABLE,0xfc); 
                                                    
    //INT_MAC中断映射：任意位设为0发送到INT1位，，设为1发送到INT2位
    //1）DATA_READY[7]   2)SINGLE_TAP[6]  3)DOUBLE_TAP[5]  4)Activity[4]
    //5)inactivity[3]    6)FREE_FALL[2]   7)watermark[1]   8)overrun[0] 
    adxl345_write_register(ADXL345_INT_MAP,0x40);   
    
    //1）SELF_TEST[7];2)SPI[6]; 3)INT_INVERT[5]：设置为0中断高电平有效，
    // 数据输出格式  高电平触发
    adxl345_write_register(ADXL345_DATA_FORMAT,0x0B);
    //adxl345_write_register(ADXL345_DATA_FORMAT,0x2B);// 数据输出格式  低电平触发         
                                                    //反之设为1低电平有效    rang[1][0]
    //设置 FIFO模式
    adxl345_write_register(ADXL345_FIFO_CTL,0xBF);
    // 进入测量模式
    //1)链接位[5]    2)AUTO_SLEEP[4]   3)测量位[3]  4)休眠位[2]  5)唤醒位[1][0]
    adxl345_write_register(ADXL345_POWER_CTL,0x28);
    uint8_t int_source; 
    adxl345_read_register(ADXL345_INT_SOURCE, &int_source);
#endif                                                   
}

static void accelerated_int_cfg(void)
{
  	TA0CTL = TASSEL_1 + MC_2 + TACLR;
	
	TA0CCTL2 &= ~CCIE;
    //设置传感器INT2  P8.2  TA0.CCI2B
    TA0CCTL2 &= ~CCIFG;     //初始为中断未挂起
    TA0CCTL2 |= CCIE;     // 开启中断使能
	  
    //设置传感器INT1  P7.3  TA1.CCI2B
//    TA1CCTL2 &= ~CCIFG;
//    TA1CCTL2 |= CCIE;
}

void acc_power_open()
{
    P6SEL &= ~(BIT6);
    P6DIR |= (BIT6);
    P6OUT |= (BIT6);    
}

void accelerated_sensor_init(void)
{
    //打开电源
    acc_power_open();
    
    accelerated_iic_init();//I2C模块的初始化:包括使用前I2C的释放操作和寄存器的配置

    accelerated_port_init();//配置硬件接口

    accelerated_settings();

    accelerated_int_cfg();//中断配置
}

void accelerated_int_handler(void)
{
    TA0CCTL2 &= ~CCIFG; //设置为无中断挂起
#if 1
    acc_event_t acc_ir;
    acc_ir.type = ACC_INT_EVENT_TYPE;

    if (NULL != acc_config_1.acc_cb)
    {
        acc_config_1.acc_cb(&acc_ir);
    }    
#endif  
#if 0
    uint8_t retvalue1 = 0;
    adxl345_read_register(0x30,&retvalue1);
    retvalue1 = (retvalue1>>6)&0x01;
    printf("read int_source[6]:single_tap = %d\r\n",retvalue1);
#endif  
}


