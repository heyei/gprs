
#include <driver.h>
#include <wsnos.h>
#if (NODE_TYPE == NODE_TYPE_TAG)
#include <escort_nfc.h>
void escort_dismantle_u2_int_handle(int16_t time);
void escort_dismantle_u6_int_handle(int16_t time);
void escort_dismantle_u18_int_handle();
#endif

void htimer_int_handler(void);
#pragma vector = TIMER0_A1_VECTOR
__interrupt void timer0_a1_isr(void)
{
    OSEL_ISR_ENTRY();

    switch (__even_in_range(TA0IV, 14))
    {
    case TA0IV_TA0CCR1:
    	rf_int_handler(TA1R);
        break;

    case TA0IV_TA0CCR2:
#if (NODE_TYPE == NODE_TYPE_TAG)
        accelerated_int_handler();
#endif
        break;

    case TA0IV_TA0CCR3:
#if (NODE_TYPE == NODE_TYPE_GATEWAY)
#include <socket.h>
        socket_interrupt_process(CARD_1);
#endif
#if (NODE_TYPE == NODE_TYPE_TAG)
        escort_dismantle_u6_int_handle(TA0CCR3);
#endif
        break;

    case TA0IV_TA0CCR4:
#if (NODE_TYPE == NODE_TYPE_TAG)
        escort_dismantle_u2_int_handle(TA0CCR4);
#endif
        break;

    case TA0IV_TA0IFG:
        break;

    default:
        break;
    }

    OSEL_ISR_EXIT();
    LPM3_EXIT;
}

#if (NODE_TYPE == NODE_TYPE_TAG)
#pragma vector = TIMER0_A0_VECTOR
__interrupt void timer0_a0_isr(void)
{
    OSEL_ISR_ENTRY();
	m24lr64e_int_handler();
    OSEL_ISR_EXIT();
    LPM3_EXIT;
}
#endif

#pragma vector = TIMER1_A1_VECTOR
__interrupt void timer1_a1_isr(void)
{
    OSEL_ISR_ENTRY();
    switch (__even_in_range(TA1IV, 14))
    {
    case TA1IV_TA1CCR1:
        htimer_int_handler();
        break;
    }
    OSEL_ISR_EXIT();
    LPM3_EXIT;
}

#pragma vector = TIMER1_A0_VECTOR
__interrupt void timer1_a0_isr(void)
{
    OSEL_ISR_ENTRY();
//#if (NODE_TYPE == NODE_TYPE_TAG)
//    escort_dismantle_u18_int_handle();
//#endif
    OSEL_ISR_EXIT();
    LPM3_EXIT;
}
