/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : LED                                                                                          
**  File        : escort_led.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是LED灯的处理                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <osel_arch.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <escort_led.h>

#define ESCORT_LED_DEFAULT_TIMER_PERIOD    500
#define ESCORT_LED_OPEN_TIMER_PERIOD    200

//此处调试时用8000ms，，版本固件使用100s
#define ESCORT_LED_CLOSE_TIMER_PERIOD   100000
#define ESCORT_LED_KEEP_TIMER_PERIOD    20000
#define ESCORT_LED_NFC_ERROR_TIMER_PERIOD  200

static hal_timer_t *led_timer = NULL;
static hal_timer_t *led_keep_timer = NULL;
static uint32_t led01_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
static uint32_t led02_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
static uint32_t led01_open_period = ESCORT_LED_OPEN_TIMER_PERIOD;
static uint32_t led01_close_period = ESCORT_LED_CLOSE_TIMER_PERIOD;
static uint16_t led_state = ESCORT_LED_INIT;

#define LED_DEV_ERR_COUNT_MAX_NUM       120//500ms闪一次，闪120次就是一分钟

//自检成功等持续时间定时器回调
static void led_self_check_keep_timer_cb(void *arg)
{
    led_keep_timer = NULL;
    osel_post(ESCORT_LED_SELF_CHECK_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void led_self_check_keep_timer(void)
{
    HAL_TIMER_SET_REL(MS_TO_TICK(ESCORT_LED_KEEP_TIMER_PERIOD),
                      led_self_check_keep_timer_cb,
                      NULL,
                      led_keep_timer);
    DBG_ASSERT(led_keep_timer != NULL __DBG_LINE);    
}

void escort_self_check_succ_handle(void *arg)
{
    escort_led_exit_state(ESCORT_LED_SELF_CHECK_SUCC);
}

static void led_timer_cb(void *led_temp)
{
    led_timer = NULL;
    osel_post(ESCORT_LED_TIMER_EVENT, (void *)led_temp, OSEL_EVENT_PRIO_LOW);
}

static void start_led_timer(uint8_t led01,uint8_t led02)
{
    uint8_t led_temp = 0;  
    
    if (NULL != led_timer)
    {
        hal_timer_cancel(&led_timer);
        DBG_ASSERT(led_timer == NULL __DBG_LINE);
    }

    led_temp = (led01 << 1) | (led02);
    if (led_temp == 0x2)
    {
        HAL_TIMER_SET_REL(MS_TO_TICK(led01_timer_period),
                          led_timer_cb,
                          (void *)led_temp,
                          led_timer);
        DBG_ASSERT(led_timer != NULL __DBG_LINE);        
    }
    else if (led_temp == 0x1)
    {
        HAL_TIMER_SET_REL(MS_TO_TICK(led02_timer_period),
                          led_timer_cb,
                          (void *)led_temp,
                          led_timer);
        DBG_ASSERT(led_timer != NULL __DBG_LINE);         
    }
    else if (led_temp == 0x3)
    {
        HAL_TIMER_SET_REL(MS_TO_TICK(ESCORT_LED_DEFAULT_TIMER_PERIOD),
                          led_timer_cb,
                          (void *)led_temp,
                          led_timer);
        DBG_ASSERT(led_timer != NULL __DBG_LINE);          
    }
}

static void stop_led_timer(void)
{
    if (NULL != led_timer)
    {
        hal_timer_cancel(&led_timer);
        DBG_ASSERT(led_timer == NULL __DBG_LINE);
    }
}

static void escort_led_set_state(e_escort_led_state_t state)
{    
    stop_led_timer();

    led01_open_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
    led01_close_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;    
    switch (state)
    {
    //初始状态(优先级最低 0x00)    
    case ESCORT_LED_INIT:
        hal_led_close(HAL_LED_BLUE);
        hal_led_close(HAL_LED_RED);
        break;         
    //工作状态(优先级 0x01)    
    case ESCORT_LED_WORK:
        hal_led_open(HAL_LED_BLUE);
        hal_led_close(HAL_LED_RED);
        //绿灯亮200ms，灭100s(即0.01hz闪烁)：
        //调试时设置亮200ms，灭8s
        led01_open_period = ESCORT_LED_OPEN_TIMER_PERIOD;
        led01_close_period = ESCORT_LED_CLOSE_TIMER_PERIOD;
        led01_timer_period = led01_open_period;
        start_led_timer(1,0);
        break;   
        
    //自检成功(优先级 0x02)
    case ESCORT_LED_SELF_CHECK_SUCC:
        hal_led_open(HAL_LED_BLUE);
        hal_led_open(HAL_LED_RED);   
        //持续亮20s后灭
        led_self_check_keep_timer();
        break;
        
    //可配置状态(优先级 0x10)
    case ESCORT_LED_CAN_CFG:
        //交替闪烁，绑定成功后进入工作状态
        hal_led_close(HAL_LED_BLUE);
        hal_led_open(HAL_LED_RED);
        led01_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
        led02_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
        start_led_timer(1,1);
        break;
        
     //强制拆除(优先级最高 0x20)
    case ESCORT_LED_DISMANTLE:
        hal_led_open(HAL_LED_BLUE);
        hal_led_open(HAL_LED_RED);
        led02_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
        start_led_timer(0,1);
        break;
        
    default :
        break;
    }
}

void escort_led_enter_state(e_escort_led_state_t state)
{
    e_escort_led_state_t current_state = ESCORT_LED_INIT;
    
    if (led_state & state)
    {
        return;
    }
    
    led_state |= state;
    
    for (uint16_t i = 0x8000; i != 0; i >>= 1)
    {
        if (led_state & i)
        {
            current_state = (e_escort_led_state_t)i;
            break;
        }
    }
    
    escort_led_set_state(current_state);
}

void escort_led_exit_state(e_escort_led_state_t state)
{
    e_escort_led_state_t current_state = ESCORT_LED_INIT;
    
    if (!(led_state & state))
    {
        return;
    }
    
    led_state &= ~state;
    
    for (uint16_t i = 0x8000; i != 0; i >>= 1)
    {
        if (led_state & i)
        {
            current_state = (e_escort_led_state_t)i;
            break;
        }
    }
    
    escort_led_set_state(current_state);
}

void escort_led_timer_handle(void *arg)
{
    uint8_t led = (uint32_t)arg;
    
    switch (led)
    {
    case 0x1://红灯闪烁
        hal_led_toggle(HAL_LED_RED);
        start_led_timer(0,1);        
        break;
        
    case 0x2://绿灯闪烁
        if (led01_timer_period == led01_open_period)
        {
            led01_timer_period = led01_close_period;
        }
        else
        {
            led01_timer_period = led01_open_period;
        }
        hal_led_toggle(HAL_LED_BLUE);
        start_led_timer(1,0);

        break;
        
    case 0x3://交替闪烁
        hal_led_toggle(HAL_LED_RED);
        hal_led_toggle(HAL_LED_BLUE);
        start_led_timer(1,1);        
        break;
        
    default :
        break;
    }
}

void escort_led_init(void)
{
    led_timer = NULL;
    led01_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
    led02_timer_period = ESCORT_LED_DEFAULT_TIMER_PERIOD;
    led_state = ESCORT_LED_INIT;
    escort_led_enter_state(ESCORT_LED_SELF_CHECK_SUCC);
}
