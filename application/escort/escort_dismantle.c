/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : app                                                                                          
**  File        : escort_dismantle.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是拆卸处理                                
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>
#include <osel_arch.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <escort_nfc.h>
#include <escort_led.h>
#include <escort_dismantle.h>
#include <escort_startup.h>
#include <nfc_module.h>
#include <mac_moniter.h>
#include <ssn.h>

#define WAIT_DISMANTLE_PERIOD     100//100ms
static bool_t dismantle_alarm_enabled = FALSE;
static hal_timer_t *avoid_wrong_dismantle_timer = NULL;
static hal_timer_t *wait_dismantle_timer = NULL;//发生防拆后起的定时器
extern uint8_t ssn_open_type;
//__no_init uint32_t escort_if_dismantle_key;

//车押2.5 增加解绑后全局标志位
extern uint8_t unbind_flag;

static void avoid_wrong_dismantle_timer_cb(void *arg)
{
    avoid_wrong_dismantle_timer = NULL;
}

static void start_avoid_wrong_dismantle_timer(void)
{
    HAL_TIMER_SET_REL(MS_TO_TICK(500),
                      avoid_wrong_dismantle_timer_cb,
                      NULL,
                      avoid_wrong_dismantle_timer);
    DBG_ASSERT(avoid_wrong_dismantle_timer != NULL __DBG_LINE);
}

static void wait_dismantle_timer_cb(void *type)
{
    wait_dismantle_timer = NULL;
    osel_post(ESCORT_DISMANTLE_EVENT, (void *)type, OSEL_EVENT_PRIO_LOW);
}

static void wait_dismantle_timer_start(void *type)
{
    if (NULL == wait_dismantle_timer)
    {
        HAL_TIMER_SET_REL(MS_TO_TICK(WAIT_DISMANTLE_PERIOD),
                          wait_dismantle_timer_cb,
                          type,
                          wait_dismantle_timer);
        DBG_ASSERT(wait_dismantle_timer != NULL __DBG_LINE);
    }
    else
    {
        return;
    }   
}

//发生防拆后的等待时间,即防抖处理
void escort_wait_dismantle_handle(void *type)
{
    wait_dismantle_timer_start((void *)type);    
}

void escort_trigger_dismantle_event(e_escort_dismantle_type_t type)
{
    //2015-01-09如果多次抖动进入，会起很多定时器，导致定时器挂掉，此处改成发消息
    osel_post(ESCORT_WAIT_DISMANTLE_EVENT, (void *)type, OSEL_EVENT_PRIO_LOW);
//    wait_dismantle_timer_start((void *)type);
}

void escort_dismantle_u2_int_handle(int16_t time)
{
	(void)time;	
	escort_trigger_dismantle_event(ESCORT_DISMANTLE_SHELL); 
}

void escort_dismantle_u6_int_handle(int16_t time)
{
	(void)time;
	escort_trigger_dismantle_event(ESCORT_DISMANTLE_INSTALL);   
}

void escort_dismantle_u18_int_handle(void)
{
    escort_trigger_dismantle_event(ESCORT_DISMANTLE_U18);        
}

//车押三期：此函数当任一个防拆开关被押下时，则返回 1;
bool_t escort_box_is_installed(void)
{
    return (((P8IN & BIT3) == 0x00) || ((P8IN & BIT4) == 0x00));
}

bool_t escort_u2_is_high_level(void)
{
  	return ((P8IN & BIT4) == 0x10);
}

bool_t escort_u2_is_low_level(void)
{
  	return ((P8IN & BIT4) == 0x00);
}

bool_t escort_u6_is_high_level(void)
{
  	return ((P8IN & BIT3) == 0x08);
}

bool_t escort_u6_is_low_level(void)
{
  	return ((P8IN & BIT3) == 0x00);
}

void escort_enable_dismantle_alarm(void)
{   
    dismantle_alarm_enabled = TRUE;
}

void escort_dismantle_init(void)
{
//  	TA0CTL = TASSEL_1 + MC_2 + TACLR;
    //配置底部防拆开关U6的中断引脚
    TA0CCTL3 &= ~CCIE;    
    P8SEL |= BIT3;
    P8DIR &= ~BIT3;
    TA0CCTL3 = CM_3 + CCIS_1 + SCS + CAP;  //CM_3上升沿和下降沿捕获，CCIS_1选择CCIxB,                                          //SCS同步捕获，CAP捕获模式
    TA0CCTL3 &= ~CCIFG;
    TA0CCTL3 |= CCIE;
    
	//配置底部防拆开关U2的中断引脚
    TA0CCTL4 &= ~CCIE;    
    P8SEL |= BIT4;
    P8DIR &= ~BIT4;
    TA0CCTL4 = CM_3 + CCIS_1 + SCS + CAP;  //CM_3上升沿和下降沿捕获，CCIS_1选择CCIxB,                                            //SCS同步捕获，CAP捕获模式
    TA0CCTL4 &= ~CCIFG;
    TA0CCTL4 |= CCIE;	
		
	//配置顶部防拆开关U18的中断引脚
//    TA1CCTL0 &= ~CCIE;    
//    P8SEL |= BIT5;
//    P8DIR &= ~BIT5;
//    TA1CCTL0 = CM_1 + CCIS_1 + SCS + CAP;  //CM_1上升沿捕获，CCIS_1选择CCIxB,                                            //SCS同步捕获，CAP捕获模式
//    TA1CCTL0 &= ~CCIFG;
//    TA1CCTL0 |= CCIE;	
		
    dismantle_alarm_enabled = FALSE;
    avoid_wrong_dismantle_timer = NULL;
    start_avoid_wrong_dismantle_timer(); // 避免上电时防拆开关的误触发
}

void escort_dismantle_event_handle(void *arg)
{
    e_escort_dismantle_type_t type = (e_escort_dismantle_type_t)(uint32_t)arg;
//    escort_frame_t frame;
	bool_t result;

    if (dismantle_alarm_enabled && (avoid_wrong_dismantle_timer == NULL))
    {   // 程序正常运行，防拆报警已被使能
        if (((type == ESCORT_DISMANTLE_SHELL || type == ESCORT_DISMANTLE_INSTALL)
            	&& (escort_u2_is_high_level() && escort_u6_is_high_level()))
            || (type == ESCORT_DISMANTLE_ACC)
			|| (type == ESCORT_DISMANTLE_U18))
        { // 防拆开关U6和U2同时弹起，或者算法防拆报警,或者U18被弹起
            if ((escort_vin_valid_key != ESCORT_VIN_INVALID_KEY)
                &&(unbind_flag == FALSE))
            {
                //nfc保存强拆log
                //nfc_save_log_info(TYPE_FORCE_RELEASE_DATA);
                dismantle_alarm_enabled = FALSE;
                escort_vin_valid_key = ESCORT_VIN_INVALID_KEY; // RAM中的车架号置为无效值              
                escort_led_enter_state(ESCORT_LED_DISMANTLE);
				//删除车架号
				do
				{
					result = nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_CARRIAGE_NUM);
				} while(result == NFC_READ_FALSE);
				
//                frame.type = ESCORT_APP_MESSAGE_APP_TYPE;
//                frame.app_data.type = ESCORT_APP_DATA_DISMANTLE_TYPE;
                
               uint8_t monitor_timer_len = 0;
               ssn_open_type |= 0x08;
               monitor_timer_len = mac_moniter_interval_time_get();
               DBG_ASSERT(monitor_timer_len != 0 __DBG_LINE);
               ssn_open(ssn_open_cb, monitor_timer_len);
   
//                escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
//                                    1, FALSE);
            }            
        }
    }
    else if ((type == ESCORT_DISMANTLE_INSTALL) 
			 || (type == ESCORT_DISMANTLE_SHELL))//此时U6和U2在无车架号的情况下被触发
    {
        if (escort_box_is_installed())//任一个被按下，即进入可配置状态
        {
            escort_nfc_start_check_timer();
        }
        else//两个都被弹起，退出可配置状态
        {
            escort_nfc_stop_check_timer();
        }
    }
}

void escort_dismantle_open_ssn_handle(void *arg)
{
    escort_frame_t frame;
    frame.type = ESCORT_APP_MESSAGE_APP_TYPE;
    frame.app_data.type = ESCORT_APP_DATA_DISMANTLE_TYPE;    
    escort_send_request(&frame, 
                        ESCORT_SEND_LIST_PRIORITY_HIGH,
                        1, 
                        FALSE);    
}


