/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : the alg of adxl345                                                                                          
**  File        : escort_acc.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是业务层周期采集加速度数据                                 
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <osel_arch.h>
#include <hal_acc_sensor.h>
#include <hal_timer.h>
#include <acc_alg.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>
#include <escort_gps.h>
#include <escort_acc.h>
#include <hal_board.h>
#include <escort_dismantle.h>
#include <escort_nfc.h>
#include <nfc_module.h>
#include <ssn.h>
#include <escort_send.h>
#include <mac_moniter.h>
//#include <stdio.h>

#include <gps.h>

#define     ACC_FILTER_TEST     1

#if ACC_FILTER_TEST  
#include <math.h>
#endif

extern bool_t delay_send_tag;
extern uint8_t ssn_open_type;
extern bool_t gps_need_deferred_close;

static hal_timer_t *acc_data_timer = NULL;
uint8_t heart_if_static_flag = TRUE;//判断发心跳时是否静止状态的标志位,默认为静止

extern void gps_open(void);

static void acc_data_timer_cb(void *arg)
{
    acc_data_timer = NULL;
    osel_post(ESCORT_ACC_DATA_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void acc_event_callback(const acc_event_t *acc_event)
{
    switch (acc_event->type)
    {
    case ACC_INT_EVENT_TYPE:
        osel_post(ESCORT_ACC_SENSOR_INT_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        break;
        
    case ACC_SINGLE_TAP_EVENT_TYPE:
        if (escort_box_is_installed())
        {
            escort_nfc_start_check_timer();
        }
        break;
        
    case ACC_DOUBLE_TAP_EVENT_TYPE:
        break;
        
    case ACC_ACTIVITY_EVENT_TYPE:
        break;
        
    case ACC_INACTIVITY_EVENT_TYPE:
        break;
        
    case ACC_FREE_FALL_EVENT_TYPE:
        break;
        
    default :
        break;
    }
}

#if GPS_DEBUG_INFO == 1
	extern uint8_t acc_status_flag;
#endif 
static void acc_alg_event_callback(const acc_alg_event_t *acc_alg_event)
{
    escort_frame_t frame;    
    frame.type = ESCORT_APP_MESSAGE_APP_TYPE;
    uint32_t monitor_timer_len = 0;
    DBG_ASSERT(acc_alg_event != NULL __DBG_LINE);
   
    if (escort_vin_valid_key != ESCORT_VIN_INVALID_KEY)
    {            
        switch (acc_alg_event->type)
        {
            case ACC_ALG_OVERRUN_EVENT_TYPE:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 50;
#endif                  
                break;
                
            case ACC_ALG_ACTIVITY_EVENT_TYPE:
                frame.app_data.type = ESCORT_APP_DATA_ACTIVITY_TYPE;
                frame.app_data.activity_data.status = 
                    acc_alg_event->activity_args.activity_status_type;
                
                switch (acc_alg_event->activity_args.activity_status_type)
                {
                case ACC_ALG_STATIC_TO_ACT_STATUS:
#if GPS_DEBUG_INFO == 1
    acc_status_flag = 10;
#endif                       
                    heart_if_static_flag = FALSE;
                    escort_gps_open();
                    
                    frame.app_data.activity_data.dimension_num = 4;
                    frame.app_data.activity_data.dimension_values[0] = 
                        acc_alg_event->activity_args.static_to_act_feature.c1;
                    frame.app_data.activity_data.dimension_values[1] = 
                        acc_alg_event->activity_args.static_to_act_feature.c2;
                    frame.app_data.activity_data.dimension_values[2] = 
                        acc_alg_event->activity_args.static_to_act_feature.c3;
                    frame.app_data.activity_data.dimension_values[3] = 
                        acc_alg_event->activity_args.static_to_act_feature.c4;                    

                    //如果正在延时发送定时器内，关闭延时发送定时器
                    if(delay_send_tag == TRUE)
                    {
                        delay_send_tag = FALSE;
                        osel_post(STOP_GPRS_DELAY_TIMER, NULL, OSEL_EVENT_PRIO_LOW);
                    }
                    escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                                        1, FALSE);
                    break;
                    
                case ACC_ALG_STATIC_RETAIN_STATUS:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 20;
#endif                     
                    heart_if_static_flag = TRUE;
                    gps_need_deferred_close = FALSE;//!需要关闭动静转换的等待GPS数据
                    escort_gps_close();
                    break;
                    
                case ACC_ALG_ACT_RETAIN_STATUS: 
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 30;
#endif                       
                    heart_if_static_flag = FALSE;  
                    escort_gps_open();
                    break;

                case ACC_ALG_ACT_TO_STATIC_STATUS:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 40;
#endif                       
                    heart_if_static_flag = TRUE;
                    
                    //恢复静止寻找ssn网络
                    ssn_open_type |= 0x02;
                    monitor_timer_len = mac_moniter_interval_time_get();
                    DBG_ASSERT(monitor_timer_len != 0 __DBG_LINE);
                    ssn_open(ssn_open_cb, monitor_timer_len);
                    break;
                    
                default :
                    DBG_ASSERT(FALSE __DBG_LINE);
                }
                break;
                
            case ACC_ALG_FALL_EVENT_TYPE:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 60;
#endif                  
                break;
                
            case ACC_ALG_TOPPLE_EVENT_TYPE:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 70;
#endif                  
                break;
                
            case ACC_ALG_TURN_EVENT_TYPE:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 80;
#endif                  
                break;
                
            case ACC_ALG_DISA_EVENT_TYPE:
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 90;
#endif                  
                escort_trigger_dismantle_event(ESCORT_DISMANTLE_ACC);
                break;
                
            default :
#if GPS_DEBUG_INFO == 1
	acc_status_flag = 100;
#endif                  
                DBG_ASSERT(FALSE __DBG_LINE);
                break;
        }
    }
}

void escort_act_to_static_open_ssn_handle(void *arg)
{
    escort_frame_t frame;    
    frame.type = ESCORT_APP_MESSAGE_APP_TYPE;    
    frame.app_data.type = ESCORT_APP_DATA_ACTIVITY_TYPE;
    frame.app_data.activity_data.status = ACC_ALG_ACT_TO_STATIC_STATUS;   
    
    frame.app_data.activity_data.dimension_num = 4;
    frame.app_data.activity_data.dimension_values[0] = 0;
    frame.app_data.activity_data.dimension_values[1] = 0;
    frame.app_data.activity_data.dimension_values[2] = 0;
    frame.app_data.activity_data.dimension_values[3] = 0;
    // 准备关闭GPS，等待最后一个GPS数据
    gps_need_deferred_close = TRUE;

    escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                        2, FALSE);    
}


static void start_acc_data_timer(uint32_t time)
{
    if (NULL != acc_data_timer)
    {
        hal_timer_cancel(&acc_data_timer);
        DBG_ASSERT(acc_data_timer == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(time),
                      acc_data_timer_cb,
                      NULL,
                      acc_data_timer);
    DBG_ASSERT(acc_data_timer != NULL __DBG_LINE);
}

void escort_acc_sensor_int_event_handle(void *arg)
{
    acc_process_int_event();
}

void escort_acc_data_timer_event_handle(void *arg)
{
    acc_data_t acc_data[32];
    uint8_t count;
    acc_alg_data_t acc_alg_data;
#if ACC_FILTER_TEST
    fp32_t alpha = 0.1;
    fp32_t rx,ccx,ry,ccy,rz,ccz;
    static fp32_t eex = 0;
    static fp32_t ex = 0;
    static fp32_t aax = 0;
    static fp32_t data_ix = 0;  
  
    static fp32_t eey = 0;
    static fp32_t ey = 0;
    static fp32_t aay = 0;
    static fp32_t data_iy = 0; 

    static fp32_t eez = 0;
    static fp32_t ez = 0;
    static fp32_t aaz = 0;
    static fp32_t data_iz = 0; 
    
    rx = alpha;
    ry = alpha;
    rz = alpha;
#endif    
    hal_wdt_clear(16000);    
    start_acc_data_timer(800);   
    count = acc_get_detect_data(acc_data, 32);
	
#if GPS_DEBUG_INFO == 1
	extern uint8_t gps_cmd_cnt;
	gps_cmd_cnt += count;
#endif
    
    for (uint8_t i = 0; i < count; i++)
    {
#if ACC_FILTER_TEST        
        //x轴迭代
        ex = acc_data[i].x - data_ix;
        eex = rx * ex + (1-rx) * eex;
        aax = rx * fabs(ex) + (1 - rx) * aax;        
        if (fabs(aax) < 0.001)
        {
            ccx = 0.5;
        }
        else
        {
            ccx = fabs(eex/aax);
        }  
        
        data_ix = ccx * acc_data[i].x + (1 - ccx) * data_ix;
        
        //y轴迭代
        ey = acc_data[i].y - data_iy;
        eey = ry * ey + (1-ry) * eey;
        aay = ry * fabs(ey) + (1 - ry) * aay;        
        if (fabs(aay) < 0.001)
        {
            ccy = 0.5;
        }
        else
        {
            ccy = fabs(eey/aay);
        } 
        
        data_iy = ccy * acc_data[i].y + (1 - ccy) * data_iy;      
        
        //z轴迭代
        ez = acc_data[i].z - data_iz;
        eez = rz * ez + (1-rz) * eez;
        aaz = rz * fabs(ez) + (1 - rz) * aaz;        
        if (fabs(aaz) < 0.001)
        {
            ccz = 0.5;
        }
        else
        {
            ccz = fabs(eez/aaz);
        }  
        
        data_iz = ccz * acc_data[i].z + (1 - ccz) * data_iz; 
        
//        printf("%f         %f          %f\r\n",data_ix,data_iy,data_iz); 
        acc_alg_data.x = (int16_t)(data_ix * 3.9);
        acc_alg_data.y = (int16_t)(data_iy * 3.9);
        acc_alg_data.z = (int16_t)(data_iz * 3.9);        
#else 
//        printf("%d         %d          %d\r\n",acc_data[i].x,acc_data[i].y,acc_data[i].z);
        acc_alg_data.x = (int16_t)(acc_data[i].x * 3.9);
        acc_alg_data.y = (int16_t)(acc_data[i].y * 3.9);
        acc_alg_data.z = (int16_t)(acc_data[i].z * 3.9);
#endif
        acc_alg_new_data(&acc_alg_data);
    }
}

void escort_enable_acc_alg(void)
{    
    acc_alg_init();
    // 配置算法模块
    acc_alg_config_t acc_alg_cfg;
    
    acc_alg_cfg.data_rate = 100;
    acc_alg_cfg.acc_alg_cb = acc_alg_event_callback;
    acc_alg_config(&acc_alg_cfg);
    
    // 启动读取加速度数据的定时器
    start_acc_data_timer(800);
}

void escort_acc_sensor_init(void)
{
    // 配置加速度传感器模块
    acc_config_t acc_config_data;
    acc_config_data.data_rate = 0x07;
    //acc_config_data.data_rate = 0x0A;
    acc_config_data.range = 0x02;
    acc_config_data.power_mode = ACC_NORMAL_OPERATION_MODE;
    acc_config_data.enabled_detects.single_tap = 1;
    acc_config_data.enabled_detects.double_tap = 0;
    acc_config_data.enabled_detects.activity   = 0;
    acc_config_data.enabled_detects.inactivity = 0;
    acc_config_data.enabled_detects.free_fall  = 0;
    acc_config_data.acc_cb = acc_event_callback;
    acc_config(&acc_config_data);
}
