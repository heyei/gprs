/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : gps                                                                                          
**  File        : escort_gps.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是gps电源的开关、初始化、数据发送                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <osel_arch.h>
#include <driver.h>
#include <hal_acc_sensor.h>
#include <hal_uart.h>
#include <gps.h>
#include <pbuf.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>
#include <escort_gps.h>
#include <escort_nfc.h>

#define ESCORT_GPS_DATA_PERIOD      10  //单位秒

#define ESCORT_GPS_STATE_FIRST_INIT         1
#define ESCORT_GPS_STATE_WAIT_OFF           2
#define ESCORT_GPS_STATE_NORMAL             3

bool_t gprs_power_on = FALSE;

bool_t gps_need_deferred_close = FALSE;
uint16_t escort_gps_data_period;
bool_t escort_gps_is_enabled = FALSE;

static void gps_data_cb(gps_simple_info_t gps_data)
{
    pbuf_t *pbuf;
    gps_simple_info_t *arg;
    
    pbuf = pbuf_alloc(sizeof(gps_simple_info_t) __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (gps_simple_info_t *)pbuf->data_p;
    *arg = gps_data;
    if (escort_gps_is_enabled == TRUE)
    {
        osel_post(ESCORT_GPS_DATA_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
    }
    else
    {
        pbuf_free(&pbuf __PLINE1);
        DBG_ASSERT(pbuf == NULL __DBG_LINE);
    }
}


void escort_gps_open(void)
{
    escort_gps_is_enabled = TRUE;    
    gps_open(); 
}

void escort_gps_close(void)
{
    escort_gps_is_enabled = FALSE;     
    gps_close(); 
}

void escort_gprs_device_enable(bool_t enable)
{
    if (enable)
    {
        gprs_power_on = TRUE;
        hal_uart_init(HAL_UART_4, 9600, 0); //打开GPRS的115200的UART
        P6SEL &=~BIT7;//EN_6130 power
        P6DIR |= BIT7;
        P6OUT |= BIT7;

        P10DIR |= BIT0;
        P10OUT |= BIT0;        
        //P2DIR |=  BIT1;
    }
    else
    {
        gprs_power_on = FALSE;
        P6SEL &=~BIT7;//EN_6130 power
        P6DIR |= BIT7;
        P6OUT &= ~BIT7;
        UCA3CTL1 = UCSWRST; //关闭GPRS的115200的UART
        P10SEL &= ~(BIT4 + BIT5);
        P10DIR &= ~(BIT4 + BIT5); //UART管脚置成输入, 否则会有几mA的功耗
//        gps_control(gps_ctrl_cb, GPS_OFF); // gps_close()里置成GPS_SLEEP状态，
                                           // 断电时再置成GPS_OFF

        P10DIR &= ~BIT0;
//        P2DIR &=  ~BIT1;         
        //P2OUT |= BIT1; 
        P2OUT &= ~BIT1;        
    }
}

static double gps_dm_to_d(double dm)
{
    int32_t dd;
    
    dd = (int32_t)dm / 100;
    
    return (dm - dd * 100) * ((double)1.0 / 60.0) + dd; 
}

void escort_gps_data_event_handle(void *arg)
{
    pbuf_t *pbuf;
    gps_simple_info_t *cb_arg;
    escort_frame_t frame;
    
    pbuf = (pbuf_t *)arg;
    cb_arg = (gps_simple_info_t *)pbuf->data_p;
    
    frame.type = ESCORT_APP_MESSAGE_APP_TYPE;
    frame.app_data.type = ESCORT_APP_DATA_GPS_TYPE;

#if GPS_DEBUG_INFO == 1
	if (cb_arg->speed > 10.0)
	{
		frame.app_data.gps_data.longitude = cb_arg->longitude;
		frame.app_data.gps_data.latitude = cb_arg->latitude;
	}
	else
	{
		frame.app_data.gps_data.longitude = gps_dm_to_d(cb_arg->longitude);
		frame.app_data.gps_data.latitude = gps_dm_to_d(cb_arg->latitude);
	}
#else
    frame.app_data.gps_data.longitude = gps_dm_to_d(cb_arg->longitude);
    frame.app_data.gps_data.latitude = gps_dm_to_d(cb_arg->latitude);
#endif
    
    if (gps_need_deferred_close)
    {
        /*gps芯片如一直不输出数据，则该处无法被调用到，静保持中关闭gps补救*/
        gps_need_deferred_close = FALSE;
        escort_gps_close();
    }
    
    if (escort_vin_valid_key != ESCORT_VIN_INVALID_KEY)
    {
        //GPS帧
        escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                            1, FALSE);        
    }
    
    pbuf_free(&pbuf __PLINE1);
    DBG_ASSERT(pbuf == NULL __DBG_LINE);
}

void escort_gps_init(void)
{
    escort_gps_data_period = ESCORT_GPS_DATA_PERIOD;
    // 初始化时打开GPRS, GPS模块电源, 待GPS模块初始化完成后关闭
#if NODE_GPRS_GPS_POWER_SHARE     
    escort_gprs_gps_device_enable(FALSE);
#else
    escort_gprs_device_enable(FALSE);
#endif
    gps_init(gps_data_cb);
}
