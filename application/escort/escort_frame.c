/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : app                                                                                          
**  File        : escort_dismantle.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是拆卸处理                                
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <crc.h>
#include <escort_frame.h>

#define UINT16_TO_BUF(n, buf, little) \
    if (little) \
    { \
        (buf)[0] = (uint8_t)(n); \
        (buf)[1] = (uint8_t)((n) >> 8); \
    } \
    else \
    { \
        (buf)[0] = (uint8_t)((n) >> 8); \
        (buf)[1] = (uint8_t)(n); \
    }
    
#define BUF_TO_UINT16(buf, n, little) \
    if (little) \
    { \
        (n) = (uint16_t)((buf)[0] | ((uint16_t)(buf)[1] << 8)); \
    } \
    else \
    { \
        (n) = (uint16_t)((buf)[1] | ((uint16_t)(buf)[0] << 8)); \
    }

#define UINT32_TO_BUF(n, buf, little) \
    if (little) \
    { \
        (buf)[0] = (uint8_t)(n); \
        (buf)[1] = (uint8_t)((n) >> 8); \
        (buf)[2] = (uint8_t)((n) >> 16); \
        (buf)[3] = (uint8_t)((n) >> 24); \
    } \
    else \
    { \
        (buf)[0] = (uint8_t)((n) >> 24); \
        (buf)[1] = (uint8_t)((n) >> 16); \
        (buf)[2] = (uint8_t)((n) >> 8); \
        (buf)[3] = (uint8_t)(n); \
    }
    
#define BUF_TO_UINT32(buf, n, little) \
    if (little) \
    { \
        (n) = (uint32_t)((buf)[0] | ((uint32_t)(buf)[1] << 8) | \
                         ((uint32_t)(buf)[2] << 16) | \
                             ((uint32_t)(buf)[3] << 24)); \
    } \
    else \
    { \
        (n) = (uint32_t)((buf)[3] | ((uint32_t)(buf)[2] << 8) | \
                         ((uint32_t)(buf)[1] << 16) | \
                             ((uint32_t)(buf)[0] << 24)); \
    }
    
#define FLOAT32_TO_BUF(f, buf, little) \
{ \
    fp32_t _float32_to_buf_f = (f); \
    if (little) \
    { \
        (buf)[0] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
        (buf)[1] = ((uint8_t *)(&_float32_to_buf_f))[1]; \
        (buf)[2] = ((uint8_t *)(&_float32_to_buf_f))[2]; \
        (buf)[3] = ((uint8_t *)(&_float32_to_buf_f))[3]; \
    } \
    else \
    { \
        (buf)[0] = ((uint8_t *)(&_float32_to_buf_f))[3]; \
        (buf)[1] = ((uint8_t *)(&_float32_to_buf_f))[2]; \
        (buf)[2] = ((uint8_t *)(&_float32_to_buf_f))[1]; \
        (buf)[3] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
    } \
}

#define BUF_TO_FLOAT32(buf, f, little) \
{ \
    fp32_t _float32_to_buf_f = (f); \
    if (little) \
    { \
        ((uint8_t *)(&(f)))[0] = (buf)[0]; \
        ((uint8_t *)(&(f)))[1] = (buf)[1]; \
        ((uint8_t *)(&(f)))[2] = (buf)[2]; \
        ((uint8_t *)(&(f)))[3] = (buf)[3]; \
    } \
    else \
    { \
        ((uint8_t *)(&(f)))[0] = (buf)[3]; \
        ((uint8_t *)(&(f)))[1] = (buf)[2]; \
        ((uint8_t *)(&(f)))[2] = (buf)[1]; \
        ((uint8_t *)(&(f)))[3] = (buf)[0]; \
    } \
}

#define DOUBLE32_TO_BUF(f, buf, little) \
{ \
    double _double32_to_buf_f = (f); \
    if (little) \
    { \
        (buf)[0] = ((uint8_t *)(&_double32_to_buf_f))[0]; \
        (buf)[1] = ((uint8_t *)(&_double32_to_buf_f))[1]; \
        (buf)[2] = ((uint8_t *)(&_double32_to_buf_f))[2]; \
        (buf)[3] = ((uint8_t *)(&_double32_to_buf_f))[3]; \
    } \
    else \
    { \
        (buf)[0] = ((uint8_t *)(&_double32_to_buf_f))[3]; \
        (buf)[1] = ((uint8_t *)(&_double32_to_buf_f))[2]; \
        (buf)[2] = ((uint8_t *)(&_double32_to_buf_f))[1]; \
        (buf)[3] = ((uint8_t *)(&_double32_to_buf_f))[0]; \
    } \
}


// 组装超限消息报文体
static uint16_t escort_app_get_overrun_payload(uint8_t  *frame,
                                               uint16_t  max_len,
                            const escort_overrun_data_t *overrun_data)
{
    uint16_t i;
    
    if (max_len < 9)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = overrun_data->status;
    UINT16_TO_BUF((uint16_t)(overrun_data->relative_x), (&frame[i]), FALSE);
    i += 2;
    UINT16_TO_BUF((uint16_t)(overrun_data->relative_y), (&frame[i]), FALSE);
    i += 2;
    UINT16_TO_BUF((uint16_t)(overrun_data->relative_z), (&frame[i]), FALSE);
    i += 2;
    frame[i++] = 0;
    frame[i++] = 0;
    
    return i;
}
                                    
// 组装静动变化消息报文体
static uint16_t escort_app_get_activity_payload(uint8_t  *frame,
                                                uint16_t  max_len,
                            const escort_activity_data_t *activity_data)
{
    uint16_t i;
    
    if (max_len < 4)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = activity_data->status;
    frame[i++] = activity_data->dimension_num;
    
    if (max_len - i < activity_data->dimension_num * 2)
    {
        return 0;
    }
    
    for (uint8_t k = 0; k < activity_data->dimension_num; k++)
    {
        UINT16_TO_BUF((uint16_t)(activity_data->dimension_values[k]),
                      (&frame[i]), FALSE);
        i += 2;
    }
    
    return i;
}                                   

// 组装跌落落体消息报文体
static uint16_t escort_app_get_fall_payload(uint8_t                  *frame,
                                            uint16_t                  max_len,
                                            const escort_fall_data_t *fall_data)
{
    uint16_t i;
    
    if (max_len < 8)
    {
        return 0;
    }
    
    i = 0;
    UINT16_TO_BUF((uint16_t)(fall_data->absolute_x), (&frame[i]), FALSE);
    i += 2;
    UINT16_TO_BUF((uint16_t)(fall_data->absolute_x), (&frame[i]), FALSE);
    i += 2;
    UINT16_TO_BUF((uint16_t)(fall_data->absolute_x), (&frame[i]), FALSE);
    i += 2;
    frame[i++] = 0;
    frame[i++] = 0;
    
    return i;
}
                                 
// 组装倾倒消息报文体
static uint16_t escort_app_get_topple_payload(uint8_t  *frame,
                                              uint16_t  max_len,
                            const escort_topple_data_t *topple_data)
{
    uint16_t i;
    
    if (max_len < 3)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = topple_data->dimension_num;
    
    if (max_len - i < topple_data->dimension_num * 2)
    {
        return 0;
    }
    
    for (uint8_t k = 0; k < topple_data->dimension_num; k++)
    {
        UINT16_TO_BUF((uint16_t)(topple_data->dimension_values[k]), (&frame[i]),
                      FALSE);
        i += 2;
    }
    
    return i;
}

// 组装转向消息报文体
static uint16_t escort_app_get_turn_payload(uint8_t                  *frame,
                                            uint16_t                  max_len,
                                            const escort_turn_data_t *turn_data)
{
    uint16_t i;
    
    if (max_len < 3)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = turn_data->dimension_num;
    
    if (max_len - i < turn_data->dimension_num * 2)
    {
        return 0;
    }
    
    for (uint8_t k = 0; k < turn_data->dimension_num; k++)
    {
        UINT16_TO_BUF((uint16_t)(turn_data->dimension_values[k]), (&frame[i]), 
                      FALSE);
        i += 2;
    }
    
    return i;
}
                                 
// 组装GPS位置报文体
static uint16_t escort_app_get_gps_payload(uint8_t                 *frame,
                                           uint16_t                 max_len,
                                           const escort_gps_data_t *gps_data)
{
    uint16_t i;
    
    if (max_len < 8)
    {
        return 0;
    }
    
    i = 0;
    DOUBLE32_TO_BUF((gps_data->longitude), (&frame[i]), FALSE);
    i += 4;
    DOUBLE32_TO_BUF((gps_data->latitude), (&frame[i]), FALSE);
    i += 4;
    
    return i;
}

// 组装温湿度报文体
uint16_t escort_app_get_humiture_payload(uint8_t                 *frame,
                                         uint16_t                 max_len,
                                    const escort_humiture_data_t *humiture_data)
{
    uint16_t i;
    
    if (max_len < 6)
    {
        return 0;
    }
    
    i = 0;
//  UINT16_TO_BUF((humiture_data->temperature), (&frame[i]), FALSE);
//  i += 2;
    FLOAT32_TO_BUF((humiture_data->temperature), (&frame[i]), FALSE);
    i += 4;
    FLOAT32_TO_BUF((humiture_data->humidity), (&frame[i]), FALSE);
    i += 4;
    
    return i;
}

// 组装算法参数配置应答报文体
static uint16_t escort_app_get_alg_ack_payload(uint8_t  *frame,
                                               uint16_t  max_len,
                            const escort_alg_data_ack_t *alg_data_ack)
{
    uint16_t i;
    
    if (max_len < 3)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = alg_data_ack->type;
    frame[i++] = alg_data_ack->result;
    
    return i;
}
                                    
// 组装传感器偏移补偿应答报文体
static uint16_t escort_app_get_sensor_ack_payload(uint8_t  *frame,
                                                  uint16_t  max_len,
                          const escort_sensor_offset_ack_t *sensor_offset_ack)
{
    uint16_t i;
    
    if (max_len < 3)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = sensor_offset_ack->type;
    frame[i++] = sensor_offset_ack->result;
    
    return i;
}

// 组装心跳帧报文体
static uint16_t escort_app_get_heartbeat_payload(uint8_t  *frame,
                                                 uint16_t  max_len,
                            const escort_heartbeat_data_t *heartbeat)
{
    uint16_t i;
    
    if (max_len < 12)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = heartbeat->control;
    frame[i++] = heartbeat->energy_residual;
    for (uint8_t k = 0; k < 8; k++)
    {
        frame[i + k] = heartbeat->father_id[7 - k];
    }
//  memcpy(&frame[i], app_frame->dest_id, 8);
    i += 8;
    frame[i++] = heartbeat->uplink_rssi;
    frame[i++] = heartbeat->downlink_rssi;
    
    return i;
}

// 组装注册帧报文体
static uint16_t escort_app_get_register_payload(uint8_t  *frame,
                                                uint16_t  max_len,
                            const escort_register_data_t *register_data)
{
    uint16_t i;
    
    if (max_len < 18)
    {
        return 0;
    }
    
    i = 0;
    frame[i] = register_data->type;
    i++;
    memcpy(&frame[i], register_data->vin, ESCORT_VIN_LENGTH);
    i += ESCORT_VIN_LENGTH;
    
    return i;
}

// 解析算法参数配置报文体
bool_t escort_app_parse_alg_data_payload(const uint8_t     *frame,
                                         uint16_t           frame_len,
                                         escort_alg_data_t *alg_data)
{
    uint16_t i;
    
    if (frame_len < 5)
    {
        return FALSE;
    }
    
    i = 0;
    alg_data->type = frame[i++];
    alg_data->dimension_num = frame[i++];
    
    if (frame_len - i < alg_data->dimension_num * 2 + 1)
    {
        return FALSE;
    }
    
    for (uint8_t k = 0; k != alg_data->dimension_num; k++)
    {
        uint16_t temp;
        
        BUF_TO_UINT16((&frame[i]), temp, FALSE);
        i += 2;
        alg_data->dimension_values[k] = (int16_t)temp;
    }
    
    return TRUE;
}
                                    
// 解析传感器偏移补偿报文体
bool_t escort_app_parse_sensor_offset_payload(const uint8_t *frame,
                                              uint16_t       frame_len,
                                     escort_sensor_offset_t *sensor_offset)
{
    uint16_t i;
    
    if (frame_len < 5)
    {
        return FALSE;
    }
    
    i = 0;
    sensor_offset->type = frame[i++];
    sensor_offset->dimension_num = frame[i++];
    
    if (frame_len - i != sensor_offset->dimension_num * 2 + 1)
    {
        return FALSE;
    }
    
    for (uint8_t k = 0; k < sensor_offset->dimension_num; k++)
    {
        uint16_t temp;
        
        BUF_TO_UINT16((&frame[i]), temp, FALSE);
        i += 2;
        sensor_offset->dimension_values[k] = (int16_t)temp;
    }
    
    return TRUE;
}

// 解析帧
bool_t escort_parse_frame(const uint8_t      *frame,
                          uint16_t            frame_len,
                          escort_frame_t     *app_frame)
{
    uint16_t i;
    bool_t result;
    
    if (frame_len < 22)
    {
        return FALSE;
    }
    
    i = 0;
    app_frame->version = frame[i++];
    app_frame->type = frame[i++];
    BUF_TO_UINT16((&frame[i]), (app_frame->length), FALSE);
    i += 2;
    
    if (app_frame->length != frame_len)
    {
        return FALSE;
    }
    
    BUF_TO_UINT16((&frame[i]), (app_frame->seq), FALSE);
    i += 2;
    memcpy(app_frame->dest_id, &frame[i], 8);
    i += 8;
    memcpy(app_frame->src_id, &frame[i], 8);
    i += 8;
    
    if (app_frame->type == ESCORT_APP_MESSAGE_APP_TYPE)
    {
        if (frame_len - i < 1)
        {
            return FALSE;
        }
        
        if (frame[frame_len - 1] != crc_compute(&frame[i], frame_len - i))
        {
            return FALSE;
        }
        
        app_frame->app_data.type = frame[i++];
        result = FALSE;
        
        switch (app_frame->app_data.type)
        {
        case ESCORT_APP_DATA_ALGORITHM_TYPE:
            result = escort_app_parse_alg_data_payload(&frame[i], frame_len - i,
                        &app_frame->app_data.alg_data);
            break;
        case ESCORT_APP_DATA_SENSOR_TYPE:
            result = escort_app_parse_sensor_offset_payload(&frame[i], 
                                                            frame_len - i,
                        &app_frame->app_data.sensor_offset);
            break;
        }
        
        return result;
    }
    
    return FALSE;
}

// 组装帧
uint16_t escort_get_frame(uint8_t                  *frame,
                          uint16_t                  max_len,
                          const escort_frame_t     *app_frame)
{
    uint16_t i;
    uint8_t *p_frame_len;
    uint8_t *p_app_type;
    uint8_t *p_payload;
    uint16_t sub_frame_len;
    
    if (max_len < 22)
    {
        return 0;
    }
    
    i = 0;
    frame[i++] = app_frame->version;
    frame[i++] = app_frame->type;
    p_frame_len = &frame[i];
    i += 2;
    UINT16_TO_BUF((app_frame->seq), (&frame[i]), FALSE);
    i += 2;
    for (uint8_t k = 0; k < 8; k++)
    {
        frame[i + k] = app_frame->dest_id[7 - k];
    }
//  memcpy(&frame[i], app_frame->dest_id, 8);
    i += 8;
    for (uint8_t k = 0; k < 8; k++)
    {
        frame[i + k] = app_frame->src_id[7 - k];
    }
//  memcpy(&frame[i], app_frame->src_id, 8);
    i += 8;
    p_payload = &frame[i];
    
    if (app_frame->type == ESCORT_APP_MESSAGE_APP_TYPE)
    {
        if (max_len - i < 1)
        {
            return 0;
        }
        p_app_type = &frame[i];
        frame[i++] = app_frame->app_data.type;
        sub_frame_len = 0;
        
        switch (app_frame->app_data.type)
        {
        case ESCORT_APP_DATA_OVERRUN_TYPE:
            sub_frame_len = escort_app_get_overrun_payload(&frame[i], 
                                                           max_len - i - 1,
                                &app_frame->app_data.overrun_data);
            break;
        case ESCORT_APP_DATA_ACTIVITY_TYPE:
            sub_frame_len = escort_app_get_activity_payload(&frame[i], 
                                                            max_len - i - 1,
                                &app_frame->app_data.activity_data);
            break;
        case ESCORT_APP_DATA_FALL_TYPE:
            sub_frame_len = escort_app_get_fall_payload(&frame[i], 
                                                        max_len - i - 1,
                                &app_frame->app_data.fall_data);
            break;
        case ESCORT_APP_DATA_TOPPLE_TYPE:
            sub_frame_len = escort_app_get_topple_payload(&frame[i], 
                                                          max_len - i - 1,
                                &app_frame->app_data.topple_data);
            break;
        case ESCORT_APP_DATA_TURN_TYPE:
            sub_frame_len = escort_app_get_turn_payload(&frame[i], 
                                                        max_len - i - 1,
                                &app_frame->app_data.turn_data);
            break;
        case ESCORT_APP_DATA_GPS_TYPE:
            sub_frame_len = escort_app_get_gps_payload(&frame[i], 
                                                       max_len - i - 1,
                                &app_frame->app_data.gps_data);
            break;
        case ESCORT_APP_DATA_DISMANTLE_TYPE:
            // 无载荷
            break;
        case ESCORT_APP_DATA_HUMITURE_TYPE:
            sub_frame_len = escort_app_get_humiture_payload(&frame[i], 
                                                            max_len - i - 1,
                                          &app_frame->app_data.humiture_data);
            break;
        case ESCORT_APP_DATA_ALGORITHM_ACK_TYPE:
            sub_frame_len = escort_app_get_alg_ack_payload(&frame[i], 
                                                           max_len - i - 1,
                                &app_frame->app_data.alg_data_ack);
            break;
        case ESCORT_APP_DATA_SENSOR_ACK_TYPE:
            sub_frame_len = escort_app_get_sensor_ack_payload(&frame[i], 
                                                              max_len - i - 1,
                                &app_frame->app_data.sensor_offset_ack);
            break;
        }
        
        if (sub_frame_len == 0 
            && (app_frame->app_data.type != ESCORT_APP_DATA_DISMANTLE_TYPE))
        {
            return 0;
        }
        
        i += sub_frame_len;
        frame[i] = crc_compute(p_app_type, &frame[i] - p_app_type);
        i++;
        UINT16_TO_BUF(i, p_frame_len, FALSE);
        
        return i;
    }
    else if (app_frame->type == ESCORT_APP_MESSAGE_REGISTER_TYPE)
    {
        sub_frame_len = escort_app_get_register_payload(&frame[i], 
                                                        max_len - i,
                                            &app_frame->register_data);
        
        if (sub_frame_len == 0)
        {
            return 0;
        }
        
        i += sub_frame_len;
        frame[i] = crc_compute(p_payload, &frame[i] - p_payload);
        i++;
        UINT16_TO_BUF(i, p_frame_len, FALSE);
        
        return i;
    }
    else if (app_frame->type == ESCORT_APP_MESSAGE_HEARTBEAT_TYPE)
    {
        sub_frame_len = escort_app_get_heartbeat_payload(&frame[i], 
                                                          max_len - i,
                                              &app_frame->heartbeat_data);
        
        if (sub_frame_len == 0)
        {
            return 0;
        }
        
        i += sub_frame_len;
        frame[i] = crc_compute(p_payload, &frame[i] - p_payload);
        i++;
        UINT16_TO_BUF(i, p_frame_len, FALSE);
        
        return i;
    }
    else if (app_frame->type == ESCORT_APP_MESSAGE_UNBIND_TYPE)
    {
        //i += sub_frame_len;
//        frame[i] = crc_compute(p_payload, &frame[i] - p_payload);
//        i++;
        UINT16_TO_BUF(i, p_frame_len, FALSE);
        
        return i;
    }
    
    return 0;
}
