/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : 配置后启动处理                                                                                         
**  File        : escort_startup.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是配置车架号后的启动处理                             
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <hal_timer.h>
#include <osel_arch.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_acc.h>
#include <escort_gps.h>
#include <escort_send.h>
#include <escort_dismantle.h>
#include <escort_humiture.h>
#include <escort_nfc.h>
#include <escort_heartbeat.h>
#include <nfc_module.h>
#include <ssn.h>

static hal_timer_t *startup_timer = NULL;

#if NODE_SSN_DEBUG
static void send_register_message(void);
static void register_timer_start(void);

static hal_timer_t *register_timer = NULL;
static void register_timer_cb(void *arg)
{
    register_timer = NULL;
    osel_post(ESCORT_STARTUP_TIMER_TEST_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void escort_startup_timer_test_handle(void *arg)
{
    send_register_message();
    register_timer_start();    
}
static void register_timer_start(void)
{
    DBG_ASSERT(register_timer == NULL __DBG_LINE);
    HAL_TIMER_SET_REL(MS_TO_TICK(30000),
                      register_timer_cb,
                      NULL,
                      register_timer);
    DBG_ASSERT(register_timer != NULL __DBG_LINE);  	
}

#endif
static void startup_timer_cb(void *arg)
{
    osel_post(ESCORT_STARTUP_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void send_register_message(void)
{
    escort_frame_t frame;
    
    if (escort_vin_valid_key == ESCORT_VIN_VALID_KEY) //判断是否有缓存的车架号
    {
        //nfc保存log
        //nfc_save_log_info(TYPE_REG_DATA);
        
        frame.type = ESCORT_APP_MESSAGE_REGISTER_TYPE;
        frame.register_data.type = 0x01;
        memcpy(frame.register_data.vin, 
               escort_vin, 
               ESCORT_VIN_LENGTH);
        escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                            1, FALSE);
    }
}

void escort_startup(void)
{
    HAL_TIMER_SET_REL(MS_TO_TICK(100),
                      startup_timer_cb,
                      NULL,
                      startup_timer);
    DBG_ASSERT(startup_timer != NULL __DBG_LINE);
}

void escort_startup_timer_handle(void *arg)
{    
    (void)arg;
    // 如果在这里的任务中配置其它模块，需要其它模块在config后才启动自己的任务
    escort_enable_acc_alg();
    escort_gps_init();
    escort_enable_dismantle_alarm();
//    escort_humiture_init();
    escort_send_init();
}


void escort_init_open_ssn_handle(void *arg)
{
    static nfc_info_flag_t nfc_info_flag = {0};
    static uint8_t clr_bind_flag_count = 0;
    bool_t result;
    
#if NODE_SSN_DEBUG
    register_timer_start();
#else
    send_register_message();
#endif  
  
    //读取nfc的flag位，如果出现绑定或解绑定标志位，则补发相应帧
    if (nfc_read_info_flag(&nfc_info_flag))
    {
        if (nfc_info_flag.carriage_num && nfc_info_flag.unbind)
        {
            nfc_clr_info_flag(FLAG_BING_INFO);
            nfc_clr_info_flag(FLAG_UNBIND_INFO);
            DBG_ASSERT(FALSE __DBG_LINE);
        }        
        else if (nfc_info_flag.carriage_num)
        {
            do
            {
                result = nfc_clr_info_flag(FLAG_BING_INFO);
                //2014-12-09 解绑不成功测试
                if (clr_bind_flag_count++ > 16)
                {
                    DBG_ASSERT(FALSE __DBG_LINE);                    
                }
            } while(result == FALSE);             
        }
        else if (nfc_info_flag.unbind)
        {
            do
            {
                result = nfc_clr_info_flag(FLAG_UNBIND_INFO);
                //2014-12-09 解绑不成功测试
                if (clr_bind_flag_count++ > 16)
                {
                    DBG_ASSERT(FALSE __DBG_LINE);               
                }            
            } while(result == FALSE);  
            
            escort_frame_t frame;
            frame.type = ESCORT_APP_MESSAGE_UNBIND_TYPE;
            escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                    1, FALSE);                 
        }
    }

    clr_bind_flag_count = 0;
    escort_heartbeat_timer_start((uint32_t)3*3600 * 1000); //启动心跳周期    
}






