/***************************************************************************
* File        : escort_led.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_LED_H
#define ESCORT_LED_H

typedef enum
{   // 状态优先级从低到高
    ESCORT_LED_INIT = 0x0000,
//    ESCORT_LED_WORK = 0x0001,//工作状态
//    ESCORT_LED_SELF_CHECK_SUCC = 0x0002u,//自检成功    

    ESCORT_LED_SELF_CHECK_SUCC = 0x0001,//自检成功   
    ESCORT_LED_WORK = 0x0002u,//工作状态   
    ESCORT_LED_DATA_COMMUNICATING = 0x0004u,//数据正在发送
    ESCORT_LED_DEV_ERR = 0x0008u,//设备异常
    ESCORT_LED_CAN_CFG = 0x0010u,    //可配置状态,即等待绑定或者解绑状态
    ESCORT_LED_DISMANTLE = 0x0020u,//强制拆除
    ESCORT_LED_NFC_ERROR = 0x0040, //NFC I2C故障
}e_escort_led_state_t;

void escort_led_enter_state(e_escort_led_state_t state);
void escort_led_exit_state(e_escort_led_state_t state);
void escort_led_init(void);
void escort_led_timer_handle(void *arg);
void escort_self_check_succ_handle(void *arg);
static void led_self_check_keep_timer(void);

#endif
