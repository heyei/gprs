/***************************************************************************
* File        : escort_gps.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_GPS_H
#define ESCORT_GPS_H

extern uint16_t escort_gps_data_period;

void escort_gps_open(void);

void escort_gps_close(void);
#if NODE_GPRS_GPS_POWER_SHARE
void escort_gprs_gps_device_enable(bool_t enable);
#else
void escort_gprs_device_enable(bool_t enable);
#endif

void escort_gps_data_event_handle(void *arg);

void escort_gps_init(void);

#endif