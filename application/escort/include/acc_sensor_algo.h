/***************************************************************************
* File        : acc_sensor_algo.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/
#ifndef _ACC_SENSOR_ALGO_H
#define _ACC_SENSOR_ALGO_H
#include <acc_sensor_queue.h>

//存放原始采集来的X Y Z，
typedef struct
{
    int16_t  src_x;
    int16_t  src_y;
    int16_t  src_z;
}src_data_buffer_t;

//存放和加速度值和x y z轴加速度分量
typedef struct
{
    int16_t combine_acc;
    int16_t  x;
    int16_t  y;
    int16_t  z;
}combine_acc_buffer_t;

//存放合加速度、x、y、z的方差值
typedef struct
{
    fp32_t v_combine_acc;
    fp32_t  v_x;
    fp32_t  v_y;
    fp32_t  v_z;
}variance_t;

//存放计数值
typedef struct
{
    uint16_t  count1;
    uint16_t  count2;
}count_buffer_t;  

typedef struct
{
    int32_t sum_x;
    int32_t sum_y;
    int32_t sum_z;
    int16_t max_x;
    int16_t max_y;
    int16_t max_z;
    int16_t min_x;
    int16_t min_y;
    int16_t min_z;  
}sum_max_min_t;


//定义运动静止状态
typedef enum
{
    ACT_INVALID_STATUS,
    ACT_NORMAL_DRIVE_STATUS,//正常行驶状态
    ACT_STATIC_STATUS,//静止状态
    //ACT_IGNITION_STATIC_STATUS,//点火静止状态
    //ACT_FLAMEOUT_STATUS,//熄火状态    
    ACT_OVERRUN_STATUS,//超限状态    
}act_status_e;

//定义转向状态
typedef enum
{
    TURN_STATUS_T0,//初始状态
    TURN_STATUS_T1,//直行状态
    TURN_STATUS_T2,//左转状态
    TURN_STATUS_T3,//右转状态
    TURN_STATUS_T4,//数值累加状态
}turn_status_e;
//跌落状态
typedef enum
{
    DROP_STATUS_T0,//初始状态
    DROP_STATUS_T1,//正常状态
    DROP_STATUS_T2,//正常跌落状态
    DROP_STATUS_T3,//跌落状态
    DROP_STATUS_T4,//跌落转正常状态
}drop_status_e;
//倾倒状态
typedef enum
{
    TOPPLE_STATUS_T0,//初始状态
    TOPPLE_STATUS_T1,//正常状态
    TOPPLE_STATUS_T2,//倾倒状态
    TOPPLE_STATUS_T3,//倾倒转正常状态
    TOPPLE_STATUS_T4,//加减速状态
}topple_status_e;

//疑似拆除状态
typedef enum
{
    DISA_STATUS_T0,//00
    DISA_STATUS_T1,//01
    DISA_STATUS_T2,//10
    DISA_STATUS_T3,//11
}disa_suspect_status_e;

#define TURN_STRAIGHT_STATUS    0
#define TURN_LEFT_STATUS    1
#define TURN_RIGHT_STATUS    -1

#define DROP_NORMAL_STATUS      0//输出正常状态
#define DROP_DROP_STATUS           1//输出跌落状态

#define TOPPLE_NORMAL_STATUS      0//输出正常状态
#define TOPPLE_FLIP_STATUS           1//输出倾倒状态

#define DISASSEMBLY_NO_STATUS          0//
#define DISASSEMBLY_STATUS      1


int16_t acc_sensor_algo_median_proc(int16_t a,int16_t b,int16_t c);
void acc_sensor_algo_combine_proc(void);
variance_t acc_sensor_algo_variance_calc_proc(const combine_acc_buffer_t *pacc_buff);
void matrix_proc(const acc_circular_queue_t* queue,
                                const int16_t *parray_in,
                                int16_t *parray_out);

#endif

