/***************************************************************************
* File        : escort_dismantle.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_DISMANTLE_H
#define ESCORT_DISMANTLE_H

typedef enum
{
    ESCORT_DISMANTLE_SHELL,
    ESCORT_DISMANTLE_INSTALL,
    ESCORT_DISMANTLE_ACC,
	ESCORT_DISMANTLE_U18,
}e_escort_dismantle_type_t;

void escort_trigger_dismantle_event(e_escort_dismantle_type_t type);

bool_t escort_box_is_installed(void);

void escort_enable_dismantle_alarm(void);

void escort_dismantle_init(void);

void escort_dismantle_event_handle(void *arg);
//static void wait_dismantle_timer_start();
void escort_wait_dismantle_handle(void *arg);

void escort_dismantle_open_ssn_handle(void *arg);

//extern uint32_t escort_if_dismantle_key;

//#define ESCORT_DISMANTLE_VALID_KEY    0x4AA4A44A
//#define ESCORT_DISMANTLE_INVALID_KEY    0xAA4A44A4

#endif
