/***************************************************************************
* File        : escort_frame.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_FRAME_H
#define ESCORT_FRAME_H

// 报文类型
typedef enum
{
    ESCORT_APP_MESSAGE_REGISTER_TYPE = 0x01,
	ESCORT_APP_MESSAGE_CONFIG_TYPE = 0x02,
	ESCORT_APP_MESSAGE_QUERY_TYPE = 0x03,
	ESCORT_APP_MESSAGE_HEARTBEAT_TYPE = 0x04,
	ESCORT_APP_MESSAGE_LINKTEST_TYPE = 0x05,
	ESCORT_APP_MESSAGE_RESET_TYPE = 0x06,
	ESCORT_APP_MESSAGE_APP_TYPE =0x07 ,
    ESCORT_APP_MESSAGE_UNBIND_TYPE =0x11 ,//注销帧或解绑帧
}e_escort_message_type_t;

///////////////////////////////////////////////////////////
//应用数据 开始
///////////////////////////////////////////////////////////

// 应用数据类型
typedef enum
{
    ESCORT_APP_DATA_OVERRUN_TYPE = 0x01,
	ESCORT_APP_DATA_ACTIVITY_TYPE = 0x02,
	ESCORT_APP_DATA_FALL_TYPE = 0x03,
	ESCORT_APP_DATA_TOPPLE_TYPE = 0x04,
	ESCORT_APP_DATA_TURN_TYPE = 0x05,
	ESCORT_APP_DATA_GPS_TYPE = 0x06,
    ESCORT_APP_DATA_DISMANTLE_TYPE = 0x07,
    ESCORT_APP_DATA_HUMITURE_TYPE = 0x08,
	ESCORT_APP_DATA_ALGORITHM_TYPE = 0x11,
	ESCORT_APP_DATA_ALGORITHM_ACK_TYPE = 0x81,
	ESCORT_APP_DATA_SENSOR_TYPE = 0x12,
	ESCORT_APP_DATA_SENSOR_ACK_TYPE = 0x82,
}e_escort_app_data_type_t;

// 超限消息帧
typedef struct
{
    uint8_t status;
	int16_t relative_x;
	int16_t relative_y;
	int16_t relative_z;
}escort_overrun_data_t;

// 静动变化消息帧
typedef struct
{
    uint8_t status;//23
	uint8_t dimension_num;//24
	int16_t dimension_values[4];//25~28
}escort_activity_data_t;

// 跌落落体消息帧
typedef struct
{
    int16_t absolute_x;
	int16_t absolute_y;
	int16_t absolute_z;
}escort_fall_data_t;

// 倾倒消息帧
typedef struct
{
    uint8_t dimension_num;
	int16_t dimension_values[4];
}escort_topple_data_t;

// 转向消息帧
typedef struct
{
    uint8_t dimension_num;
	int16_t dimension_values[4];
}escort_turn_data_t;

// GPS消息帧
typedef struct
{
    double longitude;
	double latitude;
}escort_gps_data_t;

// 温湿度消息帧
typedef struct
{
    fp32_t temperature;
    fp32_t humidity;
}escort_humiture_data_t;

// 算法配置数据
typedef struct
{
    uint8_t type;
	uint8_t dimension_num;
	int16_t dimension_values[4];
}escort_alg_data_t;

// 算法配置数据应答
typedef struct
{
    uint8_t type;
	uint8_t result;
}escort_alg_data_ack_t;

// 传感器偏移补偿
typedef struct
{
    uint8_t type;
	uint8_t dimension_num;
	int16_t dimension_values[4];
}escort_sensor_offset_t;

// 传感器偏移补偿应答
typedef struct
{
    uint8_t type;
	uint8_t result;
}escort_sensor_offset_ack_t;

// 应用数据结构
typedef struct
{
    uint8_t type;//22
	union
	{
	    escort_overrun_data_t      overrun_data;
		escort_activity_data_t     activity_data;
	    escort_fall_data_t         fall_data;
		escort_topple_data_t       topple_data;
		escort_turn_data_t         turn_data;
		escort_gps_data_t          gps_data;
        escort_humiture_data_t     humiture_data;
		escort_alg_data_t          alg_data;
		escort_alg_data_ack_t      alg_data_ack;
		escort_sensor_offset_t     sensor_offset;
		escort_sensor_offset_ack_t sensor_offset_ack;
	};
}escort_app_data_t;

///////////////////////////////////////////////////////////
//应用数据 结束
///////////////////////////////////////////////////////////

// 心跳数据结构
typedef struct
{
    union
    {
        struct
        {
            uint8_t energy : 2;
            uint8_t topology : 1;
            uint8_t link : 1;
            uint8_t node_type : 2;
            uint8_t reserved : 2;
        };
        uint8_t control;
    };
    uint8_t energy_residual;
    uint8_t father_id[8];
    int8_t uplink_rssi;
    int8_t downlink_rssi;    
}escort_heartbeat_data_t;

#define ESCORT_VIN_LENGTH   17

// 注册数据结构
typedef struct
{
    uint8_t type;
    uint8_t vin[ESCORT_VIN_LENGTH];   
}escort_register_data_t;

// 报文结构
typedef struct
{
    uint8_t version;//0
	uint8_t type;//1
	uint16_t length;//2 3
	uint16_t seq;//4 5
	uint8_t dest_id[8];//6~13
	uint8_t src_id[8];//14~21
	union
	{
	    escort_app_data_t       app_data;
        escort_heartbeat_data_t heartbeat_data;
        escort_register_data_t  register_data;
	};
}escort_frame_t;

// 解析帧
bool_t escort_parse_frame(const uint8_t      *frame,
                          uint16_t            frame_len,
						  escort_frame_t     *app_frame);

// 组装帧
uint16_t escort_get_frame(uint8_t                  *frame,
                          uint16_t                  max_len,
					      const escort_frame_t     *app_frame);

///////////////////////////////////////////////////////////
//组装和解析应用数据报文体的static函数 开始
///////////////////////////////////////////////////////////

// 组装超限消息报文体
uint16_t escort_app_get_overrun_payload(uint8_t  *frame,
                                        uint16_t  max_len,
					 const escort_overrun_data_t *overrun_data);
									
// 组装静动变化消息报文体
uint16_t escort_app_get_activity_payload(uint8_t  *frame,
                                         uint16_t  max_len,
					 const escort_activity_data_t *activity_data);
									

// 组装跌落落体消息报文体
uint16_t escort_app_get_fall_payload(uint8_t  *frame,
                                     uint16_t  max_len,
					 const escort_fall_data_t *fall_data);
								 
// 组装倾倒消息报文体
uint16_t escort_app_get_topple_payload(uint8_t  *frame,
                                       uint16_t  max_len,
					 const escort_topple_data_t *topple_data);

// 组装转向消息报文体
uint16_t escort_app_get_turn_payload(uint8_t                  *frame,
                                     uint16_t                  max_len,
								     const escort_turn_data_t *turn_data);
								 
// 组装GPS位置报文体
uint16_t escort_app_get_gps_payload(uint8_t                 *frame,
                                    uint16_t                 max_len,
								    const escort_gps_data_t *gps_data);

// 组装温湿度报文体
uint16_t escort_app_get_humiture_payload(uint8_t                 *frame,
                                         uint16_t                 max_len,
								    const escort_humiture_data_t *humiture_data);
								
// 组装算法参数配置应答报文体
uint16_t escort_app_get_alg_ack_payload(uint8_t  *frame,
                                        uint16_t  max_len,
					 const escort_alg_data_ack_t *alg_data_ack);
									
// 组装传感器偏移补偿应答报文体
uint16_t escort_app_get_sensor_ack_payload(uint8_t  *frame,
                                           uint16_t  max_len,
				   const escort_sensor_offset_ack_t *sensor_offset_ack);
										   
// 解析算法参数配置应答报文体
bool_t escort_app_parse_alg_ack_payload(const uint8_t     *frame,
                                        uint16_t           frame_len,
									    escort_alg_data_t *alg_data);
									
// 解析传感器偏移补偿应答报文体
bool_t escort_app_parse_sensor_ack_payload(const uint8_t *frame,
                                           uint16_t       frame_len,
								  escort_sensor_offset_t *sensor_offset);
									   
///////////////////////////////////////////////////////////
//组装和解析应用数据报文体的static函数 结束
///////////////////////////////////////////////////////////

#endif //ESCORT_FRAME_H