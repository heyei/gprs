/***************************************************************************
* File        : escort_startup.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_STARTUP_H
#define ESCORT_STARTUP_H

void escort_startup(void);
void escort_init_open_ssn_handle(void *arg);
void escort_startup_timer_handle(void *arg);
#if NODE_SSN_DEBUG  
void escort_startup_timer_test_handle(void *arg);
#endif
#endif
