/***************************************************************************
* File        : escort_acc.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_ACC_H
#define ESCORT_ACC_H

void escort_acc_sensor_int_event_handle(void *arg);

void escort_acc_data_timer_event_handle(void *arg);

void escort_enable_acc_alg(void);

void escort_acc_sensor_init(void);

void escort_act_to_static_open_ssn_handle(void *arg);

#endif