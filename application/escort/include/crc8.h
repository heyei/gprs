#ifndef CRC8_H
#define CRC8_H

uint8_t crc8_calc(const uint8_t *buf,
                  uint16_t       len);

bool_t crc8_check(const uint8_t *buf,
                  uint16_t       len,
				  uint8_t        crc8);

#endif