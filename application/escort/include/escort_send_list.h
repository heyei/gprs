/***************************************************************************
* File        : escort_send_list.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_SEND_LIST_H
#define ESCORT_SEND_LIST_H

typedef enum
{
    ESCORT_SEND_LIST_PRIORITY_LOW,
	ESCORT_SEND_LIST_PRIORITY_HIGH,
	
	ESCORT_SEND_LIST_STATUS_WAITING,
	ESCORT_SEND_LIST_STATUS_SENDING,
}e_escort_send_list_t;

typedef struct
{
    uint16_t seq;
}escort_ack_id_t;

void escort_send_list_init(uint16_t size);

uint16_t escort_send_list_length();

bool_t escort_send_list_add(escort_frame_t *frame, 
                            uint8_t         priority, 
                            uint8_t         allowed_send_times,
                            bool_t          need_ack);

void escort_send_list_fetch(escort_frame_t **frame,
                            uint16_t        *frame_id,
                            uint8_t         channal_type);

void escort_send_list_sent(uint16_t frame_id, bool_t is_ok,uint8_t ssn_to_gprs_flag,uint8_t gprs_to_ssn_flag);

bool_t escort_send_list_is_ack_needed(uint16_t frame_id);

bool_t escort_send_list_ack_received(escort_ack_id_t *ack_id,
                                     uint16_t        *frame_id);

void escort_send_list_no_ack(uint16_t frame_id);

#endif //ESCORT_FRAME_SEND_LIST_H
