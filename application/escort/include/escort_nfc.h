/***************************************************************************
* File        : escort_nfc.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_NFC_H
#define ESCORT_NFC_H

#define ESCORT_VIN_VALID_KEY      0x5AA5A55A
#define ESCORT_VIN_INVALID_KEY    0x00000000

extern uint8_t escort_vin[];
extern uint32_t escort_vin_valid_key;
extern bool_t escort_is_bounden;

void escort_nfc_start_check_timer(void);
void escort_nfc_stop_check_timer(void);
void escort_nfc_init(void);

//typedef void (*escort_nfc_int_cb)(void);
void escort_nfc_int_handle(void *arg);
//escort_nfc_int_cb escort_nfc_int_handle;
void m24lr64e_int_handler(void);
void escort_nfc_stop_check_timer_handle(void *arg);
#endif
