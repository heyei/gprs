/***************************************************************************
* File        : escort_humiture.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_HUMITURE_H
#define ESCORT_HUMITURE_H

void escort_humiture_init();
void escort_humiture_set_period(uint32_t period);
void escort_humiture_timer_handle(void *arg);

#endif