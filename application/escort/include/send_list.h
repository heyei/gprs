/***************************************************************************
* File        : send_list.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef SEND_LIST_H
#define SEND_LIST_H

// 比较两个发送对象的优先级
// 返回：
//     TRUE  obj_a应该排在obj_b的前面, 优先发送
//     FALSE obj_a应该排在obj_b的后面, 推迟发送
typedef bool_t (*send_list_priority_cmp_t)(void *obj_a, void *obj_b);

// 检查对象是否符合条件
// 参数：
//     obj 被检查的对象
//     arg 用来检查的参数
// 返回：
//     TRUE  符合
//     FALSE 不符合
typedef bool_t (*send_list_check_t)(void *obj_a, void *arg,uint8_t channal_type);

typedef struct
{
    list_head_t              head;
	uint16_t                 obj_size;
	send_list_priority_cmp_t priority_cmp_func;
}send_list_t;

void send_list_init(send_list_t             *send_list,
                    uint16_t                 obj_size,
					send_list_priority_cmp_t prio_cmp_func);

// 向发送对列中添加新发送对象, 先后位置按优先级排列;
// obj的值会被拷贝出来
bool_t send_list_add(send_list_t *send_list,
                     const void  *obj);

// 查找第一个符合条件的发送对象, 返回其地址;
// 如果check_func == NULL, 就返回对列中的第一个
void *send_list_find_first(send_list_t      *send_list,
                           send_list_check_t check_func,
						   void *arg,
                           uint8_t         channal_type);

// 移除一个发送对象
void send_list_remove(send_list_t *send_list,
                      void        *obj);

// 释放发送对列					  
void send_list_free(send_list_t *send_list);

uint16_t send_list_length(send_list_t *send_list);

#endif //ESCORT_FRAME_SEND_BUF_H
