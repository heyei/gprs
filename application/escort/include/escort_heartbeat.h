/***************************************************************************
* File        : escort_heartbeat.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_HEARTBEAT_H
#define ESCORT_HEARTBEAT_H

void escort_heartbeat_init();
void escort_heartbeat_timer_start(uint32_t period);
void escort_heartbeat_timer_stop();
void escort_heartbeat_timer_handle(void *arg);
void escort_low_power_1_handle(void *arg);
void escort_low_power_2_handle(void *arg);
void escort_static_heart_open_ssn_handle(void *arg);

#endif
