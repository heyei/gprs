/***************************************************************************
* File        : escort_send.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/5
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/5         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef ESCORT_SEND_H
#define ESCORT_SEND_H

typedef enum
{
    ESCORT_SEND_NO_CHANNEL,
    ESCORT_SEND_GPRS_CHANNEL,
    ESCORT_SEND_SSN_CHANNEL,
    ESCORT_SEND_UART_CHANNEL,
}e_escort_data_channel_t;

extern e_escort_data_channel_t escort_current_data_channel;

void escort_send_request(escort_frame_t *frame, 
                         uint8_t         priority, 
                         uint8_t         allowed_send_times,
                         bool_t          need_ack);

void escort_wait_sending_timer_event_handle(void *arg);

void escort_wait_ack_timer_event_handle(void *arg);

void escort_data_sent_event_handle(void *arg);

void escort_data_received_event_handle(void *arg);

void escort_ssn_ctrl_event_handle(void *arg);

void escort_send_init(void);
void ssn_open_cb(bool_t res);
void ssn_close_cb(bool_t res);
void escort_ssn_ctrl_send_handle(void *arg);
#endif
