/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : NFC                                                                                         
**  File        : escort_nfc.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是NFC的初始化、配置处理                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <osel_arch.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>
#include <escort_startup.h>
#include <escort_dismantle.h>
#include <escort_led.h>
#include <nfc_module.h>
#include <escort_nfc.h>
#include <hal_uart.h>

extern bool_t delay_send_tag;

#define ESCORT_NFC_CHECK_TIME_LIMIT  120000

extern device_info_t device_info;
__no_init uint8_t escort_vin[ESCORT_VIN_LENGTH];
__no_init uint32_t escort_vin_valid_key;
bool_t escort_is_bounden = FALSE;
static hal_timer_t *nfc_check_timer = NULL;
//static uint32_t check_times = 0;

#define WAIT_UNBIND_TO_RESET_PERIOD      120000
//static hal_timer_t *wait_unbind_timer = NULL;//发生防拆后起的定时器

//车押2.5 增加解绑后全局标志位
uint8_t unbind_flag = 0;
//车押2.5：增加单次敲击可配置标志位
volatile uint8_t nfc_may_cfg_flag = 0;

#if NODE_NFC_DEBUG
#define ESCORT_NFC_ECHO_COUNT       3
#define ESCORT_NFC_WAIT_TIME_LIMIT  2000 //2S
#define ESCORT_NFC_ECHO_TIME_LIMIT  500 //0.5S
static hal_timer_t *nfc_wait_isr_timer = NULL;
static hal_timer_t *nfc_echo_timer = NULL;//重复读取定时器
//nfc重复读取定时器:0.5s
static void nfc_echo_timer_cb(void *arg)
{    
    nfc_echo_timer = NULL;    
    osel_post(ESCORT_NFC_INT_EVENT, NULL, OSEL_EVENT_PRIO_LOW);//OSEL_EVENT_PRIO_LOW
}

static void start_echo_timer(void)
{
  	if (NULL != nfc_echo_timer)
    {
        hal_timer_cancel(&nfc_echo_timer);
        DBG_ASSERT(nfc_echo_timer == NULL __DBG_LINE);
    }
	
    HAL_TIMER_SET_REL(MS_TO_TICK(ESCORT_NFC_ECHO_TIME_LIMIT),
                      nfc_echo_timer_cb,
                      NULL,
                      nfc_echo_timer);
    DBG_ASSERT(nfc_echo_timer != NULL __DBG_LINE);
}
//nfc中断延迟定时器：2s
static void nfc_wait_isr_timer_cb(void *arg)
{    
    nfc_wait_isr_timer = NULL;    
    osel_post(ESCORT_NFC_INT_EVENT, NULL, OSEL_EVENT_PRIO_LOW);//OSEL_EVENT_PRIO_LOW
}

static void start_wait_isr_timer(void)
{
  	if (NULL != nfc_wait_isr_timer)
    {
        hal_timer_cancel(&nfc_wait_isr_timer);
        DBG_ASSERT(nfc_wait_isr_timer == NULL __DBG_LINE);
    }
	
    HAL_TIMER_SET_REL(MS_TO_TICK(ESCORT_NFC_WAIT_TIME_LIMIT),
                      nfc_wait_isr_timer_cb,
                      NULL,
                      nfc_wait_isr_timer);
    DBG_ASSERT(nfc_wait_isr_timer != NULL __DBG_LINE);
}
#endif


static void nfc_check_timer_cb(void *arg)
{
 	nfc_may_cfg_flag = 0; 
    osel_post(ESCORT_NFC_STOP_CHECK_TIME_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void escort_nfc_stop_check_timer_handle(void *arg)
{
    escort_nfc_stop_check_timer();
}

static void start_check_timer(void)
{
  	if (NULL != nfc_check_timer)
    {
        hal_timer_cancel(&nfc_check_timer);
        DBG_ASSERT(nfc_check_timer == NULL __DBG_LINE);
    }
	
    HAL_TIMER_SET_REL(MS_TO_TICK(ESCORT_NFC_CHECK_TIME_LIMIT),
                      nfc_check_timer_cb,
                      NULL,
                      nfc_check_timer);
    DBG_ASSERT(nfc_check_timer != NULL __DBG_LINE);
}

void escort_nfc_start_check_timer(void)
{
    if (NULL != nfc_check_timer)
    {
        hal_timer_cancel(&nfc_check_timer);
        DBG_ASSERT(nfc_check_timer == NULL __DBG_LINE);
    }
	
    escort_led_enter_state(ESCORT_LED_CAN_CFG);
    //车押2.5：增加单次敲击可配置标志位
    nfc_may_cfg_flag = 1;
    start_check_timer();
}

void escort_nfc_stop_check_timer(void)
{
    if (NULL != nfc_check_timer)
    {
        hal_timer_cancel(&nfc_check_timer);
        DBG_ASSERT(nfc_check_timer == NULL __DBG_LINE);
    }

    escort_led_exit_state(ESCORT_LED_CAN_CFG);
}

void escort_nfc_init(void)
{
    result_t result;
    nfc_data_info_t nfc_data;
	    
    escort_is_bounden = FALSE;
    // 向NFC写入设备ID
    nfc_data.app_type = APP_TYPE_DEVICE_INFO_ACK;
    nfc_data.nfc_device_info_pld.info_type = PARA_TYPE_DEVICE_NUI;
    memcpy(nfc_data.nfc_device_info_pld.device_nui, device_info.device_id, 8);
    nfc_write_info(&nfc_data);
    
    nfc_check_timer = NULL;
	nfc_may_cfg_flag = 0;

#if NODE_SSN_DEBUG  
    escort_vin_valid_key = ESCORT_VIN_VALID_KEY;
    escort_startup();
#else   
    if (escort_vin_valid_key == ESCORT_VIN_VALID_KEY) //判断是否有缓存的车架号
    {
        escort_is_bounden = TRUE;
    }
    else
    {
        // 从NFC读取车架号
        result = nfc_read_info(&nfc_data, APP_TYPE_BIND_INFO_CFG, 
                               PARA_TYPE_CARRIAGE_NUM);
//        DBG_ASSERT(result != NFC_READ_FALSE __DBG_LINE);
        if (result == NFC_READ_SUCCESS)
        {
            memcpy(escort_vin, 
                   nfc_data.nfc_bound_info_pld.carriage_info.carriage_info, 
                   ESCORT_VIN_LENGTH);
            escort_vin_valid_key = ESCORT_VIN_VALID_KEY; // RAM中的车架号置为有效值
            escort_is_bounden = TRUE;
        }
    }
    
    if (escort_is_bounden && escort_box_is_installed())
    { // 已绑定车架号并且防拆开关已经按下，才启动正常流程
        escort_led_enter_state(ESCORT_LED_WORK);//进入工作状态
        escort_startup();
    }
#endif     
}
#if NODE_NFC_DEBUG == 0
void m24lr64e_int_handler(void)
{        
	osel_post(ESCORT_NFC_INT_EVENT, NULL, OSEL_EVENT_PRIO_LOW);//OSEL_EVENT_PRIO_LOW
}

void escort_nfc_int_handle(void *arg)
{
    nfc_info_flag_t nfc_info_flag = {0};
    bool_t result;
    nfc_data_info_t nfc_data;
    
    hal_wdt_clear(16000);
    
    nfc_read_info_flag(&nfc_info_flag);

    if (nfc_may_cfg_flag)
    {
        if (nfc_info_flag.carriage_num)
        {
            // 清除标志
			do
			{
			  	result = nfc_clr_info_flag(FLAG_BING_INFO);
			} while(result == FALSE);
			
            // 能否读到绑定的车架号
            result = nfc_read_info(&nfc_data, APP_TYPE_BIND_INFO_CFG, 
                                   PARA_TYPE_CARRIAGE_NUM);
    //        DBG_ASSERT(result != NFC_READ_FALSE __DBG_LINE);
            if (result == NFC_READ_SUCCESS)
            {
                memcpy(escort_vin, 
                       nfc_data.nfc_bound_info_pld.carriage_info.carriage_info, 
                       ESCORT_VIN_LENGTH);
                escort_vin_valid_key = ESCORT_VIN_VALID_KEY; // RAM中的车架号置为有效值
                // 向NFC写入绑定ACK
                nfc_data.app_type = APP_TYPE_BIND_INFO_CFG_ACK;
                nfc_data.nfc_bound_info_cfg_ack.info_type = PARA_TYPE_CARRIAGE_NUM;
                nfc_data.nfc_bound_info_cfg_ack.result = result;
                DBG_ASSERT(TRUE == nfc_write_info(&nfc_data) __DBG_LINE);  				
								
                hal_board_reset();
            }
        }
        
        if (nfc_info_flag.unbind)
        {	
			do
			{
			  	result = nfc_clr_info_flag(FLAG_BING_INFO);
			} while(result == FALSE);

			do
			{
			  	result = nfc_clr_info_flag(FLAG_UNBIND_INFO);
			} while(result == FALSE);

			do
			{
			  	result = nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_CARRIAGE_NUM);
			} while(result == NFC_READ_FALSE);
			
			do
			{
			  	result = nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_UNBIND_INFO);
			} while(result == NFC_READ_FALSE);
			
//            nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_UNBIND_INFO);
            
            escort_vin_valid_key = ESCORT_VIN_INVALID_KEY; // RAM中的车架号置为无效值        
            // 向NFC写入解绑定ACK
            nfc_data.app_type = APP_TYPE_BIND_INFO_CFG_ACK;
            nfc_data.nfc_bound_info_cfg_ack.info_type = PARA_TYPE_UNBIND_INFO;
            nfc_data.nfc_bound_info_cfg_ack.result = NFC_READ_SUCCESS;              
            DBG_ASSERT(TRUE == nfc_write_info(&nfc_data) __DBG_LINE);  
			           
            unbind_flag = TRUE;
            //车押2.5：增加解绑帧发送
            escort_frame_t frame;
            frame.type = ESCORT_APP_MESSAGE_UNBIND_TYPE;
			//如果正在延时发送定时器内，关闭延时发送定时器
			if(delay_send_tag == TRUE)
			{
				delay_send_tag = FALSE;
				osel_post(STOP_GPRS_DELAY_TIMER, NULL, OSEL_EVENT_PRIO_LOW);
			}
            escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                    1, FALSE);
            //解绑成功后停掉定时器
            escort_nfc_stop_check_timer();
            nfc_may_cfg_flag = 0;
        }
		else 
		{
		  	_NOP();
		}
    }
	hal_wdt_stop(16000);
}
#else
void m24lr64e_int_handler(void)
{
//    hal_uart_send_char(HAL_UART_2, 1);
	start_wait_isr_timer();
}

void escort_nfc_int_handle(void *arg)
{
    bool_t result;
    nfc_data_info_t nfc_data;
    
    static nfc_info_flag_t nfc_info_flag = {0};
    static uint8_t read_flag_count = 0;
    static uint8_t read_data_count = 0;
    static bool_t read_flag_succ_flag = FALSE;
    
    static uint8_t clr_bind_flag_count = 0;
    static uint8_t clr_unbind_flag_count = 0;
    static uint8_t clr_bind_info_count = 0;
    static uint8_t clr_unbind_info_count = 0;
    
    hal_wdt_clear(16000);    
        
    if (nfc_may_cfg_flag) //处于可配置状态
    {
        if (FALSE == read_flag_succ_flag)
        {
            if (nfc_read_info_flag(&nfc_info_flag))
            {
                read_flag_count = 0;                        
                read_flag_succ_flag = TRUE; //记录读取成功的标志位
                
                //判断有几个数据待读取，两个或两个以上进断言 TODO 
                if (nfc_info_flag.carriage_num && nfc_info_flag.unbind)
                {
                    DBG_ASSERT(FALSE __DBG_LINE);
                }
            }
            else
            {
                read_flag_count++;
                if (read_flag_count > ESCORT_NFC_ECHO_COUNT)
                {
//                    return;
                    DBG_ASSERT(FALSE __DBG_LINE);//2014-12-09 解绑不成功测试
                }
                start_echo_timer(); //2014-12-09 解绑不成功测试          
            }            
        }
    }
    else
    {
        hal_wdt_stop(16000);        
        return; //不处于可配置状态
    }
    
    /* 连续读取数据三次失败，恢复所有标志位，放弃操作 */
    if (read_data_count++ > ESCORT_NFC_ECHO_COUNT)
    {
        read_flag_count = 0;
        read_data_count = 0;
        read_flag_succ_flag = FALSE;
        hal_wdt_clear(16000);
//        return;
        DBG_ASSERT(FALSE __DBG_LINE);//2014-12-09 解绑不成功测试
    }
    
    if (nfc_info_flag.carriage_num) //绑定数据待读取
    {
        // 能否读到绑定的车架号
        result = nfc_read_info(&nfc_data, APP_TYPE_BIND_INFO_CFG, 
                               PARA_TYPE_CARRIAGE_NUM);
        
        if (result == NFC_READ_SUCCESS)
        {
            memcpy(escort_vin, 
                   nfc_data.nfc_bound_info_pld.carriage_info.carriage_info, 
                   ESCORT_VIN_LENGTH);
            escort_vin_valid_key = ESCORT_VIN_VALID_KEY; // RAM中的车架号置为有效值
            // 清除标志
            do
            {
                result = nfc_clr_info_flag(FLAG_BING_INFO);
                //2014-12-09 解绑不成功测试
                if (clr_bind_flag_count++ > 16)
                {
                    DBG_ASSERT(FALSE __DBG_LINE);                    
                }
            } while(result == FALSE);   
                           
            hal_board_reset();
        }
        else
        {
            start_echo_timer();//启动定时器
        }
    }
    else if (nfc_info_flag.unbind)
    {
        do
        {
            result = nfc_clr_info_flag(FLAG_BING_INFO);
            //2014-12-09 解绑不成功测试
            if (clr_bind_flag_count++ > 16)
            {
                DBG_ASSERT(FALSE __DBG_LINE);                
            }            
        } while(result == FALSE);
        
        clr_bind_flag_count = 0;
        
        do
        {
            result = nfc_clr_info_flag(FLAG_UNBIND_INFO);
            //2014-12-09 解绑不成功测试
            if (clr_unbind_flag_count++ > 16)
            {
                DBG_ASSERT(FALSE __DBG_LINE);               
            }            
        } while(result == FALSE);

        clr_unbind_flag_count = 0;
        
        do
        {
            result = nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_CARRIAGE_NUM);
            //2014-12-09 解绑不成功测试
            if (clr_bind_info_count++ > 16)
            {
                DBG_ASSERT(FALSE __DBG_LINE);                
            }             
        } while(result == NFC_READ_FALSE);
        
        clr_bind_info_count = 0;
        
        do
        {
            result = nfc_clr_info(APP_TYPE_BIND_INFO_CFG, PARA_TYPE_UNBIND_INFO);
            //2014-12-09 解绑不成功测试
            if (clr_unbind_info_count++ > 16)
            {
                DBG_ASSERT(FALSE __DBG_LINE);                
            }               
        } while(result == NFC_READ_FALSE);
        
        clr_unbind_info_count = 0;
        
        escort_vin_valid_key = ESCORT_VIN_INVALID_KEY; // RAM中的车架号置为无效值        
                   
        unbind_flag = TRUE;
        //车押2.5：增加解绑帧发送
        escort_frame_t frame;
        frame.type = ESCORT_APP_MESSAGE_UNBIND_TYPE;
        //如果正在延时发送定时器内，关闭延时发送定时器
        if(delay_send_tag == TRUE)
        {
            delay_send_tag = FALSE;
            osel_post(STOP_GPRS_DELAY_TIMER, NULL, OSEL_EVENT_PRIO_LOW);
        }
        escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                1, FALSE);
        //解绑成功后停掉定时器
        escort_nfc_stop_check_timer();
        nfc_may_cfg_flag = 0;
    }
    else if (nfc_info_flag.profile_id)//复位用
    {
        hal_board_reset();
    }
    else if (nfc_info_flag.ssn_para)//ssn 配置
    {
        result = nfc_read_info(&nfc_data, APP_TYPE_SSN_DEV_INFO, 
                   PARA_TYPE_ALL_SSN_INFO);
        if (result == NFC_READ_SUCCESS)
        {
            hal_board_reset();
        }        
    }
    else
    {
//        DBG_ASSERT(FALSE __DBG_LINE);// 断言TODO
        //此处由于中断会进两次的问题，暂时不进入断言操作
        read_flag_count = 0;
        read_data_count = 0;        
        read_flag_succ_flag = FALSE;
    }
    
	hal_wdt_stop(16000);
}
#endif

