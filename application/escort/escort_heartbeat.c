/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : 心跳                                                                                          
**  File        : escort_heartbeat.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是心跳帧的发送处理                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <osel_arch.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <hal_energy.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>
#include <escort_nfc.h>
#include <hal.h>
#include <nfc_module.h>
#include <gprs.h>
#include <ssn.h>
#include <mac_moniter.h>

static hal_timer_t *heartbeat_timer = NULL;

//为电压采集设定定时器
static hal_timer_t *low_power_timer_1 = NULL;
static hal_timer_t *low_power_timer_2 = NULL;
static uint8_t low_power_data = 0;
#define LOW_POWER_TIMER_1	1000
#define LOW_POWER_TIMER_2	1450000//24分钟

static bool_t heartbeat_timer_started = FALSE;
static uint32_t heartbeat_period = 0;
static void low_power_timer_2_start();

extern uint8_t heart_if_static_flag;
extern uint8_t ssn_open_type;
volatile uint8_t heart_first_send_flag = FALSE;

static void low_power_timer_1_cb(void *arg)
{
  	low_power_timer_1 = NULL;
    osel_post(ESCORT_POWER_TIMER_1_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void low_power_timer_1_start()
{
    if (NULL != low_power_timer_1)
    {
        hal_timer_cancel(&low_power_timer_1);
        DBG_ASSERT(low_power_timer_1 == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(LOW_POWER_TIMER_1),
                      low_power_timer_1_cb,
                      NULL,
                      low_power_timer_1);
    DBG_ASSERT(low_power_timer_1 != NULL __DBG_LINE);  	
}


static void low_power_timer_2_cb(void *arg)
{
	low_power_timer_2 = NULL;
    osel_post(ESCORT_POWER_TIMER_2_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void low_power_timer_2_start()
{
    if (NULL != low_power_timer_2)
    {
        hal_timer_cancel(&low_power_timer_2);
        DBG_ASSERT(low_power_timer_2 == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(LOW_POWER_TIMER_2),
                      low_power_timer_2_cb,
                      NULL,
                      low_power_timer_2);
    DBG_ASSERT(low_power_timer_2 != NULL __DBG_LINE);  	
}



static void heartbeat_timer_cb(void *arg)
{
    heart_first_send_flag = FALSE;
    heartbeat_timer = NULL;
    osel_post(ESCORT_HEARTBEAT_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void heartbeat_timer_start()
{
    if (heartbeat_period == 0)
    {
        return;
    }
    
    if (NULL != heartbeat_timer)
    {
        hal_timer_cancel(&heartbeat_timer);
        DBG_ASSERT(heartbeat_timer == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(heartbeat_period),
                      heartbeat_timer_cb,
                      NULL,
                      heartbeat_timer);
    DBG_ASSERT(heartbeat_timer != NULL __DBG_LINE);	
}

void escort_heartbeat_init()
{
    heartbeat_timer = NULL;
	low_power_timer_1 = NULL;
	low_power_timer_2 = NULL;
    heartbeat_timer_started = FALSE;
    heartbeat_period = 0;
}

void escort_heartbeat_timer_start(uint32_t period)
{
    heartbeat_period = period;
    heartbeat_timer_started = TRUE;
    heart_first_send_flag = TRUE;
	
	//刚启动获取一次电压值
	low_power_data |= hal_energy_get();
	//定时low_power_timer_1
	low_power_timer_1_start();
}

void escort_heartbeat_timer_stop()
{
    heartbeat_timer_started = FALSE;
    if (NULL != heartbeat_timer)
    {
        hal_timer_cancel(&heartbeat_timer);
        DBG_ASSERT(heartbeat_timer == NULL __DBG_LINE);
    }
}

void escort_low_power_1_handle(void *arg)
{
    low_power_data |= hal_energy_get();
    osel_post(ESCORT_HEARTBEAT_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

void escort_low_power_2_handle(void *arg)
{
    low_power_data |= hal_energy_get();
    low_power_timer_2_start();
}

void escort_heartbeat_timer_handle(void *arg)
{
    escort_frame_t frame;
//    uint8_t reset_hb[2] = {0};
    uint16_t reset_line = 0;
    uint32_t monitor_timer_len = 0;
    
    if (!heartbeat_timer_started)
    {
        return;
    }
    
    heartbeat_timer_start(); //继续定时
	//增加low_power_timer_2定时器定时
	low_power_timer_2_start();	
    
    //判断此时车辆状态是否为静止,并且不是第一次发心跳，如果是静止则寻找一次ssn网络
    if ((!heart_first_send_flag) && (heart_if_static_flag))
    {   
        ssn_open_type |= 0x04;
        monitor_timer_len = mac_moniter_interval_time_get();
        DBG_ASSERT(monitor_timer_len != 0 __DBG_LINE);
        ssn_open(ssn_open_cb, monitor_timer_len);        
    }
    //如果第一次启动或者是车辆在运动就发送，如果是以后启动就在ssn网络打开后在发送
    if ((heart_first_send_flag) || (!heart_if_static_flag))
    {
//        hal_nvmem_read((uint8_t *)DEBUG_INFO_ADDR, reset_hb, 2);
        reset_line = debug_get_info();
               
        frame.type = ESCORT_APP_MESSAGE_HEARTBEAT_TYPE;
        frame.heartbeat_data.energy = 2; //支持上报, 电池供电
        frame.heartbeat_data.topology = 0; //不支持拓扑
        frame.heartbeat_data.link = 0; //不支持链路质量
        frame.heartbeat_data.node_type = 0; //终端类型
        frame.heartbeat_data.energy_residual = low_power_data;//获取电压值
        low_power_data = 0;
        memset(frame.heartbeat_data.father_id, 0, 8);
    //        frame.heartbeat_data.uplink_rssi = 0;
    //        frame.heartbeat_data.downlink_rssi = 0;        
        frame.heartbeat_data.uplink_rssi = (uint8_t)(reset_line >> 8);
        frame.heartbeat_data.downlink_rssi = (uint8_t)reset_line;
        
        //如果解绑或强拆后的两分钟内不会再发心跳，此处可能影响刚开机的发心跳，但影响的概率非常非常低
        if (escort_vin_valid_key != ESCORT_VIN_INVALID_KEY)
        {
            //nfc保存log
            //nfc_save_log_info(TYPE_HB_DATA);
            escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                                1, FALSE);            
        }        
    }
}

void escort_static_heart_open_ssn_handle(void *arg)
{
    escort_frame_t frame;
    uint8_t reset_hb[2] = {0};
    hal_nvmem_read((uint8_t *)DEBUG_INFO_ADDR, reset_hb, 2);
           
    frame.type = ESCORT_APP_MESSAGE_HEARTBEAT_TYPE;
    frame.heartbeat_data.energy = 2; //支持上报, 电池供电
    frame.heartbeat_data.topology = 0; //不支持拓扑
    frame.heartbeat_data.link = 0; //不支持链路质量
    frame.heartbeat_data.node_type = 0; //终端类型
    frame.heartbeat_data.energy_residual = low_power_data;//获取电压值
    low_power_data = 0;
    memset(frame.heartbeat_data.father_id, 0, 8);
    //        frame.heartbeat_data.uplink_rssi = 0;
    //        frame.heartbeat_data.downlink_rssi = 0;        
    frame.heartbeat_data.uplink_rssi = reset_hb[1];
    frame.heartbeat_data.downlink_rssi = reset_hb[0];

    //如果解绑或强拆后的两分钟内不会再发心跳，此处可能影响刚开机的发心跳，但影响的概率非常非常低
    if (escort_vin_valid_key != ESCORT_VIN_INVALID_KEY)
    {
        //nfc保存log
        //nfc_save_log_info(TYPE_HB_DATA);
        escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
                            1, FALSE);            
    }       
}


