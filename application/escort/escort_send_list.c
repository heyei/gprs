/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : 发送链表                                                                                         
**  File        : escort_send_list.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是发送链表处理                             
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <debug.h>
#include <escort_send.h>

typedef struct
{
    uint8_t        priority;
    uint16_t       frame_id;
    uint8_t        status;
    uint8_t        allowed_send_times;
    bool_t         need_ack;
    escort_frame_t frame;
}escort_send_list_obj_t;

static send_list_t escort_send_list;
static uint16_t escort_frame_id = 0;
static uint16_t escort_send_list_size = 0;

// 比较两个发送对象的优先级
// 返回：
//     TRUE  escort_obj_a应该排在escort_obj_b的前面, 优先发送
//     FALSE escort_obj_a应该排在escort_obj_b的后面, 推迟发送
static bool_t escort_send_list_priority_cmp(void *escort_obj_a,
                                            void *escort_obj_b)
{
    escort_send_list_obj_t *a;
    escort_send_list_obj_t *b;
    
    a = (escort_send_list_obj_t *)escort_obj_a;
    b = (escort_send_list_obj_t *)escort_obj_b;
    
    return (a->priority > b->priority);
}
                                     
// 检查发送对象的标识
static bool_t escort_send_list_check_frame_id(void *escort_obj,
                                              void *frame_id,
                                              uint8_t channal_type)
{
    escort_send_list_obj_t *obj;
    uint16_t *id;
    
    obj = (escort_send_list_obj_t *)escort_obj;
    id = (uint16_t *)frame_id;
    
    return (obj->frame_id == *id);
}

// 检查是否是对应的ACK
static bool_t escort_send_list_check_ack(void *escort_obj,
                                         void *ack_id,
                                         uint8_t channal_type)
{
    escort_send_list_obj_t *obj;
    escort_ack_id_t *id;
    
    obj = (escort_send_list_obj_t *)escort_obj;
    id = (escort_ack_id_t *)ack_id;
    
    return (obj->frame.seq == id->seq);
}
                                  
// 检查是否是等待发送的状态
static bool_t escort_send_list_check_waiting(void *escort_obj, void *arg,uint8_t channal_type)
{
    escort_send_list_obj_t *obj;
    
    (void)arg;
    obj = (escort_send_list_obj_t *)escort_obj;
//    if (ESCORT_SEND_GPRS_CHANNEL == channal_type)
//    {
//        return (obj->status == ESCORT_SEND_LIST_STATUS_WAITING);
//    }
//    else
//    {
//        return TRUE;
//    }
    return (obj->status == ESCORT_SEND_LIST_STATUS_WAITING);
}

// 检查优先级是否是 ESCORT_SEND_LIST_PRIORITY_LOW
static bool_t escort_send_list_check_priority_low(void *escort_obj, void *arg,uint8_t channal_type)
{
    escort_send_list_obj_t *obj;
    
    (void)arg;
    obj = (escort_send_list_obj_t *)escort_obj;
    
    return (obj->priority == ESCORT_SEND_LIST_PRIORITY_LOW);
}

// 检查优先级是否是 ESCORT_SEND_LIST_PRIORITY_HIGH
static bool_t escort_send_list_check_priority_high(void *escort_obj, void *arg,uint8_t channal_type)
{
    escort_send_list_obj_t *obj;
    
    (void)arg;
    obj = (escort_send_list_obj_t *)escort_obj;
    
    return (obj->priority == ESCORT_SEND_LIST_PRIORITY_HIGH);
}

uint16_t escort_send_list_length()
{
    return send_list_length(&escort_send_list);
}

bool_t escort_send_list_add(escort_frame_t *frame, 
                            uint8_t         priority, 
                            uint8_t         allowed_send_times,
                            bool_t          need_ack)
{
    escort_send_list_obj_t send_obj;
    
    send_obj.priority = priority;
    send_obj.allowed_send_times = allowed_send_times;
    send_obj.need_ack = need_ack;
    send_obj.frame = *frame;
    send_obj.frame_id = escort_frame_id++;
    if (escort_frame_id == 0xFFFF)
    {
        escort_frame_id = 0;
    }
    send_obj.status = ESCORT_SEND_LIST_STATUS_WAITING;
    
    if (send_list_length(&escort_send_list) == escort_send_list_size)
    {
        escort_send_list_obj_t *obj;
        
        obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                        escort_send_list_check_priority_low,
                                        NULL,0);
        if (NULL != obj)
        {
            send_list_remove(&escort_send_list, obj);
        }
        else
        {
            obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                            escort_send_list_check_priority_high,
                                            NULL,0);
            DBG_ASSERT(obj != NULL __DBG_LINE);
            if (NULL != obj)
            {
                send_list_remove(&escort_send_list, obj);
            }
        }
    }
    
    return send_list_add(&escort_send_list, &send_obj);
}

void escort_send_list_fetch(escort_frame_t **frame,
                            uint16_t        *frame_id,
                            uint8_t         channal_type)
{
    escort_send_list_obj_t *obj;
    
    obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                    escort_send_list_check_waiting, NULL,channal_type);
    if (NULL != obj)
    {
//        if (ESCORT_SEND_GPRS_CHANNEL == channal_type)//gprs发送时把帧状态置成发送态，即忙的状态
//        {
//            obj->status = ESCORT_SEND_LIST_STATUS_SENDING;
//        }
//        else//ssn发送时把帧的状态置成等待状态
//        {
//            obj->status = ESCORT_SEND_LIST_STATUS_WAITING;
//        }

        obj->status = ESCORT_SEND_LIST_STATUS_WAITING;
        
        obj->allowed_send_times--;
        *frame = &obj->frame;
        *frame_id = obj->frame_id;
        
        return;
    }
    
    *frame = NULL;
    *frame_id = 0xFFFF;
    
    return;
}

void escort_send_list_sent(uint16_t frame_id, bool_t is_ok,uint8_t ssn_to_gprs_flag,uint8_t gprs_to_ssn_flag)
{
    escort_send_list_obj_t *obj;
    
    obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                    escort_send_list_check_frame_id, &frame_id,0);
    if (NULL != obj)
    {
//        DBG_ASSERT(obj->status == ESCORT_SEND_LIST_STATUS_SENDING __DBG_LINE);
        if (is_ok)
        {
            if (!obj->need_ack)
            {
                send_list_remove(&escort_send_list, obj);
            }
        }
        else
        {
            obj->status = ESCORT_SEND_LIST_STATUS_WAITING;
			if ((ssn_to_gprs_flag == 1) || (gprs_to_ssn_flag == 1))
			{
			  	return;
			}
			else if (obj->allowed_send_times == 0)
            {
                send_list_remove(&escort_send_list, obj);
            }
        }
    }
}

bool_t escort_send_list_is_ack_needed(uint16_t frame_id)
{
    escort_send_list_obj_t *obj;
    
    obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                    escort_send_list_check_frame_id, &frame_id,0);
    if (NULL != obj)
    {
        if (obj->need_ack)
        {
            return TRUE;
        }
    }
    
    return FALSE;
}

bool_t escort_send_list_ack_received(escort_ack_id_t *ack_id,
                                     uint16_t        *frame_id)
{
    escort_send_list_obj_t *obj;
    
    obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                    escort_send_list_check_ack, ack_id,0);
    if (NULL != obj)
    {
        DBG_ASSERT(obj->need_ack __DBG_LINE);
        *frame_id = obj->frame_id;
        send_list_remove(&escort_send_list, obj);
        
        return TRUE;
    }
    
    return FALSE;
}

void escort_send_list_no_ack(uint16_t frame_id)
{
    escort_send_list_obj_t *obj;
    
    obj = (escort_send_list_obj_t *)send_list_find_first(&escort_send_list, 
                                    escort_send_list_check_frame_id, &frame_id,0);
    if (NULL != obj)
    {
        DBG_ASSERT(obj->need_ack __DBG_LINE);
        obj->status = ESCORT_SEND_LIST_STATUS_WAITING;
        if (obj->allowed_send_times == 0)
        {
            send_list_remove(&escort_send_list, obj);
        }
    }
}

void escort_send_list_init(uint16_t size)
{
    escort_send_list_size = size;
    escort_frame_id = 0;
    send_list_init(&escort_send_list, sizeof(escort_send_list_obj_t), 
                   escort_send_list_priority_cmp);
}
