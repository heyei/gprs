/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : the alg of adxl345                                                                                          
**  File        : acc_sensor_algo.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是加速度传感器的算法处理                                 
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <osel_arch.h>
#include <hal_sensor.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <acc_sensor_algo.h>
#include <sensor_accelerated.h>
#include <math.h>
#include <acc_alg.h>
#include <acc_sensor_queue.h>
#include <gps.h>

#define ACT_MEDIAN_FILTER_BUFFER_MAX_LEN    3
#define ACT_COMBINE_ACC_BUFFER_MAX_LEN      10
#define ACT_COUNT_BUFFER_MAX_LEN            4

#define DROP_TMP_FILTER_BUFFER_MAX_LEN      3
#define TOPPLE_TMP_FILTER_BUFFER_MAX_LEN    3
#define TOPPLE_TMP_BAK_BUFFER_MAX_LEN       3

#define TURN_BUFFER_MAX_LEN                 50
#define TOPPLE_BAK_BUFFER_MAX_LEN           100//背景阈值
#define TOPPLE_FILTER_BUFFER_MAX_LEN        10

//测试用宏
#define ACC_SAMPLE_TIMER                    80 //MS
#define TEST_ACC_ALG_DEBUG                  0
#define PLAN_A_ALL_DISCARD                  0
#define PLAN_B_ONLY_ONE_DISCARD             1

#define TIME_DISA                           5000
#define KEEP_STATIC_MAX_TIME                40  //450是一个小时,算法8秒一个结果
#define ACT_TO_STATIC_MAX_TIME              15
#define ACC_ORI_OVERRUN                     1000

//定义拆卸状态
#define DISASSEMBLY_FIRST                   0
#define DISASSEMBLY_SECOND                  1

//坐标变换变量
#define ACC_PRE_DATA_MAX_NUM                50

//移动静止取中值buffer
#if TEST_ACC_ALG_DEBUG
static src_data_buffer_t act_buffer_1[ACT_MEDIAN_FILTER_BUFFER_MAX_LEN] = {{1,2,3},{1,2,3},{1,2,3}};
#else
static src_data_buffer_t act_buffer_1[ACT_MEDIAN_FILTER_BUFFER_MAX_LEN] = {0};
#endif
//移动静止取合加速度buffer
static combine_acc_buffer_t act_buffer_2[ACT_COMBINE_ACC_BUFFER_MAX_LEN] = {0};
//移动静止计数buffer
static count_buffer_t act_count_buffer[ACT_COUNT_BUFFER_MAX_LEN] = {0};

static int16_t turn_tmp_sum = 0;
//倾倒缓存buffer
static int32_t topple_tmp_filter_buffer[TOPPLE_TMP_FILTER_BUFFER_MAX_LEN] = {0};
static int16_t topple_tmp_bak_buffer[TOPPLE_TMP_BAK_BUFFER_MAX_LEN] = {0,0,0};
//倾倒缓存buffer
static src_data_buffer_t topple_bak_buffer[TOPPLE_BAK_BUFFER_MAX_LEN] = {0};
static src_data_buffer_t topple_filter_buffer[TOPPLE_FILTER_BUFFER_MAX_LEN] = {0};

//定义移动静止状态
static uint8_t act_status = 0;
static uint8_t act_history_status = 0;
//定义转向的状态
static uint8_t turn_status = 0;
static uint16_t turn_num = 0;
static uint16_t turn_send_num = 0;
#define TURN_NUM_MAX        100
//定义跌落的状态
static uint8_t drop_status = 0;
//定义跌落输出状态变量
static uint8_t drop_out_status = 0;
static uint16_t drop_num = 0;
static uint16_t drop_t3_t4_num = 0;
#define DROP_T3_T4_NUM_MAX            100
//定义倾倒的状态
static uint8_t topple_status = 0;
//定义倾倒输出状态变量
static uint8_t topple_out_status = 0;
static uint16_t topple_filter_num = 0;
static uint8_t topple_history_status = 0;//需要保存的历史状态
static uint16_t topple_num = 0;
#define TOPPLE_NUM_MAX       100
static uint16_t topple_num_max = 0;

#if DISASSEMBLY_FIRST
static uint8_t overrun_status = 0;
static uint8_t disassembly_status = 0;
#endif

#if DISASSEMBLY_SECOND
static uint8_t drop_disa_flag = 0;//跌落防拆标志
static uint8_t topple_disa_flag = 0;//倾倒防拆标志
static uint8_t suspect_overrun_disa_flag = 0;//超限疑似防拆标志
static uint8_t drop_out_history_status = DROP_NORMAL_STATUS;
static uint8_t topple_out_history_status = TOPPLE_NORMAL_STATUS;
static uint8_t disa_status = DISA_STATUS_T0;
static uint8_t disa_history_status = DISA_STATUS_T0;
static uint16_t disa_count = 0;
static uint16_t disa_total_send_status = 0;
#endif

//定义移动静止数据阈值
static uint16_t act_data_out_count_thresh = 0;
//定义转向数据阈值
static uint16_t turn_data_out_count_thresh = 0;
//定义跌落数据阈值
static uint16_t drop_data_out_count_thresh = 0;
//定义倾倒数据阈值
static uint16_t topple_bak_count_thresh = 0;
static uint16_t topple_filter_count_thresh = 0;
static uint16_t topple_c_term = 0;
static uint16_t topple_f_term = 0;
static uint8_t topple_bak_buf_len = 0;
static uint8_t topple_filter_buf_len = 0;

static uint8_t act_buf1_len= 0;
static uint8_t act_buf2_len = 0;
//2014-05-13:南通夜间报移动优化，暂时屏蔽
//static uint16_t ori_acc = 0;
//static uint32_t t_acc = 0;//合加速度累加值
static uint16_t comb_acc_value = 0;
static uint16_t acc_count = 0;//超限中计数器
static uint16_t acc_count_disa = 0;//超限防拆计数器

static acc_alg_activity_overrun_detect_param_t activity_overrun_param_local;
static acc_alg_topple_detect_param_t  topple_param_local;
static acc_alg_turn_detect_param_t    turn_param_local;
static acc_alg_drop_detect_param_t    drop_param_local;
static acc_alg_config_t config_alg_local;

static uint8_t data_num_1 = 0;
static uint8_t data_num_2 = 0;
static uint8_t data_num_flag = 0;
static fp32_t pre_x = 0;
static fp32_t pre_y = 0;
static fp64_t pre_z = 0;
static uint16_t topple_f_data_len = 0;

acc_circular_queue_t buffer_queue;
static uint16_t act_to_static_t = 0;
static uint16_t keep_static_t = 0;
static uint16_t act_to_static_flag = 0;
static uint8_t in_move_flag = 0;//进入移动标志位，当进入移动置成1；当恢复静止后置成0，
// 设置算法模块参数
void acc_alg_set_param(const acc_alg_param_t *param)
{
    DBG_ASSERT(NULL != param __DBG_LINE);
    switch (param->type)
    {
        case ACC_ALG_ACTIVITY_OVERRUN_DETECT_PARAM:
            activity_overrun_param_local.combine_acc_buffer_len = 
                            param->activity_overrun_param.combine_acc_buffer_len;
            activity_overrun_param_local.data_out_time_thresh = 
                            param->activity_overrun_param.data_out_time_thresh;
            activity_overrun_param_local.median_filter_buffer_len = 
                            param->activity_overrun_param.median_filter_buffer_len;
            activity_overrun_param_local.sample_detect_time = 
                            param->activity_overrun_param.sample_detect_time;
            activity_overrun_param_local.variance_thresh_1 = 
                            param->activity_overrun_param.variance_thresh_1;
            activity_overrun_param_local.variance_thresh_2 = 
                            param->activity_overrun_param.variance_thresh_2;
            activity_overrun_param_local.variance_thresh_3 = 
                            param->activity_overrun_param.variance_thresh_3;
            activity_overrun_param_local.variance_thresh_4 = 
                            param->activity_overrun_param.variance_thresh_4;
            activity_overrun_param_local.overrun_percent = 
                            param->activity_overrun_param.overrun_percent;
            activity_overrun_param_local.overrun_lim = 
                            param->activity_overrun_param.overrun_lim;
            activity_overrun_param_local.disa_percent = 
                            param->activity_overrun_param.disa_percent;
            activity_overrun_param_local.disa_lim = 
                            param->activity_overrun_param.disa_lim;
            act_data_out_count_thresh = 
                            activity_overrun_param_local.data_out_time_thresh
                            /activity_overrun_param_local.sample_detect_time;            
            break;
        case ACC_ALG_TOPPLE_DETECT_PARAM:
            topple_param_local.sample_detect_time = param->topple_param.sample_detect_time;
            topple_param_local.topple_bak_thresh = param->topple_param.topple_bak_thresh;
            topple_param_local.topple_bak_time = param->topple_param.topple_bak_time;
            topple_param_local.topple_cmp_thresh = param->topple_param.topple_cmp_thresh;
            topple_param_local.topple_if_dynamic_bak = param->topple_param.topple_if_dynamic_bak;
            topple_param_local.topple_mod_thresh = param->topple_param.topple_mod_thresh;
            topple_param_local.topple_time_delay = param->topple_param.topple_time_delay;
            topple_param_local.topple_time_filter = param->topple_param.topple_time_filter;
            topple_param_local.topple_disa_cmp_thresh = param->topple_param.topple_disa_cmp_thresh;
            topple_param_local.topple_disa_mod_thresh = param->topple_param.topple_disa_mod_thresh;
            topple_bak_count_thresh = topple_param_local.topple_bak_time
                                                            /topple_param_local.sample_detect_time;
            topple_filter_count_thresh = topple_param_local.topple_time_filter
                                                            /topple_param_local.sample_detect_time;
            break;
        case ACC_ALG_TURN_DETECT_PARAM:
            turn_param_local.sample_detect_time = param->turn_param.sample_detect_time;
            turn_param_local.turn_acc_thresh = param->turn_param.turn_acc_thresh;
            turn_param_local.turn_angle_u = param->turn_param.turn_angle_u;
            turn_param_local.turn_angle_v = param->turn_param.turn_angle_v;
            turn_param_local.turn_angle_w = param->turn_param.turn_angle_w;
            turn_param_local.turn_time_delay = param->turn_param.turn_time_delay;
            turn_param_local.turn_time_thresh = param->turn_param.turn_time_thresh;
            turn_data_out_count_thresh = turn_param_local.turn_time_thresh
                                                                /turn_param_local.sample_detect_time;
            break;
        case ACC_ALG_FALL_DETECT_PARAM:
            drop_param_local.sample_detect_time = param->drop_param.sample_detect_time;
            drop_param_local.drop_acc_thresh = param->drop_param.drop_acc_thresh;
            drop_param_local.drop_time_delay_1 = param->drop_param.drop_time_delay_1;
            drop_param_local.drop_time_delay_2 = param->drop_param.drop_time_delay_2;
            drop_param_local.drop_time_filter = param->drop_param.drop_time_filter;
            drop_data_out_count_thresh = drop_param_local.drop_time_filter
                                                                /drop_param_local.sample_detect_time;
            break;   
    }       
}

// 配置算法回调函数
void acc_alg_config(const acc_alg_config_t *config)
{
    DBG_ASSERT(NULL != config __DBG_LINE);
    config_alg_local.data_rate = config->data_rate;
    config_alg_local.acc_alg_cb = config->acc_alg_cb; 
}

// 初始化算法模块
void acc_alg_init(void)
{
    act_status = ACT_INVALID_STATUS;
    act_history_status = ACT_STATIC_STATUS;
    #if DISASSEMBLY_FIRST
    overrun_status = ACT_INVALID_STATUS;
    disassembly_status = DISASSEMBLY_NO_STATUS;
    #endif
    turn_status = TURN_STATUS_T0;
    drop_status = DROP_STATUS_T0;
    topple_status = TOPPLE_STATUS_T0; 
    #if DISASSEMBLY_SECOND   
    disa_status = DISA_STATUS_T0;
    #endif

    activity_overrun_param_local.combine_acc_buffer_len = 10;
    activity_overrun_param_local.data_out_time_thresh = 8000;
    activity_overrun_param_local.median_filter_buffer_len = 3;
    //优化低功耗，采集频率改成12.5hz
    //activity_overrun_param_local.sample_detect_time = 10;
    activity_overrun_param_local.sample_detect_time = ACC_SAMPLE_TIMER;
    activity_overrun_param_local.variance_thresh_1 = 8;
    activity_overrun_param_local.variance_thresh_2 = 85;
    activity_overrun_param_local.variance_thresh_3 = 500;
    activity_overrun_param_local.variance_thresh_4 = 10000;
    //优化低功耗，采集频率改成12.5hz，超限上限数由12改成20
    activity_overrun_param_local.overrun_percent = 20;
    //优化低功耗，采集频率改成12.5hz，超限上限数由30改成5
    activity_overrun_param_local.overrun_lim = 5;
    activity_overrun_param_local.disa_percent = 25;//25
    //优化低功耗，采集频率改成12.5hz，超限上限数由3改成1
    activity_overrun_param_local.disa_lim = 1;// 3
    act_data_out_count_thresh = 100;            

    //优化低功耗，采集频率改成12.5hz
    //topple_param_local.sample_detect_time = 10;
    topple_param_local.sample_detect_time = ACC_SAMPLE_TIMER;
    topple_param_local.topple_bak_thresh = 50;
    //优化低功耗，采集频率改成12.5hz
    //topple_param_local.topple_bak_time = 1000;//默认500 背景阈值
    topple_param_local.topple_bak_time = 8000;//默认500 背景阈值
    topple_param_local.topple_cmp_thresh = 800;
    topple_param_local.topple_if_dynamic_bak = 1;//背景更新标志
    topple_param_local.topple_mod_thresh = 100;
    topple_param_local.topple_time_delay = 200;
    //优化低功耗，采集频率改成12.5hz
    //topple_param_local.topple_time_filter = 100;
    topple_param_local.topple_time_filter = 800;
    topple_param_local.topple_disa_cmp_thresh = 700;
    topple_param_local.topple_disa_mod_thresh = 60;
    topple_bak_count_thresh = 100;//背景阈值
    topple_filter_count_thresh = 10;
    
    turn_param_local.sample_detect_time = ACC_SAMPLE_TIMER;
    turn_param_local.turn_acc_thresh = 80;
    turn_param_local.turn_angle_u = 0;
    turn_param_local.turn_angle_v = 1;
    turn_param_local.turn_angle_w = 0;
    turn_param_local.turn_time_delay = 200;
    //优化低功耗，采集频率改成12.5hz
   // turn_param_local.turn_time_thresh = 500;
    turn_param_local.turn_time_thresh = 4000;
    turn_data_out_count_thresh = 50;

    drop_param_local.sample_detect_time = ACC_SAMPLE_TIMER;
    drop_param_local.drop_acc_thresh = 300;//300
    drop_param_local.drop_time_delay_1 = 100;//140
    drop_param_local.drop_time_delay_2 = 20;// 20
    //优化低功耗，采集频率改成12.5hz
    //drop_param_local.drop_time_filter = 100;
    drop_param_local.drop_time_filter = 800;
    drop_data_out_count_thresh = 10;

    //创建队列
    acc_queue_create(&buffer_queue);
}

//中值处理3个数中取中值
int16_t acc_sensor_algo_median_proc(int16_t a,int16_t b,int16_t c)
{
    return a > b ? (b > c ? b : (a > c ? c : a)) : (a > c ? a : (b > c ? c : b));
}


//合加速度计算
void acc_sensor_algo_combine_proc(void)
{
    int32_t median_x = 0;
    int32_t median_y = 0;
    int32_t median_z = 0;
        
    median_x = acc_sensor_algo_median_proc(act_buffer_1[0].src_x,act_buffer_1[1].src_x,act_buffer_1[2].src_x);
    median_y = acc_sensor_algo_median_proc(act_buffer_1[0].src_y,act_buffer_1[1].src_y,act_buffer_1[2].src_y);
    median_z = acc_sensor_algo_median_proc(act_buffer_1[0].src_z,act_buffer_1[1].src_z,act_buffer_1[2].src_z);

    comb_acc_value = (int16_t)sqrt((median_x * median_x) + (median_y * median_y) +  (median_z * median_z));

    return ;    
}

//计算方差函数
variance_t acc_sensor_algo_variance_calc_proc(const combine_acc_buffer_t *pacc_buff)
{
    uint8_t i = 0;
    fp64_t v_combine_acc_1 = 0;
    fp64_t v_combine_acc_2 = 0;
    fp64_t v_x1 = 0;
    fp64_t v_x2 = 0;
    variance_t acc_v = {0};  

    uint16_t buffer_2_len = 0;
#if TEST_ACC_ALG_DEBUG
    buffer_2_len = 10;
#else
    buffer_2_len = activity_overrun_param_local.combine_acc_buffer_len;
#endif    

    DBG_ASSERT(NULL != pacc_buff __DBG_LINE);

    for (i = 0;i < buffer_2_len;i++,pacc_buff++)
    {
        fp64_t  v_combine_temp = 0;
        fp64_t  v_x_temp = 0;
        v_combine_temp = pacc_buff->combine_acc;
        v_x_temp = pacc_buff->x;
        v_combine_acc_1 += (v_combine_temp * v_combine_temp);
        v_combine_acc_2 += v_combine_temp;
        v_x1 += v_x_temp * v_x_temp;
        v_x2 += v_x_temp;        
    }

    acc_v.v_combine_acc = (fp32_t)(10*v_combine_acc_1 - v_combine_acc_2 * v_combine_acc_2)/90;
    acc_v.v_x = (fp32_t)(10*v_x1 - v_x2 * v_x2)/90;
    return acc_v;    
}

 //方差阈值判断处理，得出车辆状态
 //熄火、点火静止、正常行驶
bool_t acc_sensor_algo_variance_judge_proc(const variance_t *pacc_v)
{
    fp32_t v_combine_acc = 0;
    fp32_t v_x = 0;
    uint16_t sum_v_1 = 0;
    uint16_t sum_v_2 = 0;
    acc_alg_event_t act_alg_cfm;

    DBG_ASSERT(NULL != pacc_v __DBG_LINE);

    v_combine_acc = pacc_v->v_combine_acc;
    v_x = pacc_v->v_x;

    if (v_x < activity_overrun_param_local.variance_thresh_1)
    {
        act_count_buffer[0].count1 += 1;//C(1,1)=C(1,1)+1
    }
    else
    {
        act_count_buffer[1].count1 += 1;//C(1,2)=C(1,2)+1
    }

    if (v_combine_acc < activity_overrun_param_local.variance_thresh_2)
    {
        act_count_buffer[0].count2 += 1;//C(2,1)=C(2,1)+1
    }
    else if ((v_combine_acc >= activity_overrun_param_local.variance_thresh_2)
                &&(v_combine_acc < activity_overrun_param_local.variance_thresh_3))
        {
            act_count_buffer[1].count2 += 1;//C(2,2)=C(2,2)+1
        }
    else if ((v_combine_acc >= activity_overrun_param_local.variance_thresh_3)
                &&(v_combine_acc < activity_overrun_param_local.variance_thresh_4))
        {
            act_count_buffer[2].count2 += 1;//C(2,3)=C(2,3)+1
        }
    else if (v_combine_acc >= activity_overrun_param_local.variance_thresh_4)
        {
            act_count_buffer[3].count2 += 1;//C(2,4)=C(2,4)+1
        }    

    sum_v_1 = act_count_buffer[0].count2\
                    + act_count_buffer[1].count2\
                    + act_count_buffer[2].count2\
                    + act_count_buffer[3].count2;
    
    sum_v_2 = act_count_buffer[1].count2\
                + act_count_buffer[2].count2\
                + act_count_buffer[3].count2;
/*
//2014-05-13:南通夜间报移动优化，暂时屏蔽
    if (0 == ori_acc)
    {
        t_acc += comb_acc_value;
    }
*/ 
    static uint32_t static_to_act_count = 0;
    if (sum_v_1 >= act_data_out_count_thresh)
    {
         //2014-05-13:南通夜间报移动优化，暂时把0.1改成0.2
        //if (sum_v_2 > (uint16_t)(act_data_out_count_thresh * 0.1))//移动状态
        if (sum_v_2 > (uint16_t)(act_data_out_count_thresh * 0.2))//移动状态
        {            
            //如果之前状态是静止并且还是等红绿灯状态下的相对静止(实际我们认为车还是在运动)，
            //现在突然变移动 ,是不会上报静到动帧            
            if (act_to_static_flag == 0)//如果现在不处于恢复静止状态下
            {
                static_to_act_count++;
            }

            if ((ACT_STATIC_STATUS == act_history_status)
                &&(act_to_static_flag == 0)
                &&(NULL != config_alg_local.acc_alg_cb)
                &&(static_to_act_count > 3))
            {//如果从静止到运动则报状态变化消息
                act_alg_cfm.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
                act_alg_cfm.activity_args.activity_status_type = ACC_ALG_STATIC_TO_ACT_STATUS;
                act_alg_cfm.activity_args.feature_dimen = 0x04;                    
                act_alg_cfm.activity_args.static_to_act_feature.c1 = act_count_buffer[0].count2;
                act_alg_cfm.activity_args.static_to_act_feature.c2 = act_count_buffer[1].count2;
                act_alg_cfm.activity_args.static_to_act_feature.c3 = act_count_buffer[2].count2;
                act_alg_cfm.activity_args.static_to_act_feature.c4 = act_count_buffer[3].count2;
                config_alg_local.acc_alg_cb(&act_alg_cfm);  

                act_history_status = ACT_NORMAL_DRIVE_STATUS;        
                act_status = ACT_NORMAL_DRIVE_STATUS;
                in_move_flag = 1;
            }

            keep_static_t = 0;
            act_to_static_t = 0;
            
            if ((in_move_flag == 1)//只要不恢复静止，都会发出动保持
                &&(NULL != config_alg_local.acc_alg_cb))                 
            {
                act_alg_cfm.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
                act_alg_cfm.activity_args.activity_status_type = ACC_ALG_ACT_RETAIN_STATUS;
                act_alg_cfm.activity_args.feature_dimen = 0x04;
                act_alg_cfm.activity_args.act_retain_feature.c1 = act_count_buffer[0].count2;
                act_alg_cfm.activity_args.act_retain_feature.c2 = act_count_buffer[1].count2;
                act_alg_cfm.activity_args.act_retain_feature.c3 = act_count_buffer[2].count2;
                act_alg_cfm.activity_args.act_retain_feature.c4 = act_count_buffer[3].count2;
                if (NULL != config_alg_local.acc_alg_cb)
                {
                    config_alg_local.acc_alg_cb(&act_alg_cfm);
                }                 
            }               
/*
//2014-05-13:南通夜间报移动优化，暂时屏蔽
            if (0 == ori_acc)
            {
                t_acc = 0;
            }
*/            
            osel_memset(act_count_buffer,0x00,(sizeof(count_buffer_t)*ACT_COUNT_BUFFER_MAX_LEN));            
        }
        else//静止状态
        {   
            static_to_act_count = 0;
            if (ACT_NORMAL_DRIVE_STATUS == act_history_status)
            {//如果从运动到熄火则报状态变化消息                
                act_to_static_flag = 1;
            }
            
            if (act_to_static_flag)
            {
                act_to_static_t ++;
                if ((act_to_static_t >= ACT_TO_STATIC_MAX_TIME)// 2分钟=15
                    &&(NULL != config_alg_local.acc_alg_cb))
                {
                    act_to_static_t = 0;
                    act_to_static_flag = 0;
                    act_alg_cfm.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
                    act_alg_cfm.activity_args.activity_status_type = ACC_ALG_ACT_TO_STATIC_STATUS;
                    act_alg_cfm.activity_args.feature_dimen = 0x04;
                    //动到静
                    act_alg_cfm.activity_args.act_to_static_feature.feature_flag = act_count_buffer[0].count2;
                    act_alg_cfm.activity_args.act_to_static_feature.feature_1 = act_count_buffer[1].count2;
                    act_alg_cfm.activity_args.act_to_static_feature.feature_2 = act_count_buffer[2].count2;
                    act_alg_cfm.activity_args.act_to_static_feature.feature_3 = act_count_buffer[3].count2;
                    config_alg_local.acc_alg_cb(&act_alg_cfm);
                    in_move_flag = 0;
                    
                }
            }
#if GPS_DEBUG_INFO == 1
	extern uint8_t acc_status_flag;
    acc_status_flag = 200;
#endif             
            keep_static_t++;
            act_history_status = ACT_STATIC_STATUS;         
            act_status = ACT_STATIC_STATUS;

            if ((keep_static_t >= KEEP_STATIC_MAX_TIME)
                &&(NULL != config_alg_local.acc_alg_cb))
            {// 5.3分钟调用一次静保持，主要为防止GPS未关闭
                keep_static_t = 0;
                act_alg_cfm.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
                act_alg_cfm.activity_args.activity_status_type = ACC_ALG_STATIC_RETAIN_STATUS;
                act_alg_cfm.activity_args.feature_dimen = 0x04;
                //静态保持
                act_alg_cfm.activity_args.static_retain_feature.feature_flag = act_count_buffer[0].count2;
                act_alg_cfm.activity_args.static_retain_feature.feature_1 = act_count_buffer[1].count2;
                act_alg_cfm.activity_args.static_retain_feature.feature_2 = act_count_buffer[2].count2;
                act_alg_cfm.activity_args.static_retain_feature.feature_3 = act_count_buffer[3].count2;
                config_alg_local.acc_alg_cb(&act_alg_cfm);  
            }
/*  
//2014-05-13:南通夜间报移动优化，暂时屏蔽
            if (0 == ori_acc)//ori_acc==0
            {
                ori_acc = (uint16_t)(t_acc/act_data_out_count_thresh);
            }  
*/            
            osel_memset(act_count_buffer,0x00,(sizeof(count_buffer_t)*ACT_COUNT_BUFFER_MAX_LEN));           
        }
    }

    return TRUE;    
}

//矩阵处理函数
void matrix_proc(const acc_circular_queue_t* queue,
                                const int16_t *parray_in,
                                int16_t *parray_out)
{
    DBG_ASSERT(NULL != queue __DBG_LINE);
    DBG_ASSERT(NULL != parray_in __DBG_LINE);

    int16_t  u = 0;
    int16_t  v = 0;
    int16_t  w = 0;
    uint8_t i = 0;
    uint8_t j = 0;
    j = queue->personal_turn_front;
    int16_t *p = NULL;
    p = parray_out;

    u = *parray_in;
    v = *(parray_in + 1);
    w = *(parray_in + 2);

    for (i = 0;i < turn_data_out_count_thresh;i++,j++)
    {
        if (j >= ACC_QUEUE_MAXLEN)
        {
            *p = queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_x * u 
                    + queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_y * v 
                    + queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_z * w;             
        }
        else
        {
            *p = queue->item[j].src_x * u 
                    + queue->item[j].src_y * v 
                    + queue->item[j].src_z * w;             
        }
        p ++;
    }     
}

//数组求和函数
static int32_t array_sum_proc(const int16_t *parray,uint16_t num)
{
    uint16_t i = 0;
    int32_t sum = 0;
    DBG_ASSERT(NULL != parray __DBG_LINE);

    for (i = 0;i < num;i++)
    {
        sum += *parray;
        parray ++;
    }
    return sum;    
}


//数组求最大值函数
static int16_t array_max_proc(int16_t *parray,uint16_t num)
{
    int16_t max = 0;
    int16_t *p = NULL;
    DBG_ASSERT(NULL != parray __DBG_LINE);  

    for (max = *parray,p = parray;p < (parray + num);p++)
    {
        if (*p >= max)
        {
            max = *p;
        }
    }

    return max;    
}

//数组求最小值函数
int16_t array_min_proc(int16_t *parray,uint16_t num)
{
    int16_t min = 0;
    int16_t *p = NULL;
    DBG_ASSERT(NULL != parray __DBG_LINE);  

    for (min = *parray,p = parray;p < (parray + num);p++)
    {
        if (*p <= min)
        {
            min = *p;
        }
    }

    return min;   
}

//转向状态处理
void turn_status_proc(void)
{
   int16_t turn_acc_thresh = 0;
   acc_alg_event_t alg_event_cfm;
   uint16_t turn_num_delay = 0;

   turn_acc_thresh = (int16_t)turn_param_local.turn_acc_thresh;
   //优化低功耗，采集频率改成12.5hz
   turn_num_delay = (int16_t)(turn_param_local.turn_time_delay/turn_param_local.sample_detect_time) + 1;

   alg_event_cfm.type = ACC_ALG_TURN_EVENT_TYPE;
   alg_event_cfm.turn_args.feature_1 = turn_tmp_sum;
   alg_event_cfm.turn_args.feature_dimen = 0x01;
   
    switch(turn_status)
    {
            case TURN_STATUS_T0:
                turn_status = TURN_STATUS_T1;
            break;
            
            case TURN_STATUS_T1:
                if (turn_tmp_sum >= turn_acc_thresh)//C1
                {
                    turn_status = TURN_STATUS_T2;
                    //判断当前是否为运动状态
                    if (act_status == ACT_NORMAL_DRIVE_STATUS)
                    {
                        //config_alg_local.acc_alg_cb(&alg_event_cfm);
                    }
                    turn_send_num = 0;
                }
                else if (turn_tmp_sum <= -turn_acc_thresh)//C2
                    {
                        turn_status = TURN_STATUS_T3;
                        if (act_status == ACT_NORMAL_DRIVE_STATUS)
                        {
                            //config_alg_local.acc_alg_cb(&alg_event_cfm);
                        } 
                        turn_send_num = 0;
                    }
            break;

            case TURN_STATUS_T2:
                if (-turn_acc_thresh < turn_tmp_sum < turn_acc_thresh)//C0
                {
                    turn_status = TURN_STATUS_T4;
                }
                else if (turn_tmp_sum <= -turn_acc_thresh)//C2
                    {
                        turn_status = TURN_STATUS_T3;
                        if (act_status == ACT_NORMAL_DRIVE_STATUS)
                        {
                            //config_alg_local.acc_alg_cb(&alg_event_cfm);
                        }
                        turn_send_num = 0;
                    }
                else if (turn_tmp_sum >= turn_acc_thresh)//C1
                    {
                        turn_send_num++;
                        if (turn_send_num >= TURN_NUM_MAX)
                        {
                            if ((act_status == ACT_NORMAL_DRIVE_STATUS)
                                &&(NULL != config_alg_local.acc_alg_cb))
                            {
                                config_alg_local.acc_alg_cb(&alg_event_cfm);
                            }
                            turn_send_num = 0;
                        }
                    }
            break;

            case TURN_STATUS_T3:
                if (-turn_acc_thresh < turn_tmp_sum < turn_acc_thresh)//C0
                {
                    turn_status = TURN_STATUS_T4;
                }
                else if (turn_tmp_sum >= turn_acc_thresh)//C1
                    {
                        turn_status = TURN_STATUS_T2;
                        if (act_status == ACT_NORMAL_DRIVE_STATUS)
                        {
                            //config_alg_local.acc_alg_cb(&alg_event_cfm);
                        }
                        turn_send_num = 0;
                    } 
                else if (turn_tmp_sum <= -turn_acc_thresh)//C2
                    {
                        turn_send_num++;
                        if (turn_send_num >= TURN_NUM_MAX)
                        {
                            if ((act_status == ACT_NORMAL_DRIVE_STATUS)
                                &&(NULL != config_alg_local.acc_alg_cb))
                            {
                                config_alg_local.acc_alg_cb(&alg_event_cfm);
                            }
                            turn_send_num = 0;
                        }                        
                    }
            break;
                
            case TURN_STATUS_T4:
                if (turn_tmp_sum >= turn_acc_thresh)//C1
                {
                    turn_status = TURN_STATUS_T2;
                    turn_num = 0;
                }
                else if (turn_tmp_sum <= -turn_acc_thresh)//C2
                    {
                        turn_status = TURN_STATUS_T3;
                        turn_num = 0;
                    }
                else if (turn_num >= turn_num_delay)//D1
                    {
                        turn_status = TURN_STATUS_T1;
                        turn_num = 0;
                    }
                else if (turn_num < turn_num_delay)//D0
                    {
                        turn_num++;
                        turn_send_num++;
                        if (turn_send_num >= TURN_NUM_MAX)
                        {
                            if ((act_status == ACT_NORMAL_DRIVE_STATUS)
                                &&(NULL != config_alg_local.acc_alg_cb))
                            {
                                config_alg_local.acc_alg_cb(&alg_event_cfm);
                            }
                            turn_send_num = 0;
                        }                           
                    }                   
            break;                    
    }        
}

//跌落状态处理
void drop_status_proc(const int16_t *pbuffer)
{
   uint16_t drop_acc_thresh = 0;
   acc_alg_event_t alg_event_cfm;
   uint16_t drop_num_delay_1 = 0;
   uint16_t drop_num_delay_2 = 0;

   int16_t drop_x = 0;
   int16_t drop_y = 0;
   int16_t drop_z = 0;
   
   DBG_ASSERT(NULL != pbuffer __DBG_LINE);    
   drop_x = *pbuffer;
   drop_y = *(pbuffer + 1);
   drop_z = *(pbuffer + 2);

   drop_acc_thresh = drop_param_local.drop_acc_thresh;
   //优化低功耗，采集频率改成12.5hz
   drop_num_delay_1 = (uint16_t)(drop_param_local.drop_time_delay_1/drop_param_local.sample_detect_time) + 1;
   drop_num_delay_2 = (uint16_t)(drop_param_local.drop_time_delay_2/drop_param_local.sample_detect_time) + 1;

    alg_event_cfm.type = ACC_ALG_FALL_EVENT_TYPE;
    alg_event_cfm.fall_args.acc_x = drop_x;
    alg_event_cfm.fall_args.acc_y = drop_y;
    alg_event_cfm.fall_args.acc_z = drop_z;

    switch (drop_status)
    {
        case DROP_STATUS_T0:
            drop_out_status = DROP_NORMAL_STATUS;
            drop_status = DROP_STATUS_T1;
        break;
            
        case DROP_STATUS_T1:
            if ((ABS(drop_x) < drop_acc_thresh)
                &(ABS(drop_y) < drop_acc_thresh)
                &(ABS(drop_z) < drop_acc_thresh))//C1条件
            {
                drop_status = DROP_STATUS_T2;
                drop_out_status = DROP_NORMAL_STATUS;
                drop_num = 0;//t1--t2 num=0                       
            }
        break;
            
        case DROP_STATUS_T2:
            if ((ABS(drop_x) >= drop_acc_thresh)
                |(ABS(drop_y) >= drop_acc_thresh)
                |(ABS(drop_z) >= drop_acc_thresh))//C0条件   
            {
                drop_status = DROP_STATUS_T1;
                drop_out_status = DROP_NORMAL_STATUS;
            }
            else if ((drop_num >= drop_num_delay_1)
                    &&(NULL != config_alg_local.acc_alg_cb))//D1
                    {
                        drop_status = DROP_STATUS_T3; 
                        drop_out_status = DROP_DROP_STATUS;
                        config_alg_local.acc_alg_cb(&alg_event_cfm);
                        drop_t3_t4_num = 0;
                    }
            else if (drop_num < drop_num_delay_1)//D0
                    {
                        drop_num++;
                    }
        break;
            
        case DROP_STATUS_T3:
            if ((ABS(drop_x) >= drop_acc_thresh)
                |(ABS(drop_y) >= drop_acc_thresh)
                |(ABS(drop_z) >= drop_acc_thresh))//C0条件   
            {
                drop_status = DROP_STATUS_T4; 
                drop_out_status = DROP_DROP_STATUS;
                drop_num = 0;//t3--t4 num=0                     
            }
            else if ((ABS(drop_x) < drop_acc_thresh)
                &(ABS(drop_y) < drop_acc_thresh)
                &(ABS(drop_z) < drop_acc_thresh))//C1
                {
                    drop_t3_t4_num++;
                    if ((drop_t3_t4_num >= DROP_T3_T4_NUM_MAX)
                        &&(NULL != config_alg_local.acc_alg_cb))
                    {
                        config_alg_local.acc_alg_cb(&alg_event_cfm); 
                        drop_t3_t4_num = 0;
                    }
                }
        break;     

        case DROP_STATUS_T4:
                if ((ABS(drop_x) < drop_acc_thresh)
                    &(ABS(drop_y) < drop_acc_thresh)
                    &(ABS(drop_z) < drop_acc_thresh))//C1条件
                {
                    drop_status = DROP_STATUS_T3; 
                    drop_out_status = DROP_DROP_STATUS;
                }
                else if (drop_num >= drop_num_delay_2)//E1
                    {
                        drop_status = DROP_STATUS_T1;
                        drop_out_status = DROP_NORMAL_STATUS;
                    }
                else if (drop_num < drop_num_delay_2)//E0
                    {
                        drop_num++;
                        drop_t3_t4_num++;
                        if ((drop_t3_t4_num >= DROP_T3_T4_NUM_MAX)
                            &&(NULL != config_alg_local.acc_alg_cb))
                        {
                            config_alg_local.acc_alg_cb(&alg_event_cfm); 
                            drop_t3_t4_num = 0;
                        }                        
                    }                
        break;                 
    }    
}


//倾倒状态处理
void topple_status_proc(void)
{
   acc_alg_event_t alg_event_cfm;
   uint16_t topple_delay_num = 0;
   
   topple_c_term 
    = (uint16_t)sqrt((topple_tmp_filter_buffer[0] - topple_tmp_bak_buffer[0])
            *(topple_tmp_filter_buffer[0] - topple_tmp_bak_buffer[0])
         + ((topple_tmp_filter_buffer[1] - topple_tmp_bak_buffer[1])
            *(topple_tmp_filter_buffer[1] - topple_tmp_bak_buffer[1]))
         + ((topple_tmp_filter_buffer[2] - topple_tmp_bak_buffer[2])
            *(topple_tmp_filter_buffer[2] - topple_tmp_bak_buffer[2])));

    topple_f_term = (uint16_t)ABS(sqrt(topple_tmp_filter_buffer[0]*topple_tmp_filter_buffer[0]
                   +topple_tmp_filter_buffer[1]*topple_tmp_filter_buffer[1]
                   +topple_tmp_filter_buffer[2]*topple_tmp_filter_buffer[2]) - topple_f_data_len);
    //优化低功耗，采集频率改成12.5hz
    topple_delay_num = (uint16_t)(topple_param_local.topple_time_delay
                                        /topple_param_local.sample_detect_time) + 1;

    alg_event_cfm.type = ACC_ALG_TOPPLE_EVENT_TYPE;
    alg_event_cfm.topple_args.feature_dimen = 0x02;
    alg_event_cfm.topple_args.feature_1 = topple_c_term;
    alg_event_cfm.topple_args.feature_2 = topple_f_term;

    switch (topple_status)
    {
        case TOPPLE_STATUS_T0:
            topple_status = TOPPLE_STATUS_T1;
            topple_out_status = TOPPLE_NORMAL_STATUS;
        break;

        case TOPPLE_STATUS_T1:
            if (topple_f_term >= topple_param_local.topple_mod_thresh)//F1
            {
                topple_status = TOPPLE_STATUS_T4;
                topple_history_status = TOPPLE_STATUS_T1;//记录历史状态
                topple_out_status = TOPPLE_NORMAL_STATUS;
            }
            else if ((topple_c_term >= topple_param_local.topple_cmp_thresh)
                         &&(NULL != config_alg_local.acc_alg_cb))//C1
                    {
                        topple_status = TOPPLE_STATUS_T2;
                        //由于倾倒报警较多，所以此处需要采用间隔1s
                        //,2s,3s,4s,5s,来发送，此处初始化计数器
                        topple_num_max = TOPPLE_NUM_MAX;
                        topple_num = 0;
                        topple_out_status = TOPPLE_FLIP_STATUS;
                        config_alg_local.acc_alg_cb(&alg_event_cfm);                            
                    }
        break;

        case TOPPLE_STATUS_T2:
             if (topple_f_term >= topple_param_local.topple_mod_thresh)//F1
            {
                topple_status = TOPPLE_STATUS_T4;
                topple_history_status = TOPPLE_STATUS_T2;//记录历史状态
                topple_out_status = TOPPLE_FLIP_STATUS;
            }
             else if (topple_c_term < topple_param_local.topple_cmp_thresh)//C0
                    {
                        topple_status = TOPPLE_STATUS_T3;
                        topple_filter_num = 0;//T2-->T3  num=0
                        topple_out_status = TOPPLE_FLIP_STATUS;                    
                    }
             else if (topple_c_term >= topple_param_local.topple_cmp_thresh)//C1
                {
                    topple_num++;                      
                    if ((topple_num >= topple_num_max)
                        &&(NULL != config_alg_local.acc_alg_cb))
                    {
                        config_alg_local.acc_alg_cb(&alg_event_cfm);
                        topple_num = 0;
                        if (topple_num_max < TOPPLE_NUM_MAX * 5)
                        {
                            topple_num_max += TOPPLE_NUM_MAX;
                        }
                    }
                }
        break;

        case TOPPLE_STATUS_T3:
            if (topple_f_term >= topple_param_local.topple_mod_thresh)//F1
            {
                topple_status = TOPPLE_STATUS_T4;
                topple_history_status = TOPPLE_STATUS_T3;//记录历史状态
                topple_out_status = TOPPLE_FLIP_STATUS;
            }
            else if(topple_c_term >= topple_param_local.topple_cmp_thresh)//C1
                    {
                        topple_status = TOPPLE_STATUS_T2;
                        topple_out_status = TOPPLE_FLIP_STATUS;                        
                    }
            else if (topple_filter_num < topple_delay_num)//D0
                    {
                        topple_filter_num++;
                        topple_num++;                        
                        if ((topple_num >= topple_num_max)
                            &&(NULL != config_alg_local.acc_alg_cb))
                        {
                            config_alg_local.acc_alg_cb(&alg_event_cfm);
                            topple_num = 0;
                            if (topple_num_max < TOPPLE_NUM_MAX * 5)
                            {
                                topple_num_max += TOPPLE_NUM_MAX;
                            }
                        }                       
                    }
            else if (topple_filter_num >= topple_delay_num)//D1
                    {
                        topple_status = TOPPLE_STATUS_T1;
                        topple_out_status = TOPPLE_NORMAL_STATUS;                               
                    }
        break;

        case TOPPLE_STATUS_T4:
             if (topple_f_term < topple_param_local.topple_mod_thresh)//F0
            {
                topple_status = topple_history_status;//回到历史状态
                if (topple_history_status == TOPPLE_STATUS_T1)
                {
                    topple_out_status = TOPPLE_NORMAL_STATUS;
                }
                else if ((topple_history_status == TOPPLE_STATUS_T2)
                            ||(topple_history_status == TOPPLE_STATUS_T3))
                    {
                        topple_out_status = TOPPLE_FLIP_STATUS;
                    }                
            }
            else if (topple_f_term >= topple_param_local.topple_mod_thresh)//F1
            {
                if (topple_history_status == TOPPLE_STATUS_T3)
                {
                    topple_num++;
                    if ((topple_num >= topple_num_max)
                        &&(NULL != config_alg_local.acc_alg_cb))
                    {
                        config_alg_local.acc_alg_cb(&alg_event_cfm);
                        topple_num = 0;
                        if (topple_num_max < TOPPLE_NUM_MAX * 5)
                        {
                            topple_num_max += TOPPLE_NUM_MAX;
                        }
                    }                   
                }                        
            }
        break;                                               
        }  
}

//静动处理函数
void acc_alg_action_proc(acc_data_type buffer_in)
{
    uint16_t buffer_1_len = 0;
    uint16_t buffer_2_len = 0;
#if PLAN_B_ONLY_ONE_DISCARD
    uint16_t i = 0;
#endif
    variance_t act_acc_variance = {0};
        
    buffer_1_len = activity_overrun_param_local.median_filter_buffer_len;
    buffer_2_len = activity_overrun_param_local.combine_acc_buffer_len;

    //静动处理
    if (act_buf1_len >= buffer_1_len)
    {
        acc_sensor_algo_combine_proc();//计算buffer_1 合加速度

        if (act_buf2_len >= buffer_2_len)
        {                    
            act_acc_variance = acc_sensor_algo_variance_calc_proc(act_buffer_2);//计算buffer_2 方差
            //判断方差阈值,返回车辆状态
            acc_sensor_algo_variance_judge_proc(&act_acc_variance);
            
         #if PLAN_A_ALL_DISCARD
            osel_memset(act_buffer_2, 0x00, sizeof(combine_acc_buffer_t)*ACT_COMBINE_ACC_BUFFER_MAX_LEN);
            act_buf2_len = 0;
         #endif

         #if PLAN_B_ONLY_ONE_DISCARD
         //删除最后最旧的一组数
         
             for (i = 0;i < (act_buf2_len - 1);i++)
            {
                act_buffer_2[i] = act_buffer_2[i + 1];
            }
             /*
             for (i = 0;i < (act_buf2_len - 2);i++)
            {
                act_buffer_2[i] = act_buffer_2[i + 2];
            }*/             
             osel_memset((act_buffer_2 + act_buf2_len - 1),0x00,sizeof(combine_acc_buffer_t));
             //osel_memset((act_buffer_2 + act_buf2_len - 2),0x00,sizeof(combine_acc_buffer_t) * 2);
             act_buf2_len--;
             //act_buf2_len = act_buf2_len - 2;
         #endif       
        }
        
        act_buffer_2[act_buf2_len].combine_acc = comb_acc_value;
        act_buffer_2[act_buf2_len].x = act_buffer_1[0].src_x;
        act_buffer_2[act_buf2_len].y = act_buffer_1[0].src_y;
        act_buffer_2[act_buf2_len].z = act_buffer_1[0].src_z;
        act_buf2_len ++;

        act_buffer_1[0] = act_buffer_1[1];
        act_buffer_1[1] = act_buffer_1[2];
        act_buf1_len--;              
    }
    
    act_buffer_1[act_buf1_len].src_x = buffer_in.src_x;
    act_buffer_1[act_buf1_len].src_y = buffer_in.src_y;
    act_buffer_1[act_buf1_len].src_z = buffer_in.src_z;
    act_buf1_len ++;   

}

//超限处理函数
void acc_alg_overrun_proc(void)
{
    int16_t acc_temp = 0;
    fp32_t p = 0;
    uint16_t ovr_lim = 0;
    fp32_t p_disa = 0;
    uint16_t ovr_lim_disa = 0;    
    acc_alg_event_t act_alg_cfm;
    
    p = (fp32_t)activity_overrun_param_local.overrun_percent/100;
    ovr_lim = activity_overrun_param_local.overrun_lim;
    p_disa = (fp32_t)activity_overrun_param_local.disa_percent/100;
    ovr_lim_disa = activity_overrun_param_local.disa_lim;    
    //2014-05-13:南通夜间报移动优化，暂时把ori_acc直接赋值成1000
    //acc_temp = comb_acc_value - ori_acc;
    acc_temp = comb_acc_value - ACC_ORI_OVERRUN;
    //协助防拆判断处理
   // if ((ori_acc != 0) && (ABS(acc_temp) >= (uint16_t)(ori_acc * p_disa)))
    if (ABS(acc_temp) >= (uint16_t)(ACC_ORI_OVERRUN * p_disa))
    {
        acc_count_disa ++;
        if (acc_count_disa >= ovr_lim_disa)
        {
        #if DISASSEMBLY_SECOND   
            suspect_overrun_disa_flag = 1;
        #endif
            acc_count_disa = 0;  
        }  
    }
    else
    {
        acc_count_disa = 0;
    }    
    //超限判断处理
    //if ((ori_acc != 0) && (ABS(acc_temp) >= (uint16_t)(ori_acc * p)))
    if (ABS(acc_temp) >= (uint16_t)(ACC_ORI_OVERRUN * p))
    {
        acc_count ++;
        if ((acc_count >= ovr_lim)
            &&(NULL != config_alg_local.acc_alg_cb))
        {
        #if DISASSEMBLY_FIRST
            overrun_status = ACT_OVERRUN_STATUS;
        #endif
            act_alg_cfm.type = ACC_ALG_OVERRUN_EVENT_TYPE;
            act_alg_cfm.overrun_args.overrun_flg = 0x01;
            act_alg_cfm.overrun_args.acc_x = act_buffer_2[0].x;
            act_alg_cfm.overrun_args.acc_y = act_buffer_2[0].y;
            act_alg_cfm.overrun_args.acc_z = act_buffer_2[0].z;
            config_alg_local.acc_alg_cb(&act_alg_cfm); 
            acc_count = 0;  
        }
        else
        {
        #if DISASSEMBLY_FIRST
            overrun_status = ACT_INVALID_STATUS;
        #endif
        }  
    }
    else
    {
        acc_count = 0;
    }    
    return;
}

//转向函数处理
void acc_alg_turn_proc(void)
{
    int32_t sum_turn = 0;
    int16_t max_turn = 0;
    int16_t min_turn = 0;
    int16_t array[3] = {0};
    int16_t turn_buffer_window[TURN_BUFFER_MAX_LEN] = {0};

    array[0] = turn_param_local.turn_angle_u;
    array[1] = turn_param_local.turn_angle_v;
    array[2] = turn_param_local.turn_angle_w;

    if (buffer_queue.count >= turn_data_out_count_thresh)//50
    {
        matrix_proc(&buffer_queue,array,turn_buffer_window);
        sum_turn = array_sum_proc(turn_buffer_window,turn_data_out_count_thresh);
        max_turn = array_max_proc(turn_buffer_window,turn_data_out_count_thresh);
        min_turn = array_min_proc(turn_buffer_window,turn_data_out_count_thresh);
        //计算特征值
        turn_tmp_sum = (int16_t)((sum_turn - max_turn - min_turn)/(turn_data_out_count_thresh - 2));

        //根据特征值输出状态
        turn_status_proc();
    }

    //当缓存未有达到阈值，特征值默认为0   
    if (buffer_queue.count < turn_data_out_count_thresh)
    {
        turn_tmp_sum = 0;
        //根据特征值输出状态
        turn_status_proc();        
    }
}

//数组结构体转成一维数组的处理函数
void acc_change_to_array_proc(acc_circular_queue_t* queue,
                             sum_max_min_t *temp_s,
                             uint16_t data_len,
                             uint8_t queue_flag)
{
    uint16_t i = 0;
    uint16_t j = 0;

    int16_t array_x[ACC_QUEUE_MAXLEN] = {0};
    int16_t array_y[ACC_QUEUE_MAXLEN] = {0};
    int16_t array_z[ACC_QUEUE_MAXLEN] = {0};
    
    switch(queue_flag)
    {
        case ACC_DROP_FLAG:
            j = queue->personal_drop_front;
            break;
         case ACC_TOPPLE_BAK_FLAG:
            j = queue->personal_topple_bak_front;
            break;
        case ACC_TOPPLE_FILTER_FLAG:
            j = queue->personal_topple_filter_front;
            break;            
    }
    
    for (i = 0;i < data_len;i++,j++)
    {
        if (j >= ACC_QUEUE_MAXLEN)
        {
            array_x[i] = queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_x;
            array_y[i] = queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_y;
            array_z[i] = queue->item[(j&ACC_QUEUE_MAXLEN)>>7].src_z;            
        }
        else
        {
            array_x[i] = queue->item[j].src_x;
            array_y[i] = queue->item[j].src_y;
            array_z[i] = queue->item[j].src_z;                
        }

    }

    temp_s->sum_x = array_sum_proc(array_x,data_len);
    temp_s->sum_y = array_sum_proc(array_y,data_len);
    temp_s->sum_z = array_sum_proc(array_z,data_len);

    temp_s->max_x = array_max_proc(array_x,data_len);
    temp_s->max_y = array_max_proc(array_y,data_len);
    temp_s->max_z = array_max_proc(array_z,data_len);

    temp_s->min_x = array_min_proc(array_x,data_len);
    temp_s->min_y = array_min_proc(array_y,data_len);
    temp_s->min_z = array_min_proc(array_z,data_len);      
    return;
}

//数组结构体转成一维数组的处理函数
void acc_change_to_array_proc_for_filter(uint8_t buf_len,
                             src_data_buffer_t buf_in[],
                             sum_max_min_t *temp_s)
{
    uint8_t i = 0;
    int16_t array_x[ACC_QUEUE_MAXLEN] = {0};
    int16_t array_y[ACC_QUEUE_MAXLEN] = {0};
    int16_t array_z[ACC_QUEUE_MAXLEN] = {0};
    
    for (i = 0;i < buf_len;i++)
    {
        array_x[i] = buf_in[i].src_x;
        array_y[i] = buf_in[i].src_y;
        array_z[i] = buf_in[i].src_z;
    }

    temp_s->sum_x = array_sum_proc(array_x,buf_len);
    temp_s->sum_y = array_sum_proc(array_y,buf_len);
    temp_s->sum_z = array_sum_proc(array_z,buf_len);

    temp_s->max_x = array_max_proc(array_x,buf_len);
    temp_s->max_y = array_max_proc(array_y,buf_len);
    temp_s->max_z = array_max_proc(array_z,buf_len);

    temp_s->min_x = array_min_proc(array_x,buf_len);
    temp_s->min_y = array_min_proc(array_y,buf_len);
    temp_s->min_z = array_min_proc(array_z,buf_len);       
}

//跌落函数处理
void acc_alg_drop_proc(void)
{
    sum_max_min_t temp_s = {0};
    int16_t drop_filter_buffer[DROP_TMP_FILTER_BUFFER_MAX_LEN] = {0};
    uint16_t data_len = 0;
    uint8_t queue_flag = 0;
    queue_flag = ACC_DROP_FLAG;


    if (buffer_queue.count >= drop_data_out_count_thresh)//10
    {
        data_len = drop_data_out_count_thresh;
        acc_change_to_array_proc(&buffer_queue,
                              &temp_s,
                              data_len,queue_flag);        
        //计算特征值
        drop_filter_buffer[0] = (int16_t)((temp_s.sum_x - temp_s.max_x - temp_s.min_x)
                                            /(drop_data_out_count_thresh - 2));
        drop_filter_buffer[1] = (int16_t)((temp_s.sum_y- temp_s.max_y - temp_s.min_y)
                                            /(drop_data_out_count_thresh - 2));
        drop_filter_buffer[2] = (int16_t)((temp_s.sum_z - temp_s.max_z - temp_s.min_z)
                                            /(drop_data_out_count_thresh - 2));    

        //根据特征值输出状态
        drop_status_proc(drop_filter_buffer);      
    }

    //当缓存未有达到阈值，取缓存中数据均值作为滤波后数据
    if (buffer_queue.count < drop_data_out_count_thresh)//10
    {
        data_len = buffer_queue.count;
        acc_change_to_array_proc(&buffer_queue,
                              &temp_s,
                              data_len,queue_flag);
        //计算特征值
        drop_filter_buffer[0] = (int16_t)(temp_s.sum_x/buffer_queue.count);
        drop_filter_buffer[1] = (int16_t)(temp_s.sum_y/buffer_queue.count);
        drop_filter_buffer[2] = (int16_t)(temp_s.sum_z/buffer_queue.count);

        //根据特征值输出状态
        drop_status_proc(drop_filter_buffer);
    }    
}

//倾倒函数处理
void acc_alg_topple_proc(int16_t acc_x,int16_t acc_y,int16_t acc_z)
{
    uint16_t i = 0;
    sum_max_min_t temp_s = {0};
    sum_max_min_t temp_s_1 = {0};
    
    if (topple_param_local.topple_if_dynamic_bak)
    {
        if (topple_bak_buf_len >= topple_bak_count_thresh)
        {
            acc_change_to_array_proc_for_filter(topple_bak_buf_len,
                                  topple_bak_buffer,
                                  &temp_s);           
            
            //判断数据是否稳定,若稳定计算背景更新
            if (((temp_s.max_x - temp_s.min_x) <= topple_param_local.topple_bak_thresh)
                &&((temp_s.max_y - temp_s.min_y) <= topple_param_local.topple_bak_thresh)
                &&((temp_s.max_z - temp_s.min_z) <= topple_param_local.topple_bak_thresh))
            {
                topple_tmp_bak_buffer[0] = (int16_t)((temp_s.sum_x - temp_s.max_x
                            -temp_s.min_x)/(topple_bak_buf_len - 2));
                topple_tmp_bak_buffer[1] = (int16_t)((temp_s.sum_y - temp_s.max_y
                            -temp_s.min_y)/(topple_bak_buf_len - 2));
                topple_tmp_bak_buffer[2] = (int16_t)((temp_s.sum_z - temp_s.max_z
                            -temp_s.min_z)/(topple_bak_buf_len - 2));
            }
            
            //稳定或不稳定都要把删除最旧的历史数据
            for (i = 0;i < (topple_bak_buf_len - 1);i++)
            {
                topple_bak_buffer[i] = topple_bak_buffer[i + 1];
            }

            osel_memset((topple_bak_buffer + topple_bak_buf_len -1),0x00,sizeof(src_data_buffer_t));

            topple_bak_buf_len--;            
        }
        
        //往bak缓存
        topple_bak_buffer[topple_bak_buf_len].src_x = acc_x;
        topple_bak_buffer[topple_bak_buf_len].src_y = acc_y;
        topple_bak_buffer[topple_bak_buf_len].src_z = acc_z;
        topple_bak_buf_len++;
        
        //往filter缓存
        if (topple_filter_buf_len >= topple_filter_count_thresh)
        {
            acc_change_to_array_proc_for_filter(topple_filter_buf_len,
                                  topple_filter_buffer,
                                  &temp_s_1);

            //计算中值滤波后输出值
            topple_tmp_filter_buffer[0] = (int16_t)((temp_s_1.sum_x - 
                temp_s_1.max_x - temp_s_1.min_x)/(topple_filter_buf_len - 2));
            topple_tmp_filter_buffer[1] = (int16_t)((temp_s_1.sum_y - 
                temp_s_1.max_y - temp_s_1.min_y)/(topple_filter_buf_len - 2));
            topple_tmp_filter_buffer[2] = (int16_t)((temp_s_1.sum_z - 
                temp_s_1.max_z - temp_s_1.min_z)/(topple_filter_buf_len - 2));

            //删除缓存区中最旧的历史数据
            for (i = 0;i < (topple_filter_buf_len - 1);i++)
            {
                topple_filter_buffer[i] = topple_filter_buffer[i + 1];
            }

            osel_memset((topple_filter_buffer + topple_filter_buf_len -1),0x00,sizeof(src_data_buffer_t));

            //根据特征值输出状态
            topple_status_proc();
            topple_filter_buf_len --;   
        }
        
        topple_filter_buffer[topple_filter_buf_len].src_x = acc_x;
        topple_filter_buffer[topple_filter_buf_len].src_y = acc_y;
        topple_filter_buffer[topple_filter_buf_len].src_z = acc_z;
        topple_filter_buf_len++;

        //当缓存未有达到阈值，取缓存中数据均值作为滤波后数据
        if (topple_filter_buf_len < topple_filter_count_thresh)
        {
           acc_change_to_array_proc_for_filter(topple_filter_buf_len,
                                  topple_filter_buffer,
                                  &temp_s_1);

            //计算特征值
            topple_tmp_filter_buffer[0] = (int16_t)(temp_s_1.sum_x/topple_filter_buf_len);
            topple_tmp_filter_buffer[1] = (int16_t)(temp_s_1.sum_y/topple_filter_buf_len);
            topple_tmp_filter_buffer[2] = (int16_t)(temp_s_1.sum_z/topple_filter_buf_len);

            //根据特征值输出状态
            topple_status_proc();
        }       
    }
    else
    {
        //往filter缓存
        if (topple_filter_buf_len >= topple_filter_count_thresh)
        {
            acc_change_to_array_proc_for_filter(topple_filter_buf_len,
                                  topple_filter_buffer,
                                  &temp_s_1);

            //计算中值滤波后输出值
            topple_tmp_filter_buffer[0] = (int16_t)((temp_s_1.sum_x - 
                temp_s_1.max_x - temp_s_1.min_x)/(topple_filter_buf_len - 2));
            topple_tmp_filter_buffer[1] = (int16_t)((temp_s_1.sum_y - 
                temp_s_1.max_y - temp_s_1.min_y)/(topple_filter_buf_len - 2));
            topple_tmp_filter_buffer[2] = (int16_t)((temp_s_1.sum_z - 
                temp_s_1.max_z - temp_s_1.min_z)/(topple_filter_buf_len - 2));

            //删除缓存区中最旧的历史数据
            for (i = 0;i < (topple_filter_buf_len - 1);i++)
            {
                topple_filter_buffer[i] = topple_filter_buffer[i + 1];
            }

            osel_memset((topple_filter_buffer + topple_filter_buf_len -1),0x00,sizeof(src_data_buffer_t));

            //根据特征值输出状态
            topple_status_proc();
            topple_filter_buf_len --;   
        }
        
        topple_filter_buffer[topple_filter_buf_len].src_x = acc_x;
        topple_filter_buffer[topple_filter_buf_len].src_y = acc_y;
        topple_filter_buffer[topple_filter_buf_len].src_z = acc_z;
        topple_filter_buf_len++;

        //当缓存未有达到阈值，取缓存中数据均值作为滤波后数据
        if (topple_filter_buf_len < topple_filter_count_thresh)
        {
           acc_change_to_array_proc_for_filter(topple_filter_buf_len,
                                  topple_filter_buffer,
                                  &temp_s_1);

            //计算特征值
            topple_tmp_filter_buffer[0] = (int16_t)(temp_s_1.sum_x/topple_filter_buf_len);
            topple_tmp_filter_buffer[1] = (int16_t)(temp_s_1.sum_y/topple_filter_buf_len);
            topple_tmp_filter_buffer[2] = (int16_t)(temp_s_1.sum_z/topple_filter_buf_len);

            //根据特征值输出状态
            topple_status_proc();
        }          
    }
}

//倾倒函数处理:此处理为共享buffer的处理，但由于处理速度
//问题会影响背景值的更新，故暂时屏蔽，待以后使用
#if 0
void acc_alg_topple_proc(void)
{
    sum_max_min_t temp_s = {0};
    sum_max_min_t temp_s_1 = {0};
    uint16_t data_len = 0;
    uint8_t queue_flag = 0;
   
    if (topple_param_local.topple_if_dynamic_bak)
    {
        if (buffer_queue.count >= topple_bak_count_thresh)//100
        {
            data_len = topple_bak_count_thresh;
            queue_flag = ACC_TOPPLE_BAK_FLAG;            
            acc_change_to_array_proc(&buffer_queue,
                                  &temp_s,
                                  data_len,queue_flag);
            
            //判断数据是否稳定,若稳定计算背景更新
            if (((temp_s.max_x - temp_s.min_x) <= topple_param_local.topple_bak_thresh)
                &&((temp_s.max_y - temp_s.min_y) <= topple_param_local.topple_bak_thresh)
                &&((temp_s.max_z - temp_s.min_z) <= topple_param_local.topple_bak_thresh))
            {
                topple_tmp_bak_buffer[0] = (int16_t)((temp_s.sum_x - temp_s.max_x
                            -temp_s.min_x)/(topple_bak_count_thresh - 2));
                topple_tmp_bak_buffer[1] = (int16_t)((temp_s.sum_y - temp_s.max_y
                            -temp_s.min_y)/(topple_bak_count_thresh - 2));
                topple_tmp_bak_buffer[2] = (int16_t)((temp_s.sum_z - temp_s.max_z
                            -temp_s.min_z)/(topple_bak_count_thresh - 2));
            }                          
        }
        
        //往filter缓存
        if (buffer_queue.count >= topple_filter_count_thresh)//10
        {
            data_len = topple_filter_count_thresh;
            queue_flag = ACC_TOPPLE_FILTER_FLAG;
            acc_change_to_array_proc(&buffer_queue,
                                  &temp_s_1,
                                  data_len,queue_flag);

            //计算中值滤波后输出值
            topple_tmp_filter_buffer[0] = (int16_t)((temp_s_1.sum_x - 
                temp_s_1.max_x - temp_s_1.min_x)/(topple_filter_count_thresh - 2));
            topple_tmp_filter_buffer[1] = (int16_t)((temp_s_1.sum_y - 
                temp_s_1.max_y - temp_s_1.min_y)/(topple_filter_count_thresh - 2));
            topple_tmp_filter_buffer[2] = (int16_t)((temp_s_1.sum_z - 
                temp_s_1.max_z - temp_s_1.min_z)/(topple_filter_count_thresh - 2));

            //根据特征值输出状态
            topple_status_proc(); 
        }
        
        //当缓存未有达到阈值，取缓存中数据均值作为滤波后数据
        if (buffer_queue.count < topple_filter_count_thresh)//10
        {
           data_len = buffer_queue.count;
           queue_flag = ACC_TOPPLE_FILTER_FLAG;
           acc_change_to_array_proc(&buffer_queue,
                                  &temp_s_1,
                                  data_len,queue_flag);

            //计算特征值
            topple_tmp_filter_buffer[0] = (int16_t)(temp_s_1.sum_x/buffer_queue.count);
            topple_tmp_filter_buffer[1] = (int16_t)(temp_s_1.sum_y/buffer_queue.count);
            topple_tmp_filter_buffer[2] = (int16_t)(temp_s_1.sum_z/buffer_queue.count);

            //根据特征值输出状态
            topple_status_proc();
        }       
    }    
    else
    {
        //往filter缓存
        if (buffer_queue.count >= topple_filter_count_thresh)//10
        {
            data_len = topple_filter_count_thresh;
            queue_flag = ACC_TOPPLE_FILTER_FLAG;
            acc_change_to_array_proc(&buffer_queue,
                                  &temp_s_1,
                                  data_len,queue_flag);

            //计算中值滤波后输出值
            topple_tmp_filter_buffer[0] = (int16_t)((temp_s_1.sum_x - 
                temp_s_1.max_x - temp_s_1.min_x)/(topple_filter_count_thresh - 2));
            topple_tmp_filter_buffer[1] = (int16_t)((temp_s_1.sum_y - 
                temp_s_1.max_y - temp_s_1.min_y)/(topple_filter_count_thresh - 2));
            topple_tmp_filter_buffer[2] = (int16_t)((temp_s_1.sum_z - 
                temp_s_1.max_z - temp_s_1.min_z)/(topple_filter_count_thresh - 2));

            //根据特征值输出状态
            topple_status_proc(); 
        }
        
        //当缓存未有达到阈值，取缓存中数据均值作为滤波后数据
        if (buffer_queue.count < topple_filter_count_thresh)//10
        {
           data_len = buffer_queue.count;
           queue_flag = ACC_TOPPLE_FILTER_FLAG;
           acc_change_to_array_proc(&buffer_queue,
                                  &temp_s_1,
                                  data_len,queue_flag);

            //计算特征值
            topple_tmp_filter_buffer[0] = (int16_t)(temp_s_1.sum_x/buffer_queue.count);
            topple_tmp_filter_buffer[1] = (int16_t)(temp_s_1.sum_y/buffer_queue.count);
            topple_tmp_filter_buffer[2] = (int16_t)(temp_s_1.sum_z/buffer_queue.count);

            //根据特征值输出状态
            topple_status_proc();
        }         
    }
}
#endif
//疑似拆除算法处理函数
void acc_alg_suspect_disa_proc(void)
{
    acc_alg_event_t alg_event_cfm;
    alg_event_cfm.type = ACC_ALG_DISA_EVENT_TYPE;
    alg_event_cfm.disa_args.disa_flag = 0x01;
    
    switch(disa_status)
    {
        case DISA_STATUS_T0:
            disa_total_send_status = 0;
            if (suspect_overrun_disa_flag == 1)//E1
            {
                disa_status = DISA_STATUS_T1;
                disa_count = 0;
            }
            
            if ((topple_c_term > topple_param_local.topple_disa_cmp_thresh)
                &&(topple_f_term < topple_param_local.topple_disa_mod_thresh))//E2
            {
                disa_status = DISA_STATUS_T2;
                disa_count = 0;
            }
            break;

        case DISA_STATUS_T1:
            disa_count ++;
            if (disa_count * ACC_SAMPLE_TIMER >= TIME_DISA)//T>=5000
            {
                disa_status = DISA_STATUS_T0;
            }
            
            if ((disa_count * ACC_SAMPLE_TIMER < TIME_DISA)
                &&(topple_c_term > topple_param_local.topple_disa_cmp_thresh)
                &&(topple_f_term < topple_param_local.topple_disa_mod_thresh))//E2
            {
                disa_status = DISA_STATUS_T3;
                disa_history_status = DISA_STATUS_T1;               
            }
            else 
            {
                suspect_overrun_disa_flag = 0;
            }
            
            if (suspect_overrun_disa_flag == 1)//E1
            {
                disa_count = 0;
            }
            break;
            
        case DISA_STATUS_T2:
            disa_count ++;
            if (disa_count * ACC_SAMPLE_TIMER >= TIME_DISA)//T>=5000
            {
                disa_status = DISA_STATUS_T0;
            }
            
            if ((disa_count * ACC_SAMPLE_TIMER < TIME_DISA)
                &&(suspect_overrun_disa_flag == 1))
            {
                disa_status = DISA_STATUS_T3;
                disa_history_status = DISA_STATUS_T2;             
            } 
            
            if ((topple_c_term > topple_param_local.topple_disa_cmp_thresh)
                &&(topple_f_term < topple_param_local.topple_disa_mod_thresh))//E2
            {
                disa_count = 0;
            }            
            break;

        case DISA_STATUS_T3:
            if ((disa_total_send_status == 0)
                &&(NULL != config_alg_local.acc_alg_cb))
            {
                config_alg_local.acc_alg_cb(&alg_event_cfm);
            }
            disa_total_send_status = 1;
            suspect_overrun_disa_flag = 0;
            if (disa_history_status == DISA_STATUS_T1)
            {
                disa_status = DISA_STATUS_T2;
                disa_count = 0;
            }
            else if (disa_history_status == DISA_STATUS_T2)
            {
                disa_status = DISA_STATUS_T1;
                disa_count = 0;
            }
            break;            
            
    }
    return;
}


//拆卸报警处理
void acc_alg_disassembly_proc(void)
{    
    acc_alg_event_t alg_event_cfm;
    alg_event_cfm.type = ACC_ALG_DISA_EVENT_TYPE;
    alg_event_cfm.disa_args.disa_flag = 0x01;
    
#if DISASSEMBLY_FIRST
    if (ACT_STATIC_STATUS == act_status)
    {
       if ((ACT_OVERRUN_STATUS == overrun_status)
            ||(DROP_DROP_STATUS == drop_out_status)
            ||(TOPPLE_FLIP_STATUS == topple_out_status))
        {           
            if ((DISASSEMBLY_NO_STATUS == disassembly_status)
                &&(NULL != config_alg_local.acc_alg_cb))
            {
                config_alg_local.acc_alg_cb(&alg_event_cfm);
                disassembly_status = DISASSEMBLY_STATUS;
            }           
        }
    }
    else if (ACT_NORMAL_DRIVE_STATUS == act_status)
    {
        if ((DROP_DROP_STATUS == drop_out_status)
            ||(TOPPLE_FLIP_STATUS == topple_out_status))
        {          
            if ((DISASSEMBLY_NO_STATUS == disassembly_status)
                &&(NULL != config_alg_local.acc_alg_cb))
            {
                config_alg_local.acc_alg_cb(&alg_event_cfm);
                disassembly_status = DISASSEMBLY_STATUS;
            }          
        }
    }    
#endif    

#if DISASSEMBLY_SECOND   
//必定拆除处理
    if ((DROP_NORMAL_STATUS == drop_out_history_status)
        &&(DROP_DROP_STATUS == drop_out_status))
    {
        drop_out_history_status = drop_out_status;
        drop_disa_flag = 1;
    }
    else if (drop_out_history_status != drop_out_status)
        {
            drop_out_history_status = drop_out_status;
        }

    if ((TOPPLE_NORMAL_STATUS == topple_out_history_status)
        &&(TOPPLE_FLIP_STATUS == topple_out_status))
    {
        topple_out_history_status = topple_out_status;
        topple_disa_flag = 1;
    }
    else if (topple_out_history_status != topple_out_status)
        {
            topple_out_history_status = topple_out_status;
        }
 
    if (((drop_disa_flag == 1)
        ||(topple_disa_flag == 1))
        &&(NULL != config_alg_local.acc_alg_cb))
    {
        config_alg_local.acc_alg_cb(&alg_event_cfm);
        drop_disa_flag = 0;
        topple_disa_flag = 0;
    }
 //疑似拆除处理
    acc_alg_suspect_disa_proc();
#endif
    return;
}

void acc_alg_pre_data_proc(acc_data_type buf_in)
{
    int32_t tmp_x = 0;
    int32_t tmp_y = 0;
    int32_t tmp_z = 0;
    
    if (data_num_2 < ACC_PRE_DATA_MAX_NUM)
    {
        pre_x += buf_in.src_x;
        pre_y += buf_in.src_y;
        pre_z += buf_in.src_z;     
        data_num_2 ++;
    }
    else
    {
        pre_x = pre_x/ACC_PRE_DATA_MAX_NUM;
        pre_y = pre_y/ACC_PRE_DATA_MAX_NUM;
        pre_z = pre_z/ACC_PRE_DATA_MAX_NUM;
        data_num_flag = 1;
/*
        if ((pre_x * pre_x + pre_y * pre_y) == 0)
        {
            cos_b = 1;
            sin_b = 0;
        }
        else
        {
            cos_b = pre_x/(sqrt(pre_x * pre_x + pre_y * pre_y));
            sin_b = pre_y/(sqrt(pre_x * pre_x + pre_y * pre_y));
        }

        cos_a = -pre_z/(sqrt(pre_x * pre_x + pre_y * pre_y + pre_z * pre_z));
        sin_a = (sqrt(pre_x * pre_x + pre_y * pre_y))
                        /(sqrt(pre_x * pre_x + pre_y * pre_y + pre_z * pre_z));   
               
        //以下计算倾倒特征值的初值
        
        topple_tmp_bak_buffer[0] = (int16_t)(-(pre_x * cos_a * cos_b 
                                                            + pre_y * sin_b * cos_a
                                                            + pre_z * sin_a));
        topple_tmp_bak_buffer[1] = (int16_t)(pre_y * cos_b - pre_x * sin_b);
        topple_tmp_bak_buffer[2] = (int16_t)(pre_x * sin_a * cos_b
                                                            + pre_y * sin_b * sin_a
                                                            - pre_z * cos_a);*/
        topple_tmp_bak_buffer[0] = (int16_t)(pre_x);  
        topple_tmp_bak_buffer[1] = (int16_t)(pre_y); 
        topple_tmp_bak_buffer[2] = (int16_t)(pre_z); 
        tmp_x = topple_tmp_bak_buffer[0];
        tmp_y = topple_tmp_bak_buffer[1];
        tmp_z = topple_tmp_bak_buffer[2];
        //topple_f_data_len = (uint16_t)((sqrt(pre_x * pre_x + pre_y * pre_y + pre_z * pre_z)));  
        topple_f_data_len = (uint16_t)sqrt(tmp_x * tmp_x+ tmp_y * tmp_y+ tmp_z * tmp_z);
    }   
    return;
}
// 传入加速度数据
void acc_alg_new_data(const acc_alg_data_t *acc_data)
{
    //acc_data_type buf_out;
    acc_data_type buf_pre_in;
    acc_data_type buf_in;
    DBG_ASSERT(NULL != acc_data __DBG_LINE);

    buf_pre_in.src_x = acc_data->x;
    buf_pre_in.src_y = acc_data->y;
    buf_pre_in.src_z = acc_data->z;    
    if (data_num_1 <= ACC_PRE_DATA_MAX_NUM)
    {
        acc_alg_pre_data_proc(buf_pre_in);
        data_num_1 ++;
    }
    
    if (data_num_flag)
    {
    /*
        buf_in.src_x = (int16_t)(-(acc_data->x * cos_a * cos_b 
                                + acc_data->y * sin_b * cos_a
                                + acc_data->z * sin_a));
        buf_in.src_y = (int16_t)(acc_data->y * cos_b - acc_data->x * sin_b);
        buf_in.src_z = (int16_t)(acc_data->x * sin_a * cos_b
                                + acc_data->y * sin_b * sin_a
                                - acc_data->z * cos_a);
*/
    buf_in.src_x = acc_data->x;
    buf_in.src_y = acc_data->y;
    buf_in.src_z = acc_data->z;   
    //数据入队列
        acc_queue_send(&buffer_queue, buf_in);
    //静动处理
        acc_alg_action_proc(buf_in);
    //超限处理
        acc_alg_overrun_proc();
    //转向处理
        acc_alg_turn_proc();
    //跌落处理
        acc_alg_drop_proc();
    //倾倒处理
        acc_alg_topple_proc(acc_data->x,acc_data->y,acc_data->z);
    //拆卸检测
        acc_alg_disassembly_proc();    
    //数据出队列
        if (buffer_queue.count >= ACC_QUEUE_MAXLEN)
        {
            //acc_queue_receive(&buffer_queue, &buf_out,ACC_QUEUE_MAXLEN);
            buffer_queue.count--;
        }        
    }

    return;
}














