/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : 温湿度                                                                                         
**  File        : escort_humiture.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是温湿度的发送处理                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <osel_arch.h>
#include <hal_timer.h>
#include <hal_board.h>
#include <hal_sensor.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_send.h>

#define ESCORT_HUMITURE_PERIOD      60000

static hal_timer_t *humiture_timer = NULL;
static bool_t humiture_timer_started = FALSE;
uint32_t humiture_period = ESCORT_HUMITURE_PERIOD;

static void humiture_timer_cb(void *arg)
{
    humiture_timer = NULL;
    osel_post(ESCORT_HUMITURE_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void humiture_start_timer(uint32_t period)
{
    if (period == 0)
    {
        return;
    }
    
    if (NULL != humiture_timer)
    {
        hal_timer_cancel(&humiture_timer);
        DBG_ASSERT(humiture_timer == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(period),
                      humiture_timer_cb,
                      NULL,
                      humiture_timer);
    DBG_ASSERT(humiture_timer != NULL __DBG_LINE);
    
    humiture_timer_started = TRUE;
}

static void humiture_stop_timer()
{
    if (NULL != humiture_timer)
    {
        hal_timer_cancel(&humiture_timer);
        DBG_ASSERT(humiture_timer == NULL __DBG_LINE);
    }
    
    humiture_timer_started = FALSE;
}

void escort_humiture_set_period(uint32_t period)
{
    humiture_period = period;
    if (humiture_period != 0)
    {
        humiture_start_timer(humiture_period);
    }
    else
    {
        humiture_stop_timer();
    }
}

void escort_humiture_init()
{
    humiture_timer = NULL;
    humiture_period = ESCORT_HUMITURE_PERIOD;
    escort_humiture_set_period(humiture_period);
    humiture_timer_started = FALSE;
}

void escort_humiture_timer_handle(void *arg)
{
    uint16_t temperature;
    uint16_t humidity;
//    fp32_t temperature_f;
//    int16_t temperature_f;
//    fp32_t humidity_f;
//    escort_frame_t frame;
    
    if (!humiture_timer_started)
    {
        return;
    }
    
    humiture_start_timer(humiture_period);
    hal_sensor_read(&temperature, SENSOR_DHT, DHT_SENSOR_TYPE_T);
    hal_sensor_read(&humidity, SENSOR_DHT, DHT_SENSOR_TYPE_H);

//    temperature_f = (int16_t)(-46.85 + 175.72 / 65536 * (fp32_t)temperature);
//    humidity_f = -6.0 + 125.0 / 65536 * (fp32_t)humidity;
    
//    frame.type = ESCORT_APP_MESSAGE_APP_TYPE;
//    frame.app_data.type = ESCORT_APP_DATA_HUMITURE_TYPE;
//    frame.app_data.humiture_data.temperature = temperature_f;
//    frame.app_data.humiture_data.humidity = humidity_f;
//    escort_send_request(&frame, ESCORT_SEND_LIST_PRIORITY_HIGH,
//                            0, FALSE);
    return;
}
