/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : app                                                                                          
**  File        : escort_app.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是app消息                                 
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <data_type_def.h>
#include <osel_arch.h>
#include <debug.h>
#include <hal_uart.h>
#include <serial.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <escort_acc.h>
#include <escort_gps.h>
#include <escort_send.h>
#include <escort_dismantle.h>
#include <escort_humiture.h>
#include <escort_startup.h>
#include <escort_nfc.h>
#include <escort_led.h>
#include <escort_heartbeat.h>

void escort_app_task(void *e)
{
    DBG_ASSERT(e != NULL __DBG_LINE);
    osel_event_t *p = (osel_event_t *)e;

    switch (p->sig)
    {
    case ESCORT_STARTUP_TIMER_EVENT:
        escort_startup_timer_handle(p->param);
        break;
#if NODE_SSN_DEBUG        
    case ESCORT_STARTUP_TIMER_TEST_EVENT:
        escort_startup_timer_test_handle(p->param);
        break;
#endif        
    case ESCORT_ACC_SENSOR_INT_EVENT:
        escort_acc_sensor_int_event_handle(p->param);
        break;
    case ESCORT_WAIT_SENDING_TIMER_EVENT:
        escort_wait_sending_timer_event_handle(p->param);
        break;
    case ESCORT_WAIT_ACK_TIMER_EVENT:
        escort_wait_ack_timer_event_handle(p->param);
        break;
    case ESCORT_DATA_SENT_EVENT:
        escort_data_sent_event_handle(p->param);
        break;
    case ESCORT_DATA_RECEIVED_EVENT:
        escort_data_received_event_handle(p->param);
        break;
    case ESCORT_SSN_CONTROL_EVENT:
        escort_ssn_ctrl_event_handle(p->param);
        break;
    case ESCORT_GPRS_CONTROL_EVENT:
        break;
    case ESCORT_ACC_DATA_TIMER_EVENT:
        escort_acc_data_timer_event_handle(p->param);
        break;
    case ESCORT_GPS_DATA_EVENT:
        escort_gps_data_event_handle(p->param);
        break;
    case ESCORT_DISMANTLE_EVENT:
        escort_dismantle_event_handle(p->param);
        break;
    case ESCORT_HUMITURE_TIMER_EVENT:
        escort_humiture_timer_handle(p->param);
        break;
//    case ESCORT_NFC_CHECK_TIMER_EVENT:
//        escort_nfc_check_timer_handle(p->param);
//        break;
    case ESCORT_LED_TIMER_EVENT:
        escort_led_timer_handle(p->param);
        break;
     case ESCORT_WAIT_DISMANTLE_EVENT:
        escort_wait_dismantle_handle(p->param);
        break;
     case ESCORT_LED_SELF_CHECK_TIMER_EVENT:
        escort_self_check_succ_handle(p->param);
        break;
    case ESCORT_HEARTBEAT_TIMER_EVENT:
        escort_heartbeat_timer_handle(p->param);
        break;
//    case ESCORT_WAIT_UNBIND_EVENT:
//        escort_wait_unbind_handle(p->param);
//        break;
	case ESCORT_NFC_INT_EVENT:
        escort_nfc_int_handle(p->param);
        break;
	case ESCORT_POWER_TIMER_1_EVENT:
        escort_low_power_1_handle(p->param);
        break;   
	case ESCORT_POWER_TIMER_2_EVENT:
        escort_low_power_2_handle(p->param);
        break;  
        
    case ESCORT_INIT_OPEN_SSN_EVENT:
        escort_init_open_ssn_handle(p->param);
        break;
    case ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT:
        escort_act_to_static_open_ssn_handle(p->param);
        break;  
    case ESCORT_STATIC_HEART_OPEN_SSN_EVENT:
        escort_static_heart_open_ssn_handle(p->param);
        break;    

    case ESCORT_DISMANTLE_OPEN_SSN_EVENT:
        escort_dismantle_open_ssn_handle(p->param);
        break;  
        
    case ESCORT_SSN_CTRL_SEND_EVENT:
        escort_ssn_ctrl_send_handle(p->param);
        break; 

    case ESCORT_NFC_STOP_CHECK_TIME_EVENT:
        escort_nfc_stop_check_timer_handle(p->param);
        break; 
        
    default :
        DBG_ASSERT(FALSE __DBG_LINE);
        break;
    }
}

static void uart_cfg(void)
{
    serial_reg_t uart_reg;

    uart_reg.sd.valid = TRUE;
    uart_reg.sd.len = 2;
    uart_reg.sd.pos = 0;
    uart_reg.sd.data[0] = 0xD5;
    uart_reg.sd.data[1] = 0xC8;

    uart_reg.ld.valid = FALSE;

    uart_reg.argu.len_max = UART_LEN_MAX;
    uart_reg.argu.len_min = 2;

    uart_reg.ed.valid = FALSE;

    uart_reg.echo_en = FALSE;
    uart_reg.func_ptr = NULL;

    serial_fsm_init(HAL_UART_1);
    serial_reg(HAL_UART_1, uart_reg);
}

void escort_app_init(void)
{
    osel_task_tcb *task_handle = osel_task_create(&escort_app_task, 
                                                  PRO_TASK_PRIO);

    osel_subscribe(task_handle, ESCORT_STARTUP_TIMER_EVENT);
    osel_subscribe(task_handle, ESCORT_ACC_SENSOR_INT_EVENT);
    osel_subscribe(task_handle, ESCORT_WAIT_SENDING_TIMER_EVENT);
    osel_subscribe(task_handle, ESCORT_WAIT_ACK_TIMER_EVENT);    
    osel_subscribe(task_handle, ESCORT_DATA_SENT_EVENT);
    osel_subscribe(task_handle, ESCORT_DATA_RECEIVED_EVENT);
    osel_subscribe(task_handle, ESCORT_SSN_CONTROL_EVENT);
    osel_subscribe(task_handle, ESCORT_GPRS_CONTROL_EVENT);
    osel_subscribe(task_handle, ESCORT_ACC_DATA_TIMER_EVENT);
    osel_subscribe(task_handle, ESCORT_GPS_DATA_EVENT);
    osel_subscribe(task_handle, ESCORT_DISMANTLE_EVENT);
    osel_subscribe(task_handle, ESCORT_HUMITURE_TIMER_EVENT);
//    osel_subscribe(task_handle, ESCORT_NFC_CHECK_TIMER_EVENT);
    osel_subscribe(task_handle, ESCORT_LED_TIMER_EVENT);    
    osel_subscribe(task_handle, ESCORT_WAIT_DISMANTLE_EVENT);
    osel_subscribe(task_handle, ESCORT_LED_SELF_CHECK_TIMER_EVENT);
    osel_subscribe(task_handle, ESCORT_HEARTBEAT_TIMER_EVENT);
	osel_subscribe(task_handle, ESCORT_NFC_INT_EVENT);
    osel_subscribe(task_handle, ESCORT_POWER_TIMER_1_EVENT);
    osel_subscribe(task_handle, ESCORT_POWER_TIMER_2_EVENT);
    
    osel_subscribe(task_handle, ESCORT_INIT_OPEN_SSN_EVENT);
    osel_subscribe(task_handle, ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT);
    osel_subscribe(task_handle, ESCORT_STATIC_HEART_OPEN_SSN_EVENT);
    osel_subscribe(task_handle, ESCORT_DISMANTLE_OPEN_SSN_EVENT);
    osel_subscribe(task_handle, ESCORT_SSN_CTRL_SEND_EVENT);
    osel_subscribe(task_handle, ESCORT_NFC_STOP_CHECK_TIME_EVENT);
#if NODE_SSN_DEBUG      
    osel_subscribe(task_handle, ESCORT_STARTUP_TIMER_TEST_EVENT);
#endif    
    escort_led_init();
    escort_nfc_init();
    uart_cfg();
    escort_acc_sensor_init();
    escort_dismantle_init();
    escort_heartbeat_init();
}
