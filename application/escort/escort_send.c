/*****************************************************************************************
**                                                                                                                        
**  Copyright (c)  2012,  wsn, Inc.                                                                                 
**        All    Rights Reserved.                                                                                            
**                                                                                                                          
**  Subsystem    : 发送处理                                                                                         
**  File        : escort_send.c                                                                                       
**  Created By    : zhangzhan                                                                                             
**  Created On    : 2014/03/01                                                                                                      
**                                                                                                                         
**  Purpose:                                                                                                             
**        .cpp主要是业务帧的发送处理                               
**                                                                                                                         
**  History:                                                                                                             
**  Version           Programmer     Date         Description    
**  -----------   -----------     ----------    --------------------
**   0.1            zhangzhan    2014/03/01   initial version  
*****************************************************************************************/
#include <string.h>
#include <data_type_def.h>
#include <list.h>
#include <send_list.h>
#include <escort_frame.h>
#include <escort_send_list.h>
#include <hal_board.h>
#include <hal_timer.h>
#include <hal_uart.h>
#include <escort_send.h>
#include <osel_arch.h>
#include <ssn.h>
#include <gprs.h>
#include <pbuf.h>
#include <nfc_module.h>
#include <escort_led.h>
#include <escort_gps.h>
#include <acc_alg.h>
#include <escort_nfc.h>
#include <mac_moniter.h>

#define GPRS_MAX_WAIT_SENDING_TIME      1800000
#define SSN_MAX_WAIT_SENDING_TIME       60000
#define GPRS_MAX_WAIT_ACK_TIME          10000
#define SSN_MAX_WAIT_ACK_TIME           60000

#define MAX_ESCORT_FRAME_LENGTH         42
uint8_t ssn_open_type = 0x00;

extern device_info_t device_info;
extern bool_t escort_gps_is_enabled;
extern bool_t gprs_power_on;
uint8_t escort_send_times = 0;

const uint8_t web_platform_id[8] =
{
    0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
};

typedef struct
{
    uint8_t  result;
    uint8_t  if_ssn_gprs_flag;
    uint16_t frame_id;
    uint16_t tpye;
}data_sent_event_arg_t;

typedef struct
{
    uint8_t len;
    uint8_t data[0];
}data_received_event_arg_t;

typedef enum
{
    ESCORT_SEND_SSN_OPEN,
    ESCORT_SEND_SSN_CLOSE,
}e_ssn_ctrl_type_t;

typedef struct
{
    uint8_t type;
    bool_t res;
}ssn_ctrl_event_arg_t;

extern bool_t escort_acc_is_active;

uint16_t escort_frame_seq = 0;
bool_t escort_gprs_is_enabled = FALSE;
e_escort_data_channel_t escort_current_data_channel = ESCORT_SEND_SSN_CHANNEL;

#define ESCORT_GRPS_WAKEUP_TIME     300000 // 打开GPRS模块所需的时间

static bool_t is_sending = FALSE;
//static hal_timer_t *wait_sending_timer = NULL;
static hal_timer_t *wait_ack_timer = NULL;
static uint16_t sending_frame_id = 0;

//车押2.5 增加解绑后全局标志位
extern uint8_t unbind_flag;

//static void wait_sending_timer_cb(void *arg)
//{
//    wait_sending_timer = NULL;
//    osel_post(ESCORT_WAIT_SENDING_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
//}

static void wait_ack_timer_cb(void *arg)
{
    wait_ack_timer = NULL;
    osel_post(ESCORT_WAIT_ACK_TIMER_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
}

static void ssn_send_cb(uint8_t *data, uint8_t len, uint8_t res, uint8_t mode)
{
    pbuf_t *pbuf;
    data_sent_event_arg_t *arg;
	uint8_t buf[MAX_ESCORT_FRAME_LENGTH];    

    pbuf = pbuf_alloc(sizeof(data_sent_event_arg_t) __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (data_sent_event_arg_t *)pbuf->data_p;
    memcpy(buf, data, len);
    arg->result = res;
    arg->frame_id = (buf[4]<<8)|(buf[5]);
   
	if (RELIABLE_MODE == mode)
	{		
		if ((buf[1] == ESCORT_APP_MESSAGE_REGISTER_TYPE)
                ||((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                    &&(buf[22] == ESCORT_APP_DATA_ACTIVITY_TYPE)
                    &&(buf[23] == ACC_ALG_ACT_TO_STATIC_STATUS)))
		{
			arg->tpye = 0;
		}
		else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                   &&(buf[22] == ESCORT_APP_DATA_DISMANTLE_TYPE))
		{
		  	arg->tpye = 1;
		}
		else if (buf[1] == ESCORT_APP_MESSAGE_UNBIND_TYPE)
		{
		  	arg->tpye = 2;
		}
	}
	else if (UNRELIABLE_MODE == mode)
	{
	  	arg->tpye = 0xffff;
	}
    //由于ssn上位机未做应答，暂时这么修改
//    if ((buf[1] == ESCORT_APP_MESSAGE_REGISTER_TYPE)
//        ||((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
//            &&(buf[22] == ESCORT_APP_DATA_ACTIVITY_TYPE)
//            &&(buf[23] == ACC_ALG_ACT_TO_STATIC_STATUS)))
//    {
//        arg->tpye = 0;
//    }
//    else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
//           &&(buf[22] == ESCORT_APP_DATA_DISMANTLE_TYPE))
//    {
//        arg->tpye = 1;
//    }
//    else if (buf[1] == ESCORT_APP_MESSAGE_UNBIND_TYPE)
//    {
//        arg->tpye = 2;
//    }
//    else
//    {
//        arg->tpye = 0xffff;
//    }
    
    arg->if_ssn_gprs_flag = 1;    
    osel_post(ESCORT_DATA_SENT_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

//static void ssn_receive_cb(uint8_t *data, uint8_t len)
//{
//    pbuf_t *pbuf;
//    data_received_event_arg_t *arg;
//    
//    pbuf = pbuf_alloc(sizeof(data_received_event_arg_t) + len __PLINE1);
//    DBG_ASSERT(pbuf != NULL __DBG_LINE);
//    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
//    
//    arg = (data_received_event_arg_t *)pbuf->data_p;
//    arg->len = len;
//    memcpy(arg->data, data, len);
//    
//    osel_post(ESCORT_DATA_RECEIVED_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
//}

void ssn_open_cb(bool_t res)
{
    pbuf_t *pbuf;
    ssn_ctrl_event_arg_t *arg;
    
    pbuf = pbuf_alloc(sizeof(ssn_ctrl_event_arg_t) __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (ssn_ctrl_event_arg_t *)pbuf->data_p;
    arg->type = ESCORT_SEND_SSN_OPEN;
    arg->res = res;
    
    osel_post(ESCORT_SSN_CONTROL_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

void ssn_close_cb(bool_t res)
{
    pbuf_t *pbuf;
    ssn_ctrl_event_arg_t *arg;
    
    pbuf = pbuf_alloc(sizeof(ssn_ctrl_event_arg_t) __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (ssn_ctrl_event_arg_t *)pbuf->data_p;
    arg->type = ESCORT_SEND_SSN_CLOSE;
    arg->res = res;
    
    osel_post(ESCORT_SSN_CONTROL_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

static void gprs_send_cb(uint16_t param, uint16_t tag,uint16_t type)
{
    pbuf_t *pbuf;
    data_sent_event_arg_t *arg;    
    pbuf = pbuf_alloc(sizeof(data_sent_event_arg_t) __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (data_sent_event_arg_t *)pbuf->data_p;
    if (GS_SEND_OK == param)
    {
        arg->result = TRUE;
    }
    else
    {
        arg->result = FALSE;
    }
    arg->frame_id = tag;
    arg->tpye = type;
    arg->if_ssn_gprs_flag = 0;
    
    osel_post(ESCORT_DATA_SENT_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

static void gprs_receive_cb(gprs_receive_t param)
{
    pbuf_t *pbuf;
    data_received_event_arg_t *arg;
    
    pbuf = pbuf_alloc(sizeof(data_received_event_arg_t) + param.len __PLINE1);
    DBG_ASSERT(pbuf != NULL __DBG_LINE);
    DBG_ASSERT(((uint32_t)pbuf->data_p % sizeof(int)) == 0 __DBG_LINE);
    
    arg = (data_received_event_arg_t *)pbuf->data_p;
    arg->len = param.len;
    memcpy(arg->data, param.gprs_data, param.len);
    
    osel_post(ESCORT_DATA_RECEIVED_EVENT, pbuf, OSEL_EVENT_PRIO_LOW);
}

//static void start_wait_sending_timer(uint32_t time, void *arg)
//{
//    if (NULL != wait_sending_timer)
//    {
//        hal_timer_cancel(&wait_sending_timer);
//        DBG_ASSERT(wait_sending_timer == NULL __DBG_LINE);
//    }
//    HAL_TIMER_SET_REL(MS_TO_TICK(time),
//                      wait_sending_timer_cb,
//                      arg,
//                      wait_sending_timer);
//    DBG_ASSERT(wait_sending_timer != NULL __DBG_LINE);
//}
//
//static void stop_wait_sending_timer(void)
//{
//    if (NULL != wait_sending_timer)
//    {
//        hal_timer_cancel(&wait_sending_timer);
//        DBG_ASSERT(wait_sending_timer == NULL __DBG_LINE);
//    }
//}

static void start_wait_ack_timer(uint32_t time, void *arg)
{
    if (NULL != wait_ack_timer)
    {
        hal_timer_cancel(&wait_ack_timer);
        DBG_ASSERT(wait_ack_timer == NULL __DBG_LINE);
    }
    HAL_TIMER_SET_REL(MS_TO_TICK(time),
                      wait_ack_timer_cb,
                      arg,
                      wait_ack_timer);
    DBG_ASSERT(wait_ack_timer != NULL __DBG_LINE);
}

//static void stop_wait_ack_timer()
//{
//    if (NULL != wait_ack_timer)
//    {
//        hal_timer_cancel(&wait_ack_timer);
//        DBG_ASSERT(wait_ack_timer == NULL __DBG_LINE);
//    }
//}

static void escort_uart_send(uint8_t *buf, uint16_t len)
{
    hal_uart_send_char(HAL_UART_2, 0xD5);
    hal_uart_send_char(HAL_UART_2, 0xC8);
    hal_uart_send_char(HAL_UART_2, (uint8_t)(len >> 8)); //大尾序
    hal_uart_send_char(HAL_UART_2, (uint8_t)len);
    hal_uart_send_string(HAL_UART_2, buf, len);
}

static void escort_send(void)
{
    escort_frame_t *frame;
    uint16_t frame_id;
    uint8_t buf[MAX_ESCORT_FRAME_LENGTH];
    uint8_t len;
    
    if (is_sending)
    {
        _NOP();
        return;
    }
    
    if (escort_current_data_channel == ESCORT_SEND_NO_CHANNEL)
    {
        is_sending = FALSE;
        return;
    }
    
    //先判断当前需要选择的网络
    if (TRUE == ssn_get_status())
    {
        escort_current_data_channel = ESCORT_SEND_SSN_CHANNEL;
    }
    else
    {
        escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
        if (!gprs_power_on)
        {
#if NODE_GPRS_GPS_POWER_SHARE                 
            escort_gprs_gps_device_enable(TRUE);
#else
            escort_gprs_device_enable(TRUE);
#endif
        }            
#if ESCORT_LED_DBUG    
        //指示灯进入数据交互状态
        escort_led_enter_state(ESCORT_LED_DATA_COMMUNICATING);
#endif            
    }  
   
    escort_send_list_fetch(&frame, &frame_id,escort_current_data_channel);
    
    if (NULL == frame)
    {
        is_sending = FALSE;
        if (escort_current_data_channel == ESCORT_SEND_GPRS_CHANNEL)
        {
            if (!escort_gps_is_enabled) //如果GPS已经关闭，则关闭GPRS，此处需要确认
            {
#if NODE_GPRS_GPS_POWER_SHARE                  
                escort_gprs_gps_device_enable(FALSE);
#else
                escort_gprs_device_enable(FALSE);
#endif               
#if ESCORT_LED_DBUG                
                escort_led_exit_state(ESCORT_LED_DATA_COMMUNICATING); //指示灯退出数据交互状态
#endif
            }
        }
        return;
    }

    frame->version = 0x25;
    memcpy(frame->dest_id, web_platform_id, 8);
    memcpy(frame->src_id, device_info.device_id, 8);
    //frame->seq = escort_frame_seq++;
    frame->seq = frame_id;
    
    sending_frame_id = frame_id;
    
    len = escort_get_frame(buf, MAX_ESCORT_FRAME_LENGTH, frame);
    
    if (len == 0)
    {
        return;
    }
  
    // 选择通道发送
    if (escort_current_data_channel == ESCORT_SEND_SSN_CHANNEL)
    {
        // 用SSN发送
	  	if ((buf[1] == ESCORT_APP_MESSAGE_REGISTER_TYPE)
                ||((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                    &&(buf[22] == ESCORT_APP_DATA_ACTIVITY_TYPE)
                    &&(buf[23] == ACC_ALG_ACT_TO_STATIC_STATUS))
				||((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                   &&(buf[22] == ESCORT_APP_DATA_DISMANTLE_TYPE))
				||(buf[1] == ESCORT_APP_MESSAGE_UNBIND_TYPE))
		{
            
            ssn_send(ssn_send_cb, buf, len,RELIABLE_MODE);//RELIABLE_MODE
//            start_wait_sending_timer(SSN_MAX_WAIT_SENDING_TIME, &sending_frame_id);
		}
		else
		{
		  	ssn_send(ssn_send_cb, buf, len,UNRELIABLE_MODE);
			// 设置超时定时器
//			start_wait_sending_timer(SSN_MAX_WAIT_SENDING_TIME, &sending_frame_id);
		}
    }
    else if (escort_current_data_channel == ESCORT_SEND_GPRS_CHANNEL)
    {   //todo 需确认是否是或的关系
        if (escort_send_list_length() > 1 || escort_gps_is_enabled)
        {
            //注册帧和心跳帧TCP发送一次
            if ((buf[1] == ESCORT_APP_MESSAGE_REGISTER_TYPE)
                ||(buf[1] == ESCORT_APP_MESSAGE_HEARTBEAT_TYPE))
            {
                gprs_set_mode(CONTINE_MODE,TRUE,0);
            }
            //恢复静止发送失败延时发送
            else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                     &&(buf[22] == ESCORT_APP_DATA_ACTIVITY_TYPE)
                         &&(buf[23] == ACC_ALG_ACT_TO_STATIC_STATUS))
            {
                gprs_set_mode(CONTINE_MODE,TRUE,3);
            }
            //拆卸帧TCP方式一直发送直到成功
            else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                     &&(buf[22] == ESCORT_APP_DATA_DISMANTLE_TYPE))//解绑帧
            {
                gprs_set_mode(CONTINE_MODE,TRUE,1);
            }
            //注销帧TCP方式发送一次
            else if (buf[1] == ESCORT_APP_MESSAGE_UNBIND_TYPE)
            {
                gprs_set_mode(CONTINE_MODE,TRUE,2);
            }
            else
            {
                gprs_set_mode(CONTINE_MODE,FALSE,0);
            }            
        }
        else
        {
            //注册和心跳TCP发送一次
            if ((buf[1] == ESCORT_APP_MESSAGE_REGISTER_TYPE)
                ||(buf[1] == ESCORT_APP_MESSAGE_HEARTBEAT_TYPE))
            {
                gprs_set_mode(SINGLE_MODE,TRUE,0);
            }
            //恢复静止发送失败延时发送
            else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                     &&(buf[22] == ESCORT_APP_DATA_ACTIVITY_TYPE)
                         &&(buf[23] == ACC_ALG_ACT_TO_STATIC_STATUS))
            {
                gprs_set_mode(SINGLE_MODE,TRUE,3);
            }
            //拆卸帧TCP方式一直发送
            else if ((buf[1] == ESCORT_APP_MESSAGE_APP_TYPE)
                     &&(buf[22] == ESCORT_APP_DATA_DISMANTLE_TYPE))
            {
                gprs_set_mode(SINGLE_MODE,TRUE,1);
            }
            //注销帧TCP方式发送一次
            else if (buf[1] == ESCORT_APP_MESSAGE_UNBIND_TYPE)
            {
                gprs_set_mode(SINGLE_MODE,TRUE,2);
            }
            //进入移动，GPS帧UDP发送一次
            else
            {
                gprs_set_mode(SINGLE_MODE,FALSE,0);
            }              
        }
        
        // 用GPRS发送
        if (TRUE == escort_send_list_is_ack_needed(sending_frame_id))
        {
            gprs_send(buf, len, sending_frame_id, GPRS_MAX_WAIT_ACK_TIME, 1);
        }
        else
        {
            escort_send_times --;
            gprs_send(buf, len, sending_frame_id, 0, escort_send_times);
        }
        // 设置超时定时器
//        start_wait_sending_timer(GPRS_MAX_WAIT_SENDING_TIME, &sending_frame_id);
//        escort_uart_send(buf, len);
    }
    else
    {
        escort_uart_send(buf, len);
//        ssn_send_cb(buf, len, TRUE, UNRELIABLE_MODE);
    }
    
    is_sending = TRUE;
}

// 处理下行数据和应答
static void escort_receive(uint8_t *buf, uint16_t len)
{
}

void escort_wait_sending_timer_event_handle(void *arg)
{
    uint16_t frame_id;
    
    frame_id = *(uint16_t *)arg;
    escort_send_list_sent(frame_id, FALSE,0,0);
    is_sending = FALSE;
    escort_send();
}

void escort_wait_ack_timer_event_handle(void *arg)
{
    uint16_t frame_id;
    
    frame_id = *(uint16_t *)arg;
    escort_send_list_sent(frame_id, FALSE,0,0);
    is_sending = FALSE;
    escort_send();
}

void escort_data_sent_event_handle(void *arg)
{
    pbuf_t *pbuf;
    data_sent_event_arg_t *cb_arg;
    uint8_t ssn_to_gprs_flag = 0;
    uint8_t gprs_to_ssn_flag = 0;
    
    pbuf = (pbuf_t *)arg;
    cb_arg = (data_sent_event_arg_t *)pbuf->data_p;
    
//    if (cb_arg->if_ssn_gprs_flag)//ssn网络发来的数据的回馈
//    {
//        stop_wait_sending_timer();
//    }
    
    if (cb_arg->frame_id < sending_frame_id)
    {
        escort_send_list_sent(cb_arg->frame_id, TRUE,0,0);
        is_sending = FALSE;
        escort_send();  
        pbuf_free(&pbuf __PLINE1);
        DBG_ASSERT(pbuf == NULL __DBG_LINE);    
        return;
    }
    
    if (cb_arg->result == TRUE)
    {       
        if (TRUE == escort_send_list_is_ack_needed(sending_frame_id))
        {
            start_wait_ack_timer(SSN_MAX_WAIT_ACK_TIME, &sending_frame_id);
        }
        else
        {
            escort_send_list_sent(sending_frame_id, TRUE,ssn_to_gprs_flag,gprs_to_ssn_flag);
            is_sending = FALSE;
            escort_send();
        }
        //车押2.5 如果是拆卸帧的确认
        if (cb_arg->tpye == 1)
        {
            //不删除车架号，但设备除了处理解绑操作外不能在处理其他
            escort_led_exit_state(ESCORT_LED_DISMANTLE);
            escort_led_enter_state(ESCORT_LED_INIT);
        }
        else if (cb_arg->tpye == 2)//如果是解绑帧的确认
        {
            unbind_flag = FALSE;
            hal_board_reset();
        }
        
        
#if ESCORT_SEND_DATA_ERR_DBUG
        escort_led_exit_state(ESCORT_LED_DEV_ERR); // 指示灯退出离网状态
#endif
    }
    else
    {
	  	//如果是ssn的FALSE，代表为无网络，这时需要切换为GPRS网络
	  	if (escort_current_data_channel == ESCORT_SEND_SSN_CHANNEL)
		{
		  	escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
#if NODE_GPRS_GPS_POWER_SHARE             
			escort_gprs_gps_device_enable(TRUE);
#else
            escort_gprs_device_enable(TRUE);
#endif
			ssn_to_gprs_flag = 1;
            gprs_to_ssn_flag = 0;            
		}
		else//gprs-->ssn
		{
            escort_current_data_channel = ESCORT_SEND_SSN_CHANNEL;
		  	ssn_to_gprs_flag = 0;
            gprs_to_ssn_flag = 1;            
		}
        escort_send_list_sent(sending_frame_id, FALSE,ssn_to_gprs_flag,gprs_to_ssn_flag);        
        is_sending = FALSE;
        escort_send();
#if ESCORT_SEND_DATA_ERR_DBUG
        escort_led_enter_state(ESCORT_LED_DEV_ERR); // 指示灯进入离网状态
#endif
    }
    
    pbuf_free(&pbuf __PLINE1);
    DBG_ASSERT(pbuf == NULL __DBG_LINE);
}

void escort_data_received_event_handle(void *arg)
{
    pbuf_t *pbuf;
    data_received_event_arg_t *cb_arg;
    
    pbuf = (pbuf_t *)arg;
    cb_arg = (data_received_event_arg_t *)pbuf->data_p;
    
    escort_receive(cb_arg->data, cb_arg->len);
    
    pbuf_free(&pbuf __PLINE1);
    DBG_ASSERT(pbuf == NULL __DBG_LINE);
}

void escort_ssn_ctrl_event_handle(void *arg)
{
    pbuf_t *pbuf;
    ssn_ctrl_event_arg_t *cb_arg;
    
    pbuf = (pbuf_t *)arg;
    cb_arg = (ssn_ctrl_event_arg_t *)pbuf->data_p;
    if (cb_arg->type == ESCORT_SEND_SSN_OPEN)
    {
        if (cb_arg->res == TRUE)
        {
            escort_current_data_channel = ESCORT_SEND_SSN_CHANNEL;
#if NODE_GPRS_GPS_POWER_SHARE              
            escort_gprs_gps_device_enable(FALSE);
#else
            escort_gprs_device_enable(FALSE);
            is_sending = FALSE;
//            escort_send();
            osel_post(ESCORT_SSN_CTRL_SEND_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
#endif
        }
        else
        {
#if ESCORT_USE_DATA_CHANNEL != 2
            escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
    #if NODE_GPRS_GPS_POWER_SHARE  
            escort_gprs_gps_device_enable(TRUE);
    #else
            escort_gprs_device_enable(TRUE);
    #endif
#endif
        }   
        
        if (ssn_open_type == 0x01)//初始打开
        {
            osel_post(ESCORT_INIT_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }
        else if (ssn_open_type == 0x02)//恢复静止打开
        {
            osel_post(ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }
        else if (ssn_open_type == 0x04)//静止心跳时打开
        {
            osel_post(ESCORT_STATIC_HEART_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }    
        else if (ssn_open_type == 0x06)//恢复静止与静止心跳
        {
            osel_post(ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_STATIC_HEART_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }   
        else if (ssn_open_type == 0x08)//强拆打开
        {
            osel_post(ESCORT_DISMANTLE_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }    
        else if (ssn_open_type == 0x09)//初始打开与强拆打开
        {
            osel_post(ESCORT_INIT_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_DISMANTLE_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }
        else if (ssn_open_type == 0x0A)//恢复静止与强拆打开
        {
            osel_post(ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_DISMANTLE_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }     
        else if (ssn_open_type == 0x0C)//静止心跳与强拆打开
        {
            osel_post(ESCORT_STATIC_HEART_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_DISMANTLE_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }      
        else if (ssn_open_type == 0x0E)//恢复静止与静止心跳与强拆打开
        {
            osel_post(ESCORT_ACT_TO_STATIC_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_STATIC_HEART_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
            osel_post(ESCORT_DISMANTLE_OPEN_SSN_EVENT, NULL, OSEL_EVENT_PRIO_LOW);
        }     
     
        ssn_open_type = 0x00;      
    }
    else if(cb_arg->type == ESCORT_SEND_SSN_CLOSE)
    {
#if ESCORT_USE_DATA_CHANNEL != 2
        escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
    #if NODE_GPRS_GPS_POWER_SHARE  
            escort_gprs_gps_device_enable(TRUE);
    #else
            escort_gprs_device_enable(TRUE);
    #endif
#endif
    }
    
    is_sending = FALSE;
    pbuf_free(&pbuf __PLINE1);
    DBG_ASSERT(pbuf == NULL __DBG_LINE);
}

void escort_ssn_ctrl_send_handle(void *arg)
{
    escort_send();
}


void escort_send_request(escort_frame_t *frame, 
                         uint8_t         priority, 
                         uint8_t         allowed_send_times,
                         bool_t          need_ack)
{
    bool_t result;
    
    result = escort_send_list_add(frame, priority, allowed_send_times, 
                                  need_ack);
    DBG_ASSERT(result == TRUE __DBG_LINE);
    
    escort_send_times = allowed_send_times;
    
    escort_send();
}

#define BUILD_IP_ADDRESS(b3, b2, b1, b0) ((uint32_t)(b3) << 24) | \
        ((uint32_t)(b2) << 16) | ((uint32_t)(b1) << 8) | ((uint32_t)(b0))
void escort_send_init(void)
{
    escort_frame_seq = 0;
    escort_gprs_is_enabled = FALSE;
    is_sending = FALSE;
#if ESCORT_USE_DATA_CHANNEL == 0
    escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
#elif ESCORT_USE_DATA_CHANNEL == 1
    escort_current_data_channel = ESCORT_SEND_UART_CHANNEL;
#elif ESCORT_USE_DATA_CHANNEL == 2
    escort_current_data_channel = ESCORT_SEND_NO_CHANNEL;
#elif ESCORT_USE_DATA_CHANNEL == 3
    escort_current_data_channel = ESCORT_SEND_GPRS_CHANNEL;
#else
#error "The configured data channel is wrong."
#endif
//    wait_sending_timer = NULL;
    wait_ack_timer = NULL;
    sending_frame_id = 0;

    escort_send_list_init(15);
    
    // 配置GPRS模块
    gprs_config_t gprs_cfg;
    nfc_ip_t nfc_ip = {0};
    if(FALSE == nfc_read_ip(&nfc_ip))
    {
        gprs_cfg.ip_addr = BUILD_IP_ADDRESS(221, 6, 106, 148);
        gprs_cfg.port = 4321;
//        gprs_cfg.ip_addr = BUILD_IP_ADDRESS(58, 214, 236, 152);
//        gprs_cfg.port = 8051;
    }
    else
    {
        gprs_cfg.ip_addr = BUILD_IP_ADDRESS(nfc_ip.ip[0], nfc_ip.ip[1], 
                                            nfc_ip.ip[2], nfc_ip.ip[3]);
        gprs_cfg.port = nfc_ip.port;
    }
//    gprs_cfg.ip_addr = BUILD_IP_ADDRESS(221, 6, 106, 148);
//    gprs_cfg.port = 4321;
    gprs_cfg.type_cb = gprs_send_cb;
    gprs_cfg.data_cb = gprs_receive_cb;
    gprs_config(&gprs_cfg);
    
#if ESCORT_USE_DATA_CHANNEL == 0 || ESCORT_USE_DATA_CHANNEL == 2
    // 配置SSN模块
    ssn_cb_t ssn_cb;
    ssn_cb.tx_cb = ssn_send_cb;
//    ssn_cb.rx_cb = ssn_receive_cb;
    ssn_cb.open_cb = ssn_open_cb;
    ssn_cb.close_cb = ssn_close_cb;
    ssn_config(&ssn_cb); 
    
    //刚开始指示SSN去寻找网络
   uint8_t monitor_timer_len = 0;
   ssn_open_type |= 0x01;
   monitor_timer_len = mac_moniter_interval_time_get();
   DBG_ASSERT(monitor_timer_len != 0 __DBG_LINE);
   ssn_open(ssn_open_cb, monitor_timer_len);
    
#endif
}
