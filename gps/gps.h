/***************************************************************************
* File        : gps.h
* Summary     : 
* Version     : v0.1
* Author      : xiangwenfang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            xiangwenfang        first version
***************************************************************************/


#ifndef __GPS_H
#define __GPS_H

#include "nmea/nmea.h"

typedef struct
{
    double longitude;       // 经度结果
    double latitude;        // 纬度结果
    double speed;           // 速度结果，单位km/h
}gps_simple_info_t;

typedef struct _location_t
{
	uint16_t deg;
	uint8_t min_dec;
	uint16_t min_fract;
} location_t;

typedef void (*gps_cb_t)(gps_simple_info_t gps_data);

#define GPS_UART            	UART_3

#define GPS_DEBUG           	0
#define GPS_PROTECT_DEBUG     	0
#define GPS_DATA_DEBUG     		0
#define GPS_DEBUG_INFO			0

#define GPS_RECV_LEN_MAX    	128
#define GPS_CMD_LEN_MAX     	100
#define GPS_CR              	0x0D
#define GPS_LF              	0x0A
#define GPS_CRC_PROMPT      	'*'
#define GPS_COMMA      			','
#define GPS_RMC_BLANK_NUM    	12
#define GPS_RMC_LEN_MIN    		7
#define GPS_LAT_LEN				9
#define GPS_LON_LEN				10
#define GPS_DATA_TYPE_LAT		1
#define GPS_DATA_TYPE_LON		2

void gps_init(gps_cb_t gps_cb);

void gps_open(void);

void gps_close(void);

#endif
