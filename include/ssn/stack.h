/***************************************************************************
* File        : stack.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

/**
 * @defgroup STACK STACK - SenStack WSN Protocol Stack
 *
 * @file stack.h
 *
 * This is the main include file of STACK module, one should
 * include this file if intend to use STACK functions.
 */
#ifndef __STACK_H
#define __STACK_H

void ssn_init(void);

/**
 * @}
 */
#endif
