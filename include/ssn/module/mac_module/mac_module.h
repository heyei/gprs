/***************************************************************************
* File        : mac_module.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __MAC_MODULE_H
#define __MAC_MODULE_H

#include <m_tran.h>
#include <m_slot.h>
#include <m_sync.h>
#include <osel_arch.h>

#define M_TRAN_EN                           (1u)
#define M_SLOT_EN                           (1u)
#define M_SYNC_EN                           (1u)

#define SYN_CODE_COMPRESS_EN                (0u) // 压缩SYNC代码
#define TXOK_INT_SIMU_EN                    (0u) // 模拟TXOK中断

#define MODULE_NUM_MAX                      (5u)  // 使能的模块总数
#define MSG_BIND_MODE_NUM                   (22u) // 使能模块使用消息的总数

#define MSG_LOW_PRIO                        OSEL_EVENT_PRIO_LOW
#define MSG_HIGH_PRIO                       OSEL_EVENT_PRIO_HIG

typedef struct _module_bind_event_t_
{
    osel_signal_t sig;
    uint8_t mode_index;
} module_bind_event_t;

typedef void (*task_event_handler)(const osel_event_t *const e);

/**
 * 消息与模块的绑定函数
 *
 *  @handler - 要绑定的模块入口地址
 *  @sig - 要绑定的消息类型
 */
void module_bind_event(task_event_handler handler,  osel_signal_t sig);

/**
 * 主控模块来决定消息发送到哪个模块
 * pmsg - 消息指针
 */
void m_module_ctrl_handler(osel_event_t *pmsg);
extern task_event_handler mac_mode_array[MODULE_NUM_MAX];
extern module_bind_event_t msg_bind_mode_array[MSG_BIND_MODE_NUM];
#endif


