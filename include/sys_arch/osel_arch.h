/***************************************************************************
* File        : osel_arch.h
* Summary     : 
* Version     : v0.1
* Author      : chenggang
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            chenggang        first version
***************************************************************************/

#ifndef __OSEL_ARCH_H
#define __OSEL_ARCH_H

#ifndef NO_SYS
#define NO_SYS                          1
#endif

/* you should implement this API by RTOS, if it has made up, do nothing */
#if (NO_SYS == 0)
// ...
void osel_post(osel_signal_t sig, osel_param_t param, osel_eblock_prio_t event_prio);

#else

#include <wsnos.h>

#define osel_event_t         event_block_t
#endif

#endif