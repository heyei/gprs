/***************************************************************************
* File        : hal_board.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/


#ifndef __HAL_BOARD_H
#define __HAL_BOARD_H

#include <data_type_def.h>
#include <driver.h>
#if (NODE_TYPE == NODE_TYPE_GATEWAY || NODE_TYPE == NODE_TYPE_GATEWAY_MINI)
#include <mac_global_variables.h>
#include <socket.h>
#endif

#define HAL_LED_BLUE          0x01
#define HAL_LED_RED           0x02
#define HAL_LED_GREEN         0x03
#define HAL_LED_POWER         0x04
#define HAL_LED_ERROR         0x05

#define DEVICE_INFO_ADDR                (INFO_B_ADDR)

typedef struct
{
    uint8_t device_id[8];
    uint8_t intra_ch[8];
    uint8_t exter_ch[8];
    uint8_t power_sn;
    uint8_t intra_ch_cnt;
    uint8_t intra_data_simu;        // sensor data simulate or real
    uint32_t heartbeat_cycle;       // heartbeat cycle(s)
#if (NODE_TYPE == NODE_TYPE_ROUTER)
    uint8_t nwk_depth;
#endif
#if (NODE_TYPE == NODE_TYPE_GATEWAY || NODE_TYPE == NODE_TYPE_GATEWAY_MINI)
    supf_spec_t supf_cfg_arg;
    w5100_info_t socket_card;
#endif
    // ...
} device_info_t;


/**
 * LED初始化管脚配置
 *
 * @param: 无
 *
 * @return: 无
 */
void hal_led_init(void);

/**
 * 点亮LED
 *
 * @param color: 需要点亮的LED的颜色
 *
 * @return: 无
 */
void hal_led_open(uint8_t color);

/**
 * 熄灭LED
 *
 * @param color: 需要熄灭的LED的颜色
 *
 * @return: 无
 */
void hal_led_close(uint8_t color);

/**
 * LED反转(亮-->灭  或  灭-->亮)
 *
 * @param color: 需要反转的LED的颜色
 *
 * @return: 无
 */
void hal_led_toggle(uint8_t color);

/**
 * 获取设备短地址
 *
 * @param: 无
 *
 * @return: 设备短地址
 */
uint16_t hal_board_get_device_short_addr(uint8_t id[]);

/**
 * [hal_board_info_get : get board info]
 * @return  [the struct of info temp]
 */
device_info_t hal_board_info_get(void);

/**
 * [hal_board_info_look : just look data from ram, never reload from flash]
 * @return  [description]
 */
device_info_t hal_board_info_look(void);

/**
 * [hal_board_info_clr : clear info flash, struct]
 */
void hal_board_info_clr(void);

/**
 * [hal_board_info_save : ]
 * @param  p_info [pointer to struct ]
 * @return        [TRUE]
 */
bool_t hal_board_info_save(device_info_t *p_info, bool_t flag);


bool_t hal_board_info_delay_save(void);

/**
 * 系统重启
 *
 * @param: 无
 *
 * @return: 无
 */
void hal_board_reset(void);

/**
 * 板级初始化，初始化系统时钟，外围设备等
 *
 * @param: 无
 *
 * @return: 无
 */
void hal_board_init(void);

void hal_board_info_init(void);

bool_t hal_hex_to_ascii(uint8_t *buf, uint8_t dat);
bool_t hal_ascii_to_hex(uint8_t hi, uint8_t lo, uint8_t *hex);
#endif
