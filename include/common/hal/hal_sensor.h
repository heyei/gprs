/***************************************************************************
* File        : hal_sensor.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef __HAL_SENSOR_H__
#define __HAL_SENSOR_H__

#include <data_type_def.h>

#define     DETECTOR_TYPE_DHT                   (0x01u)
#define     DETECTOR_TYPE_ACCELERATED           (0x02u)

typedef enum {
    SENSOR_DHT,
    SENSOR_ACCE,
} snesor_type_t;

#define     DHT_SENSOR_TYPE_T                   (0x00)  // temperature
#define     DHT_SENSOR_TYPE_H                   (0x01)  // humidity


bool_t hal_sensor_read(void *sensor_data_p, uint8_t sensor_type, uint8_t type);

void hal_sensor_init(void);

#endif
