/***************************************************************************
* File        : hal_acc_sensor.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef __HAL_ACC_SENSOR_H
#define __HAL_ACC_SENSOR_H

typedef enum
{
    ACC_SUCCESS,
    ACC_FAIL,
    
    ACC_NORMAL_OPERATION_MODE,
    ACC_LOW_POWER_OPERATION_MODE,
    
    ACC_AUTO_SLEEP_STATUS,//这个功能需要与链接位一起设置
    ACC_MEASURE_STATUS,//测量状态
    ACC_SLEEP_STATUS,
    ACC_STANDBY_STATUS,//待机状态
    
    ACC_INT_EVENT_TYPE,
    ACC_SINGLE_TAP_EVENT_TYPE,
    ACC_DOUBLE_TAP_EVENT_TYPE,
    ACC_ACTIVITY_EVENT_TYPE,
    ACC_INACTIVITY_EVENT_TYPE,
    ACC_FREE_FALL_EVENT_TYPE,
}e_acc_t;

//中断类型
typedef enum
{
    ACC_DATA_READY = 100,
    ACC_SINGLE_TAP,
    ACC_DOUBLE_TAP,
    ACC_ACTIVITY,
    ACC_INACTIVITY,
    ACC_FREE_FALL,
    ACC_WATERMARK,
    ACC_OVERRUN,
    ACC_FIFO_TRIG,
}e_acc_int_type_t;



// 加速度芯片初始化, 完成引脚设置，中断配置
// 参数: 无
// 返回: TRUE,  初始化成功
//       FALSE, 初始化失败
bool_t acc_init(void);

// 设置偏移值
bool_t acc_set_offset(int16_t ofsx,
                                    int16_t ofsy,
                                    int16_t ofsz);


// 设置功耗模式
// 参数：
//     power_mode 功耗模式
//         ACC_NORMAL_OPERATION_MODE
//         ACC_LOW_POWER_OPERATION_MODE
bool_t acc_set_power_mode(uint8_t power_mode);

// 设置工作状态
// 参数：
//     status
//         ACC_AUTO_SLEEP_STATUS
//         ACC_MEASURE_STATUS
//         ACC_SLEEP_STATUS
bool_t acc_set_status(uint8_t status);

// 设置自测试状态
// 参数：
//     enable
//         TRUE, 使能自测试
//         FALSE 禁用自测试
bool_t acc_enable_self_test(bool_t enabled);

typedef struct
{
    uint8_t thresh_tap;
    uint8_t dur;
    uint8_t latent;
    uint8_t window;
}acc_tap_values_t;
// 设置敲击检测的参数
void acc_set_tap_values(const acc_tap_values_t *tap_values);

typedef struct
{
    uint8_t thresh_act;
    uint8_t thresh_inact;
    uint8_t time_inact;
}acc_act_values_t;
// 设置活动检测的参数
void acc_set_act_values(const acc_act_values_t *act_values);

typedef struct
{
    uint8_t thresh_ff;
    uint8_t time_ff;
}acc_ff_values_t;
// 设置自由落体检测的参数
void acc_set_ff_values(const acc_ff_values_t *ff_values);

typedef struct
{
    uint8_t single_tap : 1;
    uint8_t double_tap : 1;
    uint8_t activity   : 1;
    uint8_t inactivity : 1;
    uint8_t free_fall  : 1;
}acc_detect_t;

typedef struct
{
    uint8_t type; // ACC_INT_EVENT_TYPE, ACC_SINGLE_TAP_EVENT_TYPE, ACC_DOUBLE_TAP_EVENT_TYPE,
                  // ACC_ACTIVITY_EVENT_TYPE, ACC_FREE_FALL_EVENT_TYPE,
}acc_event_t;

typedef void (*acc_event_callback_t)(const acc_event_t *acc_event);

typedef struct
{
    uint8_t      data_rate;       //数据速率
    uint8_t      power_mode;      //功耗模式
    acc_detect_t enabled_detects; //使能的检测功能
    uint8_t      range;           //测量范围
    acc_event_callback_t acc_cb;  //通知事件的回调函数
}acc_config_t;

// 配置加速度计主要工作参数
// 参数:
//        config 加速度计主要工作参数
void acc_config(const acc_config_t *config);

// 处理中断, 这个函数被APP调用, 在这个函数中会调用acc_cb
void acc_process_int_event();

typedef struct
{
    int16_t x;
    int16_t y;
    int16_t z;
}acc_data_t;
// 读取加速度数据
// 参数:
//     acc_data_buf 用来存放读取的加速度数据
//     buf_max_size 在acc_data_buf中最多可以存放的数据条数
uint8_t acc_get_detect_data(acc_data_t acc_data_buf[],
                            uint8_t    max_size);

#endif

/*
函数被使用的情形如下：
1.
void app_init()
{
    ...
    acc_init();
    acc_set_tap_values();
    acc_set_act_values();
    acc_set_ff_values();
    acc_config();
    acc_set_status();
}
2.
a. 在加速度中断处理中，调用acc_cb(ACC_INT_EVENT_TYPE)来通知APP；
b. acc_cb中，APP向自己发送OS消息；
c. APP收到消息后，调用acc_process_int_event()；
d. 在acc_process_int_event()中，如果判断发生了敲击等事件，再调用acc_cb()通知APP。
3.
收到配置指令会分别调用acc_set_tap_values(), ..., acc_enable_self_test()等函数。
*/
