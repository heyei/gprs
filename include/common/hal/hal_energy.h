/***************************************************************************
* File        : hal_energy.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef __HAL_ENERGY_H
#define __HAL_ENERGY_H
     
#include <data_type_def.h>

#define HAL_CHIP_ENTER_CPU_IDLE         0u
#define HAL_CHIP_POWER_MODE_1           1u
#define HAL_CHIP_POWER_MODE_2           2u
#define HAL_CHIP_POWER_MODE_3           3u

void hal_energy_init(void);

uint8_t hal_energy_get(void);

#endif

