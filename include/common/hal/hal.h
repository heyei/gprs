/***************************************************************************
* File        : hal.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

#ifndef __HAL_H
#define __HAL_H

#include <hal_board.h>
#include <hal_clock.h>
#include <hal_rf.h>
#include <hal_timer.h>
#include <hal_uart.h>
#include <hal_nvmem.h>
#include <hal_gpio.h>
#include <hal_energy.h>


typedef uint8_t hal_int_state_t;

#if (((__TID__ >> 8) & 0x7F) == 0x2b)     /* 0x2b = 43 dec */
#define HAL_ENTER_CRITICAL(x)                   \
do                                              \
{                                               \
    if (__get_SR_register()&GIE)                \
    {                                           \
        x = 1;                                  \
        _DINT();                                \
    }                                           \
    else                                        \
    {                                           \
        x = 0;                                  \
    }                                           \
} while(__LINE__ == -1)

#define HAL_EXIT_CRITICAL(x)    st(if ((x) == 1) _EINT();)

#else
#define HAL_ENTER_CRITICAL(x _DBG_LINE_)                   \
do                                              \
{                                               \
    if (EA == 1)                                \
    {                                           \
        x = 1;                                  \
        EA = 0;                                 \
    }                                           \
    else                                        \
    {                                           \
        x = 0;                                  \
    }                                           \
} while(__LINE__ == -1)

#define HAL_EXIT_CRITICAL(x)    st(if ((x) == 1) EA = 1;)
#endif



#include <debug.h>

#endif
