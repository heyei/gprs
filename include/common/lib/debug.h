/***************************************************************************
* File        : debug.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

/**
 * 1.使用调试工具前将CFG_DEBUG_EN设置 1,RELEASE设置 0
 * 2.提供debug_move_out_queue: DEBUG 信息出队列接口
 * 3.提供DBG_ASSERT: DEBUG调试函数
 * 4.提供DBG_DATA_PRINT: 普通调试信息输出
 *
 * @file
 *
 *
 *
 * Created on 2011年7月20日, 上午11:08
 */
#ifndef __DEBUG_H
#define __DEBUG_H

#include <assert.h>

#include <data_type_def.h>
#include <node_cfg.h>

#define DEBUG_INFO_ADDR		(INFO_C_ADDR)
/**
 * 调试等级设置,只有消息与更高水平
 */
extern uint8_t global_debug_level;
bool_t debug_enter_queue(uint8_t *string, uint8_t len);
void debug_sqqueue_init();

#if (((__TID__ >> 8) & 0x7F) == 0x2b)     /* 0x2b = 43 dec */
#define DISABLE_INT()           __disable_interrupt()
#else
#include <iocc2530.h>
#define DISABLE_INT()           (EA = 0)
#endif

#if DEBUG_INFO_PRINT_EN > 0

#define _DBG_LINE_      , uint16_t line
#define __DBG_LINE      , __LINE__

#define DBG_PRINT_INFO(c)

#else

/*形参*/
#define _DBG_LINE_  	, uint16_t line
/*实参*/
#define __DBG_LINE  	, __LINE__

#define DBG_PRINT_INFO(c)

#endif

void DBG_ASSERT(bool_t cond _DBG_LINE_);

uint16_t debug_get_info(void);

void debug_clr_info(void);

void debug_delay_clr_info(void);


#endif


