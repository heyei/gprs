/***************************************************************************
* File        : tfp_printf.h
* Summary     : 
* Version     : v0.1
* Author      : zhangzhan
* Date        : 2015/5/6
* Change Logs :
* Date       Versi on     Author     Notes
* 2015/5/6         v0.1            zhangzhan        first version
***************************************************************************/

/*
使用说明
1.精简printf和sprintf函数，实现和标准库函数相似的功能
2.仅支持以下类型
    d       16位有符号整型
    u       16位无符号整型
    c       8位字符型
    s       字符指针
    x,X     16位整型，16进制
    ld      长整型，需要PRINTF_LONG_SUPPORT宏支持
3.使用之前需要创建一个putc函数，例如
    void putc(void* p, char c)
    {
        UCA0TXBUF = c;
        while(!(UCA0IFG & UCTXIFG));
    }
    或
    void putc(void* p, char c)
    {
        uart_send_char(UART_1, c)
    }
4.模块初始化，建议使用以下代码
    wsnos_init_printf(NULL, putc);
    putc可以为其他功能相似的函数
    若仅使用wsnos_sprintf 初始化时可使用 wsnos_init_printf(NULL, NULL)
5.printf sprintf 函数和标准C语言库用法相似。
*/
#ifndef __TFP_PRINTF
#define __TFP_PRINTF

#include <stdarg.h>

#define PRINTF_LONG_SUPPORT     // 支持长整形

void tfp_init(void* putp, void (*putf)(void*,char));

void tfp_printf(char *fmt, ...);
void tfp_sprintf(char* s, char *fmt, ...);

void tfp_format(void* putp, void (*putf)(void*,char), char *fmt, va_list va);

#define     wsnos_init_printf   tfp_init
#define     wsnos_printf        tfp_printf
#define     wsnos_sprintf       tfp_sprintf

#endif



