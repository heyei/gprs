#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(escort_app);
}

static char *argstring[] =
{
	"main",
	"-v",
};

int main(int argc, char *argv[])
{
	argc = sizeof(argstring) / sizeof(char *);
	argv = argstring;

	return UnityMain(argc, argv, RunAllTests);
}
