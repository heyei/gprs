#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

TEST_GROUP_RUNNER(acc_alg_test)
{
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_01);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_02);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_03);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_04);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_05);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_06);
    RUN_TEST_CASE(acc_alg_test, acc_alg_test_07);
}