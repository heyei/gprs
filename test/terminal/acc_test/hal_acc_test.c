#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include <sensor_accelerated.h>
#include <hal_acc_sensor.h>

TEST_GROUP(hal_acc_test);

TEST_SETUP(hal_acc_test)
{
    acc_init();
    
    printf("test setup...\r\n");
}

TEST_TEAR_DOWN(hal_acc_test)
{
    printf("test tear down...\r\n");
}

//测试bool_t acc_set_power_mode(uint8_t power_mode)函数
TEST(hal_acc_test, hal_acc_test_01)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    
    //1初始设置为正常模式 ,预期设置为低电压模式
    oldreg = 0x00;
    adxl345_write_register(ADXL345_BW_RATE, oldreg);
    retvalue = acc_set_power_mode(ACC_LOW_POWER_OPERATION_MODE);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_BW_RATE,&newreg);
    DBG_ASSERT(0x10 == newreg __DBG_LINE);
    
    //2初始设置为低电压模式，预期设置为正常模式
    oldreg = 0x10;
    adxl345_write_register(ADXL345_BW_RATE, oldreg);
    retvalue = acc_set_power_mode(ACC_NORMAL_OPERATION_MODE);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_BW_RATE,&newreg);
    DBG_ASSERT(0x00 == newreg __DBG_LINE);       
}
//测试bool_t acc_set_offset(int16_t ofsx,int16_t ofsy,int16_t ofsz)函数
TEST(hal_acc_test, hal_acc_test_02)
{
    uint8_t retvalue = 0;
    int16_t ofsx = 0x01;
    int16_t ofsy = 0x02;
    int16_t ofsz = 0x03;
    uint8_t newreg = 0x00;
    
    //设置x,y,z分别为1,2,3,测试读取后是否与预期值相同
    retvalue = acc_set_offset(ofsx,ofsy,ofsz);
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_OFSX,&newreg);
    DBG_ASSERT(0x01 == newreg __DBG_LINE);   
    
    adxl345_read_register(ADXL345_OFSY,&newreg);
    DBG_ASSERT(0x02 == newreg __DBG_LINE); 
    
    adxl345_read_register(ADXL345_OFSZ,&newreg);
    DBG_ASSERT(0x03 == newreg __DBG_LINE);    
}
//测试bool_t acc_set_status(uint8_t status)函数
TEST(hal_acc_test, hal_acc_test_03)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    
    //1初始测量位为0，即待机模式，预期设置为测量模式
    oldreg = 0x00;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_MEASURE_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x08 == newreg __DBG_LINE);   
    
    //2初始测量位为1，预期设置成待机模式
    oldreg = 0x08;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_STANDBY_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x00 == newreg __DBG_LINE);      
    
    //3-1初始link_bit = 0，autosleep_bit = 1；预期设置成autosleep模式
    oldreg = 0x10;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_AUTO_SLEEP_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x30 == newreg __DBG_LINE);  

    //3-2初始link_bit = 0，autosleep_bit = 0；预期设置成autosleep模式
    oldreg = 0x00;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_AUTO_SLEEP_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x30 == newreg __DBG_LINE); 

    //3-3初始link_bit = 1，autosleep_bit = 0；预期设置成autosleep模式
    oldreg = 0x20;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_AUTO_SLEEP_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x30 == newreg __DBG_LINE); 
    
     //3-4初始link_bit = 1，autosleep_bit = 1；预期设置成autosleep模式
    oldreg = 0x30;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_AUTO_SLEEP_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x30 == newreg __DBG_LINE);  
   
    //4初始sleep_bit = 0，预期设置为sleep模式
    oldreg = 0x00;
    adxl345_write_register(ADXL345_POWER_CTL, oldreg);
    retvalue = acc_set_status(ACC_SLEEP_STATUS);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_POWER_CTL,&newreg);
    DBG_ASSERT(0x04 == newreg __DBG_LINE);       
}

//测试bool_t acc_enable_self_test(bool_t enabled)函数
TEST(hal_acc_test, hal_acc_test_04)
{
    uint8_t oldreg = 0x00;
    uint8_t newreg = 0x00;
    uint8_t retvalue = 0;
    
    //初始selftest_bit = 0，预期设置成使能
    oldreg = 0x00;
    adxl345_write_register(ADXL345_DATA_FORMAT, oldreg);
    retvalue = acc_enable_self_test(TRUE);    
    DBG_ASSERT(TRUE == retvalue __DBG_LINE);
    
    adxl345_read_register(ADXL345_DATA_FORMAT,&newreg);
    DBG_ASSERT(0x80 == newreg __DBG_LINE);  
}

//测试void acc_set_tap_values(const acc_tap_values_t *tap_values)函数
TEST(hal_acc_test, hal_acc_test_05)
{
    uint8_t newreg = 0x00;
    acc_tap_values_t tap_values = {0};
    
    tap_values.dur = 0x14;//12.5ms
    tap_values.thresh_tap = 0x40;//4g
    tap_values.latent = 0x10;//20ms
    tap_values.window = 0x40;//80ms
    
    //初始值为以上设置的值，检测预期值
    acc_set_tap_values(&tap_values);
    
    adxl345_read_register(ADXL345_THRESH_TAP,&newreg);
    DBG_ASSERT(0x40 == newreg __DBG_LINE); 

    adxl345_read_register(ADXL345_DUR,&newreg);
    DBG_ASSERT(0x14 == newreg __DBG_LINE); 
    
    adxl345_read_register(ADXL345_LATENT,&newreg);
    DBG_ASSERT(0x10 == newreg __DBG_LINE);
    
    adxl345_read_register(ADXL345_WINDOW,&newreg);
    DBG_ASSERT(0x40 == newreg __DBG_LINE);
}

//测试void acc_set_act_values(const acc_act_values_t *act_values)函数
TEST(hal_acc_test, hal_acc_test_06)
{
    uint8_t newreg = 0x00;
    acc_act_values_t act_values = {0};
    
    act_values.thresh_act = 0x20;//2g
    act_values.thresh_inact = 0x20;//2g
    act_values.time_inact = 0x01;//1s
    
    //初始值为以上设置的值，检测预期值
    acc_set_act_values(&act_values);
    
    adxl345_read_register(ADXL345_THRESH_ACT,&newreg);
    DBG_ASSERT(0x20 == newreg __DBG_LINE);    
    
    adxl345_read_register(ADXL345_THRESH_INACT,&newreg);
    DBG_ASSERT(0x20 == newreg __DBG_LINE);

    adxl345_read_register(ADXL345_TIME_INACT,&newreg);
    DBG_ASSERT(0x01 == newreg __DBG_LINE);    
}

//测试void acc_set_ff_values(const acc_ff_values_t *ff_values)函数
TEST(hal_acc_test, hal_acc_test_07)
{
    uint8_t newreg = 0x00;
    acc_ff_values_t ff_values = {0};
    
    ff_values.thresh_ff = 0x06;
    ff_values.time_ff = 0x28;
    
    //初始值为以上设置的值，检测预期值
    acc_set_ff_values(&ff_values);

    adxl345_read_register(ADXL345_THRESH_FF,&newreg);
    DBG_ASSERT(0x06 == newreg __DBG_LINE);  
  
    adxl345_read_register(ADXL345_TIME_FF,&newreg);
    DBG_ASSERT(0x28 == newreg __DBG_LINE);   
}

//测试void acc_config(const acc_config_t *config)函数
TEST(hal_acc_test, hal_acc_test_08)
{
    uint8_t newreg = 0x00;
    acc_config_t config = {0};
    
    config.data_rate = 0x0A;
    config.enabled_detects.activity = 0x01;
    config.enabled_detects.double_tap = 0x01;
    config.enabled_detects.free_fall = 0x01;
    config.enabled_detects.inactivity = 0x01;
    config.enabled_detects.single_tap = 0x01;
    config.power_mode = ACC_LOW_POWER_OPERATION_MODE;//低功耗 1
    config.range = 0x03;
    
    acc_config(&config);
    
    adxl345_read_register(ADXL345_BW_RATE,&newreg);//data rate和power_mode:D4~~D0  0x1A
    DBG_ASSERT(0x1A == (newreg&0x1A) __DBG_LINE);     

    adxl345_read_register(ADXL345_DATA_FORMAT,&newreg);//range:D1~~D0  0x03
    DBG_ASSERT(0x03 == (newreg&0x03) __DBG_LINE);

    adxl345_read_register(ADXL345_BW_RATE,&newreg);//中断使能:D6~~D2  0x7C
    DBG_ASSERT(0x7C == (newreg&0x7C) __DBG_LINE);        
}








