#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include <acc_sensor_algo.h>
#include <acc_alg.h>

//说明：测试此用例需要打开acc_sensor_algo.c中的#define TEST_ACC_ALG_DEBUG 1

//数组求和函数
static int32_t array_sum_proc_1(const int16_t *parray,uint16_t num)
{
    uint16_t i = 0;
    int32_t sum = 0;
    DBG_ASSERT(NULL != parray __DBG_LINE);

    for (i = 0;i < num;i++)
    {
        sum += *parray;
        parray ++;
    }
    return sum;
}
//数组求最大值函数
static int16_t array_max_proc_1(int16_t *parray,uint16_t num)
{
    int16_t max = 0;
    int16_t *p = NULL;
    DBG_ASSERT(NULL != parray __DBG_LINE);

    for (max = *parray,p = parray;p < (parray + num);p++)
    {
        if (*p >= max)
        {
            max = *p;
        }
    }

    return max;
}

//数组求最小值函数
int16_t array_min_proc_1(int16_t *parray,uint16_t num)
{
    int16_t min = 0;
    int16_t *p = NULL;
    DBG_ASSERT(NULL != parray __DBG_LINE);

    for (min = *parray,p = parray;p < (parray + num);p++)
    {
        if (*p <= min)
        {
            min = *p;
        }
    }

    return min;
}


TEST_GROUP(acc_alg_test);

TEST_SETUP(acc_alg_test)
{

    printf("test setup...\r\n");
}

TEST_TEAR_DOWN(acc_alg_test)
{
    printf("test tear down...\r\n");
}

//测试中值处理3个数中取中值
TEST(acc_alg_test, acc_alg_test_01)
{
    int16_t a = 0;
    int16_t b = 0;
    int16_t c = 0;
    int16_t retvalue = 0;

    //step#1
    a = 3;
    b = 4;
    c = 5;
    retvalue = acc_sensor_algo_median_proc(a,b,c);
    TEST_ASSERT_EQUAL_INT16(4,retvalue);
    //step#2
    a = 3;
    b = 3;
    c = 3;
    retvalue = acc_sensor_algo_median_proc(a,b,c);
    TEST_ASSERT_EQUAL_INT16(3,retvalue);
    //step#3
    a = -3;
    b = 0;
    c = 3;
    retvalue = acc_sensor_algo_median_proc(a,b,c);
    TEST_ASSERT_EQUAL_INT16(0,retvalue);

}

//测试合加速度计算
TEST(acc_alg_test, acc_alg_test_02)
{
//    int16_t retvalue = 0;
//
//    retvalue = acc_sensor_algo_combine_proc();
//    TEST_ASSERT_EQUAL_INT16(14,retvalue);

}

//测试计算方差函数
TEST(acc_alg_test, acc_alg_test_03)
{
    //int16_t retvalue = 0;
    combine_acc_buffer_t pacc_buff[10] = {{0,0,0,0},{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4},
                                   {1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4},{1,2,3,4},};
    variance_t acc_v_1 = {0,0,0,0};

    //平均值v=1.5,x=2,y=2.5,z=3
    acc_v_1 = acc_sensor_algo_variance_calc_proc(pacc_buff);
    DBG_ASSERT(1.25 == acc_v_1.v_combine_acc __DBG_LINE);
    DBG_ASSERT(1.0 == acc_v_1.v_x __DBG_LINE);
    DBG_ASSERT(1.25 == acc_v_1.v_y __DBG_LINE);
    DBG_ASSERT(2.0 == acc_v_1.v_z __DBG_LINE);

}

//测试矩阵处理函数
TEST(acc_alg_test, acc_alg_test_04)
{
    src_data_buffer_t turn_buf[3] = {{4,5,6},{7,8,9},{10,11,12}};
    uint16_t buf_len = 3;
    int16_t parray[3] = {1,2,3};

    matrix_proc(turn_buf,buf_len,parray);

    //通过函数内部看结果x=32,y=50,z=68
}

//测试数组求和函数
TEST(acc_alg_test, acc_alg_test_05)
{
    int16_t retvalue = 0;
    uint16_t buf_num = 5;
    int16_t parray[5] = {1,2,3,-6,20};

    retvalue = array_sum_proc_1(parray,buf_num);
    TEST_ASSERT_EQUAL_INT16(20,retvalue);
}

//测试数组求最大值函数
TEST(acc_alg_test, acc_alg_test_06)
{
    int16_t retvalue = 0;
    uint16_t buf_num = 5;
    int16_t parray[5] = {30,2,3,-6,20};

    retvalue = array_max_proc_1(parray,buf_num);
    TEST_ASSERT_EQUAL_INT16(30,retvalue);
}

//测试数组求最大值函数
TEST(acc_alg_test, acc_alg_test_07)
{
    int16_t retvalue = 0;
    uint16_t buf_num = 5;
    int16_t parray[5] = {30,2,3,-6,20};

    retvalue = array_min_proc_1(parray,buf_num);
    TEST_ASSERT_EQUAL_INT16(-6,retvalue);
}





