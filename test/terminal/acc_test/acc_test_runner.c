#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

TEST_GROUP_RUNNER(acc_test)
{
    osel_init();
    hal_board_init();

    RUN_TEST_CASE(acc_test, acc_test_single_read);
    RUN_TEST_CASE(acc_test, acc_test_single_write);
    RUN_TEST_CASE(acc_test, acc_test_multi_read);
}