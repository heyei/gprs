#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include <sensor_accelerated.h>
#include <hal_acc_sensor.h>

TEST_GROUP(acc_test);

TEST_SETUP(acc_test)
{
    //P5.4 作为模拟scl，输出9个信号
    P5SEL &= ~BIT4;// P5.4置成IO口模式
	P5DIR |= BIT4; //P5.4做为输出
    P5OUT |= BIT4;
    // 主设备模拟SCL，从高到低，输出9次，使得从设备释放SDA
    for(uint8_t i=0;i<9;i++)
    {
        P5OUT |= BIT4;
        delay_ms(1);
        P5OUT &= ~BIT4;
        delay_ms(1);
    }
    
    P3SEL |= BIT7;
    P5SEL |= BIT4;
    P5DIR |= BIT4;

    UCB1CTL1 |= UCSWRST;                    //软件复位使能
    UCB1CTL0 = UCMST + UCMODE_3 + UCSYNC ;  // I2C主机模式
    UCB1CTL1 |= UCSSEL_2;                   // 选择SMCLK
    UCB1BR0 = 80;                          // I2C时钟约 100hz，F(i2c)=F(smclk)/BR,smclk=8m
    UCB1BR1 = 0;
    UCB1CTL0 &= ~UCSLA10;                   // 7位地址模式
    //UCB1I2CSA = ADXL345_ADDRESS;            // ADXL345
    UCB1CTL1 &= ~UCSWRST;                   //软件清除UCSWRST可以释放USCI,使其进入操作状态 
    
    printf("test setup...\r\n");
}

TEST_TEAR_DOWN(acc_test)
{
    printf("test tear down...\r\n");
}

//测试单字节读函数
TEST(acc_test, acc_test_single_read)
{
    uint8_t deviceid = 0x00;
    uint8_t retid = I2C_FAIL;               // 读写返回值

    //测试单字节读函数
    retid = adxl345_read_register(0x00, &deviceid);
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);
    DBG_ASSERT(deviceid == 0xE5 __DBG_LINE);
}
//测试单字节写函数
TEST(acc_test, acc_test_single_write)
{
    uint8_t regvalue = 0x00;
    uint8_t retid = I2C_FAIL;               // 读写返回值
    retid = adxl345_write_register(0x31,0x0B);  // 选择一个测试寄存器
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);

    retid = adxl345_read_register(0x31, &regvalue);// 再次读取该测试寄存器内容
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);
    DBG_ASSERT(0x0B == regvalue __DBG_LINE);
}
//测试多字节读函数
TEST(acc_test, acc_test_multi_read)
{
    uint8_t retid = I2C_FAIL;               // 读写返回值
    uint8_t ofsx = 0x01;
    uint8_t ofsy = 0x02;
    uint8_t ofsz = 0x03;
    uint8_t reg_buf2[3] = { 0x00 , 0x00 , 0x00 };
    
    adxl345_write_register(0x1E,ofsx);
    adxl345_write_register(0x1F,ofsy);
    adxl345_write_register(0x20,ofsz);

    retid = adxl345_read_buff( 0x1E , reg_buf2 , 3);
    DBG_ASSERT(I2C_OK == retid __DBG_LINE);
    
    DBG_ASSERT((reg_buf2[0] == 0x01) __DBG_LINE);
    DBG_ASSERT((reg_buf2[1] == 0x02) __DBG_LINE);
    DBG_ASSERT((reg_buf2[2] == 0x03) __DBG_LINE);
}
















