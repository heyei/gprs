#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

TEST_GROUP_RUNNER(hal_acc_test)
{
    osel_init();
    hal_board_init();

    RUN_TEST_CASE(hal_acc_test, hal_acc_test_01);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_02);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_03);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_04);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_05);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_06);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_07);
    RUN_TEST_CASE(hal_acc_test, hal_acc_test_08);
    
}