#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include "tfp_printf.h"

TEST_GROUP(tfp_printf_test);

TEST_SETUP(tfp_printf_test)
{
    // ��ʼ������
    wsnos_init_printf(NULL, NULL);
}

TEST_TEAR_DOWN(tfp_printf_test)
{
    ;
}

TEST(tfp_printf_test, tfp_printf_test_d)
{
    char str[6];
    int16_t num1 = 123;
    memset(str, 0x00, sizeof str);
    tfp_sprintf(str, "%d", num1);
    TEST_ASSERT_EQUAL_STRING("123", str);

    int16_t num2 = -1;
    memset(str, 0x00, sizeof str);
    tfp_sprintf(str, "%d", num2);
    TEST_ASSERT_EQUAL_STRING("-1", str);
}

TEST(tfp_printf_test, tfp_printf_test_u)
{
    char str[6];
    uint16_t num = 65535;
    memset(str, 0x00, sizeof str);

    tfp_sprintf(str, "%u", num);
    TEST_ASSERT_EQUAL_STRING("65535", str);
}

TEST(tfp_printf_test, tfp_printf_test_c)
{
    char str[6];
    memset(str, 0x00, sizeof str);

    char chr = 'A';
    tfp_sprintf(str, "%c", chr);
    TEST_ASSERT_EQUAL_STRING("A", str);
}

TEST(tfp_printf_test, tfp_printf_test_s)
{
    char str[20];
    memset(str, 0x00, sizeof str);

    tfp_sprintf(str, "Hello %s\r\n", "World");
    TEST_ASSERT_EQUAL_STRING("Hello World\r\n", str);
}

TEST(tfp_printf_test, tfp_printf_test_x)
{
    char str[20];
    memset(str, 0x00, sizeof str);

    uint16_t num = 0xffff;
    tfp_sprintf(str, "%x", num);
    TEST_ASSERT_EQUAL_STRING("ffff", str);
}

TEST(tfp_printf_test, tfp_printf_test_X)
{
    char str[20];
    memset(str, 0x00, sizeof str);

    uint16_t num = 0xFFFF;
    tfp_sprintf(str, "%X", num);
    TEST_ASSERT_EQUAL_STRING("FFFF", str);
}


