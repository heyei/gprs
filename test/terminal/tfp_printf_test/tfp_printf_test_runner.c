#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

#include "tfp_printf.h"

TEST_GROUP_RUNNER(tfp_printf_test)
{
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_d);
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_u);
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_c);
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_s);
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_x);
    RUN_TEST_CASE(tfp_printf_test, tfp_printf_test_X);
}
