#include <string.h>
#include <data_type_def.h>
#include <unity_fixture.h>
#include <osel_arch.h>
#include <escort_app.h>
#include <acc_alg.h>
#include <gprs.h>
#include <ssn.h>
#include <gps.h>
#include <escort_dismantle.h>
#include <hal_board.h>
#include <pbuf.h>
#include <sbuf.h>
#include <crc8.h>
#include <crc.h>

#define UINT16_TO_BUF(n, buf, little) \
    if (little) \
	{ \
	    (buf)[0] = (uint8_t)(n); \
		(buf)[1] = (uint8_t)((n) >> 8); \
	} \
	else \
	{ \
	    (buf)[0] = (uint8_t)((n) >> 8); \
		(buf)[1] = (uint8_t)(n); \
	}
	
#define FLOAT32_TO_BUF(f, buf, little) \
{ \
    fp32_t _float32_to_buf_f = (f); \
    if (little) \
	{ \
	    (buf)[0] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
		(buf)[1] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
		(buf)[2] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
		(buf)[3] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
	} \
	else \
	{ \
	    (buf)[0] = ((uint8_t *)(&_float32_to_buf_f))[3]; \
		(buf)[1] = ((uint8_t *)(&_float32_to_buf_f))[2]; \
		(buf)[2] = ((uint8_t *)(&_float32_to_buf_f))[1]; \
		(buf)[3] = ((uint8_t *)(&_float32_to_buf_f))[0]; \
	} \
}

extern acc_alg_config_t acc_alg_current_config;

extern gprs_config_t gprs_current_config;
extern uint8_t gprs_current_data[128];
extern uint16_t gprs_current_data_len;
extern uint16_t gprs_current_data_tag;
extern uint16_t gprs_current_wait_ack_time;
extern gprs_mode_t gprs_current_mode;

extern ssn_cb_t ssn_current_config;
extern uint8_t ssn_current_data[128];
extern uint16_t ssn_current_data_len;

extern gps_receive_cb_t gps_receive_callback;
extern gps_control_cb_t gps_control_callback;
extern uint16_t gps_data_period;
extern uint8_t gps_current_control_type;

extern device_info_t device_info;

extern uint16_t escort_frame_seq;

#if ESCORT_USE_UART_DATA_CHANNEL == 1
    #error "Should not use uart data channel in this test."
#endif

const uint8_t device_id[8] =
{
    0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA
};

TEST_GROUP(escort_app);

TEST_SETUP(escort_app)
{
    osel_init();
    
    pbuf_init();
    bool_t res = sbuf_init();
    DBG_ASSERT(res != FALSE __DBG_LINE);
    
    memcpy(device_info.device_id, device_id, 8);
	escort_app_init();
}

TEST_TEAR_DOWN(escort_app)
{
    
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生超限事件;
// 3) 业务流程应该用SSN通道发送超限消息帧;
// 4) 当前SSN通道无法使用, 切换到GPRS通道;
// 5) SSN发送超限消息帧失败;
// 6) 算法模块产生超限事件;
// 7) 业务流程应该用GPRS通道发送超限消息帧.
TEST(escort_app, overrun_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x01, 0x01, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x00, 0x00, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);    
    acc_alg_event_t acc_alg_event;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    acc_alg_event.type = ACC_ALG_OVERRUN_EVENT_TYPE;
    acc_alg_event.overrun_args.overrun_flg = 0x01;
    acc_alg_event.overrun_args.acc_x = 0x0102;
    acc_alg_event.overrun_args.acc_y = 0x0102;
    acc_alg_event.overrun_args.acc_z = 0x0102;
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
    
    escort_frame_seq = 0;
    
    // 设置当前为GPRS通道
    ssn_current_config.open_cb(FALSE);
    osel_schedule();
    
    // SSN发送失败
    ssn_current_config.tx_cb(NULL, 0, FALSE);
    osel_schedule();
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, gprs_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, gprs_current_data, expected_length);
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生静动变化事件(熄火);
// 3) 业务流程应该用SSN通道发送静动变化消息帧.
TEST(escort_app, activity_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x02, 0x0D, 0x04, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);    
    acc_alg_event_t acc_alg_event;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    acc_alg_event.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
    acc_alg_event.activity_args.activity_status_type = ACC_ALG_FLAMEOUT_STATUS;
    acc_alg_event.activity_args.feature_dimen = 04;
    acc_alg_event.activity_args.flameout_feature.v = 0x0102;
    acc_alg_event.activity_args.flameout_feature.v_x = 0x0102;
    acc_alg_event.activity_args.flameout_feature.v_y = 0x0102;
    acc_alg_event.activity_args.flameout_feature.v_z = 0x0102;
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生静动变化事件(正常行驶);
// 3) 业务流程应该启动GPS模块;
// 4) GPS模块通知GPS数据;
// 5) 业务流程应该用SSN通道发送GPS消息帧.
TEST(escort_app, gps_data_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x06, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);
    acc_alg_event_t acc_alg_event;
    fp32_t expected_longitude = 112.112;
    fp32_t expected_latitude = 23.23;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    FLOAT32_TO_BUF(expected_longitude, expected_frame + 23, FALSE);
    FLOAT32_TO_BUF(expected_latitude, expected_frame + 27, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    gps_receive_callback = NULL;
    
    // 运动状态
    acc_alg_event.type = ACC_ALG_ACTIVITY_EVENT_TYPE;
    acc_alg_event.activity_args.activity_status_type = ACC_ALG_CAR_NORMAL_DRIVE_STATUS;
    acc_alg_event.activity_args.feature_dimen = 04;
    acc_alg_event.activity_args.normal_derive_feature.c1 = 0x0102;
    acc_alg_event.activity_args.normal_derive_feature.c2 = 0x0102;
    acc_alg_event.activity_args.normal_derive_feature.c3 = 0x0102;
    acc_alg_event.activity_args.normal_derive_feature.c4 = 0x0102;    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    // SSN发送成功
    ssn_current_config.tx_cb(NULL, 0, FALSE);
    osel_schedule();
    
    // GPS被启动
    TEST_ASSERT_TRUE(gps_receive_callback != NULL);
    
    escort_frame_seq = 0;
    
    gps_simple_info_t gps_data;
    gps_data.longitude = expected_longitude;
    gps_data.latitude = expected_latitude;
    gps_receive_callback(gps_data);
    osel_schedule();
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生跌落事件;
// 3) 业务流程应该用SSN通道发送跌落消息帧.
TEST(escort_app, fall_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x03, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x00, 0x00, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);    
    acc_alg_event_t acc_alg_event;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    acc_alg_event.type = ACC_ALG_FALL_EVENT_TYPE;
    acc_alg_event.fall_args.acc_x = 0x0102;
    acc_alg_event.fall_args.acc_y = 0x0102;
    acc_alg_event.fall_args.acc_z = 0x0102;
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生倾倒事件;
// 3) 业务流程应该用SSN通道发送倾倒消息帧.
TEST(escort_app, topple_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x04, 0x02, 0x01, 0x02, 0x01, 0x02, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);    
    acc_alg_event_t acc_alg_event;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    acc_alg_event.type = ACC_ALG_TOPPLE_EVENT_TYPE;
    acc_alg_event.topple_args.feature_1 = 0x0102;
    acc_alg_event.topple_args.feature_2 = 0x0102;
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}

// 测试用例描述:
// 1) 当前SSN通道可用;
// 2) 算法模块产生转向事件;
// 3) 业务流程应该用SSN通道发送转向消息帧.
TEST(escort_app, turn_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x05, 0x01, 0x01, 0x02, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);    
    acc_alg_event_t acc_alg_event;
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    acc_alg_event.type = ACC_ALG_TURN_EVENT_TYPE;
    acc_alg_event.turn_args.feature_1 = 0x0102;
    
    acc_alg_current_config.acc_alg_cb(&acc_alg_event);
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}

TEST(escort_app, dismantle_event_should_be_sent)
{
    uint8_t expected_frame[] = 
    {
        0x10, 0x07, 
        0xCC, 0xCC,
        0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA,
        0x07, 0xCC,
    };
    uint16_t expected_length = sizeof(expected_frame);
    
    UINT16_TO_BUF(expected_length, expected_frame + 2, FALSE);
    expected_frame[expected_length - 1] = crc_compute(expected_frame + 22, expected_length - 23);
    escort_frame_seq = 0;
    
    // 设置当前为SSN通道
    ssn_current_config.open_cb(TRUE);
    osel_schedule();
    
    escort_trigger_dismantle_event();
    
    TEST_ASSERT_EQUAL_UINT16(expected_length, ssn_current_data_len);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_frame, ssn_current_data, expected_length);
}
