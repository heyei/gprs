#include <data_type_def.h>
#include <gps.h>

gps_receive_cb_t gps_receive_callback = NULL;
gps_control_cb_t gps_control_callback = NULL;
uint16_t gps_data_period;
uint8_t gps_current_control_type;

// GPS获取数据
uint8_t gps_receive(gps_receive_cb_t gps_receive_cb, uint16_t period)
{
    gps_receive_callback = gps_receive_cb;
    gps_data_period = period;
    
    return 0;
}

// GPS模块控制，启动或休眠
uint8_t gps_control(gps_control_cb_t gps_control_cb, uint8_t type)
{
    gps_control_callback = gps_control_cb;
    gps_current_control_type = type;
    
    return 0;
}

// GPS初始化
uint8_t gps_init(void)
{
    return 0;
}
