#include <string.h>
#include <data_type_def.h>
#include "ssn.h"

ssn_cb_t ssn_current_config;
uint8_t ssn_current_data[128];
uint16_t ssn_current_data_len;

void ssn_config(ssn_cb_t *ssn_cb)
{
    ssn_current_config = *ssn_cb;
}

void ssn_open(ssn_open_cb_t open_cb, uint32_t sec)
{
    ssn_current_config.open_cb = open_cb;
    (void)sec;
}

void ssn_close(ssn_close_cb_t close_cb, uint32_t sec)
{
    ssn_current_config.close_cb = close_cb;
    (void)sec;
}

int8_t ssn_send(ssn_tx_cb_t tx_cb, uint8_t *data, uint8_t len)
{
    ssn_current_config.tx_cb = tx_cb;
    ssn_current_data_len = len;
    memcpy(ssn_current_data, data, len);
    
    return len;
}
