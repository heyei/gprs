#include <data_type_def.h>
#include <unity_fixture.h>
#include <osel_arch.h>
#include <ssn.h>
#include <hal.h>

TEST_GROUP_RUNNER(escort_app)
{
    RUN_TEST_CASE(escort_app, overrun_event_should_be_sent);
    RUN_TEST_CASE(escort_app, activity_event_should_be_sent);
    RUN_TEST_CASE(escort_app, gps_data_should_be_sent);
    RUN_TEST_CASE(escort_app, fall_event_should_be_sent);
    RUN_TEST_CASE(escort_app, topple_event_should_be_sent);
    RUN_TEST_CASE(escort_app, turn_event_should_be_sent);
    RUN_TEST_CASE(escort_app, dismantle_event_should_be_sent);
}
