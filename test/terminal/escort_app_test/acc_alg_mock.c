#include <data_type_def.h>
#include "acc_alg.h"

acc_alg_config_t acc_alg_current_config;

void acc_alg_init()
{
}

void acc_alg_set_param(const acc_alg_param_t *param)
{
    (void)param;
}

void acc_alg_config(const acc_alg_config_t *config)
{
    acc_alg_current_config = *config;
}

void acc_alg_new_data(const acc_alg_data_t *acc_data)
{
    (void)acc_data;
}
