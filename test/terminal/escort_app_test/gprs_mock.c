#include <string.h>
#include <data_type_def.h>
#include "gprs.h"

gprs_config_t gprs_current_config;
uint8_t gprs_current_data[128];
uint16_t gprs_current_data_len;
uint16_t gprs_current_data_tag;
uint16_t gprs_current_wait_ack_time;
gprs_mode_t gprs_current_mode;

bool_t gprs_config(const gprs_config_t *config)
{
    gprs_current_config = *config;
    
    return TRUE;
}

void gprs_init(void)
{
}

uint8_t gprs_send(void *data_p, uint8_t len, uint16_t tag, uint16_t time)
{
    gprs_current_data_len = len;
    memcpy(gprs_current_data, data_p, len);
    gprs_current_data_tag = tag;
    gprs_current_wait_ack_time = time;
    
    return 1;
}

void gprs_set_mode(gprs_mode_t sig)
{
    gprs_current_mode = sig;
}

void gprs_uart_recv_handler(uint8_t ch)
{
    (void)ch;
}

