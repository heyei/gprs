#include <data_type_def.h>
#include <hal_acc_sensor.h>

acc_config_t acc_config_1;

bool_t acc_init(void)
{
    return TRUE;
}

// ����ƫ��ֵ
bool_t acc_set_offset(int16_t ofsx,
                      int16_t ofsy,
                      int16_t ofsz)
{
    (void)ofsx;
    (void)ofsy;
    (void)ofsz;
    
    return TRUE;
}

bool_t acc_set_power_mode(uint8_t power_mode)
{
    (void)power_mode;
    
    return TRUE;
}

bool_t acc_set_status(uint8_t status)
{
    (void)(status);
    
    return TRUE;
}

bool_t acc_enable_self_test(bool_t enabled)
{
    (void)enabled;
    
    return TRUE;
}

void acc_set_tap_values(const acc_tap_values_t *tap_values)
{
    (void)tap_values;
}

void acc_set_act_values(const acc_act_values_t *act_values)
{
    (void)act_values;
}

void acc_set_ff_values(const acc_ff_values_t *ff_values)
{
    (void)ff_values;
}

void acc_config(const acc_config_t *config)
{
}

void acc_process_int_event()
{
}

uint8_t acc_get_detect_data(acc_data_t acc_data_buf[],
                            uint8_t    max_size)
{
    (void)acc_data_buf;
    (void)max_size;
    
    return 0;
}
