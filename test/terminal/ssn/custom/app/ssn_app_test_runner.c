#include <unity_fixture.h>
#include <osel_arch.h>
#include <ssn.h>
#include <hal.h>

TEST_GROUP_RUNNER(ssn_app)
{
    osel_init();
    hal_board_init();
    ssn_init();

    RUN_TEST_CASE(ssn_app, ssn_open);
    RUN_TEST_CASE(ssn_app, ssn_send);
    RUN_TEST_CASE(ssn_app, ssn_close);
}



