#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <ssn.h>
#include <data_type_def.h>
#include <hal.h>

static void test_ssn_open_cb(bool_t res)
{
    if(res)
    {
        PRINTF("\r\nopen ok\r\n");
    }
    else
    {
        PRINTF("\r\nopen failed\r\n");
    }
}

static void test_ssn_close_cb(bool_t res)
{
    if(res)
    {
        PRINTF("\r\nclose ok\r\n");
    }
    else
    {
        PRINTF("\r\nclose failed\r\n");
    }
}

static void test_ssn_rx_cb(uint8_t *data, uint8_t len)
{
    ;
}

static void test_ssn_tx_cb(uint8_t *data, uint8_t len, uint8_t res)
{
    if(res)
    {
        PRINTF("\r\nsend ok\r\n");
    }
    else
    {
        PRINTF("\r\nsend failed\r\n");
    }
}

static ssn_cb_t test_ssn_cb;


TEST_GROUP(ssn_app);

TEST_SETUP(ssn_app)
{
    test_ssn_cb.open_cb     = test_ssn_open_cb;
    test_ssn_cb.close_cb    = test_ssn_close_cb;
    test_ssn_cb.rx_cb       = test_ssn_rx_cb;
    test_ssn_cb.tx_cb       = test_ssn_tx_cb;

    ssn_config(&test_ssn_cb);

    _EINT();
}

TEST_TEAR_DOWN(ssn_app)
{
    ;
}


TEST(ssn_app, ssn_open)
{
    ssn_open(test_ssn_open_cb, 1);
    delay_ms(1000*1);
}


TEST(ssn_app, ssn_send)
{
    uint8_t array[] = {1,2,3};
    ssn_send(test_ssn_tx_cb, array, sizeof(array));
}

TEST(ssn_app, ssn_close)
{
    ssn_close(test_ssn_close_cb, 1);
    delay_ms(1*1000);
}
















