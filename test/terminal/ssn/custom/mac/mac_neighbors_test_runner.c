#include <unity_fixture.h>

TEST_GROUP_RUNNER(mac_neighbors)
{
    RUN_TEST_CASE(mac_neighbors, add_one_neighbor);
    RUN_TEST_CASE(mac_neighbors, add_one_neighbor_then_delete);
    RUN_TEST_CASE(mac_neighbors, delete_neighbor_node_id_Not_Exists);
    RUN_TEST_CASE(mac_neighbors, add_one_neighbor_queue_is_full);
    RUN_TEST_CASE(mac_neighbors, add_one_neighbor_queue_is_full_id_Exists);
    RUN_TEST_CASE(mac_neighbors, delete_the_last_node_then_add_queue_is_full);
    RUN_TEST_CASE(mac_neighbors, delete_one_node_then_add_queue_is_full);
    RUN_TEST_CASE(mac_neighbors, update_neighbor_table_lt_LIVE_TIMEOUT);
    RUN_TEST_CASE(mac_neighbors, update_neighbor_table_et_LIVE_TIMEOUT);
    RUN_TEST_CASE(mac_neighbors, update_neighbor_table_more_than_one_node);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch1);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch2);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch3);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch4);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch5);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch6);
    RUN_TEST_CASE(mac_neighbors, mac_get_coord_branch7);
}



