#include <unity_fixture.h>
#include <string.h>
#include <data_type_def.h>
#include <mac_neighbors.h>
#include <node_cfg.h>

#define INVAILD_ID                      0xFFFF
#define INDEX                           16
#define MAX_HOP_NUMBER                  0xFF

#define LIVE_TIMEOUT                    0x0A

TEST_GROUP(mac_neighbors);

TEST_SETUP(mac_neighbors)
{
    mac_neighbor_table_init();
}

TEST_TEAR_DOWN(mac_neighbors)
{
    ;
}

/**
* Test ID: TC_MAC_NEIGHBORS_001
* Test Name: add_one_neighbor
* Description:添加1个节点，添加成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加1个邻居节点，函数返回值为TRUE
*/ 
TEST(mac_neighbors, add_one_neighbor)
{
    neighbor_node_t neighbor;

    neighbor.dev_id     = 0x0001;
    neighbor.rssi       = -80;
    neighbor.hops       = 2;
    neighbor.time_stamp = 0x10000000;
    neighbor.beacon_ind = 0x04;
    neighbor.intra_channel = 0x03;

    uint8_t res = mac_neighbor_node_add(&neighbor);
    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_002
* Test Name: add_one_neighbor_then_delete
* Description:添加1个节点，删除该节点，删除成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加1个邻居节点
* 2) 调用mac_neighbor_node_delete，删除1)中添加的节点，返回TRUE
*/ 
TEST(mac_neighbors, add_one_neighbor_then_delete)
{
    neighbor_node_t neighbor;

    neighbor.dev_id     = 0x0002;
    neighbor.rssi       = -80;
    neighbor.hops       = 2;
    neighbor.time_stamp = 0x10000000;
    neighbor.beacon_ind = 0x04;
    neighbor.intra_channel = 0x03;

    mac_neighbor_node_add(&neighbor);
    uint8_t res = mac_neighbor_node_delete(&neighbor);
    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_003
* Test Name: delete_neighbor_node_id_Not_Exists
* Description:删除1个不存在的节点，删除不成功
* Test Step: 
* 1) 调用mac_neighbor_node_delete，节点不存在，函数返回值为FALSE
*/ 
TEST(mac_neighbors, delete_neighbor_node_id_Not_Exists)
{
    neighbor_node_t neighbor;

    neighbor.dev_id     = 0x0002;
    neighbor.rssi       = -80;
    neighbor.hops       = 2;
    neighbor.time_stamp = 0x10000000;
    neighbor.beacon_ind = 0x04;
    neighbor.intra_channel = 0x03;

    uint8_t res = mac_neighbor_node_delete(&neighbor);
    TEST_ASSERT_FALSE(res);

}

/**
* Test ID: TC_MAC_NEIGHBORS_004
* Test Name: add_one_neighbor_queue_is_full
* Description:队列满后，添加节点，添加不成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加节点，使队列加满
* 2) 调用mac_heighbor_node_add，添加一个节点，返回FALSE
*/ 
TEST(mac_neighbors, add_one_neighbor_queue_is_full)
{    
    int i;
    
    neighbor_node_t neighbor;
    
    for (i = 1 ; i <= DEV_COUNT ; i ++)
    {
        neighbor.dev_id     = i;
        neighbor.rssi       = -80;
        neighbor.hops       = 2;
        neighbor.time_stamp = 0x10000000;
        neighbor.beacon_ind = 0x04;
        neighbor.intra_channel = 0x03;
        
        mac_neighbor_node_add(&neighbor);
    }

    neighbor_node_t neighbor1;
    
    neighbor1.dev_id     = DEV_COUNT + 1;
    neighbor1.rssi       = -80;
    neighbor1.hops       = 2;
    neighbor1.time_stamp = 0x10000000;
    neighbor1.beacon_ind = 0x04;
    neighbor1.intra_channel = 0x03;
    
    uint8_t res = mac_neighbor_node_add(&neighbor1);
    TEST_ASSERT_FALSE(res);
    
}


/**
* Test ID: TC_MAC_NEIGHBORS_005
* Test Name: add_one_neighbor_queue_is_full_id_Exists
* Description:队列满后，添加已存在的节点，添加成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加1个邻居节点
* 2) 调用mac_heighbor_node_add，添加一个节点，节点号已经存在，返回TRUE
*/ 
TEST(mac_neighbors, add_one_neighbor_queue_is_full_id_Exists)
{    
    int i;
    
    neighbor_node_t neighbor;
    
    for (i = 1 ; i <= DEV_COUNT ; i ++)
    {
        neighbor.dev_id     = i;
        neighbor.rssi       = -80;
        neighbor.hops       = 2;
        neighbor.time_stamp = 0x10000000;
        neighbor.beacon_ind = 0x04;
        neighbor.intra_channel = 0x03;
        
        mac_neighbor_node_add(&neighbor);
    }

    neighbor_node_t neighbor1;
    
    neighbor1.dev_id     = DEV_COUNT;
    neighbor1.rssi       = -80;
    neighbor1.hops       = 2;
    neighbor1.time_stamp = 0x10000000;
    neighbor1.beacon_ind = 0x04;
    neighbor1.intra_channel = 0x03;
    
    uint8_t res = mac_neighbor_node_add(&neighbor1);
    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_006
* Test Name: delete_the_last_node_then_add_queue_is_full
* Description:队列满后，删除最后一个节点，添加新的节点，添加成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加节点，使队列加满
* 2) 调用mac_neighbor_node_delete，删除最后一个节点
* 3) 调用mac_neighbor_node_add，添加节点，返回TRUE
*/ 
TEST(mac_neighbors, delete_the_last_node_then_add_queue_is_full)
{    
    int i;
    
    neighbor_node_t neighbor;
    
    for (i = 1 ; i <= DEV_COUNT ; i ++)
    {
        neighbor.dev_id     = i;
        neighbor.rssi       = -80;
        neighbor.hops       = 2;
        neighbor.time_stamp = 0x10000000;
        neighbor.beacon_ind = 0x04;
        neighbor.intra_channel = 0x03;
        
        mac_neighbor_node_add(&neighbor);
    }

    neighbor_node_t neighbor1;
    
    neighbor1.dev_id     = DEV_COUNT;
    neighbor1.rssi       = -80;
    neighbor1.hops       = 2;
    neighbor1.time_stamp = 0x10000000;
    neighbor1.beacon_ind = 0x04;
    neighbor1.intra_channel = 0x03;
    
    mac_neighbor_node_delete(&neighbor1);

    neighbor_node_t neighbor2;
    
    neighbor2.dev_id     = DEV_COUNT+1;
    neighbor2.rssi       = -80;
    neighbor2.hops       = 2;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    uint8_t res = mac_neighbor_node_add(&neighbor2);
    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_007
* Test Name: delete_one_node_then_add_queue_is_full
* Description:队列满后，删除一个节点，添加新的节点，添加成功
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加节点，使队列加满
* 2) 调用mac_neighbor_node_delete，删除一个节点
* 3) 调用mac_neighbor_node_add，添加节点，返回TRUE
*/ 
TEST(mac_neighbors, delete_one_node_then_add_queue_is_full)
{    
    int i;
    
    neighbor_node_t neighbor;
    
    for (i = 1 ; i <= DEV_COUNT ; i ++)
    {
        neighbor.dev_id     = i;
        neighbor.rssi       = -80;
        neighbor.hops       = 2;
        neighbor.time_stamp = 0x10000000;
        neighbor.beacon_ind = 0x04;
        neighbor.intra_channel = 0x03;
        
        mac_neighbor_node_add(&neighbor);
    }

    neighbor_node_t neighbor1;
    
    neighbor1.dev_id     = 1;
    neighbor1.rssi       = -80;
    neighbor1.hops       = 2;
    neighbor1.time_stamp = 0x10000000;
    neighbor1.beacon_ind = 0x04;
    neighbor1.intra_channel = 0x03;
    
    mac_neighbor_node_delete(&neighbor1);

    neighbor_node_t neighbor2;
    
    neighbor2.dev_id     = DEV_COUNT+1;
    neighbor2.rssi       = -80;
    neighbor2.hops       = 2;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    uint8_t res = mac_neighbor_node_add(&neighbor2);
    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_008
* Test Name: update_neighbor_table_lt_LIVE_TIMEOUT
* Description: 更新邻居表，节点生命周期递增，小于生命周期时，删除节点
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加一个节点
* 2) 调用mac_neighbor_table_update，更新邻居表，更新次数=LIVE_TIMEOUT-1
* 3) 调用mac_neighbor_node_delete，删除节点，返回TRUE
*/ 
TEST(mac_neighbors, update_neighbor_table_lt_LIVE_TIMEOUT)
{
    int i;
    
    neighbor_node_t neighbor;

    neighbor.dev_id     = 1;
    neighbor.rssi       = -80;
    neighbor.hops       = 2;
    neighbor.time_stamp = 0x10000000;
    neighbor.beacon_ind = 0x04;
    neighbor.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor);
    
    for (i = 1 ; i < LIVE_TIMEOUT ; i ++)
    {
        mac_neighbor_table_update();
    }
    
    uint8_t res = mac_neighbor_node_delete(&neighbor);

    TEST_ASSERT_TRUE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_009
* Test Name: update_neighbor_table_et_LIVE_TIMEOUT
* Description: 更新邻居表，节点生命周期递增，等于生命周期时，删除节点
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加一个节点
* 2) 调用mac_neighbor_table_update，更新邻居表，更新次数=LIVE_TIMEOUT
* 3) 调用mac_neighbor_node_delete，删除节点，返回FALSE
*/ 
TEST(mac_neighbors, update_neighbor_table_et_LIVE_TIMEOUT)
{
    int i;
    
    neighbor_node_t neighbor;

    neighbor.dev_id     = 1;
    neighbor.rssi       = -80;
    neighbor.hops       = 2;
    neighbor.time_stamp = 0x10000000;
    neighbor.beacon_ind = 0x04;
    neighbor.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor);
    
    for (i = 1 ; i <= LIVE_TIMEOUT ; i ++)
    {
        mac_neighbor_table_update();
    }
    
    uint8_t res = mac_neighbor_node_delete(&neighbor);

    TEST_ASSERT_FALSE(res);
}

/**
* Test ID: TC_MAC_NEIGHBORS_010
* Test Name: update_neighbor_table_more_than_one_node
* Description: 更新邻居表，多个节点生命周期均递增
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加一个节点
* 2) 调用mac_neighbor_table_update，更新邻居表
* 3) 调用mac_neighbor_node_add，添加另一个节点
* 4) 调用mac_neighbor_table_update，更新邻居表，更新LIVE_TIMEOUT-1次
* 5) 调用mac_neighbor_node_delete，删除节点一，返回FALSE
* 6) 调用mac_get_coord，返回TRUE
* 7) 调用mac_neighbor_table_update，更新邻居表
* 8) 调用mac_get_coord，返回FALSE
*/ 
TEST(mac_neighbors, update_neighbor_table_more_than_one_node)
{
    int i;
    
    neighbor_node_t neighbor1;

    neighbor1.dev_id     = 1;
    neighbor1.rssi       = -80;
    neighbor1.hops       = 2;
    neighbor1.time_stamp = 0x10000000;
    neighbor1.beacon_ind = 0x04;
    neighbor1.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor1);
    
    mac_neighbor_table_update();
    
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = 2;
    neighbor2.rssi       = -80;
    neighbor2.hops       = 2;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    for (i = 2 ; i <= LIVE_TIMEOUT ; i ++)
    {
        mac_neighbor_table_update();
    }
    
    uint8_t res = mac_neighbor_node_delete(&neighbor1);

    TEST_ASSERT_FALSE(res);
    
    res = mac_get_coord(&neighbor2);

    TEST_ASSERT_TRUE(res);
    
    mac_neighbor_table_update();
    
    res = mac_get_coord(&neighbor2);

    TEST_ASSERT_FALSE(res);
}


/**
* Test ID: TC_MAC_NEIGHBORS_011
* Test Name: mac_get_coord_branch1
* Description: rssi>=RSSI_QUERY_THRESHOLD && hops<MAX_HOPS，节点满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi>=RSSI_QUERY_THRESHOLD && hops<MAX_HOPS
* 2) 调用mac_get_coord，返回TRUE，节点值与节点N一致
*/ 
TEST(mac_neighbors, mac_get_coord_branch1)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-1;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_THRESHOLD-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点N
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT;
    neighbor2.rssi       = RSSI_QUERY_THRESHOLD;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    
    neighbor_node_t neighbor3;
    
    uint8_t res = mac_get_coord(&neighbor3);

    TEST_ASSERT_TRUE(res);
    
    TEST_ASSERT_EQUAL_INT16(DEV_COUNT,neighbor3.dev_id);

}


/**
* Test ID: TC_MAC_NEIGHBORS_012
* Test Name: mac_get_coord_branch2
* Description: rssi>=RSSI_QUERY_THRESHOLD && hops==MAX_HOPS, assoc_state==TRUE，节点满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi>=RSSI_QUERY_THRESHOLD && hops==MAX_HOPS
* 2) 调用mac_get_coord，返回TRUE，节点值与节点N一致
*/ 
TEST(mac_neighbors, mac_get_coord_branch2)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-1;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_THRESHOLD-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点N
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT;
    neighbor2.rssi       = RSSI_QUERY_THRESHOLD;
    neighbor2.hops       = MAX_HOPS;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    neighbor_node_t neighbor3;
    
    uint8_t res = mac_get_coord(&neighbor3);

    TEST_ASSERT_TRUE(res);
    
    TEST_ASSERT_EQUAL_INT16(DEV_COUNT,neighbor3.dev_id);

}

/**
* Test ID: TC_MAC_NEIGHBORS_013
* Test Name: mac_get_coord_branch3
* Description: rssi>=RSSI_QUERY_THRESHOLD && hops==MAX_HOPS, assoc_state==FALSE，节点不满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi >= RSSI_QUERY_THRESHOLD && hops == MAX_HOPS,
*    一个节点M rssi > RSSI_QUERY_FAILED
* 2) 调用mac_neighbors_node_set_state，将节点N的assoc_state值设为FALSE
* 3) 调用mac_get_coord，返回TRUE，节点值与节点M一致
*/ 
TEST(mac_neighbors, mac_get_coord_branch3)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-2;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_FAILED-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点M
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT-1;
    neighbor2.rssi       = RSSI_QUERY_FAILED+1;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    // 节点N
    neighbor_node_t neighbor3;

    neighbor3.dev_id     = DEV_COUNT;
    neighbor3.rssi       = RSSI_QUERY_THRESHOLD;
    neighbor3.hops       = MAX_HOPS;
    neighbor3.time_stamp = 0x10000000;
    neighbor3.beacon_ind = 0x04;
    neighbor3.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor3);
    
    mac_neighbors_node_set_state(neighbor3.dev_id,FALSE);
    
    neighbor_node_t neighbor4;
    
    uint8_t res = mac_get_coord(&neighbor4);

    TEST_ASSERT_TRUE(res);
    
    TEST_ASSERT_EQUAL_INT16(neighbor2.dev_id,neighbor4.dev_id);

}

/**
* Test ID: TC_MAC_NEIGHBORS_014
* Test Name: mac_get_coord_branch4
* Description: rssi>=RSSI_QUERY_THRESHOLD && hops>MAX_HOPS, assoc_state==TRUE，节点不满足条件；其他节点rssi > RSSI_QUERY_FAILED，满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi >= RSSI_QUERY_THRESHOLD && hops > MAX_HOPS,
*    一个节点M rssi > RSSI_QUERY_FAILED
* 2) 调用mac_get_coord，返回TRUE，节点值与节点M一致
*/ 
TEST(mac_neighbors, mac_get_coord_branch4)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-2;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_FAILED-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点M
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT-1;
    neighbor2.rssi       = RSSI_QUERY_FAILED+1;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    // 节点N
    neighbor_node_t neighbor3;

    neighbor3.dev_id     = DEV_COUNT;
    neighbor3.rssi       = RSSI_QUERY_THRESHOLD;
    neighbor3.hops       = MAX_HOPS+1;
    neighbor3.time_stamp = 0x10000000;
    neighbor3.beacon_ind = 0x04;
    neighbor3.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor3);
    
    neighbor_node_t neighbor4;
    
    uint8_t res = mac_get_coord(&neighbor4);

    TEST_ASSERT_TRUE(res);
    
    TEST_ASSERT_EQUAL_INT16(neighbor2.dev_id,neighbor4.dev_id);

}


/**
* Test ID: TC_MAC_NEIGHBORS_015
* Test Name: mac_get_coord_branch5
* Description: rssi == RSSI_QUERY_FAILED，hops < MAX_HOPS，assoc_state==TRUE，节点满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi == RSSI_QUERY_FAILED && hops < MAX_HOPS,
* 2) 调用mac_get_coord，返回TRUE，节点值与节点N一致
*/ 
TEST(mac_neighbors, mac_get_coord_branch5)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-1;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_FAILED-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点M
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT;
    neighbor2.rssi       = RSSI_QUERY_FAILED;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    neighbor_node_t neighbor3;
    
    uint8_t res = mac_get_coord(&neighbor3);

    TEST_ASSERT_TRUE(res);
    
    TEST_ASSERT_EQUAL_INT16(neighbor2.dev_id,neighbor3.dev_id);

}

/**
* Test ID: TC_MAC_NEIGHBORS_016
* Test Name: mac_get_coord_branch6
* Description: rssi == RSSI_QUERY_FAILED，hops < MAX_HOPS，assoc_state==FALSE，节点不满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi == RSSI_QUERY_FAILED && hops < MAX_HOPS,
* 2) 调用mac_neighbors_node_set_state，将节点N的assoc_state值设为FALSE
* 3) 调用mac_get_coord，返回FALSE
*/ 
TEST(mac_neighbors, mac_get_coord_branch6)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-1;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_FAILED-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点M
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT;
    neighbor2.rssi       = RSSI_QUERY_FAILED;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    mac_neighbors_node_set_state(neighbor2.dev_id,FALSE);
    
    neighbor_node_t neighbor3;
    
    uint8_t res = mac_get_coord(&neighbor3);

    TEST_ASSERT_FALSE(res);

}

/**
* Test ID: TC_MAC_NEIGHBORS_017
* Test Name: mac_get_coord_branch7
* Description: rssi < RSSI_QUERY_FAILED，hops < MAX_HOPS, 节点不满足条件
* Test Step: 
* 1) 调用mac_neighbor_node_add，添加多个节点，一个节点N rssi == RSSI_QUERY_FAILED && hops < MAX_HOPS,
* 2) 调用mac_get_coord，返回FALSE
*/ 
TEST(mac_neighbors, mac_get_coord_branch7)
{
    int i;
    
    neighbor_node_t neighbor1;
    
    for (i=1;i<=DEV_COUNT-1;i++)
    {
        neighbor1.dev_id     = i;
        neighbor1.rssi       = RSSI_QUERY_FAILED-1;
        neighbor1.hops       = MAX_HOPS-1;
        neighbor1.time_stamp = 0x10000000;
        neighbor1.beacon_ind = 0x04;
        neighbor1.intra_channel = 0x03;
        mac_neighbor_node_add(&neighbor1);
    }
    
    // 节点M
    neighbor_node_t neighbor2;

    neighbor2.dev_id     = DEV_COUNT;
    neighbor2.rssi       = RSSI_QUERY_FAILED-1;
    neighbor2.hops       = MAX_HOPS-1;
    neighbor2.time_stamp = 0x10000000;
    neighbor2.beacon_ind = 0x04;
    neighbor2.intra_channel = 0x03;
    
    mac_neighbor_node_add(&neighbor2);
    
    neighbor_node_t neighbor3;
    
    uint8_t res = mac_get_coord(&neighbor3);

    TEST_ASSERT_FALSE(res);

}