#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>
#include "gps.h"

TEST_GROUP_RUNNER(gps_test)
{
	// �ڲ���������
//    RUN_TEST_CASE(gps_test, gps_test_hex2char);
//    RUN_TEST_CASE(gps_test, gps_test_crc8);
//    RUN_TEST_CASE(gps_test, gps_test_get_median);
	
	// NMEA�����
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_01);
//	RUN_TEST_CASE(gps_test, gps_test_nmea_parse_02);
	RUN_TEST_CASE(gps_test, gps_dm_to_d_test_01);
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_fail_a);
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_fail_b);
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_gpgga);
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_gpgsv);
//    RUN_TEST_CASE(gps_test, gps_test_nmea_parse_gpgsa);
	
	// NMEA�������ѹ����
//	RUN_TEST_CASE(gps_test, gps_nmealib_compression_test01);
//	RUN_TEST_CASE(gps_test, gps_nmealib_compression_test02);

	RUN_TEST_CASE(gps_test, gps_parse_rmc_test01);

	// GPS��������
//	RUN_TEST_CASE(gps_test, gps_test_drive);
}
