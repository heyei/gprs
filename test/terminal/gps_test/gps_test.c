/**
* GPS测试程序
*
* @file 	gps_test.c
* @author 	xiangwenfang
* @data 	2014-12-25
*
**/

#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include "gps.h"
#include "escort_gps.h"
#include <string.h>

TEST_GROUP(gps_test);

TEST_SETUP(gps_test)
{
    ;
}

TEST_TEAR_DOWN(gps_test)
{
    ;
}

// 十六进制数转化为ASCII码
static uint8_t hex2char(uint8_t value_hex)
{
    uint8_t value_char = 0;
    if (value_hex <= 9)
    {
        value_char = '0' + value_hex;
    }
    else if ((value_hex >= 10) && (value_hex <= 15))
    {
        value_char = 'A' + value_hex - 10;
    }
    return value_char;
}

// 计算异或和，GPS校验中使用
static uint8_t crc8(uint8_t *p, uint8_t len)
{
    uint8_t crc = 0;
    if (p != NULL && len > 0 && len < 128)
    {
        for (uint8_t i = 0; i < len; i++)
        {
            crc ^= *p++;
        }
    }
    return crc;
}

// 取出数字（1~65535）的十进制上各位的数值
static uint8_t get_median(uint16_t num, uint8_t *p)
{
    uint8_t len = 0;
    if (num != 0)
    {
        if (num/10000 != 0)
        {
            len = 5;
        }
        else if (num < 10000 && num >= 1000)
        {
            len = 4;
        }
        else if (num < 1000 && num >= 100)
        {
            len = 3;
        }
        else if (num < 100 && num >= 10)
        {
            len = 2;
        }
        else if (num < 10)
        {
            len = 1;
        }

        for (uint8_t i=0; i<len; i++)
        {
            *p++ = num%10;
            num /= 10;
        }
    }
    return len;
}

// 测试hex2char：边界测试（0~15）
TEST(gps_test, gps_test_hex2char)
{
    TEST_ASSERT_EQUAL_UINT8(hex2char(10) , 'A');
    TEST_ASSERT_EQUAL_UINT8(hex2char(0)  , '0');
    TEST_ASSERT_EQUAL_UINT8(hex2char(15) , 'F');
    TEST_ASSERT_EQUAL_UINT8(hex2char(16) ,  0);
    TEST_ASSERT_EQUAL_UINT8(hex2char(100),  0);
}

// 测试hex2char：计算异或和
TEST(gps_test, gps_test_crc8)
{
    TEST_ASSERT_EQUAL_UINT8(crc8("PMTK251,9600", 12), 0x17);
    TEST_ASSERT_EQUAL_UINT8(crc8("PMTK314,0,10,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0", 46), 0x19);
    TEST_ASSERT_EQUAL_UINT8(crc8("PMTK161,1", 9), 0x29);
    TEST_ASSERT_EQUAL_UINT8(crc8("PMTK010,001", 11), 0x2E);
}

// 测试get_median
TEST(gps_test, gps_test_get_median)
{
    uint8_t median[5] = {0, 0, 0, 0, 0};
    TEST_ASSERT_EQUAL_UINT8(get_median(20, &median[0]), 2);
    TEST_ASSERT_EQUAL_UINT8(median[0], 0);
    TEST_ASSERT_EQUAL_UINT8(median[1], 2);

    TEST_ASSERT_EQUAL_UINT8(get_median(255, &median[0]), 3);
    TEST_ASSERT_EQUAL_UINT8(median[0], 5);
    TEST_ASSERT_EQUAL_UINT8(median[1], 5);
    TEST_ASSERT_EQUAL_UINT8(median[2], 2);

    TEST_ASSERT_EQUAL_UINT8(get_median(1500, &median[0]), 4);
    TEST_ASSERT_EQUAL_UINT8(median[0], 0);
    TEST_ASSERT_EQUAL_UINT8(median[1], 0);
    TEST_ASSERT_EQUAL_UINT8(median[2], 5);
    TEST_ASSERT_EQUAL_UINT8(median[3], 1);

    TEST_ASSERT_EQUAL_UINT8(get_median(30000, &median[0]), 5);
    TEST_ASSERT_EQUAL_UINT8(median[0], 0);
    TEST_ASSERT_EQUAL_UINT8(median[1], 0);
    TEST_ASSERT_EQUAL_UINT8(median[2], 0);
    TEST_ASSERT_EQUAL_UINT8(median[3], 0);
    TEST_ASSERT_EQUAL_UINT8(median[4], 3);

    TEST_ASSERT_EQUAL_UINT8(get_median(1, &median[0]), 1);
    TEST_ASSERT_EQUAL_UINT8(median[0], 1);
}

// 测试nmea_parse
TEST(gps_test, gps_test_nmea_parse_01)
{
    char gps_str[] = "$GPRMC,024235.00,A,3129.48772,N,12022.09874,E,2.010,,120214,,,A*73\r\n";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_TRUE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    printf("\r\nUTC:%d\r\n",gps_info.utc.year);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

static double gps_dm_to_d(double dm)
{
    int32_t dd;
    
    dd = (int32_t)dm / 100;
    
    return (dm - dd * 100) * ((double)1.0 / 60.0) + dd; 
}

// 测试GPS数值传输
TEST(gps_test, gps_test_nmea_parse_02)
{
    char gps_str[] = "$GPRMC,024235.00,A,3129.48772,N,12022.09874,E,2.010,,120214,,,A*73\r\n";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_TRUE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
	nmea_parser_destroy(&parser);
	
	
	double lat,lon;
	lat = gps_dm_to_d(gps_info.lat);
	lon = gps_dm_to_d(gps_info.lon);
    
}

static double lat,lon;
TEST(gps_test, gps_dm_to_d_test_01)
{
	gps_simple_info_t gps_data;
	
	uint8_t gps_status;
	uint8_t gps_delay_cmd;
	
	gps_status = 1;
	gps_delay_cmd = 0;
	
	gps_data.latitude = 0.0;
	gps_data.longitude = (gps_status*128 + gps_delay_cmd)*100;
	gps_data.speed = 0.0;
	
	lat = gps_dm_to_d(gps_data.latitude);
	lon = gps_dm_to_d(gps_data.longitude);
	
	gps_status = 1;
	gps_delay_cmd = 1;
	
	gps_data.latitude = 0.0;
	gps_data.longitude = (gps_status*128 + gps_delay_cmd)*100;
	gps_data.speed = 0.0;
	
	lat = gps_dm_to_d(gps_data.latitude);
	lon = gps_dm_to_d(gps_data.longitude);
	
	uint8_t debug_read_len;
	uint8_t gie_status;
	uint8_t uart_status;
	
	debug_read_len = 56;
	gie_status = 1;
	uart_status = 0;
	
	gps_data.latitude = (gie_status*128 + uart_status)*100;
	gps_data.longitude = (gps_status*128 + debug_read_len)*100;
	gps_data.speed = 0.0;
	
	lat = gps_dm_to_d(gps_data.latitude);
	lon = gps_dm_to_d(gps_data.longitude);
	
	debug_read_len = 128;
	gie_status = 1;
	uart_status = 1;
	
	gps_data.latitude = (gie_status*128 + uart_status)*100;
	gps_data.longitude = (gps_status*128 + debug_read_len)*100;
	gps_data.speed = 0.0;
	
	lat = gps_dm_to_d(gps_data.latitude);
	lon = gps_dm_to_d(gps_data.longitude);
    
}

// 测试错误情况1
TEST(gps_test, gps_test_nmea_parse_fail_a)
{
    char gps_str[] = "$GPRMC,,V,,,,,,,,,,N*53";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_FALSE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    TEST_ASSERT_EQUAL_INT(gps_info.sig, NMEA_SIG_BAD);
    TEST_ASSERT_EQUAL_INT(gps_info.fix, NMEA_FIX_BAD);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

// 测试错误情况2
TEST(gps_test, gps_test_nmea_parse_fail_b)
{
    char gps_str[] = "$GPRMC,022649.00,V,,,,,,,020913,,,N*7F";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_FALSE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    TEST_ASSERT_EQUAL_INT(gps_info.sig, NMEA_SIG_BAD);
    TEST_ASSERT_EQUAL_INT(gps_info.fix, NMEA_FIX_BAD);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

TEST(gps_test, gps_test_nmea_parse_gpgga)
{
    char gps_str[] = "$GPGGA,111609.14,5001.27,N,3613.06,E,3,08,0.0,10.2,M,0.0,M,0.0,0000*70\r\n";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_TRUE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

TEST(gps_test, gps_test_nmea_parse_gpgsv)
{
    char gps_str[] = "$GPGSV,2,1,08,01,05,005,80,02,05,050,80,03,05,095,80,04,05,140,80*7f\r\n";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_TRUE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

TEST(gps_test, gps_test_nmea_parse_gpgsa)
{
    char gps_str[] = "$GPGSA,A,3,01,02,03,04,05,06,07,08,00,00,00,00,0.0,0.0,0.0*3a\r\n";
    nmeaPARSER parser;
    nmeaINFO gps_info;
    nmea_zero_INFO(&gps_info);
    nmea_parser_init(&parser);
    TEST_ASSERT_TRUE((nmea_parse(&parser, &gps_str[0], (int)strlen(gps_str), &gps_info)) > 0);
    nmea_parser_destroy(&parser);   // 释放解析载体的内存空间
}

static void gps_power_on(void)
{
	P7SEL &= ~BIT4;
	P7DIR |= BIT4;
	P7OUT |= BIT4;
}

// GPS驱动
TEST(gps_test, gps_test_drive)
{
	gps_power_on();
	
	fp32_t baud = 0;
	uint32_t baud_rate;
	uint8_t br0;
	uint8_t br1;
	uint8_t fract;
	
	// UART2
	P9SEL |= BIT4 + BIT5;
	P9DIR |= BIT4;
	P9DIR &= ~BIT5;

	UCA2CTL1 = UCSWRST;
	UCA2CTL0 = UCMODE_0;
	UCA2CTL1 |= UCSSEL_2;
	
	baud_rate = 115200;
	baud = (fp32_t)SMCLK / baud_rate;
    br0 = (uint16_t)baud & 0xFF;
    br1 = ((uint16_t)baud >> 8) & 0xFF;
    fract = (uint8_t)((baud - (uint16_t)baud) * 8 + 0.5);
	UCA2BR0 = br0;
	UCA2BR1 = br1;
	UCA2MCTL = UCBRF_0 | (fract << 1);
	
	UCA2CTL1 &= ~UCSWRST;
	UCA2IE |= UCRXIE;
	
	// UART1
	P7SEL &= ~BIT2;
	P7DIR |= BIT2;
	P7OUT &= ~BIT2;
	
	P5SEL &= ~(BIT0 + BIT1);
	P5DIR |= BIT0 + BIT1;             
	P5OUT |= BIT0;// force off P5.0
	P5OUT |= BIT1;              // force on P5.1        
	/* Initialize uart 1 IO */
	P5SEL |= BIT6 + BIT7;
	P5DIR |= BIT7;
	P5DIR &= ~BIT6;
	
	UCA1CTL1 = UCSWRST;
	UCA1CTL0 = UCMODE_0;
	UCA1CTL1 |= UCSSEL_2;
	
	baud_rate = 115200;
	baud = (fp32_t)SMCLK / baud_rate;
    br0 = (uint16_t)baud & 0xFF;
    br1 = ((uint16_t)baud >> 8) & 0xFF;
    fract = (uint8_t)((baud - (uint16_t)baud) * 8 + 0.5);
	UCA1BR0 = br0;
	UCA1BR1 = br1;
	UCA1MCTL = UCBRF_0 | (fract << 1);
	
	UCA1CTL1 &= ~UCSWRST;
	UCA1IE |= UCRXIE;
	
	while(1);
}

/* 
** @func:NMEA解析库抗压测试1
** 对给定GPS NMEA协议数据多次解析，包括：定位成功数据、定位不成功数据、错误数据
*/
TEST(gps_test, gps_nmealib_compression_test01)
{
	char gps_str1[] = "$GPRMC,013257.00,A,3129.51829,N,12022.10562,E,0.093,,270813,,,A*7A\r\n";
	char gps_str2[] = "$GPRMC,022649.00,V,,,,,,,020913,,,N*7F\r\n";
	char gps_str3[] = "$GPRMC,022650.00,V,,,,,,,020913,,,N*7F\r\n";
	static uint16_t a_data_a = 0;
	static uint16_t a_data_v = 0;
	static uint16_t v_data_v = 0;
	static uint16_t wrong_data = 0;

	nmeaINFO info;
	nmeaPARSER parser;
	nmea_zero_INFO(&info);

	for( int i = 0; i < 1600 ; i++)
	{
		// 解析定位成功数据
		nmea_parser_init(&parser);
		int count1 = nmea_parse(&parser, gps_str1, (int)strlen(gps_str1), &info);
		if ((info.sig == NMEA_SIG_MID) && (info.fix == NMEA_FIX_2D))
		{
			a_data_a++;
		}
		else if ((info.sig == NMEA_SIG_BAD) && (info.fix == NMEA_FIX_BAD))
		{
			a_data_v++;
		}
		nmea_parser_destroy(&parser);

		// 解析定位失败数据
		nmea_parser_init(&parser);
		int count2 = nmea_parse(&parser, gps_str2, (int)strlen(gps_str2), &info);
		if( (info.fix == NMEA_FIX_BAD ) || ( info.sig == NMEA_SIG_BAD) )
		{
		  	v_data_v++;
		}
		nmea_parser_destroy(&parser);
	}
}

typedef struct _time_t
{
	uint8_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minite;
	uint8_t second;
} time_t;

/* 
** @func:GPS组成GPRMC帧
** @para:经度、纬度、速度、时间
*/
static void gps_assembly_rmc_packet(fp32_t lon, fp32_t lat, fp32_t speed, time_t utc)
{
    // head
    uint8_t cmd[GPS_CMD_LEN_MAX] = "$GPRMC,";
    uint8_t len = 7;
    
    // UTC time
	uint8_t utc1[10];
    uint8_t i;
	utc1[0] = hex2char((uint8_t)(utc.hour/10));
	utc1[1] = hex2char((uint8_t)(utc.hour%10));
	utc1[2] = hex2char((uint8_t)(utc.minite/10));
	utc1[3] = hex2char((uint8_t)(utc.minite%10));
	utc1[4] = hex2char((uint8_t)(utc.second/10));
	utc1[5] = hex2char((uint8_t)(utc.second%10));
	utc1[6] = '.';
	utc1[7] = '0';
	utc1[8] = '0';
	utc1[9] = '0';
	
	uint8_t utc2[6];
	utc2[0] = hex2char((uint8_t)(utc.month/10));
	utc2[1] = hex2char((uint8_t)(utc.month%10));
	utc2[2] = hex2char((uint8_t)(utc.day/10));
	utc2[3] = hex2char((uint8_t)(utc.day%10));
	utc2[4] = hex2char((uint8_t)(utc.year/10));
	utc2[5] = hex2char((uint8_t)(utc.year%10));
	
	// positioning status
	uint8_t position_status = 'A';
	
    for (i=0; i<10; i++)
    {
        cmd[len] = utc1[i];
        len ++;
    }
    
    // 剩余部分
    uint8_t cmd_add[] = ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
    for (i=0; i<34; i++)
    {
        cmd[len] = cmd_add[i];
        len ++;
    }
    
    uint8_t crc = crc8(&cmd[1], len - 1);
    uint8_t crc_high = (uint8_t)(crc>>4);
    uint8_t crc_low  = (uint8_t)(crc&0x0F);
    crc_high = hex2char(crc_high);
    crc_low  = hex2char(crc_low);
    
    cmd[len] = '*';
    len ++;
    cmd[len] = crc_high;
    len ++;
    cmd[len] = crc_low;
    len ++;
    cmd[len] = GPS_CR;
    len ++;
    cmd[len] = GPS_LF;
    len ++;
    
    hal_uart_send_string(GPS_UART, &cmd[0], len);
}

/* 
** @func:NMEA解析库抗压测试2
** 对给定GPS NMEA协议数据多次解析，包括：定位成功数据、定位不成功数据、错误数据
*/
TEST(gps_test, gps_nmealib_compression_test02)
{
	uint8_t str[] = "$GPRMC,035731.000,A,3129.5418,N,12022.2170,E,2.65,70.43,260115,,,D*5E\r\n";
	nmeaINFO info;
	nmeaPARSER parser;
	uint8_t result;
	static uint16_t data_1 = 0;
	static uint16_t data_2 = 0;
	static uint16_t data_3 = 0;
	uint8_t lat_3 = 8;
	uint8_t lon_3 = 5;
	uint8_t len;
	len = 71;
	
	for( uint8_t i = 0; i < 1600 ; i++ )
	{
		nmea_zero_INFO(&info);
		nmea_parser_init(&parser);
		
		lat_3++;
		if (lat_3 >= 10)
		{
			lat_3 = 0;
		}
		str[27] = hex2char(lat_3);
		
		lon_3++;
		if (lon_3 >= 10)
		{
			lon_3 = 0;
		}
		str[40] = hex2char(lon_3);
		
		uint8_t crc = crc8(&str[1], 65);
		uint8_t crc_high = (uint8_t)(crc>>4);
		uint8_t crc_low  = (uint8_t)(crc&0x0F);
		crc_high = hex2char(crc_high);
		crc_low  = hex2char(crc_low);
		
		str[67] = crc_high;
		str[68] = crc_low;
		
		nmea_parser_init(&parser);
		result = nmea_parse(&parser, str, (uint8_t)strlen(str), &info);
		if ((info.sig == NMEA_SIG_MID) && (info.fix == NMEA_FIX_2D))
		{
			data_1++;
		}
		else if ((info.sig == NMEA_SIG_BAD) && (info.fix == NMEA_FIX_BAD))
		{
			data_2++;
		}
		else
		{
			data_3++;
		}
		nmea_parser_destroy(&parser);
	}
}

/* 
** @func:测试解析函数
*/
static uint8_t gps_recv_array[100];
static gps_simple_info_t gps_simple_info;

static uint8_t char2hex(uint8_t v_char)
{
    uint8_t v_hex = 0;
    if ((v_char >= '0') && (v_char <= '9'))
    {
        v_hex = v_char - '0';
    }
    else if ((v_char >= 'A') && (v_char <= 'F'))
    {
        v_hex = v_char - 'A' + 10;
    }
	else if ((v_char >= 'a') && (v_char <= 'f'))
	{
		v_hex = v_char - 'a' + 10;
	}
    return v_hex;
}

static bool_t gps_rmc_crc(uint8_t *p_data, uint8_t len)
{
	DBG_ASSERT(p_data != NULL __DBG_LINE);
	
	uint8_t crc;
	uint8_t high;
	uint8_t low;
	bool_t flag = FALSE;
	
	if ((len < GPS_RMC_LEN_MIN) || (len > GPS_RECV_LEN_MAX))
	{
		return flag;
	}

	if ((len > (GPS_RMC_LEN_MIN+4)) && (p_data[len-2] == GPS_CR) && (p_data[len-5] == GPS_CRC_PROMPT))
	{
		crc	= crc8(&p_data[1], len-6);
		high = hex2char((uint8_t)(crc>>4));
		low  = hex2char((uint8_t)(crc&0x0F));
		
		if ((high == p_data[len-4]) && (low == p_data[len-3]))
		{
			flag = TRUE;
		}
	}
	return flag;
}

static fp64_t get_rmc_value(uint8_t *p_data, uint8_t type)
{
	DBG_ASSERT(p_data != NULL __DBG_LINE);
	
	location_t local;
	fp64_t value = 0.0;
	
	if ((type < GPS_DATA_TYPE_LAT) || (type > GPS_DATA_TYPE_LON))
	{
		return value;
	}
	
	switch (type)
	{
	case GPS_DATA_TYPE_LAT:
		local.deg = char2hex(p_data[0])*10 
			+ char2hex(p_data[1]);
		local.min_dec = char2hex(p_data[2])*10 
			+ char2hex(p_data[3]);
		local.min_fract = char2hex(p_data[5])*1000 
			+ char2hex(p_data[6])*100
			+ char2hex(p_data[7])*10 
			+ char2hex(p_data[8]);
		value = local.deg*100 + local.min_dec + ((fp64_t)local.min_fract)/10000;
		break;
		
	case GPS_DATA_TYPE_LON:
		local.deg = char2hex(p_data[0])*100 
			+ char2hex(p_data[1])*10 
			+ char2hex(p_data[2]);
		local.min_dec = char2hex(p_data[3])*10 
			+ char2hex(p_data[4]);
		local.min_fract = char2hex(p_data[6])*1000 
			+ char2hex(p_data[7])*100
			+ char2hex(p_data[8])*10 
			+ char2hex(p_data[9]);
		value = local.deg*100 + local.min_dec + ((fp64_t)local.min_fract)/10000;
		break;
	}
	return value;
}

static uint8_t gps_parse_rmc(uint8_t *data, uint8_t len)
{
	DBG_ASSERT(data != NULL __DBG_LINE);
	
	bool_t result = FALSE;
	if ((len < GPS_RMC_LEN_MIN) || (len > GPS_RECV_LEN_MAX))
	{
		return result;
	}
	
	if ( gps_rmc_crc(data, len) )
	{
		uint8_t num = 0;
		uint8_t i = 0;
		uint8_t blank[ GPS_RMC_BLANK_NUM ] = {0};
		
		while ( i < len )
		{
			if ( data[i] == ',' )
			{
				blank[num] = i;
				num++;
			}
			i++;
		}
		
		if ( num == GPS_RMC_BLANK_NUM )
		{
			uint8_t status = data[ blank[1] + 1 ];
			
			if (status == 'A')
			{/* 定位成功 */
				if ( (blank[2] + GPS_LAT_LEN + 1 == blank[3]) 
					&& (blank[4] + GPS_LON_LEN + 1 == blank[5]) )
				{
					gps_simple_info.latitude = get_rmc_value( &data[blank[2] + 1], GPS_DATA_TYPE_LAT );
					gps_simple_info.longitude = get_rmc_value( &data[blank[4] + 1], GPS_DATA_TYPE_LON );
					gps_simple_info.speed = 0;
					result = TRUE;
				}
			}
			else if (status == 'V')
			{/* 定位不成功 */
				gps_simple_info.latitude = 0;
				gps_simple_info.longitude = 0;
				gps_simple_info.speed = 0;
				result = TRUE;
			}
			else
			{
				;
			}
		}
	}
	
	return result;
}

TEST(gps_test, gps_parse_rmc_test01)
{
	uint8_t str_1[] = "$GPRMC,075406.000,A,3129.5247,N,12022.1725,E,0.64,57.93,270115,,,A*5B\r\n";
//	uint8_t str_2[] = "$GPRMC,000000.800,V,,,,,0.00,0.00,060180,,,N*4A\r\n";
//	uint8_t str_3[] = "$GPRMC,040221.000,A,3129.5592,N,12022.1232,E,2.13,247.39,260115,,,D*67\r\n";
//	uint8_t str_4[] = "$GPRMC,040241.000,A,3129.5543,N,12022.1143,E,1.89,98.63,260115,,,D*57\r\n";
//	uint8_t str_5[] = "$GPRMC,040251.000,A,3129.5444,N,12022.1121,E,1.74,103.40,260115,,,D*64\r\n";
//	uint8_t str_6[] = "$GPRMC,040301.000,A,3129.5363,N,12022.1120,E,1.34,113.96,260115,,,D*6D\r\n";
//	uint8_t str_7[] = "$GPRMC,040311.000,A,3129.5338,N,12022.1140,E,1.88,175.40,260115,,,D*68\r\n";
//	uint8_t str_8[] = "$GPRMC,040321.000,A,3129.5320,N,12022.1182,E,1.70,159.27,260115,,,D*64\r\n";
	gps_parse_rmc(str_1,71);
	static double lat,lon;
	lat = gps_dm_to_d(gps_simple_info.latitude);
	lon = gps_dm_to_d(gps_simple_info.longitude);
//	gps_parse_rmc(str_2,49);
//	gps_parse_rmc(str_3,72);
//	gps_parse_rmc(str_4,71);
//	gps_parse_rmc(str_5,72);
//	gps_parse_rmc(str_6,72);
//	gps_parse_rmc(str_7,72);
//	gps_parse_rmc(str_8,72);
}