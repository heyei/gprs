#include <unity_fixture.h>

TEST_GROUP_RUNNER(serial_test)
{
	RUN_TEST_CASE(serial_test, serial_char_handle_test_001);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_002);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_003);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_004);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_005);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_006_01);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_006_02);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_007);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_008);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_009);
    RUN_TEST_CASE(serial_test, serial_char_handle_test_010);
}