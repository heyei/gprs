#include <unity_fixture.h>
#include <App_cmd.h>
#include <Serial.h>
#include <osel_arch.h>

static uint8_t test_recv_array[APP_CMD_SIZE_MAX];
//注意：
//此类case的测试前需要把serial.c中的serial_char_handle()函数改成
//可以外部模块调用的，即去掉static声明
extern void serial_char_handle(uint8_t serial_id, uint8_t ch);

/**
* 串口接收回调函数
*/
void app_serial_recv_proc(void)
{
    uint8_t frame_len = 0;
    uint8_t read_data = 0;
    osel_memset(test_recv_array, 0x00, sizeof(test_recv_array));
    
    while (serial_read(SERIAL_1, &read_data, sizeof(uint8_t)))
    {
        test_recv_array[frame_len++] = read_data;
        if (read_data == CMD_CR)
        {
            break;
        }
    }
    
}
/**
* 初始化函数
*/
static void serial_char_handle_test_init (void)
{
	serial_reg_t app_serial_reg;

	app_serial_reg.sd.valid = TRUE;
	app_serial_reg.sd.len = 2;
	app_serial_reg.sd.pos = 0;
	app_serial_reg.sd.data[0] = 'A';
	app_serial_reg.sd.data[1] = 'T';

	app_serial_reg.ld.valid = FALSE;

	app_serial_reg.argu.len_max = SERIAL_LEN_MAX;
	app_serial_reg.argu.len_min = SERIAL_LEN_MIN;

	app_serial_reg.ed.valid = TRUE;
	app_serial_reg.ed.len = 1;
	app_serial_reg.ed.data[0] = 0x0D;   // 'CR', enter

	app_serial_reg.echo_en = TRUE;
	app_serial_reg.func_ptr = app_serial_recv_proc;

	//串口帧初始化
   	 serial_fsm_init(SERIAL_1);

	//串口注册
	serial_reg(SERIAL_1, app_serial_reg);

} 

TEST_GROUP(serial_test);

TEST_SETUP(serial_test)
{
    printf("test setup...\r\n");
    serial_char_handle_test_init();
}

TEST_TEAR_DOWN(serial_test)
{
	printf("test tear down...\r\n");
}

/**
* Test ID: tc_serial_test_001
* Test Name: serial_char_handle_test_001
* Description:输入正确的帧，预期得到正确的帧
* Test Step: 
* 1) 调用serial_char_handle，输入预期正确的帧，接收到预期帧
  2) 第一次输入：{'A','T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_001)
{	
	uint8_t test_src_buf[4] = {'A','T',0x01,0x0D};
	uint8_t frame_len = 0;

    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    
	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_src_buf, test_recv_array , (frame_len-1));
	
}

/**
* Test ID: tc_serial_test_002
* Test Name: serial_char_handle_test_002
* Description:输入错误的帧，串口接收不到帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧
  2) 第一次输入：{'A',0x01,0x0D}
  3)预期结果：  {收不到}
*/
TEST(serial_test, serial_char_handle_test_002)
{	
	uint8_t test_src_buf[3] = {'A',0x01,0x0D};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf[1] = {0x00};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf, test_recv_array , frame_len);
	
}

/**
* Test ID: tc_serial_test_003
* Test Name: serial_char_handle_test_003
* Description:输入错误的帧，串口接收不到帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧
  2) 第一次输入：{0x01,0x0D}
  3)预期结果：  {收不到}
*/
TEST(serial_test, serial_char_handle_test_003)
{	
	uint8_t test_src_buf[2] = {0x01,0x0D};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf[1] = {0x00};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf, test_recv_array , frame_len);
	
}

/**
* Test ID: tc_serial_test_004
* Test Name: serial_char_handle_test_004
* Description:输入错误的帧，串口接收不到帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧
  2) 第一次输入：{'A','T',0x01}
  3)预期结果：  {收不到}
*/
TEST(serial_test, serial_char_handle_test_004)
{	
	uint8_t test_src_buf[3] = {'A','T',0x01};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf[1] = {0x00};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf, test_recv_array , frame_len);
	
}

/**
* Test ID: tc_serial_test_005
* Test Name: serial_char_handle_test_005
* Description:输入错误的帧，串口接收不到帧,再次输入正确的帧，能够正确得到预期帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{'A','T',0x01}
    第二次输入：{'A','T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_005)
{	
	uint8_t test_src_buf_01[3] = {'A','T',0x01};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[1] = {0x00};
    
    uint8_t test_src_buf_02[4] = {'A','T',0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , frame_len);
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_006_01
* Test Name: serial_char_handle_test_006_01
* Description:输入错误的帧，串口接收不到帧,再次输入正确的帧，能够正确得到预期帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{'A','T'}
    第二次输入：{0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_006_01)
{	
	uint8_t test_src_buf_01[2] = {'A','T'};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[1] = {0x00};
    
    uint8_t test_src_buf_02[2] = {0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , frame_len);
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_006_02
* Test Name: serial_char_handle_test_006_02
* Description:输入错误的帧，串口接收不到帧,再次输入正确的帧，能够正确得到预期帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{'A'}
    第二次输入：{'T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_006_02)
{	
	uint8_t test_src_buf_01[1] = {'A'};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[1] = {0x00};
    
    uint8_t test_src_buf_02[3] = {'T',0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , frame_len);
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_007
* Test Name: serial_char_handle_test_007
* Description:输入错误的帧，串口接收不到帧,再次输入正确的帧，能够正确得到预期帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{0x01,0x0D}
    第二次输入：{'A','T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_007)
{	
	uint8_t test_src_buf_01[2] = {0x01,0x0D};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[1] = {0x00};
    
    uint8_t test_src_buf_02[4] = {'A','T',0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , frame_len);
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_008
* Test Name: serial_char_handle_test_008
* Description:输入错误的帧，串口接收不到帧,再次输入正确的帧，能够正确得到预期帧
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{0x01}
    第二次输入：{'A','T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_008)
{	
	uint8_t test_src_buf_01[1] = {0x01};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[1] = {0x00};
    
    uint8_t test_src_buf_02[4] = {'A','T',0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 1);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , frame_len);
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_009
* Test Name: serial_char_handle_test_009
* Description:两次输入正确的帧，看是否可以正确输出
* Test Step: 
* 1) 两次输入正确的帧，看是否可以正确输出
  2)第一次输入：{'A','T',0x01,0x0D}
    第二次输入：{'A','T',0x01,0x0D}
  3)预期结果：  {'A','T',0x01,0x0D}
*/
TEST(serial_test, serial_char_handle_test_009)
{	
	uint8_t test_src_buf_01[4] = {'A','T',0x01,0x0D};
	uint8_t frame_len = 0;
    uint8_t test_expect_buf_01[4] = {'A','T',0x01,0x0D};
    
    uint8_t test_src_buf_02[4] = {'A','T',0x01,0x0D};
    uint8_t test_expect_buf_02[4] = {'A','T',0x01,0x0D};
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//step1：发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_01, test_recv_array , (frame_len-1));
    
    //step2：再次发送正确的帧
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));
    frame_len = 0;
    
   	//发送帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_02);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_02[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 5);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_expect_buf_02, test_recv_array , (frame_len-1));	
}

/**
* Test ID: tc_serial_test_010
* Test Name: serial_char_handle_test_010
* Description:测试发送AT指令的边界
* Test Step: 
* 1) 调用serial_char_handle，输入错误的帧，串口接收不到帧,再次输入正确的帧，
    能够正确得到预期帧
  2)第一次输入：{'A','T',0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x07,0x0D}
  3)预期结果：  {'A','T',0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x07,0x0D}
*/
TEST(serial_test, serial_char_handle_test_010)
{	
	uint8_t test_src_buf_01[40] = {'A','T',
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x01,0x02,0x03,0x04,0x05,0x06,
                        0x07,0x0D};
	uint8_t frame_len = 0;
    
    osel_memset(test_recv_array,0x00,sizeof(test_recv_array));

	//发送异常帧到串口
	for(uint8_t i = 0;i < sizeof (test_src_buf_01);i++)
	{
		serial_char_handle(SERIAL_1,test_src_buf_01[i]);
	}

	//从串口接收数据，放到了test_recv_array[]中
	while ('\0' != (test_recv_array[frame_len ++]));

	TEST_ASSERT_TRUE(frame_len == 41);
	TEST_ASSERT_EQUAL_UINT8_ARRAY(test_src_buf_01, test_recv_array , (frame_len-1));
}


