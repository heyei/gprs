#include <unity_fixture.h>
#include <osel_arch.h>
#include <nfc_module.h>
#include <nfc_driver.h>
#include <hal_nfc_driver.h>

static uint8_t read_para_temp[100] = {0};
static nfc_data_info_t nfc_data;

TEST_GROUP(nfc_rw);

TEST_SETUP(nfc_rw)
{
	nfc_init();
}

TEST_TEAR_DOWN(nfc_rw)
{
    _NOP();
}

TEST(nfc_rw, nfc_profile_id_rw)
{
//    static uint8_t send_data[32] = {1,2,3,4,5,6,7,8,
//                                    1,2,3,4,5,6,7,8,
//                                    1,2,3,4,5,6,7,8,
//                                    1,2,3,4,5,6,7,8};
//    uint8_t *send_p = (uint8_t *)&data_info;
//    write_nfc_data(send_p, 16, PROFILE_ID_ADDRESS);
//    nfc_read_info_flag((nfc_info_flag_t *)&read_data);
}

TEST(nfc_rw, nfc_info_flag_rw)
{
//    static uint8_t send_data[32] = {1,1,1,0,0,0,0,0,
//                                    0,0,1,0,0,0,0,0,
//                                    0,0,0,0,0,0,0,0,
//                                    0,0,0,0,0,0,0,0};
//    static uint32_t read_flag = 0;
//    static uint8_t *send_p = send_data;
//    write_nfc_data(send_p, 32, 0x0800);
//    nfc_read_info_flag((nfc_info_flag_t *)&read_flag);
}

//����NFC�ܹ�64�������Ķ�д
TEST(nfc_rw, nfc_byte_rw)
{
    static uint8_t send_data[128] = {0};
    static uint8_t recv_data[128] = {0};
    static uint8_t *send_p = send_data;
    static uint8_t *recv_p = recv_data;
    uint8_t cnt = 0;                            //������                  
    uint8_t cnt_totle = 0;                      //��д����
    uint32_t addr_temp = 0x00;                  //��д��ַ
    while(cnt_totle<20)
    {
        while(cnt < 64)                            //��д����
        {
            write_nfc_data(send_p, 128, addr_temp);
            read_nfc_data(recv_p, 128, addr_temp); 
            cnt ++;
            addr_temp += 128;
        }
        cnt = 0;
        addr_temp = 0x00; 
        cnt_totle++;
        osel_memset(send_data, cnt_totle, 128);
    }
}

//����������ά����
TEST(nfc_rw, alg_para_over_run_cfg_vm4)
{
    uint8_t para_temp[8] = {1,0,2,0,3,0,4,0};
    nfc_info_flag_t flag;
    nfc_data.app_type = APP_TYPE_DEVICE_INFO_ACK;
    nfc_data.nfc_device_info_pld.info_type = PARA_TYPE_PROFILE_INFO;
    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_info_pld.profile_info.profile_id),
                (uint8_t *)&(para_temp), 8);
    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_info_pld.profile_info.version),
                (uint8_t *)&(para_temp), 4);
    nfc_data.crc = 0xff;
    while(1)
    {
       nfc_read_info_flag(&flag);
       if(flag.unbind == 1)
       {
           nfc_read_info((nfc_data_info_t *)read_para_temp, 
                      APP_TYPE_BIND_INFO_CFG, 
                      PARA_TYPE_UNBIND_INFO);
           break;
       }
       delay_ms(1000);
    }
//    nfc_write_info(&nfc_data);
	
//    nfc_clr_info(APP_TYPE_DEVICE_INFO_ACK, PARA_TYPE_DEVICE_NUI);
//    nfc_read_info((nfc_data_info_t *)read_para_temp, APP_TYPE_DEVICE_INFO_ACK, PARA_TYPE_DEVICE_NUI);
//    hal_read_nfc_data(read_para_temp, 32, CARRIAGE_NUM_ADDRESS);
//    nfc_read_info((nfc_data_info_t *)read_para_temp, APP_TYPE_ALG_PARA_CFG, PARA_TYPE_OVERRUN_DETEC_ALG);
    _NOP();
}

//����SSN����
TEST(nfc_rw, ssn_info_cfg)
{
//    uint8_t id[8] = {1,0,2,0,3,0,4,0};
//    uint8_t in_ch[8] = {1,1,1,1,1,1,1,1};
//    uint8_t ex_ch[8] = {8,8,8,8,8,8,8,8};
//    uint8_t hb_cyc[4] = {48,0,0,0};
//    
//    nfc_data.app_type = APP_TYPE_SSN_DEV_INFO;
//    nfc_data.nfc_device_ssn_info_pld.info_type = PARA_TYPE_ALL_SSN_INFO;
//    
//    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_ssn_info_pld.device_info.device_id),
//                (uint8_t *)&id, 8);
//    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_ssn_info_pld.device_info.intra_ch),
//                (uint8_t *)&in_ch, 8);
//    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_ssn_info_pld.device_info.exter_ch),
//                (uint8_t *)&ex_ch, 8);
//    nfc_data.nfc_device_ssn_info_pld.device_info.power_sn = 0x07;
//    nfc_data.nfc_device_ssn_info_pld.device_info.intra_ch_cnt = 0x01;
//    nfc_data.nfc_device_ssn_info_pld.device_info.intra_data_simu = 0x01;
//
//    osel_memcpy((uint8_t *)&(nfc_data.nfc_device_ssn_info_pld.device_info.heartbeat_cycle),
//                (uint8_t *)&hb_cyc, 4);
//    nfc_data.crc = 0xff;
//    
//    nfc_write_info(&nfc_data);
//    nfc_read_info((nfc_data_info_t *)read_para_temp, APP_TYPE_SSN_DEV_INFO,
//                  PARA_TYPE_ALL_SSN_INFO);
//    nfc_clr_info(APP_TYPE_SSN_DEV_INFO, PARA_TYPE_ALL_SSN_INFO);
//    nfc_read_info((nfc_data_info_t *)read_para_temp, APP_TYPE_SSN_DEV_INFO,
//                  PARA_TYPE_ALL_SSN_INFO);
}