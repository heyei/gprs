#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include "gprs_queue.h"

circular_queue_t gprs_buf_queue;        // GPRS缓冲队列

static void clear_queue(void)
{
    queue_data_type old_item;

    while(queue_receive(&gprs_buf_queue, &old_item));
}

TEST_GROUP(gprs_queue_test);

TEST_SETUP(gprs_queue_test)
{
    ;
}

TEST_TEAR_DOWN(gprs_queue_test)
{
    ;
}

TEST(gprs_queue_test, gprs_test_queue_create)
{
    // 创建队列
    queue_create(&gprs_buf_queue);

    TEST_ASSERT_EQUAL_UINT8(gprs_buf_queue.front, 0);
    TEST_ASSERT_EQUAL_UINT8(gprs_buf_queue.rail, 0);
    TEST_ASSERT_EQUAL_UINT8(gprs_buf_queue.count, 0);
}

TEST(gprs_queue_test, gprs_test_queue_send)
{
    queue_data_type new_item;
    new_item.gprs_data_len = 10;
    for(uint8_t i = 0; i < new_item.gprs_data_len; i++ )
    {
        new_item.gprs_data[i] = i;
    }

    // 向队列发送内容
    TEST_ASSERT_TRUE(queue_send(&gprs_buf_queue, new_item));
}

TEST(gprs_queue_test, gprs_test_queue_receive)
{
    queue_data_type old_item;

    // 从队列中读取数据
    TEST_ASSERT_TRUE(queue_receive(&gprs_buf_queue, &old_item));

    TEST_ASSERT_EQUAL_UINT8(old_item.gprs_data_len, 10);

    for(uint8_t i = 0; i < old_item.gprs_data_len; i++)
    {
        TEST_ASSERT_EQUAL_UINT8(old_item.gprs_data[i],i);
    }
}

TEST(gprs_queue_test, gprs_test_queue_count)
{
    queue_data_type new_item,old_item;
    memset(&new_item, 0 , sizeof new_item);
    memset(&old_item, 0 , sizeof old_item);

    TEST_ASSERT_EQUAL_INT(queue_count(&gprs_buf_queue), 0);
    // 插入内容
    queue_send(&gprs_buf_queue, new_item);
    // 队列个数是否变为1
    TEST_ASSERT_EQUAL_UINT8(queue_count(&gprs_buf_queue), 1);

    queue_receive(&gprs_buf_queue, &old_item);
    // 队列个数是否变为0
    TEST_ASSERT_EQUAL_UINT8(queue_count(&gprs_buf_queue), 0);
}

TEST(gprs_queue_test, gprs_test_queue_peek)
{
    queue_data_type new_item,old_item;
    memset(&new_item, 0 , sizeof new_item);
    memset(&old_item, 0 , sizeof old_item);

    // 队列为空时 peek队列
    TEST_ASSERT_FALSE(queue_peek(&gprs_buf_queue, &old_item));
    // 插入元素
    queue_send(&gprs_buf_queue, new_item);

    TEST_ASSERT_TRUE(queue_peek(&gprs_buf_queue, &old_item));
    // 队列个数依然为1
    TEST_ASSERT_EQUAL_UINT8(queue_count(&gprs_buf_queue), 1);

    // 清空队列
    clear_queue();
}

TEST(gprs_queue_test, gprs_test_queue_empty)
{
    queue_data_type new_item,old_item;
    memset(&new_item, 0 , sizeof new_item);
    memset(&old_item, 0 , sizeof old_item);

    // 未填充任何元素时，队列为空
    TEST_ASSERT_TRUE(is_queue_empty(&gprs_buf_queue));

    // 插入元素
    queue_send(&gprs_buf_queue, new_item);

    TEST_ASSERT_FALSE(is_queue_empty(&gprs_buf_queue));

    // 清空队列
    clear_queue();
}

TEST(gprs_queue_test, gprs_test_queue_full)
{
    queue_data_type new_item,old_item;
    memset(&new_item, 0 , sizeof new_item);
    memset(&old_item, 0 , sizeof old_item);

    for(uint8_t i = 0; i < QUEUE_MAXLEN; i++)
    {
       queue_send(&gprs_buf_queue, new_item);
    }

    // 队列填充为满，无法插入内容
    TEST_ASSERT_FALSE(queue_send(&gprs_buf_queue, new_item));
    TEST_ASSERT_TRUE(is_queue_full(&gprs_buf_queue));

    // 清空队列
    clear_queue();
}

TEST(gprs_queue_test, gprs_test_queue_send_receive)
{
    queue_data_type new_item,old_item;
    memset(&new_item, 0 , sizeof new_item);
    memset(&old_item, 0 , sizeof old_item);

    // 连续填入，连续取出
    for(uint16_t i = 0; i < 2*QUEUE_MAXLEN; i++)
    {
       TEST_ASSERT_TRUE(queue_send(&gprs_buf_queue, new_item));
       TEST_ASSERT_TRUE(queue_receive(&gprs_buf_queue, &old_item));
    }
}