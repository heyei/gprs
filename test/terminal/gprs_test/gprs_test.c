#include <unity_fixture.h>
#include <string.h>
#include <stdio.h>
#include <data_type_def.h>
#include <hal.h>

#include "gprs.h"
#include <string.h>

#define BUILD_IP_ADDRESS(b3, b2, b1, b0) ((uint32_t)(b3) << 24) | \
     ((uint32_t)(b2) << 16) | ((uint32_t)(b1) << 8) | ((uint32_t)(b0)) 

extern gprs_data_cb_t   gprs_get_data_cb;
extern gprs_type_cb_t   gprs_cb; 
extern circular_queue_t gprs_queue;    
extern gprs_mode_t      gs_mode; 
extern bool_t           send_data_important;
extern uint16_t         send_data_kind;
         
static gprs_config_t test_gprs_config;

static void test_type_cb(uint16_t param, uint16_t tag, uint16_t type)
{
    switch (param)
    {
    case GS_SEND_OK:
        PRINTF("\r\nsend ok\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,1);
        TEST_ASSERT_EQUAL_UINT8(type,0);
        break;
        
    case GS_SEND_FAIL:
        PRINTF("\r\nsend fail\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,2);
        TEST_ASSERT_EQUAL_UINT8(type,1);
        break;
        
    case GS_ERROR:
        PRINTF("\r\ngprs error\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,3);
        TEST_ASSERT_EQUAL_UINT8(type,2);
        break;
        
    case GS_NO_NETWORK:
        PRINTF("\r\ngprs no network\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,4);
        TEST_ASSERT_EQUAL_UINT8(type,0);
        break;
            
    case GS_NO_DATA_SEND:
        PRINTF("\r\ngprs no data send\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,5);
        TEST_ASSERT_EQUAL_UINT8(type,1);
        break;
        
    case GS_NO_DATA_RECV:
        PRINTF("\r\ngprs no data receive\r\n");
        TEST_ASSERT_EQUAL_UINT8(tag,6);
        TEST_ASSERT_EQUAL_UINT8(type,2);
        break; 
        
    default:
        break;
    }
}

static void test_data_cb(gprs_receive_t param)
{
    TEST_ASSERT_EQUAL_UINT8(param.gprs_data[0], 2);
    TEST_ASSERT_EQUAL_UINT8(param.gprs_data[1], 3);
    TEST_ASSERT_EQUAL_UINT8(param.gprs_data[2], 4);
    TEST_ASSERT_EQUAL_UINT8(param.len, 3);
}

TEST_GROUP(gprs_test);

TEST_SETUP(gprs_test)
{ 
    test_gprs_config.data_cb = test_data_cb;
    test_gprs_config.type_cb = test_type_cb;
    test_gprs_config.ip_addr = BUILD_IP_ADDRESS(58, 214, 236, 152);
    test_gprs_config.port    = 8066;

    gprs_config(&test_gprs_config);
}

TEST_TEAR_DOWN(gprs_test)
{
    ;
}

TEST(gprs_test, gprs_test_01)
{  
    (* gprs_cb)(GS_SEND_OK, 1, 0);
}

TEST(gprs_test, gprs_test_02)
{  
    (* gprs_cb)(GS_SEND_FAIL, 2, 1);
}

TEST(gprs_test, gprs_test_03)
{  
    (* gprs_cb)(GS_ERROR, 3, 2);
}

TEST(gprs_test, gprs_test_04)
{  
    (* gprs_cb)(GS_NO_NETWORK, 4, 0);
}

TEST(gprs_test, gprs_test_05)
{  
    (* gprs_cb)(GS_NO_DATA_SEND, 5, 1);
}

TEST(gprs_test, gprs_test_06)
{  
    (* gprs_cb)(GS_NO_DATA_RECV, 6, 2);
}

TEST(gprs_test, gprs_test_07)
{
    uint8_t  DATA[] = {2,3,4};
    uint16_t len = 3;
    
    gprs_receive_t test_gprs_recive;
    memset(&test_gprs_recive, 0, sizeof(gprs_receive_t));
    
    memcpy(test_gprs_recive.gprs_data, DATA, strlen((char *)DATA));
    test_gprs_recive.len = len;
    
    (*gprs_get_data_cb)(test_gprs_recive);
}

//TEST(gprs_test, gprs_test_08)
//{
//    gprs_set_mode(SINGLE_MODE, FALSE, 2);
//    TEST_ASSERT_EQUAL_UINT8(gs_mode, SINGLE_MODE);
//    TEST_ASSERT_EQUAL_UINT8(send_data_important, FALSE);
//    TEST_ASSERT_EQUAL_UINT16(send_data_kind, 2);
//}

TEST(gprs_test, gprs_test_09)
{
    uint8_t DATA[] = {1,2,3};
    //���뷢�Ͷ���
    gprs_send(DATA, sizeof(DATA), 1, 0, 1);
    
    queue_data_type  data;
    queue_receive(&gprs_queue, &data);
    
    TEST_ASSERT_EQUAL_UINT8(data.gprs_data[0], 1);
    TEST_ASSERT_EQUAL_UINT8(data.gprs_data[1], 2);
    TEST_ASSERT_EQUAL_UINT8(data.gprs_data[2], 3);
    TEST_ASSERT_EQUAL_UINT8(data.gprs_data_len, 3);
    TEST_ASSERT_EQUAL_UINT16(data.ack_time, 0);
}
