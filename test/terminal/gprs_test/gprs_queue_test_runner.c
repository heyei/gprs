#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

#include "gprs_queue.h"

TEST_GROUP_RUNNER(gprs_queue_test)
{
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_create);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_send);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_receive);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_count);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_peek);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_empty);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_full);
    RUN_TEST_CASE(gprs_queue_test, gprs_test_queue_send_receive);
}
