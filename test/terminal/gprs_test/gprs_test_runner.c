#include <unity_fixture.h>
#include <osel_arch.h>
#include <hal.h>

TEST_GROUP_RUNNER(gprs_test)
{
    RUN_TEST_CASE(gprs_test, gprs_test_01);
    RUN_TEST_CASE(gprs_test, gprs_test_02);
    RUN_TEST_CASE(gprs_test, gprs_test_03);
    RUN_TEST_CASE(gprs_test, gprs_test_04);
    RUN_TEST_CASE(gprs_test, gprs_test_05);
    RUN_TEST_CASE(gprs_test, gprs_test_06);
    RUN_TEST_CASE(gprs_test, gprs_test_07);
//    RUN_TEST_CASE(gprs_test, gprs_test_08);
    RUN_TEST_CASE(gprs_test, gprs_test_09);
}