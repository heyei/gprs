#include "unity_fixture.h"

static void RunAllTests(void)
{
//    RUN_TEST_GROUP(ssn_app);
    RUN_TEST_GROUP(nfc_rw);
//    RUN_TEST_GROUP(gps_test);
//    RUN_TEST_GROUP(acc_alg_test);
//    RUN_TEST_GROUP(gprs_queue_test);
//    RUN_TEST_GROUP(gprs_test);
//    RUN_TEST_GROUP(tfp_printf_test);
}

static char *argstring[] =
{
	"main",
	"-v",
};

int main(int argc, char *argv[])
{
	argc = sizeof(argstring) / sizeof(char *);
	argv = argstring;

	return UnityMain(argc, argv, RunAllTests);
}
